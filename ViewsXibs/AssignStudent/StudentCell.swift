//
//  StudentCell.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2015. 11. 19..
//  Copyright © 2015년 IMLab. All rights reserved.
//

import Foundation

class AssignStudentCell : UITableViewCell {
    var log = HtLog(cName: "AssignStudentCell")
    var curStudent:HmStudent?
    
    var selectedCallBack: (HmStudent, Bool)->() = {obj, isOn in  }
    
    //@IBOutlet weak var bttnCheckImg: UIButton!
    @IBOutlet weak var imgCheck: UIImageView!


    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelEmail: UILabel!
    @IBOutlet weak var labelFinish: UILabel!
    
    @IBOutlet weak var lblProtocol: UILabel!

    override func awakeFromNib() {
    }
    
    @IBAction func switchValueChanged(sender: UISwitch) {
        print("  Touched ::  value is \(sender.on)")
        //selectedCallBack(curStudent!, swtActive.on)
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if curStudent != nil {
            //setCheckImage(curStudent!.uiTargeted)
            //curStudent!.uiTargeted = selected
        }
    }

    func setActive(valid: Bool?) {
        print("  Cell set Active \(curStudent?.name)  \(valid)   target?  \(curStudent?.uiTargeted) ")
        var chk = valid
        if valid == nil {
            chk = curStudent!.uiTargeted
        }
        setCheckImage(chk!)
    }

    private func setCheckImage(theVar: Bool) {
        print("  setCheckImage   \(theVar)")
        if theVar {
            //bttnCheckImg.setImage(UIImage(named: "btn_check"), forState: UIControlState.Normal)
            imgCheck.image = UIImage(named: "btn_check")
        } else {
            //bttnCheckImg.setImage(UIImage(named: "btn_check_not"), forState: UIControlState.Normal)
            imgCheck.image = UIImage(named: "btn_check_not")
        }
    }

    func refreshUIState() {
        if (curStudent?.isDataInDB)! == true {
            labelFinish.text = langStr.obj.exist
            labelFinish.textColor = colBttnGreen
        } else {
            labelFinish.text = "-"
        }
    }

    func setStudent(stu: HmStudent) {
        backgroundColor = HmGraphSetting.inst.viewBgBrightGray
        //swtActive.on = false
        curStudent = stu
        labelName.text = curStudent?.name
        labelEmail.text = curStudent?.email
        refreshUIState()
        
        checkProtocol()
    }
    
    func baseSet() {
        //swtActive.on = false
        labelName.text = "John Doe"
        labelEmail.text = "dkdkdk@asdj.net"
        labelFinish.text = "Not Yet"
    }


    func checkProtocol() {
        lblProtocol.text = curStudent!.getStandardStr()

        imgCheck.showMe()
        let _ = [ labelName, labelEmail ].map { (lbl) in
            lbl.textColor = UIColor.blackColor()
        }

        if AppIdTrMoTe == 1 || curStudent!.activeForStandard() { return }

        print(" AHA / ENC / ANZ \(#function)  \(curStudent!.name)   \(lblProtocol.text)  ")

        // 현재 기준이 아닐 때 Disable...
        let _ = [ labelName, labelEmail ].map { (lbl) in
            lbl.textColor = colBttnGray // UIColor.lightGrayColor()
        }
        imgCheck.hideMe()
    }

}

