//
//  AssignStudent.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2015. 11. 11..
//  Copyright © 2015년 IMLab. All rights reserved.
//

import Foundation
import UIKit

class AssignStudentView : HtViewController, UIAlertViewDelegate, SearchViewPrtcl {
    var log = HtLog(cName: "AssignStudentView")

    /// SearchViewPrtcl
    var searchTxtField: UITextField?
    var searchMode = false
    var searchConstraintWidth: NSLayoutConstraint?
    var searchedList = [HmStudent]()

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var tableStudent: UITableView!
    @IBOutlet weak var labelStudentInfo: UILabel!
    @IBOutlet weak var labelData: UILabel!
    
    @IBOutlet weak var labelA: UILabel!
    @IBOutlet weak var labelB: UILabel!
    @IBOutlet weak var labelC: UILabel!
    @IBOutlet weak var labelD: UILabel!
    
    @IBOutlet weak var labelAName: UILabel!
    @IBOutlet weak var labelBName: UILabel!
    @IBOutlet weak var labelCName: UILabel!
    @IBOutlet weak var labelDName: UILabel!
    
    @IBOutlet weak var bttnFinish: UIButton!
    @IBOutlet weak var bttnCancel: UIButton!

    /// Sorting...
    var sortByDate = true, sortAscend = false
    @IBOutlet weak var bttnSortDate: SortBttn!
    @IBOutlet weak var bttnSortName: SortBttn!

    @IBAction func sortTouched(sender: SortBttn) {
        sender.sortTouched()
        sortByDate = sender.accessibilityIdentifier! == "date"
        sortAscend = sender.isAscend
        sortAction()
    }

    func sortFromSearchProtocol(union: [HmStudent]) {
        searchedList = sortStudentArrayBy(sortByDate, ascn: sortAscend, union: studentList)
    }

    func sortAction() {
        HsBleMaestr.inst.arrStudent =
            sortStudentArrayBy(sortByDate, ascn: sortAscend, union: HsBleMaestr.inst.arrStudent)
        sortFromSearchProtocol(studentList)
        reloadTableView()
    }
    var arrSelectedStudents:[HmStudent?] = [nil, nil, nil, nil] // 항상 4개..
    var arrCell = [AssignStudentCell]()

    var studentList: [HmStudent] {
        get {
            if searchMode { return searchedList
            } else {        return HsBleMaestr.inst.arrStudent }
        }
    }
    
    @IBAction func bttnFinished(sender: AnyObject) {
        goToOperationVC()
    }

    func goToOperationVC() {

        let arrNil = arrSelectedStudents.filter { (stdnt) -> Bool in
            stdnt == nil
        }

        if testInfant && arrNil.count == 4 {
            최소1개연결메시지보이기({ print(" 테스트 영아.  할당 없슴. ") })
            return
        }

        showGeneralPopup(nil, message: langStr.obj.want_assign_completed, yesStr: langStr.obj.yes, noString: langStr.obj.no, yesAction:
            { () -> Void in
                self.assignStudentsToBleSuMan()
                if AppIdTrMoTe == 1 {
                    let vc =  OperationMainVC(nibName: "OperateMainVC", bundle: nil)
                    self.navigationController?.pushViewController(vc, animated: viewAnimate)
                } else {
                    let vc =  OperationVC(nibName: "OperationVC", bundle: nil)
                    self.navigationController?.pushViewController(vc, animated: viewAnimate)
                }
        })
    }

    @IBAction func bttnCancel(sender: AnyObject) {
        showGeneralPopup("", message: langStr.obj.want_back, yesStr: langStr.obj.yes, noString: langStr.obj.no, yesAction: { self.navigationController?.popToViewController((self.navigationController?.viewControllers[1])!, animated: viewAnimate)} )
    }

    //////////////////////////////////////////////////////////////////////     [   검색 관련    << >> ]

    @IBOutlet weak var constSearchWidth: NSLayoutConstraint!
    @IBOutlet weak var bttnSearch: UIButton!
    @IBOutlet weak var txtSearch: UITextField!

    @IBAction func bttnActSearch(sender: UIButton) { // 탐색 기능 토글..
        searchMode = !searchMode
        searchUIAnimation()
        if !searchMode {
            view.endEditing(true)
        }
        refreshSearchTextAction( {std in
            return std.name.containsString(self.txtSearch.text!) || std.email.containsString(self.txtSearch.text!)
            }, totalList: HsBleMaestr.inst.arrStudent)
    }

    @IBAction func searchTextChangedAction(sender: UITextField) {
        print("searchTextChangedAction  length : \(sender.text!.getLength())   \(sender.text!.getLengthUTF32())")

        refreshSearchTextAction( {std in
            return std.name.containsString(self.txtSearch.text!) || std.email.containsString(self.txtSearch.text!)
            }, totalList: HsBleMaestr.inst.arrStudent)
    }

    //////////////////////////////////////////////////////////////////////     [      << >> ]
    
    func assignStudentsToBleSuMan() { // 현재 선택된 놈을 할당한 후 오퍼레이션 페이지로 이동
        HsBleMaestr.inst.resetData() // Student의 myData = nil..
        for (idx, 학생) in arrSelectedStudents.enumerate() {
            if 학생 == nil { continue }
            학생?.myData = HsBleMaestr.inst.arrBleSuMan[idx].dataObj
            print("obj?.myData = HsBleMaestr.inst.arrBleSuMan[idx].dataObj \(idx) ")
            HsBleMaestr.inst.arrBleSuMan[idx].curStudent = 학생
        }
    }

    func resetAssignToBleSuMan() {
        for st in HsBleMaestr.inst.arrBleSuMan {
            st.curStudent = nil
        }
    }
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if buttonIndex == 0 {
            //self.navigationController?.popViewControllerAnimated(true)
        }
    }

    // MARK:  override func ........
    override func viewDidLoad() {
        print("  Assign Student View    view Did Load")
        view.backgroundColor = HmGraphSetting.inst.viewBgBrightGray
        tableStudent.backgroundColor = HmGraphSetting.inst.viewBgBrightGray

        let nib = UINib(nibName: "AssignStudentCell", bundle: nil)
        tableStudent.registerNib(nib, forCellReuseIdentifier: "assignStudentCell")
        labelTitle.text = "Assign Students ... "
        
        labelAName.text = "..";        labelBName.text = ".."
        labelCName.text = "..";        labelDName.text = ".."
        
        tableStudent.allowsSelection = false
        tableStudent.allowsMultipleSelection = true

        /// Search Protocol
        searchTxtField = txtSearch
        searchConstraintWidth = constSearchWidth
    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewWillAppear(animated: Bool) {
        setLanguageString()
        HsBleMaestr.inst.readDB()
        txtSearch.autocorrectionType = .No
        searchUIAnimation()
        txtSearch.hideMe()
        kitNameLabelShowHide()

//        switch cprProtoN.theVal { // 이걸 원상 복구 하는 건 Select View 에서..
//        case 0:
//            CC _STRONG_POINT = 101
//        default:
//            CC _STRONG_POINT = Int32(CC_STRONG_POINT_DEFAULT)
//        }

        /// sorting initial action
        bttnSortDate.initDateNameState(bttnSortName)
        sortAction()

        resetAssignToBleSuMan()
    }

    override func viewDidAppear(animated: Bool) {
        timerInterval = 0.8 // second
        super.viewDidAppear(animated) // 타이머 세팅.
    }

    override func update() {
        super.update()

        if testInfant { return }

        if HsBleMaestr.inst.connectionNumber() == 0 { // 앞 화면으로 돌아가기.
            uiTimer.invalidate() // 이게 없으면 여러번 뜬다..
            최소1개연결메시지보이기({
                self.navigationController?.popViewControllerAnimated(viewAnimate)
            })
            return
        }
        checkBleConnections()
    }

    func 최소1개연결메시지보이기(succ: Void -> Void) {
        let altVw = UIAlertView(title: langStr.obj.at_least_1_connect, message: nil, delegate: self,
                                cancelButtonTitle: nil)
        altVw.show()
        HsGlobal.delay(2.0, closure: { () -> () in
            altVw.dismissWithClickedButtonIndex(0, animated: viewAnimate)
            succ()
        })
    }

    private func kitNameLabelShowHide() {
        let arrMan = HsBleMaestr.inst.arrBleSuMan
        showEvery(labelA, labelB, labelC, labelD, labelAName, labelBName, labelCName, labelDName)
        if testInfant { return }
        if !arrMan[0].isConnected { labelA.hidden = true; labelAName.hidden = true }
        if !arrMan[1].isConnected { labelB.hidden = true; labelBName.hidden = true }
        if !arrMan[2].isConnected { labelC.hidden = true; labelCName.hidden = true }
        if !arrMan[3].isConnected { labelD.hidden = true; labelDName.hidden = true }
    }

    private func checkBleConnections() {
        let arrABCD = [ labelA, labelB, labelC, labelD ]
        for idx in 0...3 {
            let bObj = HsBleMaestr.inst.arrBleSuMan[idx]
            let stdnt = arrSelectedStudents[idx]
            let labelShow = !arrABCD[idx].hidden
            if (labelShow || stdnt != nil) && bObj.isConnectionTotallyFinished == false { // 끊겼는데 할당되어 있을 때..
                log.printAlways("   끊겼는데 할당되어 있을 때..  ")
                let arrSameStdnt = HsBleMaestr.inst.arrStudent.filter({ (std) -> Bool in
                    return std == stdnt
                })
                if 0 < arrSameStdnt.count {
                    let sIdx = HsBleMaestr.inst.arrStudent.indexOf(stdnt!)!
                    print("   혹시나...")
                    arrCell[sIdx].setActive(false)
                }
                kitNameLabelShowHide()
            }
        }
    }

    // MARK:  언어 세팅.
    func setLanguageString() {
        //view.backgroundColor = HmGraphSetting.inst.viewBgBrightGray
        labelTitle.text = langStr.obj.assign_student
        labelStudentInfo.text = langStr.obj.student_info
        labelData.text = langStr.obj.data
        bttnFinish.setTitle(langStr.obj.start_course, forState: UIControlState.Normal)
        bttnCancel.setTitle(langStr.obj.cancel, forState: UIControlState.Normal)
    }
}


extension AssignStudentView : UITableViewDataSource, UITableViewDelegate {

    /// Xib 에서 multi cell selection 을 선택 안해서 고생.. ㅠㅠㅠ
    func reloadTableView() {
        if UI_USER_INTERFACE_IDIOM() == .Phone {
            //tableStudent.rowHeight = 30;
        }
        bttnSearch.setImageAccordingTo(searchMode, trueImg: "search_close.png", falseImg: "search.png")
        tableStudent.reloadData()
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        arrCell = [AssignStudentCell]()
        return studentList.count // HsBleMaestr.inst.arrStudent.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        print(" AssignStudentView ::  Assign Student : \(indexPath.row) ")
        let cell:AssignStudentCell = tableView.dequeueReusableCellWithIdentifier("assignStudentCell", forIndexPath:indexPath) as! AssignStudentCell

        let student = studentList[indexPath.row]
        cell.setStudent(student)

        arrCell.append(cell)
        if student.uiTargeted {
            tableStudent.selectRowAtIndexPath(indexPath, animated: viewAnimate, scrollPosition: .None)
        }
        cell.setActive(nil)
        return cell
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) { // 선택 되었을 때..
        log.printAlways("   didSelectRowAtIndexPath    \(indexPath.row) ")
        let curCell = tableStudent.cellForRowAtIndexPath(indexPath) as! AssignStudentCell

        if AppIdTrMoTe == 2 && !curCell.curStudent!.activeForStandard() {
            curCell.setSelected(false, animated: false)
            return
        }

        if cellSelectedTogged(indexPath.row, isOn: true) {
            log.logUiAction("  cellSelectedTogged   취소..  not effective touch 이미 다 찼다..")
            curCell.setSelected(false, animated: false)
            return
        }
        curCell.setActive(true) //.swtActive.setOn(true, animated: true)
    }

    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) { // 선택 해제 시 ....
        log.printAlways(" didDeselectRowAtIndexPath 해제.. :: at \(indexPath.row)")
        cellSelectedTogged(indexPath.row, isOn: false) // 해제는 항상 가능
        let curCell = tableStudent.cellForRowAtIndexPath(indexPath) as! AssignStudentCell
        print("     curCell 해제   >>   \(curCell.curStudent!.name)  ")
        curCell.setActive(false)
    }

    /// 셀 선택 / 해제 세팅..
    func cellSelectedTogged(pIdx: Int, isOn: Bool) -> Bool {
        log.printThisFunc("cellSelectedTogged (\(pIdx),    \(isOn)  \(assignFinished()) ) ")
        if isOn && assignFinished() { return true }

        //let name = HsBleMaestr.inst.arrStudent[pIdx].name // 어레이 중에서 찾아야 함..
        let name = studentList[pIdx].name // 어레이 중에서 찾아야 함..
        let labelArray = [ labelAName, labelBName, labelCName, labelDName ]
        for (idx, student) in arrSelectedStudents.enumerate() {
            let curBleMan = HsBleMaestr.inst.arrBleSuMan[idx]
            if isOn {
                if student == nil && (curBleMan.isConnectionTotallyFinished || testInfant ) { // 이름 할당 및 BleMan에도 할당.
                    arrSelectedStudents[idx] = studentList[pIdx]
                    studentList[pIdx].uiTargeted = true
                    labelArray[idx].text = name
                    return false
                }
            } else {
                if student?.name == name { // 초기화.
                    arrSelectedStudents[idx] = nil
                    labelArray[idx].text = ".."
                    studentList[pIdx].uiTargeted = false
                    return false
                }
            }
        }
        return false
    }

    func assignFinished() -> Bool {
        let notNilObjArr = arrSelectedStudents.filter { (std) -> Bool in  return std != nil }
        if testInfant {
            return notNilObjArr.count == 4
        }
        return notNilObjArr.count == HsBleMaestr.inst.connectionNumber()
    }
}

//
//
//func oldFinishedxx() { // 연결이 없을 때 No 가 필요함.
//    if HsBleMaestr.inst.connectionNumber() == 0 { //        if HsBleMaestr.inst.connectionNumber() != 0 {
//        let altVw = UIAlertView(title: langStr.obj.at_least_1_connect, message: nil, delegate: self,
//                                cancelButtonTitle: nil) //  local Str("offline")
//        altVw.show()
//        HsGlobal.delay(3.0, closure: { () -> () in
//            altVw.dismissWithClickedButtonIndex(0, animated: viewAnimate)
//        })
//    } else {
//        showSimpleMessageWith(langStr.obj.want_assign_completed) { () -> Void in
//            self.assignStudentsToBleSuMan()
//            let vc =  OperationMainVC(nibName: "OperateMainVC", bundle: nil)
//            self.navigationController?.pushViewController(vc, animated: viewAnimate)
//        }
//    }
//}

