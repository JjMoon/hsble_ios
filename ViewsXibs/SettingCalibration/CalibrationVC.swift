//
//  CalibrationVC.swift
//  heartisense
//
//  Created by Jongwoo Moon on 2015. 10. 11..
//  Copyright © 2015년 gyuchan. All rights reserved.
//

import UIKit
import PSAlertView


@IBDesignable

class CalibrationVC: UIViewController {
    var log = HtLog(cName: "CalibrationVC")
    var uiTimer = NSTimer()
    var curStepWatch = MuState()

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelCenterTitle: UILabel!
    @IBOutlet weak var labelCounter: UILabel!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var imgVwCenter: UIImageView!
    @IBOutlet weak var imgStage12: UIImageView!
    @IBOutlet weak var imgVwBreath: UIImageView!
    @IBOutlet weak var labelKitID: UILabel!
    @IBOutlet weak var labelNotification: UILabel!
    @IBOutlet weak var bttn4Debug: UIButton!
    @IBOutlet weak var bttnGotoPrev: UIButton!
    @IBOutlet weak var bttnSkip: UIButton!

    var bleMan: HsBleCtrl? // 모니터 작업 관련 : 싱글톤 사용 금지.
    var dObj: HsData? //HsBleManager.inst().dataObj

    var imgStage1 = true
    //var sizeAni = CABasicAnimation(), sizeYani = CABasicAnimation()


    func setBleObject(bleObj: HsBleCtrl) {
        bleMan = bleObj
        dObj = bleMan!.dataObj
        log.bleObj = bleMan!

        log.printAlways("setBleObject ::  뷰 띄울 때 실행... bleMan = \(bleMan)  dObj \(dObj)")
    }

    override func shouldAutorotate() -> Bool {
        return false
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        if let nm = bleMan?.thePort?.name { // Kit ID 세팅..
            print("  port.. name : \(nm)")
            labelKitID.text = "Kit ID : \(nm.subStringFrom(4))"
        } else {
            labelKitID.text = ""
        }

        bttn4Debug.show_다음이_참이면(isDebugMode)
        //let devID = NSUserDefaults.standardUserDefaults().stringForKey("PreviousDeviceNum")
        //labelKitID.text = "Kit ID : \(devID!.subStringFrom(4))"
    }

    func setAnimations() {
        let compDistance: CGFloat = 50, brthDistance: CGFloat = 160
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Phone {
            //compDistance = 20; brthDistance = 80
        }

        UIView.animateWithDuration(0.5, delay: 0, usingSpringWithDamping: 110, initialSpringVelocity: 3, options: [ .Repeat, .Autoreverse  ],
            animations: {
                [unowned self] in self.imgVwCenter.transform = CGAffineTransformMakeTranslation(0, compDistance)
            }) { [] (finished: Bool) in }

        UIView.animateWithDuration(1.5, delay: 0, usingSpringWithDamping: 10, initialSpringVelocity: 2, options: [ .Repeat  ],
            animations: {
                [unowned self] in self.imgVwBreath.transform = CGAffineTransformMakeTranslation(brthDistance, 0)
            }) { [] (finished: Bool) in  print("  Finished ")  }
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        setLanguageString()
        
        if isCalibrationFinished == true {
            self.dismissViewControllerAnimated(viewAnimate, completion: nil)
            isCalibrationFinished = false
            return
        }
        print("  CalibrationVC ::  view Will Appear ...   bleman.startCalibration ")
        
        uiTimer = NSTimer.scheduledTimerWithTimeInterval(0.03, // second
            target:self, selector: #selector(CalibrationVC.uiUpdateAction),
            userInfo: nil, repeats: true)
        imgVwCenter.hidden = false
        imgVwBreath.hidden = true

        setAnimations()
        bleMan!.stt = .CAL_NEW_CC  //bleMan!.parsingState = CalibrationView01
        bleMan!.startCalibration() // .CAL_NEW_CC 로 바뀜..  RP 로 바뀌고..  끝나면 Sensitivity VC 열어야 함.
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        uiTimer.invalidate()
        //HsBleManager.inst().backToReady()
    }
    
    func uiUpdateAction() {
        curStepWatch.compareThis_Once((bleMan?.stt)!, willPrint: true)

        if bleMan!.isPortClosed {
            log.logUiAction(" uiUpdateAction DisConnected  ")
            showAlertPopupOfGoHome()  //navigationController?.popToRootViewControllerAnimated(true)
            uiTimer.invalidate()
        }
        
        if isBypassMode {
            log.logUiAction("  isBypassMode  ")
            self.dismissViewControllerAnimated(viewAnimate, completion: nil)
        }

        var progress:Float = 0
        if (bleMan?.stt)! == .CAL_NEW_CC {
            progress = Float(dObj!.depth) * 0.01
        } else {
            progress = Float(dObj!.amount) * 0.01
        }
        
        if imgStage1 && (bleMan?.stt)! == .CAL_NEW_RP { // 1 > 2  이미지 교체.
            imgStage12.image = UIImage(named: "cali_step2")
            labelCenterTitle.text = langStr.obj.breath_calibration // local Str("breath_calibration")
            labelNotification.text = langStr.obj.adequate5 // local Str("adequate5")
            imgStage1 = false
        }
        
        progressBar.progress = progress

        if curStepWatch.isChanged && (bleMan?.stt)! == .CAL_NEW_RP {
            imgVwCenter.hidden = true // = UIImage(named: "cali_breath")
            imgVwBreath.hidden = false
        }

        if (bleMan?.stt)! == .CAL_NEW_CC { labelCounter.text = " " + dObj!.countP.format(2) + " / 15" }
        else { labelCounter.text = " " + dObj!.countRP.format(2) + " / 05" }
        
        if (bleMan?.stt)! == .CAL_LOAD {
            log.printAlways("  캘리브레이션   압박, 호흡   실시  완료..   감도 설정 뷰 열기....  ", lnum: 20)
            gotoSensitivityView()
        }

        // 센서 체크
        let sensorInfo = bleMan?.stateMan.sensorStateCheck()
        if sensorInfo != nil {
            uiTimer.invalidate()
            showSimpleMessageWith(sensorInfo!) { () -> Void in
                self.closeWindow()
            }
        }
    }

    func gotoSensitivityView() {
        let senseVC = SensivitySettingVC()
        senseVC.setBleObject(bleMan!)
        dispatch_async(dispatch_get_main_queue()) {

            self.presentViewController(senseVC, animated: viewAnimate, completion: nil)
            //self.navigationController?.pushViewController(senseVC, animated: viewAnimate)
        }
    }

    func startTimer() {
        uiTimer = NSTimer.scheduledTimerWithTimeInterval(0.03, // second
            target:self, selector: #selector(CalibrationVC.uiUpdateAction), userInfo: nil, repeats: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func closeWindow() {
        self.dismissViewControllerAnimated(viewAnimate, completion: nil)
        //self.navigationController?.popViewControllerAnimated(viewAnimate)
    }
    
    @IBAction func bttnActBack(sender: AnyObject) {
        showGoToMainView( { () -> () in
            self.closeWindow()
        })
    }
    
    @IBAction func bttnActSkip(sender: AnyObject) {
        gotoSensitivityView()
    }

    @IBAction func bttnActDebug(sender: AnyObject) {
        
    }

    // MARK:  언어 세팅.
    func setLanguageString() {
        //view.backgroundColor = HmGraphSetting.inst.viewBgBrightGray
        bttnGotoPrev.setTitle(langStr.obj.prev_gt, forState: .Normal)
        bttnSkip.setTitle(langStr.obj.skipArrow, forState: .Normal)

        labelTitle.text = langStr.obj.option_calibration // loca lStr("option_calibration")
        labelCenterTitle.text = langStr.obj.chest_compression_calibration // ocalStr("chest_compression_calibration")
        labelNotification.text = langStr.obj.compression30  //l ocalStr("compression30")
    }

}
