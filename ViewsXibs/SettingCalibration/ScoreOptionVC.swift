//
//  ScoreOptionVC.swift
//  HS Monitor
//
//  Created by Jongwoo Moon on 2016. 8. 8..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation

class ScoreOptionVC: UIViewController {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelCompression: UILabel!
    @IBOutlet weak var labelBreath: UILabel!
    @IBOutlet weak var labelCompVal: UILabel!
    @IBOutlet weak var labelBrthVal: UILabel!

    @IBOutlet weak var labelDepth: UILabel!
    @IBOutlet weak var labelRecoil: UILabel!
    @IBOutlet weak var labelCompRate: UILabel!
    @IBOutlet weak var labelPosition: UILabel!
    @IBOutlet weak var labelCompNum: UILabel!
    @IBOutlet weak var labelAmount: UILabel!
    @IBOutlet weak var labelBrthRate: UILabel!
    @IBOutlet weak var labelBrthNum: UILabel!

    @IBOutlet weak var bttnCompMinus: UIButton!
    @IBOutlet weak var bttnCompPlus: UIButton!
    @IBOutlet weak var bttnBrthMinus: UIButton!
    @IBOutlet weak var bttnBrthPlus: UIButton!

    @IBOutlet weak var bttnPrev: UIButton!

    @IBOutlet weak var bttnSave: UIButton!
    @IBOutlet weak var bttnReset: UIButton!

    @IBOutlet var arrButtons: [UIButton]!

    /// cancel 시 버릴 값들..
    var arrTempOpts = [Bool]()   //  tempCompVal = 75
    var compCtrler = HtPlusMinus(), brthCtrler = HtPlusMinus()

    //////////////////////////////////////////////////////////////////////     [ UI   ViewDidLoad   << >> ]
    override func viewDidLoad() {
         setInitialSetting()
    }

    @IBAction func bttnActPlusMinus(sender: UIButton) {
        var curCtrl = compCtrler
        if 10 < sender.tag { curCtrl = brthCtrler } // 호흡의 태그 : 11, 12

        if sender.tag % 10 == 1 {
            curCtrl.minusAction()
        } else {
            curCtrl.plusAction()
        }
    }

    @IBAction func bttnActCheck(sender: UIButton) {
        print("  ScoreOptionVC :  bttnAct Check >>  tag : \(sender.tag)  )")
        let btnTag = sender.tag
        arrTempOpts[btnTag - 1] = !arrTempOpts[btnTag - 1] // 바꿔서.
        setCheckImgOf(sender, accordingTo: arrTempOpts[btnTag - 1])
    }

    @IBAction func bttnActGo2Prev(sender: UIButton) {
        showAlertPopupOfBackToMainInfant() { // 메인으로 가기 팝업.
            self.navigationController?.popViewControllerAnimated(viewAnimate)
        }
    }

    @IBAction func bttnActReset(sender: UIButton) {
        scoreOption = [ true, true, true, true, true, true, false, true ]
        scoreWeight = 75
        setInitialSetting()
    }

    @IBAction func bttnActFinish(sender: UIButton) {
        scoreOption = arrTempOpts
        scoreWeight = Int(compCtrler.labelVal!.text!)!
        HsBleSingle.inst().shlObj.saveScoreOptions()
        // self.navigationController?.popViewControllerAnimated(viewAnimate)
    }

    private func setInitialSetting() {
        //  하드 카피 해야하나 ???
        arrTempOpts = scoreOption

        for ix in 1...8 {
            if let cur = arrButtons.filter({ (btn) -> Bool in btn.tag == ix }).first {
                setCheckImgOf(cur, accordingTo: arrTempOpts[ix - 1])
            }
        }

        labelCompVal.text = "\(scoreWeight)"
        labelBrthVal.text = "\(100 - scoreWeight)"

        compCtrler.setUICompo(bttnCompPlus, bMinus: bttnCompMinus, lVal: labelCompVal)
        compCtrler.setOptionVal(2.0, otherLabel: labelBrthVal)
        brthCtrler.setUICompo(bttnBrthPlus, bMinus: bttnBrthMinus, lVal: labelBrthVal)
        brthCtrler.setOptionVal(2.0, otherLabel: labelCompVal)

        setLanguageString()
    }

    private func setCheckImgOf(bttn: UIButton, accordingTo: Bool) {
        if accordingTo {
            bttn.setImage(UIImage(named: "btn_check"), forState: .Normal)
        } else {
            bttn.setImage(UIImage(named: "btn_check_not"), forState: .Normal)
        }
    }

    func setLanguageString() {
        labelTitle.text = langStr.obj.bls_setting
        labelCompression.text = langStr.obj.compression
        labelBreath.text = langStr.obj.breath

        labelDepth.text = langStr.obj.depth
        labelRecoil.text = langStr.obj.recoil
        labelCompRate.text = langStr.obj.rate
        labelBrthRate.text = langStr.obj.rate
        labelPosition.text = langStr.obj.position
        labelCompNum.text = langStr.obj.count
        labelBrthNum.text = langStr.obj.count
        labelAmount.text = langStr.obj.volume

        bttnPrev.setTitle(langStr.obj.prev_gt, forState: .Normal)
        bttnSave.setTitle(langStr.obj.save, forState: .Normal)
        bttnReset.setTitle(langStr.obj.reset_settings, forState: .Normal)

        bttnSave.localizeSizeAlignment(0)
        bttnReset.localizeSizeAlignment(0)
    }
}

/// + - 조작 버튼
class HtPlusMinus : NSObject {
    enum actionOpt {
        case colorChangeAndDelayRecover
        case colorChangeByValue
    }

    var actOpt = actionOpt.colorChangeAndDelayRecover

    var timer: NSTimer?

    var bttnPlus: UIButton?, bttnMinu: UIButton?, labelVal: UILabel?, labelOtherVal: UILabel?
    var delaySec = 2.0

    //////////////////////////////////////////////////////////////////////     [ UI compo, option value setting   << >> ]
    func setUICompo(bPlus: UIButton, bMinus: UIButton, lVal: UILabel) {
        bttnPlus = bPlus; bttnMinu = bMinus; labelVal = lVal
    }

    func setOptionVal(delayTime: Double, otherLabel: UILabel?) {
        delaySec = delayTime
        labelOtherVal = otherLabel
    }

    //////////////////////////////////////////////////////////////////////     [ public   << >> ]
    func plusAction() {
        if 100 < Int(labelVal!.text!)! + 1 {
            buttonAction(bttnPlus!)
            bttnPlus?.setImage(UIImage(named: "bttnPluRed"), forState: .Normal)
            return
        }
        labelVal?.text =  "\(Int(labelVal!.text!)! + 1)"
        labelOtherVal?.text = "\(Int(labelOtherVal!.text!)! - 1)"
        bttnPlus?.setImage(UIImage(named: "bttnPluRed"), forState: .Normal)
        buttonAction(bttnPlus!)
    }

    func minusAction() {
        if Int(labelVal!.text!)! - 1 < 0 {
            buttonAction(bttnMinu!)
            bttnMinu?.setImage(UIImage(named: "bttnMinRed"), forState: .Normal)
            return
        }
        labelVal?.text =  "\(Int(labelVal!.text!)! - 1)"
        labelOtherVal?.text = "\(Int(labelOtherVal!.text!)! + 1)"
        bttnMinu?.setImage(UIImage(named: "bttnMinRed"), forState: .Normal)
        buttonAction(bttnMinu!)
    }

    private func buttonAction(sendr: UIButton) {
        switch actOpt {
        case .colorChangeAndDelayRecover:
            labelVal?.textColor = colBttnRed
            3
        //case .colorChangeByValue:

        default:
            labelVal?.textColor = colBttnRed
        }
        setTimer()
    }

    private func setTimer() {
        if timer != nil {
            timer?.invalidate()
        }
        self.timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: #selector(self.recover), userInfo: nil, repeats: false)
       // (self.delayTime, target: self, selector: #selector(UnitJob.timerAction), userInfo: nil, repeats: true)
    }

    func recover() {
        switch actOpt {
        case .colorChangeAndDelayRecover:
            labelVal?.textColor = colBttnDarkGray
            //case .colorChangeByValue:

        default:
            labelVal?.textColor = colBttnRed
        }
        labelVal?.textColor = colBttnDarkGray
        bttnPlus?.setImage(UIImage(named: "bttnPluGry"), forState: .Normal)
        bttnMinu?.setImage(UIImage(named: "bttnMinGry"), forState: .Normal)
        timer = nil
    }

}


