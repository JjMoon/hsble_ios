//
//  InitialSettingVC.m
//  heartisense
//
//  Created by Jongwoo Moon on 2015. 10. 21..
//  Copyright © 2015년 gyuchan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "InitialSettingVC.h"
#import "Common-Swift.h"
#import "HSDataStaticValues.h"
#import "NSObject+Util.h"
//#import "HsBleManager.h"
#import "HsBleSingle.h"
//#import "SearchDeviceListView.h"
#import "HSEnumSet.h"
#import "HSDataCalculator.h"
#import "KLCPopup.h"
#import "PSPDFAlertView.h"
#import "MBProgressHUD.h"
#import "DPLocalizationManager.h"

@interface InitialSettingVC () <IGLDropDownMenuDelegate>{
    MBProgressHUD *HUD;
    NSTimer* uiTimer;
    
    //SearchDeviceListView *deviceListView;
    KLCPopup* popup;
    
    HSDataCalculator *cal;
    
    bool uIsManLeft;
    int uStageLimit, uLang, uGuide, uPrevLan, uManikinKind, uInterval, uDateFormat;

    int dropY, dropWid, dropHei, arrowX, arrowY;
    // 메뉴 위치.
    int X1, X2, X3cen, Y1, Y2, Y3;


    StrLocBase *lObj;
}

@property (nonatomic, strong) IGLDropDownMenu *dropDownLanguage;  // 언어 선택 드랍다운 메뉴...
@property (nonatomic, strong) IGLDropDownMenu *dropDownCPR;  // CPR 가이드라인  드랍다운 메뉴...
@property (nonatomic, strong) IGLDropDownMenu *dropDownManikin;  // 마네킨 종류 . 0 : default
@property (nonatomic, strong) IGLDropDownMenu *dropDownCycle;
@property (nonatomic, strong) IGLDropDownMenu *dropDownInterval;

@end


//////////////////////////////////////////////////////////////////////     [ UI update   << >> ]
@implementation InitialSettingVC
- (IBAction)btnActionApply {
    PSPDFAlertView *alertVw = [[PSPDFAlertView alloc] initWithTitle:lObj.to_main message:lObj.want_main];
    //PSPDFAlertView(title: langStr.obj.to_main, message: (langStr.obj.want_main))
    [alertVw setCancelButtonWithTitle:lObj.no block:^(NSInteger buttonIndex) {
        NSLog(@"InitialSetting >> btnActionApply");
    }];
    [alertVw setCancelButtonWithTitle:lObj.yes block:^(NSInteger buttonIndex) {
        [self applyAndGo2Home];
    }];
    [alertVw show];
}

- (void)applyAndGo2Home {
    NSUserDefaults *dObj = [NSUserDefaults standardUserDefaults];
    [dObj setBool:uIsManLeft forKey:@"ManikinDirection" ];
    [dObj setInteger:uStageLimit forKey:@"StageLimit"];
    [dObj setInteger:uLang forKey:@"Language" ];

    [HsBleSingle.inst.shlObj saveCPRProtocol:uGuide]; // guideLine = uGuide;
//    switch (HsBleSingle.inst.cprProto.theVal) {
//        case 0:            CC _STRONG_POINT = 101;
//            break;
//        default:            CC _STRONG_POINT = CC_STRONG_POINT_DEFAULT;
//            break;    }

    [HsBleSingle.inst.shlObj saveManikinKind:uManikinKind];

    // 압박 속도 세팅
    int itv;
    switch (uInterval) {
        case 0: itv = 100; break;
        case 1: itv = 110; break;
        default: itv = 120; break;
    }
    /// 여기서 직접 써버린다.
    [HsBleSingle.inst.shlObj saveAudioCountInterval:itv];
    [HsBleSingle.inst.shlObj saveDateFormat:uDateFormat];

    isManikinLeft = uIsManLeft;
    langIdx = uLang;
    HsBleSingle.inst.stageLimit = uStageLimit;
    
    NSLog(@"  세팅 적용 : isManikinLeft : %d, langIdx : %d,  Stage : %d, cprProto: %d ",
          isManikinLeft, langIdx, uStageLimit, uGuide);
    [self dismissViewControllerAnimated:viewAnimate completion:nil];
}

- (IBAction)btnActionCancel {
    //[HsBleSingle.inst.shlObj setCurrentLanguageWith:langIdx];
    //[self dismissViewControllerAnimated:viewAnimate completion:nil];

    PSPDFAlertView *alertVw = [[PSPDFAlertView alloc] initWithTitle:lObj.to_main message:lObj.want_main];
    //PSPDFAlertView(title: langStr.obj.to_main, message: (langStr.obj.want_main))
    [alertVw setCancelButtonWithTitle:lObj.no block:^(NSInteger buttonIndex) {
        NSLog(@"InitialSetting >> setCancelButtonWithTitle:lObj.no");
    }];
    [alertVw setCancelButtonWithTitle:lObj.yes block:^(NSInteger buttonIndex) {
        langIdx = uPrevLan;
        [HsBleSingle.inst.shlObj setCurrentLanguageWith:langIdx];
        [self dismissViewControllerAnimated:viewAnimate completion:nil];
    }];
    [alertVw show];
}

- (IBAction)bttnActHelp:(UIButton *)sender {
    [self openHeartisenseInfoWeb:AppIdTrMoTe]; // Monitor, Test ?
}

//////////////////////////////////////////////////////////////////////     [ UI update   << >> ]
#pragma mark - UI / UX

- (void)initialUISetting {
    [self logCallerMethod];
    uStageLimit = HsBleSingle.inst.stageLimit;

    uDateFormat = HsBleSingle.inst.dateFormat;

    uIsManLeft = isManikinLeft;
    uLang = langIdx;
    
    NSLog(@"  Initial   stage : %d, global : %d", uStageLimit, HsBleSingle.inst.stageLimit);
    
}


- (void)viewDidLoad {
    [self logCallerMethodwith:@"started" newLine:5];

    switch (HsBleSingle.inst.audioCountInterval.theVal) {
        case 110: uInterval = 1; break;
        case 120: uInterval = 2; break;
        default:uInterval = 0; break;
    }
  }

- (void)viewWillAppear:(BOOL)animated {
    [self logCallerMethod];
    uPrevLan = langIdx;

    [self initialUISetting];
    [self textLocalize];


    // Cycle 설정
    if (uStageLimit < 1) {
        uStageLimit = 3;
    }

    uiTimer = [NSTimer scheduledTimerWithTimeInterval:0.03 target:self
                                             selector:@selector(uiUpdateAction) userInfo:nil repeats:YES];

    int ySpace;
    int scrWid = [UIScreen mainScreen].bounds.size.height, widHalf = scrWid / 2;
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) {
        dropY = 230; dropWid = 260; dropHei = 45; arrowX = 210; arrowY = 16;
        ySpace = 240;
        //X1 = 384 - 260 - 30;
        //X2 = 512 - 130; X3cen = 384 - dropWid / 2;
        Y1 = 200; // 230;
    } else {
        ySpace = 180;
        dropY = 90; dropWid = 150; dropHei = 30; arrowX = 120; arrowY = 12;
        X1 = 180 - 150;
        X2 = 320 - dropWid / 2 + 10; X3cen = 180 - dropWid / 2;
        Y1 = 160;
    }
    X1 = 80; // _lblCPRGuilde.frame.origin.x + _lblCPRGuilde.frame.size.width / 2 - dropWid/2;
    X2 = 384 + 30; //_lblLanguage.frame.origin.x + _lblLanguage.frame.size.width / 2 - dropWid/2;

    NSLog(@" lanX1X2  %f  %f", (float)_lblCPRGuilde.bounds.origin.x, (float)_lblLanguage.bounds.origin.x );
    X3cen = widHalf - dropWid;
    Y2 = Y1 + ySpace + 20; Y3 = Y2 + ySpace;

//    if (AppIdTrMoTe == 1) { // Monitor
//        [self setCPRGuideLine];
//    } else {
//        _lblCPRGuilde.hidden = true;
//    }

    [self setIntervalDropDown];
    [self setCPRGuideLine];
    [self setManikinKind];
    [self setCycleNum];
    [self setDropdownLanguage];

    NSArray *arr = @[ _dropDownCPR, _dropDownCycle, _dropDownManikin, _dropDownInterval, _dropDownLanguage ];
    __weak typeof(self) ws = self;
    for (IGLDropDownMenu *drop in arr) {
        drop.selectedAction = ^{
            NSArray *arr = @[ ws.dropDownCPR, ws.dropDownCycle, ws.dropDownManikin, ws.dropDownInterval, ws.dropDownLanguage ];
            for (int k = 0; k < arr.count; k++) {
                [arr[k] foldView];
            }
        };
    }
}

//////////////////////////////////////////////////////////////////////////////////////////  드롭 다운 메뉴 추가....
- (void)setDropdownLanguage {
    NSArray *dataArray = @[ @{@"title":@"English"}, @{@"title":@"Deutsch"}, @{@"title":@"Español"},
                            @{@"title":@"Français"}, @{@"title":@"한국어"} ];
    NSMutableArray *dropdownItems = [[NSMutableArray alloc] init];
    for (int i = 0; i < dataArray.count; i++) {
        NSDictionary *dict = dataArray[i];
        IGLDropDownItem *item = [[IGLDropDownItem alloc] init];
        [item setIconImage:[UIImage imageNamed:dict[@"image"]]];
        [item setText:dict[@"title"]];
        [dropdownItems addObject:item];
    }

    //       * 언어선택 UI 설정
    self.dropDownLanguage = [[IGLDropDownMenu alloc]init];
    self.dropDownLanguage.dropDownItems = dropdownItems;
    self.dropDownLanguage.paddingLeft = 15;

    [self.dropDownLanguage setFrame:CGRectMake(X2, Y1, dropWid, dropHei)]; //260, 45)]; // 1024 / 2 = 512 - 130
    self.dropDownLanguage.delegate = self;

    //style setting
    _dropDownLanguage.type = IGLDropDownMenuTypeNormal; //IGLDropDownMenuTypeStack;
    _dropDownLanguage.flipWhenToggleView = NO;

    [_dropDownLanguage reloadView];
    [_dropDownLanguage selectItemAtIndex:uLang];

    UIImageView *dropdownArrow = [[UIImageView alloc]initWithFrame:CGRectMake(arrowX, arrowY, 18.f, 15.f)];
    [dropdownArrow setImage:[UIImage imageNamed:@"btn_dropdown_arrow"]];
    [_dropDownLanguage addSubview:dropdownArrow];
    [self.view addSubview:_dropDownLanguage];
}

- (void)setCycleNum {
    NSMutableArray *dropdownItems = [[NSMutableArray alloc] init];

    for (int i = 0; i < 5; i++) {
        // NSString *cycleN = [NSString stringWithFormat:@"%d %@", i + 1, lObj.cycle];  cycle 1
        NSString *cycleN = [NSString stringWithFormat:@"%d", i + 1];
        NSDictionary *dict = @{@"title":cycleN};
        IGLDropDownItem *item = [[IGLDropDownItem alloc] init];
        [item setIconImage:[UIImage imageNamed:dict[@"image"]]];
        [item setText:dict[@"title"]];
        [dropdownItems addObject:item];
    }

    if (!_dropDownCycle) {
        _dropDownCycle = [[IGLDropDownMenu alloc]init];
        [self.view addSubview:_dropDownCycle];
    }

    _dropDownCycle.dropDownItems = dropdownItems;
    _dropDownCycle.paddingLeft = 15;

    [_dropDownCycle setFrame:CGRectMake(X2, Y2, dropWid, dropHei)]; // 250, 45)]; // 1024 / 2 = 512 - 130

    _dropDownCycle.delegate = self;

    //style setting
    _dropDownCycle.type = IGLDropDownMenuTypeNormal;
    _dropDownCycle.flipWhenToggleView = NO;
    [_dropDownCycle reloadView];

    UIImageView *dropdownArrow = [[UIImageView alloc]initWithFrame:CGRectMake(arrowX, arrowY, 18.f, 15.f)];
    [dropdownArrow setImage:[UIImage imageNamed:@"btn_dropdown_arrow"]];
    [_dropDownCycle addSubview:dropdownArrow];
    [_dropDownCycle selectItemAtIndex:uStageLimit -1];
}

- (void)setIntervalDropDown {
    NSMutableArray *dropdownItems = [[NSMutableArray alloc] init];
    NSArray *dataArray = @[@{@"title": [NSString stringWithFormat:@"100 / %@", lObj.minute] },
                           @{@"title":[NSString stringWithFormat:@"110 / %@", lObj.minute]},
                           @{@"title":[NSString stringWithFormat:@"120 / %@", lObj.minute]}];

    if (AppIdTrMoTe == 2) {
        dataArray = @[@{@"title":@"yyyy/mm/dd" }, @{@"title":@"dd/mm/yyyy"}, @{@"title":@"mm/dd/yyyy"}];
    }

    for (int i = 0; i < dataArray.count; i++) {
        NSDictionary *dict = dataArray[i];

        IGLDropDownItem *item = [[IGLDropDownItem alloc] init];
        //[item setIconImage:[UIImage imageNamed:dict[@"image"]]];
        [item setText:dict[@"title"]];
        [dropdownItems addObject:item];
    }

    if (!_dropDownInterval) {
        _dropDownInterval = [[IGLDropDownMenu alloc]init];
        [self.view addSubview:_dropDownInterval];
    }

    _dropDownInterval.dropDownItems = dropdownItems;
    _dropDownInterval.paddingLeft = 15;

    [_dropDownInterval setFrame:CGRectMake(X3cen, Y3, dropWid, dropHei)]; // 250, 45)]; // 1024 / 2 = 512 - 130

    _dropDownInterval.delegate = self;

    //style setting
    _dropDownInterval.type = IGLDropDownMenuTypeNormal;
    _dropDownInterval.flipWhenToggleView = NO;
    [_dropDownInterval reloadView];

    UIImageView *dropdownArrow = [[UIImageView alloc]initWithFrame:CGRectMake(arrowX, arrowY, 18.f, 15.f)];
    [dropdownArrow setImage:[UIImage imageNamed:@"btn_dropdown_arrow"]];
    [_dropDownInterval addSubview:dropdownArrow];

    if (AppIdTrMoTe == 2) {
        [_dropDownInterval selectItemAtIndex:uDateFormat];
    } else {
        [_dropDownInterval selectItemAtIndex:uInterval];
    }
}

- (void)setManikinKind {
    NSMutableArray *dropdownItems = [[NSMutableArray alloc] init];
    NSArray *dataArray = @[@{@"title":@"Default(Anne, etc)" }, @{@"title":@"Prestan"}];

    for (int i = 0; i < dataArray.count; i++) {
        NSDictionary *dict = dataArray[i];

        IGLDropDownItem *item = [[IGLDropDownItem alloc] init];
        [item setIconImage:[UIImage imageNamed:dict[@"image"]]];
        [item setText:dict[@"title"]];
        [dropdownItems addObject:item];
    }

    _dropDownManikin = [[IGLDropDownMenu alloc]init];
    _dropDownManikin.dropDownItems = dropdownItems;
    _dropDownManikin.paddingLeft = 15;
    [_dropDownManikin setFrame:CGRectMake(X1 - 10, Y2, dropWid + 20, dropHei)]; // 768 / 2 = 386
    _dropDownManikin.delegate = self;

    //style setting
    _dropDownManikin.type = IGLDropDownMenuTypeNormal;
    _dropDownManikin.flipWhenToggleView = NO;
    [_dropDownManikin reloadView];

    UIImageView *dropdownArrow = [[UIImageView alloc]initWithFrame:CGRectMake(arrowX + 25, arrowY, 18.f, 15.f)];
    [dropdownArrow setImage:[UIImage imageNamed:@"btn_dropdown_arrow"]];
    [_dropDownManikin addSubview:dropdownArrow];
    [self.view addSubview:_dropDownManikin];
    [_dropDownManikin selectItemAtIndex:HsBleSingle.inst.manikinKind.theVal];
}

- (void)setCPRGuideLine {
    NSArray *dataArray = @[@{@"title":@"AHA/KACPR" }, @{@"title":@"ERC"}, @{@"title":@"ANZCOR"}];

    //if (AppIdTrMoTe == 2) // test   dataArray = @[@{@"title":@"AHA/KACPR" }];

    NSMutableArray *dropdownItems = [[NSMutableArray alloc] init];
    for (int i = 0; i < dataArray.count; i++) {
        NSDictionary *dict = dataArray[i];
        IGLDropDownItem *item = [[IGLDropDownItem alloc] init];
        //[item setIconImage:[UIImage imageNamed:dict[@"image"]]];
        [item setText:dict[@"title"]];
        [dropdownItems addObject:item];
    }

    _dropDownCPR = [[IGLDropDownMenu alloc]init];
    _dropDownCPR.dropDownItems = dropdownItems;
    _dropDownCPR.paddingLeft = 15;

    [_dropDownCPR setFrame:CGRectMake(X1, Y1, dropWid, dropHei)]; // 768 / 2 = 384 - 150(Width) - 260/2
    _dropDownCPR.delegate = self;

    //style setting
    _dropDownCPR.type = IGLDropDownMenuTypeNormal;
    _dropDownCPR.flipWhenToggleView = NO;
    [_dropDownCPR reloadView];

    UIImageView *dropdownArrow = [[UIImageView alloc]initWithFrame:CGRectMake(arrowX, arrowY, 18.f, 15.f)];
    [dropdownArrow setImage:[UIImage imageNamed:@"btn_dropdown_arrow"]];
    [_dropDownCPR addSubview:dropdownArrow];

    [self.view addSubview:_dropDownCPR];
    [_dropDownCPR selectItemAtIndex:HsBleSingle.inst.cprProto.theVal];
}

- (void)viewWillDisappear:(BOOL)animated {
}



- (void)uiUpdateAction {
    if (isBypassMode && AppIdTrMoTe == 0) { // 접속이 끊어지면...
        NSLog(@"\n\n\n\n\n  DisConnected   \n\n\n\n\n");
        langIdx = uPrevLan;
        [self dismissViewControllerAnimated:true completion:nil];  //  [self.navigationController popToRootViewControllerAnimated:true];
    }
}

#pragma mark - IGLDropDownMenuDelegate
- (void)dropDownMenu:(IGLDropDownMenu *)dropDownMenu selectedItemAtIndex:(NSInteger)index
{
    NSLog(@"   dropDownMenu   selectedItemAtIndex   %ld %@ ", (long)index, dropDownMenu);
    if (dropDownMenu == _dropDownManikin) {
        NSLog(@"  dropDownMenu == _dropDown Manikin  %ld", (long)index);
        uManikinKind = (int)index;
    } else if (dropDownMenu == _dropDownCPR) {
        NSLog(@"  dropDownMenu == _dropDownCPR  ");
        uGuide = (int)index;
    } else if (dropDownMenu == _dropDownCycle) {
        NSLog(@"  dropDownMenu == _dropDownCycle  ");
        uStageLimit = (int)index + 1;
    } else if (dropDownMenu == _dropDownInterval) {
        NSLog(@"  dropDownMenu == _dropDownInterval  ");
        if (AppIdTrMoTe == 2) uDateFormat = (int)index; // 테스트 예외 처리.
        else            uInterval = (int)index;
    } else if (dropDownMenu == _dropDownLanguage) {
        NSLog(@"  dropDownMenu == _dropDownLanguage  ");
        uLang = (int)index;
        //[self setLanguage];  이건 이제 안해도 됨..
        [self textLocalize];
        [self setCycleNum];
        [self setIntervalDropDown];
    }
}

- (void)textLocalize {
    [HsBleSingle.inst.shlObj setCurrentLanguageWith:uLang];
    lObj = HsBleSingle.inst.langObj;

    _lblLanguage.text =     lObj.select_language;
    _lblCycle.text =        lObj.select_set_num;
    _lblCPRGuilde.text =    lObj.select_cpr_standard;
    _lblManekin.text =      lObj.select_manikin_type; // 마네킨 종류

    if (AppIdTrMoTe == 2) {
        _lblInterval.text =     lObj.select_date_format;
    } else {
        _lblInterval.text =     lObj.select_comp_rate_guide;
    }

    //[_btnApply setTitle:lObj.apply forState:UIControlStateNormal];
    [_btnApply setTitle:lObj.save forState:UIControlStateNormal];
    [_btnCancel setTitle:lObj.cancel forState:UIControlStateNormal];
}


@end
