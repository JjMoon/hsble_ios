//
//  InitialSettingVC.h
//  heartisense
//
//  Created by Jongwoo Moon on 2015. 10. 21..
//  Copyright © 2015년 gyuchan. All rights reserved.
//

#ifndef InitialSettingVC_h
#define InitialSettingVC_h


#endif /* InitialSettingVC_h */

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>

#import "IGLDropDownMenu.h"



/**
 디바이스 검색 결과를 선택했을 때 액션을 받기 위한 SearchDeviceListViewDelegate Protocol 추가
 Progress HUD 로딩을 보여주기 위한 MBProgressHUDDelegate Protocol 추가
 */
@interface InitialSettingVCTrn : UIViewController


{
    
}


@property (nonatomic) int appKind;

//@property (weak, nonatomic) IBOutlet UILabel *lblDirectionOfMann;


//@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *arrLabelCycle;

@property (weak, nonatomic) IBOutlet UILabel *lblGuildline;
@property (weak, nonatomic) IBOutlet UILabel *lblLanguage;
@property (weak, nonatomic) IBOutlet UILabel *lblManekin;
@property (weak, nonatomic) IBOutlet UILabel *lblCycle;
@property (weak, nonatomic) IBOutlet UILabel *lblInterval;

@property (weak, nonatomic) IBOutlet UILabel *lblManikinDir; // Trainer Only..
@property (weak, nonatomic) IBOutlet UIButton *btnManLeft;
@property (weak, nonatomic) IBOutlet UIButton *btnManRigt;


@property (weak, nonatomic) IBOutlet UIButton *btnApply;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;


@end