//
//  SensivitySettingVC.swift
//  heartisense
//
//  Created by Jongwoo Moon on 2015. 10. 11..
//  Copyright © 2015년 gyuchan. All rights reserved.
//

import UIKit

class SensivitySettingVC: UIViewController {

    var log = HtLog(cName: "SensivitySettingVC")
    var uiTimer = NSTimer()
    var curStepWatch = MuState()

    var isEditMode = false

    var bleMan: HsBleCtrl? //HsBleManager?
    var myState: CurStep { get { return (bleMan?.stt)! } set { bleMan?.stt = newValue } }
    var dObj: HsData? //HsBleManager.inst().dataObj

    //var ccLimitPointTemp:Float = Float(CC_END_POINT - CC_START_POINT), rpLimitPointTemp:Float = Float(RP_END_POINT - RP_START_POINT)
    var ccLimitPointTemp:Float = 0 //Float(dObj!.ccEndPoint - dObj!.ccStaPoint)
    var rpLimitPointTemp:Float = 0 //Float(dObj!.rpEndPoint - dObj!.rpStaPoint)

    var compValue = 0, brthValue = 0 // 0 ~ 100

    // 20 - 1620,  50 - 1050
    // 나올 때 cc end point 에 저장.
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelCompString: UILabel!
    @IBOutlet weak var labelBreathString: UILabel!
    @IBOutlet weak var labelComp: UILabel!
    @IBOutlet weak var labelBrth: UILabel!
    @IBOutlet weak var sliderComp: UISlider!
    @IBOutlet weak var sliderBrth: UISlider!
    @IBOutlet weak var progressComp: UIProgressView!
    @IBOutlet weak var progressBrth: UIProgressView!

    @IBOutlet weak var bttnRepeat: UIButton!
    @IBOutlet weak var bttnFinish: UIButton!
    @IBOutlet weak var bttnEditImg: UIButton!
    
    @IBOutlet weak var lblKitID: UILabel!

    /// AHA 빨간 바 없애기..
    @IBOutlet weak var consCompRedWidthRatio: NSLayoutConstraint!
    @IBOutlet weak var vwCompBarContainer: UIView!

    var vwCompPlmn: PlusMinusView?
    var vwBrthPlmn: PlusMinusView?

    @IBOutlet weak var lblDirectExplain: UILabel!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        //bleMan!.weakValueUI = { (Void) -> (Void) in        }
        ccLimitPointTemp = Float(dObj!.ccEndPoint - dObj!.ccStaPoint)
        rpLimitPointTemp = Float(dObj!.rpEndPoint - dObj!.rpStaPoint)

        let sliderCompValue:Float = Float(dObj!.ccLimitPointTemp - 20) / 3200.0
        let sliderBrthValue:Float = Float(dObj!.rpLimitPointTemp - 50) / 1000.0

        if cprProtoN.theVal == 0 {
            consCompRedWidthRatio = consCompRedWidthRatio.setMultiplier(0.0001)
            vwCompBarContainer.setNeedsUpdateConstraints()

        }

        print("  values >>>>  \(ccLimitPointTemp)  \(rpLimitPointTemp)   ||  \(dObj!.ccLimitPointTemp)  \(dObj!.rpLimitPointTemp) ")
        print("  초기값 : comp/brth : \(sliderCompValue)   \(sliderBrthValue)")
//        print ("\n\n\n    SensivitySettingVC  :: initial Set Values ...   CC_END_POINT : \(CC_END_POINT) .. CC_LIMIT_POINT_NEW : \(CC_LIMIT_POINT_NEW)  ")
//        print ("\n\n\n    SensivitySettingVC  :: initial Set Values ...   RP_END_POINT : \(RP_END_POINT) .. RP_LIMIT_POINT_NEW : \(RP_LIMIT_POINT_NEW)  ")
        
        sliderComp.setValue(sliderCompValue, animated: true)
        sliderBrth.setValue(sliderBrthValue, animated: true)
        
        labelComp.text = String(Int(sliderCompValue * 100))
        labelBrth.text = String(Int(sliderBrthValue * 100))


        if let nm = bleMan?.thePort?.name { // Kit ID 세팅..
            print("  port.. name : \(nm)")
            lblKitID.text = "Kit ID : \(nm.subStringFrom(4))"
        } else {
            lblKitID.text = ""
        }

        //lblKitID.text = "Kit ID : " + NSUserDefaults.standardUserDefaults().stringForKey("PreviousDeviceNum")!.subStringFrom(4)
    }

    func addPlusMinusControl() {
        var dist: CGFloat = 70 // progress 바로부터의 수직 거리.
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "PlusMinusView", bundle: bundle)

        var wid: CGFloat = 320, hei: CGFloat = 70
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Phone {
            wid = 200; hei = 40; dist = 30
        }
        vwCompPlmn = nib.instantiateWithOwner(self, options: nil).last as? PlusMinusView
        vwCompPlmn!.frame.size = CGSize(width: wid, height: hei)
        vwBrthPlmn = nib.instantiateWithOwner(self, options: nil).last as? PlusMinusView
        vwBrthPlmn!.frame.size = CGSize(width: wid, height: hei)

        self.view.addSubview(vwCompPlmn!)
        vwCompPlmn!.setConstraint()
        vwCompPlmn?.center = CGPointMake(view.bounds.midX, progressComp.frame.midY + dist)
        self.view.addSubview(vwBrthPlmn!)
        vwBrthPlmn!.setConstraint()
        vwBrthPlmn!.center = CGPointMake(view.bounds.midX, progressBrth.frame.midY + dist)
        setModeState()

        // 새로 바뀐 UI  +  -  버튼 ..
        let sliderCompValue:Float = Float(dObj!.ccLimitPointTemp - 20) / 3200.0
        let sliderBrthValue:Float = Float(dObj!.rpLimitPointTemp - 50) / 1000.0
        vwCompPlmn!.initSet(1, iniVal: sliderCompValue)
        vwBrthPlmn!.initSet(1, iniVal: sliderBrthValue)

        vwCompPlmn?.valueChanged = { val in self.compValueChanged(val) }
        vwBrthPlmn?.valueChanged = { val in self.brthValueChanged(val) }

        vwCompPlmn?.actionReverse = true
        vwBrthPlmn?.actionReverse = true
    }

    func setBleObject(bleObj: HsBleCtrl) {
        bleMan = bleObj
        dObj = bleMan!.dataObj
    }
    
    func updateProgressBar() {
        // print(" updateProgressBar >>  Depth : \(dObj!.depth)  Amount : \(dObj!.amount)")
        progressComp.progress = Float(dObj!.depth) * 0.01
        progressBrth.progress = Float(dObj!.amount) * 0.01
    }

    var compValF = 0.1, brthValF = 0.1

    //  +  -  클로져로 받는 부분..
    func compValueChanged(compVal: Float) {
        compValF = Double(compVal)
        ccLimitPointTemp = compVal * 3200 + 20
        dObj!.ccLimitPointTemp = Int(ccLimitPointTemp) // CC_LIMIT_POINT_TEMP = Int32(ccLimitPointTemp) // + CC_START_POINT
        //labelComp.text = String(Int(sliderComp.value * 100))
        print("슬라이드 변경  Comp  :  compValue : \(compVal)  ccLimitPoTemp : \(ccLimitPointTemp)")
    }

    func brthValueChanged(brVal: Float) {
        brthValF = Double(brVal)
        rpLimitPointTemp = brVal * 1000 + 50
        dObj!.rpLimitPointTemp = Int(rpLimitPointTemp)  // RP_LIMIT_POINT_TEMP = Int32(rpLimitPointTemp) // + RP_START_POINT
        //labelBrth.text = String(Int(sliderBrth.value * 100))
        print("슬라이드 변경  Breath  : breathValue : \(brVal)  rpLimitPoTemp : \(rpLimitPointTemp)")
    }
    
    @IBAction func sliderCompChanged(sender: AnyObject) {
        //ccLimitPointTemp = sliderComp.value * 1600 + 20
        ccLimitPointTemp = sliderComp.value * 3200 + 20
        dObj!.ccLimitPointTemp = Int(ccLimitPointTemp) // CC_LIMIT_POINT_TEMP = Int32(ccLimitPointTemp) // + CC_START_POINT
        labelComp.text = String(Int(sliderComp.value * 100))
        print("슬라이드 변경  Comp  :  \(sliderComp.value)  ccLimitPoTemp : \(ccLimitPointTemp)  \n")
    }
    
    @IBAction func sliderBrthChanged(sender: AnyObject) {
        rpLimitPointTemp = sliderBrth.value * 1000 + 50
        dObj!.rpLimitPointTemp = Int(rpLimitPointTemp)  // RP_LIMIT_POINT_TEMP = Int32(rpLimitPointTemp) // + RP_START_POINT
        labelBrth.text = String(Int(sliderBrth.value * 100))
        print("슬라이드 변경  Breath  :  \(sliderBrth.value)  rpLimitPoTemp : \(rpLimitPointTemp)  \n")
    }
    
    var operate:Bool = false
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

        setLanguageString()

        //bleMan!.parsingState = CalibrationView01
        myState = .CAL_LOAD

        startTimer()
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)

        if vwCompPlmn == nil { addPlusMinusControl() }
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        uiTimer.invalidate()
        bleMan!.operationStop()
        //bleMan!.parsingState = StandBy
        
        log.printAlways("  viewWillDisappear :: Set Ready ", lnum: 10)
        myState = .S_READY
        
        if !isBypassMode {
            print ("  saveToKitWith ....  @ SensitivitySettingVC  ")
            
            //  키트에 쓰기 .....   요건 나중에  ....   뒤로 갈때만....
            // 요기가 아니라 ...bleMan!.saveToKitWithComp(Int32(sliderComp.value * 100), andRspr: Int32(sliderBrth.value * 100))
        }
    }

    func startTimer() {
        uiTimer = NSTimer.scheduledTimerWithTimeInterval(0.03, // second
            target:self, selector: #selector(SensivitySettingVC.uiUpdateAction), userInfo: nil, repeats: true)
    }
    
    func uiUpdateAction() {
        curStepWatch.compareThis_Once(myState, willPrint: true)

        if bleMan!.isPortClosed {
            log.logUiAction(" 198  DisConnected  ")
            showAlertPopupOfGoHome()  //navigationController?.popToRootViewControllerAnimated(true)
            uiTimer.invalidate()
        }
        
        if bleMan!.isPortClosed || isBypassMode {
            log.logUiAction("  isBypassMode  ")
            self.dismissViewControllerAnimated(viewAnimate, completion: nil)
        }

        // 센서 체크
        // compression_pad = "Compression Pad"; breath_module = "Breath Module"; sensor_not_connected = " is not connected.";
        // isPiezoDisconnected { return "sensor_piezo" } isAccelDisconnected { return "compression_pad" }
        let sensorInfo = bleMan?.stateMan.sensorStateCheck()
        if sensorInfo != nil { //"sensor_piezo" {
            uiTimer.invalidate()
            //let mssg = langStr.obj.breath_module + langStr.obj.sensor_not_connected
            showSimpleMessageWith(sensorInfo!) { () -> Void in
                self.startTimer()
            }
        }
//        if sensorInfo == "compression_pad" {
//            uiTimer.invalidate()
//            let mssg = langStr.obj.compression_pad + langStr.obj.sensor_not_connected
//            showSimpleMessageWith(mssg) { () -> Void in
//                self.startTimer()
//            }
//        }
        updateProgressBar()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func write2kit() {
        dObj!.ccLimitPoint = dObj!.ccLimitPointTemp
        dObj!.rpLimitPoint = dObj!.rpLimitPointTemp

        print("  >> 키트에 쓰기 >>  \(dObj!.ccLimitPointTemp)   \(dObj!.rpLimitPointTemp) ")
        print("  >> saveCalibrationResultToKitWithComp  \(Int32(compValF * 100))  \(Int32(brthValF * 100))")

        isCalibrationFinished = true
        bleMan!.saveCalibrationResultToKitWithComp(Int(compValF * 100), rspr: Int(brthValF * 100))
        //(Int32(compValF * 100), andRspr: Int32(brthValF * 100), successBlock: nil)
    }

    // MARK:  다시하기, 완료 버튼 액션 처리.....    /////////////////////
    @IBAction func backToMain() {  // 완료 버튼. .....
        //  키트에 쓰기 .....

        if isEditMode {
            isEditMode = false
            setModeState()
            return
        }

        showGoToMainView { () -> () in
            self.closeMyself()
            self.write2kit()

//            let vcs = self.navigationController?.viewControllers
//            let vcNum = vcs?.count  //log.logThis("  vcNum : \(vcNum)", lnum: 2)
//            self.navigationController?.popToViewController((vcs?[vcNum! - 3])!, animated: viewAnimate)
        }
    }

    func closeMyself() {
        uiTimer.invalidate()
        self.dismissViewControllerAnimated(viewAnimate, completion: nil)
        //self.navigationController?.popViewControllerAnimated(viewAnimate)
    }


    var finishPosition = CGPointMake(0, 0)

    func setModeState() {
        vwCompPlmn!.hidden = !isEditMode
        vwBrthPlmn!.hidden = !isEditMode
        lblDirectExplain.hidden = !isEditMode

        bttnRepeat.hidden = isEditMode
        bttnEditImg.hidden = isEditMode

        if isEditMode {
            finishPosition = bttnFinish.center
            print("  position >>>  >>  center    \(bttnFinish.center)  \(view.bounds.midX)  \(bttnFinish.bounds.midY)  ")
            bttnFinish.center = CGPointMake(view.bounds.midX, bttnFinish.center.y)
            bttnFinish.translatesAutoresizingMaskIntoConstraints = true
        } else {
            bttnFinish.translatesAutoresizingMaskIntoConstraints = false
            bttnFinish.center = finishPosition
        }

        //  160730 vwCompPlmn?.hideMe()

    }

    @IBAction func bttnActEdit() { // 다시하기 ...   바로 전 화면으로 그냥 가면 된다..
        isEditMode = true
        setModeState()
        isCalibrationFinished = false // 이래야 캘립 페이지에서 그냥 나가지 않는다...
        // closeMyself()
    }

    // MARK:  언어 세팅.
    func setLanguageString() {
        labelTitle.text = langStr.obj.option_calibration
        bttnRepeat.setTitle(langStr.obj.edit, forState: .Normal)
        bttnFinish.setTitle(langStr.obj.complete_icon, forState: .Normal)
        labelCompString.text = langStr.obj.compression_depth
        labelBreathString.text = langStr.obj.breath_volume // loc alStr("respiration_amount")
        lblDirectExplain.text = langStr.obj.cali_comp_explain
    }
}
