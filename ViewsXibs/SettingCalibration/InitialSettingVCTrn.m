//
//  InitialSettingVC.m
//  heartisense
//
//  Created by Jongwoo Moon on 2015. 10. 21..
//  Copyright © 2015년 gyuchan. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "InitialSettingVCTrn.h"
#import "Common-Swift.h"
#import "HSDataStaticValues.h"
#import "NSObject+Util.h"
//#import "HsBleManager.h"
#import "HsBleSingle.h"

//#import "SearchDeviceListView.h"
#import "HSEnumSet.h"
#import "HSDataCalculator.h"

#import "KLCPopup.h"
#import "PSPDFAlertView.h"
#import "MBProgressHUD.h"
#import "DPLocalizationManager.h"


@interface InitialSettingVCTrn () <IGLDropDownMenuDelegate>{
    MBProgressHUD *HUD;
    NSTimer* uiTimer;
    
    //SearchDeviceListView *deviceListView;
    KLCPopup* popup;
    
    HSDataCalculator *cal;
    
    bool uIsManLeft;
    int uStageLimit, uLang, uPrevLan, uGuide, uManikinKind, uInterval;
    int dropWid, dropHei, arrowX, arrowY, paddingLeft;

    // 메뉴 위치.
    int X1, X2, X3, Y1, Y2;

    StrLocBase *lObj;
}

@property (nonatomic, strong) IGLDropDownMenu *dropDownLanguage;  // 언어 선택 드랍다운 메뉴...
@property (nonatomic, strong) IGLDropDownMenu *dropDownCPR;  // CPR 가이드라인  드랍다운 메뉴...
@property (nonatomic, strong) IGLDropDownMenu *dropDownManikin;  // 마네킨 종류 . 0 : default
@property (nonatomic, strong) IGLDropDownMenu *dropDownCycle;
@property (nonatomic, strong) IGLDropDownMenu *dropDownInterval;

@end

//////////////////////////////////////////////////////////////////////     _//////////_     [ InitialSettingVC ]    _//////////_
@implementation InitialSettingVCTrn
//////////////////////////////////////////////////////////////////////////////////////////
- (IBAction)btnActionApply {
    PSPDFAlertView *alertVw = [[PSPDFAlertView alloc] initWithTitle:lObj.to_main message:lObj.want_main];
    //PSPDFAlertView(title: langStr.obj.to_main, message: (langStr.obj.want_main))
    [alertVw setCancelButtonWithTitle:lObj.no block:^(NSInteger buttonIndex) {
        NSLog(@"InitialSetting >> btnActionApply");
    }];
    [alertVw setCancelButtonWithTitle:lObj.yes block:^(NSInteger buttonIndex) {
        [self applyAndGo2Home];
    }];
    [alertVw show];
}

- (void)applyAndGo2Home {
    NSUserDefaults *dObj = [NSUserDefaults standardUserDefaults];
    [dObj setBool:uIsManLeft forKey:@"ManikinDirection" ];
    [dObj setInteger:uStageLimit forKey:@"StageLimit"];
    [dObj setInteger:uLang forKey:@"Language"];
    //[dObj setInteger:uGuide forKey:@"Guideline"];

    [HsBleSingle.inst.shlObj saveCPRProtocol:uGuide]; // guideLine = uGuide;

//    switch (HsBleSingle.inst.cprProto.theVal) {
//        case 0: CC _STRONG_POINT = 101;     break;
//        default: CC _STRONG_POINT = CC_STRONG_POINT_DEFAULT; break; }

    [HsBleSingle.inst.shlObj saveManikinKind:uManikinKind];

    // 압박 속도 세팅
    int itv;
    switch (uInterval) {
        case 0: itv = 100; break;
        case 1: itv = 110; break;
        default: itv = 120; break;
    }
    [HsBleSingle.inst.shlObj saveAudioCountInterval:itv];
    isManikinLeft = uIsManLeft;
    langIdx = uLang;
    HsBleSingle.inst.stageLimit = uStageLimit;
    
    NSLog(@"  세팅 적용 : isManikinLeft : %d, langIdx : %d,  Stage : %d ", isManikinLeft, langIdx, uStageLimit);
    [self dismissViewControllerAnimated:viewAnimate completion:nil];
}

- (IBAction)btnActionCancel {
    NSLog(@" (IBAction)btnActionCancel    uLan : %d", uLang);
//    langIdx = uPrevLan;
//    [HsBleSingle.inst.shlObj setCurrentLanguageWith:langIdx];
//    [self dismissViewControllerAnimated:viewAnimate completion:nil];
    PSPDFAlertView *alertVw = [[PSPDFAlertView alloc] initWithTitle:lObj.to_main message:lObj.want_main];
    //PSPDFAlertView(title: langStr.obj.to_main, message: (langStr.obj.want_main))
    [alertVw setCancelButtonWithTitle:lObj.no block:^(NSInteger buttonIndex) {
        NSLog(@"InitialSetting >> setCancelButtonWithTitle:lObj.no");
    }];
    [alertVw setCancelButtonWithTitle:lObj.yes block:^(NSInteger buttonIndex) {
        langIdx = uPrevLan;
        [HsBleSingle.inst.shlObj setCurrentLanguageWith:langIdx];
        [self dismissViewControllerAnimated:viewAnimate completion:nil];
    }];
    [alertVw show];
}

- (IBAction)btnActManLeftTouched:(UIButton *)sender {
    uIsManLeft = true;
    [self setManikinImage];
}

- (IBAction)btnActManRightTouched:(UIButton *)sender {
    uIsManLeft = false;
    [self setManikinImage];
}

//////////////////////////////////////////////////////////////////////     _//////////_     [ UI || UX         << >> ]    _//////////_
#pragma mark - UI / UX

- (void)initialUISetting {
    [self logCallerMethod];
    uStageLimit = HsBleSingle.inst.stageLimit;
    uIsManLeft = isManikinLeft;
    uLang = langIdx;
    
    NSLog(@"\n  Initial   stage : %d, stageLimit : %d   langIdx : %d \n",
          uStageLimit, HsBleSingle.inst.stageLimit, uLang);

    [self setManikinImage]; // 트레이너만..
}


- (void)setManikinImage {
    [self logCallerMethod];
    [_btnManRigt setImage:[UIImage imageNamed:@"mannequin_right_unpress" ] forState:UIControlStateNormal];
    [_btnManLeft setImage:[UIImage imageNamed:@"mannequin_left_unpress" ] forState:UIControlStateNormal];
    if (uIsManLeft) [_btnManLeft setImage:[UIImage imageNamed:@"mannequin_left_press" ] forState:UIControlStateNormal];
    else  [_btnManRigt setImage:[UIImage imageNamed:@"mannequin_right_press" ] forState:UIControlStateNormal];
}

- (void)viewDidLoad {
    [self logCallerMethodwith:@"started" newLine:5];
    lObj = HsBleSingle.inst.langObj;

    switch (HsBleSingle.inst.audioCountInterval.theVal) {
        case 110: uInterval = 1; break;
        case 120: uInterval = 2; break;
        default:uInterval = 0; break;
    }
    CGFloat wid = [UIScreen mainScreen].bounds.size.width;
    CGFloat wid3 = wid / 3, offsetY = 20;
    dropWid =  wid3 * 0.9; // 160;
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) {
        //X1 = 212 - 130; X2 = 512 - 130; //dropWid = 250; arrowX = 210;
        paddingLeft = 15; dropHei = 45;
        Y1 = 210; Y2 = 460;
        arrowY = 16; //X3 = 512 + 180;
    } else {
        paddingLeft = 10; dropHei = 30;
        Y1 = 80;
        arrowY = 8;
        offsetY = 10; //  320 + dropWid / 2 + 60;
        //NSLog(@"  widwid >>   %f  %d  %d", wid, X1, X3);
    }
    X1 = (wid3 - dropWid) * 0.5;
    X2 = wid / 2 - dropWid / 2 ;
    X3 = 2 * wid3 + X1;
    Y1 = _lblLanguage.frame.origin.y + dropHei + offsetY; //200;
    Y2 = _lblCycle.frame.origin.y + dropHei + offsetY; //200;
    arrowX = dropWid - 30;
}

- (void)viewWillAppear:(BOOL)animated {
    [self logCallerMethod];

    NSLog(@" InitialSettingVCTrn :: viewWillAppear   langIdx : %d ", langIdx);

    uPrevLan = langIdx; // 취소를 대비해 미리 저장.
    
    [self initialUISetting];
    [self textLocalize];

    // Cycle 설정
    if (uStageLimit < 1) {
        uStageLimit = 3;
    }
    uiTimer = [NSTimer scheduledTimerWithTimeInterval:0.03 target:self
                                             selector:@selector(uiUpdateAction) userInfo:nil repeats:YES];
    [self setCPRGuideLine];
    [self setManikinKind];
    [self setCycleNum];
    [self setIntervalDropDown];
    [self setDropdownLanguage];

    NSArray *arr = @[ _dropDownCPR, _dropDownCycle, _dropDownManikin, _dropDownInterval, _dropDownLanguage ];
    __weak typeof(self) ws = self;
    for (IGLDropDownMenu *drop in arr) {
        drop.selectedAction = ^{
            NSArray *arr = @[ ws.dropDownCPR, ws.dropDownCycle, ws.dropDownManikin, ws.dropDownInterval, ws.dropDownLanguage ];
            for (int k = 0; k < arr.count; k++) {
                [arr[k] foldView];
            }
        };
    }
}

- (void)viewWillDisappear:(BOOL)animated {
}

//////////////////////////////////////////////////////////////////////////////////////////  드롭 다운 메뉴 추가....
- (void)setDropdownLanguage {
    NSArray *dataArray = @[@{@"title":@"English" }, @{@"title":@"Deutsch"},
                           @{@"title":@"Español" }, @{@"title":@"Français"}, @{@"title":@"한국어"}  ];
    NSMutableArray *dropdownItems = [[NSMutableArray alloc] init];
    for (int i = 0; i < dataArray.count; i++) {
        NSDictionary *dict = dataArray[i];
        IGLDropDownItem *item = [[IGLDropDownItem alloc] init];
        [item setIconImage:[UIImage imageNamed:dict[@"image"]]];
        [item setText:dict[@"title"]];
        [dropdownItems addObject:item];
    }
    //       * 언어선택 UI 설정
    self.dropDownLanguage = [[IGLDropDownMenu alloc]init];
    self.dropDownLanguage.dropDownItems = dropdownItems;
    self.dropDownLanguage.paddingLeft = paddingLeft;

    [self.dropDownLanguage setFrame:CGRectMake(X2, Y1, dropWid, dropHei)];
    self.dropDownLanguage.delegate = self;

    //style setting
    _dropDownLanguage.type = IGLDropDownMenuTypeNormal;
    _dropDownLanguage.flipWhenToggleView = NO;

    [_dropDownLanguage reloadView];
    [_dropDownLanguage selectItemAtIndex:uLang];

    UIImageView *dropdownArrow = [[UIImageView alloc]initWithFrame:CGRectMake(arrowX, arrowY, 18.f, 15.f)];
    [dropdownArrow setImage:[UIImage imageNamed:@"btn_dropdown_arrow"]];
    [_dropDownLanguage addSubview:dropdownArrow];
    [self.view addSubview:_dropDownLanguage];
}

- (void)setCycleNum {
    NSMutableArray *dropdownItems = [[NSMutableArray alloc] init];

    for (int i = 0; i < 5; i++) {
        NSString *cycleN = [NSString stringWithFormat:@"%d %@", i + 1, lObj.cycle];
        NSDictionary *dict = @{@"title":cycleN};
        IGLDropDownItem *item = [[IGLDropDownItem alloc] init];
        [item setIconImage:[UIImage imageNamed:dict[@"image"]]];
        [item setText:dict[@"title"]];
        [dropdownItems addObject:item];
    }
    if (!_dropDownCycle) {
        _dropDownCycle = [[IGLDropDownMenu alloc]init];
        [self.view addSubview:_dropDownCycle];
    } //_dropDownCycle = [[IGLDropDownMenu alloc]init];
    _dropDownCycle.dropDownItems = dropdownItems;
    _dropDownCycle.paddingLeft = paddingLeft;
    [_dropDownCycle setFrame:CGRectMake(X2, Y2, dropWid, dropHei)]; // 250, 45)]; // 1024 / 2 = 512 - 130

    _dropDownCycle.delegate = self;

    //style setting
    _dropDownCycle.type = IGLDropDownMenuTypeNormal;
    _dropDownCycle.flipWhenToggleView = NO;
    [_dropDownCycle reloadView];

    UIImageView *dropdownArrow = [[UIImageView alloc]initWithFrame:CGRectMake(arrowX, arrowY, 18.f, 15.f)];
    [dropdownArrow setImage:[UIImage imageNamed:@"btn_dropdown_arrow"]];
    [_dropDownCycle selectItemAtIndex:uStageLimit - 1];
    [_dropDownCycle addSubview:dropdownArrow];
}

- (void)setIntervalDropDown {
    NSMutableArray *dropdownItems = [[NSMutableArray alloc] init];
    // @"100 / min"
    NSArray *dataArray = @[@{@"title": [NSString stringWithFormat:@"100 / %@", lObj.minute] },
                           @{@"title":[NSString stringWithFormat:@"110 / %@", lObj.minute]},
                           @{@"title":[NSString stringWithFormat:@"120 / %@", lObj.minute]}];
    for (int i = 0; i < dataArray.count; i++) {
        NSDictionary *dict = dataArray[i];
        IGLDropDownItem *item = [[IGLDropDownItem alloc] init];
        [item setIconImage:[UIImage imageNamed:dict[@"image"]]];
        [item setText:dict[@"title"]];
        [dropdownItems addObject:item];
    }

    if (!_dropDownInterval) {
        _dropDownInterval = [[IGLDropDownMenu alloc]init];
        [self.view addSubview:_dropDownInterval];
    }

    _dropDownInterval.dropDownItems = dropdownItems;
    _dropDownInterval.paddingLeft = paddingLeft;
    [_dropDownInterval setFrame:CGRectMake(X3, Y2, dropWid, dropHei)]; // 250, 45)]; // 1024 / 2 = 512 - 130
    _dropDownInterval.delegate = self;
    //style setting
    _dropDownInterval.type = IGLDropDownMenuTypeNormal;
    _dropDownInterval.flipWhenToggleView = NO;
    [_dropDownInterval reloadView];

    UIImageView *dropdownArrow = [[UIImageView alloc]initWithFrame:CGRectMake(arrowX, arrowY, 18.f, 15.f)];
    [dropdownArrow setImage:[UIImage imageNamed:@"btn_dropdown_arrow"]];
    [_dropDownInterval addSubview:dropdownArrow];
    [_dropDownInterval selectItemAtIndex:uInterval];
}

- (void)setManikinKind {
    NSMutableArray *dropdownItems = [[NSMutableArray alloc] init];
    NSArray *dataArray = @[@{@"title":@"Default(Anne, etc)" }, @{@"title":@"Prestan"}];

    for (int i = 0; i < dataArray.count; i++) {
        NSDictionary *dict = dataArray[i];

        IGLDropDownItem *item = [[IGLDropDownItem alloc] init];
        [item setIconImage:[UIImage imageNamed:dict[@"image"]]];
        [item setText:dict[@"title"]];
        [dropdownItems addObject:item];
    }

    _dropDownManikin = [[IGLDropDownMenu alloc]init];
    _dropDownManikin.dropDownItems = dropdownItems;
    _dropDownManikin.paddingLeft = paddingLeft;

    [_dropDownManikin setFrame:CGRectMake(X3, Y1, dropWid, dropHei)]; // 250, 45)]; // 1024 / 2 = 512 - 130

    _dropDownManikin.delegate = self;

    //style setting
    _dropDownManikin.type = IGLDropDownMenuTypeNormal;
    _dropDownManikin.flipWhenToggleView = NO;
    [_dropDownManikin reloadView];

    UIImageView *dropdownArrow = [[UIImageView alloc]initWithFrame:CGRectMake(arrowX, arrowY, 18.f, 15.f)];
    [dropdownArrow setImage:[UIImage imageNamed:@"btn_dropdown_arrow"]];
    [_dropDownManikin addSubview:dropdownArrow];
    [self.view addSubview:_dropDownManikin];
    [_dropDownManikin selectItemAtIndex:HsBleSingle.inst.manikinKind.theVal];
}

- (void)setCPRGuideLine {
    NSArray *dataArray = @[@{@"image":@"", @"title":@"AHA/KACPR" },
                           @{@"image":@"",@"title":@"ERC"},
                           @{@"image":@"",@"title":@"ANZCOR"},
                           ];
    NSMutableArray *dropdownItems = [[NSMutableArray alloc] init];
    for (int i = 0; i < dataArray.count; i++) {
        NSDictionary *dict = dataArray[i];
        IGLDropDownItem *item = [[IGLDropDownItem alloc] init];
        //[item setIconImage:[UIImage imageNamed:dict[@"image"]]];
        [item setText:dict[@"title"]];
        [dropdownItems addObject:item];
    }

    _dropDownCPR = [[IGLDropDownMenu alloc]init];
    _dropDownCPR.dropDownItems = dropdownItems;
    _dropDownCPR.paddingLeft = paddingLeft;

    [_dropDownCPR setFrame:CGRectMake(X1, Y1, dropWid, dropHei)]; // 260, 45)]; // 1024 / 2 = 384 - 150(Width) - 260/2
    _dropDownCPR.delegate = self;

    //style setting
    _dropDownCPR.type = IGLDropDownMenuTypeNormal;
    _dropDownCPR.flipWhenToggleView = NO;
    [_dropDownCPR reloadView];

    UIImageView *dropdownArrow = [[UIImageView alloc]initWithFrame:CGRectMake(arrowX, arrowY, 18.f, 15.f)];
    [dropdownArrow setImage:[UIImage imageNamed:@"btn_dropdown_arrow"]];
    [_dropDownCPR addSubview:dropdownArrow];

    [self.view addSubview:_dropDownCPR];
    [_dropDownCPR selectItemAtIndex:HsBleSingle.inst.cprProto.theVal];
}

- (void)uiUpdateAction {
    if (isBypassMode) { // 접속이 끊어지면...
        NSLog(@"\n\n\n\n\n   Inittial Setting VCTtrn  DisConnected   \n\n\n\n\n");
        langIdx = uPrevLan;
        [self dismissViewControllerAnimated:true completion:nil];  //  [self.navigationController popToRootViewControllerAnimated:true];
    }
}

#pragma mark - IGLDropDownMenuDelegate
- (void)setDropDownCPR:(IGLDropDownMenu *)dropDownCPR {
    NSLog(@"   (void)setDropDownCPR:(IGLDropDownMenu *)dropDownCPR   ");
}

- (void)dropDownMenu:(IGLDropDownMenu *)dropDownMenu selectedItemAtIndex:(NSInteger)index
{
    NSLog(@"   dropDownMenu   selectedItemAtIndex   %ld %@ ", index, dropDownMenu);
    if (dropDownMenu == _dropDownManikin) {
        NSLog(@"  dropDownMenu == _dropDown Manikin  %ld", index);
        uManikinKind = (int)index;
    } else if (dropDownMenu == _dropDownCPR) {
        NSLog(@"  dropDownMenu == _dropDownCPR  ");
        uGuide = (int)index;
    } else if (dropDownMenu == _dropDownCycle) {
        NSLog(@"  dropDownMenu == _dropDownCycle  ");
        uStageLimit = (int)index + 1;
    } else if (dropDownMenu == _dropDownInterval) {
        NSLog(@"  dropDownMenu == _dropDownInterval  ");
        uInterval = (int)index;
    } else if (dropDownMenu == _dropDownLanguage) {
        NSLog(@"  dropDownMenu == _dropDownLanguage  ");
        uLang = (int)index;
        //[self setLanguage];  이건 이제 안해도 됨..
        [self textLocalize];
        [self setCycleNum];
        [self setIntervalDropDown];
        //[self.view bringSubviewToFront:_dropDownLanguage];
    }
}

- (void)textLocalize {
    NSLog(@"(void)textLocalize >Shl<  langIdx : %d, uLan : %d ", langIdx, uLang);
    [HsBleSingle.inst.shlObj setCurrentLanguageWith:uLang];
    lObj = HsBleSingle.inst.langObj;

    _lblLanguage.text =     lObj.select_language;
    _lblCycle.text =        lObj.select_set_num;
    _lblGuildline.text =    lObj.select_cpr_standard;
    _lblManekin.text =      lObj.select_manikin_type;

    //NSLog(@"  %@  %@", [NSString autolocalizingStringWithLocalizationKey:@"apply"], [NSString autolocalizingStringWithLocalizationKey:@"cancel"] );
    [_btnApply setTitle:lObj.apply forState:UIControlStateNormal];
    [_btnCancel setTitle:lObj.cancel forState:UIControlStateNormal];

    if (0 < _appKind) return;

    _lblManikinDir.text =   lObj.select_manikin_dir;
}

@end
