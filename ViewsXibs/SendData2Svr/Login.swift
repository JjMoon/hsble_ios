//
//  Login.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2015. 12. 17..
//  Copyright © 2015년 IMLab. All rights reserved.
//

import Foundation

class LoginVC: UIViewController, UIAlertViewDelegate {
    // admin@imlabworld.com   nimda
    var log = HtLog(cName: "LoginVC")
    var loginSuccessCallback: Void->Void = { }
    var cancelAction: Void->Void = { }
    var okAction:  Void->Void = { }
    
    @IBOutlet weak var labelLMSLogin: UILabel!
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    @IBAction func bttnActLogin(sender: UIButton) {
        
        if txtEmail.text?.getLength() < 5 || txtPassword.text?.getLength() < 2 { return }
        
        okAction()
        
        HsAuthJobs.singltn.authRequest(txtEmail.text!, pass: txtPassword.text!, successBlck: {
            self.dismissViewControllerAnimated(viewAnimate, completion: nil)
            self.loginSuccessCallback()
            }, failureBlck: {
                dispatch_async(dispatch_get_main_queue(), {
                    self.showAlertView()
                    })
                self.log.logUiAction("  FAIL !!! ")
        })
    }
    
    func showAlertView() {
        let altVw = UIAlertView(title: nil, message: langStr.obj.login_fail,  // local Str("msg_instructor_login_fail"),
            delegate: self, cancelButtonTitle: langStr.obj.cancel)
        altVw.show()
    }

    // MARK:  Alert View Delegate
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        alertView.dismissPresentingPopup()
        // txtEmail.text = ""        txtPassword.text = ""
        self.txtEmail.becomeFirstResponder()
    }

    @IBAction func bttnActCancel(sender: UIButton) {
        dismissViewControllerAnimated(viewAnimate, completion: nil)
        cancelAction()
    }
    
    @IBAction func bttnActSearchIDnPW(sender: UIButton) {
        UIApplication.sharedApplication().openURL(NSURL(string: "http://www.heartisense.com/LMS/index.php/IMLAB/find_account")!)
    }

    // MARK:  언어 세팅.
    func setLanguageString() {
        log.printThisFunc("setLanguageString")
        labelLMSLogin.text = "LMS " + langStr.obj.login
    }

}
