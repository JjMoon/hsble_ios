//
//  StudentInfo.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2015. 12. 18..
//  Copyright © 2015년 IMLab. All rights reserved.
//

import Foundation

class StudentInfo: HtViewController, UIAlertViewDelegate {
    // admin@imlabworld.com   nimda
    var log = HtLog(cName: "StudentInfo")
    var curStudent: HmStudent?

    var originalName = "원래이름."
    var refreshCallback: () -> () = { }

    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    
    func showAlertView() {
        let altVw = UIAlertView(title: nil, message: langStr.obj.login_fail,
            delegate: self, cancelButtonTitle: langStr.obj.cancel)
        altVw.show()
    }

    @IBAction func bttnApply(sender: UIButton) {
        let errMsg = HsBleMaestr.inst.checkValidityNameEmail(txtName.text!, em: txtEmail.text!)
        if errMsg != nil {
            showSimpleMessageWith(errMsg!)
        } else if originalName == txtName.text! || errMsg == nil {
            curStudent?.name = txtName.text!
            curStudent?.email = txtEmail.text!
            callBack()
            dismissViewControllerAnimated(viewAnimate, completion: nil)
        }
    }
    
    @IBAction func bttnActSearchIDnPW(sender: UIButton) {
        dismissViewControllerAnimated(viewAnimate, completion: nil)
    }

    func setStudent(myStudent: HmStudent) {
        curStudent = myStudent
        txtName.placeholder = langStr.obj.name
        txtEmail.placeholder = langStr.obj.email
        originalName = (curStudent?.name)!
        txtName.text = (curStudent?.name)!
        txtEmail.text = (curStudent?.email)!
    }
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
//        alertView.dismissPresentingPopup()
//        txtEmail.text = ""
//        txtPassword.text = ""
//        txtEmail.becomeFirstResponder()
    }
    
    // MARK:  언어 세팅.
    func setLanguageString() {
    }
    
}