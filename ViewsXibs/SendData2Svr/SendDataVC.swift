//
//  SendDataVC.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2015. 12. 22..
//  Copyright © 2015년 IMLab. All rights reserved.
//

import Foundation

// dream3014@gmail.com  tlsgn

class SendDataVC : UIViewController, SearchViewPrtcl {
    var log = HtLog(cName: "데이터 전송 VC")

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelStudentInfo: UILabel!
    @IBOutlet weak var theTable: UITableView!
    @IBOutlet weak var bttnCheckAll: UIButton!
    @IBOutlet weak var bttnCheckAllString: UIButton!
    @IBOutlet weak var vwProgressAnimation: HmDotProcess!

    @IBOutlet weak var bttnSend: UIButton!
    @IBOutlet weak var bttnCancel: UIButton!


    var checkSelected = false
    var svrRequest: HtServerRequest?

    /// SearchViewPrtcl
    var searchTxtField: UITextField?
    var searchMode = false
    var searchConstraintWidth: NSLayoutConstraint?
    var searchedList = [HmStudent]()

    /// Sorting...
    var sortByDate = true, sortAscend = false
    @IBOutlet weak var bttnSortDate: SortBttn!
    @IBOutlet weak var bttnSortName: SortBttn!

    @IBAction func sortTouched(sender: SortBttn) {
        sender.sortTouched()
        sortByDate = sender.accessibilityIdentifier! == "date"
        sortAscend = sender.isAscend
        sortAction()
    }

    func sortFromSearchProtocol(union: [HmStudent]) {
        searchedList = sortStudentArrayBy(sortByDate, ascn: sortAscend, union: studentList)
    }

    func sortAction() {
        studentsWithData = sortStudentArrayBy(sortByDate, ascn: sortAscend, union: studentsWithData!)
        sortFromSearchProtocol(studentList)
        reloadTableView()
    }

    @IBAction func bttnActInfo(sender: AnyObject) {
        openHeartisenseInfoWeb(1)// Monitor
    }
    
    @IBAction func bttnActCheckAll(sender: UIButton) {
        toggleCheckAll()
    }
    @IBAction func bttnActCheckAllBox(sender: AnyObject) {
        toggleCheckAll()
    }

    var studentsWithData: [HmStudent]? // 이거 반복할 필요 없다. 직접 부르지 말것 ..
    func getStudentsWithData() -> [HmStudent] {
        if studentsWithData == nil {
            studentsWithData = HsBleMaestr.inst.arrStudent.filter({ (std) -> Bool in
                return std.isDataInDB // 모니터, 테스트가 다름...
                //return std.uiTargeted && std.isDataInDB // 모니터, 테스트가 다름...

            })
        }
        return studentsWithData!
    }

    var studentList: [HmStudent] {
        get {
            if searchMode { return searchedList
            } else {        return getStudentsWithData() }
        }
    }

    //////////////////////////////////////////////////////////////////////     [   검색 관련    << >> ]

    @IBOutlet weak var constSearchWidth: NSLayoutConstraint!
    @IBOutlet weak var bttnSearch: UIButton!
    @IBOutlet weak var txtSearch: UITextField!

    @IBAction func bttnActSearch(sender: UIButton) { // 탐색 기능 토글..
        searchMode = !searchMode
        searchUIAnimation()
        if !searchMode {
            view.endEditing(true)
        }
        refreshSearchTextAction( {std in
            return std.name.containsString(self.txtSearch.text!) || std.email.containsString(self.txtSearch.text!)
            }, totalList: self.getStudentsWithData())
    }

    @IBAction func searchTextChangedAction(sender: UITextField) {
        print("searchTextChangedAction  length : \(sender.text!.getLength())   \(sender.text!.getLengthUTF32())")

        refreshSearchTextAction( {std in
            return std.name.containsString(self.txtSearch.text!) || std.email.containsString(self.txtSearch.text!)
            }, totalList: self.getStudentsWithData())
    }

    //////////////////////////////////////////////////////////////////////     [      << >> ]

    @IBAction func bttnActSend(sender: AnyObject) {  //  데이터 전송 /////  >>>>>>>>>>>>>>>
        //HsBleMaestr.inst.sendDatumToServer()
        showSendDataVC()
    }

    func sendAction() {
        let targetStudent = HsBleMaestr.inst.arrStudent.filter { (student) -> Bool in
            return student.uiTargeted && student.uiTargeted }

        if targetStudent.count == 0 {
            showSimpleMessageWith(langStr.obj.select + "  " + langStr.obj.student_name)
            return
        }

        vwProgressAnimation.showMe()

        log.printThisFNC("bttnActSend", comment: " 타겟 학생 수 : \(targetStudent.count) ")
        svrRequest = HtServerRequest(interval: 0.1, theArray: targetStudent)
        svrRequest?.liscenceErrorCallBack = {
            self.showSimpleMessageWith(langStr.obj.no_license)
        }
        svrRequest!.progress = vwProgressAnimation
        svrRequest?.sendDataFinishedCljr = {
            self.studentsWithData = nil
            self.getStudentsWithData()
            self.reloadTableView()
            self.vwProgressAnimation.hideMe()

            if self.getStudentsWithData().count == 0 {
                self.dismissViewControllerAnimated(viewAnimate, completion: nil)
            }
        }
    }


//    func send2Server() {
//        print(" ManageStudentVC  >>   send2Server")
//        let stntWithDataNum = HsBleMaestr.inst.arrStudent.filter({ (std) -> Bool in
//            return std.isDataInDB
//        }).count
//
//        if 0 == stntWithDataNum {
//            showSimpleMessageWith("No Data")
//            return
//        }
//
//        showSendDataVC()
//    }

    func showSendDataVC() {
        if 0 == HsBleMaestr.inst.numOfUiTargetedStudents() {
            showSimpleMessageWith("No Data")
            return
        }

        if 0 < instructorID {  //  4 Debugging..  if 0 == instructorID {
            log.logThis("  Has ..  Instructor ID : \(instructorID) ")
            sendAction()
        } else {
            let vc = LoginVC(nibName: "Login", bundle: nil)
            vc.modalPresentationStyle = .FormSheet // .Popover
            presentViewController(vc, animated: viewAnimate, completion: nil)
            vc.popoverPresentationController?.sourceView = view
            vc.popoverPresentationController?.sourceRect = view.frame
            vc.loginSuccessCallback = {
                self.sendAction()
            }
            vc.setLanguageString()
        }
    }

    @IBAction func bttnActCancel(sender: AnyObject) {
        HsBleMaestr.inst.uiTargetResetAllStudent()
        dismissViewControllerAnimated(viewAnimate, completion: nil)
    }

    override func viewDidLoad() {
        log.printThisFNC("viewDidLoad", comment: "  학생 with data \(getStudentsWithData())  ")
//        let noUse = HsBleMaestr.inst.arrStudent.map { (std) -> Void in
//            // std.uiTargeted = false
//            //print(" SendDataVC :: viewDidLoad >>>   student name ::  \(std.name)  \t targeted 을 false로 세팅 .. ")
//        }
//        print(" 매핑 한 전체 수 : \(noUse.count)")
        vwProgressAnimation.hideMe()
        setLanguageString()

        /// Search Protocol
        searchTxtField = txtSearch
        searchConstraintWidth = constSearchWidth
    }

    override func viewWillAppear(animated: Bool) {
        HsBleMaestr.inst.uiTargetResetAllStudent()
        txtSearch.hideMe()
        txtSearch.autocorrectionType = .No
        searchUIAnimation()
        
        /// sorting initial action
        bttnSortDate.initDateNameState(bttnSortName)
    }
    
    override func viewDidAppear(animated: Bool) {
        reloadTableView()
    }

    //////////////////////////////////////////////////////////////////////      전체 선택/해제
    // MARK:  Select All <---> None
    func toggleCheckAll() {
        log.printThisFNC("toggleCheckAll", comment: " ")

        checkSelected = studentList.count != studentList.filter({ (std) -> Bool in
            std.uiTargeted
        }).count

        for std in studentList {
            std.uiTargeted = checkSelected
        }

        reloadTableView()
        setCheckAllButtonAppearance(checkSelected)
    }

    func refreshDeleteBttnCheckAllState() {
        log.printThisFNC("refreshTable", comment: "모두 선택되었는지 감시...   ")   //  이거 이름 바꿔야 ???
        let allSelected = studentList.count == studentList.filter({ (std) -> Bool in
            std.uiTargeted
        }).count
        log.logThis("  All Selected ??  \(allSelected)", lnum: 1)
        setCheckAllButtonAppearance(allSelected)
    }

    func setCheckAllButtonAppearance(check: Bool) {
        bttnCheckAll.setImageAccordingTo(check, trueImg: "btn_check", falseImg: "btn_uncheck")
        bttnCheckAllString.setTtitleAccordingTo(check, trueTxt: langStr.obj.all_uncheck, falseTxt: langStr.obj.all_check)
        if 0 == HsBleMaestr.inst.numOfUiTargetedStudents() {
            bttnSend.deactivate()
            bttnSend.backgroundColor = colBttnGray
        } else {
            bttnSend.activate()
            bttnSend.backgroundColor = colBttnGreen
        }
    }

    // MARK:  언어 세팅.
    func setLanguageString() {
        labelTitle.text = langStr.obj.send_data
        labelStudentInfo.text = langStr.obj.student_info
        bttnCheckAllString.setTitle(langStr.obj.all_check, forState: .Normal)

        bttnSend.setTitle(langStr.obj.send_data, forState: .Normal)
        bttnCancel.setTitle(langStr.obj.cancel, forState: .Normal)
    }
}

extension SendDataVC : UITableViewDelegate, UITableViewDataSource {

    func reloadTableView() {
        if UI_USER_INTERFACE_IDIOM() == .Phone {
            //tableStudent.rowHeight = 30;
        }

        bttnSearch.setImageAccordingTo(searchMode, trueImg: "search_close.png", falseImg: "search.png")
        theTable.reloadData()
        refreshDeleteBttnCheckAllState()
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(  " 테이블 셀 수 ::  \(getStudentsWithData().count) ")
        return studentList.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        print ("SendData >> cellForRowAtIndexPath   \(indexPath)")
        var cell:SendDataCell? = tableView.dequeueReusableCellWithIdentifier("sendDataCell") as? SendDataCell
        if cell == nil {
            addTableCellNib()
            cell = tableView.dequeueReusableCellWithIdentifier("sendDataCell") as? SendDataCell
        }
        cell?.setItems(studentList[indexPath.row])
        //cell?.bringSubviewToFront(cell!.bttnCheck)
        if cell?.curStudent?.uiTargeted == true {
            theTable.selectRowAtIndexPath(indexPath, animated: viewAnimate, scrollPosition: .None)
            cell?.setSelection(true)
        } else {
            cell?.setSelection(false)
        }
        cell?.backgroundColor = colBttnBrightGray
        return cell!
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        log.logUiAction("  tableView(tableView: UITableView,       didSelectRowAtIndexPath")
        let curCell = theTable.cellForRowAtIndexPath(indexPath) as! SendDataCell
        curCell.setSelection(true)
        refreshDeleteBttnCheckAllState()
    }

    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        log.logUiAction("  tableView(tableView: UITableView,       didDeselectRowAtIndexPath ")
        let curCell = theTable.cellForRowAtIndexPath(indexPath) as! SendDataCell
        curCell.setSelection(false)
        refreshDeleteBttnCheckAllState()
    }

    func addTableCellNib() {
        let nib = UINib(nibName: "SendDataCell", bundle: nil)
        theTable.registerNib(nib, forCellReuseIdentifier: "sendDataCell")
    }

}

