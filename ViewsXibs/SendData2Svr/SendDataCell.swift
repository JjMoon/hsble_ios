//
//  SendDataCell.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2015. 12. 22..
//  Copyright © 2015년 IMLab. All rights reserved.
//

import Foundation


class SendDataCell : UITableViewCell {
   
    @IBOutlet weak var bttnCheck: UIButton!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelEmail: UILabel!
    var checked = false
   
    var curStudent: HmStudent?
    
    func setItems(curStu: HmStudent) {
        curStudent = curStu
        labelName.text = curStudent!.name
        labelEmail.text = curStudent!.email
        setCheckImage(curStudent!.uiTargeted)
    }

    // MARK:  언어 세팅.
    func setLanguageString() {
    }

    @IBAction func bttnActCheck(sender: AnyObject) {
//        checked = !checked
//        setCheckImage(checked)
    }

    func setSelection(forcedValue: Bool) { // 이건 테이블에서 세팅 할 때.
        //curStudent?.uiTargeted = forcedValue
        curStudent?.uiTargeted = forcedValue
        setCheckImage(forcedValue)
    }

    private func setCheckImage(theVar: Bool) { // 이미지 와 타겟 멤버 세팅..
        curStudent?.uiTargeted = theVar
        if (curStudent?.uiTargeted)! == true {
            bttnCheck.setImage(UIImage(named: "btn_check"), forState: UIControlState.Normal)
        } else {
            bttnCheck.setImage(UIImage(named: "btn_uncheck"), forState: UIControlState.Normal)
        }
    }
    
}
