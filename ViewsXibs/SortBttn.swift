//
//  SortingPrtcl.swift
//  HS Monitor
//
//  Created by Jongwoo Moon on 2016. 9. 19..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation


class SortBttn: UIButton {
    var isAscend: Bool!
    var isActive: Bool!
    var otherBttn: SortBttn!

    private var txt: String! // 날짜순, 이름순

    override func awakeFromNib() {
        super.awakeFromNib()
        print(" override func awakeFromNib() ")
    }

    /// date 버튼만 불림.
    func initDateNameState(other: SortBttn) {
        otherBttn = other
        otherBttn.otherBttn = self
        txt = langStr.obj.date
        otherBttn.txt = langStr.obj.name
        isAscend = false // 시간 역순으로 정렬 초기화.
        isActive = true
        otherBttn.isAscend = false
        otherBttn.isActive = false
        setTitleWithArrow()
        setActiveDisplay()
        otherBttn.setTitleWithArrow()
        otherBttn.setActiveDisplay()
    }

    func sortTouched() {
        print("SortBttn : sortTouched \(self.titleLabel!.text)")
        if isActive! {
            isAscend = !isAscend
        } else {
            isAscend = true
            setActive(true)
        }
        setTitleWithArrow()
        setActiveDisplay()
    }

    /// 상호 재귀 안되도록
    func setActive(actv: Bool) {
        isActive = actv
        setActiveDisplay()
        if isActive! {
            otherBttn!.setActive(false)
        } else  {
            isAscend = true // 초기값.
            setTitleWithArrow()
        }
    }

    private func setTitleWithArrow() {
        let arrow = isAscend! ? " ▵" : " ▿"
        setTitle(txt + arrow, forState: .Normal)
        if !isActive! {
            setTitle(txt + " -", forState: .Normal)
        }
    }

    private func setActiveDisplay() {
        let col = isActive! ? colBttnGreen: colBttnDarkGray
        setTitleColor(col, forState: .Normal)
    } //  순 ▿  순 ▵
}


func sortStudentArrayBy(date: Bool, ascn: Bool, union: [HmStudent]) -> [HmStudent] {
    if date {
        let compr: (HmStudent, HmStudent) -> Bool = ascn ? { (sta, stb) -> Bool in return sta.dbid < stb.dbid } :
            { (sta, stb) -> Bool in return sta.dbid > stb.dbid }
        return union.sort(compr)
    } else {
        let ascd: NSComparisonResult = ascn ? .OrderedAscending: .OrderedDescending
        return union.sort({ (t1, t2) -> Bool in
            t1.name.compare(t2.name) == ascd
        })

    }
}
