//
//  HtBars.swift
//  HS Monitor
//
//  Created by Jongwoo Moon on 2016. 8. 2..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation


class HtBars: UIView {

    //var arrBars = [UILabel]()

    convenience init(_ myFrame: CGRect, _ weightArr: [Double],  _ colArr: [UIColor]) {
        self.init(frame: myFrame)
        var staX = frame.origin.x, oriY = frame.origin.y, hei = frame.size.height

        let totalWeight = weightArr.reduce(0) { (v: Double, v2: Double) -> Double in v + v2 }
        for (idx, wei) in weightArr.enumerate() {
            let wid = frame.size.width * CGFloat(wei / totalWeight)
            let newLabel = UILabel(frame: CGRect(x: staX, y: oriY, width: wid, height: hei))
            staX += wid
            newLabel.backgroundColor = colArr[idx]
            addSubview(newLabel)
            //arrBars.append(newLabel)
        }
    }


    func setWidths(weightArr: [Double],  _ colArr: [UIColor]) {
        var staX = frame.origin.x, oriY = frame.origin.y, hei = frame.size.height

        let totalWeight = weightArr.reduce(0) { (v: Double, v2: Double) -> Double in v + v2 }
        var preVw: UILabel?
        for (idx, wei) in weightArr.enumerate() {


            print("   idx : \(idx)    \(wei)")



            let newLabel = UILabel()

            // 폭 설정 ..
            //newLabel.setWidthMultiOf(wei / totalWeight, parentVw: self)
            newLabel.setWidthConst(wei / totalWeight * Double(frame.size.width))

            // 초기 > 리딩..
            if preVw == nil {
                let lead = NSLayoutConstraint(item: newLabel, attribute: .Leading, relatedBy: .Equal, toItem: self,
                                              attribute: .Leading, multiplier: 1, constant: 0)
                self.addConstraint(lead)
            } else {
                preVw!.setNeighbors(newLabel, parenV: self)
            }

            if idx + 1 == weightArr.count {
                let trail = NSLayoutConstraint(item: newLabel, attribute: .Trailing, relatedBy: .Equal, toItem: self,
                                               attribute: .Trailing, multiplier: 1, constant: 0)
                self.addConstraint(trail)
            }

            newLabel.setHeightConst(Double(hei))

            newLabel.backgroundColor = colArr[idx]
            addSubview(newLabel)

            preVw = newLabel
            //arrBars.append(newLabel)
        }
    }



}




extension UIView {
    func setWidthMultiOf(multiple: Double, parentVw: UIView) {
        let newConst = NSLayoutConstraint(item: self, attribute: .Width, relatedBy: .Equal, toItem: parentVw,
                                          attribute: .Width, multiplier: CGFloat(multiple), constant: 0)
        parentVw.addConstraint(newConst)
    }



    func setWidthConst(wid: Double) {
        let newConst = NSLayoutConstraint(item: self, attribute: .Width, relatedBy: .Equal, toItem: self,
                                          attribute: .Width, multiplier: 1, constant: CGFloat(wid))
        self.addConstraint(newConst)
    }


    func setHeightConst(height: Double) {
        let newConst = NSLayoutConstraint(item: self, attribute: .Height, relatedBy: .Equal, toItem: self,
                                          attribute: .Height, multiplier: 1, constant: CGFloat(height))
        self.addConstraint(newConst)
    }

    func setNeighbors(rigtV: UIView, parenV: UIView) {
        let nCon = NSLayoutConstraint(item: self, attribute: .Trailing, relatedBy: .Equal,
                                      toItem: rigtV, attribute: .Leading, multiplier: 1, constant: 0)
        self.addConstraint(nCon)
    }

}