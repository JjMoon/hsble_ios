//
//  DiscriminantView.swift
//  HS Monitor
//
//  Created by Jongwoo Moon on 2016. 7. 20..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation


class DiscriminantView : UIView {

    @IBOutlet weak var labelDiscriminant: UILabel! // 판별식, 디테일 뷰 .. 결과 관련
    @IBOutlet weak var labelCompScore: UILabel!
    @IBOutlet weak var labelBrthScore: UILabel!
    @IBOutlet weak var imgCompScore: UIImageView!
    @IBOutlet weak var imgBrthScore: UIImageView!
    @IBOutlet weak var labelFlowRatio: UILabel!
    @IBOutlet weak var labelFlowRatioVal: UILabel!

    var dataObj : HsData?

    func setDiscreminant() -> UIColor {  // 리턴 색깔로 view detail 버튼 색깔 세팅..
        self.cornerRad(15)
        
        //let discrmnt: Int = (dataObj?.discreminant())!
        labelDiscriminant.text =  "\(dataObj!.scoreTotalInt) %"
        labelDiscriminant.textColor = dataObj?.totalStringColor

        labelCompScore.text = "\(dataObj!.scoreCompInt) %"
        labelBrthScore.text = "\(dataObj!.scoreBrthInt) %"

        labelCompScore.textColor = dataObj!.scoreCompColor
        labelBrthScore.textColor = dataObj!.scoreBrthColor
        imgCompScore.image = UIImage(named: dataObj!.scoreCompImgname)
        imgBrthScore.image = UIImage(named: dataObj!.scoreBrthImgname)

        // 압박 실시율
        labelFlowRatio.text = langStr.obj.compressionRatio
        labelFlowRatioVal.text = "\(dataObj!.scoreFraction) %"
        labelFlowRatio.textColor = dataObj!.scoreFractionColor
        labelFlowRatioVal.textColor = dataObj!.scoreFractionColor

        return labelDiscriminant.textColor
    }



}
