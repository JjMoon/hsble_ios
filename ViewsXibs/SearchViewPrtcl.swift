//
//  SearchViewPrtcl.swift
//  HS Test
//
//  Created by Jongwoo Moon on 2016. 9. 1..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation


protocol SearchViewPrtcl : class  {
    associatedtype SomeType : HmStudent

    var searchMode: Bool { get set }
    var searchTxtField: UITextField? { get set }
    var searchConstraintWidth: NSLayoutConstraint? { get set }
    var searchedList : [SomeType] { get set }
    var sortByDate: Bool { get set }
    var sortAscend: Bool { get set }

    //func toggleSearch()
    func searchUIAnimation()
    func reloadTableView()
    func sortFromSearchProtocol(union: [SomeType])
}

extension SearchViewPrtcl where Self: UIViewController {

    //func toggleSearch() {    }

    func searchUIAnimation() {

        print(" >>> Protocol <<< SearchViewPrtcl >> ==== ")

        var targetWidth : CGFloat = 300
        var opacityTar : Float = 0.95
        if searchMode {
            searchTxtField?.hidden = false
            searchTxtField?.becomeFirstResponder()
        } else {
            //view.endEditing(true)
            targetWidth = 0
            opacityTar = 0.6
        }
        print("   target width \(targetWidth)  opacity  \(opacityTar)")

        UIView.animateWithDuration(0.8, delay: 0, usingSpringWithDamping: 5.8,
                                   initialSpringVelocity: 0, options: .CurveEaseInOut,
                                   animations: {
                                    //self.txtSearch.frame.size.width = CGFloat(targetWidth)
                                    self.searchTxtField!.layer.opacity = opacityTar
                                    self.searchConstraintWidth!.constant = targetWidth
                                    self.view.layoutIfNeeded()
            }, completion: { finished in
                print(" >>> Protocol <<< SearchViewPrtcl >> ====   completion  ")
                self.searchTxtField!.hidden = !self.searchMode
        })
    }

    func refreshSearchTextAction(filterCond: (SomeType)->Bool, totalList: [SomeType] ) {
        if !searchMode {
            reloadTableView()
            return
        }
        if 0 < searchTxtField!.text!.getLength() || 0 < searchTxtField!.text!.getLengthUTF32() {
            searchedList = totalList.filter({ (std) -> Bool in
                filterCond(std)
            })
        } else { // 하나도 없으면 전체 표시..
            searchedList = totalList
        }

        sortFromSearchProtocol(searchedList)
        reloadTableView()
    }



}
