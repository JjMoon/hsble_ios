//
//  HtImageView.swift
//  Trainer
//
//  Created by Jongwoo Moon on 2016. 5. 11..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation


class HtImageView: UIImageView {
    var curImgName = "None"

    func setImageWith(name: String) {
        print("setImageWith >>  \(name)   \(curImgName) ")
        if curImgName == name { return }
        print(" change Image with ...    ")
        curImgName = name
        self.image = UIImage(named: name)
    }
}