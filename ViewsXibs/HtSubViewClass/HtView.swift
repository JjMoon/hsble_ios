//
//  HtView.swift
//  Test
//
//  Created by Jongwoo Moon on 2016. 2. 15..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation

class HtView : UIView {
    //////////////////////////////////////////////////////////////////////     [     ]    _//////////_   UI Activation 관련
    // MARK: UI Active, Enable Control
    var uiReleased = true

    var uiCallBack: (Void)->Void = {  } // 다용도 콜백.
    var uiPauseCallBack: (Bool)->Void = { (started) in } // true 면 제어 시작, false 면 릴리즈..
    var uiIntCallBack: (Int) -> Void = { (intVal) in } // Int 넘겨주는 콜백..
    var uiStringCallBack: (String) -> Void = { (str) in } // String 넘겨주는 콜백..

    var bleCallBack: (HsBleSuMan)->Void = { (bleObj) in } // Int 넘겨주는 콜백..

    func releaseUI() {
        uiReleased = true
    }

    func freezeUI() {
        uiReleased = false
    }

    func initSet() {
        setLanguageString()
    }

    func setCheckImgOf(imgVw: UIImageView, accordingTo: Bool) {
        if accordingTo {
            imgVw.image = UIImage(named: "img_bls_check")
        } else {
            imgVw.image = UIImage(named: "img_bls_uncheck")
        }
    }

    // MARK:  언어 세팅.
    func setLanguageString() {
    }

}
