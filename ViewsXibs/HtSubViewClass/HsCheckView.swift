//
//  HsCheckView.swift
//  HS Test
//
//  Created by Jongwoo Moon on 2016. 9. 6..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation


class HsCheckView: UIView {

    var bttn : UIButton?
    var lbl : UILabel?

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    convenience init(posi: CGPoint, labelText: String) {
        self.init(frame: CGRect(origin: posi, size: CGSize(width: 250, height: 35)))
        initSet(labelText)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func initSet(labelText: String) { // 300 x 35
        bttn = UIButton(frame: CGRect(x: 0, y: 0, width: 35, height: 35))
        bttn!.setBackgroundImage(UIImage(named: "btn_uncheck"), forState: .Normal)
        lbl = UILabel(frame: CGRect(x: 60 - 15, y: 0, width: 200, height: 35))
        lbl!.text = labelText
        lbl!.numberOfLines = 0
        lbl!.baselineAdjustment = .AlignCenters
        lbl!.adjustsFontSizeToFitWidth = true
        lbl!.font = UIFont.systemFontOfSize(15, weight: UIFontWeightThin)
        addSubview(bttn!)
        addSubview(lbl!)

        bringSubviewToFront(bttn!)
    }
    
}
