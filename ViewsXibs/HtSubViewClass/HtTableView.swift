//
//  ResultUnitVw.swift
//  HS Test
//
//  Created by Jongwoo Moon on 2016. 10. 5..
//  Copyright © 2016년 IMLab. All rights reserved.
//


import UIKit

struct HtTableSection {
    var title: String, headerHeight: CGFloat
    var arrRow: [UIView]!
}

class HtTableView: UIView, UITableViewDelegate, UITableViewDataSource {
    var data: [HtTableSection]!
    var tableView: UITableView!, cellId: String!
    var offset: CGFloat = 0

    var selectedCallback = { (cell: UIView) in }, deSelectedCallback = { (idx: NSIndexPath) in }

    override func awakeFromNib() {
    }

    func initSet(frm: CGRect, scroll: Bool, select: Bool = false) {
        tableView = UITableView(frame: frm)
        tableView.frame = CGRect(origin: CGPoint(x: 0, y: 0), size: frm.size)


        print("widdd :   initSet ::   \(tableView.frame.width)")


        //tableView.backgroundColor = UIColor.darkGrayColor()  // 4debug
        /// 배경 투명..
        backgroundColor = UIColor.clearColor()
        tableView.backgroundColor = UIColor.clearColor()
        tableView.delegate = self
        tableView.dataSource = self
        addSubview(tableView)
        tableView.scrollEnabled = scroll
        tableView.allowsSelection = select
        tableView.separatorStyle = .SingleLine

        tableView.showsVerticalScrollIndicator = false
    }

    /// 섹션의 수는 data.count ..
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return data.count
    }
    /// 섹션의 행 수..
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //print("HtTableView: UITableView, numberOfRowsInSection : \(data[section].arrRow.count)")
        return data[section].arrRow.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        //print(" HtTableView   cellForRowAtIndexPath  >>>>>  :: \(indexPath.row) ")
        let curVw = data[indexPath.section].arrRow[indexPath.row]
        let cell = UITableViewCell()

        //print("widdd : cellForRowAtIndexPath  \(cell.frame.width)   \(curVw.frame.width)")

        cell.setHeightWith(curVw.frame.height + 2 * offset)
        curVw.frame = CGRect(origin: CGPoint(x: 0, y: offset), size: CGSize(width: self.frame.width, height: curVw.frame.height))
        cell.addSubview(curVw)
        cell.backgroundColor = UIColor.clearColor()
        curVw.setNeedsLayout(); curVw.layoutIfNeeded()
        return cell
    }

    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label1 = UILabel(frame: CGRect(x: 0, y: 0,
            //width: Stt.singl.uiScrWid, height: data[section].headerHeight))
            width: self.frame.width, height: data[section].headerHeight)) // 헤더 크기..
        label1.text = "     " + data[section].title
        label1.font = label1.font.fontWithSize(20)
        label1.backgroundColor = colorGreyBright //colBttnBrightGray
        return label1
    }

    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "  Another header.. .. "
    }

    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return data[section].headerHeight
    }

    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        //print("   HtTableView  ::  heightForFooterInSection  0")
        return CGFloat.min
    }

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let height = data[indexPath.section].arrRow[indexPath.row].frame.height
        //print("   HtTableView  cell height : \(height)   section \(indexPath.section)   row \(indexPath.row)")
        return height + 2 * offset
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print(" HtTableView    did Select at \(indexPath.row) ")
        selectedCallback(data[indexPath.section].arrRow[indexPath.row])
    }

    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        print(" HtTableView    de Selected :: at \(indexPath.row)")
        deSelectedCallback(indexPath)
    }
}

