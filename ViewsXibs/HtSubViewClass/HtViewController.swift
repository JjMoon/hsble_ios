//
//  HtViewController.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2016. 1. 28..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation


class HtViewController: ViewController {
    var callBack: Void -> Void = { }

    //////////////////////////////////////////////////////////////////////     [ UI      << >> ]
    // MARK: UI Active, Enable Control
    var isAvailable = true, timerInterval = 1.1
    var uiPauseCallBack: (Bool)->Void = { (started) in } // true 면 제어 시작, false 면 릴리즈..
    var uiTimer = NSTimer()

    override func viewDidAppear(animated: Bool) {
        uiTimer = NSTimer.scheduledTimerWithTimeInterval(timerInterval, // second
            target:self, selector: #selector(HtViewController.update),
            userInfo: nil, repeats: true)
    }

    override func viewWillDisappear(animated: Bool) {
        uiTimer.invalidate()
    }

    func update() {

    }

}
