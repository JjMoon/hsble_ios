//
//  SearchDeviceListViewCell.m
//  heartisense
//
//  Created by jeongyuchan on 2015. 4. 25..
//  Copyright (c) 2015년 gyuchan. All rights reserved.
//

#import "SearchDeviceListViewCell.h"
#import "HSAttributedString.h"
#import "DPLocalizationManager.h"
#import "HSBaseService.h"
#import "HsBleManager.h"
#import "HsBleSingle.h"
#import "NSObject+util.h"
#import "Common-Swift.h"

@interface SearchDeviceListViewCell()
{
    StrLocBase *lObj;
}
@end


@implementation SearchDeviceListViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)initWithData:(HSPeripheral*)peripheral { //stringObj:(StrLocBase*)strObj {
    _peripheral = peripheral;

    NSString* nameWithNumber = [_peripheral.name substringFromIndex:4];
    lObj = HsBleSingle.inst.langObj;

    CGFloat height = 30, fontSize = 19;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        height = 20; fontSize = 15;
    }

    [self.titleLabel
     setAttributedText:[HSAttributedString
                        defaultStyleAttributedString:nameWithNumber //_peripheral.name
                        font:[UIFont fontWithName:@"AppleSDGothicNeo-Thin" size:17.0]
                        fontColor:COLOR_HS_BLACK]];
    if (_peripheral.isConnected) {
        /*           * 연결되어있는 기기니, 연결 해제버튼을 보여준다.            */
        NSLog(@" 뷰셀 : >>>>>  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>   연결 되어 있슴.   >>>>>> Name : \t\t << %@ >>", _peripheral.name );
        [_connectButton
         setAttributedTitle:[HSAttributedString
                             defaultStyleAttributedString: lObj.scanning_kit // [self local Str:@"searching_kit"]  //  접속 되면 '연결중..
                             font:[UIFont fontWithName:@"AppleSDGothicNeo-SemiBold" size:fontSize]   //20.0] //17.0]
                             fontColor:COLOR_HS_GRAY]
         forState:UIControlStateNormal];
    } else {
        NSLog(@" 뷰셀 : No Connection   >>>>>> Name : << %@ >>   Ble : %@", _peripheral.name, _bleMan);

        [_connectButton
         setAttributedTitle:[HSAttributedString
                             //defaultStyleAttributedString:HsBleSingle.inst.langObj.connect_kit // [self localxStr:@"connect"] // 연결...
                             defaultStyleAttributedString:nameWithNumber
                             font:[UIFont fontWithName:@"AppleSDGothicNeo-SemiBold" size:fontSize] //17.0]
                             fontColor:COLOR_HS_BLACK]
         forState:UIControlStateNormal];
    }
}

//////////////////////////////////////////////////////////////////////     _//////////_     [ Select Device Button Touched .. ]    _//////////_
#pragma mark - SelectDevice Process ..

- (IBAction)connectAction:(id)sender {
    [self logNewLine:10];
    [self logCallerMethodwith:@"(IBAction)connectAction  \t\t\t <<  테이블 셀..   터치....     SelectDeviceFlow01 >>" newLine:0];
    [self logNewLine:10];
    
    [_bleMan setCurCell:self];  // 매니저의 멤버 변수에 세팅한다.
    [_bleMan setCellPeripheral:_peripheral];  // 매니저의 멤버 변수에 세팅한다.
    [_bleMan startConnection]; // deviceSelectedwith:_peripheral];
    //[_bleMan stopScan];
}

@end



