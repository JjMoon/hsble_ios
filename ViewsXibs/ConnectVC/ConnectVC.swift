//
//  ConnectVC.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2015. 11. 17..
//  Copyright © 2015년 IMLab. All rights reserved.
//

import Foundation

class ConnectVC : HtViewController, BleSearchPopupPrtc, PrtclEnlargeView {
    var log = HtLog(cName: "ConnectVC")
    var isCalibration = false
    var arrAddedView = [ConnectUnitView]()

    var targetView: UIView?
    var tarPoint: CGPoint?
    var baseOrigin: CGPoint? = nil
    var scal : CGFloat = 2.0
    var enlargeClsr: ((Void) -> Void)? = {
    }
    var bgView: UIView?
    var bttnClose: UIButton?

    // Ble Search View Protocol
    var searchView: BleSearchVw?
    var popUpVw: KLCPopup?
    var disMissed: ((Void) -> Void)?

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var bttn4debug: UIButton!
    @IBOutlet weak var bttnQuit: UIButton!
    @IBOutlet weak var bttnAssignStudent: UIButton!
    @IBOutlet weak var bttnGoHome: UIButton!
    @IBOutlet weak var bttnHelp: UIButton!

    var origin4debug: CGPoint?, oriFrame: CGRect?

    @IBAction func bttnTestRun(sender: AnyObject) { // bttn4Debug
        targetView = arrAddedView.first
        tarPoint = screenCenter

        enlargeClsr = {
            let vw = self.targetView as! ConnectUnitView
            vw.labelTitleAtoD.hideMe()
        }
        var selfAsEnlargeView : PrtclEnlargeView = self
        selfAsEnlargeView.animation()
    }

    @IBAction func bttnActAssignStudent(sender: AnyObject) { // 학생 배정
        log.logUiAction("bttnActAssignStudent")
        if HsBleMaestr.inst.connectionNumber() == 0 && !(AppIdTrMoTe == 2 && !isAdult) {
            let altVw = UIAlertView(title: langStr.obj.error, message: langStr.obj.at_least_1_connect, delegate: nil, cancelButtonTitle: langStr.obj.confirm)
            altVw.show()
        } else {
            let vc =  AssignStudentView(nibName: "AssignStudentsView", bundle: nil)
            self.navigationController?.pushViewController(vc, animated: viewAnimate)
        }
    }
    
    //////////////////////////////////////////////////////////////////////       [ UI Sub  ]

    // MARK:  메인으로 돌아가기.
    @IBAction func bttnActGoHome(sender: AnyObject) {  // 효과없는 alert view
        showAlertctrlRestart()
    }

    @IBAction func bttnActQuit(sender: AnyObject) {  // 여기서 메인으로 가는 건 같다..
        if AppIdTrMoTe == 2 && !isAdult {
            showAlertPopupOfBackToMainInfant({ () -> () in
                print(" showAlertPopupOfBackToMainInfant  nil... ")
            })
        } else {
            showAlertPopupOfBackToMainInMonitor() {
                HsBleMaestr.inst.disconnectAllConnections()
            }
        }
    }

    @IBAction func bttnActHelp(sender: AnyObject) {
        openHeartisenseInfoWeb(1)// Monitor
    }

    //////////////////////////////////////////////////////////////////////       [ UI Sub  ]
    override func viewWillAppear(animated: Bool) {
        log.printAlways("\(#function)", lnum: 10)

        for vw in arrAddedView {
            vw.viewWillAppear()
        }

        view.backgroundColor = HmGraphSetting.inst.viewBgBrightGray
        setLanguageString()
        calibration관련세팅()
    }

    override func viewDidAppear(animated: Bool) {
        timerInterval = 1/5 // second
        super.viewDidAppear(animated) // 타이머 세팅.

        if testInfant { // AppIdTrMoTe == 2 && !isAdult { // 영아 테스트 ... 경우 리턴...
            addTestInfantSubView()
        } else {
            addSubViews()
        }

        disMissed = { // 선택 팝업이 사라지면....
            print("disMissed = { // 선택 팝업이 사라지면....")
        }
    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        for vw in arrAddedView {
            vw.viewWillDisappear()
            //vw.theTimer?.invalidate()
        }
    }

    override func viewDidLoad() {
        if isDebugMode { bttn4debug.showMe() } else { bttn4debug.hideMe() }
        log.logUiAction("viewDidLoad", lnum: 10, printOn: true)

        if isCalibration { isAdult = true }
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.view.frame = screenSize
    }

    func getLiveKits() -> [ConnectUnitView] { // 현결된 키트 뷰 리턴..
        return arrAddedView.filter { (vw) -> Bool in
            (vw.myBleSuMan?.isConnected)!
        }
    }

    func addSubViews() {
        let title = ["A", "B", "C", "D"]
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "ConnectUnitView", bundle: bundle)

        if 0 < arrAddedView.count { return }

        for (idx, rect) in rectArr.enumerate() {
            //log.logThis("Set Connect Unit Vie  \(idx)    name :: >>  \(HsBleMaestr.inst.arrBleSuMan[idx].name)  ", lnum: 1)

            let vw = nib.instantiateWithOwner(self, options: nil).last as! ConnectUnitView
            vw.frame = rect
            vw.myBleSuMan = HsBleMaestr.inst.arrBleSuMan[idx]

            vw.disconnectCallBack = disConnected
            vw.connectionSuccess = connectionSuccess
            vw.productTypeError = productTypeError
            vw.errorCallback = errorFromView
            vw.uiPauseCallBack = uiPauseAction
            vw.openCalibrationVC = openCalibrationVC
            vw.conPrevious = connectPrevKit
            vw.startConnection = connectNewKit

            HsGlobal.delay(0.05, closure: {
                vw.myBleSuMan!.setConnectView(vw) // ble 에 view 를 갖고 있다.
                vw.labelTitleAtoD.text = title[idx]
                vw.initializeView()
            })

            // 기존 키트 번호 세팅.
            if (vw.myBleSuMan?.isTherePrevKit)! == true {
                vw.labelKitNum.text = vw.myBleSuMan!.prevKitName
                //vw.myBleSuMan!.targetPrevKitName = vw.myBleSuMan!.prevKitName
            } else {
                vw.labelKitNum.text = langStr.obj.register_kit
            }
            vw.setLanguageString()
            vw.isCalibration = isCalibration

            vw.initSet()
            self.view.addSubview(vw)
            arrAddedView.append(vw)
        }
    }

    func setBleCtrl(ble: HsBleSuMan, vw: ConnectUnitView) {
        vw.basicProcssObj.baseSetting(ble)

        vw.basicProcssObj.productTypeErrorEvent = {  // 키트 productType 에러 시 팝업 후 꺼짐..
            self.showSimpleMessageWith(langStr.obj.productTypeErrorExit, yesCallBack: { () in
                exit(0)
            })
        }

        // 접속 후 액션..
        // disMissed = afterConnectionProcess // 키트 변경 연결
        // conPrevKitObj.finishConnection = afterConnectionProcess // 이전 키트 연결.


        //prevDeviceName = NSUserDefaults.standardUserDefaults().stringForKey("PreviousDeviceNum")

    }

    func connectPrevKit(ble: HsBleSuMan) {
        print("  prevKit:::   \(ble.name) >>><<<")
    }

    func errorFromView(vw: ConnectUnitView) {
        log.printAlways("\(#function)")

        if let msg = vw.basicProcssObj.errorMsg {
            log.printAlways("\(#function)   \(msg)")

            let actionSheetController: UIAlertController =
                UIAlertController(title: nil, message: msg, preferredStyle: .Alert)
            presentViewController(actionSheetController, animated: viewAnimate, completion: nil)
            HsGlobal.delay(1.5) { () -> () in
                actionSheetController.dismissViewControllerAnimated(viewAnimate, completion: { () -> Void in
                    print(" 에러 표시창 닫음. \(msg)")
                })
            }
            //showSimpleMessageWith(msg, yesCallBack: { () in vw.myBleSuMan!.conState = .None            })
        }
    }

    func connectNewKit(ble: HsBleSuMan) {
        showBleListPopup(ble)
    }

    func connectionSuccess(bleSu: HsBleSuMan, kitId: String) {
        self.showConnectionSuccessMessage(kitId)
        self.refreshPrevKitName()

        if self.isCalibration {
            self.log.printAlways("if self.isCalibration {")
            bleSu.sendCalibrationToTrainer(); return // bleSu.cellService.sendAppInfoCalibration(); return
        }

        if AppIdTrMoTe == 1 {
            print("bleSu.sendAppInfoResponseMonitor()")
            bleSu.sendAppInfoResponseMonitor(); return
        } else {
            print("bleSu.sendAppInfoResponseTest()")
            bleSu.sendAppInfoResponseTest(); return
        }

    }

    func disConnected(bleMan: HsBleSuMan) {
        self.showGeneralPopup(langStr.obj.disconnect, message: langStr.obj.want_disconnect, yesStr: langStr.obj.yes, noString: langStr.obj.no, yesAction: { () -> Void in
            bleMan.closePort()
        })
    }

    func openCalibrationVC(bleman: HsBleSuMan) {
        let vc = CalibrationVC(nibName: "CalibrationVC", bundle: nil)
        vc.setBleObject(bleman)
        bleman.sendCalibrationToTrainer()
        self.presentViewController(vc, animated: viewAnimate, completion: nil)
    }

    func productTypeError() {
        let actionSheetController: UIAlertController = UIAlertController(title: nil,
                                                                         message:langStr.obj.not_permission,
                                                                         preferredStyle:.Alert)
        self.presentViewController(actionSheetController, animated: viewAnimate, completion: nil)
        HsGlobal.delay(2.0) { () -> () in
            actionSheetController.dismissViewControllerAnimated(viewAnimate, completion: {
                self.log.printAlways(" 연결 성공 뷰 :: dismiss \(langStr.obj.connect_success)", lnum: 10)
            })
        }
    }

    func uiPauseAction(started: Bool) {
        for vw in arrAddedView {
            if started { vw.freezeUI() }
            else { vw.releaseUI() }
        }
    }

    /// 각 뷰의 이전 기기 관련 뷰 리프레시..
    func refreshPrevKitName() {
        log.printAlways("refreshPrevKitName \(arrAddedView.count) 개의 뷰. ")

        for (idx, vw) in arrAddedView.enumerate() {
            let usrDefName = HsBleMaestr.inst.usrDefObj.arrPrevDevName![idx]
            print(" idx : \(idx) \t \(vw.labelTitleAtoD!.text)  name :\(usrDefName)   ")

            if usrDefName.getLength() < 2 {
                vw.labelKitNum.text = langStr.obj.register_kit
            } else {
                vw.labelReady.text = usrDefName.subStringFrom(4)
                vw.labelKitNum.text = usrDefName.subStringFrom(4)
            }
            vw.enablePrevKitButtons() /// 여기서 UI 조정.
        }
    }

    override func update() {
        // 테스트 영아일 때는 예외..
        if testInfant {
            bttnAssignStudent.enabled = true // Infant Case ..
        } else { // 그외의 모든 케이스...
            let cond1 = 0 < HsBleMaestr.inst.connectionNumber()
            let workingVws = arrAddedView.filter({ (vw) -> Bool in
                !vw.hidden && !vw.imgVwLoading.hidden // 보이는 놈...
            })
            let cond2 = workingVws.count == 0
            bttnAssignStudent.enabled = cond1 && cond2
        }
        if bttnAssignStudent.enabled {
            bttnAssignStudent.backgroundColor = colorBttnActiveBlue
        } else {
            bttnAssignStudent.backgroundColor = colorBttnDarkGray
        }

        if testInfant { return }

        // 버튼을 disable 시키기 위한 조치들..  return !(_connState == Initial || _connState == Connected);  // ble.isBusy() //
        var actingNum = 0
        for obj in HsBleMaestr.inst.arrBleSuMan {
            if obj.conState == .Rest { actingNum += 1 }
        }
        //
        //let actingObj = HsBleMaestr.inst.arrBleSuMan.filter { (b) -> Bool in return (b.conState == .Rest) }
        //let actingNum = actingObj.count

        if actingNum == 0 {
            bttnQuit.enabled = true
            bttnHelp.enabled = true
        } else {
            bttnQuit.enabled = bttnAssignStudent.enabled
            bttnHelp.enabled = bttnAssignStudent.enabled
        }
    }

    func calibration관련세팅() {
        if isCalibration {
            bttnAssignStudent.hideMe()
        }
    }

    // MARK:  언어 세팅.
    func setLanguageString() {
        if isCalibration {
            labelTitle.text = langStr.obj.calibration
        } else {
            if AppIdTrMoTe == 2 { // Test
                if isAdult {
                    labelTitle.text = langStr.obj.adult_title
                } else {
                    labelTitle.text = langStr.obj.infant_title
                }
            } else {
                labelTitle.text = langStr.obj.heartisense_monitor
            }
        }
        if AppIdTrMoTe == 1 {
            bttnAssignStudent.setTitle(langStr.obj.start_course, forState: .Normal)
        } else {
            bttnAssignStudent.setTitle(langStr.obj.start_test, forState: .Normal)
        }
        //setTitle(langStr.obj.assign_student, forState: .Normal)
        bttnAssignStudent.attributedTitleForState(.Normal)
        //bttnQuit.setTitle(langStr.obj.prev_gt, forState: .Normal)
        bttnQuit.setTitle(langStr.obj.quitArrow, forState: .Normal)
    }
}

extension ConnectVC : UIAlertViewDelegate {
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if buttonIndex == 0 {
            self.navigationController?.popViewControllerAnimated(viewAnimate)
        }
    }
}

