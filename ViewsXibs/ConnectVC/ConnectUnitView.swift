//
//  Operation.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2015. 11. 10..
//  Copyright © 2015년 IMLab. All rights reserved.
//

import Foundation
import UIKit

class ConnectUnitView : HtView {
    var log = HtLog(cName: "ConnectUnitView")
    var isCalibration = false

    var myBleSuMan:HsBleSuMan?
    //var deviceListView = SearchDeviceListView()
    var popup = KLCPopup()
    var theTimer:NSTimer?

    // 상위 뷰로 보내는 콜백 들...
    var productTypeError: () -> () = { }
    var errorCallback: (ConnectUnitView) -> Void = { (vw) in  }

    var disconnectCallBack: (HsBleSuMan) -> Void = { (bleObj) in  }
    var openCalibrationVC: (HsBleSuMan) -> () = { bleman in  }
    var connectionSuccess: (HsBleSuMan, String)->() = { (bleObj, kitId) -> () in  }

    var conPrevious: (HsBleSuMan) -> () = { bleman in  }
    var startConnection: (HsBleSuMan) -> () = { bleman in  }

    var conPrevKitObj = BleConnectPreviousKit(), basicProcssObj = HsInitialConnectSession()

    @IBOutlet weak var labelTitleAtoD: UILabel!
    @IBOutlet weak var labelStateNoti: UILabel!
    @IBOutlet weak var labelConnecting: UILabel!
    
    @IBOutlet weak var bttnOffX: UIButton!
    @IBOutlet weak var bttnConnPrevKit: UIButton!
    @IBOutlet weak var bttnChangeKit: UIButton!
    @IBOutlet weak var bttnChangeKitLetter: UIButton!
    @IBOutlet weak var bttnStartCalibration: HtBoxButton!
    @IBOutlet weak var labelKitNum: UILabel!
    @IBOutlet weak var imgVwLoading: UIImageView!
    @IBOutlet weak var labelReady: UILabel!

    @IBOutlet weak var labelInfantReady: UILabel!

    @IBAction func bttnActOff(sender: AnyObject) {
        if uiReleased == false { return } // 다른 뷰 실행 시 disable

        disconnectCallBack(myBleSuMan!)

        //onlineActions()  // 160730
        //myBleSuMan?.reset()
    }
    
    @IBAction func bttnConnPrevKit(sender: UIButton) {
        myBleSuMan?.productTypeErrorEvent = { [weak self] in
            self!.productTypeErrorEvent()
        }
        log.logUiAction("  bttnConnPrevKit ::  \(myBleSuMan?.name)", lnum: 10)

        if uiReleased == false { return } // 다른 뷰 실행 시 disable

        myBleSuMan!.setStateOfVC = setState

        print("  myBleSuMan?.isTherePrevKit   \(myBleSuMan?.isTherePrevKit) ")

        if (myBleSuMan?.isTherePrevKit)! == false { // 이전 킷번호가 없을 때
            changeKitAction()
            return // 여기서 멈춤..
        }

        uiPauseAction()

        conPrevKitObj.connectWith(myBleSuMan!, kitName: "HS_A" + myBleSuMan!.prevKitName)
        conPrevious(myBleSuMan!)

        setLoadingView()

        stopTimer()
    }
    
    @IBAction func bttnActChangeKit(sender: AnyObject) {
        log.logUiAction("bttnActChangeKit")
        myBleSuMan?.productTypeErrorEvent = {
            self.productTypeErrorEvent()
        }
        if uiReleased == false { return } // 다른 뷰 실행 시 disable
        changeKitAction()
    }

    @IBAction func bttnActStartCali(sender: AnyObject) {
        log.logUiAction("bttnActStartCali  \(myBleSuMan!.prevKitName) ")
        openCalibrationVC(myBleSuMan!)
    }

    //////////////////////////////////////////////////////////////////////       [ UI Public  ]
    override func awakeFromNib() {
        //log.printAlways("\(#function) line _ \(#line)     \t\t\t  ")
        //log.printThisFunc("awakeFromNib", lnum: 3)
        // infantAction()
    }

    /// 초기에 불림.. setState 콜백 설정. 기본 설정.
    override func initSet() {
        super.initSet()
        myBleSuMan!.setStateOfVC = setState
        basicProcssObj.baseSetting(myBleSuMan!)

        initializeView()
    }

    func viewWillAppear() {
        myBleSuMan!.setStateOfVC = setState
        connectionTimerCheck(false)
    }

    func viewWillDisappear() {
        myBleSuMan!.setStateOfVC = nil
        //myBleSuMan!.dataReceived = nil
        stopTimer()
    }

    /// ConnectVC 에서 실행.
    func initializeView() {
        log.printThisFunc("initializeView", lnum: 1)
        self.setNeedsUpdateConstraints()
        infantAction()
        setPrevKitNum()
    }

    /// 이전 키트 존재하면 우상단 키트 교체 버튼들 숨기기. 온라인과는 상관 없는 기본 뷰..
    func enablePrevKitButtons() {
        log.printAlways("enablePrevKitButtons", lnum: 1)

        if myBleSuMan!.conState == .Rest { return }

        hideAllUIElements()
        showEvery(labelKitNum, bttnChangeKit, bttnChangeKitLetter, bttnConnPrevKit)
        labelStateNoti.textColor = colBttnDarkGray
        labelTitleAtoD.textColor = colBttnDarkGray
        setPrevKitNum()
    }

    //////////////////////////////////////////////////////////////////////       [ 상태 ]

    var 접속메시지 = false

    func setState() {
        //log.printAlways("\(#function) line _ \(#line)     \t\t\t  \(myBleSuMan!.prevKitName) ")
        log.printAlways("\(#file) :: \(#function) >>>    \(myBleSuMan!.nick)     \(myBleSuMan!.conState) ")
        switch myBleSuMan!.conState {
        case .None:
            print("\(#file) :: \(#function)  case .None ")
            접속메시지 = true
            initializeView()
            setOfflineView()
            //setUINormal()
        case .OpenStart:
            setLoadingView()
            print("\(#file) :: \(#function)  case .OpenStart ")
        case .ConnectFail:
            print("\(#file) :: \(#function)  Fail ??   >>>  .ConnectFail  ")
            //setUINormal()
            setOfflineView()
            conPrevKitObj.cancelAction()
            basicProcssObj.cancelAction()
            myBleSuMan!.closePort() // 여기서 상태 변경..
        case .Connected:
            접속메시지 = true
            setLoadingView()
            myBleSuMan!.futureConState = .BasicJobStarted
            basicProcssObj.startProcess() //  여기서 후속작업을 시작 시킨다...
        case .BasicJobStarted, .Cali:
            break
        case .Rest:
            setOnlineView()
            connectionTimerCheck(true)
            //connectionFinishedClojure() //goToNextPage()
        case .Error: // 다시 활성화...
            setOfflineView()
            errorCallback(self)
            uiPauseCallBack(false)
            conPrevKitObj.cancelAction()
            basicProcssObj.cancelAction()
            myBleSuMan!.futureConState = .None
        default:
            break
        }
    }

    //////////////////////////////////////////////////////////////////////     [ << UI update  & Private >> ]

    private func stopTimer() {
        print("stopTimer()   \(theTimer)")
        if theTimer != nil {
            theTimer!.invalidate()
            theTimer = nil
            //theTimer = NSTimer()
        }
        print("stopTimer()   \(theTimer)")
    }

    /// 뷰에 다시 들어왔을 때.. 상태 콜백 세팅 등..
    private func connectionTimerCheck(connSuccess: Bool) {
        log.printAlways("\(#function) line _ \(#line)     \t\t\t  \(myBleSuMan!.prevKitName) ")
        if myBleSuMan == nil || myBleSuMan!.conState != .Rest { return }

        myBleSuMan!.setStateOfVC = setState
        if theTimer == nil {
            if connSuccess && 접속메시지 {
                HsBleMaestr.inst.deviceConnected((myBleSuMan)!)
                connectionSuccess(myBleSuMan!, myBleSuMan!.prevKitName)
                접속메시지 = false
            }
            theTimer = NSTimer()
//            theTimer = NSTimer.scheduledTimerWithTimeInterval(0.3, target:self, selector: 
//            #selector(ConnectUnitView.uiUpdateAction), userInfo: nil, repeats: true)
        }
    }

    //////////////////////////////////////////////////////////////////////     [ << UI ON / OFF 뷰 세팅.  이전 기기 세팅... >> ]

    /// Off Line 뷰 세팅...
    private func setOfflineView() {
        hideEvery(bttnOffX, imgVwLoading, labelConnecting, labelReady, bttnStartCalibration)
        showEvery(bttnConnPrevKit, labelKitNum)
        labelStateNoti.text = langStr.obj.disconnected
        labelStateNoti.textColor = colBttnDarkGray
        labelTitleAtoD.textColor = colBttnDarkGray
        setPrevKitNum()
    }

    /// Off Line 뷰 세팅...
    private func setOnlineView() {
        hideEvery(labelKitNum, bttnConnPrevKit, bttnChangeKit, bttnChangeKitLetter, imgVwLoading, labelConnecting)
        showEvery(labelReady, bttnOffX)

        labelStateNoti.text = langStr.obj.connected // "Connected"
        labelStateNoti.textColor = colBttnGreen // 녹색 글씨 표시..
        labelTitleAtoD.textColor = colBttnGreen
        //labelConnecting.textColor = colorBarBgGrn

        calibrationViewSetting()
    }

    //////////////////////////////////////////////////////////////////////     [ << UI update  >> ]

    func uiUpdateAction() { // 접속이 완료되면 타이머 세팅됨...
        log.printThisFunc("\(#function)  \(myBleSuMan?.nick)  \(myBleSuMan?.thePort?.name) ", lnum: 0)
        // onlineActions()//
        connectionCheckUpdate()
    }

    private func connectionCheckUpdate() {
        //log.printThisFunc(" connectionCheck : \(labelTitleAtoD.text)")
        //log.logThis("  View : \(labelTitle.text)  \(HsUtil.curStepStr())  ", lnum: 2)
        //if bleMan == nil { disableThisView(); return }
        if (myBleSuMan!.isPortClosed) { //if (!myBleSuMan!.isConnected) {
            log.printAlways(" \(#function)   : \(labelTitleAtoD.text)  myBleSuMan!. \(myBleSuMan!.conState) ", lnum: 10)

            initializeView()
            stopTimer()
            setOfflineView()
        }
        log.함수차원_출력_End()
    }

    private func productTypeErrorEvent() {
        productTypeError() // 상위 콜백.
        //onlineActions()
    }

    private func uiPauseAction() {
        log.logUiAction("  UI 제어 시작 ....  \(connUiDiableTime) 초  connUiDiableTime  후 릴리즈됨....")
        uiPauseCallBack(true)
        HsGlobal.delay(connUiDiableTime) { () -> () in
            self.log.logUiAction("  UI 제어 끝  ....  \(connUiDiableTime) 초  connUiDiableTime 경과......")
            self.uiPauseCallBack(false)
        }
    }

    private func changeKitAction() { // 이전 기기가 없으면 여기로 들어온다..
        log.printThisFunc("changeKitAction", lnum: 1)
        setLoadingView()

        stopTimer()

        // Parent VC 로 콜백.
        startConnection(myBleSuMan!)
    }

    private func setLoadingView() {
        log.printThisFunc("setLoadingView", lnum: 1)
        hideAllUIElements()
        showEvery(imgVwLoading, labelConnecting)
        imgVwLoading.rotate360Degrees(1.5, repeatCnt: Float.infinity, completionDelegate: nil)
    }

    private func hideAllUIElements() {
        let hid = true
        log.printThisFNC("hideAllUIElements", comment: "hideUI(hid: \(hid))")
        bttnChangeKit.hidden = hid
        bttnChangeKitLetter.hidden = hid
        bttnConnPrevKit.hidden = hid
        labelReady.hidden = hid
        labelConnecting.hidden = hid
        imgVwLoading.hidden = hid
        labelKitNum.hidden = hid
    }

    private func setPrevKitNum() {
        //labelKitNum.showMe()
        if (myBleSuMan?.isTherePrevKit)! == false { // 이전 번호가 없으면..
            print("setPrevKitNum  PRevKit ??   이전 번호가 없으면.. ")
            labelKitNum.text = langStr.obj.register_kit
            hideEvery(bttnChangeKit, bttnChangeKitLetter)
        } else {
            showEvery(bttnChangeKit, bttnChangeKitLetter)
        }
    }


}

extension ConnectUnitView {

    func infantAction() {
        //log.printAlways("\(#function) line _ \(#line)     \t\t\t  crash myBleSuMan!.prevKitName) ")
        if testInfant {

            print("  testInfant... Yes?? ")

            labelInfantReady.showMe()
            labelInfantReady.cornerRad(15)
            bttnChangeKit.hideMe()
            bttnChangeKitLetter.hideMe()
            labelInfantReady.text = langStr.obj.ready
            labelTitleAtoD.textColor = colorOKGreen

            labelStateNoti.text = langStr.obj.not_assigned
            labelStateNoti.textColor = colBttnGreen
            labelTitleAtoD.textColor = colBttnGreen

            stopTimer()
        } else {
            labelInfantReady.hideMe()
        }
    }


    private func calibrationViewSetting() {
        if isCalibration {
            print("  isCalibration  \(isCalibration)")
            labelReady.hideMe()
            bttnStartCalibration.showMe()
            bttnStartCalibration.setTitle(langStr.obj.calibration_start, forState: .Normal)
            labelKitNum.showMe()

        } else {
            bttnStartCalibration.hideMe()
        }
    }

    // MARK:  언어 세팅.
    override func setLanguageString() {
        if AppIdTrMoTe == 2 && !isAdult { return }

        labelConnecting.hideMe()
        labelConnecting.text = langStr.obj.connecting_kit
        labelStateNoti.text = langStr.obj.disconnected
        bttnChangeKitLetter.setTitle(langStr.obj.change_kit_2, forState: .Normal)
        bttnChangeKitLetter.titleLabel?.numberOfLines = 0
        bttnChangeKitLetter.titleLabel?.alignCenter()
    }


}
