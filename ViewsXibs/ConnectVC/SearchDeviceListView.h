//
//  SearchDeviceListView.h
//  heartisense
//
//  Created by jeongyuchan on 2015. 4. 25..
//  Copyright (c) 2015년 gyuchan. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SearchDeviceListViewCell.h"

@class HsBleManager, HSCentralManager;

@protocol SearchDeviceListViewDelegate;

@interface SearchDeviceListView : UIView <UITableViewDataSource, UITableViewDelegate>  //, SearchDeviceListCellDelegate>
{

    //HsBleManager *bleMan;
}

@property (nonatomic, weak) HsBleManager* bleMan;
@property (weak, nonatomic) HSCentralManager *centManager;
@property (nonatomic, retain) NSString* logHeader;
@property (nonatomic, weak) id<SearchDeviceListViewDelegate> lstVwDelegate;
@property (atomic, assign) NSArray *deviceList;


- (void)setBlockAndBleMan:(HsBleManager*)pObj;

- (void)reloadListData;
- (void) sendMessageToVC:(SearchDeviceListViewCell *)cell service:(HSBaseService *)service;

@end



@protocol SearchDeviceListViewDelegate <NSObject>
- (void)connectDeviceResult:(HSPeripheral*)peripheral hsService:(HSBaseService*)service;
@end