//
//  SearchDeviceListViewCell.h
//  heartisense
//
//  Created by jeongyuchan on 2015. 4. 25..
//  Copyright (c) 2015년 gyuchan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HSPeripheral.h"



@class HsBleManager;

@class StrLocBase;


@interface SearchDeviceListViewCell : UITableViewCell
{
    //HsBleManager* bleMan;
}

@property (nonatomic, weak) HsBleManager* bleMan;
@property (weak, nonatomic) HSPeripheral *peripheral;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *connectButton;

- (void)initWithData:(HSPeripheral*)peripheral;
//- (void)initWithData:(HSPeripheral*)peripheral stringObj:(StrLocBase*)strObj;
@end

//
//@protocol SearchDeviceListCellDelegate <NSObject>
////- (void)connectResultAt:(SearchDeviceListViewCell *)cell service:(HSBaseService*)service;
//@end