//
//  TestInfantConnUnitView.swift
//  Test
//
//  Created by Jongwoo Moon on 2016. 2. 16..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation

class TestInfantConnUnitView : UIView {
    //
    @IBOutlet weak var labelABCD: UILabel!
    @IBOutlet weak var labelStateNoti: UILabel!
    @IBOutlet weak var labelReady: UILabel!

    // MARK:  언어 세팅.
    func setLanguageString() {
        labelStateNoti.text = langStr.obj.not_assigned
        labelReady.text = langStr.obj.ready
    }
}