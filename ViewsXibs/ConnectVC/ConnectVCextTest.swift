//
//  ConnectVCextTest.swift
//  HS Test
//
//  Created by Jongwoo Moon on 2016. 10. 20..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation


extension ConnectVC {

    func addTestInfantSubView() {
        let title = ["A", "B", "C", "D"]
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "TestInfantConnUnitView", bundle: bundle)

        for (idx, rect) in rectArr.enumerate() {
            //let vwFrame = vw.frame
            let newVw = nib.instantiateWithOwner(self, options: nil).last as! TestInfantConnUnitView
            newVw.frame = rect// vw.frame
            newVw.labelABCD.text = title[idx]
            //newVw.myBleSuMan = HsBleMaestr.inst.arrBleSuMan[idx]
            self.view.addSubview(newVw)
            newVw.setLanguageString()
        }
    }

    
}
