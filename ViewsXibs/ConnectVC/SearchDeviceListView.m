//
//  SearchDeviceListView.m
//  heartisense
//
//  Created by jeongyuchan on 2015. 4. 25..
//  Copyright (c) 2015년 gyuchan. All rights reserved.
//

#import "SearchDeviceListView.h"
#import "SearchDeviceListViewCell.h"
#import "HSAttributedString.h"
#import "DPLocalizationManager.h"
#import "NSObject+Util.h"
#import "Common-Swift.h"

//#import "HsBleManager.h"
#import "HsBleSingle.h"

@interface SearchDeviceListView()
{
    StrLocBase *lObj;
    NSTimer* refreshTimer;
}

@end

//////////////////////////////////////////////////////////////////////     _//////////_     [ ViewController ]    _//////////_
@implementation SearchDeviceListView
//////////////////////////////////////////////////////////////////////////////////////////
{
    UITableView *_tableView;
}

@synthesize logHeader;


- (void)awakeFromNib {

    NSLog(@"  SearchDeviceListView :: awake from nib  ");

    refreshTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self
                                                  selector:@selector(reloadListData)
                                                  userInfo:nil repeats:YES];
}


- (void)drawRect:(CGRect)rect {

    lObj = HsBleSingle.inst.langObj;

    if (refreshTimer == nil) {
        refreshTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self
                                                      selector:@selector(reloadListData)
                                                      userInfo:nil repeats:YES];
    }

    
    _tableView = [[UITableView alloc]initWithFrame:rect style:UITableViewStylePlain];
    [_tableView setDelegate:self];
    [_tableView setDataSource:self];
    [_tableView registerNib:[UINib nibWithNibName:@"SearchDeviceListViewCell" bundle:nil] forCellReuseIdentifier:@"SearchDeviceListViewCell"];
    [self addSubview:_tableView];
    
    /*
     * tableview header 설정
     */

    CGFloat height = 30, fontSize = 18;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        height = 28; fontSize = 15;
        _tableView.rowHeight = 35;
    }

    UIView *headerView =[[UIView alloc]initWithFrame:CGRectMake(10.f, 0.f, _tableView.frame.size.width, height)];
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:headerView.frame];
    // 타이틀 >> 키트 검색중 ...  메시지...
    [titleLabel setAttributedText:[HSAttributedString
                                   defaultStyleAttributedString:lObj.scanning_kit //[self loca lStr:@"bluetooth_searching"]
                                   font:[UIFont fontWithName:@"AppleSDGothicNeo-Light" size:fontSize]
                                   fontColor:COLOR_HS_GRAY]];
    [headerView addSubview:titleLabel];
    [_tableView setTableHeaderView:headerView];
    
    [self addSubview:_tableView];



    [self cornerRad:5];

    [self setNeedsDisplay];
    //or calculate the needed display rect by yourself and then
    [self setNeedsDisplayInRect:rect];
}

- (void)setBlockAndBleMan:(HsBleManager *)pObj {
    _bleMan = pObj;
    [_bleMan setUpdateDevListViewBlock:^(HSCentralManager *centralMan) {
        [self updateDeviceListView:centralMan];
    }];
}

- (void)reloadListData {
    //NSLog(@"  SearchDeviceListView :: reloadListData  ");
    [self setDeviceList:_centManager.ymsPeripherals];
    [_tableView reloadData];



    //    [_centManager removeAllPeripherals];


}

-(void) updateDeviceListView:(HSCentralManager*)centralManager {
    [self setDeviceList:centralManager.ymsPeripherals];
    _centManager = centralManager;
    [_tableView reloadData];
}

#pragma mark - Tableview datasource & delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _deviceList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSLog(@"%@ tableView:cellForRowAtIndexPath  \t\t\t <<  >> \n\n", logHeader);  // repeated..
    SearchDeviceListViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SearchDeviceListViewCell"];
    if (!cell) {
        cell = [[SearchDeviceListViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SearchDeviceListViewCell"];
    }
    
    cell.bleMan = _bleMan;
    [cell initWithData:[_deviceList objectAtIndex:indexPath.row]];    //[cell setDelegate:self];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%@ tableView:didSelectRowAtIndexPath  \t\t\t <<  >> \n\n", logHeader);
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - SearchDeviceListCellDelegate

- (void)connectResultAt:(SearchDeviceListViewCell *)cell service:(HSBaseService *)service
{
    NSLog(@"\n\n%@ connectResultAt  \t\t\t << DSFlow02 | SelectDeviceFlow02 >>  ~from~ ListCell => Delegate ~~ \n\n", logHeader);
    
    [self sendMessageToVC:cell service:service];
    
    /* if([self.lstVwDelegate respondsToSelector:@selector(connectDeviceResult:hsService:)]) { // VC 의 대리 함수를 call ..
     [self.lstVwDelegate connectDeviceResult:cell.peripheral hsService:service];
     } */
}

-(void) sendMessageToVC:(SearchDeviceListViewCell *)cell service:(HSBaseService *)service
{
    if([self.lstVwDelegate respondsToSelector:@selector(connectDeviceResult:hsService:)]) { // VC 의 대리 함수를 call ..
        [self.lstVwDelegate connectDeviceResult:cell.peripheral hsService:service];
    }
}


@end
