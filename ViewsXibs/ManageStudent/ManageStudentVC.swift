//
//  ManageStudentVC.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2015. 11. 18..
//  Copyright © 2015년 IMLab. All rights reserved.
//

import Foundation
import UIKit
import PSAlertView

// 테스트는 다른 파일로 관리한다.. xib 은 공동 사용.
/**  

 모니터 테스트 공통..

 */

class ManageStudentVC : UIViewController, UIAlertViewDelegate, SearchViewPrtcl {
    var log = HtLog(cName: "ManageStudentVC")

    var arrCell = [ManageStudentCell]()
    var checkSelected = false

    /// SearchViewPrtcl
    var searchTxtField: UITextField?
    var searchMode = false
    var searchConstraintWidth: NSLayoutConstraint?
    var searchedList = [HmStudent]()
    var studentList: [HmStudent] {
        get {
            if searchMode { return searchedList
            } else {        return HsBleMaestr.inst.arrStudent }
        }
    }

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelAddStudent: UILabel!
    @IBOutlet weak var bttnSend2Server: UIButton!
    @IBOutlet weak var bttnDelete: UIButton!
    @IBOutlet weak var bttnCheckAll: UIButton!
    @IBOutlet weak var bttnCheckAllBox: UIButton!
    @IBOutlet weak var labelStudentInfo: UILabel!
    @IBOutlet weak var bttnQuit: UIButton!

    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtSearch: UITextField!

    @IBOutlet weak var vwNameEmail: UIView!
    @IBOutlet weak var vwTableContainer: UIView!

    @IBOutlet weak var tableStudent: UITableView!
    
    @IBOutlet weak var vwProgressAnimation: HmDotProcess!
    
    // 기존 데이터 처리 관련 
    @IBOutlet weak var vwOldTestDB: UIView!
    @IBOutlet weak var lblOldTest: UILabel!
    @IBOutlet weak var btnOldTestOK: UIButton!
    @IBOutlet weak var btnOldTestCancel: UIButton!
    
    @IBAction func actionOldDbCancel(sender: UIButton) {
        vwOldTestDB.hideMe()
    }
    
    @IBAction func btnOldDbOK(sender: UIButton) {
        isTestOldCase = true
        
        btnOldTestOK.deactivate()
        btnOldTestCancel.deactivate()
        
        HsBleMaestr.inst.fmdbHandler.이전_DB_전송데이터_있는지(true) // 여기서 다시 읽고, 학생도 생성하고..
        if 0 < instructorID {  //  4 Debugging..  if 0 == instructorID {
            log.logThis("  Has ..  Instructor ID : \(instructorID) ")
            이전데이터sendAction()
        } else {
            let vc = LoginVC(nibName: "Login", bundle: nil)
            
            vc.okAction = {
                print("  OK ...   시작 됐당...")
                self.vwProgressAnimation.showMe()
                
                self.vwProgressAnimation.backgroundColor = colBttnBrightGray
            }
            
            vc.cancelAction = {
                isTestOldCase = false
                HsBleMaestr.inst.readDB()
                hideEvery(self.vwOldTestDB, self.vwProgressAnimation)
            }
            
            vc.modalPresentationStyle = .FormSheet // .Popover
            presentViewController(vc, animated: viewAnimate, completion: nil)
            vc.popoverPresentationController?.sourceView = view
            vc.popoverPresentationController?.sourceRect = view.frame
            vc.loginSuccessCallback = {
                self.이전데이터sendAction()
            }
            vc.setLanguageString()
        }
        
        log.logThis("  Has ..  Instructor ID : \(instructorID)  isTestOldCase : \(isTestOldCase)")
    }
    
    func 이전데이터sendAction() {
        let targetStudent = HsBleMaestr.inst.fmdbHandler.이전데이터_보유_학생()
        log.printThisFNC("sendAction", comment: " 타겟 학생 수 : \(targetStudent.count) ", lnum: 10)
        
        let svrRequest = HtServerRequest(interval: 0.1, theArray: targetStudent)
        
        svrRequest.progress = vwProgressAnimation
        
        svrRequest.isOldJob = true
        svrRequest.liscenceErrorCallBack = {
            self.showSimpleMessageWith(langStr.obj.no_license)
        }
        //svrRequest.progress = vwProgressAnimation
        svrRequest.sendDataFinishedCljr = {
            hideEvery(self.vwOldTestDB, self.vwProgressAnimation)
            HsBleMaestr.inst.fmdbHandler.이전데이터삭제()
            HsBleMaestr.inst.readDB()
            isTestOldCase = false
        }
    }
    
    /// Sorting...
    var sortByDate = true, sortAscend = false

    @IBOutlet weak var bttnSortDate: SortBttn!
    @IBOutlet weak var bttnSortName: SortBttn!

    @IBAction func sortTouched(sender: SortBttn) {
        sender.sortTouched()
        sortByDate = sender.accessibilityIdentifier! == "date"
        sortAscend = sender.isAscend
        sortAction()
    }

    func sortFromSearchProtocol(union: [HmStudent]) {
        searchedList = sortStudentArrayBy(sortByDate, ascn: sortAscend, union: studentList)
    }

    func sortAction() {
        HsBleMaestr.inst.arrStudent =
            sortStudentArrayBy(sortByDate, ascn: sortAscend, union: HsBleMaestr.inst.arrStudent)
        sortFromSearchProtocol(studentList)
        reloadTableView()
    }

    /// 아래 Home 버튼은 안 보임.. 기능 없슴.
    @IBAction func bttnGoHomeTouched(sender: UIButton) {
        sender.setImage(UIImage(named: "btn_bottom_home_press"), forState: .Normal)
    }
    
    @IBAction func bttnGoHome(sender: UIButton) { // btn_bottom_home_unpress
        sender.setImage(UIImage(named: "btn_bottom_home_unpress"), forState: UIControlState.Normal)
    }
    
    @IBAction func bttnActQuit(sender: AnyObject) { }

    @IBAction func bttnActInfo(sender: AnyObject) {
        openHeartisenseInfoWeb(1)// Monitor
    }

    @IBAction func bttnActExcel(sender: UIButton) {
        openExcelImportGuide()
    }

    @IBAction func bttnAddStudent(sender: AnyObject) {
        log.logUiAction("   학생   추가  ", lnum: 5)

        if txtName.text?.getLengthUTF32() == 0 {
            HtSimpleUI.ToastWithTitle("....", msg: langStr.obj.empty_name_or_email, cancelTtl: nil)
            return
        }
        if 0 < txtEmail.text!.getLengthUTF32() && !txtEmail.text!.isValidEmail() {
            HtSimpleUI.ToastWithTitle(langStr.obj.email, msg: langStr.obj.not_valid_email, cancelTtl: nil)
            return
        }

        // email validation check
        if HsBleMaestr.inst.addStudent(txtName.text!, em: txtEmail.text!, pw: "pwpw", phn: "", ag: 11) {
            log.printAlways(" Add Student ... OK  \(HsBleMaestr.inst.arrStudent.count) ", lnum: 5)
        } else {
            HtSimpleUI.ToastWithTitle("Woops !!", msg: "Existing Name  !! ", cancelTtl: nil)
            log.printAlways(" Add Student  fail ", lnum: 5)
        }
        view.endEditing(true)
        txtName.text = ""; txtEmail.text = ""
        searchMode = false
        searchUIAnimation()
        sortAction()
        //reloadTableView()
    }

    @IBAction func bttnActDelete(sender: UIButton) {
        deleteTargetedStudents()
        refreshDeleteBttnCheckAllState()
        //refreshDeleteBttn()
        //setCheckAllButtonAppearance(false)
    }

    @IBAction func bttnActCheckall(sender: AnyObject) {
        toggleCheckAll()
    }

    // MARK:  메인으로 돌아가기.
    @IBAction func bttnGo2Home(sender: AnyObject) {
        let altVw = UIAlertView(title: langStr.obj.to_main, message: langStr.obj.want_main, delegate: self,
            cancelButtonTitle: langStr.obj.yes, otherButtonTitles: langStr.obj.no)
        altVw.show()
    }

    //////////////////////////////////////////////////////////////////////     [   데이터 전송     << >> ]

    var alert: PSPDFAlertView?

    @IBAction func bttnActSend2Server(sender: AnyObject) {  //  기록 전송 버튼....
        log.logUiAction("   서버에 업로드  ", lnum: 10)
        //send2Server() //   // 로그인 건너뛰고 debug 시

        let cnt = HsBleMaestr.inst.arrStudent.filter { (std) -> Bool in
            return std.isDataInDB
            }.count
        if 0 < cnt {
            presentSendDataVC()
        } else {
            let actionSheetController: UIAlertController =
                UIAlertController(title: nil, message: langStr.obj.no_data_to_send, preferredStyle: .Alert)
            presentViewController(actionSheetController, animated: viewAnimate, completion: nil)
            HsGlobal.delay(2.0) { () -> () in
                actionSheetController.dismissViewControllerAnimated(viewAnimate, completion: { () -> Void in
                    print(" 연결 성공 뷰 :: dismiss \(langStr.obj.connect_success)")
                })
            }
        }
    }

    private func presentSendDataVC() {
        let vc = SendDataVC(nibName: "SendData", bundle: nil)
        presentViewController(vc, animated: viewAnimate, completion: nil)
    }

    //////////////////////////////////////////////////////////////////////     [   검색 관련    << >> ]
    @IBOutlet weak var constSearchWidth: NSLayoutConstraint!
    @IBOutlet weak var bttnSearch: UIButton!

    @IBAction func bttnActSearch(sender: UIButton) { // 탐색 기능 토글..
        searchMode = !searchMode
        searchUIAnimation()
        if !searchMode {
            view.endEditing(true)
        }
        refreshSearchTextAction( {std in
            return std.name.containsString(self.txtSearch.text!) || std.email.containsString(self.txtSearch.text!)
            }, totalList: HsBleMaestr.inst.arrStudent)
    }

    @IBAction func searchTextChangedAction(sender: UITextField) {
        print("searchTextChangedAction  length : \(sender.text!.getLength())   \(sender.text!.getLengthUTF32())")

        refreshSearchTextAction( {std in
            return std.name.containsString(self.txtSearch.text!) || std.email.containsString(self.txtSearch.text!)
        }, totalList: HsBleMaestr.inst.arrStudent)
        // func refreshSearchTextAction(filterCond: (SomeType)->Bool, totalList: [SomeType] ) {
    }

    @IBAction func nameEmailEditingBegan(sender: UITextField) {
        print(" nameEmailEditingBegan  ")
        searchMode = false
        searchUIAnimation()
        reloadTableView()
    }

    //////////////////////////////////////////////////////////////////////      전체 선택/해제
    // MARK:  Select All <---> None

    func toggleCheckAll() {
        checkSelected = studentList.count != studentList.filter({ (std) -> Bool in
            std.uiTargeted
        }).count

        for std in studentList {
            std.uiTargeted = checkSelected
        }
        reloadTableView()
        setCheckAllButtonAppearance(checkSelected && studentList.count != 0)
        refreshDeleteBttn()
    }

    func refreshDeleteBttnCheckAllState() {
        //log.printThisFNC("refreshTable", comment: "모두 선택되었는지 감시...   ")   //  이거 이름 바꿔야 ???
        var allSelected = studentList.count == studentList.filter({ (std) -> Bool in
            std.uiTargeted
        }).count

        if allSelected {
            if studentList.count == 0 { allSelected = false }
        }

        log.logThis("  All Selected ??  \(allSelected)", lnum: 1)
        setCheckAllButtonAppearance(allSelected)
        refreshDeleteBttn()

        let cnt = HsBleMaestr.inst.arrStudent.filter { (std) -> Bool in
            return std.isDataInDB
            }.count
        if 0 < cnt {
            bttnSend2Server.backgroundColor = colBttnGreen
        } else {
            bttnSend2Server.backgroundColor = colBttnGray
        }
        log.함수차원_출력_End()
    }

    func setCheckAllButtonAppearance(check: Bool) {
        bttnCheckAllBox.setImageAccordingTo(check, trueImg: "btn_check", falseImg: "btn_uncheck")
        bttnCheckAll.setTtitleAccordingTo(check, trueTxt: langStr.obj.all_uncheck, falseTxt: langStr.obj.all_check)
    }

    //////////////////////////////////////////////////////////////////////     [ viewDidLoad   << >> ]
    // MARK:  view will ...  did load ....
    override func viewDidLoad() {
        log.printAlways("\(#function)", lnum: 10)
        var nib = UINib(nibName: "ManageStudentCell", bundle: nil)
        view.backgroundColor = colBttnBrightGray

        if AppIdTrMoTe == 2 {
            nib = UINib(nibName: "ManageStudentCellTest", bundle: nil)
        }
        tableStudent.registerNib(nib, forCellReuseIdentifier: "manageStudentCell")
        HsBleMaestr.inst.readDB()
        setLanguageString()

        /// Search Protocol
        searchTxtField = txtSearch
        searchConstraintWidth = constSearchWidth
    }

    override func viewWillAppear(animated: Bool) {
        log.printThisFNC("viewWillAppear", comment: "studentsWithData = nil   리셋 ..  ")

        HsBleMaestr.inst.readDB()

        txtSearch.hideMe()
        txtSearch.autocorrectionType = .No
        txtName.autocorrectionType = .No
        txtEmail.autocorrectionType = .No
        searchUIAnimation()
        setCheckAllButtonAppearance(false)

        HsBleMaestr.inst.reloadStudentData = self.reloadTableView

        /// sorting initial action
        bttnSortDate.initDateNameState(bttnSortName)
        sortAction()
        
        hideEvery(vwProgressAnimation)

    }

    override func viewDidAppear(animated: Bool) {
        refreshDeleteBttnCheckAllState()
    }

    override func viewWillDisappear(animated: Bool) {
        HsBleMaestr.inst.reloadStudentData = { print(" reloadStudentData empty ") }
    }

    // ////////////////////////////////////////////////////////////////////     Clojures
    // MARK:  Call Back funcs...
    func editStudentPopup(theStdnt: HmStudent) {  // 학생수정  팝업 ...
        let vc = StudentInfo(nibName: "StudentInfo", bundle: nil)
        vc.modalPresentationStyle = .FormSheet // .Popover
        presentViewController(vc, animated: viewAnimate, completion: nil)
        vc.popoverPresentationController?.sourceView = view
        vc.popoverPresentationController?.sourceRect = view.frame

        vc.callBack = {  // DB 수정..
            self.reloadTableView() // tableStudent.reloadData()
            print("  new name ::  \(theStdnt.name)")
            HsBleMaestr.inst.fmdbHandler.updateAStudent(theStdnt)
        }
        vc.setStudent(theStdnt)
    }

    func deleteTargetedStudents() {
        log.printThisFunc(" Delete Button .... touched ", lnum: 10)
        HsBleMaestr.inst.deleteStudents(studentList.filter { (std) -> Bool in
            std.uiTargeted
            })
        self.refreshDeleteBttnCheckAllState()
        refreshSearchTextAction( {std in
            return std.name.containsString(self.txtSearch.text!) || std.email.containsString(self.txtSearch.text!)
            }, totalList: HsBleMaestr.inst.arrStudent)
        //reloadTableView() // tableStudent.reloadData()
    }

    func showGraph(sObj: HmStudent) { // callback 으로 불림.
        log.printAlways("\(#function)   Student Name : \(sObj.name) ")

        HsStudentDataJobs.singltn.setJsonString(sObj)  // 디버깅 용

        let vc = ResultGraphVC(nibName: "ResultGraph", bundle: nil)
        vc.dbStudent = sObj
        presentViewController(vc, animated: viewAnimate, completion: nil)
    }

    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if buttonIndex == 0 {
            self.navigationController?.popViewControllerAnimated(viewAnimate)
        }
    }

    func goHome() {
        self.navigationController?.popToViewController((self.navigationController?.viewControllers[1])!, animated: viewAnimate)
    }

    func refreshDeleteBttn() { // Delete 버튼 숨기는 로직
        
        bttnDelete.show_다음이_참이면(
            0 < studentList.filter({ (std) -> Bool in
                return std.uiTargeted
            }).count)
    }

    // MARK:  언어 세팅.
    func setLanguageString() {
        txtName.placeholder = langStr.obj.name
        txtEmail.placeholder = langStr.obj.email + " (\(langStr.obj.optional))"
        labelStudentInfo.text = langStr.obj.student_info
        bttnCheckAll.setTitle(langStr.obj.all_check, forState: .Normal)
        bttnQuit.setTitle(langStr.obj.prev_gt, forState: .Normal)

        labelTitle.text = langStr.obj.manage_data // ocalStr("adminstudent_title")
        labelAddStudent.text = langStr.obj.add_student
        bttnSend2Server.setTitle(langStr.obj.send_data, forState: UIControlState.Normal)
        bttnDelete.setTitle(langStr.obj.delete, forState: UIControlState.Normal)
        
        lblOldTest.text = langStr.obj.select_old_records_send
        btnOldTestOK.setTitle(langStr.obj.yes, forState: .Normal)
        btnOldTestCancel.setTitle(langStr.obj.cancel, forState: .Normal)
    }
}

