//
//  AssignStudentExt.swift
//  HS Monitor
//
//  Created by Jongwoo Moon on 2016. 9. 1..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation


extension AssignStudentView {

    func refreshSearchTextAction() {
        if !searchMode {
            reloadTableView()
            return
        }
        if 0 < txtSearch.text!.getLength() || 0 < txtSearch.text!.getLengthUTF32() {
            searchedList = HsBleMaestr.inst.arrStudent.filter { (std) -> Bool in
                std.name.containsString(txtSearch.text!) || std.email.containsString(txtSearch.text!)
            }
        } else { // 하나도 없으면 전체 표시..
            searchedList = HsBleMaestr.inst.arrStudent
        }
        reloadTableView()
    }

    //@IBAction func nameEmailEditingBegan(sender: UITextField) { // Search 에서 탈출하는 액션
    func xxescapeFromSearchMode() { // 쓰는 데 없는 것 같음..
        searchMode = false
        searchTextAnimate()
        reloadTableView()
    }

}