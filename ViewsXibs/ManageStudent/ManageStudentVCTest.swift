//
//  ManageStudentVC.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2015. 11. 18..
//  Copyright © 2015년 IMLab. All rights reserved.
//



///   모니터는 다른 파일임....  주의   !!!!!!

import Foundation
import UIKit
import PSAlertView

class ManageStudentVC : UIViewController, UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate {
    var log = HtLog(cName: "ManageStudentVC")

    var arrCell = [ManageStudentCell]()

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelAddStudent: UILabel!
    @IBOutlet weak var bttnSend2Server: UIButton!
    @IBOutlet weak var bttnDelete: UIButton!

    @IBOutlet weak var bttnCheckAll: UIButton!
    @IBOutlet weak var bttnCheckAllBox: UIButton!
    @IBOutlet weak var labelStudentInfo: UILabel!
    @IBOutlet weak var bttnQuit: UIButton!

    @IBOutlet weak var vwTableContainer: UIView!
    // 선택해서 기록 전송.
    // 아이디가 없는 학생
    // 측정 안 한 학생 > 오른쪽 그래프 보는 버튼이 없슴.
    // 측정 하고 안 보낸 학생
    // 서버에 보낸 학생 > 안 보임.
    
    @IBOutlet weak var tableStudent: UITableView!
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!

    @IBAction func bttnGoHomeTouched(sender: UIButton) {
        //sender.setImage(UIImage(named: "btn_bottom_home_press"), forState: .Normal)
    }
    
    @IBAction func bttnGoHome(sender: UIButton) { // btn_bottom_home_unpress
        //sender.setImage(UIImage(named: "btn_bottom_home_unpress"), forState: UIControlState.Normal)
    }

    @IBAction func bttnSend2Server(sender: AnyObject) {  //  기록 전송 버튼....
        log.logUiAction("   서버에 업로드  ", lnum: 10)

        let stntWithDataNum = HsBleMaestr.inst.arrStudent.filter({ (std) -> Bool in
            return std.isDataInDB
        }).count

        if 0 == stntWithDataNum {
            showSimpleMessageWith("No Data")
            return
        }

        //HsBleMaestr.inst.uiTargetResetAllStudent()
        showSendDataVC()
    }


    @IBAction func bttnAddStudent(sender: AnyObject) {
        log.logUiAction("   학생   추가  ", lnum: 5)
        if txtName.text?.getLengthUTF32() == 0 {
            HtSimpleUI.ToastWithTitle("  ", msg: langStr.obj.empty_name_or_email, cancelTtl: nil)
            return
        }

        if 0 < txtEmail.text!.getLengthUTF32() && !txtEmail.text!.isValidEmail() {
            HtSimpleUI.ToastWithTitle(langStr.obj.email, msg: langStr.obj.not_valid_email, cancelTtl: nil)
            return
        }

        // email validation check
        if HsBleMaestr.inst.addStudent(txtName.text!, em: txtEmail.text!, pw: "pwpw", phn: "000-000", ag: 11) {
            log.printAlways(" Add Student ... OK  \(HsBleMaestr.inst.arrStudent.count) ", lnum: 5)
            //tableStudent.reloadData()
        } else {
            //HtSimpleUI.ToastWithTitle("Woops !!", msg: "Existing Name  !! ", cancelTtl: nil)
            log.printAlways(" Add Student  fail ", lnum: 5)
        }
        tableStudent.reloadData()

        view.endEditing(true)
        txtName.text = ""; txtEmail.text = ""
        tableStudent.reloadData()
    }

    @IBAction func bttnActDelete(sender: UIButton) {
        deleteTargetedStudents()
        refreshDeleteBttn()
        setCheckAllButtonAppearance(false)
    }

    @IBAction func bttnActCheckall(sender: AnyObject) {
        toggleCheckAll(nil)
    }

    // MARK:  메인으로 돌아가기.
    @IBAction func bttnGo2Home(sender: AnyObject) {
        let altVw = UIAlertView(title: langStr.obj.to_main, message: langStr.obj.want_main, delegate: self,
                                cancelButtonTitle: langStr.obj.yes, otherButtonTitles: langStr.obj.no)
        altVw.show()
    }

    //////////////////////////////////////////////////////////////////////     [ UI   ViewDidLoad   << >> ]
    //////////////////////////////////////////////////////////////////////      전체 선택/해제
    // MARK:  Select All <---> None
    var checkSelected = false
    var arrStud: [HmStudent] { get { return HsBleMaestr.inst.arrStudent } }
    var studentsWithData: [HmStudent]? // 이거 반복할 필요 없다.
    let stLimit = 50 // 50명만 보여준다.
    var over50: Bool { get { return stLimit < HsBleMaestr.inst.arrStudent.count }}

    private func showSendDataVC() {
        if 0 < instructorID {
            log.logThis("  Has ..  Instructor ID : \(instructorID) ")
            presentSendDataVC()
        } else {
            let vc = LoginVC(nibName: "Login", bundle: nil)
            vc.modalPresentationStyle = .FormSheet // .Popover
            presentViewController(vc, animated: viewAnimate, completion: nil)
            vc.popoverPresentationController?.sourceView = view
            vc.popoverPresentationController?.sourceRect = view.frame
            vc.loginSuccessCallback = {
                self.presentSendDataVC() // 로그인 하고 바로 뷰 열기...
            }
            vc.setLanguageString()
        }
    }

    private func presentSendDataVC() {
        let vc = SendDataVC(nibName: "SendData", bundle: nil)
        presentViewController(vc, animated: viewAnimate, completion: nil)
    }


    func toggleCheckAll(isAll: Bool?) {
        checkSelected = !checkSelected
        if (isAll != nil) { checkSelected = isAll! }

        //for obj in HsBleMaestr.inst.arrStudent {
        var sttIdx = 0
        if over50 { sttIdx = arrStud.count - stLimit }

        for idx in sttIdx...arrStud.count - 1 {
            print(" check all... idx  :  \(idx)")
            let obj = HsBleMaestr.inst.arrStudent[idx]
            obj.uiTargeted = checkSelected
        }
        tableStudent.reloadData()        //HsBleMaestr.inst.read DB()
        setCheckAllButtonAppearance(checkSelected)
        refreshDeleteBttn()
    }

    func refreshTableView() {
        log.printThisFNC("refreshTable", comment: "모두 선택되었는지 감시...    \(HsBleMaestr.inst.numOfUiTargetedStudents())  ==  \(stLimit)")
        if HsBleMaestr.inst.numOfUiTargetedStudents() == stLimit { // HsBleMaestr.inst.arrStudent.count {
            toggleCheckAll(true)
        } else {
            setCheckAllButtonAppearance(false)
        }
        refreshDeleteBttn()
    }

    func setCheckAllButtonAppearance(check: Bool) {
        if check {
            bttnCheckAll.setTitle(langStr.obj.all_uncheck, forState: .Normal)
            bttnCheckAllBox.setImage(UIImage(named: "btn_check"), forState: .Normal)
        } else {
            bttnCheckAll.setTitle(langStr.obj.all_check, forState: .Normal)
            bttnCheckAllBox.setImage(UIImage(named: "btn_uncheck"), forState: .Normal)
        }
    }

    //////////////////////////////////////////////////////////////////////     [ UI   ViewDidLoad   << >> ]

    // MARK:  view will ...  did load ....
    override func viewDidLoad() {
        print("  ManageStudentVC ::  override func viewDidLoad() {")
        var nib = UINib(nibName: "ManageStudentCell", bundle: nil)
        if AppIdTrMoTe == 2 {
            nib = UINib(nibName: "ManageStudentCellTest", bundle: nil)
        }
        tableStudent.registerNib(nib, forCellReuseIdentifier: "manageStudentCell")
        setLanguageString()
    }

    override func viewWillAppear(animated: Bool) {
        log.printThisFNC("viewWillAppear", comment: "studentsWithData = nil   리셋 ..  ")
        studentsWithData = nil
//        HsBleMaestr.inst.readDB()
//        tableStudent.reloadData()

        setCheckAllButtonAppearance(false)
    }

    override func viewDidAppear(animated: Bool) {
        HsBleMaestr.inst.readDB()
        tableStudent.reloadData()
    }

    // ////////////////////////////////////////////////////////////////////     Clojures
    // MARK:  Call Back funcs...
    func editStudentPopup(theStdnt: HmStudent) {  // 학생수정  팝업 ...
        let vc = StudentInfo(nibName: "StudentInfo", bundle: nil)
        vc.modalPresentationStyle = .FormSheet // .Popover
        presentViewController(vc, animated: viewAnimate, completion: nil)
        vc.popoverPresentationController?.sourceView = view
        vc.popoverPresentationController?.sourceRect = view.frame

        vc.callBack = {  // DB 수정..
            self.tableStudent.reloadData()
            print("  new name ::  \(theStdnt.name)")
            HsBleMaestr.inst.fmdbHandler.updateAStudent(theStdnt)
        }
        vc.setStudent(theStdnt)
    }

    func deleteTargetedStudents() {
        HsBleMaestr.inst.deleteStudents()  // 여기서 새로 불러오는 것 까지 수행..
        tableStudent.reloadData()
    }

    func showGraph(sObj: HmStudent) {
        let vc = ResultGraphVC(nibName: "ResultGraph", bundle: nil)
        vc.dbStudent = sObj
        presentViewController(vc, animated: viewAnimate, completion: nil)
    }

    func showTestResultSheet(std: HmStudent) {
        let vc = ResultTotalVC(nibName: "ResultTotalVC", bundle: nil)
        vc.stdnt = std
        presentViewController(vc, animated: viewAnimate, completion: nil)
    }
    
    //////////////////////////////////////////////////////////////////////     [ UI   ViewDidLoad   << >> ]
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if buttonIndex == 0 {
            self.navigationController?.popViewControllerAnimated(viewAnimate)
        }
    }

    func goHome() {
        self.navigationController?.popToViewController((self.navigationController?.viewControllers[1])!, animated: viewAnimate)
    }

    func getStudentsWithData() -> [HmStudent] {
        if studentsWithData == nil {
            studentsWithData = HsBleMaestr.inst.arrStudent.filter({ (std) -> Bool in
                return std.isDataInDB
            })
        }
        return studentsWithData!
    }

    // MARK:  table view delegate, data ..
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //log.printAlways(" number of Rows in Section ...  ", lnum: 5)
        arrCell = [ManageStudentCell]()

        //print("   over50 \(over50)  cnt :  \(HsBleMaestr.inst.arrStudent.count) ")
        if over50 { return stLimit }

        return HsBleMaestr.inst.arrStudent.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        //log.printAlways("  Assign Student : \(indexPath.row) ", lnum: 1)
        let cell:ManageStudentCell = tableView.dequeueReusableCellWithIdentifier("manageStudentCell", forIndexPath:indexPath) as! ManageStudentCell
        var idx = indexPath.row  // 50개 초과 적용..
        if over50 { idx += (HsBleMaestr.inst.arrStudent.count - stLimit) }
        let student = HsBleMaestr.inst.arrStudent[idx]
        cell.setStudent(student)
        cell.deleteTargetedStudent = deleteTargetedStudents
        cell.editTargetedStudent = editStudentPopup
        if AppIdTrMoTe == 2 { cell.showGraph = showTestResultSheet }
        else { cell.showGraph = showGraph }

        cell.bringSubviewToFront(cell.bttnGraph)
        cell.bringSubviewToFront(cell.bttnEdit)
        cell.bringSubviewToFront(cell.bttnDelete)

        if student.uiTargeted == true {
            tableStudent.selectRowAtIndexPath(indexPath, animated: viewAnimate, scrollPosition: .None)
        }

        arrCell.append(cell)
        return cell
    }

    func refreshDeleteBttn() {
        bttnDelete.show_다음이_참이면(HsBleMaestr.inst.numOfUiTargetedStudents() != 0)
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //print("  tableView(tableView: UITableView,       didSelectRowAtIndexPath")
        let curCell = tableStudent.cellForRowAtIndexPath(indexPath) as! ManageStudentCell
        curCell.setSelection(true)
        curCell.curStudent!.uiTargeted = true
        refreshTableView()
    }
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        //print("  tableView(tableView: UITableView,       didDeselectRowAtIndexPath ")
        let curCell = tableStudent.cellForRowAtIndexPath(indexPath) as! ManageStudentCell
        curCell.setSelection(false)
        curCell.curStudent!.uiTargeted = false
        refreshTableView()
    }

    // MARK:  언어 세팅.
    func setLanguageString() {
        txtName.placeholder = langStr.obj.name
        txtEmail.placeholder = langStr.obj.email + " (\(langStr.obj.optional))"
        labelStudentInfo.text = langStr.obj.student_info
        bttnCheckAll.setTitle(langStr.obj.all_check, forState: .Normal)
        bttnQuit.setTitle(langStr.obj.quitArrow, forState: .Normal)

        labelTitle.text = langStr.obj.manage_data
        labelAddStudent.text = langStr.obj.add_student
        bttnSend2Server.setTitle(langStr.obj.send_data, forState: UIControlState.Normal)
        bttnDelete.setTitle(langStr.obj.delete, forState: UIControlState.Normal)
    }

}