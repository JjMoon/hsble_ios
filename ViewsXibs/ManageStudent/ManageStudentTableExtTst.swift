//
//  ManageStudentTableExtTst.swift
//  HS Test
//
//  Created by Jongwoo Moon on 2016. 9. 1..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation


extension ManageStudentVC : UITableViewDataSource, UITableViewDelegate {

    func reloadTableView() {
        if UI_USER_INTERFACE_IDIOM() == .Phone {
            tableStudent.rowHeight = 30;
        }
        bttnSearch.setImageAccordingTo(searchMode, trueImg: "search_close.png", falseImg: "search.png")
        refreshDeleteBttnCheckAllState()
        tableStudent.reloadData()
    }

    // MARK:  table view delegate, data ..
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //log.printAlways(" number of Rows in Section ...  ", lnum: 5)
        arrCell = [ManageStudentCell]()

        return studentList.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        //log.printAlways("  Assign Student : \(indexPath.row) ", lnum: 1)
        let cell:ManageStudentCell = tableView.dequeueReusableCellWithIdentifier("manageStudentCell", forIndexPath:indexPath) as! ManageStudentCell
        let idx = indexPath.row  // 50개 초과 적용..
        //if over50 { idx += (HsBleMaestr.inst.arrStudent.count - stLimit) }
        let student = studentList[idx]
        cell.setStudent(student)
        cell.deleteTargetedStudent = deleteTargetedStudents
        cell.editTargetedStudent = editStudentPopup
        if AppIdTrMoTe == 2 { cell.showGraph = showTestResultSheet }
        else { cell.showGraph = showGraph }

        cell.bringSubviewToFront(cell.bttnGraph)
        cell.bringSubviewToFront(cell.bttnEdit)
        cell.bringSubviewToFront(cell.bttnDelete)

        cell.backgroundColor = colBttnBrightGray

        if student.uiTargeted {
            tableStudent.selectRowAtIndexPath(indexPath, animated: viewAnimate, scrollPosition: .None)
        }

        arrCell.append(cell)
        return cell
    }

    func showTestResultSheet(std: HmStudent) {
        let vc = ResultTotalVC(nibName: "ResultTotalVC", bundle: nil)
        vc.stdnt = std
        presentViewController(vc, animated: viewAnimate, completion: nil)
    }

    // 테스트에 원래 있던 함수.. 폐기
    //func refreshDeleteBttn() { bttnDelete.show_다음이_참이면(HsBleMaestr.inst.numOfUiTargetedStudents() != 0) }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //print("  tableView(tableView: UITableView,       didSelectRowAtIndexPath")
        let curCell = tableStudent.cellForRowAtIndexPath(indexPath) as! ManageStudentCell
        curCell.setSelection(true)
        curCell.curStudent!.uiTargeted = true
        refreshDeleteBttnCheckAllState()
    }

    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        //print("  tableView(tableView: UITableView,       didDeselectRowAtIndexPath ")
        let curCell = tableStudent.cellForRowAtIndexPath(indexPath) as! ManageStudentCell
        curCell.setSelection(false)
        curCell.curStudent!.uiTargeted = false
        refreshDeleteBttnCheckAllState()
    }

}
