//
//  ManageStudentOverTest.swift
//  HS Test
//
//  Created by Jongwoo Moon on 2016. 9. 1..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation


class ManageStudentOver: ManageStudentVC {

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        // data check
        let dbHandle = HsBleMaestr.inst.fmdbHandler
        if dbHandle.이전_Test_테이블이_있는지() && dbHandle.이전_DB_전송데이터_있는지(false) {
            print("exsit infant1")
            vwOldTestDB.showMe()
            
        } else {
            vwOldTestDB.hideMe()
        }
    }
}


extension ManageStudentCell {

    func setStudent(stu: HmStudent) {
        curStudent = stu
        labelName.text = curStudent?.name
        labelEmail.text = curStudent?.email
        bttnGraph.show_다음이_참이면(curStudent?.isDataInDB)
        setSelection((curStudent?.uiTargeted)!)

        if AppIdTrMoTe == 1 { return }

        labelStandard.text = curStudent!.getStandardStr()

        iconAdult.setImage(UIImage(named: "icon_adult_none.png"), forState: .Normal)
        if curStudent!.finishedAdult {
            if curStudent!.passAdult {
                iconAdult.setImage(UIImage(named: "icon_adult_pass.png"), forState: .Normal)
            } else {
                iconAdult.setImage(UIImage(named: "icon_adult_fail.png"), forState: .Normal)
            }
        }
        iconInfant.setImage(UIImage(named: "icon_infant_none.png"), forState: .Normal)
        if curStudent!.finishedInfant {
            let inf = curStudent!.getInfantFromDB()

            //print("  \(curStudent?.name)   pass??? \(inf?.passRes1)  \(inf?.passRes2) ")
            //print("curStudent!.finishedInfant {")

            //if inf!.passRes1 && inf!.passRes2 {
            if curStudent!.passInfant {
                //print("curStudent!.finishedInfant {    pass")
                iconInfant.setImage(UIImage(named: "icon_infant_pass.png"), forState: .Normal)
            } else {
                //print("curStudent!.finishedInfant {    fail  ...")
                iconInfant.setImage(UIImage(named: "icon_infant_fail.png"), forState: .Normal)
            }
        }
    }
    
}
