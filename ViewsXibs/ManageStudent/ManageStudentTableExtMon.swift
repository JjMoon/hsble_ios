//
//  ManageStudentTableExtMon.swift
//  HS Monitor
//
//  Created by Jongwoo Moon on 2016. 9. 1..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation


extension ManageStudentVC : UITableViewDataSource, UITableViewDelegate {
    func reloadTableView() {
        if UI_USER_INTERFACE_IDIOM() == .Phone {
            tableStudent.rowHeight = 30;
        }
        bttnSearch.setImageAccordingTo(searchMode, trueImg: "search_close.png", falseImg: "search.png")
        refreshDeleteBttn()
        tableStudent.reloadData()
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //print("  tableView(tableView: UITableView,       didSelectRowAtIndexPath")
        view.endEditing(true)
        let curCell = tableStudent.cellForRowAtIndexPath(indexPath) as! ManageStudentCell
        curCell.setSelection(true)
        curCell.curStudent!.uiTargeted = true
        refreshDeleteBttnCheckAllState()
    }

    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        //print("  tableView(tableView: UITableView,       didDeselectRowAtIndexPath ")
        let curCell = tableStudent.cellForRowAtIndexPath(indexPath) as! ManageStudentCell
        view.endEditing(true)
        curCell.setSelection(false)
        curCell.curStudent!.uiTargeted = false
        refreshDeleteBttnCheckAllState()
    }

    // MARK:  table view delegate, data ..
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //log.printAlways(" number of Rows in Section ...  ", lnum: 5)
        arrCell = [ManageStudentCell]()
        //if over50 { return stLimit }
        return studentList.count // HsBleMaestr.inst.arrStudent.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        //log.printAlways("  Assign Student : \(indexPath.row) ", lnum: 1)
        let cell:ManageStudentCell =
            tableView.dequeueReusableCellWithIdentifier("manageStudentCell", forIndexPath:indexPath)
                as! ManageStudentCell
        let student = studentList[indexPath.row]
        cell.setStudent(student)
        cell.deleteTargetedStudent = deleteTargetedStudents
        cell.editTargetedStudent = editStudentPopup
        //if AppIdTrMoTe == 2 { cell.showGraph = showTestResultSheet }  // Test
        cell.showGraph = showGraph
        cell.bringSubviewToFront(cell.bttnGraph)
        cell.bringSubviewToFront(cell.bttnEdit)
        cell.bringSubviewToFront(cell.bttnDelete)
        cell.backgroundColor = colBttnBrightGray
        if student.uiTargeted == true {
            tableStudent.selectRowAtIndexPath(indexPath, animated: viewAnimate, scrollPosition: .None)
        }
        arrCell.append(cell)
        return cell
    }
}
