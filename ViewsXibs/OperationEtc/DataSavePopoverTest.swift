//
//  Login.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2015. 12. 17..
//  Copyright © 2015년 IMLab. All rights reserved.
//

import Foundation

class DataSavePopoverVC: HtViewController, UITableViewDataSource, UITableViewDelegate { //, UIAlertViewDelegate {
    // admin@imlabworld.com   nimda  admin140123
    var log = HtLog(cName: "DataSavePopover")
    var arrUiObj = HsUIArray()
    
    @IBOutlet weak var theTable: UITableView!
    @IBOutlet weak var bttnSaveAll: HtBoxButton!

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelCautionMsg: UILabel!

    @IBAction func bttnActClose(sender: UIButton) {
        closeAction()
    }

    var saveAllOrCloseAction: Void -> Void = { }
    @IBAction func bttnActSaveAll(sender: HtBoxButton) {
        saveAllOrCloseAction()
        setAllSaveButton()
    }

    override func viewDidLoad() {
        saveAllOrCloseAction = { // 처음에는 모두 저장 기능...
            for aCell in self.cellArr {
                aCell.saveAction()
            }
        }
    }

    override func viewWillDisappear(animated: Bool) {

    }

    override func viewDidAppear(animated: Bool) {
        if !testInfant {
            super.viewDidAppear(animated)
        }
        setAllSaveButton()
        liveKitNum = HsBleMaestr.inst.connectionNumber()
    }

    var liveKitNum = 0
    override func update() {
        if liveKitNum != HsBleMaestr.inst.connectionNumber() { closeAction() }
    }

    /// 셀 버튼에서 부르는 콜백.
    func setAllSaveButton() { // 셀에서 콜백으로 불림...  버튼 모양 기능 변경..
        log.printThisFNC("setAllSaveButton", comment: "  allConfirmed : \(arrUiObj.allConfirmed) ")

//        for aCell in cellArr {
//            if aCell.isDataRemainedToSave { return }
//        }

        if !arrUiObj.allConfirmed { return }

        // 녹색 'Close' 로 변경...
        bttnSaveAll.setTitleAndBackground(HmGraphSetting.inst.bttnBgGreen, ttlStr: langStr.obj.close)
        saveAllOrCloseAction = { self.dismissViewControllerAnimated(viewAnimate, completion: nil) }
    }

    func closeAction() {
        dismissViewControllerAnimated(viewAnimate, completion: nil)
    }

    //////////////////////////////////////////////////////////////////////     _//////////_     [ UI      << >> ]    _//////////_   Table View
    // MARK: Table View
    var cellArr = [DataSavePopCell]()

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4 // HsBleMaestr.inst.connectionNumber()
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        //print ("DataSavePopoverTest >> cellForRowAtIndexPath   \(indexPath.row)")
        let bleObj = HsBleMaestr.inst.arrBleSuMan[indexPath.row]
        var cell:DataSavePopCell? = tableView.dequeueReusableCellWithIdentifier("dataSavePopCell") as? DataSavePopCell
        if cell == nil {
            addTableCellNib()
            cell = tableView.dequeueReusableCellWithIdentifier("dataSavePopCell") as? DataSavePopCell
        }
        cell?.arrUiObj = self.arrUiObj
        print("\n DataSavePopover 테이블에서 Cell 에 할당..  ble : \(bleObj) : \(bleObj.name)")
        cell?.setItems(bleObj)
        //cell?.setItems(arrStudent[indexPath.row])
        cell?.bringSubviewToFront(cell!.bttnDataSave1)
        cell?.bringSubviewToFront(cell!.bttnDataSave2)
        cell?.tableCallBackVoid = { self.setAllSaveButton() }
        cellArr.append(cell!)
        return cell!
    }

    func addTableCellNib() {
        let nib = UINib(nibName: "DataSavePopCell", bundle: nil)
        theTable.registerNib(nib, forCellReuseIdentifier: "dataSavePopCell")
    }

    // MARK:  언어 세팅.
    func setLanguageString() {
        log.printThisFunc("setLanguageString")
        labelTitle.text = langStr.obj.save_record
        labelCautionMsg.text = langStr.obj.save_warning
        bttnSaveAll.setTitle(langStr.obj.save_all, forState: .Normal)
    }
}

