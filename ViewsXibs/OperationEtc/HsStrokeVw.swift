//
//  HsStrokeVw.swift
//  Trainer
//
//  Created by Jongwoo Moon on 2016. 10. 12..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation
import UIKit

class HsStrokeVw: UIView {

    @IBOutlet weak var lblYellow: UILabel!
    @IBOutlet weak var lblGreen: UILabel!
    @IBOutlet weak var lblLowerYellow: UILabel!
    @IBOutlet weak var lblGray: UILabel!

    var consYlw: NSLayoutConstraint!, consGrn: NSLayoutConstraint!
    var consBar: NSLayoutConstraint!

    func setConstraint(yRto: CGFloat, gRto: CGFloat) {
        setColorBars(yRto, gRto: gRto)
        setNeedsLayout()
        setBarD(0)
    }

    func setCompBrth(yRto: CGFloat, gRto: CGFloat) {
        self.removeConstraint(consYlw)
        self.removeConstraint(consGrn)
        setColorBars(yRto, gRto: gRto)
    }

    private func setColorBars(yRto: CGFloat, gRto: CGFloat) {
        consYlw = NSLayoutConstraint(item: lblYellow, attribute: .Width, relatedBy: .Equal, toItem: self, attribute: .Width, multiplier: yRto, constant: 0)
        self.addConstraint(consYlw)
        consGrn = NSLayoutConstraint(item: lblGreen, attribute: .Width, relatedBy: .Equal, toItem: self, attribute: .Width, multiplier: gRto, constant: 0)
        self.addConstraint(consGrn)
    }

    func setBarD(val: Double) { // 0.5
        let pos = CGFloat(val) * self.frame.width
        let wid: CGFloat = CGFloat(1 - val) * self.frame.width
        let yfr = lblLowerYellow.frame

        //print("  pos: wid   \(pos)   \(wid)     \(yfr.origin.y)   \(yfr.height)   \(pos + wid)")

        lblGray.frame = CGRect(origin: CGPoint(x: pos, y: yfr.origin.y), size: CGSize(width: wid, height: yfr.height))
    }

}
