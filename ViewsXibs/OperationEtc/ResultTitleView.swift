//
//  ResultTitleView.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2015. 12. 22..
//  Copyright © 2015년 IMLab. All rights reserved.
//

import Foundation

class ResultTitleView : HtView {
    var log = HtLog(cName: "ResultTitleView")
    var openSendDataViewCljr: () -> () = { }

    @IBOutlet weak var labelResultTitle: UILabel!
    @IBOutlet weak var labelResultContent: UILabel!
    @IBOutlet weak var labelNumbers: UILabel!
    @IBOutlet weak var bttnSaveRecords: UIButton!

    @IBAction func bttnActSaveRecords(sender: AnyObject) {

        print("func bttnActSaveRecords")
        if HsBleMaestr.inst.allTesterEvaluated() {
            openSendDataViewCljr()
        } else {
            HtSimpleUI.ToastWithTitle(nil, msg: langStr.obj.all_check_need, cancelTtl: nil)
        }
    }

    override func initSet() {
        super.initSet()
        if AppIdTrMoTe == 2 { testSetting() }
    }

    func testSetting() {
        labelNumbers.hideMe()
    }

    override func setLanguageString() {
        labelNumbers.text = langStr.obj.result_guide
        bttnSaveRecords.localizeSizeAlignment(2)
        bttnSaveRecords.setTitle(langStr.obj.save_record, forState: .Normal)
    }

}
