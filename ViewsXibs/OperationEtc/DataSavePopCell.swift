//
//  DataSavePopCell.swift
//  Test
//
//  Created by Jongwoo Moon on 2016. 2. 28..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation

// MARK:  Test 에만 적용되는 셀임..
/** 
  테스트 오퍼레이션 후 저장 테이블의 셀...  Test App Only ..

*/
class DataSavePopCell: HtTableViewCell {
    var bleMan : HsBleSuMan?, student1 : HmStudent?, student2 : HmStudent?
    var arrUiObj = HsUIArray()

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelName1: UILabel!
    @IBOutlet weak var bttnDataExist1: HtBoxButton!
    @IBOutlet weak var bttnDataExist2: HtBoxButton!

    @IBOutlet weak var bttnDataSave1: HtBoxButton!
    @IBOutlet weak var bttnDataSave2: HtBoxButton!
    @IBOutlet weak var labelName2: UILabel!
    @IBOutlet var arrSaveBttns: [HtBoxButton]!

    var arrExists = [HtBoxButton]()

    /// 두개 모두가 저장 안됐으면 true
    var isDataRemainedToSave: Bool { get { return isActive && (data1Remain || data2Remain) } }
    /// 접속이 되어 있고.. 학생이 있으면 Active..
    var isActive: Bool { get {
        if isAdult { return  (bleMan?.isConnected) == true && isThereAnyStudent
        } else {     return isThereAnyStudent } } }

    var isThereAnyStudent: Bool { get { return bleMan!.curStudent != nil || bleMan!.curStdntSub != nil } }
    /// 학생이 없으면 남은 데이터 없슴 : false
    var data1Remain: Bool { get { return bleMan!.curStudent != nil && student1!.dataExistOfResc(true)
        //!bleMan!.dataSaved
        }}
    var data2Remain: Bool { get { return bleMan!.curStdntSub != nil && student1!.dataExistOfResc(false)
        //!bleMan!.subDataSaved
        }}


    func isDataExistsOrSavedThisTime(student: HmStudent, is1st: Bool) -> Bool {
        return student.dataExistOfResc(is1st) || arrUiObj.isConfirmedOf(student)
    }

    func isNotSavedYetThisTime(student: HmStudent) -> Bool {
        return !arrUiObj.isConfirmedOf(student)
    }

    // /////////////////////////////////////////////////////////////////        초기 설정..

    /// 테이블 생성시 설정..
    func setItems(suMan: HsBleSuMan) {
        bleMan = suMan

        if isAdult && !bleMan!.isConnectionTotallyFinished { // 연결 유실..  나중에 이놈을 init 로 실행하게 하면 좋겠다.
            labelTitle.text = bleMan?.name
            labelName1.text = "-"; labelName2.text = "-"
            bttnDataExist1.hideMe(); bttnDataSave1.hideMe()
            bttnDataExist2.hideMe(); bttnDataSave2.hideMe()
            return
        }
        //print(" setItems : name : \(bleMan?.name)")
        setLanguageString()
        student1 = bleMan?.curStudent
        student2 = bleMan?.curStdntSub
        //print(" setItems : >>> \(labelTitle.text) ::  Student  \(student1)  \(student2)  ")
        labelTitle.text = bleMan?.name
        setInitialData()
        arrSaveBttns = [ bttnDataExist1, bttnDataExist2 ]
        //print("  >>> \(labelTitle.text) ::  Student  \(student1)  \(student2)  ")
    }

    func setInitialData() {
        //let testObj = bleMan!.testObj
        if student1 == nil { // 학생 할당 없슴.
            labelName1.text = "-"
            bttnDataExist1.hideMe(); bttnDataSave1.hideMe()
        } else {
            labelName1.text = student1!.name

            /// 요건 학생한테 물어보는 게 편하다...
            if !isNotSavedYetThisTime(student1!) { setSaved(bttnDataSave1) }
            if isDataExistsOrSavedThisTime(student1!, is1st: true) {
                changeState2dataExists(bttnDataExist1)
            } else { // 학생은 있으나 데이터도 없고, 아직 저장도 안한 상태.
                bttnDataExist1.setTextColor(HmGraphSetting.inst.bttnBgGreen)
                bttnDataExist1.setTitle("-", forState: .Normal)
            }
        }
        if student2 == nil {
            labelName2.text = "-"
            bttnDataExist2.hideMe(); bttnDataSave2.hideMe()
        } else {
            labelName2.text = student2!.name

            if !isNotSavedYetThisTime(student2!) { setSaved(bttnDataSave2) }

            if isDataExistsOrSavedThisTime(student2!, is1st: false) {
                changeState2dataExists(bttnDataExist2)
            } else {
                bttnDataExist2.setTextColor(HmGraphSetting.inst.bttnBgGreen)
                bttnDataExist2.setTitle("-", forState: .Normal)
            }
        }
    }

    @IBAction func bttnActSave1(sender: AnyObject) {
        // Save to DB
        //print("\n\n  func bttnActSave 1 _____(sender: AnyObject)  \n\n")
        saveToDatabase(true)
        arrUiObj.confirmOfStudent(student1!)
        setInitialData()
        setSaved(sender as! HtBoxButton)
        changeState2dataExists(bttnDataExist1)
    }

    @IBAction func bttnActSave2(sender: AnyObject) {
        // Save to DB
        //print("\n\n  func bttnActSave 2 (sender: AnyObject)  \n\n")
        saveToDatabase(false)
        arrUiObj.confirmOfStudent(student2!)
        setInitialData()
        setSaved(sender as! HtBoxButton)
        changeState2dataExists(bttnDataExist2)
    }

    /// Ble 의 dataSaved . 등은 ble 에서 체크함.
    func saveAction() { // 모두 저장하는 함수..  DataSavePopoverVC 에서 불림...
        //print(" Save All 이 눌린 후 불리는 함수....  \(student1?.name)  || \(student2?.name)")
        if student1 != nil && isNotSavedYetThisTime(student1!) {
            print("   \t\t student1 != nil")
            saveToDatabase(true)
            setSaved(bttnDataSave1)
            changeState2dataExists(bttnDataExist1)
            arrUiObj.confirmOfStudent(student1!)
        }
        if student2 != nil && isNotSavedYetThisTime(student2!) {
            //print("   \t\t student2 != nil")
            saveToDatabase(false)
            setSaved(bttnDataSave2)
            changeState2dataExists(bttnDataExist2)
            arrUiObj.confirmOfStudent(student2!)
        }
        setInitialData()
    }

    func saveToDatabase(is1st: Bool) {
        if isAdult { bleMan!.saveAdultRescure(is1st)  }
        else { bleMan!.saveInfantRescure(is1st) }
    }


    /// 모든 버튼을 '저장됨' 으로 교체..
    func setAllSaveBttnSaved() {
        for btn in arrSaveBttns { setSaved(btn) }
    }

    func setSaved(bttn: HtBoxButton) {
        //print("  setSaved :: \(bttn.titleLabel?.text)  )")
        bttn.makeNormalText(nil, txtCol: colorLabelGreen)
        bttn.setTitle(langStr.obj.saved, forState: .Normal) // 텍스트 ..
        bttn.enabled = false
        //print("  setSaved   after   :: \(bttn.titleLabel?.text)")
        tableCallBackVoid()
    }

    func changeState2dataExists(bttn: HtBoxButton) {
        //print ("\n  Records exist :: \(bttn.titleLabel?.text)  ")
        bttn.setTextColor(HmGraphSetting.inst.warningRed)
        bttn.setTitle(langStr.obj.save_exist, forState: .Normal)
    }

    // MARK:  언어 세팅.
    func setLanguageString() {
        bttnDataSave1.localizeSizeAlignment(1)
        bttnDataSave2.localizeSizeAlignment(1)
        bttnDataExist1.localizeSizeAlignment(1)
        bttnDataExist2.localizeSizeAlignment(1)
        bttnDataSave1.setTitle(langStr.obj.save, forState: .Normal)
        bttnDataSave2.setTitle(langStr.obj.save, forState: .Normal)
    }

}