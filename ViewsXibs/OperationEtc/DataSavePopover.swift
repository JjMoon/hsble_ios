//
//  Login.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2015. 12. 17..
//  Copyright © 2015년 IMLab. All rights reserved.
//

import Foundation

class DataSavePopoverVC: HtViewController, UITableViewDataSource, UITableViewDelegate { //, UIAlertViewDelegate {
    // admin@imlabworld.com   nimda

    var log = HtLog(cName: "DataSavePopover")
    var saveAllOrCloseAction: Void -> Void = { }
    var cellArr = [DataAfterOperationCell]()

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelCautionMsg: UILabel!
    @IBOutlet weak var theTable: UITableView!
    @IBOutlet weak var bttnSaveAll: HtBoxButton!

    @IBAction func bttnActClose(sender: UIButton) {
        closeAction()
    }

    @IBAction func bttnActSaveAll(sender: HtBoxButton) {
        saveAllOrCloseAction()
        setAllSaveButton()
    }

    override func viewDidLoad() {
        bttnSaveAll.setTitleAndBackground(colorBtnBgGray, ttlStr: langStr.obj.save_all) // 처음은 '모두 저장'
        saveAllOrCloseAction = { // 처음에는 모두 저장 기능...
            for aCell in self.cellArr {
                aCell.saveAction()
            }
        }
    }
//
//    override func viewDidAppear(animated: Bool) { // 테이블 뷰가 세팅 된 다음에 실행해야 함.
//        setAllSaveButton()
//    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        setAllSaveButton()
        liveKitNum = HsBleMaestr.inst.connectionNumber()
    }

    var liveKitNum = 0
    override func update() {
        if liveKitNum != HsBleMaestr.inst.connectionNumber() { closeAction() }
    }

    func setAllSaveButton() { // 셀에서 콜백으로 불림...  버튼 모양 기능 변경..
        for aCell in cellArr {
            //print("  cell isThereDataToSave ::  \(aCell.isThereDataToSave) ")
            if aCell.isThereDataToSave { return }  // 여기서 모든 셀이 저장할 것이 없으면..
        }
        // 버튼 모양 및 기능 변경     'Close' 로 변경...
        bttnSaveAll.setTitleAndBackground(HmGraphSetting.inst.bttnBgGreen, ttlStr: langStr.obj.close)
        saveAllOrCloseAction = { self.closeAction() }
    }

    func closeAction() {
        dismissViewControllerAnimated(viewAnimate, completion: nil)
    }

    //////////////////////////////////////////////////////////////////////     _//////////_     [ UI      << >> ]    _//////////_   Table View
    // MARK: Table View

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4 // HsBleMaestr.inst.connectionNumber()
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        //print ("SendData >> cellForRowAtIndexPath   \(indexPath.row)")
        let bleObj = HsBleMaestr.inst.arrBleSuMan[indexPath.row]
        var cell:DataAfterOperationCell? = tableView.dequeueReusableCellWithIdentifier("dataAfterOperationCell") as? DataAfterOperationCell
        if cell == nil {
            addTableCellNib()
            cell = tableView.dequeueReusableCellWithIdentifier("dataAfterOperationCell") as? DataAfterOperationCell
        }
        cell?.setItems(bleObj)
        //cell?.setItems(arrStudent[indexPath.row])
        cell?.bringSubviewToFront(cell!.bttnDataSave)
        cell?.tableCallBackVoid = { self.setAllSaveButton() }
        cellArr.append(cell!)
        return cell!
    }

    func addTableCellNib() {
        let nib = UINib(nibName: "DataAfterOperationCell", bundle: nil)
        theTable.registerNib(nib, forCellReuseIdentifier: "dataAfterOperationCell")
    }

    // MARK:  언어 세팅.
    func setLanguageString() {
        log.printThisFunc("setLanguageString")
        labelTitle.text = langStr.obj.save_record
        labelCautionMsg.text = langStr.obj.save_warning
        bttnSaveAll.setTitle(langStr.obj.save_all, forState: .Normal)
    }
}

    //////////////////////////////////////////////////////////////////////     [ UI      << >> ]    _//////////_   DataAfterOperationCell
class DataAfterOperationCell: HtTableViewCell { // 모니터에만 해당.. 테스터는 따로..
    var bleMan : HsBleSuMan?

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var bttnDataExist: HtBoxButton!
    @IBOutlet weak var bttnDataSave: HtBoxButton!

    //var dataSaved = false
    //var isThereDataToSave: Bool { get { return isActive && !bleMan!.dataSaved } } // 저장 안 한 데이터 있슴.
    //var isThereDataToSave: Bool { get { return isActive && !bleMan!.dataSaved && !existDataInDB } }
    var isThereDataToSave: Bool { get { return isActive && !bleMan!.dataSaved } } // 저장 안 한 데이터 있슴. 160718  아랫게 왜 필요하지?
    var existDataInDB: Bool { get {
        return (bleMan!.curStudent != nil && bleMan!.curStudent!.isDataInDB) || bleMan!.dataSaved
        }}
    var isActive: Bool { get { return  (bleMan?.isConnected) == true && bleMan?.curStudent != nil } }

    func setItems(suMan: HsBleSuMan) {  ////  데이터를 한번 저장한 놈은 ble SuMan 에 두자..
        bleMan = suMan
        labelTitle.text = bleMan?.nick
        if isActive {
            labelName.text = bleMan?.curStudent?.name

            if existDataInDB { // 기존 데이터가 있거나 이미 저장 했거나..
                changeState2dataExists()
            } else {
                bttnDataExist.setTitle("-", forState: .Normal)
            }
            //bttnDataSave.backgroundColor = HmGraphSetting.inst.bttnBgGreen
            if bleMan!.dataSaved { setSaved() }
        } else {
            labelName.text = "-"
            bttnDataExist.hideMe()
            bttnDataSave.hideMe()
        }
        //print("  isactiv : \(isActive)   데이터 ? : \(!bleMan!.dataSaved) ")
        setLanguageString()
    }
    
    @IBAction func bttnActSave(sender: AnyObject) {
        saveAction()        // Save to DB
    }

    func saveAction() {
        if bleMan?.curStudent == nil { return }
        HsBleMaestr.inst.fmdbHandler.saveDataAfterEntireProcess((bleMan?.curStudent)!) ////// 데이터 저장 ....
        bleMan!.dataSaved = true
        setSaved()
        changeState2dataExists()
        tableCallBackVoid()
    }

    func setSaved() {
        // Change UI  State ..
        bttnDataSave.makeNormalText(nil, txtCol: colorLabelGreen)
        bttnDataSave.setTitle(langStr.obj.saved, forState: .Normal) // 텍스트 ..
        bttnDataSave.enabled = false
    }

    func changeState2dataExists() { // Records exist  빨간색...
        bttnDataExist.setTextColor(HmGraphSetting.inst.warningRed)
        bttnDataExist.setTitle(langStr.obj.save_exist, forState: .Normal)
    }

    // MARK:  언어 세팅.
    func setLanguageString() {
        //bttnDataExist.setTitle(langStr.obj.save_exist, forState: .Normal)
        bttnDataSave.setTitle(langStr.obj.save, forState: .Normal)
    }
}


