//
//  SelectEntireStep.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2015. 11. 26..
//  Copyright © 2015년 IMLab. All rights reserved.
//

import Foundation

/// 모니터 / 테스트 공통..
class SelectEntireStep : UIView {
    var motherView: OperationMainVC?
    
    @IBOutlet weak var imgStep: UIImageView!
    @IBOutlet weak var imgEntire: UIImageView!
    @IBOutlet weak var btnStepByStep: UIButton!
    @IBOutlet weak var btnEntireProcess: UIButton!

    @IBOutlet weak var labelComment: UILabel!

    @IBAction func btnTouchDownStep(sender: AnyObject) {
        //btnStepByStep.setBackgroundImage(UIImage(named: "btn_stepbystep_select"), forState: UIControlState.Normal)
        //print("btnTouchDownStep(sender: AnyObject) ")
        imgStep.image = UIImage(named: "btn_stepbystep_select")
    }
    
    @IBAction func btnTouchExitStep(sender: AnyObject) {
        print("btnTouchExitStep(sender: AnyObject) ")
        //btnStepByStep.setBackgroundImage(UIImage(named: "btn_stepbystep_unselect"), forState: UIControlState.Normal)
        imgStep.image = UIImage(named: "btn_stepbystep_unselect")
    }
    
    @IBAction func btnTouchDownEntire(sender: AnyObject) {
        //btnEntireProcess.setBackgroundImage(UIImage(named: "btn_wholestep_select"), forState: UIControlState.Normal)
        imgEntire.image = UIImage(named: "btn_wholestep_select")
    }
    
    @IBAction func btnTouchExitEntire(sender: AnyObject) {
        //btnEntireProcess.setBackgroundImage(UIImage(named: "btn_wholestep_unselect"), forState: UIControlState.Normal)
        imgEntire.image = UIImage(named: "btn_wholestep_unselect")
    }
    
    @IBAction func btnActStep(sender: AnyObject) {
        motherView?.stepByStepStart()
        self.hidden = true
    }

    @IBAction func btnActEntire(sender: AnyObject) {
        motherView?.entireProcessStart()
        self.hidden = true
    }
    
    // MARK:  언어 세팅.
    func setLanguageString() {
        btnStepByStep.localizeSizeAlignment(2, topSpace: 0, btmSpace: 0) // 이미 여백이 있다..
        btnEntireProcess.localizeSizeAlignment(2, topSpace: 0, btmSpace: 0)

        btnStepByStep.setTitle(langStr.obj.stepbystep2line, forState: UIControlState.Normal)
        btnEntireProcess.setTitle(langStr.obj.wholestep_2line, forState: UIControlState.Normal)
        labelComment.text = langStr.obj.select_practice_mode
    }
}