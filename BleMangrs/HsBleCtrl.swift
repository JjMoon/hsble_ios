//
//  HsBleCtrl.swift
//  Trainer
//
//  Created by Jongwoo Moon on 2016. 10. 24..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation
import UIKit
import CoreBluetooth


enum ConnState {
    case None, Scanning, OpenStart, Connected, BasicJobStarted, Cali, CaliFinished, Finished, Rest, Error, ConnectFail
}

enum InstructState {
    case StandBy, Step, Entire
}

class ReceivedPack: NSObject {
    var bytes = [UInt8]()
    var len = 0, command = 0, msg = 0, val4 = 0

    func ackCheck(cmd: Int?, mesg: Int?, v4: Int?) -> Bool {
        if cmd != nil {
            if cmd! != command { return false }
        }
        if mesg != nil {
            if mesg! != msg { return false }
        }
        if v4 != nil {
            if v4! != val4 { return false }
        }
        print("\n\n  ReceivedPack :: ackCheck  >>>>>>>  ACK Checked !!!     \(cmd)    \(mesg)    \(v4)   \n\n  ")
        return true
    }

    var ackCheck : (Void -> Bool)?

    func operationStart() -> Bool {
        return ackCheck(9, mesg: 0, v4: nil) // return len == 0x88 && command == 9 && msg == 0
    }
    func operationStop() -> Bool  { return len == 0x88 && command == 9 && msg == 1  }
    func bypass() -> Bool  { return command == 17 }
    func allwaysTrue() -> Bool  { return true }
    func monitorScreenAck() -> Bool  { return ackCheck(17, mesg: 2, v4: 18) }
}


//////////////////////////////////////////////////////////////////////     [  << Hs Ble Controller  >> ]
class HsBleCtrl: NSObject {
    var log = HtLog(cName: "% HsBleCtrl %")

    var bypass = HsBypassManager()
    var calcManager = HsCalcManager()
    var dataObj = HsData()

    var packMchn = HtPacketMachine(interval: 0.1) // packMchn: HtPacketMachine?

    var messageName: String?, stage = 0, stateMan = HsStateManager() // 커넥터 유실 체크..
    var bypassModeChanged = { }, isPausing = false

    ///             Ack Handling....
    var packForAck: ReceivedPack?


    /// 상태 관련 변수.
    private var _conState: ConnState = .None {
        didSet {
            setStateOfVC?()
            conStateChangeActions()
        }
    }
    var conState: ConnState {
        get { return _conState }
        set {
            if newValue != _conState {
                print("\n\n HsBleCtrl :: newState> FROM >  \(_conState) ")
                _conState = newValue
                print("\n HsBleCtrl :: newState>  TO  >  \(_conState) \n\n")
            } } }
    var futureConState: ConnState?

    func checkConnState() {
        if futureConState != nil {
            conState = futureConState!
            futureConState = nil
        }
    }

    func conStateChangeActions() {
        switch conState {
        //case .None, .Error: packMchn?.invalidate()
        case .Rest:
            print("  HsBleCtrl :: packMchn Timer Started... ")
//            packMchn.timer.invalidate()
//            packMchn = HtPacketMachine(interval: 0.5)
        default:
            break
        }
    }

    private var _stt: CurStep = .S_INIT0 {
        didSet {
            print("\t HsBleCtrl :: _stt    didSet ")
        }
    }
    var stt: CurStep {
        get { return _stt }
        set {
            if newValue == _stt { return }
            print("\n\t HsBleCtrl :: 실습 상태 변경   stt : \(HsUtil.curStepStr(_stt))   ________________________________")
            _stt = newValue
            print("\t HsBleCtrl :: 실습 상태 변경   .완료.  stt.. \(HsUtil.curStepStr(_stt))  ________________________________\n")
        }
    }

    var stepEntireModeChangedAct : (Void -> Void)?
    private var pinstStt: InstructState = .Step {
        didSet {
            stepEntireModeChangedAct?()
        }
    }
    var instrctStt: InstructState {
        get { return pinstStt }
        set {
            if newValue != pinstStt {
                pinstStt = newValue
            }
        }
    }

    var isOperatingState: Bool {
        get {
            switch stt {
            case .S_STEPBYSTEP_COMPRESSION, .S_STEPBYSTEP_BREATH,
                 .S_WHOLESTEP_COMPRESSION, .S_WHOLESTEP_BREATH:
                return true
            default:
                return false
            }
        }
    }

    var isCalibration: Bool {
        get {
            switch stt {
            case .S_CALIBRATION, .CAL_LOAD, .CAL_NEW_CC, .CAL_NEW_RP:
                return true
            default: return false
            }
        }
    }

    var isAllInOnePrcss: Bool {
        get {
            switch stt {
            case .WholeStepStart, .S_WHOLESTEP_RESULT, .S_WHOLESTEP_DESCRIPTION, .S_WHOLESTEP_AED, .S_WHOLESTEP_PRECPR,
                 .S_WHOLESTEP_COMPRESSION, .S_WHOLESTEP_BREATH:
                return true
            default:
                return false
            }
        }
    }

    var allPorts = [BLEPort](), thePort: BLEPort?
    var comMan: BLESerialComManager?
    var portNum: Int { get { return allPorts.count } }


    var isPortClosed: Bool { get { return thePort == nil }}
    var isConnected: Bool { get { return thePort != nil }}
    var isConnectionTotallyFinished: Bool { get { return thePort != nil && conState == .Rest }}

    var batery = 0

    /// 접속 관련 콜백들.
    var scanFinished: (Void -> Void)?, actNewPortFound: (Void -> Void)?, productTypeErrorEvent: (Void -> Void)?
    var setStateOfVC: (Void -> Void)?, dataReceived : (ReceivedPack -> ())?
    var portOpened = { (p: BLEPort) in  }, portClosed: ((BLEPort) -> Void)?

    var filterStr = "HS_B"

    //////////////////////////////////////////////////////////////////////       [   ]
    override init() {
        super.init()
        print("\n\n  HsBleCtrl :: init  \n\n")
    }

    func initJob() {
        log.printThisFunc("initJob", lnum: 5)
        comMan = BLESerialComManager()
        comMan!.delegate = self
        var openParams = paramsPackage4Configure()
        openParams.lengthOfPackage = 10
        comMan!.configure(openParams)
        comMan!.initBLECentralManager()

        calcManager = HsCalcManager(bleObj: self, bypassObj: bypass)

        packMchn.additionalUpdate = {
            self.checkConnState()
        }

        if 0 < AppIdTrMoTe { filterStr = "HS_A" }

        log.logThis("  AppIdTrMoTe  :  \(AppIdTrMoTe)  filter : \(filterStr) ", lnum: 1)

        switch AppIdTrMoTe {
        //case 0: //bypass.ackToMonitor = ackToMonitor
        case 1:
            break
        default:
            break
        }
    }

    //////////////////////////////////////////////////////////////////////       [ 포트 스캔. 접속. 종료 관련..  ]
    func startScan() {
        allPorts = [BLEPort]()  // 초기화..
        comMan!.startEnumeratePorts(10)
        conState = .Scanning
    }

    func stopScan() {
        comMan!.stopEnumeratePorts()
    }

    func openPort(prt: BLEPort) {
        let param = paramsPackage4Open()
        if prt.state == PORT_STATE.STATE_IDLE {
            comMan!.startOpen(prt, withParams: param)
        }
        conState = .OpenStart



        log.bleObj = self
    }

    func closePort() {
        log.printAlways("\t\t \(#function)   \t\t ")
        if thePort != nil {
            comMan!.closePort(thePort!)
        }
        thePort = nil
        conState = .None
    }

    func savePrevKit() {    } // override target in Monitor use..


    //////////////////////////////////////////////////////////////////////       [  Operation Write ..  ]
    func writeData(bytes: [UInt8]) {
        if thePort == nil { return }
        log.printAlways("\t ======  HsBleCtrl   writeData  =====  \(bytes) ")
        let dt = NSData(bytes: bytes, length: bytes.count)
        comMan!.writeData(dt, toPort: thePort!)
    }

    func ackCheckAndReset() -> Bool {
        if let pack = packForAck, let ackCljr = pack.ackCheck {
            let ackChk = ackCljr()
            if ackChk {
                log.printAlways("  ack 확인 .....  ", lnum: 5)
                packForAck = nil
            }
            return ackChk
        }
        return false
    }

    /// Operation Start ..  End
    func operationStart(noDelay: Bool = false) {
        log.printAlways(" ''''\(#file) '''  ::  ^ \(#function)    operation Start", lnum: 5)
        calcManager.log.bleObj = self
        if noDelay {
            writeData([ 0x02,0x05,0x09,0x00,0x03 ])
            packForAck = ReceivedPack()
            packForAck!.ackCheck = packForAck!.operationStart
            return
        }
        HsGlobal.delay(0.3) {
            self.writeData([ 0x02,0x05,0x09,0x00,0x03 ])
        }
        
    }
    func operationStop(noDelay: Bool = false) {
        log.printAlways("operation Stop ", lnum: 5)
        if noDelay {
            writeData([ 0x02,0x05,0x09,0x01,0x03 ])
            packForAck = ReceivedPack()
            packForAck!.ackCheck = packForAck!.operationStop
            return
        }
        HsGlobal.delay(0.3) {
            self.writeData([ 0x02,0x05,0x09,0x01,0x03 ])
        }
    }

    //////////////////////////////////////////////////////////////////////       [  App dependant Methods ..  ]

    func startWholeInOneProcess() {
        log.printAlways("startWholeInOneProcess", lnum:20)
        stt = .S_WHOLESTEP_DESCRIPTION;  //_parsingState = AllInOne10; // 2가지 상태 변수 조절..
        calcManager = HsCalcManager(bleObj: self, bypassObj: bypass)
        calcManager.prepareEntireProcess()
        stage = 1;
    }

    func startCompressionProcess() { //:(NSString*)option {
        operationStart()
        if isAllInOnePrcss { stt = .S_WHOLESTEP_COMPRESSION }
        else { stt = .S_STEPBYSTEP_COMPRESSION }

        calcManager.initializeCompressionGoToNextSet()
    }

    func startStepBreathProcess() { // 단계별 학습만 부른다.
        operationStart()
        dataObj.rpCorCount = 0; dataObj.count = 0;
    }

    func progressValue4Display() -> Double {
        var dispValue: Double = 0
        switch (stt) {
        case .S_STEPBYSTEP_COMPRESSION, .S_WHOLESTEP_COMPRESSION:
            //print("   depth : %d", dataObj.depth )
            dispValue = Double(dataObj.depth)
        default: // .S_STEPBYSTEP_BREATH, .S_WHOLESTEP_BREATH: default:
            //print("   breath : %d", dataObj.depth )
            dispValue = Double(dataObj.amount)
        }
        return dispValue * 0.01;
    }

    func startCalibration() {
        operationStart()
        print("\(#function) dataObj.ccLimitPointTemp  \(dataObj.ccLimitPointTemp)  ")
        dataObj.ccLimitPointTemp = dataObj.ccLimitPoint; //    CC_LIMIT_POINT_TEMP = CC_LIMIT_POINT;
        dataObj.rpLimitPointTemp = dataObj.rpLimitPoint; //    RP_LIMIT_POINT_TEMP = RP_LIMIT_POINT;
        calcManager.activateNewCC()
    }

    func saveCalibrationResultToKitWithComp(comp: Int, rspr: Int) {
        let calibUpdateJob = UnitJob(nm: "CompressionCaliEditData")
        //   unsigned char bytes[7] = {0x02,0x07,0x07,0x01,0x01,compValue,0x03};  comp
        //   unsigned char bytes[7] = {0x02,0x07,0x07,0x01,0x02,rsprValue,0x03};
        calibUpdateJob.initJob = {
            self.writeData( [0x02,0x07,0x07,0x01,0x01, UInt8(comp),0x03] )
        }
        calibUpdateJob.didFinished = { Void -> Bool in
            if let pck = self.packForAck {
                return pck.ackCheck(7, mesg: 3, v4: 1)
            }
            return false
        }        //calibUpdateJob.failJob = ^{ return; };
        calibUpdateJob.retryLimit = 5;

        let respUpdateJob = UnitJob(nm: "RespirationCaliEditData")
        calibUpdateJob.nextJobObj = respUpdateJob
        respUpdateJob.retryLimit = 5

        respUpdateJob.initJob = {
            self.writeData( [0x02,0x07,0x07,0x01,0x02, UInt8(rspr),0x03] )
        }
        respUpdateJob.didFinished = { Void -> Bool in
            if let pck = self.packForAck {
                return pck.ackCheck(7, mesg: 3, v4: 2)
            }
            return false
        }
        //[calibUpdateJob startProcess];
    }

    /// Depth : cm, Amount : ml
    func progressValueWithUnit() -> String {
        var dispValue: Double = 0, rstr = ""
        switch (stt) {
        case .S_STEPBYSTEP_COMPRESSION, .S_WHOLESTEP_COMPRESSION:
            dispValue = calcManager.parseDepthForObjC() / 10 // [_calcManager parseDepthForObjC] / 10;
            rstr = dispValue.format(".1") + " cm"  // "%.1f cm", dispValue];
        default:
            dispValue = calcManager.parseAmountForObjC() //   [_calcManager parseAmountForObjC];
            rstr = dispValue.format(".0") + " ml" // [NSString stringWithFormat:@"%.0f ml", dispValue]
        }
        return rstr;
    }

    func progressValue4Counter() -> Double {
        var dispValue: Double = 0
        switch (stt) {
        case .S_STEPBYSTEP_COMPRESSION, .S_WHOLESTEP_COMPRESSION:
            dispValue = 30
        default:  // case S_STEPBYSTEP_BREATH:       case S_WHOLESTEP_BREATH: default:
            dispValue = 2
        }
        return Double(dataObj.count) / dispValue;
    }
    /// Trainer / Monitor
    func instructorAppInfoRequest() { // 리턴이 안 올수 있으므로 리트라이 없이..
        log.printAlways("instructorAppInfoRequest")
        log.logThis("SENDING : data from 트레이너가 키트에 Instructor App Info 를 요청  arrPckt :: \(packMchn.arrPckt.count) ")
        self.writeData([ 0x02, 0x06, 0x11, 0x01, 0x03, 0x03 ])
//        packForAck = ReceivedPack()
//        packForAck!.ackCheck = packForAck!.allwaysTrue
    }

    func debugPrintPortNames() {
        print("\t\t\t ======   Port Names  =====")
        for p in allPorts {
            print("    Port Name : \(p.name)")
        }
        print("\t\t\t ======   Port Names  =====\n\n")
    }

    func checkProductType() -> Bool {
        switch (AppIdTrMoTe) { // 0 : Trainer, 1 : Monitor, 2 : Test 트레이너는 그냥 세팅 안해도 됨..
        case 1:
            if (!(productType == 1 || productType == 3 || productType == 4 || productType == 5)) {
                return false;
            }
            break;
        case 2:
            if (!(productType == 2 || productType == 3 || productType == 4 || productType == 5)) {
                return false;
            }
            break;
        default:
            break;
        }
        return true;
    }


    private func ackToMonitorScreen() {
        if AppIdTrMoTe == 0 {
            self.writeData([ 0x02,0x06,0x11,0x02,0x12,0x03 ])
        }
    }

//    private func ackScreenCheckInMonitor() {
//        if AppIdTrMoTe == 1 {
//            print("  애크 받음..  packMchn!.arrPckt.count  \(packMchn!.arrPckt.count) ")
//            if 0 < packMchn!.arrPckt.count { packMchn!.arrPckt.removeFirst() }
//        }
//    }
}

extension HsBleCtrl : BLESerialComManagerDelegate {

    func bleSerilaComManager(bleSerialComManager: BLESerialComManager!, didFoundPort port: BLEPort!) {
        //print("\n\n\n didFound port :: \(port.name)  \(port.name.subStringTo(4))   \(port.name.getLength())   \(port.name.subStringTo(4) == filterStr)  \n\n\n")
        // filtering
        if port == nil || port.name == nil {
            print("port == nil XXX")
            return
        }

        if 5 < port.name.getLength() && port.name.subStringTo(4) == filterStr {
            allPorts.append(port)
            actNewPortFound?() // 여기서 새로운 포트가 추가된 걸 알려줌..  Table reload 유발..
        }
    }

    /// 포트 오픈 / 클로징....
    func bleSerilaComManager(bleSerialComManager: BLESerialComManager!, didOpenPort port: BLEPort!, withResult result: resultCodeType) {
        print("didOpen port ::   \(bleSerialComManager.state)    result \(result)  ")
        thePort = port
        conState = .Connected
        comMan!.stopEnumeratePorts()
        portOpened(port)
    }

    func bleSerialComManager(bleSerialComManager: BLESerialComManager!, didClosedPort port: BLEPort!) {
        print("didClosedPort ::    \(bleSerialComManager.state) ")
        thePort = nil // 이걸로 연결 확인 ?
        conState = .None
        portClosed?(port)
    }

    /// 데이터 수신...
    func bleSerialComManager(bleSerialComManager: BLESerialComManager!, didDataReceivedOnPort port: BLEPort!, withLength length: UInt32) {
        let rd = comMan!.readDataFromPort(port, withLength: Int32(length))
        if rd == nil {
            print("\n\n\n\n\n  didDataReceivedOnPort   data from port == nil  \n\n\n\n\n")
            return
        }
        let bytes = Array(UnsafeBufferPointer(start: UnsafePointer<UInt8>(rd.bytes), count: rd.length))

        //print("\t\t  HsBleCtrl :: didDataReceivedOnPort <<<<<<<<<<<<<    \t\t\t \(bytes)   state : \(conState)")
        log.logThis("didDataReceivedOnPort <<   \(conState)   \(packMchn.arrPckt.count)  <<<<< \(bytes) =========", lnum: 0)

        packMchn.timerAction()

        if 4 < bytes.count {
            var newPack: ReceivedPack?
            if packForAck == nil {
                newPack = ReceivedPack()
            } else {
                newPack = packForAck
            }
            newPack!.bytes = bytes
            newPack!.len = Int(bytes[1])
            newPack!.command = Int(bytes[2])
            newPack!.msg = Int(bytes[3])
            newPack!.val4 = Int(bytes[4])

            dataReceived?(newPack!)

            if conState == .Rest {
                switch newPack!.command {
                case 16:
                    operationDataParsing(newPack!)
                case 17:
                    bypass.parsePacket(newPack!, state: stt)
                    checkInstructorAppResponse(newPack!)
                    if newPack!.msg == 2 { ackToMonitorScreen() }
                    //if newPack!.ackCheck(17, mesg: 2, v4: 18) { ackScreenCheckInMonitor() }
                default:
                    break
                }

                if AppIdTrMoTe == 0 && newPack!.ackCheck(5, mesg: nil, v4: nil) { // BLE 1 신호..  트레이너 에서만 처리..

                    log.printAlways(" BLE 1 신호 ...   \(newPack!.msg) ")
                    switch newPack!.msg {
                    case 0:
                        isBypassMode = true
                    case 1:
                        trnBleCtrl.stt = .S_QUIT //  모니터가 끊을 때..
                        //isBypassMode = false  이렇게 하지 말고, quit 일 때 Operation 화면에서 나가고 선택 화면에서 false로 하고 스텝을 초기화 하는 것이 좋겠다.
                        HsBleSingle.inst().stageLimit =  Int32(NSUserDefaults.standardUserDefaults().integerForKey("StageLimit"))
                    default:
                        print(" Error ")
                    }
                }
            }
        }
        setStateOfVC?()
    }

    /// 모니터 / 테스트 App Info 바로 Response
    func checkInstructorAppResponse(pck: ReceivedPack) {
        if AppIdTrMoTe == 0 || !pck.ackCheck(17, mesg: 1, v4: 3) { return }
        if isCalibration {
            log.printAlways("  checkInstructorAppResponse  현재 Calibration ", lnum: 10)
            self.writeData([ 0x02,0x06,0x11,0x01, 0x03, 0x03 ])
        } else {
            log.printAlways("  checkInstructorAppResponse AppInfo : \(AppIdTrMoTe) ", lnum: 10)
            self.writeData([ 0x02,0x06,0x11,0x01, UInt8(AppIdTrMoTe), 0x03 ])
        }
    }

    func operationDataParsing(obj: ReceivedPack) {
        //log.logThis("operationDataParsing", lnum: 0)
            // [self setSpeedLogic];
        let dataUInt8 = obj.bytes

        if stt == .S_CALIBRATION {
            calcManager.calibrationViewDataParsing(dataUInt8) // 캘리브레이션 세팅 화면에서..
        } else if isAllInOnePrcss {
            calcManager.allInOneDataParsing(dataUInt8) // 올인원은 따로..
        } else {
            calcManager.stepByStepParsing(dataUInt8)  // 스텝스텝 .. 여기서.
        }
    }

    func bleSerilaComManagerDidStateChange(bleSerialComManager: BLESerialComManager!) {
        print("bleSerilaComManagerDidStateChange ::   \(bleSerialComManager.state) ")
        setStateOfVC?()
    }

    func bleSerilaComManagerDidEnumComplete(bleSerialComManager: BLESerialComManager!) {
        print("bleSerilaComManagerDidEnumComplete :: ")
        scanFinished?()
        setStateOfVC?()
    }
}
