//
//  HSPeripheral.h
//  heartisense
//
//  Created by jeongyuchan on 2015. 4. 21..
//  Copyright (c) 2015년 gyuchan. All rights reserved.
//

#import "YMSCBPeripheral.h"

@class HSBaseService;

@interface HSPeripheral : YMSCBPeripheral

- (HSBaseService *)allInfo;

@end
