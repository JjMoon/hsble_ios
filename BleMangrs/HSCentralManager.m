//
//  HSCentralManager.m
//  heartisense
//
//  Created by jeongyuchan on 2015. 4. 20..
//  Copyright (c) 2015년 gyuchan. All rights reserved.
//

#import "HSCentralManager.h"
#import "HSDataStaticValues.h"
#import "YMSCBStoredPeripherals.h"
#import "YMSCBPeripheral.h"
#import "HSPeripheral.h"

#include "TISensorTag.h"

#define CALLBACK_EXAMPLE 1

//static HSCentralManager *sharedCentralManager;

@implementation HSCentralManager {
    bool logDiscoveredMSG;
}
//
//
//+ (HSCentralManager *)initSharedServiceWithDelegate:(id)delegate{
//    if (sharedCentralManager == nil) {
//        dispatch_queue_t queue = dispatch_queue_create("com.imlab.heartisense", 0);
//        
//        NSArray *nameList = @[@"HS_01"];
//        
//        NSLog(@"HsCentralManager ::   initSharedServiceWithDelegate  nameList count : %d ", nameList.count);
//        
//        sharedCentralManager = [[super allocWithZone:NULL] initWithKnownPeripheralNames:nameList
//                                                                                  queue:queue
//                                                                   useStoredPeripherals:YES
//                                                                               delegate:delegate];
//    }
//    return sharedCentralManager;
//    
//}
//
//+ (HSCentralManager *)sharedService{
//    if (sharedCentralManager == nil) {
//        NSLog(@"ERROR: must call initSharedServiceWithDelegate: first.");
//    }
//    return sharedCentralManager;
//}

-(instancetype)initWithDelegate:(id)delegateObj
{
    dispatch_queue_t queue = dispatch_queue_create("com.imlab.heartisense", 0);

     NSArray *nameList = @[@"HS_01"];
    self = [super initWithKnownPeripheralNames:nameList queue:queue useStoredPeripherals:YES delegate:delegateObj];
    return self;
}


- (void)removeAllPeripherals {
    [super removeAllPeripherals];
    NSLog(@"\n\n\n\n  HsCentralManager :: super removeAllPeripherals  \n\n\n\n ");
}

- (void)startScan {
    
    NSLog(@"\n\n\n\n\n HSCentralManager :: startScan \n\n\n\n\n");
    /*
     Setting CBCentralManagerScanOptionAllowDuplicatesKey to YES will allow for repeated updates of the RSSI via advertising.
     */
    
    logDiscoveredMSG = true;
    
    NSDictionary *options = @{ CBCentralManagerScanOptionAllowDuplicatesKey: @YES };
    // NOTE: TI SensorTag firmware does not included services in advertisementData.
    // This prevents usage of serviceUUIDs array to filter on.
    
    /*
     Note that in this implementation, handleFoundPeripheral: is implemented so that it can be used via block callback or as a
     delagate handler method. This is an implementation specific decision to handle discovered and retrieved peripherals identically.
     
     This may not always be the case, where for example information from advertisementData and the RSSI are to be factored in.
     */
    
#ifdef CALLBACK_EXAMPLE
    __weak HSCentralManager *this = self;
    [self scanForPeripheralsWithServices:@[[CBUUID UUIDWithString:SERVICE_UUID_STRING]]
                                 options:options
                               withBlock:^(CBPeripheral *peripheral, NSDictionary *advertisementData, NSNumber *RSSI, NSError *error) {
                                   if (error) {
                                       NSLog(@"Something bad happened with scanForPeripheralWithServices:options:withBlock:");
                                       return;
                                   }
                                   
                                   //if (logDiscoveredMSG) {
                                    //NSLog(@"DISCOVERED: %@, %@, %@ db", peripheral, peripheral.name, RSSI);
                                   //}

                                   [this handleFoundPeripheral:peripheral];
                               }];
#else
    [self scanForPeripheralsWithServices:@[[CBUUID UUIDWithString:SERVICE_UUID]] options:options];
#endif
    
}

- (void)handleFoundPeripheral:(CBPeripheral *)peripheral {
    //NSLog(@"\n\n\n\n\n HSCentralManager :: handleFoundPeripheral \n\n\n\n\n");
    YMSCBPeripheral *yp = [self findPeripheral:peripheral];
    
    if (yp == nil) {
        BOOL isUnknownPeripheral = YES;
        for (NSString *pname in self.knownPeripheralNames) {
            // 트레이너 버전은 HS_02에만 연결시키도록 한다.  // 기기 이름의 앞 두글자가 HS인 디바이스 체크
            NSRange range = {0,4};  // HS_B
            NSString *nameHead = [peripheral.name substringWithRange:range];
            
            NSString *strB = @"HS_B"; // [pname substringWithRange:range];
            if (AppIdTrMoTe > 0) strB = @"HS_A";
            //NSLog(@"___  이름 필터링..   HSCentralManager ::  >>  handleFoundPeripheral  /t/t <<  %@  >>    Filter : %@ ", peripheral.name, strB);
            
            //             if ([pname isEqualToString:peripheral.name]) {  // MOOON :: 150831

            if ([nameHead isEqualToString:strB]) { // 조건 검색 무력화
                HSPeripheral *sensorTag = [[HSPeripheral alloc] initWithPeripheral:peripheral
                                                                           central:self
                                                                            baseHi:kSensorTag_BASE_ADDRESS_HI
                                                                            baseLo:kSensorTag_BASE_ADDRESS_LO];
                NSLog(@"___  addPeripheral \n\n");
                [self addPeripheral:sensorTag];
                isUnknownPeripheral = NO;
                break;
            }

        }
        
        if (isUnknownPeripheral) {
            //TODO: Handle unknown peripheral
//            yp = [[YMSCBPeripheral alloc] initWithPeripheral:peripheral central:self baseHi:0 baseLo:0];
//            [self addPeripheral:yp];
        }
    }
    
    logDiscoveredMSG = false;

    
    
    
}


@end
