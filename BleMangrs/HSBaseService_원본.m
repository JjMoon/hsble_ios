//
//  HSBaseService.m
//  heartisense
//
//  Created by jeongyuchan on 2015. 4. 24..
//  Copyright (c) 2015년 gyuchan. All rights reserved.
//
#import "Common-Swift.h"
#import "HSBaseService.h"
#import "YMSCBCharacteristic.h"
#import "HsDataStaticValues.h"
#import "NSObject+util.h"
#import "HsBleSingle.h"



@interface HSBaseService()
{
    HtLog* log;
    UnitJob *retryJob;
    BOOL ack88received, monitorAck;
    
    NSTimer* timer;
    NSMutableArray* arrPckt;
}

@end


@implementation HSBaseService

- (instancetype)initWithName:(NSString *)oName parent:(YMSCBPeripheral *)pObj UUID:(NSString *)UUID{
    self = [super initWithName:oName
                        parent:pObj
                          UUID:UUID];
    if(self){
        [self addCharacteristic:@"all_information" withAddress:DATALINE_UUID];
    }
    
//    timer = [NSTimer scheduledTimerWithTimeInterval:0.2 target:self
//                                             selector:@selector(myTimerAction) userInfo:nil repeats:YES];
    
    
    log = [[HtLog alloc] initWithName:@"HS Base Service.m"];
    
    return self;
}

- (void)turnOn:(void (^)())successBlock{
    __weak HSBaseService *wself = self;
    
    YMSCBCharacteristic *dataCt = self.characteristicDict[@"all_information"];
    [dataCt setNotifyValue:YES withBlock:^(NSError *error) {
        __strong __typeof(wself) this = wself;
        
        if(!error){
            // MOOON :: 150831
            NSLog(@"\n\n\n");
            NSLog(@"   Data notification이 켜졌습니다 : %@", this.name);
            successBlock();
        }
    }];
    
    _YMS_PERFORM_ON_MAIN_THREAD(^{
        __strong __typeof(wself) this = wself;
        this.isOn = YES;
    });
}
- (void)turnOff:(void (^)())successBlock{
    __weak HSBaseService *this = self;
    
    YMSCBCharacteristic *dataCt = self.characteristicDict[@"all_information"];
    [dataCt setNotifyValue:NO withBlock:^(NSError *error) {
        if(!error){
            NSLog(@"Data notification이 꺼졌습니다 : %@", this.name);
            successBlock();
        }
    }];
    
    _YMS_PERFORM_ON_MAIN_THREAD(^{
        this.isOn = NO;
    });
}

- (void)arraySetup {
    
        arrPckt = [[NSMutableArray alloc] init];
    
    HtBaseJob* aJob = [[HtBaseJob alloc] initWithNm:@"None" retryLimitN:3];
    [arrPckt addObject:aJob];
    
}

//////////////////////////////////////////////////////////////////////       [  Send Machine ]
#pragma mark - Timer Action
- (void)myTimerAction {
    
    if (arrPckt == nil) [self arraySetup];
    
    if (arrPckt.count < 2) return;
    
    [self logCallerMethodwith:@"  arrPckt.count > 0   " newLine:2];
    
    NSLog(@"  me : %@     ", self);
    NSLog(@"  arr : %@   %lu  ", arrPckt, (unsigned long)arrPckt.count);
    NSLog(@"  me : %@     obj :   %@", self, [arrPckt objectAtIndex:0]);
    
    HtBaseJob* curJob = (HtBaseJob*)[arrPckt objectAtIndex:1];
    
    if (curJob.retryCount == 0) {
        [self logCallerMethodwith:@" arrPckt :: >>  initJob   " newLine:2];
        //curJob.initJob();
        //curJob.retryCount = 1;
        return;
    } else {
        if (curJob.didFinished()) { // OK..  done..
            //curJob = nil;
            NSLog(@"arrPckt   removefirst");
            [arrPckt removeObjectAtIndex:1];
            NSLog(@"  arr : %@   %lu  ", arrPckt, (unsigned long)arrPckt.count);
            return;
        }
    }
    
    if (curJob.retryLimit <= curJob.retryCount) {
        [log logUiAction:@"  retryLimit <= curJob.retryCount  실패 .... " lnum:10 printOn:true];    [curJob logFailure];
        NSLog(@"arrPckt   removefirst");
        [arrPckt removeObjectAtIndex:1];
        NSLog(@"  arr : %@   %lu  ", arrPckt, (unsigned long)arrPckt.count);
    } else {
        curJob.initJob();
        curJob.retryCount++;
        NSLog(@"arrPckt     retry  ...    %ld", (long)curJob.retryCount);
    }
}


//////////////////////////////////////////////////////////////////////       [ 모니터 커멘드 ]
#pragma mark - Monitor Command
- (void)monitorCommand:(UInt16)mntCmd {
    [self logCallerMethodwith:@" monitorCommand  . " newLine:20];
    UInt16 blsLay = isRescureMode? 0x01: 0x02;
    unsigned char bytes[9] = {0x02, 0x09, 0x11, 0x02, mntCmd, 15, blsLay,  HsBleSingle.inst.stageLimit, 0x03};
    NSData *data = [NSData dataWithBytes:bytes length:sizeof(bytes)];
    [self writeValue:data withBlock:nil];
}
//
//- (void)monitorCommand:(UInt16)mntCmd {
//    
//    [self logCallerMethodwith:@" monitorCommand  . " newLine:20];
//    UInt16 blsLay = isRescureMode? 0x01: 0x02;
//    unsigned char bytes[9] = {0x02, 0x09, 0x11, 0x02, mntCmd, 15, blsLay,  HsBleSingle.inst.stageLimit, 0x03};
//    NSData *data = [NSData dataWithBytes:bytes length:sizeof(bytes)];
//    
//    __weak HSBaseService* wself = self;
//    __strong __typeof(wself) this = wself;
//    
//    HtBaseJob* aJob = [[HtBaseJob alloc] initWithNm:@"monitorCommand" retryLimitN:5];
//
//    aJob.initJob = ^{
//        monitorAck = false;
//        [this writeValue:data withBlock:nil];
//    };
//    
//    aJob.didFinished = ^BOOL(void) {
//        return monitorAck;
//    };
//    [arrPckt addObject:aJob];
//    NSLog(@" monitorCommand   arrPckt count : %lu", (unsigned long)arrPckt.count);
//}
//

- (void)sendAppInfoResponseMonitor {
    [self logCallerMethodwith:@" sendAppInfoResponseMonitor  . " newLine:20];
    unsigned char bytes[9] = {0x02, 0x06, 0x11, 0x01, 0x01, 0x03};
    NSData *data = [NSData dataWithBytes:bytes length:sizeof(bytes)];
    [self writeValue:data withBlock:nil];
}

/*
 * Noti가 오는 Characteristic에 대한 처리
 */
- (void)notifyCharacteristicHandler:(YMSCBCharacteristic *)yc error:(NSError *)error {
    if (error) {
        return;
    }
    if ([yc.name isEqualToString:@"all_information"]) {
        NSData *data = yc.cbCharacteristic.value;
        char val[data.length];
        [data getBytes:&val length:data.length];
    }
}

/*
 * Status Infomation에 대한 요청
 */

- (void)requestLabelCheck {
    [self logCallerMethodwith:@" 프로세스 시작. " newLine:20];
    Byte appID = AppIdTrMoTe + 1; // 1, 2, 3
    unsigned char bytes[5] = {0x02, 0x05, 0x08, appID, 0x03};
    NSData *data = [NSData dataWithBytes:bytes length:sizeof(bytes)];
    [self writeValue:data withBlock:nil];
}

- (void)requestSensorInfo:(Byte)cmd {
    unsigned char bytes[5] = {0x02, 0x05, cmd, 0x03, 0x03};
    NSData *data = [NSData dataWithBytes:bytes length:sizeof(bytes)];
    [self writeValue:data withBlock:nil];
}


- (void)ackToMonitor {
    
    [self logCallerMethodwith:@"<<<<   ACT  TO  MONITOR App    >>> " newLine:10];
    
    unsigned char bytes[6] = {0x02, 0x06, 0x11, 0x02, 0x12, 0x03};
    NSData *data = [NSData dataWithBytes:bytes length:sizeof(bytes)];
    [self writeValue:data withBlock:nil];
}

- (void)instructorAppInfoRequest {
    unsigned char bytes[6] = {0x02, 0x06, 0x11, 0x01, 0x03, 0x03};
    NSData *data = [NSData dataWithBytes:bytes length:sizeof(bytes)];
    [self writeValue:data withBlock:nil];
}


/*
 * Calibration Infomation에 대한 요청
 */

- (void)requestCompressionCaliSetData
{
    [self logNewLine:8];
    [self logCallerMethodwith:@" <<  requestCompressionCaliSetData  >>   write : 267013 " newLine:1];
    [self logNewLine:8];
    
    unsigned char bytes[6] = {0x02,0x06,0x07,0x00,0x01,0x03};  // 267013
    NSData *data = [NSData dataWithBytes:bytes length:sizeof(bytes)];

    [self writeValue:data withBlock:nil];
}

- (void)requestRespirationCaliSetData{
    [self logNewLine:8];
    [self logCallerMethodwith:@" <<  requestRespirationCaliSetData   >>   write : 267023 " newLine:1];
    [self logNewLine:8];

    unsigned char bytes[6] = {0x02,0x06,0x07,0x00,0x02,0x03};
    NSData *data = [NSData dataWithBytes:bytes length:sizeof(bytes)];
    
    
    [self writeValue:data withBlock:nil];
}

//////////////////////////////////////////////////////////////////////       [   Operation  관련   멈추고...  시작하고...  ]
#pragma mark - Operation Infomation에 대한 요청

//- (void)requestOperationStart {
//    NSLog(@"\n\n\n\n\n");
//    [self logCallerMethodwith:@" << requestOperationStart >>   write : 259 0 3 " newLine:0];
//    NSLog(@"\n\n\n\n\n");
//    
//    unsigned char bytes[5] = {0x02,0x05,0x09,0x00,0x03};
//    NSData *data = [NSData dataWithBytes:bytes length:sizeof(bytes)];
//    
//    __weak HSBaseService* wself = self;
//    __strong __typeof(wself) this = wself;
//    
//    HtBaseJob* aJob = [[HtBaseJob alloc] initWithNm:@"Operation Start" retryLimitN:3];
////
////    aJob.initJob = ^{
//    
//        //NSLog(@"  Operation start   initjob  operationon : %d", _weakManager.operationOn);
//        
////        ack88received = false;
////        if (_weakManager.operationOn)
////            ack88received = true;  // 그냥 넘기도록.
////        else
//            //[this writeValue:data withBlock:nil];
////    };
//    
////    aJob.didFinished = ^BOOL(void) {
////        if (ack88received)
////            _weakManager.operationOn = true;
////        return ack88received;
////    };
//    [arrPckt addObject:aJob];
//    NSLog(@"  arrPckt count : %lu", (unsigned long)arrPckt.count);
//}
//
//- (void)requestOperationStop {
//    NSLog(@"\n\n\n\n\n");
//    [self logCallerMethodwith:@" <<  requestOperationStop  >>   write : 259 1 3 " newLine:0];
//    NSLog(@"\n\n\n\n\n");
//    
//    unsigned char bytes[5] = {0x02,0x05,0x09,0x01,0x03};
//    
//    NSData *data = [NSData dataWithBytes:bytes length:sizeof(bytes)];
//    __weak HSBaseService* wself = self;
//    
//    HtBaseJob* aJob = [[HtBaseJob alloc] initWithNm:@"Operation Stop" retryLimitN:3];
//    
//    aJob.initJob = ^{
//        __strong __typeof(wself) this = wself;
//        ack88received = false;
//        if (_weakManager.operationOn)
//            [this writeValue:data withBlock:nil];
//        else
//            ack88received = true;
//    };
//    aJob.didFinished = ^BOOL(void) {
//        if (ack88received)
//            _weakManager.operationOn = false;
//        return ack88received;
//    };
//    [arrPckt addObject:aJob];
//    
//    NSLog(@"  arrPckt count : %lu", (unsigned long)arrPckt.count);
//}


- (void)requestOperationStart {
    NSLog(@"\n\n\n\n\n");
    [self logCallerMethodwith:@" << requestOperationStart >>   write : 259 0 3 " newLine:0];
    NSLog(@"\n\n\n\n\n");

    unsigned char bytes[5] = {0x02,0x05,0x09,0x00,0x03};
    NSData *data = [NSData dataWithBytes:bytes length:sizeof(bytes)];
    
    __weak HSBaseService* wself = self;

    [self addUnitJob:^{
        __strong __typeof(wself) this = wself;
        ack88received = false;
        if (_weakManager.operationOn)   ack88received = true;
        else                            [this writeValue:data withBlock:nil];
    } withFinishCondition:^BOOL(void) {
        if (ack88received)    _weakManager.operationOn = true;
        return ack88received; } andName:@"Operation Start  "];
}

- (void)requestOperationStop {
    NSLog(@"\n\n\n\n\n");
    [self logCallerMethodwith:@" <<  requestOperationStop  >>   write : 259 1 3 " newLine:0];
    NSLog(@"\n\n\n\n\n");
    
    unsigned char bytes[5] = {0x02,0x05,0x09,0x01,0x03};
    
    NSData *data = [NSData dataWithBytes:bytes length:sizeof(bytes)];
    __weak HSBaseService* wself = self;
    [self addUnitJob:^{
        __strong __typeof(wself) this = wself;
        ack88received = false;
        if (_weakManager.operationOn)   [this writeValue:data withBlock:nil];
        else                            ack88received = true;
        } withFinishCondition:^BOOL(void) {
            if (ack88received)    _weakManager.operationOn = false;
            return ack88received; } andName:@"Operation Stop !!! "];
}

//////////////////////////////////////////////////////////////////////       [   Retry 관련 ]
#pragma mark - Retry Logic
- (UnitJob*)addUnitJob:(void (^)(void))withEntryAction withFinishCondition:(BOOL (^)(void))finishCondition   andName:(NSString*)name {  //  간격은 0.5초.. 회수는 5번.
    [self logCallerMethodwith:@" <<  HSBaseService ::  리트라이 시도 시작 ..______  >>  " newLine:2];
    UnitJob *nJob = [[UnitJob alloc] initWithName:name];
    nJob.initJob = withEntryAction;
    nJob.didFinished = finishCondition;
    nJob.delayTime = 0.5;  nJob.retryLimit = 5;

    if (retryJob != nil) { [retryJob cancelAllActions]; }
    
    retryJob = nJob;
    [retryJob startProcess];
    
//    if (retryJob == nil) {
//        retryJob = nJob;  // 일이 끝났으면 교체하거나.
//        [retryJob startProcess];
//    } else {
//        if (retryJob.myJobWasDone) {
//            retryJob = nJob;  // 일이 끝났으면 교체하거나.
//            [retryJob startProcess];
//        } else {
//            [retryJob getEndOfNextJob].nextJobObj = nJob; // 마지막에 등록.
//        }
//    }
    return nJob;
}

- (void)receivedData:(CBCharacteristic *)characteristic {
    NSData *data = characteristic.value;    unsigned char val[data.length];
    [data getBytes:&val length:data.length];
    
    if (data.length < 5)         return;
    
    int len = val[1] & 0xFF, bypass = val[2] & 0xFF, ack18 = val[4] & 0xFF; //, cmd = val[2] & 0xFF, msg = val[3] & 0xFF, vl4 = val[4] & 0xFF;
    //char charLen = val[1] & 0xFF;
    
    //NSLog(@"  len : 136이면 애크 : %@     %d   %d", data, len, charLen);
    if (len == 136) { // || charLen == -120) {
        NSLog(@"\n\n\n\n\n\n  \t\t\t\t HsBaseService : ##### received     현재 작업 : < %@ > 에 대한 ACK_88 #####  \n\n\n\n\n\n\n\n", retryJob.jobName);
        ack88received = true;
    }
    if (bypass == 17 && ack18 == 18) {
        monitorAck = true;
    }
}

//////////////////////////////////////////////////////////////////////       [ requestCompressionCaliEditData & requestRespirationCaliEditData ]
#pragma mark - Calibration Edit에 대한 요청
- (void)requestCompressionCaliEditData:(UInt16)compValue {
    [self logCallerMethodwith:[NSString stringWithFormat:@"  압력 값 키트에 쓰기 >>   compValue : %d", compValue] newLine:10];
    unsigned char bytes[7] = {0x02,0x07,0x07,0x01,0x01,compValue,0x03};
    NSData *data = [NSData dataWithBytes:bytes length:sizeof(bytes)];
    [self writeValue:data withBlock:nil];
}

- (void)requestRespirationCaliEditData:(UInt16)rsprValue{
    [self logCallerMethodwith:[NSString stringWithFormat:@"  호흡 값 키트에 쓰기 >>   rsprValue : %d", rsprValue] newLine:10];
    unsigned char bytes[7] = {0x02,0x07,0x07,0x01,0x02,rsprValue,0x03};
    NSData *data = [NSData dataWithBytes:bytes length:sizeof(bytes)];
    [self writeValue:data withBlock:nil];
}

//////////////////////////////////////////////////////////////////////   writeValue withBlock
- (void)writeValue:(NSData *)data withBlock:(void (^)(NSError *error))writeCallback {
    //  /*   4 Debugging
    unsigned char datas[data.length];
    [data getBytes:&datas length:data.length];
    UInt16 len = datas[1] & 0xFF;
    UInt16 cmd = datas[2] & 0xFF;
    NSLog(@"\n\n\n\n\n");
    NSLog(@" << HsBaseService :: >>> SENDING : data : %@  >> len : %d, cmd : %d   \t\t====================>>>>>   패킷 송신  =====>>>>>", data, len, cmd);
    NSLog(@"\n\n\n\n\n");
    
    YMSCBCharacteristic *battery_infoCt = self.characteristicDict[@"all_information"];
    [battery_infoCt writeValue:data withBlock:writeCallback];
}


@end


//3456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
//////////////////////////////////////////////////////////////////////       [ CLASSname ]

