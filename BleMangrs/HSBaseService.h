//
//  HSBaseService.h
//  heartisense
//
//  Created by jeongyuchan on 2015. 4. 24..
//  Copyright (c) 2015년 gyuchan. All rights reserved.
//

#import "YMSCBService.h"
#include "TISensorTag.h"


@class HsBleManager;

@interface HSBaseService : YMSCBService
{
    
}

@property (atomic, assign) bool isTrainerALive;
@property (atomic, assign) NSString* titleAtoD;
@property (atomic, assign) int protoN;
@property (nonatomic, weak) HsBleManager* weakManager;
@property (nonatomic, readonly) NSDictionary *sensorValues;

@property (nonatomic, strong, readonly) NSNumber *batteryInfo;


/*
 * Status Infomation에 대한 요청
 */
- (void)requestLabelCheck;
- (void)requestKitEnable;
- (void)requestSensorInfo:(Byte)cmd;

/*
 * Bypass 관련 패킷..
 */
- (void)instructorAppInfoRequest; // Ble 1 connected..  경우.
- (void)sendAppInfoResponseTest;
- (void)sendAppInfoCalibration; // 캘리브레이션 시작 시 트레이너에게 전달..
- (void)ackToMonitor;


/*
 * Calibration Value Data에 대한 요청
 */
- (void)requestCompressionCaliSetData;
- (void)requestRespirationCaliSetData;

- (void)requestDValueUpdate;

- (void)requestCompressionCaliEditData:(UInt16)byte;
- (void)requestRespirationCaliEditData:(UInt16)byte;

//////////////////////////////////////////////////////////////////////       [ 모니터 커멘드 ]
- (void)monitorCommand:(UInt16)mntCmd name:(NSString*)name;
- (void)ackReceivedInMonitor;
- (void)sendAppInfoResponseMonitor;


/*
 * Operation Infomation에 대한 요청
 */
- (void)requestOperationStart;
- (void)requestOperationStop;


- (void)receivedData:(CBCharacteristic *)characteristic;


- (void)turnOn:(void (^)())successBlock;
- (void)turnOff:(void (^)())successBlock;


@end
