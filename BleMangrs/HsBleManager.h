//
//  HsBleManager.h
//  heartisense
//
//  Created by Jongwoo Moon on 2015. 9. 1..
//  Copyright (c) 2015년 gyuchan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "HSPeripheral.h"
#import "HSBaseService.h"
#import "HSCentralManager.h"
#import "SearchDeviceListViewCell.h"
#import "HSEnumSet.h"
#import "HSDataStaticValues.h"

@class MuState, HsCalcManager, HsBypassManager, HsStateManager, HsData, SearchDeviceListView;

@protocol BleManToVCtrlProtocol <NSObject>
@required
- (void)dataParse:(CBCharacteristic*)charObj;

@end


typedef bool(^ConditionBlock)(void);


typedef enum  {
    Initial, PrevConnectionStarted, ConnectionStarted, Connected, Failed
} ConnectionState;

typedef enum  {  // 파싱할 때 사용.  단계 | 전과정 을 나누는 기준.
    StandBy = 0, InitCalibration00 = 3, CalibrationView01 = 5, AllInOne10 = 10, StepByStep20 = 20
} ParsingState;


@interface HsBleManager : NSObject <CBPeripheralDelegate>

{
    //BOOL stepMode;
    UIViewController *curViewCtrl;
 
    //  모니터에서 사용.
    //BOOL isTrainerConnected;
    UInt16 caliComp, caliBrth;
    //NSString *prevDevName;
}




+ (HsBleManager*)inst; // singletone
//- (void)reset;

- (void)requestCompCalibrSetDataPeri:(HSPeripheral*)peri withService:(HSBaseService*)service;
//- (void)bypassActions;


// UI | UX
- (double)progressValue4Display; // 0.88 등의 값. depth, amount
- (NSString*)progressValueWithUnit; // 0.88 등의 값. depth, amount
- (double)progressValue4Counter; // 횟수.
- (NSString*)uiCountInfo;
- (BOOL)uiMessageOn;  // Too Weak 등의 메시지 on/off
- (BOOL)uiMessageOff;
- (void)startCalibration;
- (void)backToReady;
- (BOOL)isEntireProcess;
- (BOOL)isOperatingState;
- (BOOL)isCalibating;

// Connection
- (void)startConnection; //with:(HSPeripheral*)peripheral; //withBlock:(void(^)(HSBaseService*))connectionBlock;
- (void)removePeripherals;
- (void)stopScan;
- (NSString*)bleButtonText;
- (void)connectCompletedAction;
// Connection Functions with Block Parameters.
- (void)connectPreviousDevice:(NSString *)prevDevNum withFailBlock:(void (^)(void))failureBlock; // 이전 기기 연결
- (void)connect:(void (^)(void))setSearchBttnTxt andPopupBlock:(void (^)(void))showPopupView;    // 다른 기기 연결

- (void)setSpeedLogic;

// Block setting
-(void)setUpdateDevListViewBlock:(void (^)(HSCentralManager*))updateBlock;
-(void)setVoidVoidBlockRepeat:(int)num  withBlock:(void (^)(void))voidBlock;
//-(void)setVoidVoidBlock:(void (^)(void))voidBlock;

- (void)operationStart;
- (void)operationStop;
- (void)startWholeInOneProcess;
- (void)startCompressionProcess;
- (void)startStepBreathProcess;
- (BOOL)isBusy;

- (void)tempExceptPopup;

- (void)saveCalibrationResultToKitWithComp:(int)comp andRspr:(int)rspr successBlock:(void (^)())successBlock;


- (void)setDeviceListView:(SearchDeviceListView*)listView;


// Monitor
@property (strong, nonatomic) NSString* nick, *connectedName;

// Bypass and ...
@property (nonatomic) int manikinKind; // 0 : default, 1 : prestan
@property (nonatomic) BOOL operationOn, isPausing, calibrationMode; // 모니터, 테스트 에서만 쓰임..
@property (strong, nonatomic) HsCalcManager* calcManager;
@property (strong, nonatomic) HsBypassManager* bypass;
@property (strong, nonatomic) HsStateManager* stateMan;
@property (strong, nonatomic) HsData* dataObj;
@property (nonatomic) CurStep bleState;

//@property (weak, nonatomic) NSString* prevDevName;

@property (nonatomic) BOOL monitorAck;
@property (nonatomic) int stage, wrongPositionNum; // 세트 수, 세팅에서 정한 세트 수.. 1 .. 5
@property (weak, nonatomic) NSMutableArray * position;

@property (nonatomic) BOOL isConnected, isConnectionTotallyFinished;
@property (nonatomic) ConnectionState connState;
@property (nonatomic) ParsingState parsingState;
@property (nonatomic) HSCalculatorType calType;
@property (nonatomic, strong) NSString *messageName, *prevDevName;;
@property (nonatomic, strong) void (^productTypeErrorEvent)(void);
@property (nonatomic, strong) void (^bypassStartedBlock)(void);

@property (nonatomic, weak) void(^weakValueUI)(void);
@property (nonatomic, strong) void (^goToSelectionModeView)(void);
@property (nonatomic, strong) void(^bypassModeChanged)(void);
//@property (nonatomic, strong) void (^exceptionProcess)(void);
@property (nonatomic, strong) void (^exceptionProc)(int, int);
@property (nonatomic, strong) void (^timerErrorBlock)(void);

@property (nonatomic, strong) NSMutableArray *arrUnitJob;
@property (nonatomic, strong) NSMutableArray *arrBle;
//@property (strong) NSTimer* mtmTimer;
@property (nonatomic, strong) NSTimer* mtmTimer;


@property (nonatomic, weak) id<BleManToVCtrlProtocol> bleMan2VcDelegate;
@property (nonatomic, weak) HSPeripheral* cellPeripheral;
@property (nonatomic, weak) HSBaseService* cellService;

@property (nonatomic, weak) SearchDeviceListViewCell* curCell;
@property (nonatomic, weak) UIView* tempViewObj;


@end

