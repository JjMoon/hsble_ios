//
//  HSBaseService.m
//  heartisense
//
//  Created by jeongyuchan on 2015. 4. 24..
//  Copyright (c) 2015년 gyuchan. All rights reserved.
//
#import "Common-Swift.h"
#import "HSBaseService.h"
#import "YMSCBCharacteristic.h"
#import "HsDataStaticValues.h"
#import "NSObject+util.h"
#import "HsBleSingle.h"



@interface HSBaseService()
{
    HtLog* log;
    UnitJob *retryJob;
    BOOL ack88received, monitorAck;
    
    HtPacketMachine* pcktMachine;
}

@end


@implementation HSBaseService

- (instancetype)initWithName:(NSString *)oName parent:(YMSCBPeripheral *)pObj UUID:(NSString *)UUID{
    self = [super initWithName:oName
                        parent:pObj
                          UUID:UUID];
    if(self){
        [self addCharacteristic:@"all_information" withAddress:DATALINE_UUID];
    }
    
    
    log = [[HtLog alloc] initWithCName:@"HS Base Service.m" active:true];
    
    pcktMachine = [[HtPacketMachine alloc] initWithInterval:0.8]; // 0.16 0.08];
    
    return self;
}

- (void)turnOn:(void (^)())successBlock{
    __weak HSBaseService *wself = self;
    
    YMSCBCharacteristic *dataCt = self.characteristicDict[@"all_information"];
    [dataCt setNotifyValue:YES withBlock:^(NSError *error) {
        __strong __typeof(wself) this = wself;
        
        if(!error){
            // MOOON :: 150831
            NSLog(@"\n\n\n");
            NSLog(@"   Data notification이 켜졌습니다 : %@", this.name);
            successBlock();
        }
    }];
    
    _YMS_PERFORM_ON_MAIN_THREAD(^{
        __strong __typeof(wself) this = wself;
        this.isOn = YES;
    });
}
- (void)turnOff:(void (^)())successBlock{
    __weak HSBaseService *this = self;
    
    YMSCBCharacteristic *dataCt = self.characteristicDict[@"all_information"];
    [dataCt setNotifyValue:NO withBlock:^(NSError *error) {
        if(!error){
            NSLog(@"Data notification이 꺼졌습니다 : %@", this.name);
            successBlock();
        }
    }];
    
    _YMS_PERFORM_ON_MAIN_THREAD(^{
        this.isOn = NO;
    });
}


//////////////////////////////////////////////////////////////////////       [ 모니터 커멘드 ]
#pragma mark - Monitor Command

- (void)monitorCommand:(UInt16)mntCmd name:(NSString*)name {
    //[self logCallerMethodwith:@" monitorCommand  . " newLine:20];
    UInt16 blsLay = isRescureMode? 0x01: 0x02;
    unsigned char bytes[11] = {0x02, 0x0B, 0x11, 0x02, mntCmd, 15, blsLay,
        HsBleSingle.inst.stageLimit,
        _protoN,  // cellService?.protoN = Int32(cprProtoN.theVal) + 1
        HsBleSingle.inst.manikinKind.theVal + 1, //  default : 1 / prestan : 2
        0x03};  // protoN : 1, 2, 3 ...
    NSData *data = [NSData dataWithBytes:bytes length:sizeof(bytes)];
    
    __weak HSBaseService* wself = self;
    __strong __typeof(wself) this = wself;
    
    HtBaseJob* aJob = [[HtBaseJob alloc] initWithNm:[NSString stringWithFormat:@"monitorCommand >> %@", name]
                                        retryLimitN:5]; // 30번...

    aJob.initJob = ^{
        //[log logUiAction:[NSString stringWithFormat:@"BaseService :: monitorCommand [[ %d ]]", mntCmd] lnum:5 printOn:true];
        monitorAck = false;
        NSLog(@"\t << HsBaseService :: >>> SENDING : data from %@", @"MonitorCommand의 initJob");
        [this writeValue:data withBlock:nil];
    };
    
    aJob.didFinished = ^BOOL(void) {
        //NSLog(@"\n\n  monitorCommand ::   didFinished ??   monitorAck :  %d  \n\n", monitorAck);
        // return monitorAck || !_isTrainerALive;  // 아래 ackReceivedInMonitor로 대체..
        return !_isTrainerALive;
    };
    [pcktMachine addBaseJob:aJob];
}

- (void)ackReceivedInMonitor {
    [pcktMachine ackReceivedAndRemoveJob];
}


//  요 세개...
- (void)instructorAppInfoRequest { // 트레이너가 키트에 Instructor App Info 를 요청하는 함수..  sendAppInfoCalibration 과 동일 패킷.
    unsigned char bytes[6] = {0x02, 0x06, 0x11, 0x01, 0x03, 0x03}; // 2 6 11 1 3 (request) 3
    NSData *data = [NSData dataWithBytes:bytes length:sizeof(bytes)];
    NSLog(@"\t << HsBaseService :: >>> SENDING : data from %@", @"트레이너가 키트에 Instructor App Info 를 요청");
    [self writeValue:data withBlock:nil];
}

- (void)sendAppInfoCalibration { // 캘리브레이션 시작 시 트레이너에게 전달..
    [self logCallerMethodwith:@" sendAppInfoCalibration  . " newLine:20];
    unsigned char bytes[6] = {0x02, 0x06, 0x11, 0x01, 0x03, 0x03};
    NSData *data = [NSData dataWithBytes:bytes length:sizeof(bytes)];
    NSLog(@"\t << HsBaseService :: >>> SENDING : data from %@", @"캘리브레이션 시작 시 트레이너에게 전달");
    [self writeValue:data withBlock:nil];
}

- (void)sendAppInfoResponseTest { // Test가 트레이너에게 전달..
    [self logCallerMethodwith:@" sendAppInfoResponseTest  . " newLine:20];
    unsigned char bytes[6] = {0x02, 0x06, 0x11, 0x01, 0x02, 0x03};
    NSData *data = [NSData dataWithBytes:bytes length:sizeof(bytes)];
    NSLog(@"\t << HsBaseService :: >>> SENDING : data from %@", @"Test 가  트레이너에게 전달");
    [self writeValue:data withBlock:nil];
}

- (void)sendAppInfoResponseMonitor { // 모니터가 트레이너에게 전달..
    [self logCallerMethodwith:@" sendAppInfoResponseMonitor  . " newLine:20];
    unsigned char bytes[6] = {0x02, 0x06, 0x11, 0x01, 0x01, 0x03};
    NSData *data = [NSData dataWithBytes:bytes length:sizeof(bytes)];
    NSLog(@"\t << HsBaseService :: >>> SENDING : data from %@", @"Monitor가 트레이너에게 전달");
    [self writeValue:data withBlock:nil];
}


- (void)requestDValueUpdate {
    [self logCallerMethodwith:@" requestDValueUpdate  . " newLine:20];
    unsigned char bytes[7] = {0x02, 0x07, 0x0a, 0x01, 0x03, 0xd4, 0x03};
    NSData *data = [NSData dataWithBytes:bytes length:sizeof(bytes)];
    NSLog(@"\t << HsBaseService :: >>> SENDING : data from %@", @"requestDValueUpdate");
    [self writeValue:data withBlock:nil];
}


/*
 * Noti가 오는 Characteristic에 대한 처리
 */
- (void)notifyCharacteristicHandler:(YMSCBCharacteristic *)yc error:(NSError *)error {
    if (error) {
        return;
    }
    if ([yc.name isEqualToString:@"all_information"]) {
        NSData *data = yc.cbCharacteristic.value;
        char val[data.length];
        [data getBytes:&val length:data.length];
    }
}

/*
 * Status Infomation에 대한 요청
 */

- (void)requestLabelCheck {
    [self logCallerMethodwith:@" 라벨 체크. " newLine:20];
    //Byte appID = AppIdTrMoTe + 1; // 1, 2, 3
    //unsigned char bytes[5] = {0x02, 0x05, 0x08, appID, 0x03};  // 20160705 변경..
    unsigned char bytes[5] = {0x02, 0x05, 0x0F, 0x03, 0x03};
    NSData *data = [NSData dataWithBytes:bytes length:sizeof(bytes)];
    [self writeValue:data withBlock:nil];
}

- (void)requestKitEnable {
    [self logCallerMethodwith:@" 키트 활성화. " newLine:10];
    unsigned char bytes[5] = {0x02, 0x05, 0x08, productType, 0x03};
    NSData *data = [NSData dataWithBytes:bytes length:sizeof(bytes)];
    [self writeValue:data withBlock:nil];

}

- (void)requestSensorInfo:(Byte)cmd {
    unsigned char bytes[5] = {0x02, 0x05, cmd, 0x03, 0x03};
    NSData *data = [NSData dataWithBytes:bytes length:sizeof(bytes)];
    NSLog(@"\t << HsBaseService :: >>> SENDING : data from %@", @"requestSensorInfo");
    [self writeValue:data withBlock:nil];
}

- (void)ackToMonitor {
    [self logCallerMethodwith:@"<<<<   ACT  TO  MONITOR App    >>> " newLine:10];
    unsigned char bytes[6] = {0x02, 0x06, 0x11, 0x02, 0x12, 0x03};
    NSData *data = [NSData dataWithBytes:bytes length:sizeof(bytes)];
    [self writeValue:data withBlock:nil];
}


/*
 * Calibration Infomation에 대한 요청
 */

- (void)requestCompressionCaliSetData
{
    [self logNewLine:8];
    [self logCallerMethodwith:@" <<  requestCompressionCaliSetData  >>   write : 267013 " newLine:1];
    [self logNewLine:8];
    
    unsigned char bytes[6] = {0x02,0x06,0x07,0x00,0x01,0x03};  // 267013
    NSData *data = [NSData dataWithBytes:bytes length:sizeof(bytes)];

    [self writeValue:data withBlock:nil];
}

- (void)requestRespirationCaliSetData{
    [self logNewLine:8];
    [self logCallerMethodwith:@" <<  requestRespirationCaliSetData   >>   write : 267023 " newLine:1];
    [self logNewLine:8];

    unsigned char bytes[6] = {0x02,0x06,0x07,0x00,0x02,0x03};
    NSData *data = [NSData dataWithBytes:bytes length:sizeof(bytes)];
    
    [self writeValue:data withBlock:nil];
}

//////////////////////////////////////////////////////////////////////       [   Operation  관련   멈추고...  시작하고...  ]
#pragma mark - Operation Infomation에 대한 요청

- (void)requestOperationStart {
    NSLog(@"\n\n\n\n\n");
    [self logCallerMethodwith:@" << requestOperationStart >>   write : 259 0 3 " newLine:0];
    NSLog(@"\n\n\n\n\n");
    
    unsigned char bytes[5] = {0x02,0x05,0x09,0x00,0x03};
    NSData *data = [NSData dataWithBytes:bytes length:sizeof(bytes)];
    __weak HSBaseService* wself = self;
    __strong __typeof(wself) this = wself;
    
    HtBaseJob* aJob = [[HtBaseJob alloc] initWithNm:@"Operation Start" retryLimitN:5];

    aJob.initJob = ^{
        //[log logUiAction:[NSString stringWithFormat:@"  Operation start   initjob  operationon : %d", _weakManager.operationOn] lnum:5 printOn:true];
        
        ack88received = false;
        if (_weakManager.operationOn)
            ack88received = true;  // 그냥 넘기도록.
        else {
            NSLog(@"\t << HsBaseService :: >>> SENDING : data from %@", @"^{ operation Start InitJob }");
            [this writeValue:data withBlock:nil];
        }
    };
    aJob.didFinished = ^BOOL(void) {
        //NSLog(@"\n   requestOperationStart  didFinished  ??  >>>   BleManager.operationOn : %d,   ack88received ?  : %d \n", _weakManager.operationOn, ack88received);
        if (ack88received)
            _weakManager.operationOn = true;
        return ack88received;
    };
    [pcktMachine addBaseJob:aJob];
}

- (void)requestOperationStop {
    NSLog(@"\n\n\n\n\n");
    [self logCallerMethodwith:@" <<  requestOperationStop  >>   write : 259 1 3 " newLine:0];
    NSLog(@"\n\n\n\n\n");
    
    unsigned char bytes[5] = {0x02,0x05,0x09,0x01,0x03};
    NSData *data = [NSData dataWithBytes:bytes length:sizeof(bytes)];
    __weak HSBaseService* wself = self;
    __strong __typeof(wself) this = wself;
    
    HtBaseJob* aJob = [[HtBaseJob alloc] initWithNm:@"Operation Stop" retryLimitN:5];
    
    aJob.initJob = ^{
        //[log logUiAction:[NSString stringWithFormat:@"  Operation Stop   initjob ==>>> : %d", _weakManager.operationOn] lnum:5 printOn:true];
        ack88received = false;
        if (_weakManager.operationOn) {
            NSLog(@"\t << HsBaseService :: >>> SENDING : data from %@", @"^ { operation Stop initJob }");
            [this writeValue:data withBlock:nil];
        } else
            ack88received = true;
    };
    aJob.didFinished = ^BOOL(void) {
        //NSLog(@"\n  requestOperationStop    didFinished  ??  >>>   BleManager.operationOn : %d,   ack88received ?  : %d \n", _weakManager.operationOn, ack88received);
        
        
        if (ack88received)
            _weakManager.operationOn = false;
        return ack88received;
    };
    [pcktMachine addBaseJob:aJob];
}

//////////////////////////////////////////////////////////////////////       [   Retry 관련 ]
#pragma mark - Retry Logic
- (UnitJob*)addUnitJob:(void (^)(void))withEntryAction withFinishCondition:(BOOL (^)(void))finishCondition   andName:(NSString*)name {  //  간격은 0.5초.. 회수는 5번.
    [self logCallerMethodwith:@" <<  HSBaseService ::  리트라이 시도 시작 ..______  >>  " newLine:2];
    UnitJob *nJob = [[UnitJob alloc] initWithName:name];
    nJob.initJob = withEntryAction;
    nJob.didFinished = finishCondition;
    nJob.delayTime = 0.5;  nJob.retryLimit = 5;

    if (retryJob != nil) { [retryJob cancelAllActions]; }
    
    retryJob = nJob;
    [retryJob startProcess];
    
//    if (retryJob == nil) {
//        retryJob = nJob;  // 일이 끝났으면 교체하거나.
//        [retryJob startProcess];
//    } else {
//        if (retryJob.myJobWasDone) {
//            retryJob = nJob;  // 일이 끝났으면 교체하거나.
//            [retryJob startProcess];
//        } else {
//            [retryJob getEndOfNextJob].nextJobObj = nJob; // 마지막에 등록.
//        }
//    }
    return nJob;
}

- (void)receivedData:(CBCharacteristic *)characteristic {
    NSData *data = characteristic.value;    unsigned char val[data.length];
    [data getBytes:&val length:data.length];
    
    if (data.length < 5)         return;
    
    int len = val[1] & 0xFF, bypass = val[2] & 0xFF, ack18 = val[4] & 0xFF; //, cmd = val[2] & 0xFF, msg = val[3] & 0xFF, vl4 = val[4] & 0xFF;
    //char charLen = val[1] & 0xFF;
    
    //NSLog(@"  len : 136이면 애크 : %@     %d  ", data, len);
    if (len == 136) { // || charLen == -120) {
        //NSLog(@"\n\n\n\n\n\n  \t\t\t\t HsBaseService : ##### received     현재 작업 : < %@ > 에 대한 ACK_88 #####  \n\n\n\n\n\n\n\n", retryJob.jobName);
        ack88received = true;
    }
    if (bypass == 17 && ack18 == 18) {
        NSLog(@"HsBaseService :: receivedData >>  bypass == 17 && ack18 == 18");
        monitorAck = true;
    }
}

//////////////////////////////////////////////////////////////////////       [ requestCompressionCaliEditData & requestRespirationCaliEditData ]
#pragma mark - Calibration Edit에 대한 요청
- (void)requestCompressionCaliEditData:(UInt16)compValue {
    [self logCallerMethodwith:[NSString stringWithFormat:@"  압력 값 키트에 쓰기 >>   compValue : %d", compValue] newLine:10];
    unsigned char bytes[7] = {0x02,0x07,0x07,0x01,0x01,compValue,0x03};
    NSData *data = [NSData dataWithBytes:bytes length:sizeof(bytes)];
    [self writeValue:data withBlock:nil];
}

- (void)requestRespirationCaliEditData:(UInt16)rsprValue{
    [self logCallerMethodwith:[NSString stringWithFormat:@"  호흡 값 키트에 쓰기 >>   rsprValue : %d", rsprValue] newLine:10];
    unsigned char bytes[7] = {0x02,0x07,0x07,0x01,0x02,rsprValue,0x03};
    NSData *data = [NSData dataWithBytes:bytes length:sizeof(bytes)];
    [self writeValue:data withBlock:nil];
}

//////////////////////////////////////////////////////////////////////   writeValue withBlock
- (void)writeValue:(NSData *)data withBlock:(void (^)(NSError *error))writeCallback {
    //  /*   4 Debugging
    unsigned char datas[data.length];
    [data getBytes:&datas length:data.length];
    //UInt16 len = datas[1] & 0xFF;    UInt16 cmd = datas[2] & 0xFF;
    //NSLog(@"\n\n\n\n");

    NSLog(@"\n\n\n  \t\t====================>>>>>   패킷 송신  ============>>>>>  :: \t\t [ %@ ]\n\n\n", data);
    // 160801 여기서 크래쉬... NSLog(@"\t << HsBaseService :: >>> SENDING : data : %@  >> len : , cmd :    \t\t====================>>>>>   패킷 송신  =====>>>>> %@ \n\n", data, _titleAtoD);

    YMSCBCharacteristic *battery_infoCt = self.characteristicDict[@"all_information"];
    [battery_infoCt writeValue:data withBlock:writeCallback];
}


@end


//3456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
//////////////////////////////////////////////////////////////////////       [ CLASSname ]

