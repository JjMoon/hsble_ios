//
//  HSCentralManager.h
//  heartisense
//
//  Created by jeongyuchan on 2015. 4. 20..
//  Copyright (c) 2015년 gyuchan. All rights reserved.
//

#import "YMSCBCentralManager.h"

@interface HSCentralManager : YMSCBCentralManager
{
    
    
}
/**
 Return singleton instance.
 @param delegate UI delegate.
 */
//+ (HSCentralManager *)initSharedServiceWithDelegate:(id)delegate;


/**
 Return singleton instance.
 */
//+ (HSCentralManager *)sharedService;

- (instancetype)initWithDelegate:(id)delegateObj;

- (void)removeAllPeripherals;

@end
