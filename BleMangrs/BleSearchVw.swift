//
//  BleSearchVwPrtc.swift
//  Trainer
//
//  Created by Jongwoo Moon on 2016. 10. 24..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation

class BleSearchVw: UIView {
    var log = HtLog(cName: "BleSearchVw")

    var ble: HsBleCtrl?
    var container: HtTableView!

    var parentView: KLCPopup?
    var disMissed = { }

    @IBOutlet weak var btnRefresh: UIButton!

    var scanning = false

    @IBOutlet weak var lblTitle: UILabel!

    @IBAction func actionRefrsh(sender: AnyObject) {
        log.printAlways("  actionRefresh  ", lnum: 5)
        btnRefresh.rotate360Degrees(repeatCnt: 3)
        openPort()
    }

    func searchBleViewSet(bleObj: HsBleCtrl, parent: KLCPopup) { // 초기 작업.
        ble = bleObj
        parentView = parent

        ble!.scanFinished = {
            self.scanning = false
            self.log.printAlways("  self.scanning = false  ", lnum: 1)
        }
        ble!.actNewPortFound = {
            self.refreshTable()
        }

        ble!.portOpened = { prt in
            self.parentView?.dismiss(true)  // 접속되면 parent view 를 닫는다..
            self.disMissed() // 이건 VC 에 알리는 콜백.
        }
        if container == nil {
            setInitTableView()
        }
        openPort()
    }

    /// 초기 세팅..
    func setInitTableView() {
        lblTitle.text = "키트 이름"

        /// container 생성 및 세팅..
        let wid: CGFloat = 400, hgt: CGFloat = 200
        let rct = CGRect(x: 0, y: 40, width: wid, height: hgt - 40)
        container = HtTableView(frame: rct)
        container.initSet(rct, scroll: true)
        container.tableView.allowsSelection = true  // 선택 모드는
        container.tableView.allowsMultipleSelection = false  // 싱글로...

        container.selectedCallback = { cell in
            let ble = cell as! BlePortView
            self.ble!.openPort(ble.port)
        }

        //container.offset = 3
        //container.backgroundColor = UIColor.whiteColor()
        container.data = [ HtTableSection(title: "", headerHeight: 0,
            arrRow: [ ])
        ]
        //container.cornerRad(10)
        cornerRad(10)
        addSubview(container)
    }

    func openPort() {
        ble!.stopScan()
        ble!.startScan()
        scanning = true
        refreshTable()
        //timer = NSTimer.scheduledTimerWithTimeInterval(2.5, 
        //   target: self, selector: #selector(self.refreshTimer), userInfo: nil, repeats: true)
    }

    /// 새로운 포트가 추가된 경우에 리로드..
    func refreshTable() {
        ble!.debugPrintPortNames()

        container.data[0].arrRow = [ ]
        for p in ble!.allPorts {
            let lbl = BlePortView(frame: CGRect(x: 0, y: 0, width: frame.width, height: 50))
            lbl.set(p)
            container.data[0].arrRow.append(lbl)
        }
        container.tableView.reloadData()
    }
}


class BlePortView : UILabel {
    var port: BLEPort!
    func set(prt: BLEPort) {
        port = prt
        self.text = prt.name.subStringFrom(4)
        alignCenter()
    }
}

