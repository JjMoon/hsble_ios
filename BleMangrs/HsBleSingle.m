//
//  HsBleSingle.m
//  Trainer
//
//  Created by Jongwoo Moon on 2015. 11. 11..
//  Copyright © 2015년 IMLab. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "HsBleSingle.h"
#import "NSObject+Util.h"
#import "Common-Swift.h"


//////////////////////////////////////////////////////////////////////     [ HsBleManager ]
@interface HsBleSingle()
//////////////////////////////////////////////////////////////////////////////////////////
{
    NSTimer* oneSecTimer;
}


// BLE Related ..
//@property (nonatomic, weak) HSPeripheral *xxhsPeripheral;
//@property (nonatomic, weak) HSBaseService *hsBaseService;


@end

//////////////////////////////////////////////////////////////////////     _//////////_     [ HsBleManager ]    _//////////_
@implementation HsBleSingle
//////////////////////////////////////////////////////////////////////////////////////////

//@synthesize mtmTimer, bypass, stateMan;
//@synthesize arrUnitJob, arrBle;

//static HsBleManager *sharedMyManager;

+(id)inst {
    static HsBleSingle *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (void)setLanguageString {
    // 언어 객체 할당..
    [_shlObj setStrings];
    _langObj = _shlObj.obj;
    NSLog(@" HsBleSingle :: setLanguageString ::  langIdx : %d    Yes >>  %@ ", langIdx, _langObj.yes);
}



-(instancetype)init
{
    self = [super init];
    oneSecTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self
                                                 selector:@selector(oneSecTimerAction) userInfo:nil repeats:YES];
    self.stageLimit = 3;
    NSLog(@"\n\n\t\tHsBleSingle : Singleton Creation ...  >Shl<   \n\n");
    //[HsGlobal delay:2.0 closure:^{
    NSUserDefaults *dObj = [NSUserDefaults standardUserDefaults];
    //HsBleManager.inst.stage = 1;
    if ([dObj integerForKey:@"StageLimit"])
        _stageLimit = (int)[dObj integerForKey:@"StageLimit"];
    //if ([dObj boolForKey:@"ManikinDirection"]) {
        NSLog(@"Read ManikinDirection");
        isManikinLeft = [dObj boolForKey:@"ManikinDirection"];
    //}
    if ([dObj integerForKey:@"Language"]) {
        langIdx = (int)[dObj integerForKey:@"Language"];
    }
//    if ([dObj integerForKey:@"Guideline"]) {
//        guideLine = (int)[dObj integerForKey:@"Guideline"];
//    }

    NSLog(@"\t\t HsBleSingle   >Shl<  langIdx : %d, stageLimit : %d   %@  manikinLeft : %d",
          langIdx, _stageLimit, _langObj.yes, isManikinLeft);
    //}];
    return self;
}

- (void)oneSecTimerAction {
}


@end
