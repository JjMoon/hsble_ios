//
//  HsBleMaestr.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2015. 11. 11..
//  Copyright © 2015년 IMLab. All rights reserved.
//

import Foundation

//////////////////////////////////////////////////////////////////////     _//////////_     [ Breath      << >> ]    _//////////_   호 흡
// MARK: DataParsing :  호흡 관련 breath


///  192. 168.0.23 : 80 ..   admin@imlabworld.com   admin140123  접속..

class HsBleMaestr: NSObject { // singletone..   >>>  HsBleMaestr.inst  <<<
    var log = HtLog(cName: "<< HsBleMaestr >>")
    static let inst = HsBleMaestr()
    
    var parseState: InstructState = .StandBy
    
    var usrDefObj = HtUserDef()
    var arrBleSuMan = [HsBleSuMan]()
    var arrStudent = [HmStudent]() {
        didSet {
            reloadStudentData()
        }
    }
    var reloadStudentData = {}

    var fmdbHandler = HsMainDB()

    var tempBle: HsBleSuMan?
    
    var isAllInOne: Bool { get {
        switch globalState {
        case .S_WHOLESTEP_DESCRIPTION, .S_WHOLESTEP_COMPRESSION, .S_WHOLESTEP_BREATH, .S_WHOLESTEP_AED, .S_WHOLESTEP_PRECPR, .S_WHOLESTEP_RESULT:
            return true
        default:
            return false
        } } }

    /** 생성 후 초기 작업.     */
    override init() {
        super.init()
        log.printThisFunc(" init  ", lnum: 5)
        globalState = .S_INIT0
        arrBleSuMan = [ HsBleSuMan(pName: "A"), HsBleSuMan(pName: "B"), HsBleSuMan(pName: "C"), HsBleSuMan(pName: "D") ]

        let nickList = [ "A", "B", "C", "D" ]
        for (idx, obj) in arrBleSuMan.enumerate() {
            obj.index = idx
            obj.connectedName = usrDefObj.arrPrevDevName![idx]
            obj.nick = nickList[idx]
            log.logThis(" ble [\(obj.name)] 의 connectedName :: \(obj.connectedName)   at \(idx)")
        }

        HsGlobal.delay(0.1) { () -> () in self.initProcess()  }
    }

    // MARK: - init Process
    private func initProcess() {

        log.printThisFunc("initProcess() ", lnum: 4)
        fmdbHandler.checkAndMakeDB()
        readDB()
    }

    /**  디비 전체를 읽어 들인다.  readAppDependentData 를 통해 모/테 구별.  
     - 학생 매니지 viewDidLoad 에서 다시 읽어들임.  */
    func readDB() {
        log.printThisFunc("readDB ", lnum: 20)
        fmdbHandler.versionCheckAndUpgrade_Ver2()
        fmdbHandler.versionCheckAndUpgrade_Ver3()
        fmdbHandler.versionCheckAndUpgrade_Ver4()

        if AppIdTrMoTe == 1 {
            fmdbHandler.versionCheckAndUpgrade_Monitor1()
        }
        if AppIdTrMoTe == 2 {
            //fmdbHandler.versionCheckAndUpgrade_Test1()
            //fmdbHandler.versionCheckAndUpgrade_Test2()
            //fmdbHandler.versionCheckAndUpgrade_Test3()



            fmdbHandler.versionCheckAndUpgrade_Test4()
        }

        arrStudent = fmdbHandler.grabAllStudents()
        fmdbHandler.readStudentDataSetInfo()

        fmdbHandler.reloadStudentTestInfo()
        //fmdbHandler.readAppDependentData() // 테스트 에서 의미 있다.

        for stnt in arrStudent { stnt.showMySelf() }
        manageMaxStudentNumber()
    }

    func manageMaxStudentNumber() {
        let num = arrStudent.count - studentTableLimit
        if 0 < num {
            for _ in 0..<num {
                arrStudent.removeFirst()
            }
        }
    }

    //////////////////////////////////////////////////////////////////////       [ UI Sub  ]

    /// 특정 학생을 기준으로 어레이 앞/뒤 제일 근처에서 조건을 만족하는 놈 골라내기.
    func getNeighborStudent(isNext: Bool, curStu: HmStudent, filterF: (HmStudent) -> Bool) -> HmStudent? {
        let sliced = arrStudent.split(curStu, maxSplit: 1, allowEmptySlices: true)
        if isNext {
            if 0 == sliced[1].count { return nil }
            return sliced[1].filter(filterF).first
        }
        if 0 == sliced[0].count { return nil }
        return sliced[0].filter(filterF).last
    }

    func getData(isNext: Bool, bleMan: HsBleSuMan?, dbStud: HmStudent?) -> HsData? {

        if bleMan == nil {
            let sliced = arrStudent.split(dbStud!, maxSplit: 1, allowEmptySlices: true)
            if isNext {
                if 0 == sliced[1].count { return nil }
                return sliced[1].filter({ (std) -> Bool in std.isDataInDB }).first?.myDataFromDB
            }
            if 0 == sliced[0].count { return nil }
            return sliced[0].filter({ (std) -> Bool in std.isDataInDB }).last?.myDataFromDB
        }
        return nil
    }

//    func getDataOld(isNext: Bool, bleMan: HsBleSuMan?, dbStud: HmStudent?) -> HsData? {
//        if bleMan == nil {
//            if isNext {
//                return arrStudent.getNextMatch(dbStud!, filter: { (std: HmStudent) -> Bool in std.isDataInDB })?.myDataFromDB
//            } else {
//                return arrStudent.getPrevMatch(dbStud!, filter: { (std: HmStudent) -> Bool in std.isDataInDB })?.myDataFromDB
//            }
//        }
//        if isNext {
//            arrBleSuMan.getNextMatch(bleMan!) { (ble) -> Bool in ble.isConnectionTotallyFinished }?.dataObj
//        } else {
//            arrBleSuMan.getPrevMatch(bleMan!) { (ble) -> Bool in ble.isConnectionTotallyFinished }?.dataObj
//        }
//
//        return nil
//    }

    /// 테스트 후 P, F 모두 선택했는지..  모니터도 처리해야 함..
    func allTesterEvaluated() -> Bool {
        for bObj in arrBleSuMan {
            if bObj.curStudent != nil && !bObj.curStudent!.evalFinished {

                return false
            }
            if AppIdTrMoTe == 2 && bObj.curStdntSub != nil && !bObj.curStdntSub!.evalFinished {
                return false
            }
        }
        return true
    }

    func numOfUiTargetedStudents() -> Int {
        let arrStu = arrStudent.filter { (obj) -> Bool in
            return obj.uiTargeted
        }
        return arrStu.count
    }

    func uiTargetResetAllStudent() {
        for sobj in arrStudent {
            sobj.uiTargeted = false
        }
    }

    func switchToEntireProcess() { // 버튼으로 변환 시..
        sendCommand(.S_WHOLESTEP_DESCRIPTION)
        HsBleMaestr.inst.setParsingState(.Entire)  //(AllInOne10)
    }

    func didAllConnectedEntireProcessFinished() -> Bool {
        let conObjs = connectedBleObjs()
        for obj in conObjs {
            //if !(obj.bleState == .S_WHOLESTEP_RESULT || obj.bleState == .S_WHOLESTEP_AED) { return false }
            if !(obj.stt == .S_WHOLESTEP_RESULT) { return false }
        }
        return true
    }

    func studentArrayForSend() -> [HmStudent] { // myDataFromDB가 있는 놈들.
        log.printThisFunc("studentArrayForSave")
        for obj in arrStudent {
            log.logThis("  student.myData >>> : \(obj.myData),  ...myDataFromDB : \(obj.myDataFromDB) ")
        }
        return arrStudent.filter({ (sObj) -> Bool in return sObj.myDataFromDB != nil })
    }
    
    func resetData() {
        for sObj in arrStudent { sObj.myData = nil }
    }

    //////////////////////////////////////////////////////////////////////       [ Bypass Command ]
    func setParsingState(newState: InstructState) {
        parseState = newState // 내 변수에도 세팅..
        let conObjs = arrBleSuMan.filter { (bleObj) -> Bool in  return !bleObj.isPortClosed }
        for obj in conObjs {
            obj.instrctStt = newState
            //if newState == InitCalibration00 { obj.operationStop() }    else { obj.operationStart() }
        }
    }
    
    func sendCommand(pState: CurStep) {
        globalState = pState
        log.logUiAction(" 모니터 명령 :: \(globalState) ", lnum: 5)

        // 전과정에서 ble 의 step..  같이 맞춰줄 것...
        switch globalState {
        case .S_STEPBYSTEP_SAFETY:      sendScreenCommand(21) // 0x15
        case .S_STEPBYSTEP_AIRWAY:      sendScreenCommand(19) // 0x13
        case .S_STEPBYSTEP_CHECK_BRTH:  sendScreenCommand(20) // 0x14

        case .S_STEPBYSTEP_RESPONSE:    sendScreenCommand(3) // 이 코드는 안드로이드와 공통..
        case .S_STEPBYSTEP_EMERGENCY:   sendScreenCommand(4)
        case .S_STEPBYSTEP_CHECKPULSE:  sendScreenCommand(5)
        case .S_STEPBYSTEP_COMPRESSION: sendScreenCommand(6)
        case .S_STEPBYSTEP_BREATH:      sendScreenCommand(7)
        case .S_STEPBYSTEP_AED:         sendScreenCommand(8)
        case .S_WHOLESTEP_DESCRIPTION:  sendScreenCommand(9)
        for obj in connectedBleObjs() {
            obj.startWholeInOneProcess()  // 이게 맞을까 ?
            obj.operationStart()  // 160730
            }
        case .S_WHOLESTEP_PRECPR:       sendScreenCommand(10)
        //  160730   for obj in connectedBleObjs() { obj.operationStart() }
        case .S_WHOLESTEP_RESULT:       sendScreenCommand(14)
        case .S_READY:                  sendScreenCommand(15)
        case .S_COMPLETE:               sendScreenCommand(18)
        case .S_QUIT:                   sendScreenCommand(16)
        //case .S_WHOLESTEP_COMPRESSION:  sendScreenCommand(20)
        //case .S_WHOLESTEP_BREATH:       sendScreenCommand(21)
        default: break
        }
        for bleman in arrBleSuMan { // 전체 단계를 맞춰야한다..
            bleman.stt = globalState
        }

        switch globalState {
        case .S_STEPBYSTEP_COMPRESSION, .S_STEPBYSTEP_BREATH, .S_WHOLESTEP_DESCRIPTION:
            operationControl(true)
        case .S_WHOLESTEP_PRECPR, .S_WHOLESTEP_COMPRESSION, .S_WHOLESTEP_BREATH:  // 160730
            log.printAlways(" Skip Send [Operation 관련] Command in Entire step ... >>> ", lnum: 5)
        default:
            operationControl(false)
        }
        
        switch globalState {
        case .S_STEPBYSTEP_RESPONSE, .S_STEPBYSTEP_EMERGENCY, .S_STEPBYSTEP_CHECKPULSE, .S_STEPBYSTEP_COMPRESSION, .S_STEPBYSTEP_BREATH, .S_STEPBYSTEP_AED:
            let conObjs = arrBleSuMan.filter { (bleObj) -> Bool in  return !bleObj.isPortClosed }
            for obj in conObjs { obj.dataObj.reset() } //  데이터 리셋..  카운터 등...
            setParsingState(.Step)
        case .S_WHOLESTEP_AED, .S_WHOLESTEP_DESCRIPTION, .S_WHOLESTEP_PRECPR, .S_WHOLESTEP_RESULT, .S_WHOLESTEP_COMPRESSION, .S_WHOLESTEP_BREATH:
            setParsingState(.Entire)
        default:
            setParsingState(.StandBy)
        }
    }
    
    private func operationControl(isStart: Bool) {
        for obj in self.arrBleSuMan {
            if isStart { obj.operationStart() }
            else { obj.operationStop() }
        }
    }
    
    private func sendScreenCommand(pMsg: UInt16) {
        let sttMsg = HsUtil.curStepLocalized()
        log.printThisFunc("sendScreenCommand")
        for obj in arrBleSuMan {
            if !obj.isPortClosed {
                obj.sendMonitorCommand(pMsg, msg:sttMsg)
            }
        }
    }
    
    //////////////////////////////////////////////////////////////////////       [ 이름, 이메일 검색  학생 등록/수정 ]
    func checkValidityNameEmail(nm: String, em: String) -> String? { // nil 이면 통과.. 아니면 오류 메시지..
        if nm.getLengthUTF32() == 0 { return langStr.obj.no_named } // 이름 없음.
        //print("  em.getLengthUTF32()  \(em.getLengthUTF32())   ")
        if 0 < em.getLengthUTF32() && !em.isValidEmail() {
            return langStr.obj.not_valid_email
        }

        for obj in arrStudent { // 중복된 이름 체크.
            if obj.name == nm {
                // return langStr.obj.existing_name
            }
        }
        return nil
    }

    func addStudent(nm: String, em: String, pw: String, phn: String, ag: Int) -> Bool {
        // for obj in arrStudent { if obj.name == nm { return false } }  같은 이름 제외하는 부분...
        let newObj = HmStudent(nm: nm, em: em, pw: pw, phn: phn, ag: ag)
        arrStudent.append(newObj)
        fmdbHandler.addAStudent(newObj)

        readDB() // 최대 표시 숫자 제한 때문에...

        //log.printAlways("  읽은 학생 이름... ::  \(newObj.name)   DB ID : \(newObj.dbid) ", lnum: 2)
        return true
    }

    func deleteServerSentStudents() {
        let students = arrStudent.filter { (std) -> Bool in
            return std.svrSaved == 1
        }
        log.printThisFNC("deleteServerSentStudents", comment: " 서버로 보낸 학생 수 : \(students.count)")
        for tar in students {
            if fmdbHandler.deleteStudent(tar) {
                log.printAlways(" Error   set   uiTargeted = false  ", lnum: 10)
            } else { log.printAlways("  \(tar.dbid) | \(tar.name)  is removed from Database   OK  no error ", lnum: 3)  }
        }
        readDB()
    }

    func deleteStudents() {
        log.printThisFunc("deleteStudents()", lnum: 5)
        for tar in arrStudent {
            if tar.uiTargeted {
                if fmdbHandler.deleteStudent(tar) {
                    log.printAlways(" Error   set   uiTargeted = false  ", lnum: 10)
                } else { log.logThis("  \(tar.dbid) | \(tar.name)  is removed from Database", lnum: 1)  }
            }
        }
        readDB() // 아래로 하면 테스트에서 삭제할 때 액션이 이상해진다.
        //arrStudent = fmdbHandler.grabAllStudents()        fmdbHandler.readStudentDataSetInfo()
    }

    func deleteStudents(stdArr: [HmStudent]) {
        for std in stdArr {
            if fmdbHandler.deleteStudent(std) {
                log.printAlways("  deleteStudents(stdArr: [HmStudent])   Error   set   uiTargeted = false  ", lnum: 10)
            } else { log.logThis(" deleteStudents(stdArr: [HmStudent])   \(std.dbid) | \(std.name)  is removed from Database", lnum: 1)  }
            arrStudent.removeAtIndex(arrStudent.indexOf(std)!)
        }
        readDB()
    }
    
    func deviceConnected(bleSuMan: HsBleSuMan) {
        log.printThisFunc(" deviceConnected :: maestro \(bleSuMan.prevKitName) ", lnum: 3)
        // 기존에 이 이름이 있다면 지우고..
        for (idx, prevName) in (usrDefObj.arrPrevDevName?.enumerate())! {
            print("    idx : \(idx) \t " + prevName + "  /t " + bleSuMan.connectedName)
            if prevName == bleSuMan.connectedName {
                print("  이름 지우기 ..  \(prevName)")
                usrDefObj.arrPrevDevName![idx] = "-"
            }
        }

        // 다시 그 위치에 쓴다.
        for (idx, ble) in arrBleSuMan.enumerate() {
            if ble == bleSuMan {
                print("  idx : \(idx)   새 이름 쓰기 : \(bleSuMan.connectedName)")
                usrDefObj.arrPrevDevName![idx] = bleSuMan.connectedName
            }
        }
        log.logThis("   \(usrDefObj.arrPrevDevName!) ", lnum: 0)
        usrDefObj.writePrevDevName()
    }

    //////////////////////////////////////////////////////////////////////       [ 디버깅용 ]
    func showStudentsTest() {
        for ble in arrBleSuMan {
            log.printThisFNC(" Ble 의 학생 정보 ", comment: "")
            log.logThis(" \(ble.name) >>  연결 상태 : \(ble.isPortClosed)  학생 : \(ble.curStudent)   학생 Sub : \(ble.curStdntSub) ")
            if ble.curStudent != nil { ble.curStudent!.debugMyStateInTest() }
            if ble.curStdntSub != nil { ble.curStdntSub!.debugMyStateInTest() }
        }
        log.endFunctionLog()
    }
    
}
