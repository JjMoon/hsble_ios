//
//  HsBleMaestr+Sort.swift
//  HS Monitor
//
//  Created by Jongwoo Moon on 2016. 9. 19..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation

extension HsBleMaestr {

    func sortByDate(isAsend: Bool) {
        let compr: (HmStudent, HmStudent) -> Bool = isAsend ? { (sta, stb) -> Bool in return sta.dbid < stb.dbid } :
            { (sta, stb) -> Bool in return sta.dbid > stb.dbid }

        arrStudent = arrStudent.sort(compr)
    }

    func sortByName(isAsend: Bool) {
        let ascd: NSComparisonResult = isAsend ? .OrderedAscending: .OrderedDescending
        arrStudent = arrStudent.sort({ (t1, t2) -> Bool in
        t1.name.compare(t2.name) == ascd
        })
    }
}