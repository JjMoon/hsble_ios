//
//  HsBleSuMan.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2015. 11. 23..
//  Copyright © 2015년 IMLab. All rights reserved.
//

import Foundation


class HsBleSuMan : HsBleCtrl {
    //var log = HtLog(cName: "HsBleSuMan")
    
    var name = "SuperManager" // LeftUp 
    //var nick = "N"
    var index = 0
    var connectVw:ConnectUnitView?
    var curJob: UnitJob?
    var curStudent: HmStudent?, curStdntSub: HmStudent?
    var testObj: HmTest? // 애/어른 공통..
    var dataSaved = false // 모니터에서만 쓰임.., subDataSaved = false
    var spdLgc = HsSpeedLgcMon() // 모니터, 테스트 에서는 여기서 생성해서 calc에 할당하자..  트레이너에서는 뷰에서 생성함...

    var nick = "A", connectedName = ""

    var calibrationMode = false

    //var goToSelectionModeView: (Void -> Void)?

    var isTrainerAlive: Bool {
        get { return true } // cellService.isTrainerALive }
    }

    override init() {
        super.init()
        log.clsName = "> HsBle Su Man <"
    }
    
    convenience init(pName: String) {
        self.init()
        name = pName
        calcManager.speedLogic = spdLgc

        initJob()
    }
    
    //////////////////////////////////////////////////////////////////////   Property
    var prevKitFullName:String {
        get { return HsBleMaestr.inst.usrDefObj.arrPrevDevName![index] }
    }
    
    var prevKitName:String {
        get {
            let rStr = HsBleMaestr.inst.usrDefObj.arrPrevDevName![index]
            if rStr.lengthOfBytesUsingEncoding(NSASCIIStringEncoding) == 1 { return rStr }
            if rStr.lengthOfBytesUsingEncoding(NSASCIIStringEncoding) > 4 { return rStr.subStringFrom(4) }
            return "_"
        }
    }

    var isTherePrevKit: Bool {
        get { return prevKitName.lengthOfBytesUsingEncoding(NSASCIIStringEncoding) > 4 }
    }

//    func connectCompletedActionxx() {
//        log.bleObj = self; log.clsName = "HsBleSuMana"
//        log.printThisFNC("connectCompletedAction", comment: ">> BleSuMan <<")
//    }

    override func savePrevKit() {
        print("   savePrevKit>>>  \(thePort?.name)   \(thePort?.name.subStringFrom(4))")
        connectedName = thePort!.name //.subStringFrom(4)
    }

    //////////////////////////////////////////////////////////////////////   Local Data Management
    // MARK: Data Save
    func saveInfantRescure(is1stRescure: Bool) {
        log.printThisFNC("saveInfantRescure1", comment: "  Test :: 영아 :  데이터 저장 시작.. ")
        if testStudentNil(is1stRescure) { return } // student 가 nil 일 때 예외 처리.

        let infant = testObj as! HmTestInft

        HsBleMaestr.inst.fmdbHandler.addInfantInfo(infant, student: curStudent!)
        infant.showMySelf()
        //let inserted = HsBleMaestr.inst.fmdbHandler.grabInfantInfo(parStudent, is1st: is1stRescure) // 지금 입력한 놈..
        //inserted?.showMySelf()
    }

    func testOperationStart() {
        startWholeInOneProcess() // 나레이션 부분...
        stt = .S_WHOLESTEP_PRECPR  // 기다리는 부분...
        operationStart()
        startCompressionProcess()
    }

    func saveAdultRescure(is1stRescure: Bool) {
        log.printThisFNC("saveAdultRescure", comment: "  Test :: 성인 : 구조자 1? \(is1stRescure)   데이터 저장 시작.. ")
        if testStudentNil(is1stRescure) { return }
        let adlt = testObj as! HmTestAdlt
//        var parStudent : HmStudent
//        if is1stRescure {
//            parStudent = curStudent!
//        } else {
//            parStudent = curStdntSub! } // 학생과 sub 학생 중에서 택일..
        HsBleMaestr.inst.fmdbHandler.addAdultInfo(adlt, student: curStudent!)
        adlt.showMySelf()
        let inserted = HsBleMaestr.inst.fmdbHandler.grabAdultInfo(curStudent!) // 지금 입력한 놈..
        inserted?.showMySelf()
        if is1stRescure {
            HsBleMaestr.inst.fmdbHandler.saveDataAfterEntireProcess(curStudent!)
        }
    }

    func uiCountInfo() -> String {
        var rStr = ""
        switch (stt) {
        case .S_STEPBYSTEP_COMPRESSION, .S_WHOLESTEP_COMPRESSION:
            rStr = "\(dataObj.ccCorCount) / \(dataObj.count)"
        case .S_STEPBYSTEP_BREATH, .S_WHOLESTEP_BREATH:
            rStr = "\(dataObj.rpCorCount) / \(dataObj.count)"
        default: rStr = "-"
        }
        return rStr;
    }

    func testStudentNil(is1stResc: Bool) -> Bool {
        if is1stResc { return curStudent == nil }
        return curStdntSub == nil
    }

    func uiMessageOff() {
        
    }

    //////////////////////////////////////////////////////////////////////   Property
    // MARK:   완전 연결 후 클로져 연결..
    func setConnectView(conVw: ConnectUnitView) { // 완전 연결 후 클로져 연결..
        log.printAlways("완전 연결 후 클로져 연결  \(nick) ")
        connectVw = conVw
        calcManager.speedLogic = spdLgc
        //goToSelectionModeView = {
            // self.connectVw?.connectionFinishedClojure()        }
    }

    func logCurrentState() {
        if stt != globalState {
            print("\n\n\n\n\n\n\n\n\n\n  currentStep != stepEntire   전과정 ..  .  \n\n\n\n\n\n\n\n\n\n")
        } else {
            print("\n\n   HsBleSuMan ::     curState : \(HsUtil.curStepStr()),   stepEntire : \( HsUtil.curStepStr(stt) )  \n\n")
        }
    }

    func sendCalibrationToTrainer() {
        log.logUiAction("sendCalibrationToTrainer")
        self.writeData([ 0x02, 0x06, 0x11, 0x01, 0x03, 0x03 ])
    }

    func reset() {
        curJob = nil
        curStudent = nil; curStdntSub = nil
        //operationStop()
        //stopScan()
        //isConnected = false
        //isConnectionTotallyFinished = false
        //removePeripherals() // 160215 크래쉬 원인이었슴.
        //super.reset()
        calcManager.speedLogic = spdLgc
    }

    func connectedActions() {
        if isCalibration { return }
        calcManager.speedLogic = spdLgc
        sendMonitorCommand(15, msg: "0x0F Ready")
    }

    func setSpeedLogic() {
        if AppIdTrMoTe == 1 {
            calcManager.speedLogic = spdLgc
        } else if AppIdTrMoTe == 2 {
            calcManager.speedLogic = nil
        }
    }

    func sendAppInfoResponseMonitor() {
        writeData([ 0x02, 0x06, 0x11, 0x01,    0x01, 0x03 ])
    }
    func sendAppInfoResponseTest() {
        writeData([ 0x02, 0x06, 0x11, 0x01,    0x02, 0x03 ])
    }
    
    func sendPauseCommand(isPauseCase: Bool) {
        var pk: [UInt8] = [ 0x02, 0x06, 0x11, 0x03, 0x02, 0x03 ]
        if isPauseCase {
            pk = [ 0x02, 0x06, 0x11, 0x03, 0x01, 0x03 ]
        }
        
        let uJob = UnitJob(name: "  키트 활성화  ")
        uJob.initJob = {
            print(" Pause ")
            self.packForAck = nil
            self.writeData(pk)
        }
        uJob.didFinished = { () -> Bool in
            if let received = self.packForAck {
                return received.command == 17 && received.msg == 3
            }
            return false
        }
        packMchn.addBaseJob(uJob)
    }

    func sendMonitorCommand(pMsg: UInt16, msg: String) {
        log.logUiAction("sendMonitorCommand ::  \(pMsg)  \(msg)", lnum: 10)
        //cellService?.protoN = Int32(cprProtoN.theVal) + 1
        //self.cellService?.monitorCommand(pMsg, name: msg)  // 트레이너 연결 됐을때만 .. 리트라이.
        let manikinK = UInt8(HsBleSingle.inst().manikinKind.theVal + 1) //  default : 1 / prestan : 2
        let blsLay: UInt8 = (isRescureMode ? 0x01 : 0x02)

        addPacketAndExe("Monitor \(pMsg)", bts: 0x02, 0x0B, 0x11, 0x02, UInt8(pMsg), 15, blsLay,
                        UInt8(HsBleSingle.inst().stageLimit),
                        UInt8(cprProtoN.theVal + 1),  // protoN : 1, 2, 3 ...
            manikinK, 0x03 )

//        writeData( [ 0x02, 0x0B, 0x11, 0x02, UInt8(pMsg), 15, blsLay,
//            UInt8(HsBleSingle.inst().stageLimit),
//            UInt8(cprProtoN.theVal + 1),  // protoN : 1, 2, 3 ...
//            manikinK, 0x03 ] )
        packForAck = ReceivedPack()
        packForAck!.ackCheck = packForAck!.monitorScreenAck
    }

    private func addPacketAndExe(ttl: String, bts: UInt8...) {
        log.printAlways("addPacketAndExe   \(ttl)", lnum: 1)
        let uJob = UnitJob(name: ttl)
        uJob.initJob = {
            self.writeData(bts)
        }
        uJob.didFinished = { () -> Bool in
            return true


//            if self.packForAck != nil && self.packForAck!.ackCheck != nil {
//                print("FinishJob;;  Ack Finished..")
//                return self.packForAck!.ackCheck!()
//            }
//            return false
        }
        packMchn.addBaseJob(uJob)
        packMchn.timerAction()
        log.printAlways("addPacketAndExe   \(ttl)", lnum: 1)
    }
}




