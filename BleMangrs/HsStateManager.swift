//
//  HsStateManager.swift
//  heartisense
//
//  Created by Jongwoo Moon on 2015. 11. 6..
//  Copyright © 2015년 gyuchan. All rights reserved.
//

import Foundation

class HsStateManager : NSObject { // 센서 커넥터 빠졌을 때만 쓰임..
    var log = HtLog(cName: "HsStateManager 상태 매니저")
    var isPiezoDisconnected = false, isAccelDisconnected = false

    var bypassStateInOutChanged = false, prevIsBypass = false
    
    func sensorConnectionsAreAllGood() {
        isPiezoDisconnected = false
        isAccelDisconnected = false
    }
    
    func sensorConnectionLost(isPiezo : Bool) {
        //log.printAlways(" 센서 유실 : 피에조 case ? \(isPiezo) ", lnum: 10)
        if isPiezo { isPiezoDisconnected = true }
        else { isAccelDisconnected = true }
    }

    var cnt = 0

    func sensorStateCheck() -> String? {  // 리턴 값을 옵셔널로 변경..  정상이면 nil 임..
        //print("check ::  \(cnt)")
        if isPiezoDisconnected || isAccelDisconnected {
            cnt += 1
        } else {
            cnt = 0
        }
        if 20 < cnt {
            return lostString()
        }
        return nil // "OK"
    }

    func lostString() -> String {
        log.printThisFunc(" 완전히 연결 유실..  ", lnum: 20)
        if isPiezoDisconnected {
            log.logThis("   >>  " + langStr.obj.breath_module)
            return langStr.obj.breath_module + " " + langStr.obj.sensor_not_connected
        }
        if isAccelDisconnected {
            log.logThis("   >>  " + langStr.obj.compression_pad)
            return langStr.obj.compression_pad + " " + langStr.obj.sensor_not_connected
        }
        return ""
    }

    func errorString() -> String {
        return langStr.obj.error
    }

    /// bleman 에서 패킷 받을 때마다 불려야 함.
    func bypassDoWatch() {
        if prevIsBypass != isBypassMode {
            bypassStateInOutChanged = true
            prevIsBypass = isBypassMode
            if isBypassMode { // 바이패스로 들어옴.
                isGlobalMuteOn = true
            }
        }
    }

    
}

/*
"compression_pad" = "압박 패드";
*/
