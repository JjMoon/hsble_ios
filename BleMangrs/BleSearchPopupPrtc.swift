//
//  BleSearchPopupPrtc.swift
//  Trainer
//
//  Created by Jongwoo Moon on 2016. 10. 25..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation

protocol BleSearchPopupPrtc : class {
    var searchView: BleSearchVw? { get set }
    var popUpVw: KLCPopup? { get set }

    var disMissed: ((Void) -> Void)? { get set }

    func showBleListPopup(bleCtrl: HsBleCtrl)
}

extension BleSearchPopupPrtc {
    func showBleListPopup(bleCtrl: HsBleCtrl) {
        if searchView == nil {
            let bundle = NSBundle(forClass: self.dynamicType)
            let nib = UINib(nibName: "BleSearchVw", bundle: bundle)
            //let cenX = screenSize.width * 0.5, cenY = screenSize.height * 0.5
            searchView = nib.instantiateWithOwner(self, options: nil).last as? BleSearchVw
            searchView!.frame = CGRect(x: 0, y: 0, width: 400, height: 200)

            searchView!.disMissed = self.disMissed!

            //searchView!.searchBleViewSet(trnBleCtrl)
            //view.addSubview(searchView!)
        } else {
            //searchView!.showMe()
        }
        let layout = KLCPopupLayout(horizontal: KLCPopupHorizontalLayout.Center, vertical: KLCPopupVerticalLayout.Center)
        popUpVw = KLCPopup(contentView: searchView, showType: KLCPopupShowType.FadeIn,
                           dismissType: KLCPopupDismissType.FadeOut,
                           maskType: KLCPopupMaskType.Dimmed,
                           dismissOnBackgroundTouch: true, dismissOnContentTouch: false)
        popUpVw!.showWithLayout(layout)
        searchView!.searchBleViewSet(bleCtrl, parent: popUpVw!)

        popUpVw!.didFinishDismissingCompletion = {
            bleCtrl.stopScan()
            if bleCtrl.conState == .Scanning {
                bleCtrl.conState = .None
            }
        }
    }
}

class BleConnectPreviousKit : NSObject {
    var connTimer: NSTimer?, ble: HsBleCtrl?, kit = "", retryNum = 0
    //var finishConnection = {}

    func connectWith(bleCtrl: HsBleCtrl, kitName: String) {
        print("\n\n\n  BleConnectPreviousKit :: connectWith     \(kitName)  \n\n\n")
        ble = bleCtrl
        kit = kitName
        connTimer = NSTimer.scheduledTimerWithTimeInterval(1,
           target: self, selector: #selector(self.refreshConn), userInfo: nil, repeats: true)
        ble!.startScan()
        retryNum = 0
    }

    func searchAndConnect() {
        print("BleConnectPreviousKit :: func searchAndConnect  ")
        let prevPort = ble!.allPorts.filter { (prt) -> Bool in
            prt.name == kit
        }
        print("BleConnectPreviousKit :: 포트 카운트 : \(prevPort.count)  ")

        if 0 < prevPort.count {
            print("\n\n  \t\t Found::  \(kit)  Open Port ....   ")
            ble!.openPort(prevPort[0])
        } else{
            print("\n\n  \t\t Not Found::  \(kit)  start scan ....   ")
            ble!.stopScan()
            ble!.startScan()
        }
    }

    func cancelAction() {
        print("\(#file) :: \(#function)  ")
        connTimer?.invalidate(); connTimer = nil
        retryNum = 10
    }

    func refreshConn()  {
        retryNum += 1

        if 6 < retryNum {
            ble!.conState = .ConnectFail
            cancelAction()
            return
        }

        print("\(#file) :: \(#function)   \(ble?.conState) ")
        switch ble!.conState {
        case .Scanning, .None:
            print("\(#file) :: \(#function)  case .Scanning, .None:  searchAndConnect ")
            searchAndConnect()
        case .Connected, .OpenStart:
            break
        default: // 연결이 되었으니 후속 작업..
            print("\(#file) :: \(#function)   연결 / 에러 >>>  타이머 초기화....... \n\n\n")
            cancelAction()
        }
    }

}
