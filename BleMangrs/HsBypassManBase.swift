//
//  HsBypassManBase.swift
//  heartisense
//
//  Created by imlab on 2015. 11. 3..
//  Copyright © 2015년 gyuchan. All rights reserved.
//

import Foundation

class HsBypassManager : NSObject {
    var log = HtLog(cName: "HsBypassManager")

    /// 0: not set, 1: Monitor, 2: Test 3: Calibration
    var isMonitor = HtNullableBool()
    var isPause = false

    //var ackToMonitor : (Void -> Void)?

    var stage:Int {
        get { return Int(trnBleCtrl.stage) }
        set { trnBleCtrl.stage = newValue }
    }

    //var curDatas:UnsafeMutablePointer<UInt8> = UnsafeMutablePointer<UInt8>()
    var curDatas:UnsafeMutablePointer<UInt8> = nil
    
    override init() {
        isMonitor.intVal = 0 // 0: not set, 1: Monitor, 2: Test 3: Calibration
        super.init()
        //log.bleObj = trnBleCtrl //HsBleManager.inst()
    }
    
    func enterBypassModeAtConnection() { // 접속 시 다른 앱에 연결 탐지..  [_cellService instructorAppInfoRequest];  이걸 접속 후에 날린다.
        log.printThisFunc("enterBypassModeAtConnection", lnum: 10)
        isBypassMode = true
        isMonitor.doSetValue(true)
    }

    /// isMonitor? 변수 사용..
    func quitApplication() -> Bool {
        if isMonitor.validAndTrue() && isMonitor.intVal > 1 {  return true }
        return false
    }
    
    func gotoOperationView() -> Bool {
        switch trnBleCtrl.stt {
        case .S_INIT0, .S_READY, .S_QUIT, .S_CALIBRATION, .S_COMPLETE, .CAL_NEW_RP, .CAL_NEW_CC, .CAL_LOAD:
            return false
        default:
            return true
        }
    }
    
    func bypassParseOfMonitor(pData:UnsafeMutablePointer<UInt8>, pMsg:Int8) {  //  바이패스는 여기로 ...   0 ..  1 : App Info.    2 : ack 보내기
        curDatas = pData
        // pMsg : 1 : App Info Request  ,  2 : Ack
        let length = parseOneByte(pData, at: 1)
        let charAt4 = parseOneByte(pData, at: 4) // 앱 Info.. 또는 스크린 종류.
        log.printThisFunc("bypassParseOfMonitor 냉무  len : \(length),   char 4 : \(charAt4),  char 5 : \(parseOneByte(pData, at: 5))")
    }

    func parsePacket(obj: ReceivedPack, state: CurStep) {

        isBypassMode = true;  // NSLog(@"  HsBleMan >>> L815  ");

        //_cellService.titleAtoD = nick;

        if obj.msg == 1 { // 모니터나 테스트에서 트레이너의 요청에 의해 현재 나의 상태를 보내는 부분.
            //NSLog(@"\n\n Instructor App Info 받음..  AppIdTrMoTe : %d 리턴..\n\n", AppIdTrMoTe);
            if state == .S_CALIBRATION {
                // if (calibrationMode) { [_cellService sendAppInfoCalibration]; 
                return  // 여기선 바로 리턴해야 함..
            }
            if (AppIdTrMoTe == 1) {
                //[_cellService sendAppInfoResponseMonitor]; 
                return;
            }
            if (AppIdTrMoTe == 2) {
                //[_cellService sendAppInfoResponseTest]; 
                return;
            }
        }

        if (AppIdTrMoTe == 2) { // 테스트
            // [_cellService sendAppInfoResponseTest];
            return;
        }

        if (AppIdTrMoTe == 1) { // Monitor
            //switch (obj.msg) {
                //case 1: // App Info 를 알려줌.  [_cellService sendAppInfoResponseMonitor];
            //break;
//            case 2: // 스크린 애크임..  이거 bleCtrl 로 이동..
//                [log printAlways:@"  모니터 액크 <스크린 애크> 받음..   02 06 11 02 12 03" lnum:3];
//                [bypass bypassParseOfMonitor:val pMsg:msg];
//                [_cellService ackReceivedInMonitor];
//                monitorAck = true;  break;
//            default:  break;
//            }
            return;
        }  //  */

        let rBypass = bypassParseOfTrainer(obj)
        // 0 ..  1 : App Info.    2 : ack 보내기
        switch (rBypass) {
        case 1: // 접속 초기에 App Info 가 왔다.
            print("  rBypass   \(rBypass)")
            break;
        case 2: // 스크린 정보..  애크를 보냄.
            //ackToMonitor?()  // [_cellService ackToMonitor];
            return;
        default:  break;
        }
        //if (rBypass > 0) isDeviceCheckOK = true;
    }

    func bypassParseOfTrainer(obj: ReceivedPack) -> UInt8 {  //  바이패스는 여기로 ...   0 ..  1 : App Info.    2 : ack 보내기
        let length = obj.len // parseOneByte(pData, at: 1)
        let charAt4 = obj.val4 // parseOneByte(pData, at: 4) // 앱 Info.. 또는 스크린 종류.
        
        if obj.msg == 3 && charAt4 == 1 {
        
            print("  모니터 포즈")
            isPause = true
            trnBleCtrl.writeData([ 0x02, 0x06, 0x11, 0x03, 0x01, 0x03 ])
            return 0
        }
        if obj.msg == 3 && charAt4 == 2 {
            print("  모니터 포즈 end.. ")
            isPause = false
            trnBleCtrl.writeData([ 0x02, 0x06, 0x11, 0x03, 0x02, 0x03 ])
            return 0
        }

        log.printThisFNC("bypassParseOfTrainer", comment: " len : \(length), char4: \(charAt4), 실제길이 : \(obj.bytes.count)")
        isMonitor.doSetValue(true)
        
        if 10 < obj.bytes.count { //  실제 길이로 해야.. 크래쉬 방지...     length > 7 {
            if obj.bytes[6] == 1 { isRescureMode = true } // isDoctor = false  }
            else { isRescureMode = false }
            HsBleSingle.inst().stageLimit = Int32(obj.bytes[7]) // 여기서 전과정 스테이지 넘버 세팅..  BypassManager 에서는 이를 복원..

            let cprProtocol = Int(obj.bytes[8]) // AHA : 0x01 ERC : 0x02 ANZ : 0x03
            cprProtoN.parseValueInTrainerBypassMode(cprProtocol - 1)
            
            let manikinInfo = Int(obj.bytes[9])
            manikinKindN.parseValueInTrainerBypassMode(manikinInfo - 1) // default : 1 / prestan : 2
            //print("바이패스 공통 정보 ")
            //log.printAlways("  isRescureMode ? \(isRescureMode)   stageLimit : \(HsBleSingle.inst().stageLimit) cprProtocol [0, 1, 2] : \(cprProtoN.theVal) ", lnum: 5)
        }

        switch obj.msg { // [3] .. command or msg..
        case 1: // App Info
            log.logThis(" App Info   return 1", lnum: 10)
            isMonitor.intVal = Int(charAt4) // 1 : Monitor, 2 : Test, 3 : Calibration
            trnBleCtrl.stt = .S_READY
            log.printAlways(" App Info  isMonitor.intVal :: \(isMonitor.intVal)   .S_READY ", lnum: 10)
            return 1
        case 2: // Screen
            HsBleSingle.inst().stageLimit = Int32(obj.bytes[7])
            //let cprProtocol = parseOneByte(pData, at: 8)
            
            log.logThis(" Screen Info return 2 . StageLimit : \(HsBleSingle.inst().stageLimit)  ..  ", lnum: 10)
            trainerParseScreenInfo(UInt16(obj.val4))
            return 2  // ack 를 보내라는 신호.
        default:
            print("\n\n\n\n\n bypassParseOfTrainer    Error Case !!! \n\n\n\n\n")
        }
        return 0
    }

    // CPR protocol 을 어떻게 처리할 까.. 바이패스 변수를 더 두자.
    // 바이패스 클래스를 만들까..
    func trainerParseScreenInfo(screenID:UInt16) {
        log.bleObj = trnBleCtrl
        log.printThisFNC(" 바이패스 스크린 정보 받음..", comment: "  screenID : \(screenID)", lnum: 10)
        isBypassMode = true
        let prevMode = trnBleCtrl.instrctStt // HsBleManager.inst().parsingState
        if screenID > 8 && screenID != 19 && screenID != 20 && screenID != 21 {
            trnBleCtrl.instrctStt = .Entire
            //HsBleManager.inst().parsingState = AllInOne10;
        } // 전과정으로 세팅.
        else {
            trnBleCtrl.instrctStt = .Step // HsBleManager.inst().parsingState = StepByStep20;
        }
//        if prevMode != trnBleCtrl.instrctStt {  // HsBleManager.inst().parsingState {
//            log.printAlways("  전과정 <<< ==== >>>  단계별  전환  ==> ", lnum: 10)
//            trnBleCtrl.bypassModeChanged()
//        }

        switch screenID { // 같은 상태에서 신호를 다시 받을 때.. BypassStepComp, WholeStepRestart ..  처리
        case 0:
            trnBleCtrl.stt = .S_INIT0
            print("\t\t HsCalcBypass ::  trainerParseScreenInfo    ID = 0   S_INIT0     \n\n\n")
        case 21:
            trnBleCtrl.stt = .S_STEPBYSTEP_SAFETY
            print("\t\t HsCalcBypass ::  trainerParseScreenInfo    ID = 19 0x15   S_STEPBYSTEP_SAFETY     \n\n\n")
        case 19:
            trnBleCtrl.stt = .S_STEPBYSTEP_AIRWAY
            print("\t\t HsCalcBypass ::  trainerParseScreenInfo    ID = 19 0x13   S_STEPBYSTEP_AIRWAY     \n\n\n")
        case 20:
            trnBleCtrl.stt = .S_STEPBYSTEP_CHECK_BRTH
            print("\t\t HsCalcBypass ::  trainerParseScreenInfo    ID = 20 0x14   S_STEPBYSTEP_CHECK_BRTH     \n\n\n")
        case 3:
            trnBleCtrl.stt = .S_STEPBYSTEP_RESPONSE
            print("\t\t HsCalcBypass ::  trainerParseScreenInfo    ID = 3   S_STEPBYSTEP_RESPONSE     \n\n\n")
        case 4:
            trnBleCtrl.stt = .S_STEPBYSTEP_EMERGENCY
            print("\t\t HsCalcBypass ::  trainerParseScreenInfo    ID = 4   S_STEPBYSTEP_EMERGENCY     \n\n\n")
        case 5:
            trnBleCtrl.stt = .S_STEPBYSTEP_CHECKPULSE
            print("\t\t HsCalcBypass ::  trainerParseScreenInfo    ID = 5   S_STEPBYSTEP_CHECKPULSE     \n\n\n")
        case 6:
            trnBleCtrl.stt = .BypassStepComp // .S_STEPBYSTEP_COMPRESSION  //
            print("\t\t HsCalcBypass ::  trainerParseScreenInfo    ID = 6   S_STEPBYSTEP_COMPRESSION     \n\n\n")
        case 7:
            trnBleCtrl.stt = .BypassStepBrth //.S_STEPBYSTEP_BREATH
            print("\t\t HsCalcBypass ::  trainerParseScreenInfo    ID = 7   S_STEPBYSTEP_BREATH     \n\n\n")
        case 8:
            trnBleCtrl.stt = .S_STEPBYSTEP_AED
            print("\t\t HsCalcBypass ::  trainerParseScreenInfo    ID = 8   S_STEPBYSTEP_AED     \n\n\n")
        case 9:
            trnBleCtrl.stt = .WholeStepStart  // .S_WHOLESTEP_DESCRIPTION
            print("\t\t HsCalcBypass ::  trainerParseScreenInfo    ID = 19  Whole Step Description >>   S_WHOLESTEP_DESCRIPTION     \n\n\n")
        case 10:
            trnBleCtrl.stt = .S_WHOLESTEP_PRECPR // 이때 압박으로 건너 뜀..
            print("\t\t HsCalcBypass ::  trainerParseScreenInfo    ID = 10  S_WHOLESTEP_PRECPR >>        S_WHOLESTEP_PRECPR     \n\n\n")
        case 14: // E
            trnBleCtrl.stt = .S_WHOLESTEP_RESULT
            print("\t\t HsCalcBypass ::  trainerParseScreenInfo    ID = 14  S_WHOLESTEP_RESULT >>        S_WHOLESTEP_RESULT     \n\n\n")
        case 15: // F
            trnBleCtrl.stt = .S_READY
            print("\t\t HsCalcBypass ::  trainerParseScreenInfo    ID = 15   S_READY     \n\n\n")
        case 16: // 10
            trnBleCtrl.stt = .S_QUIT //  모니터가 끊을 때..
            //isBypassMode = false  이렇게 하지 말고, quit 일 때 Operation 화면에서 나가고 선택 화면에서 false로 하고 스텝을 초기화 하는 것이 좋겠다.
            HsBleSingle.inst().stageLimit =  Int32(NSUserDefaults.standardUserDefaults().integerForKey("StageLimit"))
            print("\t\t HsCalcBypass ::  trainerParseScreenInfo    ID = 16   S_QUIT     \n\n\n")
        case 18: // 0x12
            trnBleCtrl.stt = .S_COMPLETE
            print("\t\t HsCalcBypass ::  trainerParseScreenInfo    ID = 18   S_COMPLETE     \n\n\n")
        default:

            print("\n\n\n\n\n HsCalcBypass :: trainerParseScreenInfo    Error Case     S_INIT0   !!! \n\n\n\n\n")
        }

        if prevMode != trnBleCtrl.instrctStt {  // HsBleManager.inst().parsingState {
            log.printAlways("  전과정 <<< ==== >>>  단계별  전환  ==> ", lnum: 10)
            trnBleCtrl.bypassModeChanged()
        }
    }


}
