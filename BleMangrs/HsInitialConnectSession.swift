//
//  HsInitialConnectSession.swift
//  Trainer
//
//  Created by Jongwoo Moon on 2016. 10. 25..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation

class HsInitialConnectSession: HtPacketMachine {

    var ble: HsBleCtrl?, curReceivedPack: ReceivedPack?
    var productTypeErrorEvent = {}

    var dataCalc = HSDataCalculator()

    var Ble1Conn = false, Ble2Conn = false, InstructorAppInfo = 0 // 1, 2, 3: Cali
    var errorMsg: String?
    var time = CFAbsoluteTimeGetCurrent()

    override init() {
        super.init()
    }

    func baseSetting(bleCtrl: HsBleCtrl) {
        print("HsInitialConnectSession :: baseSetting ")
        log.clsName = "초기 작업 Session"
        ble = bleCtrl

        ble!.conState = .None
        dataCalc.dataObj = ble!.dataObj
    }

    func dataReceived(obj: ReceivedPack) { // call back..
        log.logThis("dataReceived", lnum: 2)
        curReceivedPack = obj

        if ble!.conState == .Finished && obj.len == 0x88 && obj.command == 9 && obj.msg == 1 {
            log.printAlways("Operation Stop :: ACK 받음..", lnum: 5)
            ble!.conState = .Rest
            //ble!.dataReceived = nil
        }

        if obj.command == 5 { // BLE 1 센서 정보.
            switch obj.msg {
            case 0: Ble1Conn = true
            case 1: Ble1Conn = false
            default: print(" Sensor Error BLE 1")
            }
        }
        if obj.command == 6 { // BLE 1 센서 정보.
            switch obj.msg {
            case 0: Ble2Conn = true
            case 1: Ble2Conn = false
            default: print(" Sensor Error BLE 2")
            }
        }
        if obj.command == 17 {
            switch obj.msg {
            case 1: // Instructor App Info Response
                InstructorAppInfo = obj.val4
                ble?.calcManager.bypass?.isMonitor.doSetValue(true)
                print("InstructorAppInfo  ::  \(InstructorAppInfo)")
            default:
                print("  obj.command == 17  ")
            }
        }

        if obj.command == 16 && ble!.conState == ConnState.Cali {
            print("obj.command == 16 && ble!.conState == ConnState.Cali  ")
            let data = NSData(bytes: obj.bytes , length: obj.len)
            dataCalc.sensorDataParsing(data, successBlock: {
                self.ble!.conState = .CaliFinished  // .Finished
                //self.ble!.operationStop()
            })
        }

        if ble!.conState == ConnState.CaliFinished {
            //timerAction()
        }

        checkFinishAndStartNext()        // checkFinish()
    }

    func cancelAction() {
        log.printAlways("\(#function)")
        timer.invalidate()
        arrPckt = [HtBaseJob]()
        ble!.dataReceived = nil
        ble!.closePort()
    }

    //////////////////////////////////////////////////////////////////////     [ << start Process >> ]

    func startProcess() {
        log.printAlways("startProcess >> START   \(ble!.conState)", lnum: 10)

        errorMsg = nil
        ble!.dataReceived = dataReceived
        Ble1Conn = false; Ble2Conn = false

        time = CFAbsoluteTimeGetCurrent() // 시작 시점.

        timer.invalidate()
        timer = NSTimer.scheduledTimerWithTimeInterval(0.5, target: self,
                                                       selector: #selector(self.timerAction),
                                                       userInfo: nil, repeats: true)
        arrPckt = [HtBaseJob]()
        dataCalc.resetCounter()
        //dataCalc = HSDataCalculator()

        addPacket([0x02, 0x05, 0x0F, 0x03, 0x03], title: "  라벨 체크  ") { () -> Bool in
            if self.curReceivedPack!.command == 15 {
                productType = Int32(self.curReceivedPack!.msg)
                print("\n\n  라벨 체크  prodType received   :  \(productType)    \(self.ble!.conState)  \n\n")
                if self.ble!.checkProductType() {
                    print("\n\n  라벨 체크  확인...  다음으로")
                    return true
                }
                else {
                    self.ble!.conState = .Error
                    self.productTypeErrorEvent()
                    return false
                }
            }
            return false
        }

        var uJob = UnitJob(name: "  키트 활성화  ")
        uJob.initJob = {
            print("키트 활성화  2 5 8 ? 3 ")
            self.curReceivedPack = nil
            self.ble!.writeData([ 0x02, 0x05, 0x08, UInt8(productType), 0x03 ])
        }
        uJob.didFinished = { () -> Bool in
            if let received = self.curReceivedPack {
                return received.command == 8 && received.msg == 0
            }
            return false
        }
        arrPckt.append(uJob)

        addPacket([ 0x02, 0x05,    0x01, 0x03, 0x03 ], title: "  배터리 체크  ") { () -> Bool in
            if self.curReceivedPack!.ackCheck(1, mesg: nil, v4: nil) {
                self.ble!.batery = self.curReceivedPack!.msg
                print("  배터리 잔량 : \(self.ble!.batery)")
                if self.ble!.batery < 11 {
                    self.errorMsg = langStr.obj.low_batery_kit
                    self.ble?.conState = .Error
                    return false
                } else { return true }
            }
//
//            if self.curReceivedPack!.command == 1 {
//                self.ble!.batery = self.curReceivedPack!.msg
//
//                print("배터리 체크 : \(self.ble!.batery)")
//
//                if received.ackCheck(3, mesg: 1, v4: nil) {
//                    self.errorMsg = langStr.obj.compression_pad + " " + langStr.obj.sensor_not_connected
//                    self.ble?.conState = .Error
//                    return false
//                }
//
//                return true
//            }
            return false
        }

        /////  예외 처리 및 메시지 보여주고  끝내기...

        addPacket([ 0x02, 0x05,    0x03, 0x03, 0x03 ], title: "Accel Info Check : 0x03") { () -> Bool in
            if let received = self.curReceivedPack {
                if received.command != 3 { return false }
                if received.ackCheck(3, mesg: 0, v4: nil) { return true } // 정상..
                if received.ackCheck(3, mesg: 1, v4: nil) {
                    self.errorMsg = langStr.obj.compression_pad + " " + langStr.obj.sensor_not_connected
                    self.ble?.conState = .Error
                    return false
                } // Not Connected

                if received.ackCheck(3, mesg: 2, v4: nil) {  // Sensor Error
                    self.errorMsg = langStr.obj.compression_pad + " " + langStr.obj.sensor_error_detected
                    self.ble?.conState = .Error
                    return false
                }
            }
            return false
        }
        addPacket([ 0x02, 0x05,    0x04, 0x03, 0x03 ], title: "ATM Info : 0x04") { () -> Bool in
            if let received = self.curReceivedPack {
                if received.command != 4 { return false }
                if received.ackCheck(4, mesg: 0, v4: nil) { return true } // 정상..
                if received.msg == 1 { // not connect
                    self.errorMsg = langStr.obj.breath_module + " " + langStr.obj.sensor_not_connected
                }
                if received.msg == 2 { // error
                    self.errorMsg = langStr.obj.breath_module + " " + langStr.obj.sensor_error_detected
                }
                self.ble?.conState = .Error
                return false
            }
            return false
        }

        // D Value 가져오고 무조건 쓰기..  980 ..
        addPacket([ 0x02, 0x05, 0x0a, 0x00, 0x03 ], title: "D Value Req " ) { () -> Bool in
            let rslt = self.curReceivedPack!.ackCheck(0x0a, mesg: 2, v4: nil)
            if rslt {
                let bt = self.curReceivedPack!.bytes
                self.log.printAlways("  D Value from Kit ::  \(self.parseTwoBytes(bt[4], and: bt[5]))")
            }
            return rslt
        }
        addPacket([ 0x02, 0x07, 0x0a, 0x01, 0x03, 0xd4, 0x03 ], title: "D Value  Update" ) { () -> Bool in
            return self.curReceivedPack!.ackCheck(0x0a, mesg: 3, v4: nil)
        }

        addPacket([ 0x02, 0x06, 0x07, 0x00, 0x01, 0x03 ], title: "Cali Value Req : Pressure") { () -> Bool in
            let pk = self.curReceivedPack!, rV = (pk.command == 7 && pk.msg == 2 && pk.val4 == 1)
            if rV {
                self.log.printAlways("Cali Value Req : Pressure ::  \(UInt16(pk.bytes[5])) ")
                self.dataCalc.cc_cali_value = UInt16(pk.bytes[5])
            }
            return rV
        }

        addPacket([ 0x02, 0x06, 0x07, 0x00, 0x02, 0x03 ], title: "Cali Value Req : ATM") { () -> Bool in
            let pk = self.curReceivedPack!, rV = (pk.command == 7 && pk.msg == 2 && pk.val4 == 2)
            if rV {
                self.log.printAlways("Cali Value Req : ATM  ::  \(UInt16(pk.bytes[5])) ")
                self.dataCalc.rp_cali_value = UInt16(pk.bytes[5])
                self.dataCalc.setCaliValue()
            }
            return rV
        }

        // BLE 모니터, 테스트 등 체크....  후속 처리..
        addPacket([ 0x02, 0x05,    0x05, 0x03, 0x03 ], title: "BLE 1 : 0x05") { () -> Bool in
            return self.curReceivedPack!.command == 5
        }
        uJob = UnitJob(name: "  앱 정보 요청..  ") // 0x02, 0x06, 0x11, 0x01, 0x03, 0x03
        uJob.initJob = {
            self.curReceivedPack = nil
            if AppIdTrMoTe == 0 && self.Ble1Conn { // 트레이너에서 A 에 연결되어 있을 때.. 확인..
                self.log.printAlways("  AppIdTrMoTe == 0 && self.Ble1Conn == true  ", lnum: 5)
                self.ble!.writeData([ 0x02, 0x06, 0x11,  0x01, 0x03, 0x03 ])
            } else {  // skip
                HsGlobal.delay(0.05, closure: {
                    self.timerAction()
                    self.timerAction()
                })
            }
        }
        uJob.didFinished = { () -> Bool in
            if AppIdTrMoTe == 0 && !self.Ble1Conn { // 트레이너에서 A 에    Not   연결되어 skip ..
                print("\n\n 트레이너에서 A 에    Not   연결되어 skip .. \n\n")
                return true
            }
            if let pck = self.curReceivedPack {
                if pck.command == 17 && pck.msg == 1 {
                    self.log.printAlways("앱 정보 요청. :  ", lnum: 5)
                    return true
                }
            }
            return false
        }
        arrPckt.append(uJob)

        addPacket([ 0x02, 0x05,    0x06, 0x03, 0x03 ], title: "BLE 2 : 0x06") { () -> Bool in
            return self.curReceivedPack!.command == 6
        }

        addUnitJobWith({ self.ble!.operationStart(true)
            }, finish: {
                return self.ble!.ackCheckAndReset()
            }, title: " Operation Start")


        addUnitJobWith({
            self.ble!.conState = .Cali
            }, finish: {
                return self.ble!.conState != .Cali
            }, title: "  Calibration  ")

        addUnitJobWith({ self.ble!.operationStop(true)
            }, finish: {
                return self.ble!.ackCheckAndReset()
            }, title: " Operation Stop")

        addUnitJobWith({
            print(">>> LastJob")
            self.ble!.savePrevKit()
            self.ble!.conState = .Rest
            }, finish: {
                if AppIdTrMoTe == 0 && self.Ble1Conn {
                    print(">>> LastJob  isBypassMode : \(isBypassMode)")
                    isBypassMode = true
                }
                return false
            }, title: " 접속 성공 .. ")

        timerAction()
    }


    private func addPacket(byts: [UInt8], title: String, finish: Void -> Bool) {
        let uJob = UnitJob(name: title)
        uJob.initJob = {
            self.log.printAlways("  Init Job ::   \(title)  ")
            self.curReceivedPack = nil
            self.ble!.writeData(byts)
        }
        uJob.didFinished = { () -> Bool in
            if self.curReceivedPack != nil && finish() {
                self.log.printAlways(title, lnum: 5)
                return true
            }
            return false
        }
        arrPckt.append(uJob)
    }

    override func timerAction() {
        if ble!.conState == .Error || ble!.conState == .None { return }
        super.timerAction()

        let diff = CFAbsoluteTimeGetCurrent() - time // 8초 타이머.

        log.printAlways("  지연 시간 :  \(CFAbsoluteTimeGetCurrent())  - \(time)  =   \(diff)")

        /// 전과정 초 이내 실행해야..
        if 7 < diff {
            log.printAlways("  지연 시간 :  7 < \(diff)  ble!.conState = .Error  ")
            ble!.conState = .Error
        }
        checkFinish()
    }

    private func checkFinish() {
        if ble!.conState == .Rest {
            log.printAlways(" ble!.conState == .Rest  타이머 종료 ....   >>>>>   Finish   \(timer.valid)", lnum: 3)
            timer.invalidate()
            log.printAlways(" ble!.conState == .Rest  타이머 종료 ....   >>>>>   Finish   \(timer.valid)  ?? ", lnum: 3)
        }
    }

}
