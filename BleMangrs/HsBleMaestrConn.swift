//
//  HsBleMaestrConn.swift
//  HS Test
//
//  Created by Jongwoo Moon on 2016. 8. 10..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation

extension HsBleMaestr {

    //////////////////////////////////////////////////////////////////////       [ UI Sub  ]


    /// A B C D  가 연결되었는지
    func getConnectedBLEwithhNick(nck: String) -> HsBleSuMan? {
        let rObj = getBleWithNick(nck)
        if rObj.isConnectionTotallyFinished {
            tempBle = rObj
            return rObj
        }
        //        for ble in arrBleSuMan {
        //            if ble.nick == nck {
        //                if ble.isConnectionTotallyFinished { return ble }
        //                else { return nil }
        //            }
        //        }
        return nil
    }

    func connectionNumber() -> Int {
        return connectedBleObjs().count
    }

    func getBleWithNick(nck: String) -> HsBleSuMan {
        for ble in arrBleSuMan {
            if ble.nick == nck { return ble }
        }
        return arrBleSuMan[0]
    }

    func setConnectionMode(isCalibration: Bool) {
        for ble in arrBleSuMan {
            ble.calibrationMode = isCalibration
        }
    }

    var numOfTestInfantActiveView : Int { // 테스트 영아 화면 갯수 관련..
        get { return arrBleSuMan.filter({ (bObj) -> Bool in
            return bObj.curStudent != nil || bObj.curStdntSub != nil }).count
        }}

    func getInfantViewArray() -> [HsBleSuMan?] {
        var rArr = [HsBleSuMan?]()
        for ble in arrBleSuMan {
            if ble.curStudent != nil || ble.curStdntSub != nil {
                rArr.append(ble)
            } else {
                rArr.append(nil)
            }
        }
        return rArr
    }

    func disconnectAllConnections() {
        log.printThisFNC("disconnectAllConnections", comment: "")
        let bles = connectedBleObjs()
        for bObj in bles {
            bObj.reset()
        }
    }


    func connectedBleObjs() -> [HsBleSuMan] {
        return arrBleSuMan.filter { (bleObj) -> Bool in  return bleObj.isConnectionTotallyFinished }
    }


}