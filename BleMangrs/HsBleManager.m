//
//  HsBleManager.m
//  heartisense
//
//  Created by Jongwoo Moon on 2015. 9. 1..
//  Copyright (c) 2015년 gyuchan. All rights reserved.
//

#import "HsBleManager.h"
#import "NSObject+Util.h"
#import "Common-Swift.h"

#import "HSPeripheral.h"
#import "HSBaseService.h"

#import "HSDataCalculator.h"
#import "HSStepDataCalculator.h"
#import "HSDataStaticValues.h"

#import "HSEnumSet.h"
#import "HSCentralManager.h"

#import "SearchDeviceListView.h"
#import "DPLocalizationManager.h"

#import "BLESerialComDefinition.h"
#import "BLEPort.h"



//////////////////////////////////////////////////////////////////////     [ HsBleManager ]
@interface HsBleManager()
//////////////////////////////////////////////////////////////////////////////////////////
{
    HtLog* log;
    BOOL checkCompressionCaliSetData, checkRespirationCaliSetData;
    bool isOperating, isPrevDeviceReconnect, bleAconnectedInMonitor;
    int voidBlockRepeat;
    double uiMessageTimestamp;
    void (^blockUpdateDevListView) (HSCentralManager*);
    void (^blockVoid)(void);
    int connectRetryNum, packCnt;

    //void(^PrevKitConnCheck)(void);
    NSTimer *PrevKitConnCheck, *FinalConnCheck;
    void(^prevKitConnfailureBlock)(void);

    HSCentralManager* hsCentralManager;
    HtOperationWatchDog* operationWatchDog;
    //161027 HSDataCalculator *dataCalcObj;
    HSStepDataCalculator *stepCal;
    
    // unit job related
    bool didCaliPacketReceived, isDeviceCheckOK;
    UnitJob *calibUpdateJob, *prevDevConnJob;
    UnitJob *initDeviceCheckJob;
    
    //HsExceptHand *exceptHand;
    StrLocBase *lObj;
}

@end

//////////////////////////////////////////////////////////////////////     _//////////_     [ HsBleManager ]    _//////////_
@implementation HsBleManager
//////////////////////////////////////////////////////////////////////////////////////////

@synthesize mtmTimer, bypass, stateMan, isPausing, nick, connectedName;
@synthesize arrUnitJob, arrBle, dataObj, monitorAck, bleState, calibrationMode;

//static HsBleManager *sharedMyManager;

+(id)inst {
    //NSLog(@"BleManager : Just Call ... \n\n\n");
    static HsBleManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
    
        NSLog(@"\n\n\n  BleManager : Singleton Creation ... \n\n\n");
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (void)reset {
    _operationOn = isBypassMode = false;
    [self logCallerMethodwith:@"  Resetting   ! ! ! " newLine:5];
    
    if (dataObj == nil)  dataObj = [[HsData alloc] init]; // 2번 생성되면 안됨..  ccStartPoint 등등 때문에..

    if([hsCentralManager.ymsPeripherals count] > 0) {
        for(HSPeripheral *temp in hsCentralManager.ymsPeripherals){
            if(temp.isConnected) { [temp disconnect]; }
        }
    }

    stepCal = [[HSStepDataCalculator alloc]init];
    //161027 dataCalcObj = [[HSDataCalculator alloc]init];
    arrUnitJob = [[NSMutableArray alloc] init];
    arrBle = [[NSMutableArray alloc] init];
    //exceptHand = [[HsExceptHand alloc] init];
    stateMan = [[HsStateManager alloc] init];
    //_calcManager = [[HsCalcManager alloc] initWithBleObj:self bypassObj:bypass];
    stepCal.dataObj = dataObj;
    //161027 dataCalcObj.dataObj = dataObj;
    NSLog(@"  HsBleManager :: reset  >>>>>   Object Initialize  !!! ");

    [FinalConnCheck invalidate];  FinalConnCheck = nil;
    [PrevKitConnCheck invalidate];  PrevKitConnCheck = nil;

    if (operationWatchDog == nil)
        operationWatchDog = [[HtOperationWatchDog alloc] initWithBleMan:self];
    //NSLog(@"  HsBleManager :: reset  >>>>>   Watch dog  !!! ");

    _exceptionProc =  ^(int command, int value) {
        NSLog(@"   exception Proc >>>   command : %d,   value : %d  ", command, value);
    };
    
    _connState = Initial;
    [self logMarkWith:@" BleManager :: reset >>>   _connState = Initial 로 세팅"];
    
    connectRetryNum = -10; // 끊어졌다가 재 접속 시  centralManager:didConnectPeripheral  오는 경우..

    [HsGlobal delay:0.2 closure:^ {
        [hsCentralManager removeAllPeripherals]; // hsCentralManager 상위 ymsCenter... 에서
        log = [[HtLog alloc] initWithCName:@"HsBle 매니저" active: true];
        NSLog(@"\n\n\n\n    HsBle 매니저  hsCentralManager removeAllPeripherals    \n\n\n\n");

        lObj = HsBleSingle.inst.langObj;
    }];
    //switch (langIdx) {
//        case 0:            dp_set_current_language(@"en");            break;
//        case 1:        default:            dp_set_current_language(@"ko");            break;    }
    _cellService = nil;
}

- (BOOL)isConnected {
    if (_cellPeripheral) {
        return _cellPeripheral.isConnected;
    }
    return false;
}

- (BOOL)isBusy {
    return !(_connState == Initial || _connState == Connected);
}

- (BOOL)isConnectionTotallyFinished {
    if (![self isConnected]) {
        _connState = Initial; //[self logMarkWith:@"  _connState = Initial;  131 "];
    }
    return _connState == Connected;
}


- (int)wrongPositionNum {  return (int)_calcManager.wrongPositionNum; }
- (void)setWrongPositionNum:(int)wrongPositionNum {  _calcManager.wrongPositionNum = wrongPositionNum; }

-(instancetype)init
{
    self = [super init];
    nick = @"Nick";
    connectedName = @"";
    _bypassModeChanged = ^{ NSLog(@"bleManager. bypassModeChanged 세팅 안됨.. "); };
    isBypassMode = false;
    _calcManager.weakUIcljr = _weakValueUI;
    uiMessageTimestamp = [NSDate timeIntervalSinceReferenceDate];
    _connState = Initial; // [self logMarkWith:@"  _connState = Initial; 154 "];
    bleState = S_INIT0;
    NSLog(@"  HsBleManager :: init  >>>>>   bleState = S_INIT0;  !!! ");
    hsCentralManager = [[HSCentralManager alloc] initWithDelegate:self]; // [HSCentralManager sharedService];
    NSLog(@"  HsBleManager :: init  >>>>>   HSCentralManager alloc  !!! ");
    // 타이머 개시.
    mtmTimer = [NSTimer scheduledTimerWithTimeInterval:5 target:self
                                              selector:@selector(mainTimerProcess:)
                                              userInfo:nil repeats:YES];

    [self reset];
    return self;
}

- (void)removePeripherals {
    [self logCallerMethodwith:@"  hsCentralManager removeAllPeripherals  " newLine:10];
    [hsCentralManager removeAllPeripherals];
}

-(void)mainTimerProcess:(NSTimer*)pTimer
{
    //[self logCallerMethodwith:@"Timer   ....  " newLine:3];    NSLog(@"\n\n\n");
    if (blockVoid) {
        if (voidBlockRepeat > 0) {
            blockVoid();
            voidBlockRepeat--;
        } else
            blockVoid = nil;
    }
}

-(void) operationStart {
    [_cellService requestOperationStart];
    [_cellPeripheral setDelegate:self];
    isOperating = true;
    packCnt = 0;
}

-(void) operationStop {
    [_cellService requestOperationStop];
    isOperating = false;
}

//////////////////////////////////////////////////////////////////////       [ Block || Delegates ]
-(void)setVoidVoidBlockRepeat:(int)num withBlock:(void (^)(void))voidBlock {
    voidBlockRepeat = num;
    blockVoid = voidBlock;
}

-(void)setUpdateDevListViewBlock:(void (^)(HSCentralManager*))updateBlock {
    blockUpdateDevListView  = updateBlock;
}

//////////////////////////////////////////////////////////////////////       [ UI support methods ]
#pragma mark - UI support methods

- (void)setDeviceListView:(SearchDeviceListView*)listView {
    listView.centManager = hsCentralManager;
}

- (BOOL)isEntireProcess {
    switch (bleState) {
        case S_WHOLESTEP_DESCRIPTION:   case S_WHOLESTEP_COMPRESSION:
        case S_WHOLESTEP_BREATH:        case S_WHOLESTEP_AED:
        case S_WHOLESTEP_PRECPR:        case S_WHOLESTEP_RESULT:
            return true;
        default:   return false;
    }
}

- (BOOL)isOperatingState {
    switch (bleState) {
        case S_WHOLESTEP_COMPRESSION:  case S_WHOLESTEP_BREATH:
        case S_STEPBYSTEP_COMPRESSION: case S_STEPBYSTEP_BREATH:
            return true;
        default:
            return false;
    }
}

- (BOOL)isCalibating {
    switch (bleState) {
        case CAL_NEW_CC:  case CAL_NEW_RP: case CAL_LOAD: case S_CALIBRATION:
            return true;
        default: return false;
    }
}

- (void)startCalibration {
    [self logCallerMethodwith:@"  set initial values ...  call activateNewCC  " newLine:10];
    [self operationStart];
    
    dataObj.ccLimitPointTemp = dataObj.ccLimitPoint; //    CC_LIMIT_POINT_TEMP = CC_LIMIT_POINT;
    dataObj.rpLimitPointTemp = dataObj.rpLimitPoint; //    RP_LIMIT_POINT_TEMP = RP_LIMIT_POINT;
    
    [_calcManager activateNewCC];
}

- (void)backToReady {
    [self logCallerMethodwith:@" Set Ready" newLine:5];
    
    bleState = S_READY;
}

- (double)progressValue4Display {
    double dispValue;
    switch (bleState) {
        case S_STEPBYSTEP_COMPRESSION:  case S_WHOLESTEP_COMPRESSION:
            //NSLog(@"   depth : %d", dataObj.depth );
            dispValue = dataObj.depth; break;
        case S_STEPBYSTEP_BREATH:       case S_WHOLESTEP_BREATH: default:
            dispValue = dataObj.amount; break;
    }
    return dispValue * 0.01;
}
/// Depth : cm, Amount : ml
- (NSString*)progressValueWithUnit {
    double dispValue;  NSString* rstr;
    switch (bleState) {
        case S_STEPBYSTEP_COMPRESSION:  case S_WHOLESTEP_COMPRESSION:
            dispValue = [_calcManager parseDepthForObjC] / 10;
            rstr = [NSString stringWithFormat:@"%.1f cm", dispValue];
            break;
        case S_STEPBYSTEP_BREATH:       case S_WHOLESTEP_BREATH: default:
            dispValue = [_calcManager parseAmountForObjC];
            rstr = [NSString stringWithFormat:@"%.0f ml", dispValue];
            break;
    }
    return rstr;
}

- (NSString*)uiCountInfo {
    NSString *rStr;
    switch (bleState) {
        case S_STEPBYSTEP_COMPRESSION:  case S_WHOLESTEP_COMPRESSION:
            rStr = [NSString stringWithFormat:@"%ld / %ld", (long)dataObj.ccCorCount, (long)dataObj.count]; break;
        case S_STEPBYSTEP_BREATH:       case S_WHOLESTEP_BREATH: default:
            rStr = [NSString stringWithFormat:@"%ld / %ld", (long)dataObj.rpCorCount, (long)dataObj.count]; break;
    }
    return rStr;
}

- (double)progressValue4Counter {
    double dispValue;
    switch (bleState) {
        case S_STEPBYSTEP_COMPRESSION:  case S_WHOLESTEP_COMPRESSION:
            dispValue = 30; break;
        case S_STEPBYSTEP_BREATH:       case S_WHOLESTEP_BREATH: default:
            dispValue = 2; break;
    }
    return dataObj.count / dispValue;
}

- (BOOL)uiMessageOn {
    double curTime = [NSDate timeIntervalSinceReferenceDate];
    //NSLog(@" 호흡 사인 관련 >>   on ?  isMessage ? %d   time : %f", dataObj.isMessageDone, (curTime - uiMessageTimestamp) );
    if (!dataObj.isMessageDone || [_messageName isEqualToString:@"NO"])
        return false; // 출력할 메시지가 없슴.
    if (curTime - uiMessageTimestamp < 0.5) return false; // 먼저 끄고 좀 있다가 이거 실행..
    uiMessageTimestamp = [NSDate timeIntervalSinceReferenceDate]; // 타임은 여기서 시작.
    return true;
}

- (BOOL)uiMessageOff {
    double curTime = [NSDate timeIntervalSinceReferenceDate];
    //NSLog(@" 호흡 사인 관련 >>   off ? time : %f", (curTime - uiMessageTimestamp) );
    if (curTime - uiMessageTimestamp > 0.4) { // 켜져 있고, 0.4초 지났을 때.
        //NSLog(@"\n\n 호흡 사인 관련 >>  여기서 이미지를 끈다...  \n\n" );
        _messageName = @"NO";
        dataObj.isMessageDone = false;
        return true;
    }
    return false; // 0.4초 이내일 때.
}

#pragma mark - start compression | respiration    캘리브레이션 값 업데이트 등..  Process ...
- (void)startWholeInOneProcess {
    [log printThisFunc:@"startWholeInOneProcess" lnum:20];

    bleState = S_WHOLESTEP_DESCRIPTION;  _parsingState = AllInOne10; // 2가지 상태 변수 조절..
    
    _calcManager = [[HsCalcManager alloc] initWithBleObj:self bypassObj:bypass];
    [_calcManager prepareEntireProcess];

    
    HsBleManager.inst.stage = 1;
    [log logThis:@" set Stage = 1" lnum:3];
}

- (void)startCompressionProcess { //:(NSString*)option {
    [self logCallerMethodwith:@" BleMan >> startCompressionProcess setCurrentStep to   _COMPRESSION  " newLine:5];
    [HsBleManager.inst operationStart];
    bleState = (_parsingState == AllInOne10)? S_WHOLESTEP_COMPRESSION: S_STEPBYSTEP_COMPRESSION;
    [_calcManager initializeCompressionGoToNextSet];
}

- (void)startStepBreathProcess { // 단계별 학습만 부른다.
    [self logCallerMethodwith:@"  setCurrentStep to   _BREATH " newLine:5];
    NSLog(@"     %@ ", [HsUtil parseStr:self]);
    [HsBleManager.inst operationStart];
    dataObj.rpCorCount = dataObj.count = 0;
    //currentStep = (_parsingState == AllInOne10)? S_WHOLESTEP_COMPRESSION: S_STEPBYSTEP_COMPRESSION;
    //currentStep = S_STEPBYSTEP_BREATH;
}

//////////////////////////////////////////////////////////////////////       [ 키트에 캘리브레이션 값 저장 ]
- (void)saveCalibrationResultToKitWithComp:(int)comp andRspr:(int)rspr successBlock:(void (^)())successBlock {
    NSLog(@"  캘리브레이션 값 : 압박 : %d  호흡 : %d", comp, rspr);

    calibUpdateJob = [[UnitJob alloc] initWithName:@"CompressionCaliEditData"];
    didCaliPacketReceived = false;
    //161027 dataCalcObj.cc_cali_value = comp; // 앱에 반영 ...
    //161027 dataCalcObj.rp_cali_value = rspr;    [dataCalcObj setCaliValue];
    
    __weak typeof(self) wself = self;    __strong __typeof(wself) this = wself;
    
    calibUpdateJob.initJob = ^{ [this.cellService requestCompressionCaliEditData:comp]; };
    calibUpdateJob.didFinished = ^BOOL(void) {
        return didCaliPacketReceived; };
    calibUpdateJob.failJob = ^{ return; };
    calibUpdateJob.retryLimit = 30;
    
    UnitJob *resJob = [[UnitJob alloc] initWithName:@"RespirationCaliEditData"];
    calibUpdateJob.nextJobObj = resJob;    resJob.retryLimit = 30;
    
    //[calibUpdateJob makeNextUnitJob:@"RespirationCaliEditData"];
    resJob.initJob = ^{
        didCaliPacketReceived = false;
        [_cellService requestRespirationCaliEditData:rspr];
    };
    resJob.didFinished = ^BOOL(void) { return didCaliPacketReceived; };
    if (successBlock != nil) resJob.lastJob = successBlock;
    
    [calibUpdateJob startProcess];
}

//////////////////////////////////////////////////////////////////////     _//////////_     [ UI || UX    << Connection !!  이전 기기 연결.  >> ]    _//////////_
#pragma mark - Connection !!  이전 기기 연결.
- (void)connectPreviousDevice:(NSString *)prevDevNum withFailBlock:(void (^)(void))failureBlock {
    isPrevDeviceReconnect = true;
    connectRetryNum = 0;
    __weak HSCentralManager *weakMan = hsCentralManager; //  [HSCentralManager sharedService];
    __strong __typeof(weakMan) centralManager = weakMan;

    [self logCallerMethodwith:[NSString stringWithFormat:@" 이전 기기 연결 시작 >> 여긴 없는게 정상 <<   Scan Started with %@ peri. count : %lu",
                               prevDevNum, (unsigned long)centralManager.ymsPeripherals.count] newLine:30];
    //하나에 연결시켜야 한다. 연결되어있다면..
    if([centralManager.ymsPeripherals count] > 0) {
        [self logCallerMethodwith:@"  [centralManager.ymsPeripherals count]  OK " newLine:2];
        for(HSPeripheral *temp in centralManager.ymsPeripherals){
            if(temp.isConnected) { [temp disconnect]; }
        }
    } else
        [self logCallerMethodwith:@"  Peripheral 이 없다... 그래서 스캔 시작..  " newLine:4];
    [centralManager startScan];  // 여기서 명령을 내림..

    // 나중에 배터리 체크부터 할 때 아래 소스 사용..
    prevDevConnJob = [[UnitJob alloc] initWithName:@"기존 기기 연결"];
    prevDevConnJob.delayTime = 0.8;
    prevDevConnJob.initJob = ^{
        [self prevDeviceConnectionJob];
    };
    prevDevConnJob.didFinished = ^BOOL(void) { // return true; };  // 임시로 건너 뜀..
        return _connState == PrevConnectionStarted; };
    prevDevConnJob.retryLimit = 8;
    [prevDevConnJob startProcess];
    //[HsGlobal delay:0.8 closure:^{  // peripheral 의 어레이가 어디 있나?            }];

    [log logThis:@" [HsGlobal >>delay<<:12 연결 확인 타이머 설정.. ... " lnum:1];
    PrevKitConnCheck = [NSTimer scheduledTimerWithTimeInterval:10 target:self
                                                      selector:@selector(connectionCheck)
                                                      userInfo:nil repeats:NO];
    prevKitConnfailureBlock = failureBlock;


//    [HsGlobal delay:12 closure:^{  // 6초 후에 연결 성공했는 지 확인...  모든 작업 취소해야 함...
//        [log logThis:@" [HsGlobal >>delay<<:12 closure   연결 확인 부분 " lnum:1];
//        if (_connState != Connected) {
//            [log logThis:@" [HsGlobal >>delay<<:12 연결 안됐으므로 ... " lnum:1];
//            [log logThis:@" [HsGlobal >>delay<<:12 기존 기기 연결  최종 실패.  failure Block 실행  reset.. timerErrorBlock 은 nil  " lnum:1];
//            [self reset];
//            failureBlock();
//            _timerErrorBlock = nil;
//        }
//    }];
}

- (void)connectionCheck {
    [log logThis:@" [HsGlobal >>delay<<:12 연결 확인 ... " lnum:1];
    if (_connState == Connected) return;
    [log logThis:@" [HsGlobal >>delay<<:12 연결 안됐으므로 ... " lnum:1];
    [log logThis:@" [HsGlobal >>delay<<:12 기존 기기 연결  최종 실패.  failure Block 실행  reset.. timerErrorBlock 은 nil  " lnum:1];
    [self reset];
    [self stopScan];
    if (prevKitConnfailureBlock) prevKitConnfailureBlock();  // 연결이 안 되었다는 팝업...
    _timerErrorBlock = nil;
    [FinalConnCheck invalidate];  FinalConnCheck = nil;
}


- (void)prevDeviceConnectionJob {
    [self logMarkWith:@" Prev Conn ...."];  [self logMarkWith:@" Prev Conn ...."];
    [self logMarkWith:@"prevDeviceConnectionJob  이전 기기 연결 시도....."];

    NSString* previousName;
    if (AppIdTrMoTe == 0) {
        previousName = [[NSUserDefaults standardUserDefaults] stringForKey:@"PreviousDeviceNum"];
        _prevDevName = previousName;
    } else
        previousName = connectedName;

    NSLog(@"  접속하려는 키트 이름 : %@", previousName);

    for(YMSCBPeripheral *yperipheral in [hsCentralManager ymsPeripherals]) {
        [self logMarkWith: yperipheral.name ]; // 여기와 이름이 같아야...
        if ( [yperipheral.name isEqualToString:previousName]) {  // 이전 기기와 같으면..
            _connState = PrevConnectionStarted;
            [self logMarkWith:@"  _connState = PrevConnectionStarted;  "];
            _cellPeripheral = yperipheral;
            [self connectThisPeripheral:_cellPeripheral]; //// 이름 비교하여 연결..
        }
    }
}


// VC 에서 기기 변경할 때 불림.  기기 팝업을 띄움...
- (void)connect:(void (^)(void))setSearchBttnTxt andPopupBlock:(void (^)(void))showPopupView {
    [self logCallerMethodwith:@"<< SearchFlow02 >>" newLine:3];
    
    isPrevDeviceReconnect = false;
    connectRetryNum = 0;
    
    HSCentralManager *centralManager = hsCentralManager; //[HSCentralManager sharedService];
    
    //하나에 연결시켜야 한다. 연결되어있다면..
    if([centralManager.ymsPeripherals count] > 0) {
        for(HSPeripheral *temp in centralManager.ymsPeripherals){
            if(temp.isConnected) {
                [self logCallerMethodwith:@"   이미 연결 된 기기 있슴..   끊고 다시 시작.. .....  " newLine:2];
                
                [temp disconnect];
                connectRetryNum--;
                if (setSearchBttnTxt != nil) setSearchBttnTxt();
                //  151025 return;   끊었으면 기기를 선택하는 팝업을 띄워야지..
            }
        }
    }
    
    if (centralManager.isScanning == NO) {
        [centralManager startScan];  // 여기서 명령을 내림..
    }
    showPopupView(); // blockViewContrllerShowPopupView();  //__   __________     Block     __________
}

-(NSString*)bleButtonText // 버튼 스트링 표시 로직.
{
    //NSLog(@"  _connState  ::  %d  ", _connState);

    switch (_connState) {
        case Initial:
            return lObj.searching_kit; //  @"searching_kit";
            break;
        case PrevConnectionStarted:
            return lObj.searching_kit; // @"searching_kit";//@"pairing";
            break;
        case ConnectionStarted:
            return lObj.connecting_kit; // @"connecting_kit";
            break;
        case Connected:
            return lObj.connected; // @"bluetooth_connected";
        case Failed:
            return lObj.connect_fail; // @"connect_fail";
        default:
            return lObj.error; // @"error";
            break;
    }
}

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral {
    [self logCallerMethodwith:@" <<  Connected ...  @687 >> " newLine:3];
    [self logMarkWith:[NSString stringWithFormat:@" BleManager :: 연결>> connState : %d ", _connState]];
    if (_connState == Initial) {
        [self logCallerMethodwith:@"  central cancelPeripheralConnection:peripheral  " newLine:1];
        [central cancelPeripheralConnection:peripheral];
        return;
    }

    HSCentralManager *centralManager = hsCentralManager; // [HSCentralManager sharedService];
    YMSCBPeripheral *yp = [centralManager findPeripheral:peripheral];
    yp.delegate = self;
    [yp readRSSI];
}

// 연결이 끊길 때 오는 이벤트...
- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    if (connectRetryNum < 0) {
        [self logCallerMethodwith:@" CentralManager Delegate ...  일부러 끊을 때 ...   리트라이  안함.... " newLine:10];
        connectRetryNum++;
        return;
    }
    
    [self logCallerMethodwith:@" CentralManager Delegate ...  끊어졌슴..   connectRetryNum++;   리트라이...  여기부터 보자. " newLine:10];
    connectRetryNum++;
    NSLog(@"  \t\t\t\t\t connectRetryNum ::  %d", connectRetryNum);
    if (connectRetryNum < 5) [self connectThisPeripheral:_cellPeripheral];
}


//////////////////////////////////////////////////////////////////////     _//////////_     [ UI || UX         << SelectDeviceFlow>> ]    _//////////_
#pragma mark -   테이블 에서 셀을 선택했슴..  Connection  !!!
-(void)startConnection  // 리스트 뷰에서 기기를 선택하면 불림..
{
    [self logCallerMethodwith:@"  <<  SelectDeviceFlow02  >> " newLine:3];
    //NSLog(@"%@ deviceSelectedwith :   \t\t\t << DSFlow0? | SelectDeviceFlow02 >>   Started ....... \n\n", logHeader);
    
    _connState = ConnectionStarted;
    [self logMarkWith:@"  _connState = ConnectionStarted;  "];
    connectRetryNum = 0; // 리트라이를 스킵해야함..
    
    __block HSBaseService *theService;
    
    if (_cellPeripheral == nil) {
        NSLog(@"\n\n\n\n  (peripheral == nil)    \n\n");
    }
    
    if(_cellPeripheral.isConnected){
        NSLog(@"    deviceSelectedwith :   \t\t\t \t\t   isConnected ..  seldom case .... ");
        [_cellPeripheral disconnect];
        connectRetryNum--;
        return;
    }

    [self logCallerMethodwith:@"  <<  SelectDeviceFlow02  >> 페리퍼럴 리셋 와치독  " newLine:1];
    [_cellPeripheral resetWatchdog];
        
    //[_cellPeripheral setDelegate:self];  //  from ViewController - (void)connectDeviceResult:(HSPeripheral*)peripheral hsService
        
    [self logCallerMethodwith:[NSString stringWithFormat:@" Peri : %@", _cellPeripheral] newLine:0];

    [self connectThisPeripheral:_cellPeripheral];
}

- (void)connectThisPeripheral:(YMSCBPeripheral*)ymscbPeri {
    [ymscbPeri connectWithOptions:nil withBlock:
     ^(YMSCBPeripheral *yp, NSError *error) {
         // NSLog(@"\n\n \t\t\t << DSFlow01 | SelectDeviceFlow01 >>  for service  Start of BLOCK 00    in BleManager \n");
         if (error) {
             NSLog(@" deviceSelectedwith : withBlock  \t\t\t   error .. ");  // #warning 에러 알럿창 띄우고 앱 종료시키기
             return;
         }
         
         [yp discoverServices:[yp services] withBlock:
          ^(NSArray *yservices, NSError *error) {
              [self serviceDiscoveredCallBack:yservices withError:error]; }
          ];
     }];
}

- (void)serviceDiscoveredCallBack:(NSArray*)yservices withError:(NSError*)error {
    [self logCallerMethodwith:[NSString stringWithFormat:@"   <<  SelectDeviceFlow04   service.cnt : %d  state : %u >>  ", yservices.count, _connState] newLine:2];

    if (error) {
        NSLog(@" deviceSelectedwith :   \t\t\t   error another .. ");
        // #warning 에러 알럿창 띄우고 앱 종료시키기
        return;
    }
    __block HSBaseService *theService;
    
    for (HSBaseService *service in yservices) {
        [self logCallerMethodwith:[NSString stringWithFormat:@" service name : %@", service.name] newLine:0];
        if ([service.name isEqualToString:@"allInfo"]) {
            [self logCallerMethodwith:@" <<  SelectDeviceFlow05  >> 서비스 이름이 allInfo 임.. .. " newLine:2];
            
            theService = (HSBaseService *)service;
            _cellService = service;
            _cellService.weakManager = self;
            
            [service discoverCharacteristics:[service characteristics] withBlock:
             ^(NSDictionary *chDict, NSError *error) {
                 [self logCallerMethodwith:@"  Service안에 정의한 Characteristics가 있는지를 확인한다. " newLine:3];
                [service turnOn:
                 ^{ [self serviceTurnedOnCallBack]; }];
            }];
        }
    }
}

- (void)finalConnectionCheck {
    [log printAlways:@" [HsGlobal >>delay<<:18 serviceTurnedONCallBack  timerErrorBlock이 유효하면 실행... " lnum:5];
    //[self logCallerMethodwith:[NSString stringWithFormat:@" serviceTurnedOnCallBack  peri Num : %d  >L574", 3] newLine:10];
    if (_timerErrorBlock) { // 여기서 타이머 실패 창이 뜨지 않도록 함.
        _connState = Failed;
        [self logMarkWith:@"  _connState = Failed;  "];
        _timerErrorBlock();
        [PrevKitConnCheck invalidate];
        PrevKitConnCheck = nil;
        [self stopScan];
    }
}


-(void) serviceTurnedOnCallBack {
    bypass = [[HsBypassManager alloc] init]; // 이건 reset 에서 해야하는데.. blemanager 가 생성되기 전에 멈춤...
    [self logCallerMethodwith:@" << SelectDeviceFlow07  >> 배터리 체크 시작 " newLine:3];
    _parsingState = InitCalibration00;
    [log logThis:@" [HsGlobal >>delay<<:18 serviceTurnedONCallBack      타이머 설정.. ... " lnum:1];
    PrevKitConnCheck = [NSTimer scheduledTimerWithTimeInterval:18 target:self
                                                      selector:@selector(finalConnectionCheck)
                                                      userInfo:nil repeats:NO];
    [(SearchDeviceListView*)_tempViewObj sendMessageToVC:_curCell service:_cellService ];
    
    [self stopScan];

    __weak HsBleManager* this = self;
    // 나중에 배터리 체크부터 할 때 아래 소스 사용..
    initDeviceCheckJob = [[UnitJob alloc] initWithName:@"라벨 체크"];

    initDeviceCheckJob.initJob = ^{
        isDeviceCheckOK = false;
        [this.cellService requestLabelCheck]; };
    initDeviceCheckJob.didFinished = ^BOOL(void) { // return true; };  // 임시로 건너 뜀..
        return isDeviceCheckOK; };

    UnitJob *secJob = [[UnitJob alloc] initWithName:@" 키트 활성화 "];   initDeviceCheckJob.nextJobObj = secJob;
    secJob.initJob = ^{  isDeviceCheckOK = false; [_cellService requestKitEnable]; };
    secJob.didFinished = ^BOOL(void) { return isDeviceCheckOK; };

    UnitJob *firJob = [[UnitJob alloc] initWithName:@" 배터리 채크. "];  secJob.nextJobObj = firJob;
    firJob.initJob = ^{
        isDeviceCheckOK = false;
        [_cellService requestSensorInfo:0x01]; };
    firJob.didFinished = ^BOOL(void) { // return true; };  // 임시로 건너 뜀..
        return isDeviceCheckOK; };

    UnitJob *resJob = [[UnitJob alloc] initWithName:@"Accel Info Check : 0x03"];    firJob.nextJobObj = resJob;
    resJob.initJob = ^{
        _connState = ConnectionStarted; // 여기서 메시지를 바꿔주자.
        [self logMarkWith:@"  _connState = ConnectionStarted;  "];
        isDeviceCheckOK = false; [_cellService requestSensorInfo:0x03];
    };
    resJob.retryLimit = 10;
    resJob.didFinished = ^BOOL(void) { return true; }; // isDeviceCheckOK; }; 역시 임시로 ....
    
    secJob = [[UnitJob alloc] initWithName:@"ATM Info : 0x04"];            resJob.nextJobObj = secJob;
    secJob.initJob = ^{  isDeviceCheckOK = false; [_cellService requestSensorInfo:0x04]; };
    secJob.didFinished = ^BOOL(void) { return isDeviceCheckOK; };

    resJob = [[UnitJob alloc] initWithName:@"BLE 1 : 0x05"];
    secJob.nextJobObj = resJob;
    resJob.initJob = ^{
        bleAconnectedInMonitor = false;
        isDeviceCheckOK = false;
        [_cellService requestSensorInfo:0x05]; };
    resJob.didFinished = ^BOOL(void) { return isDeviceCheckOK; };

    //  instructorAppInfoRequest 를 부른다.
    UnitJob *addJob = [[UnitJob alloc] initWithName:@"AppInfoRequest"];
    addJob.retryLimit = 1;
    resJob.nextJobObj = addJob;
    addJob.initJob = ^{
        isDeviceCheckOK = false;
        if (AppIdTrMoTe == 0 && bleAconnectedInMonitor == true) {
            NSLog(@"  AppIdTrMoTe == 0 && bleAconnectedInMonitor == true  ");
            [_cellService instructorAppInfoRequest];
        } else {
            NSLog(@"  AppIdTrMoTe == 0 && bleAconnectedInMonitor == true    아님 ...  스킵...");
            isDeviceCheckOK = true;  // Skip this process..
        }
    };
    addJob.didFinished = ^BOOL(void) {
        return isDeviceCheckOK;
    };

    secJob = [[UnitJob alloc] initWithName:@"BLE 2 : 0x06"];
    addJob.nextJobObj = secJob;
    secJob.initJob = ^{     isDeviceCheckOK = false;     [_cellService requestSensorInfo:0x06]; };
    secJob.didFinished = ^BOOL(void) { return true; };

    addJob = [[UnitJob alloc] initWithName:@"D - Value"];
    secJob.nextJobObj = addJob;
    addJob.initJob = ^{
        isDeviceCheckOK = false;
        [_cellService requestDValueUpdate]; };
    addJob.didFinished = ^BOOL(void) { return true; };  // 나중에 애크는 처리...  일단은 보내기만 한다.








    resJob = [[UnitJob alloc] initWithName:@" request Compression "];
    addJob.nextJobObj = resJob;
    resJob.initJob = ^{     isDeviceCheckOK = false;     [_cellService requestCompressionCaliSetData]; };
    resJob.didFinished = ^BOOL(void) { return isDeviceCheckOK; };

    secJob = [[UnitJob alloc] initWithName:@" request Respiration "];
    resJob.nextJobObj = secJob;
    secJob.initJob = ^{    isDeviceCheckOK = false;     [_cellService requestRespirationCaliSetData]; };
    secJob.didFinished = ^BOOL(void) { return isDeviceCheckOK; };

//161027 //    secJob.lastJob = ^{
//        [self saveCalibrationResultToKitWithComp:dataCalcObj.cc_cali_value andRspr:dataCalcObj.rp_cali_value successBlock:^{
//            [self operationStart];
//        }];
//161027 //    };

    [initDeviceCheckJob startProcess];
    
    // 여기에 값을 에디트하는 부분 넣자..
}

- (void)stopScan
{
    [self logCallerMethodwith:@" stop scan " newLine:10];
    if(hsCentralManager.isScanning)   [hsCentralManager stopScan];
    [self logNewLine:10];
}


//////////////////////////////////////////////////////////////////////       [ CBCentralManagerDelegate  methods ]
#pragma mark - CBCentralManagerDelegate Methods

- (void)connectDeviceResult:(HSPeripheral*)peripheral service:(HSBaseService*)service{
    [self logCallerMethodwith:@"<< DSFlow02 >> {{ CBCentralManagerDelegate }}" newLine:3];
}

- (void)checkDevice:(HSPeripheral*)peripheral withService:(HSBaseService *)service{
    NSLog(@"___   \tHsBleManager  \t:: \t>>   checkDevice:peripheral  <<  >> ");
}

/// 제품 종류 학인..
- (BOOL)productTypeCheck {
    switch (AppIdTrMoTe) { // 0 : Trainer, 1 : Monitor, 2 : Test 트레이너는 그냥 세팅 안해도 됨..
        case 1:
            if (!(productType == 1 || productType == 3 || productType == 4 || productType == 5)) {
                _productTypeErrorEvent();
                return false;
            }
            break;
        case 2:
            if (!(productType == 2 || productType == 3 || productType == 4 || productType == 5)) {
                _productTypeErrorEvent();
                return false;
            }
            break;
        default:
            break;
    }
    return true;
}

//////////////////////////////////////////////////////////////////////       [ peripheral  ]   메인 파싱 ...   >>>>>
#pragma mark -  didUpdateValueForCharacteristic   메인 파싱 ...   >>>>>
- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic
             error:(NSError *)error {
    // 받은 내용 출력.
    NSData *data = characteristic.value;    unsigned char val[data.length];
    [data getBytes:&val length:data.length];
    char idx0 = val[0] & 0xFF, len = val[1] & 0xFF, cmd = val[2] & 0xFF, msg = val[3] & 0xFF, vl4 = val[4] & 0xFF;

    //NSLog(@" \t   HsBleManager :: > %@ <= = = = = = = = = = =  Len : %d, cmd : %d, msg : %d, vl4 : %d  >> 상태 : %@, %@,  bypass : %d",
      //    nick, len, cmd, msg, vl4, [HsUtil curStepStr:bleState],  [HsUtil parseStr:self], isBypassMode);

    [_cellService receivedData:characteristic]; // ACK 처리하도록 보냄..
    if (_connState == Connected)
        [operationWatchDog injectParsingAction:[[NSDate date] timeIntervalSinceReferenceDate]];
    
    if (idx0 != 2 || len > 30 || 0 > len || len > data.length) {
        [self logCallerMethodwith:[NSString stringWithFormat:@"   Parsing Error  ....  Wrong Packet >>>  STX : %d,  Len : %d, 실제 길이 : %lu", idx0, len, (unsigned long)data.length] newLine:1];
        isDeviceCheckOK = false;        return;    }

    [stateMan bypassDoWatch];

    if (cmd == 15) { // 접속 이후 최초 패킷..
        productType = msg;
        NSLog(@"\n\n\n                제품 타잎 : %d  \n\n\n", productType);
        if ([self productTypeCheck] == true) // 키트가 앱하고 맞아야 계속 진행..
            isDeviceCheckOK = true;
    }

    if (cmd == 8) { // 활성화 리턴.
        if (msg == 0x00) { // status 0 : permit, 1 : forbidden
            [self logCallerMethodwith:@"   활성화 리턴.   Status : 0  Permit....  " newLine:20];
            isDeviceCheckOK = true;
        } else {
            _productTypeErrorEvent();
        }
        return;
    }

    if (cmd == 7) {
        if (msg == 2) {
            isDeviceCheckOK = true;
            //161027
            /* char vl5 = val[5] & 0xFF;   NSLog(@"\n\n\n \t\t\t\t\t HsBleManager ::  @657  Calibration Value :  %d  \n", vl5);
            if (vl4 == 1)   { dataCalcObj.cc_cali_value = vl5;  NSLog(@"\t\t\t\t\t HsBleManager ::  @657  Calibration Comp  :  %d   \n\n\n", vl5);  }
            else            { dataCalcObj.rp_cali_value = vl5;  NSLog(@"\t\t\t\t\t HsBleManager ::  @657  Calibration Brth  :  %d   \n\n\n", vl5);
                [dataCalcObj setCaliValue];
                NSLog(@"\t\t\t\t\t HsBleManager ::  @657  Calibration Breath  :  %d \n\n\n ", vl5);
            }  //  */
            return;  // 초기 캘리브레이션
        }
        
        if (msg == 3 && (vl4 == 1 || vl4 == 2)) {
            NSLog(@"\n\n\n  isDeviceCheckOK = true;        return;  // 캘리브레이션 디바이스에 업데이트 성공.   \n\n\n");
            didCaliPacketReceived = true;        return;  // 캘리브레이션 업데이트 성공.
        }
    }
    
    if (cmd == 17) { // 바이패스 모드
//        [self logCallerMethodwith:@"    bypass mode ....    cmd == 17  came ...  parsing starting" newLine:1];
//
//        _cellService.titleAtoD = nick;
//
//        if (msg == 1) { // 모니터나 테스트에서 트레이너의 요청에 의해 현재 나의 상태를 보내는 부분.
//            NSLog(@"\n\n Instructor App Info 받음..  AppIdTrMoTe : %d 리턴..\n\n", AppIdTrMoTe);
//            if (calibrationMode) { [_cellService sendAppInfoCalibration]; return; } // 여기선 바로 리턴해야 함..
//            if (AppIdTrMoTe == 1) { [_cellService sendAppInfoResponseMonitor]; return; }
//            if (AppIdTrMoTe == 2) { [_cellService sendAppInfoResponseTest]; return; }
//        }
//
//        if (AppIdTrMoTe == 2) {
//            [_cellService sendAppInfoResponseTest]; return;
//        }
//        
//        if (AppIdTrMoTe == 1) { // Monitor
//            switch (msg) {
//                //case 1: // App Info 를 알려줌.  [_cellService sendAppInfoResponseMonitor];
//                    //break;
//                case 2: // 스크린 애크임..
//                    [log printAlways:@"  모니터 액크 <스크린 애크> 받음..   02 06 11 02 12 03" lnum:3];
//                    [bypass bypassParseOfMonitor:val pMsg:msg];
//                    [_cellService ackReceivedInMonitor];
//                    monitorAck = true;  break;
//                default:  break;
//            }
//            return;
//        }
//
//
//        if (_connState != Connected) {
//            isDeviceCheckOK = true;
//            //[bypass enterBypassModeAtConnection];
//        }
//
//
//        isBypassMode = true;  // NSLog(@"  HsBleMan >>> L815  ");
//        Byte rBypass = [bypass bypassParseOfTrainer:val pMsg:msg];
//        // 0 ..  1 : App Info.    2 : ack 보내기
//        switch (rBypass) {
//            case 1: // 접속 초기에 App Info 가 왔다.
//                isDeviceCheckOK = true;
//                ; // 모든 점검 후 초기 화면에서 바이패스 레디 화면으로 넘어간다.
//                break;
//            case 2: // 스크린 정보..  애크를 보냄.
//                [_cellService ackToMonitor];
//                return;
//            default:  break;
//        }
//        //if (rBypass > 0) isDeviceCheckOK = true;
//        NSLog(@"  Bypass parsing ...  rBypass : %d (1:App, 2:Ack)     isbypass mode ? %d ", rBypass, isBypassMode );
//        [self operationCheck];
//        return;
    }

    if (cmd == 16) { // 맨 밑에 있던 것..  디버깅 하면서 옮기다..
//        [self setSpeedLogic];
//        switch (_parsingState) {
//            case InitCalibration00:
//                [self connectionDataParsing:data]; break;  // 0x10  connection 할 때 캘리브레이션 값..
//            case CalibrationView01:
//                [_calcManager calibrationViewDataParsing:val]; break; // 캘리브레이션 세팅 화면에서..
//            case AllInOne10:
//                [_calcManager allInOneDataParsing:val]; break; // 올인원은 따로..
//            default:
//                [_calcManager stepByStepParsing:val]; break;  // 스텝스텝 .. 여기서.
//        }
//    } else { // 배터리 등 센서 체크,
//        [self didUpdateCalibration:characteristic.value];
    }

    //  리턴 케이스..  파싱할 필요가 없슴.
    if (isBypassMode && bleState == S_READY) { // 160215_v1.0(4)
        if (cmd == 5 && msg == 1) { // BLE 1 이 Disconnected..  모니터가 떨어져 나감...
            isBypassMode = false;
            NSLog(@"\n\n\n\n\n \t\t\t\t (cmd == 5 && msg == 1) >> BLE 1 이 Disconnected.. >> 모니터가 떨어져 나감... ");
        } else {
            return; // 여기서 나갈 수도 있슴..
        }
    }

    if (_parsingState == StandBy) return;  //[self logCallerMethodwith:@"   _parsingState == StandBy   return  " newLine:3];
}

- (void)setSpeedLogic {

}

- (void)operationCheck { // 현재 상태가 압박 | 호흡 이면 operation start  아니면 stop..
    [self logCallerMethod];
    switch (bleState) {
        case S_STEPBYSTEP_BREATH: case S_STEPBYSTEP_COMPRESSION: case S_WHOLESTEP_BREATH: case S_WHOLESTEP_COMPRESSION: case S_WHOLESTEP_PRECPR: case S_WHOLESTEP_DESCRIPTION:
            [self operationStart];        break;
        default:
            [self operationStop];         break;
    }
}

- (void)didUpdateCalibration:(NSData*)data {
    unsigned char datas[data.length];
    [data getBytes:&datas length:data.length];
    UInt16 len = datas[1] & 0xFF, cmd = datas[2] & 0xFF, value = datas[3] & 0xFF;
    [self logCallerMethodwith:[NSString stringWithFormat:@" <<   %@  >> len : %d, cmd : %d, val : %d", data, len, cmd, value] newLine:1];

    if (cmd < 0 || 7 < cmd) return; // 패킷 오류 케이스..
    
    if (cmd <= 6) { // 2 : piezo.  센서들...
        [self sensorCheckProcess:cmd withValue:value];
        return;
    } else {
        [self logCallerMethodwith:[NSString stringWithFormat:@" <<  CalibrationFlow 8  >> len : %d, val : %d", len, cmd] newLine:1];
        [self caliValueDataParsing:data];
    }
}

//////////////////////////////////////////////////////////////////////       [  parsing... ]
#pragma mark - 접속 초기 배터리 체크 등 ...
- (void)sensorCheckProcess:(UInt16)command withValue:(UInt16)value {
    //[self logCallerMethodwith:[NSString stringWithFormat:@" cmd : %d, val : %d", command, value] newLine:2];
    
    if (command == 1) { // 배터리..
        if (value > 5) {  // 25 % 이상..
            isDeviceCheckOK = true;
            [self logCallerMethodwith:@" Battery OK : continue ..  Sending Peizo Check.. " newLine:10];
            NSLog(@"\t\t\t  Batery :: %d remained ... \n\n\n\n\n", value);
        } else {
            NSLog(@"\t\t\t  Batery :: %d remained ... \n\n\n\n\n", value);
            _exceptionProc(1, -1);
        }
        return;
    }
    
    if (value == 2)
        _exceptionProc(command, value);
    
    if (command == 5) {  // BLE 1 ...
        [self logCallerMethodwith:@"  BLE 1....   value ?   " newLine:10];
        //NSLog(@"   value : %d  ", value);
        switch (value) {
            case 0: // '모니터' 또는 뭔가 와 연결...
                // 디바이스가 모니터와 연결..
                isDeviceCheckOK = true;
                if (AppIdTrMoTe == 0) {
                    bleAconnectedInMonitor = true;
                }








                    // [bypass enterBypassModeAtConnection]; // 트레이너는 바이패스 모드로..










                break;
            case 1: // not connected ..
                isDeviceCheckOK = true;
                if (_connState == Connected && isBypassMode) {
                    [log logUiAction:@" 블루투스 신호 단절 메시지...  Not Connected    S_QUIT 로 변환.  " lnum:20 printOn:true];
                    bleState = S_QUIT;
                }
                break;
            default:  // 2 : error ..
                _exceptionProc(command, value); // value = 1 케이스
                break;
        }
        return;
    }

    if (command == 6) { // BLE 2..
        [self logCallerMethodwith:@"  BLE 2....   value ?   " newLine:10];
        NSLog(@"   value : %d  ", value);
        if (value == 0) {  isDeviceCheckOK = true;  _cellService.isTrainerALive = true;  return;        }
        
        if (AppIdTrMoTe == 1 && value == 1) { // 모니터..
            _cellService.isTrainerALive = false; return;
        }
        _exceptionProc(command, value); //
        return;
    }
    
    if (value == 0)
        // [_cellService request SensorInfo:sendCmd];  다음번 0x0? 번호..
        isDeviceCheckOK = true; // 제대로 된 값이 온 경우.
    else
        _exceptionProc(command, value);
}


#pragma mark - 캘리브레이션 >>   초기 값 10개 설정..  state : Connected ..
- (void)connectionDataParsing:(NSData*)data{  }
//    
//    [self logCallerMethodwith:[NSString stringWithFormat:@" <<  data : %@  >> ", data] newLine:0];
//    NSLog(@"  \n\n\n ");
//    
//    [dataCalcObj sensorDataParsing:data successBlock:
//     ^{
//        [self logCallerMethodwith:@" This is BleMan's successBlock.   Called from calculator .." newLine:10];
//        // 10/16 일단 막아놓자... [_cellPeripheral setDelegate:nil];  // ???  이게 왜 ...
//         NSLog(@"\n\n\n \t\t\t  startEndValues ::  CC_START_POINT, CC_END_POINT, RP_START_POINT, RP_END_POINT)   %ld %ld %ld %ld",
//               (long)dataObj.ccStaPoint , (long)dataObj.ccEndPoint, (long)dataObj.rpStaPoint, (long)dataObj.rpEndPoint);
//
//
//         [HsGlobal delay:0.2 closure:^ {
//             [_cellService requestOperationStop];
//         }];
//
//
//
//
//         _timerErrorBlock = nil;
//         _connState = Connected;  // 이걸 나중으로 옮겨야 할까?  여기가 자리다..
//
//         connectedName = _cellPeripheral.name;
//
//         _goToSelectionModeView();
//         _parsingState = StandBy;
//         
//         [[NSUserDefaults standardUserDefaults] setObject:_cellPeripheral.name forKey:@"PreviousDeviceNum"];
//
//         NSLog(@"\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
//         NSLog(@"  \t\t\t\t\t 화면 전환..  접속 완전 성공   _parsingState = StandBy;   Device name : %@   ", _cellPeripheral.name);
//         NSLog(@"\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
//
//
//         [PrevKitConnCheck invalidate];
//         [FinalConnCheck invalidate];
//
//         [self connectCompletedAction];
//    }];


- (void)connectCompletedAction {

}

- (void)xxconnectionSuccessAction {       // 디바이스가 모니터와 연결..
    if (bypass.isMonitor.value) {
        
        initDeviceCheckJob = [[UnitJob alloc] initWithName:@"바이패스 ..  앱 인포 요청"];
        
        __weak HsBleManager* this = self;
        
        initDeviceCheckJob.initJob = ^{
            NSLog(@"bypass.isMonitor.value is true     sending....     instructorAppInfoRequest isMonitor : %ld", (long)this.bypass.isMonitor.intVal);
            [this.cellService instructorAppInfoRequest];
        };
        initDeviceCheckJob.didFinished = ^BOOL(void) { return bypass.isMonitor.intVal > 0; };

        [initDeviceCheckJob startProcess];
    }
}

- (void)caliValueDataParsing:(NSData*)data {  // cmd == 7 인 경우 ..
    [self logCallerMethodwith:@" <<  CalibrationFlow 9  >> " newLine:3];

    //161027
    /*
    [dataCalcObj sensorCalibrationData:data successBlock:
     ^(HSCommendList sensor, HSCaliResultType result, UInt16 resultData)
     {
         isDeviceCheckOK = true;
         if(sensor == HSPressureModule){ // 압력 모듈
             NSLog(@"___   BleMan   초기 캘리브레이션..  압력모듈 ..    결과값 : %d", resultData);
             if(result == HSCaliSuccessType) {
                 //다음 스텝으로
                 checkCompressionCaliSetData = YES;
             }else{
                 [self logCallerMethodwith:@"HSCaliSuccessType  NOT " newLine:3];
                 //edit 요청
                 //  일단 패스...  [_cellService requestCompressionCaliEditData:resultData];
             }
         }
         if(sensor == HSATMModule){ // 기압 모듈
             NSLog(@"___   BleMan   초기 캘리브레이션..  기압모듈 ..    결과값 : %d", resultData);
             if(result == HSCaliSuccessType) {
                 //다음 스텝으로 (operation 요청)
                 checkRespirationCaliSetData = YES;
             }else{
                 [self logCallerMethodwith:@"requestRespirationCaliEditData" newLine:3];
                 //edit 요청
             }
         }
     }];   //   */
}


#pragma mark - CentralManager  Delegate     ...   Search Flow ..

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral
     advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI {  //  기기 탐색 ...   반복 되는 부분
    // NSLog(@"%@ centralManager : didDiscoverPeripheral \t\t\t  반복 되는 부분   ", logHeader);
    
    for(YMSCBPeripheral *yperipheral in [hsCentralManager ymsPeripherals]){
        [arrBle addObject:@{@"text":[yperipheral name]}];
    }
    NSOrderedSet *userSet = [[NSOrderedSet alloc] initWithArray:arrBle];
    arrBle = [[NSMutableArray alloc] initWithArray:[userSet array]];
    
    HSCentralManager *centralManager = hsCentralManager;
    if (centralManager.isScanning && blockUpdateDevListView) {
        // 디바이스 리스트 뷰로 콜백을 보낸다..  데이터를 업데이트한다.
        blockUpdateDevListView(hsCentralManager);
    }
}

- (void)centralManager:(CBCentralManager *)central didRetrieveConnectedPeripherals:(NSArray *)peripherals {
    [self logCallerMethodwith:@"  to erase ??  @ 716" newLine:10];
    HSCentralManager *centralManager = hsCentralManager;
    for (CBPeripheral *peripheral in peripherals) {
        YMSCBPeripheral *yp = [centralManager findPeripheral:peripheral];
        if (yp) {
            yp.delegate = self;
        }
    }
}

//////////////////////////////////////////////////////////////////////       [ CBCentralManagerDelegate  methods ]
#pragma mark - CBCentralManagerDelegate Methods    Central Device 로 기능 가능한 지 여부 검토.
- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{ // CBCentralManager 를 생성하면 이놈을 부른다.
    [self logCallerMethodwith:[NSString stringWithFormat:@"central:state >> %d", central.state] newLine:10];
    switch (central.state) {
        case CBCentralManagerStatePoweredOn:
            [self logCallerMethodwith:@"CBCentralManagerStatePoweredOn" newLine:1]; // log ..
            break;
        case CBCentralManagerStatePoweredOff:
            [self logCallerMethodwith:@"CBCentralManagerStatePoweredOff" newLine:1]; // log ..
            break;
            
        case CBCentralManagerStateUnsupported:
        {
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle:@"Dang." message:@"Unfortunately this device can not talk to Bluetooth Smart (Low Energy) Devices"
                                  delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil];
            [alert show];       break;
        }
        case CBCentralManagerStateResetting:
            [self logCallerMethodwith:@"CBCentralManagerStateResetting" newLine:1]; // log ..
            break;
        
        case CBCentralManagerStateUnauthorized:
            break;
            
        case CBCentralManagerStateUnknown:
            NSLog(@" case : CBCentralManagerStateUnknown ");
            break;
        default:
            break;
    }
}

@end

