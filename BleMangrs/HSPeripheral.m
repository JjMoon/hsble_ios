//
//  HSPeripheral.m
//  heartisense
//
//  Created by jeongyuchan on 2015. 4. 21..
//  Copyright (c) 2015년 gyuchan. All rights reserved.
//

#import "HSPeripheral.h"
#import "YMSCBCharacteristic.h"
#import "YMSCBDescriptor.h"
#import "YMSCBService.h"

#import "HSBaseService.h"

#import "TISensorTag.h"

@implementation HSPeripheral

- (instancetype)initWithPeripheral:(CBPeripheral *)peripheral
                           central:(YMSCBCentralManager *)owner
                            baseHi:(int64_t)hi
                            baseLo:(int64_t)lo {
    
    self = [super initWithPeripheral:peripheral central:owner baseHi:hi baseLo:lo];
    
    if (self) {
        HSBaseService *allInfo = [[HSBaseService alloc] initWithName:@"allInfo" parent:self UUID:SERVICE_UUID_STRING];
//        HSDeviceInfoService *allInfo = [[HSDeviceInfoService alloc] initWithName:@"allInfo" parent:self baseHi:hi baseLo:lo serviceOffset:SERVICE_UUID];

        self.serviceDict = @{@"allInfo": allInfo};
        
    }
    return self;
    
}

- (HSBaseService *)allInfo{
    return (HSBaseService *)[self findService:self.serviceDict[@"allInfo"]];
//    return self.serviceDict[@"allInfo"];
}

@end
