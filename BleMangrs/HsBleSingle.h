//
//  HsBleSingle.h
//  Trainer
//
//  Created by Jongwoo Moon on 2015. 11. 11..
//  Copyright © 2015년 IMLab. All rights reserved.
//

#ifndef HsBleSingle_h
#define HsBleSingle_h


#endif /* HsBleSingle_h */


#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

@class StrLocBase, Shl, HsBypassInt;

//@class MuState, HsCalcManager, HsBypassManager, HsStateManager;

//@protocol BleManToVCtrlProtocol <NSObject>
//@required
//- (void)dataParse:(CBCharacteristic*)charObj;
//
//@end



@interface HsBleSingle : NSObject
{
    
}



+ (HsBleSingle*)inst; // singletone

- (void)setLanguageString;

//+ (void)resetInst;
@property (nonatomic) NSArray* scoreOption;

@property (weak, nonatomic) HsBypassInt *cprProto, *manikinKind, *audioCountInterval;
@property (weak, nonatomic) Shl *shlObj;
@property (weak, nonatomic) StrLocBase *langObj;
@property (nonatomic) int stageLimit, scoreWeight, dateFormat; // 세트 수, 세팅에서 정한 세트 수.. 1 .. 5


@end

