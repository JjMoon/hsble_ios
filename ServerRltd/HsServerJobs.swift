//
//  HsServerJobs.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2015. 12. 17..
//  Copyright © 2015년 IMLab. All rights reserved.
//

import Foundation

class HsAuthJobs: HsJsonBase {
    static var singltn = HsAuthJobs()
    //var log = HtLog(cName: "HsServerJobs")
    
    func authRequest(email: String, pass: String, successBlck: ()->(), failureBlck: ()->() ) { // email, password
        //jStr = "email=admin@imlabworld.com&password=nimda"
        jStr = "email=\(email)&password=\(pass)"
        successJob = successBlck;  failJob = failureBlck

        requestSecure("authenticate", finishClj: {  // request("authenticate.php", finishClj: {
            self.parseAuth()
        })
    }

    func parseAuth() {
        log.printThisFunc("  parseAuth  ")
        do {
            if let jDic: NSDictionary = try (NSJSONSerialization.JSONObjectWithData (self.jData!, options: NSJSONReadingOptions.MutableContainers) as? NSDictionary)! {
                result = jDic["success"] as! Int
                log.logThis("data >>  \(jData)   result : \(result)") //  serviceCode : \(jDic["serviceCode"])  result : \(jDic["result"])  ")
                if result == 1 {
                    let recordid = jDic["recordid"] as! String
                    instructorID = UInt(recordid)!
                    // 디바이스에 저장하지 않는다... HsBleMaestr.inst.usrDefObj.writeInstructorID()
                    
                    log.logUiAction("  Auth 성공 ..  recoird id str : \(recordid)  instructorID : \(instructorID)")
                    successJob!()
                } else {
                    failJob!()
                }
                
                //log.logThis("  parseAuth result : \(result)    0 : 실패,   1 : 성공 ")
            }
        } catch {
        }
    }

}

class HsCheckLicense: HsJsonBase {
    static var singltn = HsCheckLicense()
    //var log = HtLog(cName: "HsCheckLicense")

    func checkLicence(successBlck: Void -> Void, failureBlck: Void -> Void ) {
        makeHeader("")
        jStr = "id=\(instructorID)"
        successJob = successBlck;  failJob = failureBlck

        requestSecure("check_license") { () -> Void in
            self.parseLicense()
        }
    }

    func parseLicense() {
        log.clsName = "HsCheckLicense"
        log.printThisFunc("  parseLicense  ")
        do {
            if let jDic: NSDictionary = try (NSJSONSerialization.JSONObjectWithData (self.jData!, options: NSJSONReadingOptions.MutableContainers) as? NSDictionary)! {

                if jDic["error"] != nil && 2 < (jDic["error"] as! String).getLength() { // 에러 케이스
                    result = -1
                } else {
                    result = Int(jDic["id"] as! String)!  // 이놈이 라이센스 번호 임...  매번 바뀐다..
                }
                log.logThis(" 라이센스 번호 >> : \(result)")
                if 0 < result  {
                    log.logUiAction("  라이센스 획득 성공 ..   : \(result) ")
                    successJob!() // 여기서 다음 작업을 한 후...
                } else {
                    log.logUiAction("  실 패 ")
                    failJob!()
                }
                //log.logThis("  result : \(result)    0 : 실패,   1 : 성공 ")  이 로그가 찍히면 헷갈린다..
            }
        } catch {
        }
    }
}

class HsAddStudentJobs: HsJsonBase {
    static var singltn = HsAddStudentJobs()
    //var log = HtLog(cName: "HsSendStudentJobs")
    var curStudent: HmStudent?

    func addStudentRequest(sObj: HmStudent, successBlck: ()->(), failureBlck: ()->() ) { // email, password
        curStudent = sObj
        jStr = ""
        addKeyValue("name", value: sObj.name, withFinishComma: true)
        addKeyValue("phone", value: sObj.phone, withFinishComma: true)
        //addKeyValue("instructor", value: HsSetting.singltn.instructorID, withFinishComma: true)
        addKeyValue("instructor", value: instructorID, withFinishComma: true)
        addKeyValue("student", value: "NONE")
        addKeyValue("email", value: sObj.email, withFinishComma: false)
        finishEncoding()

        jStr = "student=\(jStr)"

        successJob = successBlck
        failJob = failureBlck
        requestSecure("send_student", finishClj: {
            self.parseResp()
        })
    }

    func parseResp() {
        log.printThisFunc("  parse  ")
        do {
            if let jDic: NSDictionary = try (NSJSONSerialization.JSONObjectWithData (self.jData!, options: NSJSONReadingOptions.MutableContainers) as? NSDictionary)! {
                log.logThis("data >>  \(jData) ") //  serviceCode : \(jDic["serviceCode"])  result : \(jDic["result"])  ")
                result = jDic["success"] as! Int
                
                if result == 1 {
                    let recordid = jDic["recordid"] as! UInt
                    //let msg = jDic["message"]
                    //HsSetting.singltn.instructorID = UInt64(recordid)!
                    log.logUiAction("  학생 등록 성공 ..  id: \(recordid),  :) ")
                    curStudent!.svrID = recordid
                    curStudent!.updateDatabase()
                    successJob!()
                } else {
                    failJob!()
                }
                
                //log.logThis(" HsAddStudentJobs result : \(result)    0 : 실패,   1 : 성공 ")
            }
        } catch {
        }
    }
}
