//
//  HsMonitorServerJobs.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2016. 3. 11..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation


/// 모니터 서버

class HsStudentDataJobs: HsJsonBase {
    static var singltn = HsStudentDataJobs()
    //var log = HtLog(cName: "HsStudentDataJobs")

    func sendDataRequest(studntObj: HmStudent, successBlck: ()->(), failureBlck: ()->() ) { // email, password
        log.printThisFunc("sendDataRequest")
        successJob = successBlck
        failJob = failureBlck
        setJsonString(studntObj)
        //requestSecure("sendMonitorRecord.php", finishClj: {
        requestSecure("send_monitor_record", finishClj: {
            self.parseResp()
        })
    }
    func sendOldDataRequest(studntObj: HmStudent, successBlck: ()->(), failureBlck: ()->() ) { // email, password
    }

    func setJsonString(studntObj: HmStudent) {
        jStr = ""

        let dObj = studntObj.myDataFromDB
        let curDate = NSDate(timeIntervalSinceNow: 0)
        var pasStr = 0

        addKeyValue("license", value: licenseID)
        addKeyValue("version", value: 3)
        addKeyValue("standard", value: studntObj.getStandardIdx() + 1)
        addKeyValue("timestamp", value: dObj!.timeStamp.formatYYMMDDspaceTime)
        addKeyValue("class_idx", value: 0)

        addKeyArray("score", value: dObj!.scoreJson()) /// 점수..

        addKeyArray("comp_count", value: dObj!.ccCountJson(), withFinishComma: true)
        addKeyValue("studentID", value: studntObj.svrID, withFinishComma: true)  // 서버에서 받은 '아이뒤'
        addKeyArray("comp_depth", value: dObj!.compDepthJson(), withFinishComma: true)
        addKeyArray("comp_recoil", value: dObj!.compRecoilJson(), withFinishComma: true)
        addKeyValue("recordid", value: dObj!.dbRecordID, withFinishComma: true)
        addKeyValue("timestamp", value: curDate.formatYYMMDDspaceTime, withFinishComma: true)
        addKeyArray("percent", value: dObj!.rightPositionPercent(), withFinishComma: true)

        addKeyValue("overall", value: dObj!.scoreTotalInt, withFinishComma: true)

        addKeyArray("comp_cycle", value: dObj!.compStrokeJson(), withFinishComma: true)
        if dObj!.discreminantPass { pasStr = 1 }
        addKeyArray("hands_off_time", value: dObj!.handsOffTime(), withFinishComma: true)
        addKeyArray("resp_correct", value: dObj!.rpCorrectJson(), withFinishComma: true)
        addKeyArray("comp_correct", value: dObj!.ccCorrectJson(), withFinishComma: true)
        addKeyValue("pass", value: pasStr, withFinishComma: true)
        addKeyArray("resp_vol", value: dObj!.breathVolJson(), withFinishComma: true)


        addKeyArray("resp_cycle", value: dObj!.breathCycleJson())


        addKeyValue("instructor", value: instructorID, withFinishComma: true)
        addKeyArray("resp_count", value: dObj!.rpCountJson(), withFinishComma: true)

        jStr += dObj!.positionJson()

        finishEncoding()

        jStr = "monitor=\(jStr)"

        print(jStr)

    }

    func parseResp() {
        log.printThisFunc("  parse  ")
        do {
            if let jDic: NSDictionary = try (NSJSONSerialization.JSONObjectWithData (self.jData!, options: NSJSONReadingOptions.MutableContainers) as? NSDictionary)! {
                log.logThis("data >>  \(jData) ") //  serviceCode : \(jDic["serviceCode"])  result : \(jDic["result"])  ")
                result = jDic["success"] as! Int
                if result == 1 {
                    let recordid = jDic["recordid"] as! Int
                    let msg = jDic["message"]
                    //HsSetting.singltn.instructorID = UInt64(recordid)!
                    log.logUiAction("  측정 결과 등록 성공 ..  id: \(recordid),  message : \(msg) ")
                    successJob!()
                } else {
                    failJob!()
                }

                log.logThis("  result : \(result)    0 : 실패,   1 : 성공 ")
            }
        } catch {
        }
    }
    
}

