//
//  HsTestServerJobs.swift
//  Test
//
//  Created by Jongwoo Moon on 2016. 3. 11..
//  Copyright © 2016년 IMLab. All rights reserved.
//
//

import Foundation



/// Test App 에서 다시 정의했슴.
class HsStudentDataJobs: HsJsonBase {
    static var singltn = HsStudentDataJobs()

    func sendDataRequest(studntObj: HmStudent, successBlck: ()->(), failureBlck: ()->() ) { // email, password
        log.clsName = "HsStudentDataJobs"
        log.printThisFunc("sendDataRequest")

        setJsonString(studntObj)
        successJob = successBlck
        failJob = failureBlck
        requestSecure("send_test_record", finishClj: {
            self.parseResp()
        })
    }
    
    func sendOldDataRequest(studntObj: HmStudent, successBlck: ()->(), failureBlck: ()->() ) { // email, password
        log.clsName = "HsStudentDataJobs"
        log.printThisFunc("sendOldDataRequest")
        
        setOldJsonString(studntObj)
        successJob = successBlck
        failJob = failureBlck
        requestSecure("send_test_record", finishClj: {
            self.parseResp()
        })
    }

    func parseResp() {
        log.printThisFunc("  TestServer >> parse    \(self.jData!)")
        do {
            if let jDic: NSDictionary = try (NSJSONSerialization.JSONObjectWithData (self.jData!, options: NSJSONReadingOptions.MutableContainers) as? NSDictionary)! {
                log.logThis("data >>  \(jData) ") //  serviceCode : \(jDic["serviceCode"])  result : \(jDic["result"])  ")
                result = jDic["success"] as! Int
                log.logThis("  result :  >>  \(result)")

                if result == 1 {
                    let recordid = jDic["recordid"] as! Int
                    let msg = jDic["message"]
                    //HsSetting.singltn.instructorID = UInt64(recordid)!
                    log.logUiAction("    측정 결과 등록 성공 ..  id: \(recordid),  message : \(msg) ")
                    self.successJob!()
                } else {
                    self.failJob!()
                }
                //log.logThis(" 테스트 자료 response  result : \(result)    0 : 실패,   1 : 성공 ")
            }
        } catch {
            log.printAlways("  Catch   뭔가 받았지만 파싱 에러....   self.failJob?() 을 부름...   ", lnum: 5)
            self.failJob?()
        }
    }
    
    func setOldJsonString(studntObj: HmStudent) {
        jStr = ""
        
        let dObj = studntObj.myDataFromDB
        let curDate = NSDate(timeIntervalSinceNow: 0)
        var pasStr = 0
        
        addKeyValue("license", value: licenseID)
        addKeyValue("version", value: 3, withFinishComma: true) /// server ver2  -->  ver3  2016.10.18
        addKeyValue("studentID", value: studntObj.svrID, withFinishComma: true)  // 서버에서 받은 '아이뒤'
        addKeyValue("instructor", value: instructorID, withFinishComma: true)
        var stanIdx02 = studntObj.getStandardIdx()
        if stanIdx02 < 0 { stanIdx02 = 1 }
        else { stanIdx02 += 1 }
        addKeyValue("standard", value: stanIdx02) //print("  after ... stanIdx02  \(stanIdx02)")
        
        addKeyValue("timestamp", value: studntObj.getOperateTime()!.formatYYMMDDspaceTime)
        addKeyValue("class_idx", value: -1)
        
        let adultObj = studntObj.getOldAdultFromDB()
        
        print("\(#function) ::  adult  \t\t @ \(#line)  완료?  \(studntObj.finishedAdultOld)  몇세트? \(studntObj.myDataFromDB?.arrSet.count)")
        
        //addKeyArray("score", value: dObj!.scoreJson()) /// 점수..
        
        if studntObj.finishedAdultOld  { //  dObj?.arrSet.count
            
            print("\(#function) ::  성인 케이스  \t\t @ \(#line)")
            
            addKeyArray("comp_count", value: dObj!.ccCountJson(), withFinishComma: true)
            addKeyArray("comp_depth", value: dObj!.compDepthJson(), withFinishComma: true)
            addKeyArray("comp_recoil", value: dObj!.compRecoilJson(), withFinishComma: true)
            
            addKeyArray("percent", value: dObj!.rightPositionPercent(), withFinishComma: true)
            
            //addKeyValue("overall", value: dObj!.discreminant(), withFinishComma: true)
            addKeyValue("overall", value: dObj!.scoreTotalInt, withFinishComma: true)
            
            addKeyArray("comp_cycle", value: dObj!.compStrokeJson(), withFinishComma: true)
            addKeyArray("hands_off_time", value: dObj!.handsOffTime(), withFinishComma: true)
            addKeyArray("resp_correct", value: dObj!.rpCorrectJson(), withFinishComma: true)
            addKeyArray("comp_correct", value: dObj!.ccCorrectJson(), withFinishComma: true)
            if dObj!.discreminantPass { pasStr = 1 }
            addKeyValue("pass", value: pasStr, withFinishComma: true)
            addKeyArray("resp_vol", value: dObj!.breathVolJson(), withFinishComma: true)
            
            addKeyArray("resp_cycle", value: dObj!.breathCycleJson())
            
            addKeyArray("resp_count", value: dObj!.rpCountJson(), withFinishComma: true)
            jStr += dObj!.positionJson() + ", "
        } else {
            log.printThisFNC("sendDataRequest", comment: " Adult 측정 자료 없슴.....")
        }
        
        /// 성인 측정 결과
        if studntObj.finishedAdultOld {
            addKeyArray("score", value: dObj!.scoreJson()) /// 점수..
            let operStr = studntObj.operationJsonString()
            addKeyString("adult", value: adultObj!.adultJson(operStr)) // Test JSON
            addKeyValue("cpr_pass", value: adultObj!.bool2int(adultObj!.passRes1))
            addKeyValue("aed_pass", value: adultObj!.bool2int(adultObj!.passRes2))
        } else {
            addKeyValue("adult", value: "null")  // addKeyString("adult", value: "null")
        }
        /// 영아 측정 결과
        let infntObj = studntObj.getOldInfantFromDB()
        if !studntObj.finishedInfantOld {
            addKeyValue("infant", value: "null", withFinishComma: false)
        } else {
            addKeyString("infant", value: makeCaretItem(infntObj!.infantJson()), withFinishComma: false) //
        }
        
        finishEncoding()
        
        jStr = "test=\(jStr)"
        
        print(jStr)
    }

    func setJsonString(studntObj: HmStudent) {
        log.printAlways("\(#function)   Student Name : \(studntObj.name) ")
        jStr = ""

        let dObj = studntObj.myDataFromDB
        let curDate = NSDate(timeIntervalSinceNow: 0)
        var pasStr = 0

        addKeyValue("license", value: licenseID)
        addKeyValue("version", value: 2, withFinishComma: true) /// server ver2  -->  ver3  2016.10.18
        addKeyValue("studentID", value: studntObj.svrID, withFinishComma: true)  // 서버에서 받은 '아이뒤'
        addKeyValue("instructor", value: instructorID, withFinishComma: true)

        //addKeyValue("timestamp", value: curDate.formatYYMMDDspaceTime)
        addKeyValue("timestamp", value: studntObj.getOperateTime()!.formatYYMMDDspaceTime)
        addKeyValue("standard", value: studntObj.getStandardIdx() + 1)
        addKeyValue("class_idx", value: 0)
        //addKeyValue("recordid", value: dObj!.dbRecordID)

        let adultObj = studntObj.getAdultFromDB()

        //addKeyArray("score", value: dObj!.scoreJson()) /// 점수..

        if studntObj.finishedAdult && 0 < studntObj.myDataFromDB?.arrSet.count { //  dObj?.arrSet.count {
            addKeyArray("comp_count", value: dObj!.ccCountJson(), withFinishComma: true)
            addKeyArray("comp_depth", value: dObj!.compDepthJson(), withFinishComma: true)
            addKeyArray("comp_recoil", value: dObj!.compRecoilJson(), withFinishComma: true)

            addKeyArray("percent", value: dObj!.rightPositionPercent(), withFinishComma: true)

            //addKeyValue("overall", value: dObj!.discreminant(), withFinishComma: true)
            addKeyValue("overall", value: dObj!.scoreTotalInt, withFinishComma: true)

            addKeyArray("comp_cycle", value: dObj!.compStrokeJson(), withFinishComma: true)
            addKeyArray("hands_off_time", value: dObj!.handsOffTime(), withFinishComma: true)
            addKeyArray("resp_correct", value: dObj!.rpCorrectJson(), withFinishComma: true)
            addKeyArray("comp_correct", value: dObj!.ccCorrectJson(), withFinishComma: true)
            if dObj!.discreminantPass { pasStr = 1 }
            addKeyValue("pass", value: pasStr, withFinishComma: true)
            addKeyArray("resp_vol", value: dObj!.breathVolJson(), withFinishComma: true)

            addKeyArray("resp_cycle", value: dObj!.breathCycleJson())

            addKeyArray("resp_count", value: dObj!.rpCountJson(), withFinishComma: true)
            jStr += dObj!.positionJson() + ", "
        } else {
            log.printThisFNC("sendDataRequest", comment: " Adult 측정 자료 없슴.....")
        }

        /// 성인 측정 결과
        if studntObj.finishedAdult {
            addKeyArray("score", value: dObj!.scoreJson()) /// 점수..
            jStr += studntObj.operationJsonString()

            let opString = " \"comp_resp_data\" : { "  + studntObj.operationJsonStringTest() + " } "
            let adltObj = " { \(opString) , \(adultObj!.adultJson())  } "
            print("\n\n \(adltObj) \n\n")

            addKeyString("adult", value: adltObj) // Test JSON
//            addKeyValue("cpr_pass", value: adultObj!.bool2int(adultObj!.passRes1))
//            addKeyValue("aed_pass", value: adultObj!.bool2int(adultObj!.passRes2))
        } else {
            addKeyValue("adult", value: "null")  // addKeyString("adult", value: "null")
        }
        /// 영아 측정 결과
        let infntObj = studntObj.getInfantFromDB()
        if studntObj.finishedInfant {
            addKeyString("infant", value: makeCaretItem(infntObj!.infantJson()), withFinishComma: false) //
        } else {
            addKeyValue("infant", value: "null", withFinishComma: false)
        }

        finishEncoding()
        
        jStr = "test=\(jStr)"
        
        print(jStr)
    }

}
