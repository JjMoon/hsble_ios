//
//  ResultDetail.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2015. 11. 11..
//  Copyright © 2015년 IMLab. All rights reserved.
//

import Foundation
import UIKit


class ResultGraphVC : HtViewController {

    var curData = HsData()
    var bleMan: HsBleSuMan?
    var dbStudent: HmStudent?

    var prevData: HsData? {
        get {
            if bleMan == nil { return dbStudent?.myDataFromDB }
            else { return bleMan!.dataObj }
        }
    }


    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelName: UILabel!

    @IBOutlet weak var labelCompScore: UILabel!
    @IBOutlet weak var labelBrthScore: UILabel!
    @IBOutlet weak var labelCompRatio: UILabel!
    @IBOutlet weak var imgCompIcon: UIImageView!
    @IBOutlet weak var imgBrthIcon: UIImageView!

    @IBOutlet weak var labelTotalScore: UILabel!
    @IBOutlet weak var labelTotalScoreTitle: UILabel!


    ///  A B C D   버튼 관련..
    @IBOutlet var bttnABCD: [UIButton]!

    @IBOutlet weak var bttnA: UIButton!
    @IBOutlet weak var bttnB: UIButton!
    @IBOutlet weak var bttnC: UIButton!
    @IBOutlet weak var bttnD: UIButton!

    @IBAction func btnActSelectAtoD(sender: UIButton) {
        for btn in bttnABCD {
            btn.setTitleColor(colBttnDarkGray, forState: .Normal)
            btn.setBorder(0, rad: 1, borderCol: colBttnGreen, bgCol: nil)
        }
        sender.setTitleColor(colBttnGreen, forState: .Normal)
        sender.setBorder(1, rad: 1, borderCol: colBttnGreen, bgCol: nil)

        bleMan = HsBleMaestr.inst.getConnectedBLEwithhNick(sender.titleLabel!.text!)

        // 테스트에서 사용..  여기서 세팅하고.. 나가면 ResultVC 에서 다시 세팅..
        HsBleMaestr.inst.tempBle = bleMan!

        curData = bleMan!.dataObj
        setLanguageString()

        tableContent.reloadData()
    }

    func setInitABCDBttns() {
        let nickList = [ "A", "B", "C", "D" ]
        let btnArr = [ bttnA, bttnB, bttnC, bttnD ]
        for (ix, nk) in nickList.enumerate() {
            if HsBleMaestr.inst.getConnectedBLEwithhNick(nk) == nil {
                btnArr[ix].hideMe()
            }
            if nk == bleMan!.nick { // 내 버튼은 선택된 걸로 놔야지...
                btnActSelectAtoD(btnArr[ix])
            }
        }
    }

    func setCurObject(ble: HsBleSuMan) {

    }


    @IBOutlet weak var tableContent: UITableView!


    func setNewData(isNext: Bool) {

    }

    @IBAction func btnActGo2NextPage(sender: AnyObject) {
        let tS = HsBleMaestr.inst.getNeighborStudent(true, curStu: dbStudent!)
        { (std) -> Bool in std.isDataInDB }

        print("  data > \(tS?.name)")
    }

    @IBAction func btnActGo2PrevPage(sender: AnyObject) {
        let tS = HsBleMaestr.inst.getNeighborStudent(false, curStu: dbStudent!)
        { (std) -> Bool in std.isDataInDB }
        print("  data > \(tS?.name)")
    }

    @IBAction func btnActGoBack(sender: AnyObject) {
        //showWantBackMessage { () -> Void in }
        self.dismissViewControllerAnimated(viewAnimate, completion: nil)
    }

    //////////////////////////////////////////////////////////////////////     [ UI viewWillAppear  << >> ]

    override func viewDidLoad() {
//        switch cprProtoN.theVal { // 이걸 원상 복구 하는 건 Select View 에서..
//        case 0:
//            CC _STRONG_POINT = 101
//        default:
//            CC _STRONG_POINT = Int32(CC_STRONG_POINT_DEFAULT)
//        }
    }

    override func viewWillAppear(animated: Bool) {
        // V.1.08.160718
        if bleMan == nil {
            for vw in bttnABCD { vw.hideMe() }
            dbStudent?.myDataFromDB?.세트점수() // debugging 용.
            curData = dbStudent!.myDataFromDB!

            HsStudentDataJobs.singltn.setJsonString(dbStudent!)  // 디버깅 용
        } else {
            setInitABCDBttns()
            bleMan?.dataObj.세트점수()
            curData = bleMan!.dataObj
        }
        //labelCompRatioValue.textColor = UIColor(hex: "5D5D5D")
        setLanguageString()
    }

    override func viewDidAppear(animated: Bool) {
        if !testInfant {
            super.viewDidAppear(animated)  // 1.1 sec timer set..
            HsBleMaestr.inst.tempBle = bleMan  //  nil 일 수도 있슴.
        }
    }

    override func update() {
        if bleMan == nil { return } // 학생 관리 화면에서 볼 경우.
        let btnArr = [ bttnA, bttnB, bttnC, bttnD ]

        for (ix, ble) in HsBleMaestr.inst.arrBleSuMan.enumerate() {
            if ble.isPortClosed { // !ble.isConnectionTotallyFinished {
                btnArr[ix].hideMe()
                if ble.nick == bleMan!.nick {
                    self.dismissViewControllerAnimated(viewAnimate, completion: nil)
                }
            }
        }
        let connNum = HsBleMaestr.inst.arrBleSuMan.filter { (ble) -> Bool in
            return !ble.isPortClosed // isConnectionTotallyFinished
        }

        if connNum.count == 0 {
            self.dismissViewControllerAnimated(viewAnimate, completion: nil)
        }
        
    }

    // MARK:  언어 세팅.
    func setLanguageString() {
        /// 압박, 호흡 점수..
        labelCompScore.text = "\(curData.scoreCompInt) %"
        labelCompScore.textColor = curData.scoreCompColor
        imgCompIcon.image = UIImage(named: curData.scoreCompImgname)

        labelBrthScore.text = "\(curData.scoreBrthInt) %"
        labelBrthScore.textColor = curData.scoreBrthColor
        imgBrthIcon.image = UIImage(named: curData.scoreBrthImgname)


        

        labelTotalScore.text = "\(curData.scoreTotalInt)"
        labelTotalScore.textColor = curData.totalStringColor

        labelCompRatio.text = langStr.obj.compressionRatio + " \(curData.scoreFraction) %"
        labelCompRatio.textColor = curData.scoreFractionColor

        if bleMan == nil {
            labelName.text = dbStudent?.name
        } else {
            if bleMan?.curStudent == nil {
                labelName.text = ""
            } else { labelName.text = bleMan?.curStudent?.name }
        }

        labelTitle.text = langStr.obj.detail
        labelTotalScoreTitle.text = langStr.obj.total_score
    }
}


extension ResultGraphVC : UITableViewDelegate, UITableViewDataSource {

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        //print(" ResultGraphVC tableview  :: \(HsBleSingle.inst().stageLimit) ")

        if bleMan == nil {
            return (dbStudent?.myDataFromDB?.arrSet.count)!
        } else {
            if (bleMan?.curStudent == nil) {
                return (bleMan?.dataObj.arrSet.count)!
            } else {
                return (bleMan?.curStudent?.myData?.arrSet.count)!
            }
        }  // return Int(HsBleSingle.inst().stageLimit)
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        //print ("ResultGraphVC >> cellForRowAtIndexPath   \(indexPath)")
        var cell:ResultGraphUnit? = tableView.dequeueReusableCellWithIdentifier("graphCell") as? ResultGraphUnit
        if cell == nil {
            addTableCellNib()
            cell = tableView.dequeueReusableCellWithIdentifier("graphCell") as? ResultGraphUnit
        }
        if bleMan == nil { cell?.dbStudent = dbStudent!  }
        else { cell?.bleMan = bleMan! }
        cell?.setCycleNumber(indexPath.row)
        //cell?.cellDidLoad()        cell?.cellDidAppear()
        //cell?.setItems(indexPath.row + 1, dObj: (bleMan?.dataObj)!)
        return cell!
    }
    func addTableCellNib() {
        let nib = UINib(nibName: "ResultGraphUnit", bundle: nil)
        tableContent.registerNib(nib, forCellReuseIdentifier: "graphCell")
    }
}



