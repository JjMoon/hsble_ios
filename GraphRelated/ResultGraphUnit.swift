//
//  ResultDetail.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2015. 11. 11..
//  Copyright © 2015년 IMLab. All rights reserved.
//

import Foundation
import UIKit


class ResultGraphUnit : UITableViewCell {
    var log = HtLog(cName: "ResultGraphUnit")

    @IBOutlet weak var labelCycleNum: UILabel!
    @IBOutlet weak var vwDetail: CompStrokeView! // HtVerticalGraph
    @IBOutlet weak var vwDetailL: ResultSpeedView!
    @IBOutlet weak var vwDetailBreath: ResultBreathView!
    @IBOutlet weak var vwBreathSpeed: BreathSpeedView!
    @IBOutlet weak var vwAccuracy: ResultAccurateView!


    @IBOutlet weak var labelCompDepth: UILabel!
    @IBOutlet weak var labelCompSpeed: UILabel!
    @IBOutlet weak var labelCompTimeAndSpeed: UILabel!
    @IBOutlet weak var labelBreathAmount: UILabel!
    @IBOutlet weak var labelBreathTime: UILabel!
    @IBOutlet weak var labelCompPosition: UILabel!

    var curSet = HmUnitSet()
    var bleMan: HsBleSuMan?
    var dbStudent: HmStudent?
    
    func setCycleNumber(n: Int) { // 0, 1, 2 ..
        //log.printThisFunc("setCycleNumber (\(n))", lnum: 2)

        if bleMan != nil {
            curSet = (bleMan?.dataObj.arrSet[n])!  // n - 1 아님..
        } else {
            //print("  (dbStudent?.myDataFromDB?.arrSet.count : \(dbStudent?.myDataFromDB?.arrSet.count) ")
            curSet = (dbStudent?.myDataFromDB?.arrSet[n])!
        }

        curSet.showAllBreathTime() // debugging log .... 호흡 시간 보기.

        labelCycleNum.text = "Set \(curSet.setNum)"
        //log.logThis(labelCycleNum.text!, lnum: 1)
        
        labelCompTimeAndSpeed.text = "Due Time : \( formatDouble2f(curSet.ccTime)) / Mean Speed : \(curSet.분당압박횟수()) cycle/min"

        // 호흡 그래프들...
        vwDetailBreath.curSet = curSet
        vwDetailBreath.setNeedsDisplay()
        vwBreathSpeed.curSet = self.curSet
        vwBreathSpeed.setNeedsDisplay()
        vwDetail.strokeObj = curSet
        vwDetail.setNeedsDisplay()
        vwDetailL.strokeObj = curSet
        vwDetailL.setNeedsDisplay()
        vwAccuracy.setObj = curSet
        vwAccuracy.setNeedsDisplay()

        setLanguageString(n)
        log.함수차원_출력_End()
    }

    // MARK:  언어 세팅.
    func setLanguageString(n: Int) {
        switch langIdx {
        case 1:
            labelCycleNum.text = " \(n+1) \(langStr.obj.cycle)"
        default:
            labelCycleNum.text = " \(langStr.obj.cycle) \(n+1)"
        }

        labelCompDepth.text = langStr.obj.compression_depth
        labelCompSpeed.text = langStr.obj.compression_rate
        // 소요시간 : 333 초, 평균 속도:  222/분
        labelCompTimeAndSpeed.text = curSet.getLabelCompTimeAndSpeed()
        labelBreathAmount.text = langStr.obj.breath_volume + " & " + langStr.obj.rate
        labelBreathTime.text = "\(langStr.obj.hands_off_time) : \(curSet.holdTimeStr) \(langStr.obj.second) "
        labelCompPosition.text = langStr.obj.compression_position
    }
}

