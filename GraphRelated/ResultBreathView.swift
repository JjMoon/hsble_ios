//
//  GraphViews.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2015. 12. 2..
//  Copyright © 2015년 IMLab. All rights reserved.
//

import Foundation

//////////////////////////////////////////////////////////////////////     [ << ResultBreathView : 호흡 뷰 >> ]

/// 기존 바 그래프
class ResultBreathView : UIView {
    var graph = GraphSub3Dot() // 바 그래프
    var curSet = HmUnitSet()
    
    override func awakeFromNib() {
    }

    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        //print("\n\n\n ResultSpeedView : awakeFromNib")
        let upRect = CGRect(x: 50, y: 0, width: frame.width - 50, height: frame.height)
        graph = GraphSub3Dot(pArea: upRect, xNum: 30, yNum: 3, dotDia: 10,
                             midColor: HmGraphSetting.inst.graphAreaTransparentGreen,
                             dotX1: frame.width * 0.2, dotX2: frame.width * 0.9)
        //graph.startDraw(UIGraphicsGetCurrentContext()!)
        graph.ctx = UIGraphicsGetCurrentContext()!
        graph.drawBreathElements(curSet)
    }
}

/// 스피드 도트 그래프
class BreathSpeedView : UIView {
    var graph = GraphSub3Dot() // 바 그래프
    var curSet = HmUnitSet()

    override func awakeFromNib() {
    }

    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        //print("\n\n\n BreathSpeedView : drawRect")
        let upRect = CGRect(x: 50, y: 0, width: frame.width - 50, height: frame.height)
        //let loRect = CGRect(x: 50, y: frame.height * 0.5,   width: frame.width - 50, height: frame.height * 0.5)
        graph = GraphSub3Dot(pArea: upRect, xNum: 30, yNum: 3, dotDia: 10,
                             midColor: HmGraphSetting.inst.graphAreaTransparentGreen,
                             dotX1: frame.width * 0.2, dotX2: frame.width * 0.9)
        //graph.startDraw(UIGraphicsGetCurrentContext()!)
        graph.ctx = UIGraphicsGetCurrentContext()!
        graph.drawBreathSpeedDot(curSet)
    }
}

