//
//  HmGraphicBase.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2015. 11. 13..
//  Copyright © 2015년 IMLab. All rights reserved.
//

import Foundation

//////////////////////////////////////////////////////////////////////     _//////////_     [     ]    _//////////_   Go to Main View
// MARK:  Graph

class GraphSub3Dot: HtGraph {
    var log = HtLog(cName: "GraphSub3Dot")
    var dotDia:CGFloat = 0, midCol:UIColor =  colGraphGreenArea // UIColor.greenColor()
    var pitch:CGFloat = 1, dotAreaX1:CGFloat = 0, dotAreaX2:CGFloat = 0
    var staX:CGFloat = 0, distY:CGFloat = 0, fastY:CGFloat = 0, normY:CGFloat = 0, slowY:CGFloat = 0
    var font: UIFont? = UIFont(name: "Helvetica Neue", size: HmGraphSetting.inst.fontSizeSmallTitle)
    let paraStyle = NSMutableParagraphStyle()

    var dashLineY = [CGFloat](), numberY: CGFloat = 0
    let w: CGFloat = 80,  h: CGFloat = 20, textY: CGFloat = 20, skew = 0.1 // fast / slow / weak 글자 크기.
    let dotY: CGFloat = 20
    let dash32: [CGFloat] = [3, 2]

    override init() {  super.init()  }

    convenience init(pArea: CGRect, xNum: Int, yNum: Int,  dotDia: CGFloat, midColor: UIColor, dotX1:CGFloat, dotX2:CGFloat) {
        self.init()
        area = pArea
        numX = xNum; numY = yNum
        self.dotDia = dotDia
        midCol = colGraphGreenArea
        dotAreaX1 = dotX1; dotAreaX2 = dotX2
        log.함수차원_출력_End()
    }

    ///  호흡 Speed Dot  그래프 그리기..
    func drawBreathSpeedDot(obj: HmUnitSet) {
        unitSet = obj
        setBreathDimensions()
        //log.logThis(" (area?.width)!  \((area?.width)!)  dotAreaX1 : \(dotAreaX1)  ==> pitch   \(pitch)   ", lnum: 2)
        drawArea(0.30, loVal: 0.65)
        drawDashLines(0.30, loVal: 0.65)
        drawBreathSpeedTexts()  //drawBreathTexts()
        drawBreathDots()
        log.함수차원_출력_End()
    }


    ///  호흡 그래프 그리기..
    func drawBreathElements(obj: HmUnitSet) {
        unitSet = obj
        setBreathDimensions()
        //log.logThis(" (area?.width)!  \((area?.width)!)  dotAreaX1 : \(dotAreaX1)  ==> pitch   \(pitch)   ", lnum: 2)
        drawArea(0.30, loVal: 0.65)
        drawDashLines(0.30, loVal: 0.65)
        drawBreathTexts()
        drawBreathBars()
        log.함수차원_출력_End()
    }


    private func setBreathDimensions() {
        distY = (area?.height)! * 0.25
        /// fast / good / slow 수직 위치..
        fastY = distY * 0.5
        normY = fastY + distY
        slowY = normY + distY
        numberY = slowY + distY * 0.6 // count number Y
        let wid = dotAreaX2 - dotAreaX1
        staX = dotAreaX1 + wid * 0.6  // 250 //  175
        pitch = wid * 0.35
        //pitch = ( (area?.width)! - dotAreaX1) / 3 + 30 // 350 - 70 / 3 ...  100 인데... 10은 보정 수..
    }

    /// 호흡 속도 점 표시...
    private func drawBreathDots() {
        //log.printThisFunc("drawSpeedDots")
        log.logThis(" unitSet.arrComprs count >> :  \(unitSet.arrComprs.count)  ")

        var xStand = staX, number12 = 1
        for (idx, sObj) in unitSet.arrBreath.enumerate() {
            if idx == 2 { break }   //print("  Circle at \(staX),  \(normY)")

            var eaPerMin = 44.1 // 초기값.. 빠르게..
            if 0.1 < sObj.strkTime { eaPerMin = 60 / sObj.strkTime }

            if 4 < eaPerMin && eaPerMin < 12 {
                drawCircle(xStand, corY: dashLineY[1] - dotY, dia: dotDia, col: colBttnGreen)
            } else if sObj.strkTime <= 4 { // Fast
                drawCircle(xStand, corY: dashLineY[0] - dotY, dia: dotDia, col: colBttnRed)
            } else {
                drawCircle(xStand, corY: dashLineY[2] - dotY, dia: dotDia, col: colBttnRed)
            }

            let numStr = String(number12)
            let attributes = [
                NSForegroundColorAttributeName: HmGraphSetting.inst.graphText, NSParagraphStyleAttributeName: paraStyle,
                NSObliquenessAttributeName: 0,      NSFontAttributeName: font!  ]
            numStr.drawInRect(CGRectMake(xStand, numberY, 100.0, 30.0), withAttributes: attributes)
            number12 += 1

            xStand += pitch
        }
        log.함수차원_출력_End()
    }

    /// 점선 사이 영역 칠하기..
    func drawArea(upVal: CGFloat, loVal: CGFloat) { // 0.75 를 100으로 해서 계산해야 함..
        let yUp = (area?.height)! * 0.75 * upVal, yLo = (area?.height)! * 0.75 * loVal
        let x1 = (area?.origin.x)!, x2 = x1 + (area?.width)!
        drawBox(x1, x2: x2, y1: yUp, y2: yLo, col: midCol)
    }

    /// 대쉬 라인 ..  3개 그리기
    func drawDashLines(upVal: CGFloat, loVal: CGFloat) { // 0.75 를 100으로 해서 계산해야 함..
        let curCol = colBttnGreen
        let x1:CGFloat = (area?.origin.x)!, x2:CGFloat = x1 + (area?.width)!

        let yUp:CGFloat = (area?.height)! * 0.75 * upVal
        let yLo:CGFloat = (area?.height)! * 0.75 * loVal
        let yBlack:CGFloat = (area?.height)! * 0.75
        dashLineY = [ yUp, yLo, yBlack ]

        drawDashLine(x1, yUp, x2, yUp, color: curCol, wid: 0.5, dash: dash32)
        drawDashLine(x1, yLo, x2, yLo, color: curCol, wid: 0.5, dash: dash32)
        drawDashLine(x1, yBlack, x2, yBlack, color: colBttnDarkGray, wid: 0.5, dash: [3, 1])
    }

    ///  호흡 바 그리기.
    func drawBreathBars() {
        //log.printThisFunc("drawBreathBars 호흡 바 그리기 ", lnum: 3)
        let barWidthH:CGFloat = 30, yBlack:CGFloat = (area?.height)! * 0.75
        //var coX:CGFloat = (area?.origin.x)! + dotAreaX1 + pitch

        var coX:CGFloat =  staX // dotAreaX1 + pitch

        log.logThis("  호흡 수 : \(unitSet.arrBreath.count)", lnum: 2)
        //log.logThis("  그래프 위치 값 ...  \(area?.origin.x) +   \(dotAreaX1) + \(pitch)  ")
        var number12 = 1

        for (ixMax2, sObj) in unitSet.arrBreath.enumerate() {
            log.logThis("  호흡 strkTime : \(sObj.strkTime)  ", lnum: 1)

            var curCol = HmGraphSetting.inst.graphYello
            //if sObj.amount == .Good {
            if 35 < sObj.max && sObj.max < 70 {  // 바 색깔 조정.
                curCol = colBttnGreen
            } else if 70 <= sObj.max {
                curCol = colBttnRed
            }
            let yUp:CGFloat = (100.0 - CGFloat(sObj.max)) * (area?.height)! * 0.75 / 100
            let x1 = coX - barWidthH, x2 = coX + barWidthH

            drawBox(x1, x2: x2, y1: yUp, y2: yBlack, col: curCol)

            let numStr = String(number12)
            let attributes = [
                NSForegroundColorAttributeName: HmGraphSetting.inst.graphText, NSParagraphStyleAttributeName: paraStyle,
                NSObliquenessAttributeName: 0,      NSFontAttributeName: font!  ]
            numStr.drawInRect(CGRectMake(coX, numberY, 100.0, 30.0), withAttributes: attributes)
            number12 += 1
            coX += pitch

            if ixMax2 == 1 { break }
        }
        log.함수차원_출력_End()
    }

    /// 호흡 스피드 글자...  Strong / Good / Weak..
    func drawBreathSpeedTexts() {
        //fast = langStr.obj.fast, norm = langStr.obj.good_rate + " 100~120/min", slow = langStr.obj.slow
        let positionX:CGFloat = 50, over = langStr.obj.fast, good = langStr.obj.good_rate + " 4~12/min",
        short = langStr.obj.slow  // , diffY = fastY * -0.2
        //let coX1 = (area?.origin.x)! + dotAreaX1 + pitch - 30, coX2 = coX1 + pitch
        //let coX1 = (area?.origin.x)! + dotAreaX1 + pitch  // , coX2 = coX1 + pitch

        //paraStyle.lineSpacing = 6.0
        // set the Obliqueness to 0.1
        var attributes: NSDictionary = [
            NSForegroundColorAttributeName: colBttnRed, NSParagraphStyleAttributeName: paraStyle,
            NSObliquenessAttributeName: skew,      NSFontAttributeName: font!  ]

        over.drawInRect(CGRectMake(positionX, dashLineY[0] - textY, w, h), withAttributes: attributes as? [String : AnyObject])
        short.drawInRect(CGRectMake(positionX, dashLineY[2] - textY, w, h), withAttributes: attributes as? [String : AnyObject])
        attributes = [
            NSForegroundColorAttributeName: colBttnGreen , NSParagraphStyleAttributeName: paraStyle,
            NSObliquenessAttributeName: skew,      NSFontAttributeName: font!  ]
        good.drawInRect(CGRectMake(positionX, dashLineY[1] - textY, w + 150, h), withAttributes: attributes as? [String : AnyObject])
    }


    /// 호흡 Strong / Good / Weak..
    func drawBreathTexts() {
        let positionX:CGFloat = 50, over = langStr.obj.excessive, good = langStr.obj.good_amount + " 400~700cc",
        short = langStr.obj.insufficient  // , diffY = fastY * -0.2
        //let coX1 = (area?.origin.x)! + dotAreaX1 + pitch - 30, coX2 = coX1 + pitch
        //let coX1 = (area?.origin.x)! + dotAreaX1 + pitch  // , coX2 = coX1 + pitch
        
        //paraStyle.lineSpacing = 6.0
        // set the Obliqueness to 0.1
        var attributes: NSDictionary = [
            NSForegroundColorAttributeName: colBttnRed, NSParagraphStyleAttributeName: paraStyle,
            NSObliquenessAttributeName: skew,      NSFontAttributeName: font!  ]

        over.drawInRect(CGRectMake(positionX, dashLineY[0] - textY, w, h), withAttributes: attributes as? [String : AnyObject])
        short.drawInRect(CGRectMake(positionX, dashLineY[2] - textY, w, h), withAttributes: attributes as? [String : AnyObject])
        attributes = [
            NSForegroundColorAttributeName: colBttnGreen , NSParagraphStyleAttributeName: paraStyle,
            NSObliquenessAttributeName: skew,      NSFontAttributeName: font!  ]
        good.drawInRect(CGRectMake(positionX, dashLineY[1] - textY, w + 150, h), withAttributes: attributes as? [String : AnyObject])
    }
}


//  압박 관련...
extension GraphSub3Dot {


    /// 압박 속도 점 찍는 그래프
    func drawSpeedElements(obj: HmUnitSet) {
        unitSet = obj
        distY = (area?.height)! * 0.25
        staX = dotAreaX1 + pitch * 0.5;    fastY = distY * 0.5; normY = fastY + distY; slowY = normY + distY
        pitch = (dotAreaX2 - dotAreaX1) / CGFloat(numX)
        drawArea(0.30, loVal: 0.70)
        drawSpeedDots()
        drawDashLines(0.30, loVal: 0.70)
        drawSpeedTexts()
    }


    /// fast, good, slow  글자 쓰기..  압박만...
    func drawSpeedTexts() {
        let positionX:CGFloat = 50, fast = langStr.obj.fast, norm = langStr.obj.good_rate + " 100~120/min", slow = langStr.obj.slow
        //, diffY = fastY * 0.8
        var attributes: NSDictionary = [
            NSForegroundColorAttributeName: colBttnRed, NSParagraphStyleAttributeName: paraStyle,
            NSObliquenessAttributeName: skew,      NSFontAttributeName: font!  ]

        fast.drawInRect(CGRectMake(positionX, dashLineY[0] - textY, w, h), withAttributes: attributes as? [String : AnyObject])
        slow.drawInRect(CGRectMake(positionX, dashLineY[2] - textY, w, h), withAttributes: attributes as? [String : AnyObject])
        attributes = [
            NSForegroundColorAttributeName: colBttnGreen , NSParagraphStyleAttributeName: paraStyle,
            NSObliquenessAttributeName: skew,      NSFontAttributeName: font!  ]
        norm.drawInRect(CGRectMake(positionX, dashLineY[1] - textY, w + 100, h), withAttributes: attributes as? [String : AnyObject])
    }


    /// 압박 속도 점 표시...
    private func drawSpeedDots() {
        //log.printThisFunc("drawSpeedDots")
        log.logThis(" unitSet.arrComprs count >> :  \(unitSet.arrComprs.count)  ")

        staX = 269
        var xStand = staX

        for (idx, sObj) in unitSet.arrComprs.enumerate() {
            if idx == 30 { break }   //print("  Circle at \(staX),  \(normY)")

            var coY = normY//, dotCol = HmGraphSetting.inst.graphYello
            if sObj.strkTime <= 0.5 { coY = fastY } // Fast
            if 0.6 <= sObj.strkTime { coY = slowY
                //dotCol = HmGraphSetting.inst.graphYello
            } // Slow
            if 0 < sObj.wPosiIdx {
                drawX(xStand, arrowY: coY - 6, size: 12, color: UIColor.grayColor())
                xStand += pitch
                continue
            }

            //print("  sObj.period  :  \(idx)  \(sObj.strkTime)")  // 0.5 <  < 0.6

            if 0.5 < sObj.strkTime && sObj.strkTime < 0.6 {
                drawCircle(xStand, corY: normY, dia: dotDia, col: colBttnGreen)
            } else if sObj.strkTime <= 0.5 { // Fast
                drawCircle(xStand, corY: fastY, dia: dotDia, col: colBttnRed) // HmGraphSetting.inst.graphYello)
            } else {
                drawCircle(xStand, corY: slowY, dia: dotDia, col: colBttnRed) //HmGraphSetting.inst.graphYello)
            }
            xStand += pitch
        }

        // 카운터 쓰기...        print("  staX  \(staX),  ")
        for idx in 0...29 {
            if (idx % 5) == 4 {
                let str = "\(idx+1)"
                var coX = staX - dotDia * 0.5
                if idx > 6 { coX -= dotDia * 0.5 } // 10 같은 두자릿 수 처리
                let attributes: NSDictionary = [
                    NSForegroundColorAttributeName: HmGraphSetting.inst.graphText, NSParagraphStyleAttributeName: paraStyle,
                    NSObliquenessAttributeName: 0,      NSFontAttributeName: font!  ]

                if idx > 9 { coX = coX - dotDia * 0.5 }
                str.drawInRect(CGRectMake(coX, slowY + distY * 0.8, 30.0, 30.0), withAttributes: attributes as? [String : AnyObject])

                //print("  pitch is ...  \(pitch),    coX : \(coX)   Y 위치 : \(slowY + distY * 0.8)")
            }
            staX += pitch
        }
        log.함수차원_출력_End()
    }


}
