//
//  HtLog.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2015. 11. 19..
//  Copyright © 2015년 IMLab. All rights reserved.
//

import Foundation


class HtLog : NSObject {
    static let uiMarker = "===================================================================================================================  UI ====="
    var willPrn = true, willPrnFun = true, clsName : String = "NSObject", funName : String = "Void", cnt = 0
    
    init (cName: String) {  self.clsName = cName  }
    
    func doNotPrintThisClass() { willPrn = false }
    
    func getFunName() -> String { return " \(clsName) :: \(funName) >> " }
    
    func newLine(lnum:Int) {
        if lnum == 0 { return }
        for _ in 0...lnum-1 { print("...") }
    }
    
    func logBase(lnum:Int = 0) {
        if willPrn && willPrnFun { newLine(lnum); print("\t\t   \(getFunName()) 🀟 \(HsUtil.curStepStr())  << "); newLine(lnum) }
    }
    
    func printThisFunc(funcName:String = "  func  ", lnum:Int = 2) {
        willPrn = true;         willPrnFun = true;  funName = funcName;   logBase(lnum)
    }
    
    func printAlways(stt: String = "  {{{{{  This is important  }}}}}   ", lnum:Int = 5) {
        newLine(lnum);  print( " >>>  🀟  \(HsUtil.curStepStr()) __  " + getFunName() + " <<< " + stt )
        newLine(lnum)
    }
    
    func logThis(cont:String = "default", lnum:Int = 1) {
        var pStr = cont
        if cont == "default" { pStr = " Simple Loging  <<<  Count ::  \(cnt++)   >>> " }
        if !willPrn || !willPrnFun { return }
        newLine(lnum)
        print(getFunName() + " 🀟 \(HsUtil.curStepStr())  <<   COMMENT :: " + pStr )
        newLine(lnum)
    }
    
    func logUiAction(cont:String = "<<< UI Action >>>", lnum:Int = 3, printOn: Bool = false) {
        newLine(lnum);  willPrn = printOn
        print(HtLog.uiMarker); print(HtLog.uiMarker); print(HtLog.uiMarker)
        print("\n +   <<< \(clsName) >>> +  \t  🀟  \(HsUtil.curStepStr())  <<  \t\t UI ACTION : " + cont + "\n" )
        print(HtLog.uiMarker); print(HtLog.uiMarker); print(HtLog.uiMarker)
        newLine(lnum)
    }

    func endFunctionLog() { 함수차원_출력_End() }

    func 함수차원_출력_End() {
        funName = "func"
        willPrnFun = false
    }
}

