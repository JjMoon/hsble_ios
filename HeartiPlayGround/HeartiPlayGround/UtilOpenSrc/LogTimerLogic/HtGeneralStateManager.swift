//
//  HtGeneralStateManager.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2015. 12. 14..
//  Copyright © 2015년 IMLab. All rights reserved.
//

import Foundation



//////////////////////////////////////////////////////////////////////     _//////////_     [   State Manager ..  << >> ]    _//////////_
// MARK: -  상태 관리 매니저..

class HtUnitState : NSObject {
    var name = "UnitState", entryActionDone = false
    var dueTime:Double = -1
    var entryAction: (Void)-> Void = { }
    var durinAction: (Void)-> Void = { }
    var exittAction: (Void)-> Void = { }
    var exitCondition: (Void)-> Bool = { return false }
    
    override init() {
        super.init()
    }
    
    convenience init(duTime: Double, myName: String) {
        self.init()
        dueTime = duTime
        name = myName
    }
}

class HtGeneralStateManager : HtStopWatch {
    var log2 = HtLog(cName: "HtGeneralStateManager")
    var arrState = [HtUnitState]()
    var curState: HtUnitState? = nil
    
    func prepareActions() {
        curState = arrState[0]
    }
    
    private func startEntryAction() {
        print("\n\n\n  ===============================  ________________________________________ ________ ________________________    Entry Action of [ \(curState?.name)]  =====    \n\n\n")
        curState?.entryAction()
        curState?.entryActionDone = true
        resetTime()
    }
    
    func update() {
        
        if curState == nil { return }
        
        if curState?.entryActionDone == false { startEntryAction(); return } // entry action 수행.. 한 후 간격 띄었다가..  during action..
        
        curState?.durinAction()
        
        setDueTime()
        
        let exitAndGotoNext  = {
            print("\n\n\n  ===============================  ________________________________________ Exit Action of [ \(self.curState?.name)]  =====  \n\n\n")
            self.curState?.exittAction()
            print("\n\n\n  ===============================  ________________________________________ go to next State   =====   ----------- >>>>>>>>>  \n\n\n")
            self.goToNextState()
        }
        
        if curState?.dueTime > 0 && curState?.dueTime < dueTime {
            exitAndGotoNext()
        }
        if curState?.exitCondition() == true {
            exitAndGotoNext()
        }
    }
    
    func goToNextState() {
        let idx = arrState.indexOf(curState!)
        if (idx! + 1) < arrState.count {
            curState = arrState[idx! + 1]
            startEntryAction()
        } else {
            curState = nil
        }
    }
    
    func addAState(aState : HtUnitState) {
        arrState.append(aState)
    }
    
    func setState(newSttName: String) {
        let sameNameObj = arrState.filter { (objt) -> Bool in
            return objt.name == newSttName
        }
        if sameNameObj.count == 1 {
            curState = sameNameObj[0]
        } else {
            log2.printThisFunc("setState")
            log2.logThis(" 상태가 없거나 중복... 이름 확인.. 필요  \(newSttName) ")
        }
    }
}


