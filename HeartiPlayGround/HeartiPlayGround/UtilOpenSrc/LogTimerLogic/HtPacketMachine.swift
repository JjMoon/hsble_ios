//
//  HtPacketMachine.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2015. 12. 9..
//  Copyright © 2015년 IMLab. All rights reserved.
//

import Foundation


class HtPacketMachine : NSObject {
    
    var log = HtLog(cName: "HtPacketMachine")
    
    var arrPckt = [HtBaseJob]()
    
    var timer = NSTimer()
    
    //

    override init() {
        super.init()
        
    }
    
    convenience init(interval: Double) {
        self.init()
        timer = NSTimer.scheduledTimerWithTimeInterval(interval, target: self, selector: "timerAction", userInfo: nil, repeats: true)
    }
    
    func addBaseJob(aJob: HtBaseJob) {
        arrPckt.append(aJob)
    }
    
    
    func timerAction() {
        
        if (arrPckt.count == 0) { return }
        
        //log.printThisFunc("  timerAction( cnt : \(arrPckt.count) <\(arrPckt[0].jobName) > ) ", lnum: 2)
            
        
        let curJob = arrPckt[0]
        
        if (curJob.retryCount == 0) {
            log.logThis(" arrPckt :: >>  initJob  of   \(curJob.jobName)   ")
            curJob.initJob();
            curJob.retryCount = 1;
            return;
        } else {
            if (curJob.didFinished()) { // OK..  done..
                //curJob = nil;
                log.logThis("arrPckt   removefirst   \(curJob.jobName) ")
                arrPckt.removeFirst()// [arrPckt removeObjectAtIndex:1];
                return;
            }
        }
        
        if (curJob.retryLimit <= curJob.retryCount) {
            log.logUiAction("  retryLimit <= curJob.retryCount    \(curJob.jobName)   실패 .... ", lnum:10)
            arrPckt.removeFirst()
        } else {
            curJob.initJob();
            curJob.retryCount++;
            log.logThis("arrPckt   \(curJob.jobName)   retry  ...   \(curJob.retryCount)  ", lnum: 5)
        }
        
        log.함수차원_출력_End()
    }
    
}