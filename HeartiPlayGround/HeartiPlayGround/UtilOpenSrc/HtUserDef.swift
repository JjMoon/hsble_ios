//
//  HtUserDef.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2015. 11. 17..
//  Copyright © 2015년 IMLab. All rights reserved.
//

import Foundation

//////////////////////////////////////////////////////////////////////     _//////////_     [ Ht User Def     << >> ]    _//////////_
// MARK: User Defaults  파일 입출력..  관련 ..

class HtUserDef : NSObject {  //
    var log = HtLog(cName: "HtUserDef")
    let uDef = NSUserDefaults.standardUserDefaults()
    
    var arrPrevDevName:[String]?
    
    override init() {
        super.init()
        log.printThisFunc("Init :: ", lnum: 5)
        arrPrevDevName = uDef.objectForKey("arrPrevDevName") as? [String]
        
        log.printAlways(" arrPrevDevName   \(arrPrevDevName)  ")
        
        HsBleSingle.inst().stageLimit = Int32(uDef.integerForKey("StageLimit"))
        if HsBleSingle.inst().stageLimit == 0 { HsBleSingle.inst().stageLimit = 3 }
        
        langIdx = Int32(uDef.integerForKey("Language"))
        log.printAlways("   Stage Limit :  \(HsBleSingle.inst().stageLimit)   lanIdx : \(langIdx)   0: en,   1: 한글 ")
        
        if arrPrevDevName == nil {
            log.logThis("  no Prev Dev Name ...   set   _ _ _ _ ", lnum: 1)
            arrPrevDevName = ["_", "_", "_", "_"]
        } else {
            log.logThis("  arrPrevDevName  ::  \(arrPrevDevName?.count)     \(arrPrevDevName) ", lnum: 2)
        }
    }
    
    func writeStudentInfo() {
        uDef.setObject(HsBleMaestr.inst.arrStudent, forKey: "arrStudent")
    }
    
    func writePrevDevName() { uDef.setObject(arrPrevDevName, forKey: "arrPrevDevName") }
}
