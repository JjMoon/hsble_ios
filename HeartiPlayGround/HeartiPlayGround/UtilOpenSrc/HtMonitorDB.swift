//
//  HtMonitorDB.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2015. 11. 19..
//  Copyright © 2015년 IMLab. All rights reserved.
//

import Foundation




//struct CompObj {
//    var set_num:Int = 0, try_num:Int = 0, depth:Int = 0, recoil:Int = 0, cycle:Double = 0, record_id:Int = 0
//}
//struct RespObj {
//    var set_num:Int = 0, try_num:Int = 0, amount:Int = 0, record_id:Int = 0
//}
//struct CompInf {
//    var set_num:Int = 0, position_count1:Int = 0, position_count2:Int = 0, position_count3:Int = 0, position_count4:Int = 0, position_count5:Int = 0,
//    center_percent:Int = 0, record_id:Int = 0
//}


class HtMonitorDB: NSObject {
    var log = HtLog(cName: "HtMonitorDB")
 
    var dbPath = NSString()
    
    func checkAndMakeDB() {
        log.printThisFunc("  checkAndMakeDB  ", lnum: 5)
        let fileMan = NSFileManager.defaultManager()
        let path = NSSearchPathForDirectoriesInDomains(.DocumentationDirectory, .UserDomainMask, true)
        let docDir = path[0] as! String
        
        dbPath = docDir.stringByAppendingString("main.db")
        
        if !fileMan.fileExistsAtPath(dbPath as String) {
            let newDB = FMDatabase(path: dbPath as String)
            
            if newDB == nil {  log.printAlways("  Database Creation Failed !!!!", lnum: 20)  }
            
            if newDB.open() {
                let sqlState = "CREATE TABLE IF NOT EXISTS STUDENT (ID INTEGER PRIMARY KEY AUTOINCREMENT, Name TEXT, Password Text, Email TEXT, Phone TEXT, Age Int)"
                if !newDB.executeStatements(sqlState) {  log.printAlways("  another error  ", lnum: 20) }
                newDB.close()
            } else { log.printAlways("  Error   ", lnum: 20) }
        } else { log.printAlways("  기존 DB 있슴   OK ", lnum: 10) }
    }
    
    func addAStudent(sObj: HmStudent) -> Bool {
        log.printThisFunc("add A Student", lnum: 5)
        let curDB = FMDatabase(path: dbPath as String)
        var rVal = false
        if curDB.open() {
            let insertSql = "INSERT INTO STUDENT (Name, Email, Password, Phone, Age) VALUES ('\(sObj.name)', '\(sObj.email)', '\(sObj.password)', '\(sObj.phone)', \(sObj.age))"
            log.logThis(insertSql, lnum: 1)
            let rslt = curDB.executeUpdate(insertSql, withArgumentsInArray: nil)
            if !rslt { log.printAlways("  Error ", lnum: 10); rVal = true } // error
            else { log.printAlways(" Inserted :: OK   \(sObj.name) ", lnum: 5) }
        }
        curDB.close()
        return rVal
    }
    
    func deleteStudent(sObj: HmStudent) -> Bool {
        log.printThisFunc("deleteStudent A Student", lnum: 5)
        let curDB = FMDatabase(path: dbPath as String)
        var rVal = false
        if curDB.open() {
            let sql = "Delete From STUDENT Where Name is '\(sObj.name)'"
            log.logThis(sql, lnum: 1)
            let rslt = curDB.executeUpdate(sql, withArgumentsInArray: nil)
            if !rslt { log.printAlways("  Error ", lnum: 10); rVal = true } // error
            else { log.printAlways(" Inserted :: OK   \(sObj.name) ", lnum: 5) }
        }
        curDB.close()
        return rVal
    }
    
    func grabAllStudents() -> [HmStudent]{
        log.printThisFunc("grabAllStudents", lnum: 5)
        let curDB = FMDatabase(path: dbPath as String)
        var rArr = [HmStudent]()
        
        if curDB.open() {
            let sql = "SELECT * from  STUDENT"
            log.logThis(sql, lnum: 5)
            let rslt:FMResultSet? = curDB.executeQuery(sql, withArgumentsInArray:  nil)
            var hmStudent: HmStudent?
            while rslt?.next() == true {
                log.printThisFunc("  Add Student ..  ", lnum: 1)
                hmStudent = HmStudent(nm: (rslt?.stringForColumn("name"))!, em: (rslt?.stringForColumn("email"))!, pw: (rslt?.stringForColumn("password"))!,
                    phn: (rslt?.stringForColumn("phone"))!, ag: Int((rslt?.stringForColumn("age"))!)!)
                hmStudent!.dbid = Int((rslt?.stringForColumn("ID"))!)!
                rArr.append(hmStudent!)
            }
        }
        curDB.close()
        return rArr
    }
    
    func findAStudent(pName: String) -> HmStudent? {
        log.printThisFunc("findAStudent", lnum: 5)
        let curDB = FMDatabase(path: dbPath as String)
        var hmStudent: HmStudent?
        if curDB.open() {
            let sql = "SELECT * from  STUDENT WHERE Name = '\(pName)'"
            log.logThis(sql, lnum: 5)
            let rslt:FMResultSet? = curDB.executeQuery(sql, withArgumentsInArray:  nil)
            if rslt?.next() == true {
                hmStudent = HmStudent(nm: (rslt?.stringForColumn("name"))!, em: (rslt?.stringForColumn("email"))!, pw: (rslt?.stringForColumn("password"))!,
                    phn: (rslt?.stringForColumn("phone"))!, ag: Int((rslt?.stringForColumn("age"))!)!)
            } else {
                log.printAlways(" find :: error ", lnum: 5);
                hmStudent = nil
            }
        }
        curDB.close()
        return hmStudent
    }
}