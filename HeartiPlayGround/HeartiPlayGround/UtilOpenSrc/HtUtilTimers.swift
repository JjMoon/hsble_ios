//
//  HtUtilTimers.swift
//  heartisense
//
//  Created by Jongwoo Moon on 2015. 10. 27..
//  Copyright © 2015년 gyuchan. All rights reserved.
//

import Foundation


class HtGenTimer : NSObject {
    var startTime = NSDate.timeIntervalSinceReferenceDate()
    var endTime = NSDate.timeIntervalSinceReferenceDate()
    var totalSec: Double { get { return endTime - startTime } }
    var timeSinceStart: Double { get { return NSDate.timeIntervalSinceReferenceDate() - startTime } }
    var timeSinceEnd: Double { get { return NSDate.timeIntervalSinceReferenceDate() - endTime } }
    
    func markStart() { startTime = NSDate.timeIntervalSinceReferenceDate() }
    func markEnd() { endTime = NSDate.timeIntervalSinceReferenceDate() }
    
}

class HtTimer : NSObject { // 조건부 타이머..  3초 지나면 나타나는 용도..
    var condTimeLimit:Double = 3
    var theTime = NSDate.timeIntervalSinceReferenceDate()
    
    func Reset() {
        theTime = NSDate.timeIntervalSinceReferenceDate()
    }
    
    func IsConditionTrue() -> Bool {
        let delayTime = NSDate.timeIntervalSinceReferenceDate() - theTime
        return delayTime > condTimeLimit
    }
    
    func GetSecond(cnt: Int = 0) -> Double {
        if cnt == 0 {
            return NSDate.timeIntervalSinceReferenceDate() - theTime
        }
        if IsConditionTrue() {
            return NSDate.timeIntervalSinceReferenceDate() - theTime
        } else {
            return -1
        }
    }
}

class HtDelayTimer : NSObject {
    var theTimer = NSTimer()
    var theAction: (Void)-> Void = { }
    
    func doAction() {
        dispatch_after(
            dispatch_time( DISPATCH_TIME_NOW, Int64(0.1 * Double(NSEC_PER_SEC))),
            dispatch_get_main_queue(), theAction)
    }
    
    func cancel() {
        theTimer.invalidate()
    }
    
    func setDelay(delay:Double, closure:()->()) {  // 딜레이 함수..
        theAction = closure
        theTimer = NSTimer.scheduledTimerWithTimeInterval(delay, target: self,
            selector: "doAction", userInfo: nil, repeats: false)  // 한번만 실행..
    }
}


class HtStopWatch : NSObject { // 1/100 초까지 표시하는 용도...
    var log = HtLog(cName: "HtStopWatch")
    var condTimeLimit:Double = 3
    var theTime = NSDate.timeIntervalSinceReferenceDate()
    var pauseStartTime = NSDate.timeIntervalSinceReferenceDate()
    var totalPauseTime:Double = 0
    var isPause = false
    
    var dueTime:Double = 0
    var timeSinceStart: Double { get { setDueTime(); return dueTime }}
    
    func resetTime() {
        log.printThisFunc("resetTime", lnum: 10)
        
        theTime = NSDate.timeIntervalSinceReferenceDate()
        pauseStartTime = theTime
        totalPauseTime = 0
        isPause = false
    }
    
    func startOrReleaseToggle() {
        if isPause { togglePause() } // 아니면 그냥 놔둠..
    }
    
    func pause() {
        if !isPause { togglePause() }
    }
    
    func togglePause() {
        isPause = !isPause
        
        if isPause { // 시작..
            pauseStartTime = NSDate.timeIntervalSinceReferenceDate()
        } else {
            totalPauseTime += NSDate.timeIntervalSinceReferenceDate() - pauseStartTime
        }
    }
    
    func setDueTime() {
        
        dueTime = NSDate.timeIntervalSinceReferenceDate() - theTime - totalPauseTime
        //print(" \(theTime)  \(totalPauseTime)  \t \(dueTime)")
        if isPause {
            //dueTime = dueTime - pauseStartTime
            dueTime = dueTime - (NSDate.timeIntervalSinceReferenceDate() - pauseStartTime)
        }
        //print(" \(theTime)  \(totalPauseTime)  \t\t \(dueTime)")
    }
    
    func timeMMSSHH() -> String { // 1/100 sec ...
        setDueTime()
        //var tSec = NSDate.timeIntervalSinceReferenceDate() - theTime - totalPauseTime
        // if isPause {            tSec = tSec - pauseStartTime        }
        let mm = Int(dueTime / 60)
        let ss:Double = dueTime - Double(60 * mm)
        let ssInt = Int(ss)
        let hhInt = Int( 100 * (ss - Double(ssInt)))
        var rStr = "\(mm):"
        if ssInt < 10 { rStr += "0\(ssInt):" } else { rStr += "\(ssInt):" }
        if hhInt < 10 { rStr += "0\(hhInt)" } else { rStr += "\(hhInt)" }
        return rStr
    }
    
}


//////////////////////////////////////////////////////////////////////     _//////////_     [   Pause ..  << >> ]    _//////////_
// MARK: -  Pause 용 타이머

class HtUnitJob : NSObject {
    var actTimeInSec:Double = 0
    var theAction: (Void)-> Void = { }
    
    override init() { super.init() }
    
    convenience init(pTime: Double, pAct : ()->()) {
        self.init()
        self.actTimeInSec = pTime
        self.theAction = pAct
    }
}

class HtPauseCtrlTimer : HtStopWatch {
    var arrJobs = [HtUnitJob]()
    
    func resetJobs() {
        resetTime()
        arrJobs = [HtUnitJob]()
    }
    
    func addJob(pTime: Double, pAct : (Void)->Void) {
        let newJob = HtUnitJob(pTime: pTime, pAct: pAct)
        arrJobs.append(newJob)
    }
    
    func updateJob() {
        setDueTime()
        
        if arrJobs.count == 0 || dueTime < arrJobs[0].actTimeInSec { return }
        
        let curJob = arrJobs[0]
        curJob.theAction()
        arrJobs.removeAtIndex(0)
    }
}


