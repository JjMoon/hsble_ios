//
//  HsGlobal.swift
//  heartisense
//
//  Created by Jongwoo Moon on 2015. 9. 16..
//  Copyright (c) 2015년 gyuchan. All rights reserved.
//

import Foundation
import UIKit

class HsGlobal : NSObject {
    let foreGrndColors : [UIColor] =
        ([UIColor] (count: 19*2, repeatedValue: UIColor(red: 1, green: 0.96, blue: 0.32, alpha: 1)))
            + ([UIColor] (count: 7*2, repeatedValue: UIColor(red: 0.12, green: 0.98, blue: 0.33, alpha: 1.0)))
            + ([UIColor] (count: 4*3, repeatedValue: UIColor(red: 1, green: 0.12, blue: 0.12, alpha: 1)))

    //static 
    let backGrndColors : [UIColor] =  // 60ea..
    ([UIColor] (count: 19*2, repeatedValue: UIColor(red: 0.82, green: 0.79, blue: 0.25, alpha: 0.5)))
        + ([UIColor] (count: 7*2, repeatedValue: UIColor(red: 0.07, green: 0.44, blue: 0.14, alpha: 0.8)))
        + ([UIColor] (count: 4*3, repeatedValue: UIColor(red: 0.69, green: 0.07, blue: 0.1, alpha: 0.8)))
    

    let rpForeGrndColors : [UIColor] =  // 60ea.. 36
    ([UIColor] (count: 21, repeatedValue: UIColor(red: 1, green: 0.96, blue: 0.32, alpha: 1)))
        + ([UIColor] (count: 21, repeatedValue: UIColor(red: 0.12, green: 0.98, blue: 0.33, alpha: 1.0)))
        + ([UIColor] (count: 18, repeatedValue: UIColor(red: 1, green: 0.12, blue: 0.12, alpha: 1)))
    
    let rpBackGrndColors : [UIColor] =
    ([UIColor] (count: 21, repeatedValue: UIColor(red: 0.82, green: 0.79, blue: 0.25, alpha: 0.5)))
        + ([UIColor] (count: 21, repeatedValue: UIColor(red: 0.07, green: 0.44, blue: 0.14, alpha: 0.8)))
        + ([UIColor] (count: 18, repeatedValue: UIColor(red: 0.69, green: 0.07, blue: 0.1, alpha: 0.8)))

    let countBG : [UIColor] = ([UIColor] (count: 120, repeatedValue: UIColor(red: 0.1, green: 0.1, blue: 0.1, alpha: 0.1)))
    let countFG : [UIColor] = ([UIColor] (count: 120, repeatedValue: UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1)))
    
    static func delay(delay:Double, closure:()->()) {  // 딜레이 함수..
        dispatch_after(
            dispatch_time(
                DISPATCH_TIME_NOW,
                Int64(delay * Double(NSEC_PER_SEC))
            ),
            dispatch_get_main_queue(), closure)
    }
}


class HsUtil : NSObject {
    //       InitCalibration00, CalibrationView01, AllInOne10 = 10, StepByStep20 = 20, StandBy
    static func parseStr(bleObj: HsBleManager) -> String {
        switch bleObj.parsingState {
        case InitCalibration00 : return "InitCalibration00"
        case CalibrationView01:  return "CalibrationView01"
        case AllInOne10:         return "AllInOne10"
        case StepByStep20:       return "StepByStep20"
        case StandBy:            return "StandBy"
        default:        return "parsingState Error"
        }
    }
    
    /*
    "response_info_title" = "의식 확인";
    "response_info_txt" = "환자의 어깨를 흔들어 의식을 확인하세요.";
    "emergency_info_title" = "응급 구조 요청";
    "emergency_info_txt" = "주변의 한 사람을 지목하여 119 신고를 요청하세요.";
    "pulsecheck_info_title" = "맥박 확인";
    "pulsecheck_info_txt" = "최소 5초에서 최대 10초 동안 환자의 맥박을 확인한 후 맥박이 느껴지지 않으면 즉시 심폐소생술을 실시하세요.";
    "compression_info_title" = "가슴 압박";
    "compression_info_txt" = "가슴 압박을 30회 실시하세요.";
    "respiration_info_title" = "인공 호흡";
    "respiration_info_txt" = "인공 호흡을 2회 실시하세요.";
    "aed_info_title" = "AED";
  
*/
    
    static func curStepLocalized() -> String {
        switch currentStep {
        case .S_INIT0:                  return "S_INIT0"
        case .S_STEPBYSTEP_RESPONSE:    return localStr("response")
        case .S_STEPBYSTEP_EMERGENCY:   return localStr("emergency")
        case .S_STEPBYSTEP_CHECKPULSE:  return localStr("pulse_check")
        case .S_STEPBYSTEP_COMPRESSION, .S_WHOLESTEP_COMPRESSION:
            return localStr("compression")
        case .S_STEPBYSTEP_MULTI:       return localStr("response")
        case .S_STEPBYSTEP_BREATH, .S_WHOLESTEP_BREATH:
            return localStr("respiration")
        case .S_STEPBYSTEP_AED, .S_WHOLESTEP_AED:
            return localStr("aed")
        case .S_WHOLESTEP_DESCRIPTION:  return "WHOLESTEP_DESCRIPTION"

        case .S_WHOLESTEP_RESULT:       return localStr("result")
        case .S_WHOLESTEP_PRECPR:       return localStr("precpr")
            
        case .S_READY:                  return localStr("ready")
        case .S_QUIT:                   return "S_QUIT"
        case .CAL_LOAD:                 return "CAL_LOAD"
        case .CAL_NEW_CC:               return "CAL_NEW_CC"
        case .CAL_NEW_RP:               return "CAL_NEW_RP"
        case .S_CALIBRATION:            return "S_CALIBRATION"
        case .S_COMPLETE:               return "S_COMPLETE"
        }
    }
    
    static func curStepStr(pStep: CurStep = currentStep) -> String {
        switch pStep {
        case .S_INIT0:                  return "S_INIT0"
        case .S_STEPBYSTEP_RESPONSE:    return "S_STEPBYSTEP_RESPONSE"
        case .S_STEPBYSTEP_EMERGENCY:   return "S_STEPBYSTEP_EMERGENCY"
        case .S_STEPBYSTEP_CHECKPULSE:  return "S_STEPBYSTEP_CHECKPULSE"
        case .S_STEPBYSTEP_COMPRESSION: return "S_STEPBYSTEP_COMPRESSION"
        case .S_STEPBYSTEP_MULTI:       return "S_STEPBYSTEP_MULTI"
        case .S_STEPBYSTEP_BREATH:      return "S_STEPBYSTEP_BREATH"
        case .S_STEPBYSTEP_AED:         return "S_STEPBYSTEP_AED"
        case .S_WHOLESTEP_DESCRIPTION:  return "S_WHOLESTEP_DESCRIPTION"
        case .S_WHOLESTEP_COMPRESSION:  return "S_WHOLESTEP_COMPRESSION"
        case .S_WHOLESTEP_BREATH:       return "S_WHOLESTEP_BREATH"
        case .S_WHOLESTEP_AED:          return "S_WHOLESTEP_AED"
        case .S_WHOLESTEP_RESULT:       return "S_WHOLESTEP_RESULT"
        case .S_WHOLESTEP_PRECPR:       return "S_WHOLESTEP_PRECPR"
            
        case .S_READY:                  return "S_READY"
        case .S_QUIT:                   return "S_QUIT"
        case .CAL_LOAD:                 return "CAL_LOAD"
        case .CAL_NEW_CC:               return "CAL_NEW_CC"
        case .CAL_NEW_RP:               return "CAL_NEW_RP"
        case .S_CALIBRATION:            return "S_CALIBRATION"
        case .S_COMPLETE:               return "S_COMPLETE"
        }
    }
    
    static func prinCurStep(pStr:String) {
        print(" currentStep is ::   " + HsUtil.curStepStr() + "  _____  " + pStr)
    }
    
    static func isComp() -> Bool {
        switch currentStep {
        case .S_STEPBYSTEP_COMPRESSION, .S_WHOLESTEP_COMPRESSION:
            return true
        default:
            return false
        }
    }
    
    static func isBreath() -> Bool {
        switch currentStep {
        case .S_STEPBYSTEP_BREATH, .S_WHOLESTEP_BREATH:
            return true
        default:
            return false
        }
    }
}


class MuState : NSObject {
    var sName = "MuState"
    var isChanged = false
    var prevStep:CurStep = .S_INIT0
    
    var initJob:(Void)->Void = { }

    
    override init() {
        super.init()
    }

    convenience init(name:String) {
        self.init()
        self.sName = name
    }
    
    func setInitValueTo(newState:Bool) {
        //print ("  new State :  \(newState)")
        isChanged = newState
    }
    
    func compareThis_Once(curState:CurStep, willPrint:Bool) -> Bool {
        isChanged = prevStep != curState
        if isChanged && willPrint { print ("\n\n  State of < \(sName) >  is Changed  !!!  \n\n") }
        prevStep = curState
        return isChanged
    }
    
//    var initJob:(Void)->Void = { }
//    var compareState:(Void) -> Bool = { return false }
    
}