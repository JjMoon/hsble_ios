//
//  YgJsonBase.swift
//  YgManager
//
//  Created by Jongwoo Moon on 2015. 11. 14..
//  Copyright © 2015년 Jongwoo Moon. All rights reserved.
//

import Foundation

class HsJsonBase : NSObject {
    var logB = HtLog(cName: "HsJsonBase")
    var wasURL = "192.168.0.23/src"
    var serviceCode = "INIT"
    var version: Int = 1 // 패킷별 버전
    let qm = "\""
    
    var jStr = "", jResp:NSURLResponse?, jData:NSData?
    
    // response 관련 공통 값들.
    var result = -1
    
    override init() {
        super.init()
    }
    
    convenience init(theCode: String) {
        self.init()
        serviceCode = theCode
        makeHeader(theCode)
    }
    
    func makeHeader(theCode: String) {
        jStr = ""
        serviceCode = theCode
        addKeyValue("serviceCode", value: serviceCode, withFinishComma: true)
        addKeyValue("version", value: version, withFinishComma: true)
    }
    
    func finishEncoding() -> String {
        jStr = "{  " + jStr + "  }"
        return jStr
    }
    
    func makeArray(content: String) -> String {
        return " [ \(content) ] "
    }
    
    func quoat(strObj: String) -> String {
        return qm + strObj + qm
    }
    
    func keyValueString(key: String, valStr: String) -> String {
        return quoat(key) + " : " + valStr
    }
    
    func addKeyValue(key: String, value: AnyObject, withFinishComma: Bool) {
        switch value {
        case is Int, is Int32, is Int64, is Int8, is UInt, is UInt16, is Double, is Float:
                    jStr += keyValueString(key, valStr: ("\(value)"))
        default:    jStr += keyValueString(key, valStr: quoat("\(value)"))
        }
        if withFinishComma { jStr += ", " }
    }
    
    func request(addr: String, finishClj: (Void) -> Void) {
        logB.printThisFunc("*****     request   < \(addr) >  *****")
        let url = NSURL(string: wasURL + addr)
        let reqst = NSMutableURLRequest(URL: url!)
        reqst.HTTPMethod = "POST"
        reqst.HTTPBody = jStr.dataUsingEncoding(NSUTF8StringEncoding)
        
        print("\n\n\n  send Request >>> JSON >>>    \(jStr)  \n\n\n ")
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(reqst) {
            data, respse, errr in
            print ("  response :  \(respse)")
            self.jResp = respse
            self.jData = data
            if errr != nil {
                print("\n\n\n\n\n  YgJsonBase :: request  >>>    http error   \(errr?.description)  !!!!!  >>>>>   >>>>>   >>>>>   >>>>>   >>>>>   >>>>>   >>>>>   >>>>>   >>>>>    ERROR   \n\n\n")
                return
            } else { finishClj() }
        }
        task.resume()
    }
    
    
}