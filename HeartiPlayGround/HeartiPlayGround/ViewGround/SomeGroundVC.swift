//
//  SomeGroundVC.swift
//  HeartiPlayGround
//
//  Created by Jongwoo Moon on 2015. 12. 17..
//  Copyright © 2015년 HansTransformular. All rights reserved.
//


import UIKit


//////////////////////////////////////////////////////////////////////     _//////////_     [ Ygpk Init ]    _//////////_  첫 화면
// MARK: -  첫 화면
class SomeGroundVC: UIViewController {
    
    var log = HtLog(cName: "FdScene01VC")

    @IBOutlet weak var imgApple: UIImageView!

    override func viewWillAppear(animated: Bool) {

        log.printThisFunc("viewDidLoad")

        //imgApple.rotate360Degrees(1.2, repeatCnt: Float.infinity, completionDelegate: nil)

        var t = CGAffineTransformIdentity
        t = CGAffineTransformTranslate(t, CGFloat(100), 0)
        //t = CGAffineTransformRotate(t, CGFloat(M_PI_4))
        //t = CGAffineTransformScale(t, CGFloat(-1), CGFloat(2))
        // ... add as many as you want, then apply it to to the view
        //imgApple.transform = t

        //imgApple.updownAnimation(3, repeatCnt: 22, completionDelegate: nil)



    }

    @IBAction func bttnActButton(sender: AnyObject) {


        moveAnimation()
    }


    func compressAnimation() {
        UIView.animateWithDuration(0.5, delay: 0, usingSpringWithDamping: 110, initialSpringVelocity: 3, options: [ .Repeat, .Autoreverse  ],
            animations: {
                [unowned self] in self.imgApple.transform = CGAffineTransformMakeTranslation(0, 33)
            }) { [unowned self] (finished: Bool) in
                print("  Finished ")
        }

    }


    func moveAnimation() {
        UIView.animateWithDuration(1.5, delay: 0, usingSpringWithDamping: 10, initialSpringVelocity: 2, options: [ .Repeat  ],
            animations: {
                [unowned self] in self.imgApple.transform = CGAffineTransformMakeTranslation(40, 0)
            }) { [unowned self] (finished: Bool) in
                print("  Finished ")
        }

            //{
              //  [unowned self] (finished: Bool) in
//                UIView.animateWithDuration(1, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 5, options: [],
//                    animations: { [unowned self] in
//                        self.imgApple.transform = CGAffineTransformIdentity
//                    }) { [unowned self] (finished: Bool) in  print("  Finished ")  }
        //}


    }

}

public extension UIView {


    func simpleMove(duration: CFTimeInterval = 1.0, repeatCnt: Float, completionDelegate: AnyObject? = nil) {


    }



    func updownAnimation(duration: CFTimeInterval = 1.0, repeatCnt: Float, completionDelegate: AnyObject? = nil) {
        let transformAnim = CAKeyframeAnimation(keyPath:"transform")
        transformAnim.values = [
            NSValue(CATransform3D: CATransform3DMakeRotation(3 * CGFloat(M_PI/180), 0, 0, -1)),
            NSValue(CATransform3D: CATransform3DConcat(CATransform3DMakeScale(1.5, 1.5, 1), CATransform3DMakeRotation(3 * CGFloat(M_PI/180), 0, 0, 1))),
            NSValue(CATransform3D: CATransform3DMakeScale(1.5, 1.5, 1)),
            NSValue(CATransform3D: CATransform3DConcat(CATransform3DMakeScale(1.5, 1.5, 1), CATransform3DMakeRotation(-8 * CGFloat(M_PI/180), 0, 0, 1)))]
        transformAnim.keyTimes       = [0, 0.349, 0.618, 1]
        transformAnim.duration       = duration
        transformAnim.repeatCount = repeatCnt

        self.layer.addAnimation(transformAnim, forKey: "transform")
    }

    func rotate360Degrees(duration: CFTimeInterval = 1.0, repeatCnt: Float, completionDelegate: AnyObject? = nil) {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.repeatCount = repeatCnt
        rotateAnimation.fromValue = CGFloat(2 * M_PI)
        rotateAnimation.toValue = 0.0
        rotateAnimation.duration = duration
        if let delegate: AnyObject = completionDelegate {
            rotateAnimation.delegate = delegate
        }
        self.layer.addAnimation(rotateAnimation, forKey: nil)
    }
}
