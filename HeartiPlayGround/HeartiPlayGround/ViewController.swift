//
//  ViewController.swift
//  HeartiPlayGround
//
//  Created by Jongwoo Moon on 2015. 12. 17..
//  Copyright © 2015년 HansTransformular. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var bttnMain: UIButton!
    
    @IBOutlet weak var myTableVw: MyTable!
    @IBOutlet weak var scrlView: UIScrollView!

    
    @IBAction func bttnActMain(sender: UIButton) {


        let vc = SomeGroundVC(nibName: "SomeGroundView", bundle: nil)

        //let vc = LoginVC(nibName: "Login", bundle: nil)
        //vc.modalPresentationStyle = .Popover // .FormSheet // .Popover
        presentViewController(vc, animated: true, completion: nil)
        //vc.popoverPresentationController?.sourceView = view
        
        // let vc:UIViewController = UIStoryboard(name: "FrontDoor", bundle: nil).instantiateViewControllerWithIdentifier("RegistUserSB") as UIViewController
        //self.presentViewController(vc, animated: true, completion: nil)

    }
    
    @IBAction func bttnActBtnInView(sender: AnyObject) {
        let vc = MyCollectionVC(nibName: "MyCollectionVC", bundle: nil)
        //self.navigationController?.pushViewController(vc, animated: true)
        presentViewController(vc, animated: true, completion: nil)
    }
    

    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        print("  vc  collection View  touchesBegan ")
        //myTableView.touchesBegan(touches, withEvent: event)
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        print("  vc  touchesEnded ")
        //myTableView.touchesEnded(touches, withEvent: event)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        myTableVw.dataSource = myTableVw
        myTableVw.delegate = myTableVw

        scrlView.contentSize = myTableVw.bounds.size
    
        //myTableVw.allowsSelection = false
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}


class MyTable: UITableView, UITableViewDataSource, UITableViewDelegate {
    
    var arrCell = [MyCell]()
    
    
    override func awakeFromNib() {
        
        let nib = UINib(nibName: "MyCell", bundle: nil)
        registerNib(nib, forCellReuseIdentifier: "myCell")
        
    }
    
    override func hitTest(point: CGPoint, withEvent event: UIEvent?) -> UIView? {
        let vw = super.hitTest(point, withEvent: event)
        
        
        
        //scrollEnabled = false
        
        //print(" hit test : \( vw )  point : \(point)   event : \(event) ")
        
        return vw
    }
    
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        print("  tableView   touchesBegan ")
        //myTableView.touchesBegan(touches, withEvent: event)
    }

    override func touchesShouldCancelInContentView(view: UIView) -> Bool {
        
//        if view.isKindOfClass(UIButton) {
//            return true
//        }
        
        
        return false
    }
    
    // MARK:  table view delegate, data ..
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 5
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        //log.printAlways("  Assign Student : \(indexPath.row) ", lnum: 5)
        let cell:MyCell = tableView.dequeueReusableCellWithIdentifier("myCell", forIndexPath:indexPath) as! MyCell
        
        cell.tag = indexPath.row
        
        //myTableView.touchesShouldCancelInContentView(cell)
        
        //cell.buttonBut.addTarget(self, action: "cellButtonTouched", forControlEvents: .TouchUpInside)
        cell.buttonBut.userInteractionEnabled = true
        
        cell.contentView.userInteractionEnabled = false
        
        cell.bringSubviewToFront(cell.buttonBut)
        bringSubviewToFront(cell)
        
        
        arrCell.append(cell)
        
        return cell
    }
    
    
    @IBAction func cellButtonTouched(sender: UIButton) {
        print("   cell button touched in table view ")
    }

    
}



class MyCell: UITableViewCell {
    
    @IBOutlet weak var buttonBut: UIButton!
    
    @IBAction func bttnAction(sender: AnyObject) {
        
        print("\n\n\n  cell button touched  \n\n\n")
    }
    
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
//        print("  cell  \(self.tag)   touchesBegan ")
//        
//        
//        
//        let loc = touches.first?.locationInView(self)
//        
//        print("   \(buttonBut.frame.origin)   \(buttonBut.frame.size)  loc : \(loc) ")
//        

        
        
        //buttonBut.touchesBegan(touches, withEvent: event)
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        print("  cell  \(self.tag)   touches  Ended  !!!!!!!!!!   \(touches.count) ")
        
//        buttonBut.touchesEnded(touches, withEvent: event)

        
    }

    override func setHighlighted(highlighted: Bool, animated: Bool) {
        print ("\n cell Highlighted \(highlighted)  at \(self.tag) ")
        super.setHighlighted(highlighted, animated: animated)
        buttonBut.hidden = highlighted
        
        //self.contentView.addSubview(buttonBut)
        
        //self.buttonBut.highlighted = false
//        self.buttonBut.backgroundColor = UIColor.whiteColor()
//        buttonBut.setTitleColor(UIColor.blueColor(), forState: UIControlState.Normal)
    }
}





class PopoverProgVC: UIViewController {
    let dismissButton:UIButton! = UIButton(type:.Custom)
    let myImage:UIImage! = UIImage(named:"images")
    
    func pizzaDidFinish(){
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Build a programmatic view
        //background
        view.backgroundColor = UIColor(red: 0.8, green: 0.5, blue: 0.2, alpha: 1.0)
        //image
        if (myImage != nil){
            let myImageView = UIImageView(image: myImage)
            myImageView.frame = view.frame
            myImageView.frame = CGRectMake(10, 10, 200, 200)
            view.addSubview(myImageView)
        }else{
            print("image not found")
        }
        // add the label
        let myLabel = UILabel()
        myLabel.text = "Cheddar Popover"
        myLabel.frame = CGRectMake(10, 250, 300, 50)
        myLabel.font = UIFont(name: "Helvetica",
            size: 24)
        myLabel.textAlignment = .Left
        view.addSubview(myLabel)
        // add the done button
        dismissButton.setTitle("Done",
            forState: .Normal)
        dismissButton.titleLabel!.font = UIFont(name: "Helvetica", size: 24)
        dismissButton.titleLabel!.textAlignment = .Left
        dismissButton.frame = CGRectMake(150,175,200,50)
        dismissButton.addTarget(self,
            action: "pizzaDidFinish",
            forControlEvents: .TouchUpInside)
        view.addSubview(dismissButton)
    }
}