//
//  Login.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2015. 12. 17..
//  Copyright © 2015년 IMLab. All rights reserved.
//

import Foundation
import UIKit

class LoginVC: UIViewController {
    var log = HtLog(cName: "LoginVC")
    
    @IBOutlet weak var labelLMSLogin: UILabel!
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    @IBAction func bttnActLogin(sender: UIButton) {
    }
    
    @IBAction func bttnActCancel(sender: UIButton) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func bttnActSearchIDnPW(sender: UIButton) {
    }
    
}