//
//  HsExceptions.swift
//  HS Monitor
//
//  Created by Jongwoo Moon on 2016. 8. 2..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation



enum HsExptViewCtrl : ErrorType {
    case SubviewNumber
    case Thro222
}


enum HsExptAudio : ErrorType {
    case Initial
    case Play
}


let exptDivider = "===================================================================================================================   Exception catched   =====  "
func printException(msg: String) {
    print("\(exptDivider)")
    print("\n\n\n\n\n\n  \t\t  \(msg)  \n\n\n\n\n\n")
    print("\(exptDivider)")
}



/*

do {
    try self.exceptionTest()
} catch MyExceptTry.Throw1 {
    print("   catched ...  throw 1 ")
} catch {
    print("  final   ")
}

try! exceptionTest()



var arrSomething = [Int]()


func exceptionTest() throws {

    defer {
        print("  defer at 187")
    }

    defer {
        print("   defer at   191")
    }

    arrSomething.append(5)

    print("added  5  \(arrSomething.count)")

    guard throwww else {
        print("  oh my ")
        throw MyExceptTry.Throw1
    }

    guard arrSomething.count == 1 else {
        throw MyExceptTry.Thro222
    }



} */
