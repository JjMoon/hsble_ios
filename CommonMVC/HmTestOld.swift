//
//  HmTestAdult.swift
//  Test
//
//  Created by Jongwoo Moon on 2016. 2. 18..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation


///  이전 DB ....  ///////////////////////////////////            OLD

class HmTestBase: HsJsonBase { // 한 사람이 이 객체를 4개 갖는다..
    var stdntID = 0, dbID1 = 0, dbID2 = 0, standard = 0 // 이게 값을 갖으면 로컬에 저장된 거임..
    var amIAdult = false
    var timeStamp = NSDate()
    var include1 = false, include2 = false // 구조자 1, 2 데이터를 포함하는가..  둘 다 갖고 있으면 true 가능.
    // 테스트 끝나고 로컬 저장 전.
    // 서버는 두개 프로토콜..
    var tst1 = false, tst2 = false, tst3 = false, tst5 = false, passRes1 = false, passRes2 = false
    var chkSafety = false, airway = false, chkBreath = false
    var isRes1 = false, isRes2 = false

    func setCommonData(rcrd: FMResultSet, is1st: Bool) {
        stdntID = readIntFromDB(rcrd, name: "StudentID")
        standard = readIntFromDB(rcrd, name: "Standard")
        timeStamp = readDate(rcrd, name: "theTime")
        //print(" stdntID  \(stdntID)  standard  \(standard)  \(timeStamp.formatYYMMDDspaceTime) ")
    }

    func setChkSafeAirwayChkBrth(chkSafe: Int, airWay: Int, chkBrth: Int) {
        chkSafety = chkSafe == 1
        airway = airWay == 1
        chkBreath = chkBrth == 1
    }

    func setTst123(rcrd: FMResultSet) {
        // Response 의식 확인, Emergency 구조 요청, Check pulse 맥박 확인
        tst1 = readBoolFrom(rcrd, name: "Test1") // Int 에서 Bool 로
        tst2 = readBoolFrom(rcrd, name: "Test2")
        tst3 = readBoolFrom(rcrd, name: "Test3")
        include1 = true

//        if let timeobj = rcrd.dateForColumn("thetime") {
//            timeStamp = timeobj
//        } else { timeStamp = NSDate() }
    }

    func setTstAirwayChkbrth(rcrd: FMResultSet) {
        // Response 의식 확인, Emergency 구조 요청, Check pulse 맥박 확인
        chkSafety = readBoolFrom(rcrd, name: "ChkSafety") // Int 에서 Bool 로
        airway = readBoolFrom(rcrd, name: "Airway")
        chkBreath = readBoolFrom(rcrd, name: "ChkBreath")
    }

    func showMySelf() {
        log.logThis(" StdnID : \(stdntID)  \(timeStamp.formatYYMMDDspaceTime)  tst1,2,3,5 : \(tst1), \(tst2), \(tst3), \(tst5), passRes : \(passRes1), \(passRes2)   dbID : \(dbID1) | \(dbID2) ")
        log.logThis(" \t\t\t    standard : \(standard) chkSafety : \(chkSafety), airway : \(airway),  chkBreath : \(chkBreath) ")
    }

    func setResponseEmergencyPulse(resp: Int, emer: Int, puls: Int) {
        tst1 = (resp == 1)
        tst2 = (emer == 1)
        tst3 = (puls == 1)
        log.logUiAction(" response1 : \(tst1),   emergency1 : \(tst2),  pulse1 : \(tst3) ")
    }

    /// hard copy ...
    func setResc1From(obj: HmTestBase) {
        standard = obj.standard
        tst1 = obj.tst1
        tst2 = obj.tst2
        tst3 = obj.tst3

        chkSafety = obj.chkSafety
        airway = obj.airway
        chkBreath = obj.chkBreath

        passRes1 = obj.passRes1
        isRes1 = true
        include1 = true
    }

    func setResc2From(obj: HmTestBase) {
        //log.printThisFNC("setResc2From", comment: " dbID2 >> \(dbID2)")
        standard = obj.standard
        tst5 = obj.tst5
        passRes2 = obj.passRes2
        isRes2 = true
        include2 = true
    }

    /// Bool 을 JSON 에서 0, 1 로..
    func bool2int(bol: Bool) -> Int {
        if bol { return 1 }
        return 0
    }

    // MARK:  DB Update ...  디비 갱신 관련 함수..   Pass 정보도 포함하고 있슴..
    /** 디비 갱신 관련 함수..   Pass 정보도 포함하고 있슴..  제 1 구조자  */
    func sqlStr1Update() -> String {
        return "Test1 = \(getInt(tst1)), Test2 = \(getInt(tst2)), Test3 = \(getInt(tst3)), Pass1 = \(getInt(passRes1))"
    }
    /** 디비 갱신 관련 함수..   tst5, Pass 정보  제 2 구조자  */
    func sqlStr2Update() -> String {
        return "Test5 = \(getInt(tst5)), Pass2 = \(getInt(passRes2))"
    }

    func sqlStr1() -> String {
        return "\(getInt(tst1)), \(getInt(tst2)), \(getInt(tst3))"
    }

    func sqlStrEuNz() -> String {
        return "\(getInt(chkSafety)), \(getInt(airway)), \(getInt(chkBreath))"
    }

    func sqlStr2() -> String {
        return "\(getInt(tst5))"
    }

    /// 기준에 따른 초기 값.
    /*
     
     "testERC": {
     "testERC_1": 1, "testERC_2": 1, "testERC_3": 1, "testERC_4": 1, "testERC_5": 1
     },

     # NZ 기준일때
     "testNZ": {
     "testNZ_1": 1, "testNZ_2": 1, "testNZ_3": 1, "testNZ_4": 1, "testNZ_5": 1
     },


     */
    func addBaseJson() {
        let js = HsJsonBase()
        switch standard {
        case 1:
            js.addKeyValue("testERC_1", value: bool2int(chkSafety))
            js.addKeyValue("testERC_2", value: bool2int(tst1))
            js.addKeyValue("testERC_3", value: bool2int(airway))
            js.addKeyValue("testERC_4", value: bool2int(chkBreath))
            js.addKeyValue("testERC_5", value: bool2int(tst2), withFinishComma: false)
            js.makeObject()
            addKeyArray("testERC", value: js.jStr)
        case 2:
            js.addKeyValue("testNZ_1", value: bool2int(chkSafety))
            js.addKeyValue("testNZ_2", value: bool2int(tst1))
            js.addKeyValue("testNZ_3", value: bool2int(tst2))
            js.addKeyValue("testNZ_4", value: bool2int(airway))
            js.addKeyValue("testNZ_5", value: bool2int(chkBreath), withFinishComma: false)
            js.makeObject()
            addKeyArray("testNZ", value: js.jStr)
        default:
            addKeyValue("test1", value: bool2int(tst1));   addKeyValue("test2", value: bool2int(tst2))
            addKeyValue("test3", value: bool2int(tst3));
        }
    }

    /// Report 관련
//    func getBlsStepArray() -> (arrStr: [String], arrVal: [Bool]) {
//        var arrStr = ["abc"], arrVal = [ true ]
//
//        print("\n\n getBlsStepArray : amIAdult \(amIAdult)  standard : \(standard) ")
//
//        if amIAdult {
//            switch standard {
//            case 0:
//                 arrStr = [ langStr.obj.bls_adult_sheet_check_1, langStr.obj.bls_adult_sheet_check_2, langStr.obj.bls_adult_sheet_check_3 ]
//                 arrVal = [ tst1, tst2, tst3 ]
//            case 1: // 안전확인,
//                arrStr = [ langStr.obj.check_danger, langStr.obj.bls_adult_sheet_check_1, langStr.obj.open_airway, langStr.obj.check_breath, langStr.obj.bls_adult_sheet_check_2 ]
//                arrVal = [ chkSafety, tst1, airway, chkBreath, tst2 ]
//            default:
//                arrStr = [ langStr.obj.check_danger, langStr.obj.bls_adult_sheet_check_1, langStr.obj.bls_adult_sheet_check_2, langStr.obj.open_airway, langStr.obj.check_breath ]
//                arrVal = [ chkSafety, tst1, tst2, airway, chkBreath ]
//            }
//        } else {
//            switch standard {
//            case 0:
//                arrStr = [ langStr.obj.bls_infant_sheet_check_1, langStr.obj.bls_infant_sheet_check_2, langStr.obj.bls_infant_sheet_check_3 ]
//                arrVal = [ tst1, tst2, tst3 ]
//            case 1: // 안전확인,
//                arrStr = [ langStr.obj.check_danger, langStr.obj.bls_infant_sheet_check_1, langStr.obj.open_airway, langStr.obj.check_breath, langStr.obj.bls_infant_sheet_check_2 ]
//                arrVal = [ chkSafety, tst1, airway, chkBreath, tst2 ]
//            default:
//                arrStr = [ langStr.obj.check_danger, langStr.obj.bls_infant_sheet_check_1, langStr.obj.bls_infant_sheet_check_2, langStr.obj.open_airway, langStr.obj.check_breath ]
//                arrVal = [ chkSafety, tst1, tst2, airway, chkBreath ]
//            }
//        }
//        return (arrStr, arrVal)
//    }
}


class HmTestAdult: HmTestBase {
    var logg = HtLog(cName: "HmTestAdult", active: true)

    var tet6 = false, respWithBag1 = false, respWithBag2 = false // 1st
    var tet7 = false, tet8 = false, afterComp9a = false, afterComp9b = false // 2nd

    var finalStepOf1stRescure : Bool { get { return respWithBag1 && respWithBag2 } }
    var finalStepOf2ndRescure : Bool { get { return afterComp9a && afterComp9b } }


    // ////////////////////////////////////////////////////////////////////       [ DB 관련 ]
    // MARK:  DB   디비 관련 함수..

    override init() {
        super.init()
        amIAdult = true
    }

    convenience init(rcrd: FMResultSet, is1st: Bool) {
        self.init()
        log.clsName = "HmTestAdult"
        setCommonData(rcrd, is1st: is1st)

        if is1st {
            setTst123(rcrd) // tst1, 2, 3 세팅..
            setTstAirwayChkbrth(rcrd)
            dbID1 = readIntFromDB(rcrd, name: "ID")
            // Test6 Int, Test9rs1 Int, Test9rs2 Int, Pass1 Int) "
            tet6 =  readBoolFrom(rcrd, name: "Test6")
            respWithBag1 = readBoolFrom(rcrd, name: "Test9rs1")
            respWithBag2 = readBoolFrom(rcrd, name: "Test9rs2")
            passRes1 = readBoolFrom(rcrd, name: "Pass1") // 전체 패스 여부.
        } else { // "Test5 Int, Test7 Int, Test8 Int, Test9cc1 Int, Test9cc2 Int, Pass2 Int)"
            dbID2 = readIntFromDB(rcrd, name: "ID")
            tst5 = readBoolFrom(rcrd, name: "Test5")
            tet7 = readBoolFrom(rcrd, name: "Test7")
            tet8 = readBoolFrom(rcrd, name: "Test8")
            afterComp9a = readBoolFrom(rcrd, name: "Test9cc1")
            afterComp9b = readBoolFrom(rcrd, name: "Test9cc2")
            passRes2 = Int(rcrd.stringForColumn("Pass2"))! == 1
            include2 = true
        }
        showMySelf()
    }

//    func setValuesResc1FromDB(recd: FMResultSet) {
//        dbID1 = Int(recd.stringForColumn("ID"))!
//        setTst123(recd)//    }

    func sqlString(isFirst: Bool) -> String { // Adult
        if isFirst {
            // "Test1 Int, Test2 Int, Test3 Int, Test6 Int, Test9rs1 Int, Test9rs2 Int, Pass1 Int) "
            return "\(sqlStrEuNz()), \(sqlStr1()), \(getInt(tet6)), \(getInt(respWithBag1)), \(getInt(respWithBag2)), \(getInt(passRes1))"
        } else {
            // "Test5 Int, Test7 Int, Test8 Int, Test9cc1 Int, Test9cc2 Int, Pass2 Int)"
            return "\(getInt(tst5)), \(getInt(tet7)), \(getInt(tet7)), \(getInt(afterComp9a)), \(getInt(afterComp9b)), \(getInt(passRes2))"
        }
    }

    func updateSqlStr(is1st: Bool) -> String {
        if is1st { // Test6 Int, Test9rs1 Int, Test9rs2 Int,
            return "UPDATE ADULT1 SET \(sqlStr1Update()), Test6 = \(getInt(tet6)), Test9rs1 = \(getInt(respWithBag1)), " +
                "Test9rs2 = \(getInt(respWithBag2)) WHERE ID = \(dbID1)"
        } else { // Test7 Int, Test8 Int, Test9cc1 Int, Test9cc2 Int
            return "UPDATE ADULT2 SET \(sqlStr2Update()), Test7 = \(getInt(tet7)), Test8 = \(getInt(tet8)), " +
            "Test9cc1 = \(getInt(afterComp9a)), Test9cc2 = \(getInt(afterComp9b)) WHERE ID = \(dbID2)"
        }
    }

    /// Server Json
    func adultJson(test4Str: String) -> String {
        log.clsName = "HmTestAdult"
        jStr = "" // 초기화..

        addBaseJson()

        addKeyValue("test5", value: bool2int(tst5))
        addKeyValue("test6", value: bool2int(tet6));   addKeyValue("test7", value: bool2int(tet7))
        addKeyValue("test8", value: bool2int(tet8))
        if 10 < test4Str.getLength() { // 측정 결과가 있어야...
            addKeyString("test4", value: " { \(test4Str) } ")
        }
        addKeyArray("test9", value: "[ [\(bool2int(afterComp9a)), \(bool2int(afterComp9b)) ], " +
            "[\(bool2int(respWithBag1)), \(bool2int(respWithBag2)) ] ]")
        addKeyArray("pass", value:  makeArray( "\(bool2int(passRes1)), \(bool2int(passRes2))" ), withFinishComma: false)
        return finishEncoding()
    }

    // MARK:  UI 에서 값 저장 서브 함수.
    func setContinueCompOf_1stRescure(contComp: UIButton) {
        log.clsName = "HmTestAdult"
        tet6 = contComp.tag == 1
    }

    func setSecondRescureSwitch(bring: UIButton, handsOff: UIButton, defib: UIButton) {
        log.clsName = "HmTestAdult"
        tst5 = bring.tag == 1
        tet7 = handsOff.tag == 1 // handsoff 와 같음..
        tet8 = defib.tag == 1
    }

    func set30CompOf_2ndRescure(comp1: UIButton, comp2: UIButton) {
        log.clsName = "HmTestAdult"
        afterComp9a = comp1.tag == 1
        afterComp9b = comp2.tag == 1
    }

    func setBackMaskBreath2_1stRescure(resp1: UIButton, resp2: UIButton) {
        respWithBag1 = resp1.tag == 1
        respWithBag2 = resp2.tag == 1
    }

    /** 다른 객체로부터 프라퍼티를 복사하는 유틸 함수     */
    override func setResc1From(obj: HmTestBase) {
        super.setResc1From(obj)
        let adlt = obj as! HmTestAdult
        tet6 = adlt.tet6
        respWithBag1 = adlt.respWithBag1
        respWithBag2 = adlt.respWithBag2
    }

    override func setResc2From(obj: HmTestBase) {
        super.setResc2From(obj)
        let adlt = obj as! HmTestAdult
        tet7 = adlt.tet7
        tet8 = adlt.tet8
        afterComp9a = adlt.afterComp9a
        afterComp9b = adlt.afterComp9b
    }

    override func showMySelf() {
        super.showMySelf()
        log.logThis("    >>> Student : \(stdntID),   tet6 : \(tet6), respWithBag1 : \(respWithBag1),    respWithBag2 : \(respWithBag2) > Pass : \(passRes1) ")
        log.logThis("    >>> tet7 : \(tet7), tet8 : \(tet8), afterComp9a : \(afterComp9a), afterComp9b :  \(afterComp9b) > Pass : \(passRes2)" )
    }

}


class HmTestInfant: HmTestBase {

    var tst6a = false, tst6b = false
    var finger1 = false, compRateN = 0, adqDepth3 = false, recoil4 = false, minSusComp5 = false
    var tst7a = 0, tst7b = 0
    var tst7pass: Bool { get { return tst7a <= 9 && tst7b <= 9 }}

    var isStep4Pass: Bool {
        get { return finger1 && (compRateN < 18) && adqDepth3 && recoil4 && minSusComp5 }
    }

    var isStep6Pass: Bool { get { return tst6a && tst6b } }
    var isStep7Pass: Bool { get { return (tst7a < 9) && (tst7b < 9) } }

    convenience init(rcrd: FMResultSet, is1st: Bool) {
        self.init()
        log.clsName = "HmTestInfant"

        setCommonData(rcrd, is1st: is1st)

        if is1st {
            setTst123(rcrd) // tst1, 2, 3 세팅..
            setTstAirwayChkbrth(rcrd)
            dbID1 = readIntFromDB(rcrd, name: "ID")
            tst6a = Int(rcrd.stringForColumn("Test6a"))! == 1
            tst6b = Int(rcrd.stringForColumn("Test6b"))! == 1
            tst7a = readIntFromDB(rcrd, name: "Test7a")
            tst7b = readIntFromDB(rcrd, name: "Test7b")
            finger1 = Int(rcrd.stringForColumn("CompPosi"))! == 1
            compRateN = readIntFromDB(rcrd, name: "CompRate")
            adqDepth3 = Int(rcrd.stringForColumn("CompDepth"))! == 1
            recoil4 = Int(rcrd.stringForColumn("CompRecoil"))! == 1
            minSusComp5 = Int(rcrd.stringForColumn("Interrupt"))! == 1
            passRes1 = readBoolFrom(rcrd, name: "Pass1") // 전체 패스 여부.
        } else { // StudentID, Test5, Pass2
            dbID2 = readIntFromDB(rcrd, name: "ID")
            tst5 = readBoolFrom(rcrd, name: "Test5")
            passRes2 = readBoolFrom(rcrd, name: "Pass2") // 전체 패스 여부.
            include2 = true
        }
        showMySelf()
    }

    override func showMySelf() {
        super.showMySelf()
        log.logThis("    >>> Student : \(stdntID),   finger : \(finger1), compRateN : \(compRateN),    adqDepth : \(adqDepth3) ")
        log.logThis("    >>> recoil4 : \(recoil4), minSusComp : \(minSusComp5), passRes : \(passRes1) | \(passRes2)" )
    }

    func setStep4(fingerPlace: UIButton, rateInt: UITextField, depth: UIButton, recoil: UIButton, minimumSusComp: UIButton) {
        log.clsName = "HmTestInfant"
        finger1 = (fingerPlace.tag == 1)
        compRateN = rateInt.getInt(0)
        adqDepth3 = (depth.tag == 1)
        recoil4 = (recoil.tag == 1)
        minSusComp5 = (minimumSusComp.tag == 1)
        log.logUiAction(" setStep4 >>> finger : \(finger1), compRateN : \(compRateN),  adqDepth : \(adqDepth3)   recoil4 : \(recoil4) ,     minSusComp : \(minSusComp5)  ")
    }

    func setSwitchRoleOf2ndRescure(theRole: UIButton) {
        log.clsName = "HmTestInfant"
        tst5 = theRole.tag == 1
    }

    /// Server Json
    func infantJson() -> String {
        log.clsName = "HmTestAdult"
        jStr = "" // 초기화..

        addBaseJson()

        addKeyValue("test5", value: bool2int(tst5))
        //addKeyValue("test6", value: bool2int(tst6a && tst6b));
        addKeyArray("test6", value: "[ \(bool2int(tst6a)), \(bool2int(tst6b)) ]")

        //addKeyValue("test7", value: bool2int(tst7pass))
        addKeyArray("test7", value: "[ \(tst7a), \(tst7b) ]")

        let test4json = HsJsonBase()
        test4json.addKeyArray("interruption", value: minSusComp5)
        test4json.addKeyArray("comp_recoil", value: bool2int(recoil4))
        test4json.addKeyArray("comp_position", value: bool2int(finger1))
        test4json.addKeyArray("comp_depth", value: bool2int(adqDepth3))
        test4json.addKeyArray("comp_rate", value: compRateN, withFinishComma: false)

        addKeyString("test4", value: test4json.finishEncoding())
        addKeyValue("pass", value: bool2int(passRes1 && passRes2), withFinishComma: false)
        return jStr
    }

    // Test6a, Test6b, Test7a, Test7b,CompPosi, CompRate, CompDepth, CompRecoil, Interrupt, Pass1
    func sqlString(isFirst: Bool) -> String { // Infant
        if isFirst {
        return "\(sqlStrEuNz()), \(sqlStr1()), \(getInt(tst6a)), \(getInt(tst6b)), \(tst7a), \(tst7b), \(getInt(finger1)), \(compRateN), \(getInt(adqDepth3)), \(getInt(recoil4)), \(getInt(minSusComp5)), \(getInt(passRes1))"
        } else {
            return "\(getInt(tst5)), \(getInt(passRes2))"
        }
    }

    func updateSqlStr(is1st: Bool) -> String {
        if is1st {
            return "UPDATE INFANT1 SET \(sqlStr1Update()), Test6a = \(getInt(tst6a)), Test6b = \(getInt(tst6b)), " +
                "Test7a = \(tst7a), Test7b = \(tst7b), CompRate = \(getInt(finger1)), CompDepth = \(compRateN), " +
                "CompRecoil = \(getInt(recoil4)), Interrupt = \(getInt(minSusComp5)) " +
            " WHERE ID = \(dbID1)"
        } else {
            return "UPDATE INFANT2 SET \(sqlStr2Update()) " + " WHERE ID = \(dbID2)"
        }
    }

    func sqlString67() -> String {
        return "\(getInt(tst6a)), \(getInt(tst6b)), \(tst7a), \(tst7b)"
    }

    func setConductCPR_1stRescure(resp1: UIButton, resp2: UIButton, comp1: UITextField, comp2: UITextField) {
        tst6a = resp1.tag == 1
        tst6b = resp2.tag == 1
        log.logUiAction("  >>> respWithBag1 : \(tst6a),  respWithBag2 : \(tst6b)  ")
        tst7a = comp1.getInt(0) // Int(comp1.text!)!
        tst7b = comp2.getInt(0)
        log.logUiAction("  >>> setComp15Sec  >>>  tst7a : \(tst7a),   tst7b : \(tst7b) ")
    }

    /** 다른 객체로부터 프라퍼티를 복사하는 유틸 함수     */
    override func setResc1From(obj: HmTestBase) {
        super.setResc1From(obj)
        //log.printThisFNC("setResc1From", comment: " dbID1 >> \(dbID1)")
        let inft = obj as! HmTestInfant
        standard = inft.standard
        finger1 = inft.finger1;     compRateN = inft.compRateN
        adqDepth3 = inft.adqDepth3; recoil4 = inft.recoil4
        minSusComp5 = inft.minSusComp5

        tst6a = inft.tst6a; tst6b = inft.tst6b
        tst7a = inft.tst7a; tst7b = inft.tst7b
        log.함수차원_출력_End()
    }
}
