//
//  HtObject.swift
//  Test
//
//  Created by Jongwoo Moon on 2016. 3. 6..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation

class HtObject : NSObject {
    var log = HtLog(cName: "HtObject")

    func getInt(bol: Bool) -> Int { // 불리언을 0, 1 로 디비에 쓸 때.
        if bol == true { return 1 }
        return 0
    }

    func readBoolFromOptInt(rslt: FMResultSet, name: String) -> Bool? {
        if let val = rslt.stringForColumn(name) {
            return Int(val) == 1
        }
        print("\n\t\t\t DB Read Bool >> \(name) 이 없슴.  return nil ")
        return nil // 숫자가 없으면 nil
    }

    func readBoolFrom(rslt: FMResultSet, name: String) -> Bool {
        if let val = rslt.stringForColumn(name) {
            return Int(val) == 1
        }
        print("\n\t\t\t DB Read Bool >> \(name) 이 없슴.  return false ")
        return false // 숫자가 없으면 false
        //return Int(rslt.stringForColumn(name))! == 1
    }

    func readIntFromDB(rslt: FMResultSet, name: String) -> Int {
        if let val = rslt.stringForColumn(name) {
            return Int(val)!
        }
        print("\n\t\t\t DB Read Int >> \(name) 이 없슴.  return 0 ")
        return 0 // 숫자가 없으면 0
        //return Int(rslt.stringForColumn(name))!
    }

    func readDate(rslt: FMResultSet, name: String) -> NSDate {
        if let st = rslt.stringForColumn(name) {
            let format = NSDateFormatter()
            format.dateFormat = "yyyy-MM-dd HH:mm:ss"
            format.calendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierISO8601)!
            format.locale = NSLocale(localeIdentifier: "en_US_POSIX")
            if let date = format.dateFromString(st) {
                return date
            }
            return NSDate()
        }
        return NSDate()
    }
}
