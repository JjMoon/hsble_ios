//
//  HmUnitSet.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2015. 12. 15..
//  Copyright © 2015년 IMLab. All rights reserved.
//

import Foundation


//////////////////////////////////////////////////////////////////////     _//////////_     [     ]    _//////////_   Go to Main View
// MARK:  HmStroke
class HmUnitSet : HsJsonBase { // 한 세트에 대응하는 단위.
    var setNum = 1 // HtLog(cName: "HmUnitSet"),
    var dbRecordID = -1, parentID = -1
    var timeStamp = NSDate()
    
    var arrComprs = [HmCompStroke]()
    var arrBreath = [HmBrthStroke]()

    private var cTime: Double = 0
    var ccTime: Double {
        get { return cTime }
        set {
            if cTime < newValue { cTime = newValue }
        }
    }
    
    var ccPassCnt = 0, rpPassCnt = 0, ccCount = 0, rpCount = 0
    var arrPositionCnt = [Int](count:5, repeatedValue: 0)  // 5번째가 correct count ..
    var ccPositionStr: String { get {
        var str = ""
        for val in arrPositionCnt { str += ", \(val)" }
        return str.subStringFrom(2)
        }}


    var colorCCTime: UIColor { get { return colorGreenRedBy(ccTimePass) }}
    var colorCCCount: UIColor { get { return colorGreenRedBy(ccCntPass) }}
    var colorRPTime: UIColor { get { return colorGreenRedBy(rpTimePass) }}
    var colorRPCount: UIColor { get { return colorGreenRedBy(rpCntPass) }}

    var ccTimeIntStr: String { get { return "\(Int(ccTime))" } }
    var rpTimeIntStr: String { get { return "\(Int(rpTime))" } }

    var ccTimeStr: String { get { return "\(ccTime.format(".1"))" } }
    var rpTimeStr: String { get { return "\(rpTime.format(".1"))" } }
    var ccTimeSec: String { get { return "\(ccTimeNormal.format(".0"))s" } }
    var rpTimeSec: String { get { return "\(rpTimeNormal.format(".0"))s" } }

    var ccTimePass: Bool { get { return ccTimeNormal <= 18 }} // 결과 시트에서 색깔 표현 관련 판단 기준.
    var rpTimePass: Bool { get { return rpTimeNormal <= 10 }}
    var ccCntPass: Bool { get { return 23 <= ccPassCnt && 27 <= ccCount && ccCount <= 33  }}
    var rpCntPass: Bool { get { return 1 <= rpPassCnt && 1 <= rpCount && rpCount <= 3 }}

    var allPass: Bool { get { return ccTimePass && rpTimePass && ccCntPass && rpCntPass }}
    
    var holdTimeStr: String { get { return "\(holdTime.format(".1"))" } }
    // 아래는 임시 사용.
    var rpTime: Double = 0, holdTime: Double = 0

    /// 음수에 대한 예외 처리..
    var ccTimeNormal: Double {
        get { if ccTime < 0 { return 0 } else { return ccTime }}    }
    var rpTimeNormal: Double {
        get { if rpTime < 0 { return 0 } else { return rpTime }}    }

    // 정상 압박 위치 ..   position 비율 ...
    var ccCorrectPositionRatioString: String { get { return String(ccCorrectPositionPercent) } }
    var ccCorrectPositionPercent: Int { get { return Int(ccCorrectPositionRatio * 100) }}
    var ccCorrectPositionRatio: Double { get {
        if ccCount == 0 { return -1 }
        var cenCnt = 0
        for ob in arrComprs { if ob.wPosiIdx == 0 { cenCnt += 1 } }
        return Double(cenCnt) / Double(ccCount) } } // ccPassCnt

    //var rightPositionPercent: Int { get { return Int( Double(arr PositionCnt[4]) / Double(ccCount) * 100 ) } }
    
    var strokePerMin: Double { get {
        if ccTime == 0 { return 0 }
        return Double(ccCount) / ccTime * 60 } }
    var strokePerMinStr: String { get { return "\(Int(strokePerMin))" } }
    
    var lastCompObj: HmCompStroke {
        get {
            if arrComprs.count == 0 {
                print(" Error ! arrComprs.append( HmCompStroke()  ");
                arrComprs.append( HmCompStroke() ) }
            return arrComprs.last!
        }
    }
    
    var lastBrthObj: HmBrthStroke {
        get {
            if arrBreath.count == 0 { arrBreath.append( HmBrthStroke() ) }
            return arrBreath.last!
        }
    }

    var prevCompRate: Int {
        get {
            if arrComprs.count <= 1 { return 110 }
            return arrComprs[arrComprs.count - 1].rate;
        }
    }

    var compCountStr: String {
        //get { return "\(ccPassCnt) / \(ccCount)" }
        get { return ccPassCnt.format(2) + "/" + ccCount.format(2) }
    }
    var brthCountStr: String {
        //get { return "\(rpPassCnt) / \(rpCount)" }
        get { return rpPassCnt.format(2) + "/" + rpCount.format(2) }
    }

    override init() {
        super.init()
        log.clsName = "HmUnitSet"
    }

    var strokeStartTime = NSDate.timeIntervalSinceReferenceDate()
    /// V.1.08.160719 호흡 시간
    func markBreathTime() {
        // finalProcess 에서 압박의 마지막..  호흡의 첫 객체를 없앤다..
        if arrBreath.count == 2 {
            arrBreath.last?.strkTime = 7.5 // V.1.08.160719 호흡 시간 추가..

            print("호흡 시간 :  초기값 7.5 ")
        } else {
            //arrBreath[arrBreath.count - 2].markFinishTime()
            arrBreath.last?.strkTime = NSDate.timeIntervalSinceReferenceDate() - strokeStartTime

            //print("호흡 시간 : at > \(arrBreath.count - 2)  \(arrBreath[arrBreath.count - 2].time )")
            print("호흡 시간 : at > \(arrBreath.count - 2)  \(arrBreath[arrBreath.count - 2].strkTime )")
            print("호흡 시간 :  \(arrBreath.last?.strkTime)")
        }

        strokeStartTime = NSDate.timeIntervalSinceReferenceDate()

        
        //arrBreath.map { obj -> () in            print("  Time :  \(obj.strkTime) ")        }
    }

    func showAllBreathTime() {
//        for br in arrBreath {
//            //print("  Breath Stroke Time : \(br.strkTime)")
//        }
    }

    func getPositionRatio(idx: Int) -> CGFloat {
        if ccCount == 0 || 4 < idx { return 0 }

        var cnt = 0
        for ob in arrComprs {
            if ob.wPosiIdx == idx { cnt += 1 }
        }
        //print("getPositionRatio :: CGFloat of   \(cnt)) / \(ccCount)")

        return CGFloat(cnt) / CGFloat(ccCount) //return CGFloat(arrP ositionCnt[idx]) / CGFloat(ccCount)
    }

    func setValuesFromDB(recd: FMResultSet) {
        dbRecordID = Int(recd.stringForColumn("ID"))!
        setNum = Int(recd.stringForColumn("SetNum"))!
        parentID = Int(recd.stringForColumn("Owner"))!
        ccCount = Int(recd.stringForColumn("CcCount"))!; rpCount = Int(recd.stringForColumn("RpCount"))!
        ccPassCnt = Int(recd.stringForColumn("CcPassCount"))!; rpPassCnt = Int(recd.stringForColumn("RpPassCount"))!
        holdTime = Double(recd.stringForColumn("HandsOffTime"))!

        // CcTime Double, RpTime Double,
        ccTime = Double(recd.stringForColumn("CcTime"))!
        rpTime = Double(recd.stringForColumn("RpTime"))!

        arrPositionCnt = [ Int(recd.stringForColumn("CcPosi2"))!, Int(recd.stringForColumn("CcPosi2"))!,
            Int(recd.stringForColumn("CcPosi2"))!, Int(recd.stringForColumn("CcPosi2"))!, Int(recd.stringForColumn("CcPosi2"))!]

        if let timeobj = recd.dateForColumn("thetime") {
            timeStamp = timeobj
        } else { timeStamp = NSDate() }

        showMySelf()
    }

    func showMySelf() {
        print("\n\t\t HmUnitSet :: showMySelf >>>   StudentID : \(parentID),  dbRecordID : \(dbRecordID),  setNum : \(setNum), parentID : \(parentID), \n" +
            "\t\t  count : \(ccCount) | \(rpCount)    ccPassCnt : \(ccPassCnt), rpPassCnt : \(rpPassCnt), " +
        "holdTime : \(holdTime) timeStamp : \(timeStamp.formatYYMMDDspaceTime) \n") // arr PositionCnt : \(arr PositionCnt)
    }

    func getLabelCompTimeAndSpeed() -> String {
        // 소요시간 : 333 초, 평균 속도:  222/분
        return langStr.obj.duration  + " : \(ccTimeStr) \(langStr.obj.second),  " +
            langStr.obj.average_rate + " : \(strokePerMinStr)/ \(langStr.obj.minute) "
    }

    func finalProcess() {
        log.printThisFNC("finalProcess()", comment: " ccCount : \(ccCount) = \(arrComprs.count) arr, rpCount : \(rpCount) = \(arrBreath.count) arr    rpPassCnt : \(rpPassCnt)")
        if ccCount < arrComprs.count { arrComprs.removeLast() }

        for br in arrBreath {
            br.showMySelf()
        }

        if rpCount < arrBreath.count { arrBreath.removeFirst() }
    }
    
    func 분당압박횟수() -> Int {
        if ccTime < 0.01 {
            log.printAlways("  분당압박횟수   uSet.ccTime == 0   \(ccTime) ", lnum: 20)
            return 40
        }
        return Int(Double(ccCount) / ccTime * 60)
    }

    func breathVolJson() -> String {
        if arrBreath.count == 0 { return "[ ]" }
        var rStr = ""
        for strk in arrBreath {
            rStr += ", \(strk.max)"
        }
        return "[ \(rStr.subStringFrom(2)) ]"
    }






    func breathCycleJson() -> String {
        if arrBreath.count == 0 { return "[ ]" }
        var rStr = ""
        for strk in arrBreath {
            rStr += ", \(strk.strkTime)"
        }
        return "[ \(rStr.subStringFrom(2)) ]"
    }

    func compDepthJson() -> String {
        if arrComprs.count == 0 { return "[ ]" }
        var rStr = ""
        for strk in arrComprs {
            rStr += ", \(strk.maxDepth)"
        }
        return "[ \(rStr.subStringFrom(2)) ]"
    }

    func compRecoilJson() -> String {
        if arrComprs.count == 0 { return "[ ]" }
        var rStr = ""
        for strk in arrComprs {
            rStr += ", \(strk.recoilDepth)"
        }
        return "[ \(rStr.subStringFrom(2)) ]"
    }
    func compStrokeJson() -> String {
        if arrComprs.count == 0 { return "[ ]" }
        var rStr = ""
        for strk in arrComprs {
            rStr += ", \(strk.strkTime)"
        }
        return "[ \(rStr.subStringFrom(2)) ]"
    }

    func myrandom() -> Int {
        
        // get the current date and time
        let currentDateTime = NSDate()
        
        // get the user's calendar
        let userCalendar = NSCalendar.currentCalendar()
        
        // choose which date and time components are needed
        let requestedComponents: NSCalendarUnit = [
            NSCalendarUnit.Year,
            NSCalendarUnit.Month,
            NSCalendarUnit.Day,
            NSCalendarUnit.Hour,
            NSCalendarUnit.Minute,
            NSCalendarUnit.Second
        ]
        
        // get the components
        let dateTimeComponents = userCalendar.components(requestedComponents, fromDate: currentDateTime)
        
        for _ in 1 ... dateTimeComponents.second {
            arc4random()
            //random()
        }
        return Int(arc4random()) // random()
    }
    
    
    func autoGen4Debug(pNum: Int) {
        //        log.printThisFunc("auto Gen ...  4 Debug", lnum: 2)
        //        for _ in 1...2 {
        //            let aBr = HmUnitStoke()
        //            aBr.isComp = false
        //            aBr.max = myrandom() % 99
        //
        //            log.logThis(" breath value : max is \(aBr.max)", lnum: 1)
        //
        //            //if aBr.max > 60 { aBr.amount = .Over }
        //            //if aBr.max < 30 { aBr.amount = .Short }
        //            arrBreath.append(aBr)
        //        }
        //
        //        log.logBase(3)
        //
        //        for _ in 1...pNum {
        //            let aUnit = HmUnitStoke()
        //
        //            if random() % 4 == 1 {
        //                aUnit.isExist = false
        //                arrStokes.append(aUnit)
        //                continue
        //            }
        //
        ////            switch random() % 3 {
        ////            case 0: aUnit.speed = .Fast
        ////            case 1: aUnit.speed = .Norm
        ////            default: aUnit.speed = .Slow
        ////            }
        //
        //            aUnit.min = myrandom() % 30
        //            aUnit.max = 70 + myrandom() % 30
        //            
        //            log.logThis(" comp value : \(aUnit.min) ~ \(aUnit.max)", lnum: 0)
        //            
        //            arrStokes.append(aUnit)
        //}
        
    }
    
    
}
