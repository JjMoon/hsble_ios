//
//  HmStroke.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2015. 11. 13..
//  Copyright © 2015년 IMLab. All rights reserved.
//

import Foundation

/* 
static class Scenario_Result_Data {
    boolean is_save_db;

    int[][] cc_depth = new int[5][30]; // max depth
    int[][] cc_recoil_depth = new int[5][30]; // 리코일 안 될 때 min 값. 정상일 경우 0
    double[][] cc_cycle = new double[5][30];  //
    double[] cc_time = new double[5]; // 중첩되니 옮겨야 할까?
    
    int[] cc_pass_count = new int[5];
    int[] cc_count = new int[5];
    int[][] cc_position_count = new int[5][5];

    int[][] rp_amount = new int[5][2];
    int[] rp_pass_count = new int[5];
    int[] rp_count = new int[5];
    double[] rp_time = new double[5];

*/

class HmUnitStoke: NSObject {
    var name = "HmUnitStroke"
    var max: Int = 0, min: Int = 0
    var isExist = true, isComp = true
    var setID: Int = 0, sn: Int = 0
    var strkTime = 0.01

    override  var description: String {
        return "   min : \(min),  \t max : \(max) \t isExist : \(isExist)  \t "
    }

//    /// HsData 에서 세트 증가시 무조건 객체 하나를 추가하므로 첫번째건 여기서 다시 세팅해줘야 함.
//    func markStartTime() { // // V.1.08.160719
//        startTime = NSDate.timeIntervalSinceReferenceDate()
//    }
//
//    func markFinishTime() {
//        strkTime = NSDate.timeIntervalSinceReferenceDate() - startTime
//    }

    func getStaEnd (from: CGFloat, topo: CGFloat ) -> (CGFloat, CGFloat) {
        let diff = topo - from // 100일때 길이
        if !isExist {
            return (from + diff * CGFloat(70) / 100, from + diff * CGFloat(30) / 100)
        }        // 0 ... 100 사이 내분점.
        return (from + diff * CGFloat(max) / 100, from + diff * CGFloat(min) / 100)  // Y 는 방향이 반대니... 뒤집어야..
    }
}

class HmCompStroke: HmUnitStoke {
    var maxDepth = 0, recoilDepth = 0, wPosiIdx = 0 // 0 이면 정상.
    // var position = [Bool] (count: 4, repeatedValue: false)  이넘은 wPosiIdx 로 대치...
    // idx : 1 > Left Arrow Image, 2 > Up, 3 > Right, 4 > Down..
    // // arrP 순서 : 머리, 왼, 다리, 오른.


    var rate: Int { get { return Int(60.0 / strkTime) }}

    var packetNum = 0


    // MARK:  압박 한 스트로크 관리 타이머.
    var strokeTimer = HtGenTimer()

    override init() {
        super.init()
        name = "Compression";    isComp = true
    }

    convenience init(rcrd: FMResultSet) {
        self.init()
        // SetID Int, SN Int, MaxD Int, RecoilD Int, Period Double)"
        setID = Int(rcrd.stringForColumn("SetID"))!
        sn = Int(rcrd.stringForColumn("SN"))!
        maxDepth = Int(rcrd.stringForColumn("MaxD"))!
        recoilDepth = Int(rcrd.stringForColumn("RecoilD"))!
        strkTime = Double(rcrd.stringForColumn("Period"))!

        if let posiIdx = rcrd.stringForColumn("wPosiIdx") { wPosiIdx = Int(posiIdx)! }
        else { print(" DB Version is old :: >> Wrong Position") }

        //showMySelf()
    }

    func newPacketReceived() {
        packetNum += 1
    }


    func setPeriodByPacket() {
        strkTime = Double(packetNum) * 0.05
    }

    func showMySelf() {
        print("\t\t HmCompStroke :: showMySelf >>>    setID : \(setID), sn : \(sn), maxDepth : \(maxDepth), " +
            "recoilDepth : \(recoilDepth), period : \(strkTime)  wPosiIdx : \(wPosiIdx) \n")
    }

    override func getStaEnd(from: CGFloat, topo: CGFloat) -> (CGFloat, CGFloat) {
        let diff = topo - from // 100일때 길이
        return (from + diff * CGFloat(maxDepth) / 100, from + diff * CGFloat(recoilDepth) / 100)  // Y 는 방향이 반대니... 뒤집어야..
    }


    func get78position(from: CGFloat, topo: CGFloat) -> CGFloat {
        let diff = topo - from // 100일때 길이
        return from + diff * CGFloat(78) / 100
    }

    func setVars(maxD: Int, recoilD: Int, prd: Double) {
        maxDepth = maxD; recoilDepth = recoilD; strkTime = prd
    }

}

/// 호흡 속도 세팅 : strkTime

class HmBrthStroke: HmUnitStoke {
    /*
        HmUnitSet  ::  markBreathTime       
            >> arrBreath.last?.strkTime = 7.5 // V.1.08.160719 호흡 시간 추가..
        CalcManager :: recoilAmount
            >> if isAllInOnePrcss {  curStageObj.markBreathTime() } // V.1.08.160719
     */

    override init() {
        super.init()
        name = "Breath";         isComp = false
    }
    
    convenience init(mx: Int, prd: Double) {
        self.init()
        max = mx; strkTime = prd
    }

    convenience init(rcrd: FMResultSet) {
        self.init()
        // SetID Int, SN Int, Max Int, Period Double)"
        setID = Int(rcrd.stringForColumn("SetID"))!
        sn = Int(rcrd.stringForColumn("SN"))!
        max = Int(rcrd.stringForColumn("Max"))!
        strkTime = Double(rcrd.stringForColumn("Period"))!
        // showMySelf()
    }

    func showMySelf() {
        print("\t\t HmBrthStroke :: showMySelf >>>    setID : \(setID), sn : \(sn), max : \(max), time : \(time) \n")
    }


}


