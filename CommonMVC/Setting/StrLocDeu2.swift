//
//  ShlKr.swift
//  Test
//
//  Created by Jongwoo Moon on 2016. 1. 21..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation


class StrLocDeu2 : StrLocBase {
    override func setValues() {

        // iOS 에서 수정/추가한 스트링...
        select_menu = "Einstellung";

        
        
        
        
        
        
        
        
        
        
        
        
        /// etc
        pause = "Pause";
        select_old_records_send = "There are old test data that were not sent to the server. You must transfer it to the server in order to see it. Do you want to transfer?"
        
        
        
        
        
        
        
        select_manikin_type = "Puppe";
        select_manikin_dir = "Richtung der Puppe";

        rescuer = "Retter";
        
        date = "Datum"

        quitArrow = "◀︎ Beenden";
        skipArrow = "Überspringen ▶︎"
        repeatKey = "Wiederholen" ///repeat = "Wiederholen";
        prev_gt = "◀︎ Zurück";
        complete_icon = "✓ Vollständig";
        volume = "Volumen"
        select_comp_rate_guide = "Anleitung der Herzdruckmassagenrate";
        select_date_format = "Datumsformat"

        // Score Option
        bls_setting = "Einstellungen BLS"
        total_score = "Gesamtpunktzahl"
        reset_settings = "Einstellungen zurücksetzen";

        stepbystep2line = "Schritt-für- \nSchritt-Übung"
        wholestep_2line = "Üben Sie den \ngesamten Prozess"

        repeat_icon = "✎ Wiederholen";
        edit_icon = "✎ Bearbeiten";

        compressionRatio = "Zeit mit CPR Tätigkeit"
        aed_infoTotalTxt = "Schalten Sie den Defibrillator vorab ein, wenn er betriebsbereit ist.\n" +
        "Öffnen Sie das Hemd des Patienten oder durchschneiden Sie es und bringen Sie die \n" +
        "Defibrillatorelektroden auf seiner Brust an.\n" +
        "Stellen Sie sicher, dass das Opfer bei Bewusstsein ist und verabreichen Sie \n" +
        "dem Opfer Schocks, indem Sie die Taste auf dem Defibrillator drücken.\n" +
        "Nachdem Sie dem Opfer einen Schock verabreicht haben, müssen Sie die HLW \nsofort fortsetzen."


        login = "Anmeldung";
        sending = "Wird gesendet...";
        select_cpr_standard = "HLW Richtlinie";
        select_language = "Sprache";
        //select_mannequin = "Richtung der Puppe";
        select_set_num = "Zykluseinstellung";
        first_setting_txt = "Wenn Sie das Programm zum ersten Mal ausführen, befolgen Sie die Schritte für die erstmalige Einstellung und Kalibrierung.";
        scanning_kit = "Kit wird gescannt...";
        select_kit = "Kit auswählen";
        calibration_start = "Start Kalibrierung";
        calibration = "Kalibrierung";
        kit_id = "Kit-ID:";
        new_ = "Neu";
        load = "Laden";
        prev = "Zurück";
        chest_compression_calibration = "Kalibrierung der Herzdruckmassage";
        breath_calibration = "Kalibrierung des Atems";
        write_calibration_name = "";
        preset = "";
        custom = "";
        current = "Aktuell";
        compression_depth = "Tiefe der Herzdruckmassage";
        breath_volume = "Atemvolumen";
        edit = "Bearbeiten";
        save = "Speichern";
        apply = "Anwenden";
        delete = "Löschen";
        set_breath = "Atem einstellen";
        set_compression = "Herzdruckmassage einstellen";
        compression30 = "15 Herzdruckmassagen durchführen";
        adequate5 = "Atmen Sie fünf Mal gemäßigt tief ein.";
        lay_rescuer = "Laienhelfer";
        bls_rescuer = "Gesundheitsdienstleister";
        start_course = "Kurs starten";
        adult = "Erwachsener";
        infant = "Kleinkind";
        start_test = "Test starten";
        manage_data = "Datenmanagement";
        add_student = "Student hinzufügen";
        student_info = "Info Student";
        all_check = "Alle auswählen";
        all_uncheck = "Auswahl aufheben";
        name = "Name";
        email = "Email";
        view_record = "Datensätze anzeigen";
        data_selected = "Datensatz ist ausgewählt";
        datas_selected = "Datensätze sind ausgewählt";
        send_data = "Daten senden";
        optional = "Optionen";
        register_kit = "Kit registrieren";
        heartisense_monitor = "HeartiSense Monitor";
        select_practice_mode = "Wählen Sie den Übungsmodus aus";
        step_by_step_mode_short = "Schritt-für-Schritt-Übung";
        whole_step_mode_short = "Üben Sie den gesamten Prozess";
        step_by_step_mode_full = "Schritt-für-Schritt-Übung";
        whole_step_mode_full = "Üben Sie den gesamten Prozess";
        adult_title = "Erwachsenen-LSM und AED";
        infant_title = "Kleinkinder-LSM-Beurteilung";
        all_title = "LSM- und Defibrillator-Beurteilung";
        assign_student = "Student zuweisen";
        connect_kit = "Kit verbinden";
        change_kit_1 = "Kit ändern";
        change_kit_2 = "Kit ändern";
        connecting_kit = "Kit wird verbunden...";
        searching_kit = "Kit wird gesucht...";
        compression_pad = "Herzdruckmassagekissen";
        breath_module = "Atemmodul";
        and = " und ";
        pass = "Bestanden";
        fail = "Nicht bestanden";
        cycle = "Zyklus";
        ready = "Bereit";
        scenario_description = "Szenariobeschreibung";
        response = "Antwort";
        emergency = "Notfall";
        check_pulse = "Puls prüfen";
        compression = "Herzdruckmassage";
        breath = "Atem";
        cpr = "HLW";
        aed = "AED";
        precpr_aha_bls = "Antwort\nNotfall\nAtem prüfen\nPuls prüfen";
        precpr_aha_lay = "Antwort\nNotfall\nAtem prüfen";
        precpr_erc = "Auf Gefahr prüfen\nAntwort\nLuftweg freimachen\nAtem prüfen\nNotfall";
        precpr_anzcor = "Auf Gefahr prüfen\nAntwort\nNotfall\nLuftweg freimachen\nAtem prüfen";
        disconnected = "Verbindung getrennt";
        connected = "Verbunden";
        hands_off_time = "Hands-off-Zeit";
        second = "Sek.";
        minute = "Min.";
        time = "Zeit";
        timer = "Zeit";
        position = "Position";
        depth = "Profondeur";
        rate = "Rate";
        count = "Zähler";
        no_named = "Kein Name";
        save_record = "Datensätze speichern";
        save_warning = "*Beachten Sie, dass frühere Datensätze mit neuen Datensätzen überschrieben werden.";
        save_exist = "Datensätze vorhanden";
        step = "Schritt";
        result = "Ergebnis";
        auto_connect = "Automatisch verbinden";
        result_guide = "* Anzahl der Erfolge/Anzahl der Versuche";
        save_all = "Alle speichern";
        saved = "Gespeichert";
        close = "Schließen";
        view_detail = "Detail anzeigen";
        not_assigned = "nicht zugewiesen";

        ///.repeat = "Wiederholen";

        complete = "Vollständig";
        exist = "Vorhanden";
        data = "Daten";
        student_name = "Name Student";
        select = "Auswählen";
        assign_result = "Zuweisung";
        detail = "Detail";
        graph = "Diagramm";
        group = "Gruppe";
        compression_rate = "Herzdruckmassagenrate";
        compression_position = "Herzdruckmassage-Position";
        duration = "Dauer";
        average_rate = "Durchschnittsrate";
        recoil = "Entlastung";
        good_depth = "Gut";
        strong_depth = "Stark";
        fast = "Schnell";
        good_rate = "Gut";
        slow = "Langsam";
        excessive = "Zu stark";
        good_amount = "Gut";
        insufficient = "Schwach";
        recoil_needed = "Entlastung erforderlich";
        too_weak = "Zu schwach";
        wrong_position = "Falsche Position";
        option_calibration = "    Kalibrierung";
        option_preference = "    Erste Einstellung";
        not_valid_email = "Die E-Mail-Adresse hat keine gültige Form.";
        login_fail = "E-Mail-Adresse oder Kennwort ungültig.";
        not_assigned_inst_id = "Die von Ihnen eingegebene ID ist nicht der Einrichtung zugewiesen.";
        server_error = "Beim Senden der Daten an den Server ist ein Fehler aufgetreten. Wenn der Fehler weiterhin besteht, wenden Sie sich bitte an uns.";
        no_license = "Die von Ihnen eingegebene ID hat keine Lizenz.";
        license_expiration = "Der Lizenzzeitraum der von Ihnen eingegebenen ID ist abgelaufen.";
        license_data_limit = "Die Datennummer-Lizenz der von Ihnen eingegebenen ID ist abgelaufen.";
        connect_fail = "Verbindung mit dem Kit nicht möglich.";
        connect_success = "Verbindung mit dem Kit hergestellt.";
        disconnect_kit = "Verbindung unterbrochen.";
        using_data_calibration = "Sie können diesen Konfigurationswert nicht löschen, da er verwendet wird.";
        sensor_error_detected = "";
        sensor_error_detected_m = "";
        sensor_not_connected = " ist nicht angeschlossen.";
        sensor_not_connected_m = " sind nicht angeschlossen.";
        at_least_1_connect = "Der Kit ist nicht angeschlossen.";
        not_permission = "Sie können dieses Programm nicht mit diesem HeartiSense-Kit verwenden. Prüfen Sie die Aufschrift des Kits.";
        empty_name_or_email = "Geben Sie Ihren Namen und Ihre E-Mail-Adresse ein.";
        all_check_need = "Sie können die Daten erst speichern, wenn alle Auszubildenden beurteilt sind.";
        not_valid_name = "Ungültiger Name. (Verwenden Sie Buchstaben a bis z, A bis Z, Ziffern 0 bis 9, koreanische Zeichen, - und _.)";
        search_the_kit = "Kit automatisch durchsuchen";
        network_disconnected = "Sie sind nicht mit einem Netzwerk verbunden";
        bt_off_msg = "Alle Bluetooth-Verbindungen werden getrennt.";
        need_connect_kit = "Das Kit muss angeschlossen sein.";
        start_single_mode = "Beginnen Sie mit dem individuellen Lernmodus.";
        start_monitor_mode = "Beginnen Sie mit dem Überwachungsmodus.";
        bls_using = "HeartiSense-Test wird verwendet. Dieses Programm wird geschlossen.";
        calibration_using = "Kalibrierung wird im Programm des Dozenten verwendet. Dieses Programm wird geschlossen.";
        wait_instructor = "Warten Sie auf die Anweisungen des Ausbilders.";
        search_fail = "Kit-Suche fehlgeschlagen. Netzschalter des Kits prüfen.";
        want_connect_retry = "Verbindung zum Kit fehlgeschlagen.";
        want_quit = "Sind Sie sicher, dass Sie das Programm schließen möchten?";
        want_back_calibration = "Sind Sie sicher, dass Sie zur vorherigen Phase zurückkehren möchten? Der von Ihnen erstellte Kalibrierungswert wird nicht gespeichert.";
        want_send = "Sind Sie sicher, dass Sie die ausgewählten Datensätze an den Server senden möchten?";
        want_delete = "Sind Sie sicher, dass Sie die ausgewählten Datensätze löschen möchten?";
        want_main = "Sind Sie sicher, dass Sie zum Hauptmenü zurückkehren möchten?";
        want_back = "Sind Sie sicher, dass Sie zur vorherigen Phase zurückkehren möchten?";
        want_turn_on_bluetooth = "Möchten Sie Bluetooth aktivieren?";
        want_cali_main = "Speichern abgeschlossen. Möchten Sie zur Hauptkalibrierungsseite wechseln?";
        want_disconnect = "Möchten Sie die Verbindung trennen?";
        want_restart_training = "Möchten Sie die Schulung fortsetzen?";
        want_restart_test = "Möchten Sie den Test fortsetzen?";
        want_start_stepbystep_mode = "Möchten Sie mit der schrittweisen Übung beginnen?";
        want_start_wholestep_mode = "Möchten Sie mit dem Einüben des gesamten Prozesses beginnen?";
        want_delete_kit = "Möchten Sie das registrierte Kit entfernen und zum manuellen Modus wechseln?";
        want_assign_completed = "Ist die Zuweisung vollständig? Daten der nicht zugewiesenen Studenten werden nicht gespeichert.";
        want_restart_cpr = "Möchten Sie die LSM-Phase erneut starten?";
        quit = "Beenden";
        reconnect = "Erneut verbinden";
        send = "Senden";
        confirm = "Bestätigen";
        cancel = "Abbrechen";
        yes = "Ja";
        no = "Nein";
        dont_save = "Nicht speichern";
        existing_name = "Bestehender Name";
        error = "Fehler";
        to_main = "Zu Hauptseite";
        bluetooth_off = "Bluetooth aus";
        disconnect = "Verbindung trennen";
        restart = "Erneut starten";
        search = "Suchen";
        network_error = "Verbindung getrennt";
        skip = "Überspringen";
        find_id_pw = "Haben Sie Ihre ID oder Ihr Kennwort vergessen?";
        exit = "Beenden";
//        bls_adult_1_title = "Erwachsenen-LSM-Fertigkeiten";
//        bls_adult_2_title = "Kompetenzen im Umgang mit dem Defibrillator";
//        bls_adult_2_check_1 = "Defibrillator und Beutel-Maske bringen";
//        bls_adult_2_check_2 = "Herzdruckmassage fortsetzen";
//        bls_adult_2_check_3 = "Umgebung des Patienten lüften";
//        bls_adult_2_check_4 = "Schock abgeben";
//        bls_adult_3_title = "Beutel-Masken-Beatmung";
//        bls_adult_3_check_1 = "Zyklus 1-30 Herzdruckmassagen durchführen";
//        bls_adult_3_check_2 = "Zyklus 2-30 Herzdruckmassagen durchführen";
//        bls_adult_3_check_3 = "Zyklus 1-2 Mal mit Beutel-Maske beatmen";
//        bls_adult_3_check_4 = "Zyklus 2-2 Mal mit Beutel-Maske beatmen";
//        bls_adult_sheet_title_1 = "1-Retter-Erwachsenen-LSM-Beurteilung";
//        bls_adult_sheet_check_1 = "Auf Reaktion prüfen (mindestens 5 Sekunden)";
//        bls_adult_sheet_check_2 = "Notfallrettung und Defibrillator anfordern";
//        bls_adult_sheet_check_3 = "Puls prüfen (nicht länger als 10 Sekunden)";
        bls_adult_sheet_check_4 = "HLW durchführen";
        bls_adult_sheet_check_4_1 = "Herzdruckmassagenzeit";
        bls_adult_sheet_check_4_2 = "Korrekte Herzdruckmassage";
        bls_adult_sheet_check_4_3 = "Zeit ohne Herzdruckmassage";
        bls_adult_sheet_check_4_4 = "Korrekte Atmung";
//        bls_adult_sheet_title_2 = "Defibrillator-Beurteilung und Wechsel 2. Retter";
//        bls_adult_sheet_check_5 = "2. Retter : Bringen Sie den Defibrillator und die Beutel-Maske, schalten Sie den Defibrillator ein und bringen Sie die Elektroden an";
//        bls_adult_sheet_check_6 = "1. Retter : Setzen Sie die Herzdruckmassage fort, bis der Defibrillator bereit ist ";
//        bls_adult_sheet_check_7 = "2. Retter : Anweisung, die Hände zur Analyse zu entfernen";
//        bls_adult_sheet_check_8 = "2. Retter : Wenn eine Defibrillation erforderlich ist, geben Sie die Anweisung, die Hände wegzunehmen und führen Sie die Defibrillation durch";
//        bls_adult_sheet_title_3 = "1. Retter-Beutel-Maske Beatmungsbeurteilung";
//        bls_adult_sheet_check_9 = "Führen Sie die HLW direkt nach der Schockabgabe durch";
//        bls_adult_sheet_9_cc = "2. Retter : 30 Herzdruckmassagen durchführen";
//        bls_adult_sheet_9_rp = "1. Retter : 2 Mal mit Beutel-Maske beatmen";
//        bls_infant_1_title = "1-Retter-Kleinkind-LSM-Fertigkeiten";
//        bls_infant_2_title = "HLW durchführen";
//        bls_infant_2_check_1 = "Fingerhaltung";
//        bls_infant_2_check_2 = "Herzdruckmassagenrate";
//        bls_infant_2_check_3 = "Tiefe der Herzdruckmassage";
//        bls_infant_2_check_4 = "Entlastung des Brustkorbs";
//        bls_infant_2_check_5 = "Aussetzung der Herzdruckmassage minimieren";
//        bls_infant_3_title = "2-Retter-HLW- und -Wechsel";
//        bls_infant_3_check_1 = "Rollen wechseln";
//        bls_infant_3_check_2 = "Zyklus 1-2 Mal mit Beutel-Maske beatmen";
//        bls_infant_3_check_3 = "Zyklus 2-2 Mal mit Beutel-Maske beatmen";
//        bls_infant_3_check_4 = "Zyklus 1-15 Herzdruckmassagen durchführen";
//        bls_infant_3_check_5 = "Zyklus 2-15 Herzdruckmassagen durchführen";
//        bls_infant_sheet_title_1 = "1-Retter-Kleinkind-LSM-Beurteilung";
//        bls_infant_sheet_check_1 = "Auf Reaktion prüfen (mindestens 5 Sekunden)";
//        bls_infant_sheet_check_2 = "Notfallrettung und Defibrillator anfordern";
//        bls_infant_sheet_check_3 = "Puls prüfen (nicht länger als 10 Sekunden)";
//        bls_infant_sheet_check_4 = "HLW durchführen";
//        bls_infant_sheet_check_4_1 = "Korrekte Fingerhaltung";
//        bls_infant_sheet_check_4_2 = "Ausreichende Rate (bis zu 18s)";
//        bls_infant_sheet_check_4_3 = "Ausreichende Tiefe";
//        bls_infant_sheet_check_4_4 = "Vollständige Entlastung des Brustkorbs zulassen";
//        bls_infant_sheet_check_4_5 = "Aussetzung der Herzdruckmassage minimieren (bis zu 10s)";
//        bls_infant_sheet_title_2 = "2-Retter-HLW- und -Wechsel";
//        bls_infant_sheet_check_5 = "Defibrillator und Beutel-Maske bringen und Rollen wechseln";
//        bls_infant_sheet_check_6 = "HLW durchführen";
//        bls_infant_sheet_check_6_1 = "2. Retter : 15 Herzdruckmassagen mit Zweidaumen-Technik durchführen (bis zu 9s)";
//        bls_infant_sheet_check_6_2 = "1. Retter : 2 Mal mit Beutel-Maske beatmen";
//        bls_infant_sheet_check_7 = "HLW durchführen";
//        bls_infant_sheet_check_7_1 = "1. Retter : 15 Herzdruckmassagen mit Zweidaumen-Technik durchführen (bis zu 9s)";
//        bls_infant_sheet_check_7_2 = "2. Retter : 2 Mal mit Beutel-Maske beatmen";
        response_info_title = "Bewusstsein bestätigen";
        response_info_txt = "Reaktionsfähigkeit des Patienten durch leichtes Rütteln an der Schulter überprüfen.";
        emergency_info_title = "Notdienstanforderung";
        emergency_info_txt = "Bitten Sie eine Person, den Notruf zu wählen.";
        pulsecheck_info_title = "Puls prüfen";
        pulsecheck_info_txt = "Nehmen Sie sich mindestens 5, aber nicht mehr als 10 Sekunden Zeit, den Puls zu fühlen. Wenn Sie keinen Puls fühlen, beginnen Sie mit der HLW.";
        compression_info_title = "Herzdruckmassage";
        compression_info_txt = "Führen Sie Herzdruckmassagen durch.";
        respiration_info_title = "Atem";
        respiration_info_txt = "Beatmen Sie.";
        aed_info_title = "AED";
        aed_info_1_txt = "Schalten Sie den Defibrillator vorab ein, wenn er betriebsbereit ist.";
        aed_info_2_txt = "Öffnen Sie das Hemd des Patienten oder durchschneiden Sie es und bringen Sie die Defibrillatorelektroden auf seiner Brust an.";
        aed_info_3_txt = "Stellen Sie sicher, dass das Opfer bei Bewusstsein ist und verabreichen Sie dem Opfer Schocks, indem Sie die Taste auf dem Defibrillator drücken.";
        aed_info_4_txt = "Nachdem Sie dem Opfer einen Schock verabreicht haben, müssen Sie die HLW sofort fortsetzen.";
        press_sensor_pad = "Setzen Sie die Herzdruckmassage fort und die HLW-Phase beginnt.";
        
        app_guide_address_trainer = "http://www.heartisense.com/pdf/Trainer_E.html";
        app_guide_address_monitor = "http://www.heartisense.com/pdf/Monitor_E.html";
        app_guide_address_test = "http://www.heartisense.com/pdf/Test_E.html";
        
        check_danger_info_title = "Auf Gefahr prüfen";
        check_danger_info_txt = "Prüfen Sie zunächst, ob das Opfer nicht an einem gefährlichen oder riskanten Ort liegt.";
        open_airway_info_title = "Luftweg freimachen";
        open_airway_info_txt = "Legen Sie Ihre Hand auf die Stirn des Opfers und neigen Sie dessen Kopf vorsichtig zurück; mit Ihren Fingerspitzen unter der Spitze des Kinns des Opfers, heben Sie das Kinn an, um den Luftweg zu öffnen.";
        check_breath_info_title = "Atem prüfen";
        check_breath_info_txt = "Nicht mehr als 10 Sekunden schauen, hören und fühlen, um zu ermitteln, ob das Opfer normal atmet.";
        
        check_danger = "Auf Gefahr prüfen";
        open_airway = "Luftweg freimachen";
        check_breath = "Atem prüfen";
    }
}
