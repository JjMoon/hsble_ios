//
//  ShlKr.swift
//  Test
//
//  Created by Jongwoo Moon on 2016. 1. 21..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation


class StrLocFrn3 : StrLocBase {
    override func setValues() {

        // iOS 에서 수정/추가한 스트링...
        select_menu = "Configuration";

        
        
        
        
        
        
        
        
        
        
        
        
        /// etc
        pause = "Pause";
        
        
        
        select_manikin_type = "Mannequin";
        select_manikin_dir = "Direction du mannequin";

        rescuer = "Sauveteur";

        date = "Date"

        quitArrow = "◀︎ Quitter";
        skipArrow = "Passer ▶︎"
        repeatKey = "Répéter" //////repeat = "Répéter";
        prev_gt = "◀︎ Précédent"
        complete_icon = "✓ Terminé";
        volume = "Volume"
        select_comp_rate_guide = "Guide de la taux de compression";
        select_date_format = "Format de date"

        // Score Option
        bls_setting = "Réglage BLS"
        total_score = "Score Total"
        reset_settings = "Restaurer les paramètres";

        stepbystep2line = "Pratique étape \npar étape"
        wholestep_2line = "Pratiquer l'ensemble \ndu processus"

        repeat_icon = "✎ Répéter";
        edit_icon = "✎ Modifier";

        compressionRatio = "Fraction des compressions"
        aed_infoTotalTxt = "Mettez le défibrillateur en marche à l'avance pour qu’il soit \nprêt à l'emploi." +
        "Ouvrez ou coupez la chemise du patient et appliquez les électrodes du \ndéfibrillateur sur sa poitrine.\n" +
        "Assurez-vous que la victime soit dégagée et donnez un choc à la victime en \nappuyant sur le bouton du défibrillateur." +
        "Une fois que vous avez donné un choc à la victime, vous devez continuer immédiatement la RCR."


        login = "Connexion";
        sending = "Envoi…";
        select_cpr_standard = "RCR Directives";
        select_language = "Langue";
        //select_mannequin = "Direction du mannequin";
        select_set_num = "Réglage de cycle";
        first_setting_txt = "Lors de l'exécution du programme pour la première fois, veuillez suivre les étapes de réglage initial et d'étalonnage.";
        scanning_kit = "Numérisation du kit…";
        select_kit = "Sélectionner un kit";
        calibration_start = "Démarrage de l'étalonnage";
        calibration = "Étalonnage";
        kit_id = "Identifiant du kit:";
        new_ = "Nouveau";
        load = "Charger";
        prev = "Précédent";
        chest_compression_calibration = "Étalonnage de la compression";
        breath_calibration = "Étalonnage de l'insufflation";
        write_calibration_name = "";
        preset = "";
        custom = "";
        current = "Actuel";
        compression_depth = "Profondeur de compression";
        breath_volume = "Volume d'insufflation";
        edit = "Modifier";
        save = "Enregistrer";
        apply = "Appliquer";
        delete = "Supprimer";
        set_breath = "Définir l'insufflation";
        set_compression = "Définir la compression";
        compression30 = "Livrer 15 compressions";
        adequate5 = "Prendre modérément cinq respirations profondes.";
        lay_rescuer = "Sauveteur novice";
        bls_rescuer = "Fournisseur de soins de santé";
        start_course = "Démarrer le cours";
        adult = "Adulte";
        infant = "Nourrisson";
        start_test = "Démarrer le test";
        manage_data = "Gestion de données";
        add_student = "Ajouter un étudiant";
        student_info = "Informations étudiant";
        all_check = "Cocher tout";
        all_uncheck = "Décocher tout";
        name = "Nom";
        email = "Email";
        view_record = "Voir les enregistrements";
        data_selected = "enregistrement est sélectionné";
        datas_selected = "enregistrements sont sélectionnés";
        send_data = "Envoyer les données";
        optional = "Options";
        register_kit = "Enregistrer un kit";
        heartisense_monitor = "HeartiSense Monitor";
        select_practice_mode = "Veuillez sélectionner le mode de pratique";
        step_by_step_mode_short = "Pratique étape par étape";
        whole_step_mode_short = "Pratiquer l'ensemble du processus";
        step_by_step_mode_full = "Pratique étape par étape";
        whole_step_mode_full = "Pratiquer l'ensemble du processus";
        adult_title = "Évaluation SIR adultes et DEA";
        infant_title = "Évaluation SIR nourrisson";
        all_title = "Évaluation SIR et DEA";
        assign_student = "Assigner un étudiant";
        connect_kit = "Connecter un kit";
        change_kit_1 = "Changer de kit";
        change_kit_2 = "Changer de kit";
        connecting_kit = "Connexion du kit...";
        searching_kit = "Recherche du kit...";
        compression_pad = "Coussinet de compression";
        breath_module = "Module d'insufflation";
        and = " et ";
        pass = "Réussite";
        fail = "Échec";
        cycle = "Cycle";
        ready = "Prêt";
        scenario_description = "Description scénario";
        response = "Réponse";
        emergency = "Urgence";
        check_pulse = "Contrôler le pouls";
        compression = "Compression";
        breath = "Insufflation";
        cpr = "RCR";
        aed = "DEA";
        precpr_aha_bls = "Réponse\nUrgence\nContrôlez le souffle\nContrôler le pouls";
        precpr_aha_lay = "Réponse\nUrgence\nContrôlez le souffle";
        precpr_erc = "Vérifier un danger\nRéponse\nOuvrez les voies respiratoires\nContrôlez le souffle\nUrgence";
        precpr_anzcor = "Vérifier un danger\nRéponse\nUrgence\nOuvrez les voies respiratoires\nContrôlez le souffle";
        disconnected = "Déconnecté";
        connected = "Connecté";
        hands_off_time = "Durée d'interruption";
        second = "s";
        minute = "min";
        time = "Durée";
        timer = "Durée";
        position = "Position";
        depth = "Profondeur";
        rate = "Taux";
        count = "Calcul";
        no_named = "Pas de nom";
        save_record = "Sauvegarder les enregistrements";
        save_warning = "*Veuillez noter que tous les enregistrements précédents seront remplacés par les nouveaux enregistrements.";
        save_exist = "Des enregistrements existent";
        step = "Étape";
        result = "Résultat";
        auto_connect = "Connexion automatique";
        result_guide = "* Nombre de réussites / Nombre de tentatives";
        save_all = "Enregistrer tout";
        saved = "Enregistré";
        close = "Fermer";
        view_detail = "Afficher les détails";
        not_assigned = "non affecté";

        ///repeat = "Répéter";
        
        complete = "Terminé";
        exist = "Existe";
        data = "Données";
        student_name = "Nom de l'étudiant";
        select = "Sélectionner";
        assign_result = "Affectation";
        detail = "Détail";
        graph = "Graphique";
        group = "Groupe";
        compression_rate = "Taux de compression";
        compression_position = "Position de compression";
        duration = "Durée";
        average_rate = "Taux moyen";
        recoil = "Relaxation";
        good_depth = "Bon";
        strong_depth = "Fort";
        fast = "Rapide";
        good_rate = "Bon";
        slow = "Lent";
        excessive = "Trop fort";
        good_amount = "Bon";
        insufficient = "Faible";
        recoil_needed = "Relaxation nécessaire";
        too_weak = "Trop faible";
        wrong_position = "Mauvaise position";
        option_calibration = "    Étalonnage";
        option_preference = "    Configuration initiale";
        not_valid_email = "L'adresse e-mail n'est pas d'une forme valide.";
        login_fail = "Adresse e-mail ou mot de passe incorrect.";
        not_assigned_inst_id = "L'identifiant que vous avez saisi n'est pas attribué à l'institution.";
        server_error = "Une erreur est survenue lors de l'envoi des données au serveur. Si cette erreur persiste, veuillez nous contacter.";
        no_license = "L'identifiant que vous avez saisi n'a pas de licence.";
        license_expiration = "La durée de la licence de l'identifiant que vous avez saisi a expiré.";
        license_data_limit = "Le numéro des données de la licence de l'identifiant que vous avez saisi a expiré.";
        connect_fail = "Impossible de se connecter avec le kit.";
        connect_success = "La connexion avec le kit est réussie.";
        disconnect_kit = "Connexion perdue.";
        using_data_calibration = "Vous ne pouvez pas supprimer cette valeur de configuration car elle est en cours d'utilisation.";
        sensor_error_detected = "";
        sensor_error_detected_m = "";
        sensor_not_connected = " n'est pas connecté.";
        sensor_not_connected_m = " ne sont pas connectés.";
        at_least_1_connect = "Le kit n'est pas connecté.";
        not_permission = "Vous ne pouvez pas utiliser ce programme avec ce kit HeartiSense. Veuillez vérifier l'étiquette du kit.";
        empty_name_or_email = "Saisissez vos nom et adresse e-mail.";
        all_check_need = "Vous ne pouvez pas enregistrer les données jusqu'à ce que tous les stagiaires soient évalués.";
        not_valid_name = "Nom invalide. (Utilisez des lettres de a à z, de A à Z, chiffres de 0 à 9, caractères coréens, -. et _.)";
        search_the_kit = "Recherche automatique du kit";
        network_disconnected = "Vous n'êtes pas connecté à un réseau";
        bt_off_msg = "Toutes les connexions Bluetooth vont être déconnectées.";
        need_connect_kit = "Le kit doit être connecté.";
        start_single_mode = "Commencer par le mode d'apprentissage individuel.";
        start_monitor_mode = "Commencer par le mode de contrôle.";
        bls_using = "Le test HeartiSense est en cours d'utilisation. Ce programme se ferme.";
        calibration_using = "L'étalonnage est utilisé dans le programme de l'enseignant. Ce programme va se fermer.";
        wait_instructor = "Veuillez attendre les instructions de l'instructeur.";
        search_fail = "Échec de recherche du kit. Vérifiez le bouton d'alimentation du kit.";
        want_connect_retry = "La connexion au kit a échoué.";
        want_quit = "Êtes-vous sûr de vouloir fermer le programme ?";
        want_back_calibration = "Êtes-vous sûr de vouloir revenir à l'étape précédente ? La valeur d'étalonnage que vous avez créée ne sera pas enregistrée.";
        want_send = "Êtes-vous sûr de vouloir envoyer les enregistrements sélectionnés au serveur ?";
        want_delete = "Êtes-vous sûr de vouloir supprimer les enregistrements sélectionnés ?";
        want_main = "Êtes-vous sûr de vouloir retourner au menu principal ?";
        want_back = "Êtes-vous sûr de vouloir revenir à l'étape précédente ?";
        want_turn_on_bluetooth = "Voulez-vous activer le Bluetooth ?";
        want_cali_main = "Sauvegarde terminée. Voulez-vous passer à la page principale d'étalonnage ?";
        want_disconnect = "Voulez-vous vous déconnecter ?";
        want_restart_training = "Voulez-vous reprendre la formation ?";
        want_restart_test = "Voulez-vous reprendre le test ?";
        want_start_stepbystep_mode = "Voulez-vous commencer la pratique étape par étape ?";
        want_start_wholestep_mode = "Voulez-vous commencer la pratique de l'ensemble du processus ?";
        want_delete_kit = "Voulez-vous supprimer le kit enregistré et passer en mode manuel ?";
        want_assign_completed = "L'attribution est-elle complète ? Les données des élèves non affectés ne seront pas sauvegardées.";
        want_restart_cpr = "Voulez-vous redémarrer la phase de RCR ?";
        quit = "Quitter";
        reconnect = "Reconnexion";
        send = "Envoyer";
        confirm = "Confirmer";
        cancel = "Annuler";
        yes = "Oui";
        no = "Non";
        dont_save = "Ne pas sauvegarder";
        existing_name = "Nom existant";
        error = "Erreur";
        to_main = "Vers principal";
        bluetooth_off = "Bluetooth désactivé";
        disconnect = "Déconnexion";
        restart = "Redémarrer";
        search = "Chercher";
        network_error = "Déconnecté";
        skip = "Passer";
        find_id_pw = "Vous avez oublié votre identifiant / mot de passe ?";
        exit = "Quitter";
//        bls_adult_1_title = "Compétences en SIR sur adulte";
//        bls_adult_2_title = "Compétences sur DEA";
//        bls_adult_2_check_1 = "Apportez un DEA et un sac respiratoire";
//        bls_adult_2_check_2 = "Continuer la compression";
//        bls_adult_2_check_3 = "Ventiler l'environnement du patient";
//        bls_adult_2_check_4 = "Délivrer un choc";
//        bls_adult_3_title = "Ventilation par sac respiratoire";
//        bls_adult_3_check_1 = "Cycle 1-Donner 30 compressions";
//        bls_adult_3_check_2 = "Cycle 2-Donner 30 compressions";
//        bls_adult_3_check_3 = "Cycle 1-Donner 2 insufflations avec le sac respiratoire";
//        bls_adult_3_check_4 = "Cycle 2-Donner 2 insufflations avec le sac respiratoire";
//        bls_adult_sheet_title_1 = "Évaluation SIR sur adulte 1 sauveteur";
//        bls_adult_sheet_check_1 = "Contrôles de réponse (au moins 5 secondes)";
//        bls_adult_sheet_check_2 = "Demandez des secours d'urgence et un DEA";
//        bls_adult_sheet_check_3 = "Contrôle du pouls (pas plus de 10 secondes)";
        bls_adult_sheet_check_4 = "Effectuer une RCR";
        bls_adult_sheet_check_4_1 = "Temps de compression";
        bls_adult_sheet_check_4_2 = "Compression correcte";
        bls_adult_sheet_check_4_3 = "Temps de compression suspendu";
        bls_adult_sheet_check_4_4 = "Respiration correcte";
//        bls_adult_sheet_title_2 = "Évaluation DEA 2ème sauveteur et permutation";
//        bls_adult_sheet_check_5 = "2ème sauveteur : Apportez le DEA et le sac respiratoire, mettez le DEA en marche et attachez les coussinets";
//        bls_adult_sheet_check_6 = "1er sauveteur : Continuez la compression jusqu'à ce que le DEA soit prêt ";
//        bls_adult_sheet_check_7 = "2ème sauveteur : Demandez une interruption pour analyse";
//        bls_adult_sheet_check_8 = "2ème sauveteur : Si une défibrillation est nécessaire, demandez une interruption et procédez à la défibrillation";
//        bls_adult_sheet_title_3 = "Évaluation de la ventilation par sac respiratoire du 1er sauveteur";
//        bls_adult_sheet_check_9 = "Effectuez une RCR immédiatement après avoir délivré le choc";
//        bls_adult_sheet_9_cc = "2ème sauveteur : Donner 30 compressions";
//        bls_adult_sheet_9_rp = "1er sauveteur : Donner 2 insufflations avec le sac respiratoire";
//        bls_infant_1_title = "Compétences en SIR sur nourrisson du 1er sauveteur";
//        bls_infant_2_title = "Effectuer une RCR";
//        bls_infant_2_check_1 = "Placement du doigt";
//        bls_infant_2_check_2 = "Taux de compression";
//        bls_infant_2_check_3 = "Profondeur de compression";
//        bls_infant_2_check_4 = "Relaxation thoracique";
//        bls_infant_2_check_5 = "Réduire la suspension de la compression";
//        bls_infant_3_title = "RCR 2ème sauveteur et permutation";
//        bls_infant_3_check_1 = "Échanger les rôles";
//        bls_infant_3_check_2 = "Cycle 1-Donner 2 insufflations avec le sac respiratoire";
//        bls_infant_3_check_3 = "Cycle 2-Donner 2 insufflations avec le sac respiratoire";
//        bls_infant_3_check_4 = "Cycle 1-Donner 15 compressions";
//        bls_infant_3_check_5 = "Cycle 2-Donner 15 compressions";
//        bls_infant_sheet_title_1 = "Évaluation en SIR sur nourrisson du 1er sauveteur";
//        bls_infant_sheet_check_1 = "Contrôles de réponse (au moins 5 secondes)";
//        bls_infant_sheet_check_2 = "Demandez des secours d'urgence et un DEA";
//        bls_infant_sheet_check_3 = "Contrôle du pouls (pas plus de 10 secondes)";
//        bls_infant_sheet_check_4 = "Effectuer une RCR";
//        bls_infant_sheet_check_4_1 = "Placement correct du doigt";
//        bls_infant_sheet_check_4_2 = "Taux adéquat (jusqu'à 18 s)";
//        bls_infant_sheet_check_4_3 = "Profondeur adéquate";
//        bls_infant_sheet_check_4_4 = "Autoriser une relaxation thoracique complète";
//        bls_infant_sheet_check_4_5 = "Réduire la suspension de compression (jusqu'à 10s)";
//        bls_infant_sheet_title_2 = "RCR 2ème sauveteur et permutation";
//        bls_infant_sheet_check_5 = "Apportez le DEA et le sac respiratoire puis inversez les rôles";
//        bls_infant_sheet_check_6 = "Effectuer une RCR";
//        bls_infant_sheet_check_6_1 = "2ème sauveteur : Donnez 15 compressions à l'aide de la technique d'encerclement avec appui des 2 pouces (jusqu'à 9s)";
//        bls_infant_sheet_check_6_2 = "1er sauveteur : Donner 2 insufflations avec le sac respiratoire";
//        bls_infant_sheet_check_7 = "Effectuer une RCR";
//        bls_infant_sheet_check_7_1 = "1er sauveteur : Donnez 15 compressions à l'aide de la technique d'encerclement avec appui des 2 pouces (jusqu'à 9s)";
//        bls_infant_sheet_check_7_2 = "2ème sauveteur : Donner 2 insufflations avec le sac respiratoire";
        response_info_title = "Confirmer la sensibilisation";
        response_info_txt = "Confirmez la réactivité du patient en secouant doucement son épaule.";
        emergency_info_title = "Demande de service d'urgence";
        emergency_info_txt = "Choisissez une personne autour de vous et demandez-lui d'appeler le 112.";
        pulsecheck_info_title = "Contrôler le pouls";
        pulsecheck_info_txt = "Prenez au moins 5 mais pas plus de 10 secondes pour sentir le pouls. Si vous ne vous sentez pas le pouls, commencez la réanimation.";
        compression_info_title = "Compression";
        compression_info_txt = "Effectuez des compressions.";
        respiration_info_title = "Insufflation";
        respiration_info_txt = "Donnez des insufflations.";
        aed_info_title = "DEA";
        aed_info_1_txt = "Mettez le défibrillateur en marche à l'avance pour qu’il soit prêt à l'emploi.";
        aed_info_2_txt = "Ouvrez ou coupez la chemise du patient et appliquez les électrodes du défibrillateur sur sa poitrine.";
        aed_info_3_txt = "Assurez-vous que la victime soit dégagée et donnez un choc à la victime en appuyant sur le bouton du défibrillateur.";
        aed_info_4_txt = "Une fois que vous avez donné un choc à la victime, vous devez continuer immédiatement la RCR.";
        press_sensor_pad = "Procédez à la compression et le stade de la RCR va commencer.";
        
        app_guide_address_trainer = "http://www.heartisense.com/pdf/Trainer_E.html";
        app_guide_address_monitor = "http://www.heartisense.com/pdf/Monitor_E.html";
        app_guide_address_test = "http://www.heartisense.com/pdf/Test_E.html";
        
        check_danger_info_title = "Vérifier un danger";
        check_danger_info_txt = "Tout d'abord, vérifiez si la victime n'est pas présente dans un endroit dangereux ou risqué.";
        open_airway_info_title = "Ouvrez les voies respiratoires";
        open_airway_info_txt = "Placez votre main sur son front et inclinez doucement sa tête en arrière ; avec vos doigts sous la pointe du menton de la victime, levez le menton pour ouvrir les voies respiratoires.";
        check_breath_info_title = "Contrôlez le souffle";
        check_breath_info_txt = "Regardez, écoutez et sentez pas plus de 10 secondes pour déterminer si la victime respire normalement.";
        
        check_danger = "Vérifier un danger";
        open_airway = "Ouvrez les voies respiratoires";
        check_breath = "Contrôlez le souffle";
    }
}
