//
//  ShlKr.swift
//  Test
//
//  Created by Jongwoo Moon on 2016. 1. 21..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation


class StrLocTrainerEng0: StrLocTrainerBase {

}





class StrLocTrainerKor1 : StrLocTrainerBase {
    override func setValues() {
        /* Login */
        login = "로그인";

        /* Sending */
        sending = "전송중…";

        /* Setting */
        select_language = "언어";
        select_mannequin = "마네킹 방향";
        select_set_num = "주기 설정";
        first_setting_txt = "최초 실행 시, 반드시 초기설정과 감도설정을 실시해주세요.";

        /* Bluetooth List */
        scanning_kit = "키트 검색중…";
        select_kit = "키트 선택";

        /* Calibration */
        calibration_start = "감도설정 시작";
        calibration = "감도설정";
        kit_id = "키트 아이디: ";
        new_ = "새로 만들기";
        load = "불러오기";
        prev = "이전";
        chest_compression_calibration = "압박 감도설정";
        breath_calibration = "호흡 감도설정";
        write_calibration_name = "감도조절 이름 입력";
        preset = "프리셋";
        custom = "커스텀";
        current = "사용중";
        compression_depth = "압박 깊이";
        breath_volume = "호흡량";
        edit = "수정";
        // edit_title = "수정하기";
        save = "저장";
        apply = "적용";
        deleteString = "삭제";
        chest_compression_calibration_manual_info = "5cm 만큼 압박을 하세요. 화살표가 노란색 영역에 있다면 물방울을 왼쪽으로 옮기고, 화살표가 빨간색 영역에 있다면 물방울을 오른쪽으로 옮기세요.";
        breath_calibration_manual_info = "적당량의 호흡을 하세요. 화살표가 노란색 영역에 있다면 물방울을 왼쪽으로 옮기고, 화살표가 빨간색 영역에 있다면 물방울을 오른쪽으로 옮기세요.";
        set_breath = "호흡 설정";
        set_compression = "압박 설정";
        compression30 = "가슴을 30번 압박";
        adequate5 = "적당한 양으로 5회 호흡";

        /* Main */
        adult = "성인";
        infant = "유아";
        start_test = "평가 시작";
        manage_data = "학생 정보 관리";

        /* Manage Student */
        add_student = "학생 추가";
        student_info = "학생 정보";
        all_check = "모두 선택";
        all_uncheck = "모두 해제";
        name = "이름";
        email = "이메일";
        view_record = "데이터 보기";
        data_selected = "개의 데이터 선택됨";
        datas_selected = "개의 데이터 선택됨";
        send_data = "데이터 전송";
        optional = "선택사항";

        /* Test */
        register_kit = "키트 등록";
        adult_title = "성인 BLS 및 AED 평가";
        infant_title = "영아 BLS 평가";
        all_title = "BLS 및 AED 평가";
        assign_student = "학생 배정";
        connect_kit = "키트 연결";
        change_kit = "키트\n변경";
        connecting_kit = "키트 연결중…";
        compression_pad = "압박 패드";
        breath_module = "호흡 모듈";
        and = "와 ";
        pass = "성공";
        fail = "실패";
        cycle = "주기";
        ready = "준비";
        response = "의식 확인";
        emergency = "구조 요청";
        check_pulse = "맥박 확인";
        compression = "압박";
        breath = "호흡";
        disconnected = "연결안됨";
        connected = "연결됨";
        second = "초";
        minute = "분";
        no_named = "이름 없음";
        time = "시간";
        count = "횟수";
        save_record = "데이터 저장하기";
        save_warning = "*기존 데이터이 있는 경우 새 데이터이 덮어쓰기 되오니 주의하세요";
        save_exist = "데이터 있음";
        step = "단계";
        result = "결과";
        auto_connect = "자동 연결";
        save_all = "모두 저장";
        saved = "저장됨";
        close = "닫기";
        not_assigned = "배정되지 않음";

        /* Assign */
        repeatString = "다시하기";
        complete = "완료";
        student_name = "학생 이름";
        select = "선택";
        assign_result = "배정 결과";

        /* Detail */
        detail = "상세 그래프";
        // result_page = "결과 페이지";
        graph = "그래프";
        group = "그룹";
        compression_rate = "압박 속도";
        compression_position = "압박 위치";
        duration = "소요시간";
        average_rate = "평균 속도";
        recoil = "이완";
        good_depth  = "적정깊이";
        fast = "빠름";
        good_rate = "적정속도";
        slow = "느림";
        excessive = "지나침";
        good_amount = "적정량";
        insufficient = "부족함";
        hands_off_time = "압박 중단 시간";
        recoil_needed = "이완 필요";
        too_weak = "압박 부족";
        wrong_position = "잘못된 위치";

        /* Option */
        option_calibration = "    감도 설정";
        option_preference = "    초기 설정";

        /* Message */
        not_valid_email = "이메일 주소의 형식이 유효하지 않습니다!";
        login_fail = "입력하신 이메일 주소 혹은 비밀번호가 일치하지 않습니다!";
        connect_fail = "연결에 실패하였습니다!";
        connect_success = "연결에 성공하였습니다!";
        disconnect_kit = "연결이 끊어졌습니다!";
        using_data_calibration = "이 설정값은 사용중이므로 삭제할 수 없습니다.";
        sensor_error_detected = "에서 에러가 발견되었습니다.";
        sensor_error_detected_m = "에서 에러가 발견되었습니다.";
        sensor_not_connected = "이(가) 연결되지 않았습니다.";
        sensor_not_connected_m = "이(가) 연결되지 않았습니다.";
        at_least_1_connect = "키트가 연결되지 않았습니다.";
        not_permission = "이 프로그램은 해당 하티센스 키트에서 사용 할 수 없습니다! 키트의 라벨을 확인해주세요.";
        empty_name_or_email = "이름과 이메일을 모두 입력해주세요.";
        all_check_need = "모든 학생의 평가가 끝나야 데이터 저장을 할 수 있습니다.";
        not_valid_name = "이름이 유효하지 않습니다!(a~z, A~Z, 0~9, 한글, -, _ 만 사용가능)";
        searching_kit = "자동으로 키트를 검색중입니다.";
        network_disconnected = "네트워크에 연결되어 있지 않습니다.";
        bt_off_msg = "모든 블루투스 연결이 끊어집니다.";

        /* Question */
        want_quit = "프로그램을 종료하시겠습니까?";
        want_back_calibration = "이전으로 돌아가시겠습니까? 새로 만들면 설정값은 저장되지 않습니다.";
        want_delete_calibration = "이 캘리브레이션 데이터를 삭제하시겠습니까?";
        want_save_calibration = "이전으로 돌아가시겠습니까? 수정하면 설정값은 저장되지 않습니다.";
        want_overwrite_calibration = "중복된 이름을 가진 설정값이 있습니다. 덮어쓰시겠습니까?";
        want_send = "선택된 데이터를 서버에 전송하시겠습니까?";
        want_delete = "선택된 데이터를 삭제하시겠습니까?";
        want_main = "메인으로 돌아가시겠습니까?";
        want_back = "이전으로 돌아가시겠습니까?";
        want_turn_on_bluetooth = "블루투스를 켜시겠습니까?";
        want_cali_main = "저장이 완료되었습니다. 감도 설정 메인 페이지로 이동하시겠습니까?";
        want_disconnect = "연결을 종료하시겠습니까?";
        want_restart_course = "평가를 다시 시작하겠습니까?";
        want_delete_kit = "등록된 키트를 제거하고 수동모드로 전환하시겠습니까?";
        want_assign_completed = "배정을 완료하시겠습니까? 배정하지 않은 학생의 데이터는 저장되지 않습니다.";
        want_restart_cpr = "CPR 단계를 다시 시작하겠습니까?";

        /* Dialog */
        quit = "종료";     quitArrow = "◀︎ 종료";
        send = "전송";
        delete = "삭제";
        confirm = "확인";
        cancel = "취소";
        yes = "예";
        no = "아니요";
        dont_save = "저장하지 않음";
        existing_name = "중복된 이름";
        error = "오류";
        to_main = "메인으로";
        bluetooth_off = "블루투스 꺼짐";
        disconnect = "연결 끊기";
        restart = "재시작";
        search = "검색";
        network_error = "네트워크 에러";
        skip = "넘기기";
        find_id_pw = "아이디/비밀번호 찾기";
        exit = "나가기";

        /* Test Adult */
        bls_adult_1_title = "BLS";
        bls_adult_2_title = "AED 평가";
        bls_adult_2_check_1 = "AED와 백마스크 가져오기";
        bls_adult_2_check_2 = "압박 지속하기";
        bls_adult_2_check_3 = "환자 주변 환기";
        bls_adult_2_check_4 = "제세동 시행";
        bls_adult_3_title = "백마스크 호흡";
        bls_adult_3_check_1 = "1주기 - 압박 30회";
        bls_adult_3_check_2 = "2주기 - 압박 30회";
        bls_adult_3_check_3 = "1주기 -백마스크 호흡 2회 ";
        bls_adult_3_check_4 = "2주기 -백마스크 호흡 2회 ";
        bls_adult_sheet_title_1 = "1인 구조자 성인 BLS 평가";
        bls_adult_sheet_check_1 = "환자의 의식 확인 (10초 이내)";
        bls_adult_sheet_check_2 = "응급 구조 및 AED 요청";
        bls_adult_sheet_check_3 = "맥박확인 (10초 이내)";
        bls_adult_sheet_check_4 = "심폐소생술 시행";
        bls_adult_sheet_check_4_1 = "압박 시간";
        bls_adult_sheet_check_4_2 = "정확한 압박";
        bls_adult_sheet_check_4_3 = "압박 중단 시간";
        bls_adult_sheet_check_4_4 = "정확한 호흡";
        bls_adult_sheet_title_2 = "두 번째 구조자 AED 평가 및 교대";
        bls_adult_sheet_check_5 = "두 번째 구조자 : AED와 백마스크 장비 가져오기, AED 전원을 켜고 패드 부착";
        bls_adult_sheet_check_6 = "첫 번째 구조자 : AED 준비시간 동안 압박 지속";
        bls_adult_sheet_check_7 = "두 번째 구조자 : 분석을 위하여 손을 떼도록 지시";
        bls_adult_sheet_check_8 = "두 번째 구조자 : 제세동이 필요하다면, 손을 떼도록 지시하고 제세동 시행";
        bls_adult_sheet_title_3 = "첫 번째 구조자의 백마스크 장비를 이용한 호흡 능력 평가";
        bls_adult_sheet_check_9 = "제세동 시행 후 즉시 심폐소생술 재개";
        bls_adult_sheet_9_cc = "두 번째 구조자 : 30회 가슴압박 시행";
        bls_adult_sheet_9_rp = "첫 번째 구조자 : 백마스크 장비를 이용하여 호흡 2회 실시";

        /* Test Infant */
        bls_infant_1_title = "1인 구조자 BLS";
        bls_infant_2_title = "CPR 시행";
        bls_infant_2_check_1 = "압박 위치";
        bls_infant_2_check_2 = "압박 속도";
        bls_infant_2_check_3 = "압박 깊이";
        bls_infant_2_check_4 = "가슴 이완";
        bls_infant_2_check_5 = "압박 중단 최소화";
        bls_infant_3_title = "2인 구조자 BLS";
        bls_infant_3_check_1 = "역할 교대";
        bls_infant_3_check_2 = "1주기 - 백마스크 호흡 2회";
        bls_infant_3_check_3 = "2주기 - 백마스크 호흡 2회";
        bls_infant_3_check_4 = "1주기 - 압박 15회";
        bls_infant_3_check_5 = "2주기 - 압박 15회";
        bls_infant_sheet_title_1 = "1인 구조자 영아 BLS 평가";
        bls_infant_sheet_check_1 = "환자의 의식 확인 (10초 이내)";
        bls_infant_sheet_check_2 = "응급 구조 및 AED 요청";
        bls_infant_sheet_check_3 = "맥박확인 (10초 이내)";
        bls_infant_sheet_check_4 = "심폐소생술 시행";
        bls_infant_sheet_check_4_1 = "정확한 압박 위치";
        bls_infant_sheet_check_4_2 = "적절한 압박 속도(18초 이내)";
        bls_infant_sheet_check_4_3 = "적절한 압박 깊이";
        bls_infant_sheet_check_4_4 = "적절한 가슴 이완";
        bls_infant_sheet_check_4_5 = "압박 중단 최소화(10초 이내)";
        bls_infant_sheet_title_2 = "2인 구조자 영아 BLS 평가 및 교대";
        bls_infant_sheet_check_5 = "두 번째 구조자 : AED와 백마스크 장비 가져오기, 역할 교대";
        bls_infant_sheet_check_6 = "심폐소생술 시행";
        bls_infant_sheet_check_6_1 = "두 번째 구조자 : 2 thumb-encircling hands 방법으로 15회 압박 (9초 이내)";
        bls_infant_sheet_check_6_2 = "첫 번째 구조자 : 백마스크 장비를 이용하여 호흡 2회 실시";
        bls_infant_sheet_check_7 = "심폐소생술 시행";
        bls_infant_sheet_check_7_1 = "첫 번째 구조자 : 2 thumb-encircling hands 방법으로 15회 압박 (9초 이내)";
        bls_infant_sheet_check_7_2 = "두 번째 구조자 : 백마스크 장비를 이용하여 호흡 2회 실시";
    }
}
