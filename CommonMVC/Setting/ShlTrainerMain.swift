//
//  ShlTestMain.swift
//  Test
//
//  Created by Jongwoo Moon on 2016. 1. 21..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation

class Shl: NSObject { // SinHooLanguage ..
    var arrLangStr = [StrLocTrainerBase]()
    var obj: StrLocTrainerBase {
        get { return arrLangStr[Int(langIdx)] }
    }
    func setStrings() {
    }
}

class ShlTrainer : Shl {
    override init() {
        super.init()
        print("\n\n\n  ShlTest : init \n\n\n")
        self.setStrings()
    }

    override func setStrings() {
        print("\n\n\n  ShlMonitor : setStrings  \n\n\n")
        arrLangStr.append(StrLocTrainerEng0())
        arrLangStr.append(StrLocTrainerKor1())
        arrLangStr[1].setValues()
    }

}

