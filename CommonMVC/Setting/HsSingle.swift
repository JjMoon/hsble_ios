//
//  HsSingle.swift
//  HS Test
//
//  Created by Jongwoo Moon on 2016. 10. 5..
//  Copyright © 2016년 IMLab. All rights reserved.
//


import Foundation
import UIKit


private let _singletoneObj = Stt()

/// auth
enum Identity { case None; case Teacher; case Parent; case Student }

enum ViewState { case Init; case Main; case Auth; case Setting }


class Stt: NSObject {
    /// singletone obj : Stt.singl
    class var singl: Stt { return _singletoneObj }

    override init() {
        super.init()

        //initialJob()

    }
    /// UI, View
    var uiScrWid: CGFloat = UIScreen.mainScreen().bounds.width, uiScrHigh: CGFloat = UIScreen.mainScreen().bounds.height
    var bttnOff: CGFloat = 30, bttnYOff: CGFloat = 10, bttnVwHeight: CGFloat = 70

    var tableSectionHeight: CGFloat = 40

}

