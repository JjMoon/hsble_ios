//
//  ShlKr.swift
//  Test
//
//  Created by Jongwoo Moon on 2016. 1. 21..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation


class StrLocEng0: StrLocBase {

}


class StrLocKor1 : StrLocBase {
    override func setValues() {

        // 아직 미정인 것들..
        no_data_to_send = "전송 가능한 데이터가 없습니다."
        cali_comp_explain = "+를 누르면 바가 더 많이 움직이고, -를 누르면 바가 덜 움직입니다."
        low_batery_kit = "키트의 배터리가 없습니다. 배터리를 교체해주세요."






        /// etc
        pause = "일시정지";
        select_old_records_send = "서버에 전송하지 않은 예전 테스트 데이터가 있습니다. 확인하시려면 데이터를 서버로 전송해야합니다. 서버로 전송 하시겠습니까?"
        
    
        /// Test 변경 관련 스트링.
        needs_remediation = "재교육";
        pass_nr_info = "위의 결과를 확인하고 통과/재교육을 선택해주세요.";

        bls_adult_1_title = "평가 및 가동" // BLS";
        bls_adult_1_check_danger = "안전 확인";
        bls_adult_1_check_response = "의식 확인";
        bls_adult_1_shouts_for_help = "도움 요청 / 응급의료체제 가동 / AED 요청";
        bls_adult_1_check_pulse = "맥박 확인";
        bls_adult_1_open_airway = "기도 확보";
        bls_adult_1_check_breath = "호흡 확인";

        bls_adult_2_title = "자동제세동기";
        bls_adult_2_check_1 = "자동제세동기의 전원을 켬";
        bls_adult_2_check_2 = "패드를 올바르게 부착";
        bls_adult_2_check_3 = "분석을 위해 환자에게서 물러남";
        bls_adult_2_check_4 = "안전한 제세동 전달을 위해 환자에게서 물러남";
        bls_adult_2_check_5 = "안전하게 제세동 전달";

        bls_adult_3_title = "가슴압박 재개";
        bls_adult_3_check_1 = "제세동 전달 후 곧바로 가슴 압박 재개";

        /// 성인 결과지
        bls_adult_sheet_title_1 = "평가 및 가동";
        bls_adult_sheet_title_2 = "심폐소생술 시행";
        bls_adult_sheet_cpr_ct = "압박 시간";
        bls_adult_sheet_cpr_cc = "정확한 압박";
        bls_adult_sheet_cpr_hot = "압박 중단 시간";
        bls_adult_sheet_cpr_rp = "정확한 호흡";
        bls_adult_sheet_title_3 = "자동제세동기";
        bls_adult_sheet_aed1 = "자동제세동기의 전원을 켬";
        bls_adult_sheet_aed2 = "패드를 올바르게 부착";
        bls_adult_sheet_aed3 = "분석을 위해 환자에게서 물러남";
        bls_adult_sheet_aed4 = "안전한 제세동 전달을 위해 환자에게서 물러남";
        bls_adult_sheet_aed5 = "안전하게 제세동 전달";
        bls_adult_sheet_title_4 = "가슴압박 재개";
        bls_adult_sheet_cont_cpr = "제세동 시행 후 즉시 심폐소생술 재개";

        bls_infant_1_title = "평가 및 가동";
        bls_infant_1_check_danger = "안전 확인";
        bls_infant_1_check_response = "의식 확인";
        bls_infant_1_shouts_for_help = "도움 요청 / 응급의료체제 가동 / AED 요청";
        bls_infant_1_check_pulse = "맥박 확인";
        bls_infant_1_open_airway = "기도 확보";
        bls_infant_1_check_breath = "호흡 확인";
        bls_infant_2_title = "심폐소생술 시행";
        bls_infant_2_check_1 = "1주기 - 압박";
        bls_infant_2_check_2 = "1주기 - 호흡";

        bls_infant_2_check_3 = "2주기 - 압박";
        bls_infant_2_check_4 = "2주기 - 호흡";
        bls_infant_2_check_5 = "2주기 - 압박 중단 시간";
        bls_infant_3_title = "심폐소생술 시행";
        bls_infant_3_check_1 = "3주기 - 압박";
        bls_infant_3_check_2 = "4주기 - 백마스크 호흡";
        bls_infant_sheet_title_1 = "평가 및 가동";
        bls_infant_sheet_title_2 = "심폐소생술 시행";
        bls_infant_sheet_cpr_cc = "압박";
        bls_infant_sheet_cpr_rp = "호흡";
        bls_infant_sheet_cpr_hot = "압박 중단 시간";
        bls_infant_sheet_cpr_bvm_rp = "백마스크 호흡";




        // iOS 에서 수정/추가한 스트링...
        select_menu = "설정";
        //quit = "종료";
        select_manikin_type = "마네킹";
        select_manikin_dir = "마네킹 방향";

        rescuer = "구조자";

        date = "날짜"
        
        quitArrow = "◀︎ 종료";
        skipArrow = "넘기기 ▶︎"
        repeatKey = "다시하기";
        prev_gt = "◀︎ 이전"
        complete_icon = "✓ 완료";
        repeat_icon = "✎ 다시하기";
        edit_icon = "✎ 수정";
        volume = "양"
        select_comp_rate_guide = "압박 속도 가이드"
        select_date_format = "날짜 형식"

        // Score Option
        bls_setting = "BLS 설정"
        total_score = "총점"
        reset_settings = "설정 초기화"

        compressionRatio = "압박 실시율"
        productTypeErrorExit = "제품을 사용할 수 없습니다. !!!";

        stepbystep2line = "단계별 연습"
        wholestep_2line = "전과정 실습"

        aed_infoTotalTxt = "자동 제세동기가 준비되면, 먼저 전원 버튼을 누르세요.\n" +
        "환자의 상의를 벗기고, 두 개의 패드를 환자의 가슴에 단단히 부착하세요.\n" +
        "환자와 접촉한 사람이 없음을 확인한 뒤, 제세동 버튼을 누르세요.\n" +
        "제세동을 시행한 뒤에는 지체 없이 심폐소생술을 다시 시작해야 합니다."



        login = "로그인";
        sending = "전송중…";
        select_cpr_standard = "CPR 가이드라인";
        select_language = "언어";
        //select_mannequin = "마네킹 방향";
        select_set_num = "주기 설정";
        first_setting_txt = "최초 실행 시, 반드시 초기설정과 감도설정을 실시해주세요.";
        scanning_kit = "키트 검색중…";
        select_kit = "키트 선택";
        calibration_start = "감도설정 시작";
        calibration = "감도설정";
        kit_id = "키트 아이디: ";
        new_ = "새로 만들기";
        load = "불러오기";
        prev = "이전";
        chest_compression_calibration = "압박 감도설정";
        breath_calibration = "호흡 감도설정";
        write_calibration_name = "감도설정 이름 입력";
        preset = "프리셋";
        custom = "커스텀";
        current = "사용중";
        compression_depth = "압박 깊이";
        breath_volume = "호흡량";
        edit = "수정";
        save = "저장";
        apply = "적용";
        delete = "삭제";
        set_breath = "호흡 설정";
        set_compression = "압박 설정";
        compression30 = "가슴을 15번 압박";
        adequate5 = "적당한 양으로 5회 호흡";
        lay_rescuer = "일반인";
        bls_rescuer = "의료인";
        start_course = "교육 시작";
        adult = "성인";
        infant = "영아";
        start_test = "평가 시작";
        manage_data = "학생 정보 관리";
        add_student = "학생 추가";
        student_info = "학생 정보";
        all_check = "모두 선택";
        all_uncheck = "모두 해제";
        name = "이름";
        email = "이메일";
        view_record = "데이터 보기";
        data_selected = "개의 데이터 선택됨";
        datas_selected = "개의 데이터 선택됨";
        send_data = "데이터 전송";
        optional = "선택사항";
        register_kit = "키트 등록";
        heartisense_monitor = "하티센스 모니터";
        select_practice_mode = "연습 모드를 선택해 주세요";
        step_by_step_mode_short = "단계별 연습";
        whole_step_mode_short = "전과정 실습";
        step_by_step_mode_full = "단계별 연습";
        whole_step_mode_full = "전과정 실습";
        adult_title = "성인 BLS 및 AED 평가";
        infant_title = "영아 BLS 평가";
        all_title = "BLS 및 AED 평가";
        assign_student = "학생 배정";
        connect_kit = "키트 연결";
        change_kit_1 = "키트 변경";
        change_kit_2 = "키트\n변경";
        connecting_kit = "키트 연결중…";
        searching_kit = "키트 검색중…";
        compression_pad = "압박 패드";
        breath_module = "호흡 모듈";
        and = "와 ";
        pass = "성공";
        fail = "실패";
        cycle = "주기";
        ready = "준비";
        scenario_description = "시나리오 설명";
        response = "의식 확인";
        emergency = "구조 요청";
        check_pulse = "맥박 확인";
        compression = "압박";
        breath = "호흡";
        cpr = "CPR";
        aed = "AED";
        precpr_aha_bls = "의식 확인\n구조 요청\n호흡 확인\n맥박 확인";
        precpr_aha_lay = "의식 확인\n구조 요청\n호흡 확인";
        precpr_erc = "안전 확인\n의식 확인\n기도 확보\n호흡 확인\n구조 요청";
        precpr_anzcor = "안전 확인\n의식 확인\n구조 요청\n기도 확보\n호흡 확인";
        disconnected = "연결안됨";
        connected = "연결됨";
        hands_off_time = "압박 중단 시간";
        second = "초";
        minute = "분";
        time = "시간";
        timer = "진행 시간";
        position = "위치";
        depth = "깊이";
        rate = "속도";
        count = "횟수";
        no_named = "이름 없음 ";
        save_record = "데이터 저장하기";
        save_warning = "*기존 데이터가 있는 경우 새 데이터가 덮어쓰기 되오니 주의하세요";
        save_exist = "데이터 있음";
        step = "단계";
        result = "결과";
        auto_connect = "자동 연결";
        result_guide = "* 성공 횟수 / 시도 횟수";
        save_all = "모두 저장";
        saved = "저장됨";
        close = "닫기";
        view_detail = "자세히 보기";
        not_assigned = "배정되지 않음";

        /// repeat = "다시하기";

        complete = "완료";
        exist = "있음";
        data = "데이터";
        student_name = "학생 이름";
        select = "선택";
        assign_result = "배정 결과";
        detail = "상세 그래프";
        graph = "그래프";
        group = "그룹";
        compression_rate = "압박 속도";
        compression_position = "압박 위치";
        duration = "소요시간";
        average_rate = "평균 속도";
        recoil = "이완";
        good_depth = "적정깊이";
        strong_depth = "강함";
        fast = "빠름";
        good_rate = "적정속도";
        slow = "느림";
        excessive = "지나침";
        good_amount = "적정량";
        insufficient = "부족함";
        recoil_needed = "이완 필요";
        too_weak = "압박 부족";
        wrong_position = "잘못된 위치";


        option_calibration = "감도 설정";
        option_preference = "초기 설정";


        not_valid_email = "이메일 주소의 형식이 유효하지 않습니다.";
        login_fail = "입력하신 이메일 주소 혹은 비밀번호가 일치하지 않습니다.";
        not_assigned_inst_id = "입력 하신 아이디는 기관에 배정되지 않았습니다.";
        server_error = "서버에 전송하는 도중 오류가 발생하였습니다. 에러가 반복된다면 문의 바랍니다.";
        no_license = "입력하신 아이디의 라이센스가 없습니다.";
        license_expiration = "입력하신 아이디의 기간 라이센스가 만료되었습니다.";
        license_data_limit = "입력하신 아이디의 데이터 개수 라이센스가 만료되었습니다.";
        connect_fail = "연결에 실패하였습니다.";
        connect_success = "연결에 성공하였습니다.";
        disconnect_kit = "연결이 끊어졌습니다.";
        using_data_calibration = "이 설정값은 사용중이므로 삭제할 수 없습니다.";
        sensor_error_detected = "에서 에러가 발견되었습니다.";
        sensor_error_detected_m = "에서 에러가 발견되었습니다.";
        sensor_not_connected = "이(가) 연결되지 않았습니다.";
        sensor_not_connected_m = "이(가) 연결되지 않았습니다.";
        at_least_1_connect = "키트가 연결되지 않았습니다.";
        not_permission = "이 프로그램은 해당 하티센스 키트에서 사용 할 수 없습니다. 키트의 라벨을 확인해주세요.";
        empty_name_or_email = "이름과 이메일을 모두 입력해주세요.";
        all_check_need = "모든 학생의 평가가 끝나야 데이터 저장을 할 수 있습니다.";
        not_valid_name = "이름이 유효하지 않습니다.(a~z, A~Z, 0~9, 한글, -, _ 만 사용가능)";
        search_the_kit = "자동으로 키트를 검색중입니다.";
        network_disconnected = "네트워크에 연결되어 있지 않습니다.";
        bt_off_msg = "모든 블루투스 연결이 끊어집니다.";
        need_connect_kit = "키트와 연결이 필요합니다.";
        start_single_mode = "개별학습 모드로 시작합니다.";
        start_monitor_mode = "하티센스 모니터 모드로 시작합니다.";
        bls_using = "하티센스 테스트가 사용되고 있습니다. 이 프로그램은 종료됩니다.";
        calibration_using = "강사용 프로그램에서 감도 조절이 사용되고 있습니다. 이 프로그램은 종료됩니다.";
        wait_instructor = "강사의 지시를 기다려주세요";
        search_fail = "키트 검색에 실패했습니다. 키트 전원을 확인해주세요.";
        want_connect_retry = "키트와 연결이 실패했습니다.";
        want_quit = "프로그램을 종료하시겠습니까?";
        want_back_calibration = "이전으로 돌아가시겠습니까? 새로 만들면 설정값은 저장되지 않습니다.";
        want_send = "선택된 데이터를 서버에 전송하시겠습니까?";
        want_delete = "선택된 데이터를 삭제하시겠습니까?";
        want_main = "메인으로 돌아가시겠습니까?";
        want_back = "이전으로 돌아가시겠습니까?";
        want_turn_on_bluetooth = "블루투스를 켜시겠습니까?";
        want_cali_main = "저장이 완료되었습니다. 감도 설정 메인 페이지로 이동하시겠습니까?";
        want_disconnect = "연결을 종료하시겠습니까?";
        want_restart_training = "교육을 다시 시작하겠습니까?";
        want_restart_test = "평가를 다시 시작하겠습니까?";
        want_start_stepbystep_mode = "단계별 연습을 시작하시겠습니까?";
        want_start_wholestep_mode = "전과정 실습을 시작하시겠습니까?";
        want_delete_kit = "등록된 키트를 제거하고 수동모드로 전환하시겠습니까?";
        want_assign_completed = "배정을 완료하시겠습니까? 배정하지 않은 학생의 데이터는 저장되지 않습니다.";
        want_restart_cpr = "CPR 단계를 다시 시작하겠습니까?";
        quit = "종료";
        reconnect = "재연결";
        send = "전송";
        confirm = "확인";
        cancel = "취소";
        yes = "예";
        no = "아니요";
        dont_save = "저장하지 않음";
        existing_name = "중복된 이름";
        error = "오류";
        to_main = "메인으로";
        bluetooth_off = "블루투스 꺼짐";
        disconnect = "연결 끊기";
        restart = "재시작";
        search = "검색";
        network_error = "네트워크 에러";
        skip = "넘기기";
        find_id_pw = "아이디/비밀번호 찾기";
        exit = "나가기";























//        bls_adult_2_title = "AED 평가";
//        bls_adult_2_check_1 = "AED와 백마스크 가져오기";
//        bls_adult_2_check_2 = "압박 지속하기";
//        bls_adult_2_check_3 = "환자 주변 환기";
//        bls_adult_2_check_4 = "제세동 시행";
//        bls_adult_3_title = "백마스크 호흡";
//        bls_adult_3_check_1 = "1주기-압박 30회";
//        bls_adult_3_check_2 = "2주기-압박 30회";
//        bls_adult_3_check_3 = "1주기-백마스크 호흡 2회 ";
//        bls_adult_3_check_4 = "2주기-백마스크 호흡 2회 ";
        //bls_adult_sheet_title_1 = "1인 구조자 성인 BLS 평가";
//        bls_adult_sheet_check_1 = "환자의 의식 확인 (10초 이내)";
//        bls_adult_sheet_check_2 = "응급 구조 및 AED 요청";
//        bls_adult_sheet_check_3 = "맥박확인 (10초 이내)";
        bls_adult_sheet_check_4 = "심폐소생술 시행";
        bls_adult_sheet_check_4_1 = "압박 시간";
        bls_adult_sheet_check_4_2 = "정확한 압박";
        bls_adult_sheet_check_4_3 = "압박 중단 시간";
        bls_adult_sheet_check_4_4 = "정확한 호흡";
//        //bls_adult_sheet_title_2 = "두 번째 구조자 AED 평가 및 교대";
//        bls_adult_sheet_check_5 = "두 번째 구조자 : AED와 백마스크 장비 가져오기, AED 전원을 켜고 패드 부착";
//        bls_adult_sheet_check_6 = "첫 번째 구조자 : AED 준비시간 동안 압박 지속";
//        bls_adult_sheet_check_7 = "두 번째 구조자 : 분석을 위하여 손을 떼도록 지시";
//        bls_adult_sheet_check_8 = "두 번째 구조자 : 제세동이 필요하다면, 손을 떼도록 지시하고 제세동 시행";
//        //bls_adult_sheet_title_3 = "첫 번째 구조자의 백마스크 장비를 이용한 호흡 능력 평가";
//        bls_adult_sheet_check_9 = "제세동 시행 후 즉시 심폐소생술 재개";
//        bls_adult_sheet_9_cc = "두 번째 구조자 : 30회 가슴압박 시행";
//        bls_adult_sheet_9_rp = "첫 번째 구조자 : 백마스크 장비를 이용하여 호흡 2회 실시";
////        bls_infant_1_title = "1인 구조자 BLS";
////        bls_infant_2_title = "CPR 시행";
////        bls_infant_2_check_1 = "압박 위치";
////        bls_infant_2_check_2 = "압박 속도";
////        bls_infant_2_check_3 = "압박 깊이";
////        bls_infant_2_check_4 = "가슴 이완";
////        bls_infant_2_check_5 = "압박 중단 최소화";
////        bls_infant_3_title = "2인 구조자 BLS";
////        bls_infant_3_check_1 = "역할 교대";
////        bls_infant_3_check_2 = "1주기 - 백마스크 호흡 2회";
//        bls_infant_3_check_3 = "2주기 - 백마스크 호흡 2회";
//        bls_infant_3_check_4 = "1주기 - 압박 15회";
//        bls_infant_3_check_5 = "2주기 - 압박 15회";
//        //bls_infant_sheet_title_1 = "1인 구조자 영아 BLS 평가";
//        bls_infant_sheet_check_1 = "환자의 의식 확인 (10초 이내)";
//        bls_infant_sheet_check_2 = "응급 구조 및 AED 요청";
//        bls_infant_sheet_check_3 = "맥박확인 (10초 이내)";
//        bls_infant_sheet_check_4 = "심폐소생술 시행";
//        bls_infant_sheet_check_4_1 = "정확한 압박 위치";
//        bls_infant_sheet_check_4_2 = "적절한 압박 속도(18초 이내)";
//        bls_infant_sheet_check_4_3 = "적절한 압박 깊이";
//        bls_infant_sheet_check_4_4 = "적절한 가슴 이완";
//        bls_infant_sheet_check_4_5 = "압박 중단 최소화(10초 이내)";
//        //bls_infant_sheet_title_2 = "2인 구조자 영아 BLS 평가 및 교대";
//        bls_infant_sheet_check_5 = "두 번째 구조자 : AED와 백마스크 장비 가져오기, 역할 교대";
//        bls_infant_sheet_check_6 = "심폐소생술 시행";
//        bls_infant_sheet_check_6_1 = "두 번째 구조자 : 2 thumb-encircling hands 방법으로 15회 압박 (9초 이내)";
//        bls_infant_sheet_check_6_2 = "첫 번째 구조자 : 백마스크 장비를 이용하여 호흡 2회 실시";
//        bls_infant_sheet_check_7 = "심폐소생술 시행";
//        bls_infant_sheet_check_7_1 = "첫 번째 구조자 : 2 thumb-encircling hands 방법으로 15회 압박 (9초 이내)";
//        bls_infant_sheet_check_7_2 = "두 번째 구조자 : 백마스크 장비를 이용하여 호흡 2회 실시";
        response_info_title = "의식 확인";
        response_info_txt = "환자의 어깨를 가볍게 두드리며 의식을 확인하세요.";
        emergency_info_title = "응급 구조 요청";
        emergency_info_txt = "주변의 한 사람을 지목하여 119 신고를 요청하세요.";
        pulsecheck_info_title = "맥박 확인";
        pulsecheck_info_txt = "최소 5초에서 최대 10초 동안 환자의 맥박을 확인한 후 맥박이 느껴지지 않으면 즉시 심폐소생술을 실시하세요.";
        compression_info_title = "압박";
        compression_info_txt = "압박을 실시하세요.";
        respiration_info_title = "호흡";
        respiration_info_txt = "호흡을 실시하세요.";
        aed_info_title = "자동 제세동기(AED)";
        aed_info_1_txt = "자동 제세동기가 준비되면, 먼저 전원 버튼을 누르세요.";
        aed_info_2_txt = "환자의 상의를 벗기고, 두 개의 패드를 환자의 가슴에 단단히 부착하세요.";
        aed_info_3_txt = "환자와 접촉한 사람이 없음을 확인한 뒤, 제세동 버튼을 누르세요.";
        aed_info_4_txt = "제세동을 시행한 뒤에는 지체 없이 심폐소생술을 다시 시작해야 합니다.";
        press_sensor_pad = "가슴 압박을 진행하면 CPR 단계가 시작됩니다.";
        
        app_guide_address_trainer = "http://www.heartisense.com/pdf/Trainer_K.html";
        app_guide_address_monitor = "http://www.heartisense.com/pdf/Monitor_K.html";
        app_guide_address_test = "http://www.heartisense.com/pdf/Test_K.html";
        
        check_danger_info_title = "안전 확인";
        check_danger_info_txt = "먼저 환자가 위험한 곳에 있지 않도록 확인하세요.";
        open_airway_info_title = "기도 확보";
        open_airway_info_txt = "손을 환자의 이마에 놓고 부드럽게 그의 머리를 뒤로 기울이세요. 손가락은 환자의 턱 밑에 두고 턱을 밀어올려 기도를 확보하세요.";
        check_breath_info_title = "호흡 확인";
        check_breath_info_txt = "최대 10초동안 보고, 듣고, 느끼며 환자가 호흡을 제대로 하는지 확인하세요.";
        
        check_danger = "안전 확인";
        open_airway = "기도 확보";
        check_breath = "호흡 확인";

    }
}
