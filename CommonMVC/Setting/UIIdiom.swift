//
//  UIIdiom.swift
//  HS Monitor
//
//  Created by Jongwoo Moon on 2016. 7. 14..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation


class UIIdiom: NSObject {
    static let sing = UIIdiom()

    var isPad: Bool { return UI_USER_INTERFACE_IDIOM() == .Pad }
    var isPhone: Bool { return UI_USER_INTERFACE_IDIOM() == .Phone }

    var cornerRad: CGFloat {
        if isPad {
            return 15
        } else {
            return 8
        }
    }





}