//
//  ShlTestBase.swift
//  Test
//
//  Created by Jongwoo Moon on 2016. 1. 21..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation


var langStr = ShlTrainer()







class StrLocTrainerBase : NSObject { // 파일을 가려서 취사선택하면 됨..
    /* Login */
    var login = "Login";

    /* Sending */
    var sending = "Sending…";

    /* Setting */
    var select_language = "Language";
    var select_mannequin = "Direction of the manikin";
    var select_set_num = "Cycle setting";
    var first_setting_txt = "For the first time of running our program, please follow the steps for initial setting and calibration.";

    /* Bluetooth List */
    var scanning_kit = "Scanning Kit…";
    var select_kit = "Select Kit";

    /* Calibration */
    var calibration_start = "Calibration Start";
    var calibration = "Calibration";
    var kit_id = "Kit ID: ";
    var new_ = "New";
    var load = "Load";
    var prev = "Prev";
    var chest_compression_calibration = "Compression Calibration";
    var breath_calibration = "Breath Calibration";
    var write_calibration_name = "Write down the name of calibration";
    var preset = "Preset";
    var custom = "Custom";
    var current = "Current";
    var compression_depth = "Compression Depth";
    var breath_volume = "Breath Volume";
    var edit = "Edit";
    // edit_title = "Edit Title";
    var save = "Save";
    var apply = "Apply";
    var delete = "Delete";
    var chest_compression_calibration_manual_info = "Compress for 5cm. Move the pointer to the left when the arrow is in the yellow area; move the pointer to the right when the arrow is in the red area.";
    var breath_calibration_manual_info = "Take moderate breaths. Move the pointer to the left when the arrow is in the yellow area; move the pointer to the right when the arrow is in the red area.";
    var set_breath = "Set Breath";
    var set_compression = "Set Compression";
    var compression30 = "Deliver 30 compressions";
    var adequate5 = "Take five moderate breaths.";

    /* Main */
    var adult = "Adult";
    var infant = "Infant";
    var start_test = "Start Test";
    var manage_data = "Data Management";

    /* Manage Student */
    var add_student = "Add Student";
    var student_info = "Student Info";
    var all_check = "Check All";
    var all_uncheck = "Uncheck All";
    var name = "Name";
    var email = "Email";
    var view_record = "View records";
    var data_selected = " record is selected";
    var datas_selected = " records are selected";
    var send_data = "Send data";
    var optional = "Options";

    /* Test */
    var register_kit = "Register Kit";
    var adult_title = "Adult BLS and AED Assessment";
    var infant_title = "Infant BLS Assessment";
    var all_title = "BLS and AED Assessment";
    var assign_student = "Assign Student";
    var connect_kit = "Connect kit";
    var change_kit = "Change Kit";
    var connecting_kit = "Connecting kit…";
    var compression_pad = "Compression Pad";
    var breath_module = "Breath Module";
    var and = " and ";
    var pass = "Pass";
    var fail = "Fail";
    var cycle = "Cycle";
    var ready = "Ready";
    var response = "Response";
    var emergency = "Emergency";
    var check_pulse = "Check Pulse";
    var compression = "Compression";
    var breath = "Breath";
    var disconnected = "Disconnected";
    var connected = "Connected";
    var second = "sec";
    var minute = "min";
    var no_named = "No name";
    var time = "Time";
    var count = "Count";
    var save_record = "Save the records";
    var save_warning = "*Beware that any previous records will be overwritten by new records.";// "*기존 기록이 있는 학생의 경우 새 기록이 덮어쓰기 되오니 주의하세요";
    var save_exist = "Records exist";
    var step = "Step";
    var result = "Result";
    var auto_connect = "Auto Connect";
    var save_all = "Save all";
    var saved = "Saved";
    var close = "Close";
    var not_assigned = "not assigned";

    /* Assign */
    var repeatString = "Repeat";
    var complete = "Complete";
    var student_name = "Student Name";
    var select = "Select";
    var assign_result = "Assignment";

    /* Detail */
    var detail = "Detail";
    // result_page = "Result Page";
    var graph = "Graph";

    var group = "Group";
    var compression_rate = "Compression Rate";
    var compression_position = "Compression Position";
    var duration = "Duration";
    var average_rate = "Average rate";
    var recoil = "Recoil";
    var good_depth  = "Good";
    var fast = "Fast";
    var good_rate = "Good";
    var slow = "Slow";
    var excessive = "Strong";
    var good_amount = "Good";
    var insufficient = "Weak";
    var hands_off_time = "Hands off time";
    var recoil_needed = "Recoil needed";
    var too_weak = "Too weak";
    var wrong_position = "Wrong position";

    /* Option */
    var option_calibration = "    Calibration";
    var option_preference = "    Initial Setting";

    /* Message */
    var not_valid_email = "The email address is not in valid form.";
    var login_fail = "Incorrect email address or password!.";
    var connect_fail = "Failed to connect with the kit.";
    var connect_success = "Connection with the kit is successful.";
    var disconnect_kit = "Connection lost.";
    var using_data_calibration = "You cannot delete this configuration value because it is in use.";
    var sensor_error_detected = " error has been found.";
    var sensor_error_detected_m = " error have been found.";
    var sensor_not_connected = " is not connected.";
    var sensor_not_connected_m = " are not connected.";
    var at_least_1_connect = "The kit is not connected.";
    var not_permission = "You cannot use this program in this HeartiSense kit! Check the kit’s label.";
    var empty_name_or_email = "Enter your name and email address.";
    var all_check_need = "You cannot save the data until all trainees are assessed.";
    var not_valid_name = "Invalid name! (Use letters a to z, A to Z, 0 to 9, Korean characters, -, and _.)";
    var searching_kit = "Automatically searching the kit";
    var network_disconnected = "You're not connected to a network";
    var bt_off_msg = "All Bluetooth connections will be disconnected.";

    /* Question */
    var want_quit = "Do you want to close the program?";
    var want_back_calibration = "Do you want to return to the previous stage? The calibration value you were newly creating will not be saved.";
    var want_delete_calibration = "Do you want to delete this calibration data?";
    var want_save_calibration = "Do you want to return to the previous stage? The calibration value you were editing will not be saved.";
    var want_overwrite_calibration = "Calibration value with the same name already exists. Do you want to overwrite it?";
    var want_send = "Do you want to send the selected records to server?";
    var want_delete = "Do you want to delete the selected records?";
    var want_main = "Do you want to return to the main menu?";
    var want_back = "Do you want to return to the previous stage?";
    var want_turn_on_bluetooth = "Do you want to turn on Bluetooth?";
    var want_cali_main = "Saving completed. Do you want to move to the main calibration page?";
    var want_disconnect = "Do you want to disconnect?";
    var want_restart_course = "Do you want to resume test? ";
    var want_delete_kit = "Do you want to remove the registered kit and switch to the manual mode?";
    var want_assign_completed = "Is the assignment complete? Data of unassigned students will not be saved.";
    var want_restart_cpr = "Do you want to restart cpr phase?";

    /* Dialog */
    var quit = "Quit";  var quitArrow = "◀︎ Quit";
    var send = "Send";
    var deleteString = "Delete";
    var confirm = "Confirm";
    var cancel = "Cancel";
    var yes = "Yes";
    var no = "No";
    var dont_save = "Do not Save";
    var existing_name = "Existing Name";
    var error = "Error";
    var to_main = "To main";
    var bluetooth_off = "Bluetooth Off";
    var disconnect = "Disconnect";
    var restart = "Restart";
    var search = "Search";
    var network_error = "Disconnected";
    var skip = "Skip";
    var find_id_pw = "Forgot your ID/password?";
    var exit = "Exit";

    /* Test Adult */
    var bls_adult_1_title = "Adult BLS Skills";
    var bls_adult_2_title = "AED skills";
    var bls_adult_2_check_1 = "Bring AED & bag-mask";
    var bls_adult_2_check_2 = "Continue compression";
    var bls_adult_2_check_3 = "Ventilate the patient’s surrounding";
    var bls_adult_2_check_4 = "Deliver shock";
    var bls_adult_3_title = "Bag-mask Ventilation";
    var bls_adult_3_check_1 = "Cycle 1 - Give 30 compressions";
    var bls_adult_3_check_2 = "Cycle 2 - Give 30 compressions";
    var bls_adult_3_check_3 = "Cycle 1 - Give 2 breaths with bag-mask";
    var bls_adult_3_check_4 = "Cycle 2 - Give 2 breaths with bag-mask";
    var bls_adult_sheet_title_1 = "1-Rescuer Adult BLS Assessment";
    var bls_adult_sheet_check_1 = "Checks for response (at least 5 seconds)";
    var bls_adult_sheet_check_2 = "Request emergency rescue and AED";
    var bls_adult_sheet_check_3 = "Checks for pulse (no more than 10 seconds)";
    var bls_adult_sheet_check_4 = "Conduct CPR";
    var bls_adult_sheet_check_4_1 = "Compression Time";
    var bls_adult_sheet_check_4_2 = "Correct Compression";
    var bls_adult_sheet_check_4_3 = "Time of suspended compression";
    var bls_adult_sheet_check_4_4 = "Correct Respiration";
    var bls_adult_sheet_title_2 = "2nd Rescuer AED Assessment and Switch";
    var bls_adult_sheet_check_5 = "2nd Rescuer : Bring the AED and bag-mask, switch on the AED and attach the pads";
    var bls_adult_sheet_check_6 = "1st Rescuer : Continue compression until the AED is prepared ";
    var bls_adult_sheet_check_7 = "2nd Rescuer : Instruct to take hands off for analysis";
    var bls_adult_sheet_check_8 = "2nd Rescuer : If a defibrillation is required, instruct to take hands off and proceed with defibrillation";
    var bls_adult_sheet_title_3 = "1st Rescuer Bag-mask Ventilation Assessment";
    var bls_adult_sheet_check_9 = "Conduct CPR immediately after shock delivery";
    var bls_adult_sheet_9_cc = "2nd Rescuer : Give 30 compressions";
    var bls_adult_sheet_9_rp = "1st Rescuer : Give 2 breaths with bag-mask";

    /* Test Infant */
    var bls_infant_1_title = "1-Rescuer Infant BLS Skills";
    var bls_infant_2_title = "Conduct CPR";
    var bls_infant_2_check_1 = "Finger Placement";
    var bls_infant_2_check_2 = "Compression rate";
    var bls_infant_2_check_3 = "Compression depth";
    var bls_infant_2_check_4 = "Chest Recoil";
    var bls_infant_2_check_5 = "Minimize suspension of compression";
    var bls_infant_3_title = "2-Rescuer CPR and Switch";
    var bls_infant_3_check_1 = "Switch roles";
    var bls_infant_3_check_2 = "Cycle 1 - Give breaths with bag-mask";
    var bls_infant_3_check_3 = "Cycle 2 - Give breaths with bag-mask";
    var bls_infant_3_check_4 = "Cycle 1 - Give 15 compressions";
    var bls_infant_3_check_5 = "Cycle 2 - Give 15 compressions";
    var bls_infant_sheet_title_1 = "1-Rescuer Infant BLS Assessment";
    var bls_infant_sheet_check_1 = "Checks for response (at least 5 seconds)";
    var bls_infant_sheet_check_2 = "Request emergency rescue and AED";
    var bls_infant_sheet_check_3 = "Checks for pulse (up to 10s)";
    var bls_infant_sheet_check_4 = "Conduct CPR";
    var bls_infant_sheet_check_4_1 = "Correct finger placement";
    var bls_infant_sheet_check_4_2 = "Adequate rate (up to 18s)";
    var bls_infant_sheet_check_4_3 = "Adequate depth";
    var bls_infant_sheet_check_4_4 = "Allows complete chest recoil";
    var bls_infant_sheet_check_4_5 = "Minimize suspension of compression (up to 10s)";
    var bls_infant_sheet_title_2 = "2-Rescuer CPR and Switch";
    var bls_infant_sheet_check_5 = "2nd Rescuer : Brings the AED and bag-mask, and Switch roles";
    var bls_infant_sheet_check_6 = "Conduct CPR";
    var bls_infant_sheet_check_6_1 = "2nd Rescuer : Give 15 compressions by using 2 thumb-encircling hands technique (up to 9s)";
    var bls_infant_sheet_check_6_2 = "1st Rescuer : Give 2 breaths with bag-mask";
    var bls_infant_sheet_check_7 = "Conduct CPR";
    var bls_infant_sheet_check_7_1 = "1st Rescuer : Give 15 compressions by using 2 thumb-encircling hands technique (up to 9s)";
    var bls_infant_sheet_check_7_2 = "2nd Rescuer : Give 2 breaths with bag-mask";
    
    func setValues() {
        
    }
}





