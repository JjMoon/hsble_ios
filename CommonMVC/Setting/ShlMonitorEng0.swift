//
//  ShlMonitorEng0.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2016. 1. 28..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation










class StrLocMonitorEng0: StrLocMonitorBase {
    override func setValues() {

        lisence_error_msg = "Lisence Error !!! ";

        /* Login */
        login = "Login";

        /* Sending */
        sending = "Sending…";

        /* Setting */
        select_language = "Language";
        select_mannequin = "Direction of the manikin";
        select_set_num = "Cycle setting";
        first_setting_txt = "For the first time of running our program, please follow the steps for initial setting and calibration.";

        /* Bluetooth List */
        scanning_kit = "Scanning Kit…";
        select_kit = "Select Kit";

        /* Calibration */
        calibration_start = "Calibration Start";
        calibration = "Calibration";
        kit_id = "Kit ID: ";
        new_ = "New";
        load = "Load";
        prev = "Prev";
        chest_compression_calibration = "Compression Calibration";
        breath_calibration = "Breath Calibration";
        // chest_compression_calibration_load_info =
        // "Manually set chest compression and breath parameters";
        write_calibration_name = "Write down the name of calibration";
        preset = "Preset";
        custom = "Custom";
        current = "Current";
        compression_depth = "Compression Depth";
        breath_volume = "Breath Volume";
        edit = "Edit";
        // edit_title = "Edit Title";
        save = "Save";
        apply = "Apply";
        delete = "Delete";
        chest_compression_calibration_manual_info = "Compress for 5cm. Move the pointer to the left when the arrow is in the yellow area; move the pointer to the right when the arrow is in the red area.";
        breath_calibration_manual_info = "Take moderate breaths. Move the pointer to the left when the arrow is in the yellow area; move the pointer to the right when the arrow is in the red area.";
        set_breath = "Set Breath";
        set_compression = "Set Compression";
        compression30 = "Deliver 30 compressions";
        adequate5 = "Take five moderate breaths.";

        /* Main */
        lay_rescuer = "Lay Rescuer";
        bls_rescuer = "Healthcare Provider";
        start_course = "Start Course";
        manage_data = "Data Management";

        /* Manage Student */
        add_student = "Add Student";
        student_info = "Student Info";
        all_check = "Check All";
        all_uncheck = "Uncheck All";
        name = "Name";
        email = "Email";
        view_record = "View records";
        data_selected = " record is selected";
        datas_selected = " records are selected";
        send_data = "Send data";
        optionalKey = "Options";

        /* Monitor */
        register_kit = "Register Kit";
        heartisense_monitor = "HeartiSense Monitor";
        assign_student = "Assign Student";
        select_practice_mode = "Please select the practice mode";
        step_by_step_mode = "Step-by-step";
        whole_step_mode = "Entire process";
        connect_kit = "Connect kit";
        change_kit = "Change Kit";
        connecting_kit = "Connecting kit…";
        compression_pad = "Compression Pad";
        breath_module = "Breath Module";
        and = " and ";
        pass = "Pass";
        fail = "Fail";
        cycle = "Cycle";
        ready = "Ready";
        scenario_description = "Scenario Description";
        response = "Response";
        emergency = "Emergency";
        check_pulse = "Check Pulse";
        compression = "Compression";
        breath = "Breath";
        AED = "AED";
        precpr_bls = "Response\n\nEmergency\n\nCheck Pulse";
        precpr_lay = "Response\n\nEmergency";
        disconnected = "Disconnected";
        connected = "Connected";
        hands_off_time = "Hands off time";
        second = "sec";
        minute = "min";
        rate = "Rate";
        time = "Time";
        count = "Count";
        no_named = "No name";
        save_record = "Save the records";
        save_warning = "*Beware that any previous records will be overwritten by new records.";// "*기존 기록이 있는 학생의 경우 새 기록이 덮어쓰기 되오니 주의하세요";
        save_exist = "Records exist";
        result = "Result";
        auto_connect = "Auto Connect";
        result_guide = "* Number of success / Number of attempts";
        save_all = "Save all";
        saved = "Saved";
        close = "Close";
        view_detail = "View Detail";

        /* Assign */
        repeatKey = "Repeat";
        complete = "Complete";
        exist = "Exist";
        data = "Data";

        /* Detail */
        detail = "Detail";
        compression_rate = "Compression Rate";
        compression_position = "Compression Position";
        duration = "Duration";
        average_rate = "Average rate";
        recoil = "Recoil";
        good_depth  = "Good";
        fast = "Fast";
        good_rate = "Good";
        slow = "Slow";
        excessive = "Strong";
        good_amount = "Good";
        insufficient = "Weak";
        recoil_needed = "Recoil needed";
        too_weak = "Too weak";
        wrong_position = "Wrong position";

        /* Option */
        option_calibration = "    Calibration";
        option_preference = "    Initial Setting";

        /* Message */
        not_valid_email = "The email address is not in valid form.";
        login_fail = "Incorrect email address or password!.";
        connect_fail = "Failed to connect with the kit.";
        connect_success = "Connection with the kit is successful.";
        disconnect_kit = "Connection lost.";
        using_data_calibration = "You cannot delete this configuration value because it is in use.";
        sensor_error_detected = " error has been found.";
        sensor_error_detected_m = " error have been found.";
        sensor_not_connected = " is not connected.";
        sensor_not_connected_m = " are not connected.";
        at_least_1_connect = "The kit is not connected.";
        not_permission = "You cannot use this program in this HeartiSense kit! Check the kit’s label.";
        empty_name_or_email = "Enter your name and email address.";
        not_valid_name = "Invalid name! (Use letters a to z, A to Z, 0 to 9, Korean characters, -, and _.)";
        searching_kit = "Automatically searching the kit";
        network_disconnected = "You're not connected to a network";
        bt_off_msg = "All Bluetooth connections will be disconnected.";

        /* Question */
        want_quit = "Do you want to close the program?";
        want_back_calibration = "Do you want to return to the previous stage? The calibration value you were newly creating will not be saved.";
        want_delete_calibration = "Do you want to delete this calibration data?";
        want_save_calibration = "Do you want to return to the previous stage? The calibration value you were editing will not be saved.";
        want_overwrite_calibration = "Calibration value with the same name already exists. Do you want to overwrite it?";
        want_send = "Do you want to send the selected records to server?";
        want_delete =  "Do you want to delete the selected records?";

        want_main = "Do you want to return to the main menu?";
        want_back = "Do you want to return to the previous stage?";
        want_turn_on_bluetooth = "Do you want to turn on Bluetooth?";
        want_cali_main = "Saving completed. Do you want to move to the main calibration page?";
        want_disconnect = "Do you want to disconnect?";
        want_restart_course = "Do you want to resume training?";
        want_start_stepbystep_mode = "Do you want to begin step-by-step practice?";
        want_start_wholestep_mode = "Do you want to begin practice the entire process?";
        want_delete_kit = "Do you want to remove the registered kit and switch to the manual mode?";
        want_assign_completed = "Is the assignment complete? Data of unassigned students will not be saved.";
        
        /* Dialog */
        quit = "Quit";
        send = "Send";
        deleteKey = "Delete";
        confirm = "Confirm";
        cancel = "Cancel";
        yes = "Yes";
        no = "No";
        dont_save = "Do not Save";
        existing_name = "Existing Name";
        error = "Error";
        to_main = "To main";
        bluetooth_off = "Bluetooth Off";
        disconnect = "Disconnect";
        restart = "Restart";
        search = "Search";
        network_error = "Disconnected";
        skip = "Skip";
        find_id_pw = "Forgot your ID/password?";
        exit = "Exit";
    }
    
}
