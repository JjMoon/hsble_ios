//
//  ShlTestBase.swift
//  Test
//
//  Created by Jongwoo Moon on 2016. 1. 21..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation





var langStr = ShlMonitor()




class StrLocMonitorBase : NSObject { // 파일을 가려서 취사선택하면 됨..

    var lisence_error_msg = "라이센스 에러 입니다.";

    /*Login*/
    var login = "로그인";

    /*Sending*/
    var sending = "전송중…";

    /*Setting*/
    var select_language = "언어";
    var select_mannequin = "마네킹 방향";
    var select_set_num = "주기 설정";
    var first_setting_txt = "최초 실행 시, 반드시 초기설정과 감도설정을 실시해주세요.";

    /*Bluetooth List*/
    var scanning_kit = "키트 검색중…";
    var select_kit = "키트 선택";

    /*Calibration*/
    var calibration_start = "감도설정 시작";
    var calibration = "감도설정";
    var kit_id = "키트 아이디: ";
    var new_ = "새로 만들기";
    var load = "불러오기";
    var prev = "이전";
    var chest_compression_calibration = "압박 감도설정";
    var breath_calibration = "호흡 감도설정";
    var write_calibration_name = "감도설정 이름 입력";
    var preset = "프리셋";
    var custom = "커스텀";
    var current = "사용중";
    var compression_depth = "압박 깊이";
    var breath_volume = "호흡량";
    var edit = "수정";
    // edit_title = "수정하기";
    var save = "저장";
    var apply = "적용";
    var delete = "삭제";
    var chest_compression_calibration_manual_info = "5cm 만큼 압박을 하세요. 화살표가 노란색 영역에 있다면 물방울을 왼쪽으로 옮기고, 화살표가 빨간색 영역에 있다면 물방울을 오른쪽으로 옮기세요.";
    var breath_calibration_manual_info = "적당량의 호흡을 하세요. 화살표가 노란색 영역에 있다면 물방울을 왼쪽으로 옮기고, 화살표가 빨간색 영역에 있다면 물방울을 오른쪽으로 옮기세요.";
    var set_breath = "호흡 설정";
    var set_compression = "압박 설정";
    var compression30 = "가슴을 30번 압박";
    var adequate5 = "적당한 양으로 5회 호흡";

    /*Main*/
    var lay_rescuer = "일반인";
    var bls_rescuer = "의료인";
    var start_course = "교육 시작";
    var manage_data = "학생 정보 관리";

    /*Manage Student*/
    var add_student = "학생 추가";
    var student_info = "학생 정보";
    var all_check = "모두 선택";
    var all_uncheck = "모두 해제";
    var name = "이름";
    var email = "이메일";
    var view_record = "데이터 보기";
    var data_selected = "개의 데이터 선택됨";
    var datas_selected = "개의 데이터 선택됨";
    var send_data = "데이터 전송";
    var optionalKey = "선택사항";

    /*Monitor*/
    var register_kit = "키트 등록";
    var heartisense_monitor = "하티센스 모니터";
    var assign_student = "학생 배정";
    var select_practice_mode = "연습 모드를 선택해 주세요";
    var step_by_step_mode = "단계별 연습";
    var whole_step_mode = "전과정 실습";
    var connect_kit = "키트 연결";
    var change_kit = "키트\n변경";
    var connecting_kit = "키트 연결중…";
    var compression_pad = "압박 패드";
    var breath_module = "호흡 모듈";
    var and = "와 ";
    var pass = "성공";
    var fail = "실패";
    var cycle = "주기";
    var ready = "준비";
    var scenario_description = "시나리오 설명";
    var response = "의식 확인";
    var emergency = "구조 요청";
    var check_pulse = "맥박 확인";
    var compression = "압박";
    var breath = "호흡";
    var AED = "AED";
    var precpr_bls = "의식 확인\n\n구조 요청\n\n맥박 확인";
    var precpr_lay = "의식 확인\n\n구조 요청";
    var disconnected = "연결안됨";
    var connected = "연결됨";
    var hands_off_time = "압박 중단 시간";
    var second = "초";
    var minute = "분";
    var time = "시간";
    var rate = "속도";
    var count = "횟수";
    var no_named = "이름 없음 ";
    var save_record = "데이터 저장하기";
    var save_warning = "*기존 데이터가 있는 경우 새 데이터가 덮어쓰기 되오니 주의하세요";
    var save_exist = "데이터 있음";
    var result = "결과";
    var auto_connect = "자동 연결";
    var result_guide = "* 성공 횟수 / 시도 횟수";
    var save_all = "모두 저장";
    var saved = "저장됨";
    var close = "닫기";
    var view_detail = "자세히 보기";

    /*Assign*/
    var repeatKey = "다시하기";
    var complete = "완료";
    var exist = "있음";
    var data = "데이터";

    /*Detail*/
    var detail = "상세 그래프";
    var compression_rate = "압박 속도";
    var compression_position = "압박 위치";
    var duration = "소요시간";
    var average_rate = "평균 속도";
    var recoil = "이완";
    var good_depth  = "적정깊이";
    var fast = "빠름";
    var good_rate = "적정속도";
    var slow = "느림";
    var excessive = "지나침";
    var good_amount = "적정량";
    var insufficient = "부족함";
    var recoil_needed = "이완 필요";
    var too_weak = "압박 부족";
    var wrong_position = "잘못된 위치";

    /*Option*/
    var option_calibration = "    감도 설정";
    var option_preference = "    초기 설정";

    /*Message*/
    var not_valid_email = "이메일 주소의 형식이 유효하지 않습니다!";
    var login_fail = "입력하신 이메일 주소 혹은 비밀번호가 일치하지 않습니다!";
    var connect_fail = "연결에 실패하였습니다!";
    var connect_success = "연결에 성공하였습니다!";
    var disconnect_kit = "연결이 끊어졌습니다!";
    var using_data_calibration = "이 설정값은 사용중이므로 삭제할 수 없습니다.";
    var sensor_error_detected = "에서 에러가 발견되었습니다.";
    var sensor_error_detected_m = "에서 에러가 발견되었습니다.";
    var sensor_not_connected = "이(가) 연결되지 않았습니다.";
    var sensor_not_connected_m = "이(가) 연결되지 않았습니다.";
    var at_least_1_connect = "키트가 연결되지 않았습니다.";
    var not_permission = "이 프로그램은 해당 하티센스 키트에서 사용 할 수 없습니다! 키트의 라벨을 확인해주세요.";
    var empty_name_or_email = "이름과 이메일을 모두 입력해주세요.";
    var not_valid_name = "이름이 유효하지 않습니다!(a~z, A~Z, 0~9, 한글, -, _ 만 사용가능)";
    var searching_kit = "자동으로 키트를 검색중입니다.";
    var network_disconnected = "네트워크에 연결되어 있지 않습니다.";
    var bt_off_msg = "모든 블루투스 연결이 끊어집니다.";

    /*Question*/
    var want_quit = "프로그램을 종료하시겠습니까?";
    var want_back_calibration = "이전으로 돌아가시겠습니까? 새로 만들면 설정값은 저장되지 않습니다.";
    var want_delete_calibration = "이 캘리브레이션 데이터를 삭제하시겠습니까?";
    var want_save_calibration = "이전으로 돌아가시겠습니까? 수정하면 설정값은 저장되지 않습니다.";
    var want_overwrite_calibration = "중복된 이름을 가진 설정값이 있습니다. 덮어쓰시겠습니까?";
    var want_send = "선택된 데이터를 서버에 전송하시겠습니까?";
    var want_delete = "선택된 데이터를 삭제하시겠습니까?";
    var want_main = "메인으로 돌아가시겠습니까?";
    var want_back = "이전으로 돌아가시겠습니까?";
    var want_turn_on_bluetooth =  "블루투스를 켜시겠습니까?";
    var want_cali_main = "저장이 완료되었습니다. 감도 설정 메인 페이지로 이동하시겠습니까?";
    var want_disconnect = "연결을 종료하시겠습니까?";
    var want_restart_course = "교육을 다시 시작하겠습니까?";
    var want_start_stepbystep_mode = "단계별 연습을 시작하시겠습니까?";
    var want_start_wholestep_mode = "전과정 실습을 시작하시겠습니까?";
    var want_delete_kit = "등록된 키트를 제거하고 수동모드로 전환하시겠습니까?";
    var want_assign_completed = "배정을 완료하시겠습니까? 배정하지 않은 학생의 데이터는 저장되지 않습니다.";

    /*Dialog*/
    var quit = "종료";
    var send = "전송";
    var deleteKey = "삭제";
    var confirm = "확인";
    var cancel = "취소";
    var yes = "예";
    var no = "아니요";
    var dont_save = "저장하지 않음";
    var existing_name = "중복된 이름";
    var error = "오류";
    var to_main = "메인으로";
    var bluetooth_off="블루투스 꺼짐";
    var disconnect = "연결 끊기";
    var restart = "재시작";
    var search = "검색";
    var network_error = "네트워크 에러";
    var skip = "넘기기";
    var find_id_pw = "아이디/비밀번호 찾기";
    var exit = "나가기";

    func setValues() {
        
    }
}



class StrLocMonitorKor1: StrLocMonitorBase {


}



