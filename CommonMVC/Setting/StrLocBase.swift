//
//  ShlTestBase.swift
//  Test
//
//  Created by Jongwoo Moon on 2016. 1. 21..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation


class StrLocBase : NSObject { // 파일을 가려서 취사선택하면 됨..


    func setValues() {

    }


    // 아직 미정인 것들..
    var no_data_to_send = "No data available for transmission."
    var cali_comp_explain = "Click + for more bars, Click – for less bars."
    var low_batery_kit = "Kit battery low.  Please replace the battery."






    /// etc
    var pause = "Pause";
    var select_old_records_send = "There are old test data that were not sent to the server. You must transfer it to the server in order to see it. Do you want to transfer?";
    
    
    /// Test 변경 관련 스트링.
    var needs_remediation = "Needs Remediation";
    var pass_nr_info = "Please check the results above and select Pass or Needs Remediation."

    var bls_adult_1_title = "Assessment and Activation" // "Adult BLS Skills";
    var bls_adult_1_check_danger = "Check for danger";
    var bls_adult_1_check_response = "Check responsiveness";
    var bls_adult_1_shouts_for_help = "Shout for help / Activate EMS / Send for AED";
    var bls_adult_1_check_pulse = "Check pulse";
    var bls_adult_1_open_airway = "Open airway";
    var bls_adult_1_check_breath = "Check breath";

    var bls_adult_2_title = "AED";
    var bls_adult_2_check_1 = "Powers on AED";
    var bls_adult_2_check_2 = "Correctly attaches pads";
    var bls_adult_2_check_3 = "Clears for analysis";
    var bls_adult_2_check_4 = "Clears to safely deliver a shock";
    var bls_adult_2_check_5 = "Safely delivers a shock";

    var bls_adult_3_title = "Resumes Compressions";
    var bls_adult_3_check_1 = "Ensures compressions are resumed immediately after shock delivery";

    /// 성인 결과지
    var bls_adult_sheet_title_1 = "Assessment and Activation";
    var bls_adult_sheet_title_2 = "Conduct CPR";
    var bls_adult_sheet_cpr_ct = "Compression Time";
    var bls_adult_sheet_cpr_cc = "Correct Compression";
    var bls_adult_sheet_cpr_hot = "Time of suspended compression";
    var bls_adult_sheet_cpr_rp = "Correct Respiration";
    var bls_adult_sheet_title_3 = "AED";
    var bls_adult_sheet_aed1 = "Powers on AED";
    var bls_adult_sheet_aed2 = "Correctly attaches pads";
    var bls_adult_sheet_aed3 = "Clears for analysis";
    var bls_adult_sheet_aed4 = "Clears to safely deliver a shock";
    var bls_adult_sheet_aed5 = "Safely delivers a shock"
    var bls_adult_sheet_title_4 = "Resumes Compressions"
    var bls_adult_sheet_cont_cpr = "Ensures compressions are resumed immediately after shock delivery";

    var bls_infant_1_title = "Assessment and Activation";
    var bls_infant_2_title = "Conduct CPR";
    var bls_infant_2_check_1 = "Cycle 1 - Comressions";
    var bls_infant_2_check_2 = "Cycle 1 - Breaths";
    var bls_infant_2_check_3 = "Cycle 2 - Comressions";
    var bls_infant_2_check_4 = "Cycle 2 - Breaths";
    var bls_infant_2_check_5 = "Cycle 2 - Time of suspended compression";
    var bls_infant_3_title = "Conduct CPR";
    var bls_infant_3_check_1 = "Cycle 3 - Comressions";
    var bls_infant_3_check_2 = "Cycle 4 - Bag-mask Ventilation";

    var bls_infant_sheet_title_1 = "Assessment and Activation"
    var bls_infant_1_check_danger = "Check for danger";
    var bls_infant_1_check_response = "Check responsiveness";
    var bls_infant_1_shouts_for_help  = "Shout for help / Activate EMS / Send for AED"
    var bls_infant_1_check_pulse = "Check pulse";
    var bls_infant_1_open_airway = "Open airway";
    var bls_infant_1_check_breath = "Check breath";
    var bls_infant_sheet_title_2 = "Conduct CPR";
    var bls_infant_sheet_cpr_cc = "Comressions";
    var bls_infant_sheet_cpr_rp = "Breaths";
    var bls_infant_sheet_cpr_hot = "Time of suspended compression";
    var bls_infant_sheet_cpr_bvm_rp = "Bag-mask Ventilation";

    // iOS 에서 수정/추가한 스트링...
    var select_menu = "Settings";

    var select_manikin_type = "Manikin";
    var select_manikin_dir = "Direction of the manikin";

    var rescuer = "Rescuer"

    var date = "Date"

    var quitArrow = "◀︎ Quit";
    var skipArrow = "Skip ▶︎"
    var repeatKey = "Repeat";
    var prev_gt = "◀︎ Prev"
    var preCpr = "PreCPR" // 영어만 있으면 됨.. 디버깅 용.
    var complete_icon = "✓ Complete";
    var repeat_icon = "✎ Repeat";
    var edit_icon = "✎ Edit";
    var volume = "Volume"
    var select_comp_rate_guide = "Guide of compression rate";
    var select_date_format = "Date format"

    var bls_setting = "BLS setting"
    var total_score = "Total Score"
    var reset_settings = "Reset settings"

    var compressionRatio = "Flow fraction"
    var productTypeErrorExit = "Product Type Error !!!";

    var stepbystep2line = "Step-by-step"
    var wholestep_2line = "Entire process"

    var aed_infoTotalTxt = "Turn on the defibrillator beforehand when it is ready to use. \n" +
    "Open or cut through the patient's shirt, and apply the defibrillator pads on his chest. \n" +
    "Make sure that the victim is clear, and shock the victim by pushing the button  \n" +
    "on the defibrillator.  \nOnce you have given a shock to the victim, you must continue CPR immediately."

    var login = "Login";
    var sending = "Sending…";
    var select_cpr_standard = "CPR Guideline";
    var select_language = "Language";
    //var select_mannequin = "Direction of the manikin";
    var select_set_num = "Cycle setting";
    var first_setting_txt = "When running the program for the first time, please follow the steps for initial setting and calibration.";
    var scanning_kit = "Scanning Kit…";
    var select_kit = "Select Kit";
    var calibration_start = "Calibration Start";
    var calibration = "Calibration";
    var kit_id = "Kit ID: ";
    var new_ = "New";
    var load = "Load";
    var prev = "Prev";
    var chest_compression_calibration = "Compression Calibration";
    var breath_calibration = "Breath Calibration";
    var write_calibration_name = "Write down the name of calibration";
    var preset = "Preset";
    var custom = "Custom";
    var current = "Current";
    var compression_depth = "Compression Depth";
    var breath_volume = "Breath Volume";
    var edit = "Edit";
    var save = "Save";
    var apply = "Apply";
    var delete = "Delete";
    var set_breath = "Set Breath";
    var set_compression = "Set Compression";
    var compression30 = "Deliver 15 compressions"; // 캘리에서만 실행해야함..
    var adequate5 = "Take five moderately deep breaths.";
    var lay_rescuer = "Lay Rescuer";
    var bls_rescuer = "Healthcare Provider";
    var start_course = "Start Course"; // 모니터
    var adult = "Adult";
    var infant = "Infant";
    var start_test = "Start Test"; // 테스트.
    var manage_data = "Data Management";
    var add_student = "Add Student";
    var student_info = "Student Info";
    var all_check = "Check All";
    var all_uncheck = "Uncheck All";
    var name = "Name";
    var email = "Email";
    var view_record = "View records";
    var data_selected = " record is selected";
    var datas_selected = " records are selected";
    var send_data = "Send data";
    var optional = "Options";
    var register_kit = "Register Kit";
    var heartisense_monitor = "HeartiSense Monitor";
    var select_practice_mode = "Please select the practice mode";
    var step_by_step_mode_short = "Step-by-step";
    var whole_step_mode_short = "Entire process";
    var step_by_step_mode_full = "Step-by-step Practice";
    var whole_step_mode_full = "Practice the entire process";
    var adult_title = "Adult BLS and AED Assessment";
    var infant_title = "Infant BLS Assessment";
    var all_title = "BLS and AED Assessment";
    var assign_student = "Assign Student";
    var connect_kit = "Connect kit";
    var change_kit_1 = "Change Kit";
    var change_kit_2 = "Change Kit";
    var connecting_kit = "Connecting kit…";
    var searching_kit = "Searching for Kit…";
    var compression_pad = "Compression Pad";
    var breath_module = "Breath Module";
    var and = " and ";
    var pass = "Pass";
    var fail = "Fail";
    var cycle = "Cycle";
    var ready = "Ready";
    var scenario_description = "Scenario Description";
    var response = "Response";
    var emergency = "Emergency";
    var check_pulse = "Check Pulse";
    var compression = "Compression";
    var breath = "Breath";
    var cpr = "CPR";
    var aed = "AED";
    var precpr_aha_bls = "Response\nEmergency\nCheck breath\nCheck Pulse";
    var precpr_aha_lay = "Response\nEmergency\nCheck breath";
    var precpr_erc = "Check for danger\nResponse\nOpen airway\nCheck breath\nEmergency";
    var precpr_anzcor = "Dangers\nResponsiveness\nSend\nAirway\nBreathing";
    var disconnected = "Disconnected";
    var connected = "Connected";
    var hands_off_time = "Hands off time";
    var second = "sec";
    var minute = "min";
    var time = "Time";
    var timer = "Time";
    var position = "Position";
    var depth = "Depth";
    var rate = "Rate";
    var count = "Count";
    var no_named = "No name";
    var save_record = "Save records";
    var save_warning = "*Please note that any previous records will be overwritten by new records.";
    var save_exist = "Records exist";
    var step = "Step";
    var result = "Result";
    var auto_connect = "Auto Connect";
    var result_guide = "* Number of successes / Number of attempts";
    var save_all = "Save all";
    var saved = "Saved";
    var close = "Close";
    var view_detail = "View Detail";
    var not_assigned = "not assigned";

    ///var repeat = "Repeat";

    var complete = "Complete";
    var exist = "Exist";
    var data = "Data";
    var student_name = "Student Name";
    var select = "Select";
    var assign_result = "Assignment";
    var detail = "Detail";
    var graph = "Graph";
    var group = "Group";
    var compression_rate = "Compression Rate";
    var compression_position = "Compression Position";
    var duration = "Duration";
    var average_rate = "Average rate";
    var recoil = "Recoil";
    var good_depth = "Good";
    var strong_depth = "Strong";
    var fast = "Fast";
    var good_rate = "Good";
    var slow = "Slow";
    var excessive = "Strong";
    var good_amount = "Good";
    var insufficient = "Weak";
    var recoil_needed = "Recoil needed";
    var too_weak = "Too weak";
    var wrong_position = "Wrong position";


    var option_calibration = "Calibration" //"    Calibration";
    var option_preference = "Initial Setting";


    var not_valid_email = "The email address is not in valid form.";
    var login_fail = "Incorrect email address or password.";
    var not_assigned_inst_id = "The ID you entered is not assigned to the institution.";
    var server_error = "An error occurred while sending data to the server. If this error persists, please contact us.";
    var no_license = "The ID you entered has no license.";
    var license_expiration = "The license period of the ID you entered has expired.";
    var license_data_limit = "The data number license of the ID you entered has expired.";
    var connect_fail = "Failed to connect with the kit.";
    var connect_success = "Connection with the kit is successful.";
    var disconnect_kit = "Connection lost.";
    var using_data_calibration = "You cannot delete this configuration value because it is in use.";
    var sensor_error_detected = " error has been found.";
    var sensor_error_detected_m = " error have been found.";
    var sensor_not_connected = " is not connected.";
    var sensor_not_connected_m = " are not connected.";
    var at_least_1_connect = "The kit is not connected.";
    var not_permission = "You cannot use this program with this HeartiSense kit. Please check the kit’s label.";
    var empty_name_or_email = "Enter your name and email address.";
    var all_check_need = "You cannot save the data until all trainees are assessed.";
    var not_valid_name = "Invalid name. (Use letters a to z, A to Z, 0 to 9, Korean characters, -, and _.)";
    var search_the_kit = "Automatically searching the kit";
    var network_disconnected = "You are not connected to a network";
    var bt_off_msg = "All Bluetooth connections will be disconnected.";
    var need_connect_kit = "The kit must be connected.";
    var start_single_mode = "Begin with the individual learning mode.";
    var start_monitor_mode = "Begin with the Monitor mode.";
    var bls_using = "HeartiSense test is in use. This program close.";
    var calibration_using = "Calibration is being used in the lecturer’s program. This program will close.";
    var wait_instructor = "Please wait for the instructor's directions.";
    var search_fail = "Kit search failed. Check the kit’s power button.";
    var want_connect_retry = "Connection to the kit failed.";
    var want_quit = "Are you sure you want to close the program?";
    var want_back_calibration = "Are you sure you want to return to the previous stage? The calibration value you have been creating will not be saved.";
    var want_send = "Are you sure you want to send the selected records to server?";
    var want_delete = "Are you sure you want to delete the selected records?";
    var want_main = "Are you sure you want to return to the main menu?";
    var want_back = "Are you sure you want to return to the previous stage?";
    var want_turn_on_bluetooth = "Do you want to turn on Bluetooth?";
    var want_cali_main = "Saving completed. Do you want to move to the main calibration page?";
    var want_disconnect = "Do you want to disconnect?";
    var want_restart_training = "Do you want to resume training?";
    var want_restart_test = "Do you want to resume test?";
    var want_start_stepbystep_mode = "Do you want to begin step-by-step practice?";
    var want_start_wholestep_mode = "Do you want to begin practicing the entire process?";
    var want_delete_kit = "Do you want to remove the registered kit and switch to the manual mode?";
    var want_assign_completed = "Is the assignment complete? Data of unassigned students will not be saved.";
    var want_restart_cpr = "Do you want to restart the CPR phase?";
    var quit = "Quit";
    var reconnect = "Reconnect";
    var send = "Send";
    var confirm = "Confirm";
    var cancel = "Cancel";
    var yes = "Yes";
    var no = "No";
    var dont_save = "Do not Save";
    var existing_name = "Existing Name";
    var error = "Error";
    var to_main = "To main";
    var bluetooth_off = "Bluetooth Off";
    var disconnect = "Disconnect";
    var restart = "Restart";
    var search = "Search";
    var network_error = "Disconnected";
    var skip = "Skip";
    var find_id_pw = "Forgot your ID/password?";
    var exit = "Exit";


//    var bls_adult_2_title = "AED skills";
//    var bls_adult_2_check_1 = "Bring AED & bag-mask";
//    var bls_adult_2_check_2 = "Continue compression";
//    var bls_adult_2_check_3 = "Ventilate the patient’s surrounding";
//    var bls_adult_2_check_4 = "Deliver shock";
//    var bls_adult_3_title = "Bag-mask Ventilation";
//    var bls_adult_3_check_1 = "Cycle 1-Give 30 compressions";
//    var bls_adult_3_check_2 = "Cycle 2-Give 30 compressions";
//    var bls_adult_3_check_3 = "Cycle 1-Give 2 breaths with bag-mask";
//    var bls_adult_3_check_4 = "Cycle 2-Give 2 breaths with bag-mask";
    //var bls_adult_sheet_title_1 = "1-Rescuer Adult BLS Assessment";
//    var bls_adult_sheet_check_1 = "Checks for response (at least 5 seconds)";
//    var bls_adult_sheet_check_2 = "Request emergency rescue and AED";
//    var bls_adult_sheet_check_3 = "Checks for pulse (no more than 10 seconds)";
    var bls_adult_sheet_check_4 = "Conduct CPR";
    var bls_adult_sheet_check_4_1 = "Compression Time";
    var bls_adult_sheet_check_4_2 = "Correct Compression";
    var bls_adult_sheet_check_4_3 = "Time of suspended compression";
    var bls_adult_sheet_check_4_4 = "Correct Respiration";
//    //var bls_adult_sheet_title_2 = "2nd Rescuer AED Assessment and Switch";
//    var bls_adult_sheet_check_5 = "2nd Rescuer : Bring the AED and bag-mask, switch on the AED and attach the pads";
//    var bls_adult_sheet_check_6 = "1st Rescuer : Continue compression until the AED is prepared ";
//    var bls_adult_sheet_check_7 = "2nd Rescuer : Instruct to take hands off for analysis";
//    var bls_adult_sheet_check_8 = "2nd Rescuer : If a defibrillation is required, instruct to take hands off and proceed with defibrillation";
//    //var bls_adult_sheet_title_3 = "1st Rescuer Bag-mask Ventilation Assessment";
//    var bls_adult_sheet_check_9 = "Conduct CPR immediately after shock delivery";
//    var bls_adult_sheet_9_cc = "2nd Rescuer : Give 30 compressions";
//    var bls_adult_sheet_9_rp = "1st Rescuer : Give 2 breaths with bag-mask";
////    var bls_infant_1_title = "1-Rescuer Infant BLS Skills";
////    var bls_infant_2_title = "Conduct CPR";
////    var bls_infant_2_check_1 = "Finger Placement";
////    var bls_infant_2_check_2 = "Compression rate";
////    var bls_infant_2_check_3 = "Compression depth";
////    var bls_infant_2_check_4 = "Chest Recoil";
////    var bls_infant_2_check_5 = "Minimize suspension of compression";
////    var bls_infant_3_title = "2-Rescuer CPR and Switch";
////    var bls_infant_3_check_1 = "Switch roles";
////    var bls_infant_3_check_2 = "Cycle 1-Give 2 breaths with bag-mask";
//    var bls_infant_3_check_3 = "Cycle 2-Give 2 breaths with bag-mask";
//    var bls_infant_3_check_4 = "Cycle 1-Give 15 compressions";
//    var bls_infant_3_check_5 = "Cycle 2 Give 15 compressions";
////    var bls_infant_sheet_title_1 = "1-Rescuer Infant BLS Assessment";
//    var bls_infant_sheet_check_1 = "Checks for response (at least 5 seconds)";
//    var bls_infant_sheet_check_2 = "Request emergency rescue and AED";
//    var bls_infant_sheet_check_3 = "Checks for pulse (up to 10s)";
//    var bls_infant_sheet_check_4 = "Conduct CPR";
//    var bls_infant_sheet_check_4_1 = "Correct finger placement";
//    var bls_infant_sheet_check_4_2 = "Adequate rate (up to 18s)";
//    var bls_infant_sheet_check_4_3 = "Adequate depth";
//    var bls_infant_sheet_check_4_4 = "Allows complete chest recoil";
//    var bls_infant_sheet_check_4_5 = "Minimize suspension of compression (up to 10s)";
////    var bls_infant_sheet_title_2 = "2-Rescuer CPR and Switch";
//    var bls_infant_sheet_check_5 = "2nd Rescuer : Bring the AED and bag-mask, and Switch roles";
//    var bls_infant_sheet_check_6 = "Conduct CPR";
//    var bls_infant_sheet_check_6_1 = "2nd Rescuer : Give 15 compressions using 2 thumb-encircling hands technique (up to 9s)";
//    var bls_infant_sheet_check_6_2 = "1st Rescuer : Give 2 breaths with bag-mask";
//    var bls_infant_sheet_check_7 = "Conduct CPR";
//    var bls_infant_sheet_check_7_1 = "1st Rescuer : Give 15 compressions by using 2 thumb-encircling hands technique (up to 9s)";
//    var bls_infant_sheet_check_7_2 = "2nd Rescuer : Give 2 breaths with bag-mask";
    var response_info_title = "Confirm Awareness";
    var response_info_txt = "Confirm the patient's responsiveness by gently shaking their shoulder.";
    var emergency_info_title = "Request for Emergency Service";
    var emergency_info_txt = "Pick one person around you and ask them to call emergency services.";
    var pulsecheck_info_title = "Check pulse";
    var pulsecheck_info_txt = "Take at least 5 but no more than 10 seconds to feel for a pulse. If you do not feel a pulse, begin CPR.";
    var compression_info_title = "Compression";
    var compression_info_txt = "Perform compressions.";
    var respiration_info_title = "Breath";
    var respiration_info_txt = "Give rescue breaths.";
    var aed_info_title = "AED";
    var aed_info_1_txt = "Turn on the defibrillator beforehand when it is ready to use.";
    var aed_info_2_txt = "Open or cut through the patient's shirt, and apply the defibrillator pads on his chest.";
    var aed_info_3_txt = "Make sure that the victim is clear, and shock the victim by pushing the button on the defibrillator.";
    var aed_info_4_txt = "Once you have given a shock to the victim, you must continue CPR immediately.";
    var press_sensor_pad = "Proceed with compression, and the CPR stage will begin.";

    var app_guide_address_trainer = "http://www.heartisense.com/pdf/Trainer_E.html";
    var app_guide_address_monitor = "http://www.heartisense.com/pdf/Monitor_E.html";
    var app_guide_address_test = "http://www.heartisense.com/pdf/Test_E.html";
    
    var check_danger_info_title = "Check for danger";
    var check_danger_info_txt = "First of all, check whether the victim is not present at any hazardous or risky place.";
    var open_airway_info_title = "Open airway";
    var open_airway_info_txt = "Place your hand on his forehead and gently tilt his head back; with your fingertips under the point of the victim's chin, lift the chin to open the airway.";
    var check_breath_info_title = "Check breath";
    var check_breath_info_txt = "Look, listen and feel for no more than 10 seconds to determine whether the victim is breathing normally.";

    var check_danger = "Check for danger";
    var open_airway = "Open airway";
    var check_breath = "Check breath";

    /// 영어만 있는 부분.
    var check_danger_info_title_anz = "Dangers";
    var check_danger_info_txt_anz = "Look for hazards, assess risks and ensure safety of patient and rescuer.";
    var response_info_title_anz = "Responsiveness";
    var response_info_txt_anz = "Grasp and squeeze the shoulders to elicit a response.";
    var emergency_info_title_anz = "Send";
    var emergency_info_txt_anz = "Call an Ambulance.";
    var open_airway_info_title_anz = "Airway";
    var open_airway_info_txt_anz = "Open the airway using head tilt-chin lift. One hand is placed on the forehead and the other hand is used to provide chin lift.";
    var check_breath_info_title_anz = "Breathing";
    var check_breath_info_txt_anz = "Look for movement of upper abdomen or lower chest. Listen for the escape of air from nose and mouth. Feel for movement of air at mouth or nose. Start compressions if not breathing or abnormal breathing.";
    var aed_info_title_anz = "Defibrillation";
    var aed_info_txt_anz = "Attach an Automated External Defibrillator as soon as available and follow prompts.";

}

