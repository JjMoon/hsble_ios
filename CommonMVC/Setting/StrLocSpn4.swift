//
//  ShlKr.swift
//  Test
//
//  Created by Jongwoo Moon on 2016. 1. 21..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation


class StrLocSpn4 : StrLocBase {
    override func setValues() {

        // iOS 에서 수정/추가한 스트링...
        select_menu = "Ajustes";

        
        
        
        
        
        
        
        
        
        
        
        
        /// etc
        pause = "Pause";
        
        
        
        
        select_manikin_type = "Maniquí";
        select_manikin_dir = "Posición del maniquí";

        rescuer = "Reanimador";

        quitArrow = "◀︎ Abandonar";
        date = "Fecha"

        skipArrow = "Omitir ▶︎"
        repeatKey = "Repetir" //////repeat repeat = "Repetir";
        prev_gt = "◀︎ Anterior"
        complete_icon = "✓ Completar";
        volume = "Volumen"
        select_comp_rate_guide = "Guía de la ritmo de la compresión";
        select_date_format = "Formato de fecha"

        // Score Option
        bls_setting = "Ajustes SVB"
        total_score = "Puntaje Total"
        reset_settings = "Restablecer configuración";

        stepbystep2line = "Práctica paso \na paso"
        wholestep_2line = "Práctica de todo \nel proceso"

        repeat_icon = "✎ Repetir";
        edit_icon = "✎ Editar";

        compressionRatio = "Fracción de compresiones"
        aed_infoTotalTxt = "Encienda el desfibrilador con antelación si está preparado para su uso.\n" +
        "Abra o corte la camisa del paciente para aplicar en su tórax los parches para \nel desfibrilador.\n" +
        "Asegúrese de que el paciente está despejado y aplique la descarga sobre él \npresionando el botón del desfibrilador.\n" +
        "En cuanto haya aplicado la descarga, tiene que continuar inmediatamente \ndespués con la RCP."

        login = "Iniciar sesión";
        sending = "Enviando…";
        select_cpr_standard = "Directriz RCP";
        select_language = "Idioma";
        //select_mannequin = "Posición del maniquí";
        select_set_num = "Ajustes del ciclo";
        first_setting_txt = "Si utiliza el programa por primera vez, siga los pasos que se indican para la configuración y el ajuste inicial.";
        scanning_kit = "Escaneando kit…";
        select_kit = "Seleccione kit";
        calibration_start = "Iniciar configuración";
        calibration = "Configuración";
        kit_id = "Identificación del kit:";
        new_ = "Nuevo";
        load = "Cargar";
        prev = "Anterior";
        chest_compression_calibration = "Configuración de la compresión";
        breath_calibration = "Configuración de la respiración";
        write_calibration_name = "";
        preset = "";
        custom = "";
        current = "Electricidad";
        compression_depth = "Intensidad de la compresión";
        breath_volume = "Volumen de la respiración";
        edit = "Editar";
        save = "Guardar";
        apply = "Aplicar";
        delete = "Borrar";
        set_breath = "Ajustar respiración";
        set_compression = "Ajustar compresión";
        compression30 = "Aplicar 15 compresiones";
        adequate5 = "Tomar cinco respiraciones profundas";
        lay_rescuer = "Reanimador";
        bls_rescuer = "Asistencia médica";
        start_course = "Iniciar curso";
        adult = "Adulto";
        infant = "Niño";
        start_test = "Iniciar test";
        manage_data = "Gestión de datos";
        add_student = "Añadir estudiante";
        student_info = "Información del estudiante";
        all_check = "Verificar todo";
        all_uncheck = "Desactivar todo";
        name = "Nombre";
        email = "Email";
        view_record = "Ver registros";
        data_selected = "registro seleccionado";
        datas_selected = "registros seleccionados";
        send_data = "Enviar datos";
        optional = "Opciones";
        register_kit = "Registrar Kit";
        heartisense_monitor = "HeartiSense Monitor";
        select_practice_mode = "Seleccione el modo práctica, por favor";
        step_by_step_mode_short = "Práctica paso a paso";
        whole_step_mode_short = "Práctica de todo el proceso";
        step_by_step_mode_full = "Práctica paso a paso";
        whole_step_mode_full = "Práctica de todo el proceso";
        adult_title = "Evaluación de SVB y DEA en adultos";
        infant_title = "Evaluación de SVB en niños";
        all_title = "Evaluación de SVB y DEA";
        assign_student = "Asignar estudiante";
        connect_kit = "Conectar kit";
        change_kit_1 = "Cambiar kit";
        change_kit_2 = "Cambiar kit";
        connecting_kit = "Conectando kit…";
        searching_kit = "Buscando kit…";
        compression_pad = "Parche para desfibrilador";
        breath_module = "Sistema de vías respiratorias";
        and = " y ";
        pass = "Aprobado";
        fail = "Suspenso";
        cycle = "Ciclo";
        ready = "Preparado";
        scenario_description = "Descripción del entorno";
        response = "Reacción";
        emergency = "Emergencia";
        check_pulse = "Comprobar pulso";
        compression = "Compresión";
        breath = "Respiración";
        cpr = "RCP";
        aed = "DEA";
        precpr_aha_bls = "Reacción\nEmergencia\nComprobar respiración\nComprobar pulso";
        precpr_aha_lay = "Reacción\nEmergencia\nComprobar respiración";
        precpr_erc = "Comprobar peligro\nReacción\nAbrir vía respiratoria\nComprobar respiración\nEmergencia";
        precpr_anzcor = "Comprobar peligro\nReacción\nEmergencia\nAbrir vía respiratoria\nComprobar respiración";
        disconnected = "Desconectado";
        connected = "Conectado";
        hands_off_time = "Retirar las manos";
        second = "seg.";
        minute = "min.";
        time = "Tiempo";
        timer = "Tiempo";
        position = "Posición";
        depth = "Intensidad";
        rate = "Ritmo";
        count = "Total";
        no_named = "Sin nombre";
        save_record = "Guardar registros";
        save_warning = "*Tenga en cuenta que los registros anteriores se borrarán al guardar los nuevos.";
        save_exist = "Registros existentes";
        step = "Paso";
        result = "Resultado";
        auto_connect = "Conectar automáticamente";
        result_guide = "* Número de éxitos/Número de intentos";
        save_all = "Guardar todo";
        saved = "Guardado";
        close = "Cerrar";
        view_detail = "Ver detalles";
        not_assigned = "Sin asignar";

        ///repeat = "Repetir";

        complete = "Completar";
        exist = "Existir";
        data = "Datos";
        student_name = "Nombre del estudiante";
        select = "Seleccionar";
        assign_result = "Encargo";
        detail = "Detalles";
        graph = "Gráfico";
        group = "Grupo";
        compression_rate = "Ritmo de la compresión";
        compression_position = "Posición para la compresión";
        duration = "Duración";
        average_rate = "Ritmo medio";
        recoil = "Retroceder";
        good_depth = "Bien";
        strong_depth = "Fuerte";
        fast = "Rápido";
        good_rate = "Bien";
        slow = "Lento";
        excessive = "Muy fuerte";
        good_amount = "Bien";
        insufficient = "Débil";
        recoil_needed = "Retroceder necesariamente";
        too_weak = "Muy débil";
        wrong_position = "Posición errónea";
        option_calibration = "    Configuración";
        option_preference = "    Ajustes iniciales";
        not_valid_email = "La dirección de email no es válida.";
        login_fail = "Email o contraseña incorrectos.";
        not_assigned_inst_id = "La identificación que ha introducido no está asignada a la institución.";
        server_error = "Se ha producido un error durante el envío de datos al servidor. Contacte con nosotros si el error persiste, por favor.";
        no_license = "La identificación que ha introducido no tiene licencia.";
        license_expiration = "Ha expirado la licencia de la identificación que ha introducido.";
        license_data_limit = "Ha expirado el número de licencia de los datos de su identificación.";
        connect_fail = "No se ha podido conectar con el kit.";
        connect_success = "Se ha conectado el kit de manera correcta.";
        disconnect_kit = "Conexión perdida.";
        using_data_calibration = "No puede borrar esta configuración ya que se encuentra en uso.";
        sensor_error_detected = "";
        sensor_error_detected_m = "";
        sensor_not_connected = " no está conectado";
        sensor_not_connected_m = " no están conectados.";
        at_least_1_connect = "El kit no está conectado.";
        not_permission = "No puede utilizar el programa con este kit de HeartiSense. \"Compruebe la marca del kit, por favor.\"";
        empty_name_or_email = "Introduzca su nombre y su dirección de email.";
        all_check_need = "No puede guardar los datos hasta que los estudiantes hayan sido evaluados.";
        not_valid_name = "Nombre no válido. (Use letras de la a-z, A-Z, números del 0 al 9, caracteres coreanos, - y _.)";
        search_the_kit = "Búsqueda automática del kit.";
        network_disconnected = "No está conectado a ninguna red.";
        bt_off_msg = "Se desconectarán todas las conexiones de Bluetooth.";
        need_connect_kit = "El kit debe estar conectado.";
        start_single_mode = "Utilizar el modo de aprendizaje individual.";
        start_monitor_mode = "Utilizar el modo Monitor.";
        bls_using = "Test HeartiSense en uso. Cerrar programa.";
        calibration_using = "Se está utilizando la configuración en el programa docente. El programa se cerrará.";
        wait_instructor = "Por favor, espere a las directrices del docente.";
        search_fail = "Búsqueda del kit errónea. Compruebe que el kit está encendido.";
        want_connect_retry = "Conexión con el kit errónea.";
        want_quit = "¿Está seguro de que desea cerrar el programa?";
        want_back_calibration = "¿Está seguro de que desea volver a la fase anterior? La configuración que ha creado no se guardará.";
        want_send = "¿Está seguro de que desea enviar los registros seleccionados al servidor?";
        want_delete = "¿Está seguro de que desea borrar los registros seleccionados?";
        want_main = "¿Está seguro de que desea volver al menú principal?";
        want_back = "¿Está seguro de que desea volver a la fase anterior?";
        want_turn_on_bluetooth = "¿Desea conectar el Bluetooth?";
        want_cali_main = "Guardado. ¿Desea ir a la página principal de la configuración?";
        want_disconnect = "¿Desea desconectarse?";
        want_restart_training = "¿Desea reanudar el entrenamiento?";
        want_restart_test = "¿Desea reanudar el test?";
        want_start_stepbystep_mode = "¿Desea comenzar con la práctica paso a paso?";
        want_start_wholestep_mode = "¿Desea comenzar con la práctica de todo el proceso?";
        want_delete_kit = "¿Desea extraer el kit registrado y cambiar al modo manual?";
        want_assign_completed = "¿Se ha completado el encargo? No se guardarán los datos de los estudiantes que no estén asignados.";
        want_restart_cpr = "¿Desea reiniciar la fase RCP?";
        quit = "Abandonar";
        reconnect = "Volver a conectar";
        send = "Enviar";
        confirm = "Confirmar";
        cancel = "Cancelar";
        yes = "Sí";
        no = "No";
        dont_save = "No guardar";
        existing_name = "Ese nombre ya existe";
        error = "Error";
        to_main = "Ir al menú principal";
        bluetooth_off = "Desconectar Bluetooth";
        disconnect = "Desconectar";
        restart = "Reiniciar";
        search = "Buscar";
        network_error = "Desconectado";
        skip = "Omitir";
        find_id_pw = "¿Ha olvidado su usuario/contraseña?";
        exit = "Salir";
//        bls_adult_1_title = "Destrezas SVB en adultos";
//        bls_adult_2_title = "Destrezas DEA";
//        bls_adult_2_check_1 = "Colocar DEA y Ambu";
//        bls_adult_2_check_2 = "Compresión continua";
//        bls_adult_2_check_3 = "Ventilar el entorno del paciente";
//        bls_adult_2_check_4 = "Suministrar descarga";
//        bls_adult_3_title = "Ventilación con Ambu";
//        bls_adult_3_check_1 = "Ciclo 1-Dar 30 compresiones";
//        bls_adult_3_check_2 = "Ciclo 2-Dar 30 compresiones";
//        bls_adult_3_check_3 = "Ciclo 1-Aplicar 2 respiraciones con el Ambu";
//        bls_adult_3_check_4 = "Ciclo 2-Aplicar 2 respiraciones con el Ambu";
//        bls_adult_sheet_title_1 = "Evaluación SVB en adultos del 1er reanimador";
//        bls_adult_sheet_check_1 = "Esperar si hay reacción (al menos 5 segundos)";
//        bls_adult_sheet_check_2 = "Pedir rescate de emergencia y DEA";
//        bls_adult_sheet_check_3 = "Comprobar si hay pulso (durante 10 segundos como máximo)";
        bls_adult_sheet_check_4 = "Llevar a cabo RCP";
        bls_adult_sheet_check_4_1 = "Tiempo de compresión";
        bls_adult_sheet_check_4_2 = "Compresión correcta";
        bls_adult_sheet_check_4_3 = "Dejar la compresión";
        bls_adult_sheet_check_4_4 = "Respiración correcta";
//        bls_adult_sheet_title_2 = "Evaluación DEA del 2º reanimador y cambio";
//        bls_adult_sheet_check_5 = "2º reanimador : Coloque DEA y Ambu, conecte DEA y coloque el parche.";
//        bls_adult_sheet_check_6 = "1er reanimador : Aplique compresión continua hasta que el DEA esté preparado ";
//        bls_adult_sheet_check_7 = "2º reanimador : Instrucciones sobre cuándo retirar las manos para el análisis";
//        bls_adult_sheet_check_8 = "2º reanimador : Si necesita el desfibrilador, informe de que va a retirar las manos y proceda a utilizarlo";
//        bls_adult_sheet_title_3 = "Evaluación de la ventilación con Ambu del 1er reanimador";
//        bls_adult_sheet_check_9 = "Lleve a cabo la RCP inmediatamente después de aplicar la descarga";
//        bls_adult_sheet_9_cc = "2º reanimador : Dar 30 compresiones";
//        bls_adult_sheet_9_rp = "1er reanimador : Aplicar 2 respiraciones con el Ambu";
//        bls_infant_1_title = "Destrezas del 1er reanimador de SVB en niños";
//        bls_infant_2_title = "Llevar a cabo RCP";
//        bls_infant_2_check_1 = "Disposición de los dedos";
//        bls_infant_2_check_2 = "Ritmo de la compresión";
//        bls_infant_2_check_3 = "Intensidad de la compresión";
//        bls_infant_2_check_4 = "Vuelta del tórax a su posición inicial";
//        bls_infant_2_check_5 = "Reduzca la suspensión de la compresión";
//        bls_infant_3_title = "RCP del 2º reanimador y cambio";
//        bls_infant_3_check_1 = "Cambiar posiciones";
//        bls_infant_3_check_2 = "Ciclo 1-Aplicar 2 respiraciones con el Ambu";
//        bls_infant_3_check_3 = "Ciclo 2-Aplicar 2 respiraciones con el Ambu";
//        bls_infant_3_check_4 = "Ciclo 1-Dar 15 compresiones";
//        bls_infant_3_check_5 = "Ciclo 2-Dar 15 compresiones";
//        bls_infant_sheet_title_1 = "Evaluación SVB en niños del 1er reanimador";
//        bls_infant_sheet_check_1 = "Esperar si hay reacción (al menos 5 segundos)";
//        bls_infant_sheet_check_2 = "Pedir rescate de emergencia y DEA";
//        bls_infant_sheet_check_3 = "Comprobar si hay pulso (durante 10 segundos como máximo)";
//        bls_infant_sheet_check_4 = "Llevar a cabo RCP";
//        bls_infant_sheet_check_4_1 = "Corregir la disposición de los dedos";
//        bls_infant_sheet_check_4_2 = "Ritmo adecuado (hasta 18 seg.)";
//        bls_infant_sheet_check_4_3 = "Intensidad adecuada";
//        bls_infant_sheet_check_4_4 = "Permitir la vuelta del tórax a su posición inicial";
//        bls_infant_sheet_check_4_5 = "Reducir la suspensión de la compresión (hasta 10 seg.)";
//        bls_infant_sheet_title_2 = "RCP del 2º reanimador y cambio";
//        bls_infant_sheet_check_5 = "Colocar DEA y Ambu y cambiar posiciones";
//        bls_infant_sheet_check_6 = "Llevar a cabo RCP";
//        bls_infant_sheet_check_6_1 = "2º reanimador : Aplicar 15 compresiones utilizando la técnica de compresión del tórax con los 2 pulgares y rodear el tórax con las manos (hasta 9 seg.)";
//        bls_infant_sheet_check_6_2 = "1er reanimador : Aplicar 2 respiraciones con el Ambu";
//        bls_infant_sheet_check_7 = "Llevar a cabo RCP";
//        bls_infant_sheet_check_7_1 = "1er reanimador : Aplicar 15 compresiones utilizando la técnica de compresión del tórax con los 2 pulgares y rodear el tórax con las manos (hasta 9 seg.)";
//        bls_infant_sheet_check_7_2 = "2º reanimador : Aplicar 2 respiraciones con el Ambu";
        response_info_title = "Confirmar Conocimiento";
        response_info_txt = "Confirme el grado de reacción del paciente sacudiendo suavemente sus hombros.";
        emergency_info_title = "Solicitud de Servicio de Emergencia";
        emergency_info_txt = "Pida a una persona de su alrededor que llame al 112.";
        pulsecheck_info_title = "Comprobar pulso";
        pulsecheck_info_txt = "Compruebe el pulso durante 5 a 10 segundos. Si no percibe el pulso, comience con la RCP.";
        compression_info_title = "Compresión";
        compression_info_txt = "Proceda con las compresiones.";
        respiration_info_title = "Respiración";
        respiration_info_txt = "Aplique la respiración boca a boca.";
        aed_info_title = "DEA";
        aed_info_1_txt = "Encienda el desfibrilador con antelación si está preparado para su uso.";
        aed_info_2_txt = "Abra o corte la camisa del paciente para aplicar en su tórax los parches para el desfibrilador.";
        aed_info_3_txt = "Asegúrese de que el paciente está despejado y aplique la descarga sobre él presionando el botón del desfibrilador.";
        aed_info_4_txt = "En cuanto haya aplicado la descarga, tiene que continuar inmediatamente después con la RCP.";
        press_sensor_pad = "Proceda con la compresión y así habrá comenzado el proceso de la RCP.";
        
        app_guide_address_trainer = "http://www.heartisense.com/pdf/Trainer_E.html";
        app_guide_address_monitor = "http://www.heartisense.com/pdf/Monitor_E.html";
        app_guide_address_test = "http://www.heartisense.com/pdf/Test_E.html";
        
        check_danger_info_title = "Comprobar peligro";
        check_danger_info_txt = "Antes de nada, compruebe si la víctima no se encuentra presenta en ningún lugar peligroso o que presente riesgos.";
        open_airway_info_title = "Abrir vía respiratoria";
        open_airway_info_txt = "Coloque su mano en la frente e incline la cabeza con cuidado; con la punta de sus dedos debajo de la barbilla de la víctima, levante la barbilla para abrir la vía respiratoria.";
        check_breath_info_title = "Comprobar respiración";
        check_breath_info_txt = "Mire, escuche y sienta durante no más de 10 segundos para determinar si la víctima está respirando con normalidad.";
        
        check_danger = "Comprobar peligro";
        open_airway = "Abrir vía respiratoria";
        check_breath = "Comprobar respiración";
        
    }
}
