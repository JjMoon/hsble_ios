//
//  HsCommonSetting.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2016. 1. 14..
//  Copyright © 2016년 IMLab. All rights reserved.
//

/** 설명

 # Header

 - 순서없는 리스트 (인덴트)

 1) 순서 리스트

 2)붙여 쓰면 인덴트가 없다.
 
 
 호흡 good : 4 ~ 12 sec

*/

import Foundation

let isDebugMode = false
let trainer0 = 0, monitor1 = 1, test2 = 2
let trainerVersion = "V 1.12.01", monitorVersion = "V 1.12.01", testVersion = "V 1.12.01"

var isTestOldCase = false

//var stageLimit = 3, isManikinLeft = true, langIdx = 0, state = 0  // UserDefaults 에 쓸 변수들.  // 0 : Eng, 1 : Kor
var instructorID: UInt = 0, isAdult = true, licenseID = 0, dateFormat = 0
var cprProtoN = HsBypassInt(), manikinKindN = HsBypassInt(), audioCountInterval = HsBypassInt(iniVal: 110), scoreWeight = 75
// AHA : 0, 1, 2
var scoreOption = [ true, true, true, true, true, true, false, true ]

let studentTableLimit = 100 // 테이블 뷰에 표시되는 최대 학생 수..

var globalState: CurStep = .S_INIT0

var langStr = Shl() // Monitor() //ShlTest()
var testInfant: Bool { get { return AppIdTrMoTe == 2 && !isAdult }}

/// Connection related
/// 모니터/테스트 접속 시 다른 뷰 disable 하는 시간.
let connUiDiableTime = 6.0

/// Calculation Static Vars
var Comp5cmPoint = 58, Comp6cmPoint = 84, Brth400mlPoint = 35, Brth700mlPoint = 70

let colorBtnBgGray = UIColor.grayColor()
//UIColor(colorLiteralRed: 0.3, green: 0.3, blue: 0.3, alpha: 1.0) // 회색 버튼 배경색.


/// Linkcon Ble Library 관련 작업 
var trnBleCtrl = HsBleCtrl() // 트레이너에서만 사용 싱글톤.

/// 서버 주소.
var wasURL = "https://www.heartisense.com/LMS/Monitor_communication/"

/// Debugging
let barDash = "---------- ", barLDash = "__________"
let barDashBase = "\(barDash) \(barDash) \(barDash) \(barDash) \(barDash) \(barDash) \n"
let barLDashBase = "\(barLDash) \(barLDash) \(barLDash) \(barLDash) \(barLDash) \(barLDash) \n"

let colorGreyBright = UIColor(red: 220, green255: 220, blue255: 220)
let colorTransparent = UIColor(red: 1, green: 0.96, blue: 0.32, alpha: 0)
let colorWhiteAlpha10 = UIColor(red255: 255, green255: 255, blue255: 255, alpha: 0.1)
let colorWhiteAlpha20 = UIColor(red255: 255, green255: 255, blue255: 255, alpha: 0.2)
let colorWhiteAlpha80 = UIColor(red255: 255, green255: 255, blue255: 255, alpha: 0.8)

let colorBarYelow = UIColor(red: 1, green: 0.96, blue: 0.32, alpha: 1)
let colorBarGreen = UIColor(red: 0.12, green: 0.98, blue: 0.33, alpha: 1)
let colorBarRedfg = UIColor(red: 1, green: 0.12, blue: 0.12, alpha: 1)
let colorBarBgYlw = UIColor(red: 0.82, green: 0.79, blue: 0.25, alpha: 0.5)
let colorBarBgGrn = UIColor(red: 0.07, green: 0.44, blue: 0.14, alpha: 0.8)
let colorBarBgRed = UIColor(red: 0.69, green: 0.07, blue: 0.1, alpha: 0.8)
let colorTxtGreen = UIColor(red255: 20, green255: 142, blue255: 56, alpha: 1)

let colBttnYellow = UIColor.init(hex: "F8B000")
let colBttnGreen = UIColor.init(hex: "69B97B")
let colBttnRed = UIColor.init(hex: "E54411")
let colBttnDarkGray = UIColor.init(hex: "5D5D5D")
let colBttnGray = UIColor.init(hex: "8E8E8E")
let colBttnBrightGray = UIColor.init(hex: "D6D6D6")
let colGraphGreenArea = UIColor.init(hex: "EFFFFA")
let colHalfTransGray = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 0.7)


/// OK Green   //  69 b9 7b
let colorOKGreen = UIColor(red: 80, green255: 200, blue255: 80)
let colorLabelGreen = UIColor(red: 105, green255: 185, blue255: 123)
let colorLblDarkGreen = UIColor(red: 80, green255: 200, blue255: 80)
/// Not Good  !!! Red
let colorNtGdRed = UIColor(red: 1, green: 0.12, blue: 0.12, alpha: 1)

let colorBttnActiveBlue = UIColor(red: 105, green255: 185, blue255: 123)
let colorBttnDarkGray = UIColor(red: 142, green255: 142, blue255: 142)
let colorBttnDarkGreen = UIColor(red: 86, green255: 216, blue255: 180)

let scorePassIndex = 70

/// Screen Size, iPhone 포팅 관련
var screenSize = UIScreen.mainScreen().bounds
var screenCenter = CGPointMake(screenSize.width/2, screenSize.height/2)
var enlargeScale: CGFloat = 2.2
var effectvWidth = screenSize.width * 0.95
var enlargedViewRect = CGRect(origin: screenCenter,  // 300 x 340
                              size: CGSize(width: effectvWidth, height: effectvWidth * 3.4 / 3))

var rectArr = [ CGRectMake(  5.0, 220.0, 180.0, 210.0),  // 이건 아이폰임..
                CGRectMake(190.0, 200.0, 180.0, 210.0),
                CGRectMake(  5.0, 390.0, 180.0, 210.0),
                CGRectMake(190.0, 390.0, 180.0, 210.0) ]

infix operator <^> {}
func <^> (object1: AnyObject!, object2: AnyObject!) -> Bool {
    return (object_getClassName(object1) == object_getClassName(object2))
}

class HmGraphSetting: NSObject {
    let trainerInitShlUse = langStr.obj.yes

    let fontSizeSmallTitle: CGFloat = 15 // 16

    let bttnBgGreen = UIColor(colorLiteralRed: 0.2, green: 0.7, blue: 0.2, alpha: 1.0)
    //let bttnBgDarkGray = UIColor(colorLiteralRed: 0.2, green: 0.2, blue: 0.2, alpha: 1.0)
    let viewBgGray = UIColor(colorLiteralRed: 0.95, green: 0.95, blue: 0.95, alpha: 1.0)

    let viewBgBrightGray = UIColor(red: 214, green255: 214, blue255: 214)

    //let graphGreen = UIColor(red: 86, green255: 216, blue255: 163) //(colorLiteralRed: 0.1, green: 0.6, blue: 0.1, alpha: 1.0)
    let graphYello = UIColor(red: 255, green255: 205, blue255: 92) //(colorLiteralRed: 0.9, green: 0.7, blue: 0.0, alpha: 1.0)
    let graphRed = UIColor(red: 216, green255: 69, blue255: 25)

    let warningRed = UIColor(colorLiteralRed: 0.95, green: 0.1, blue: 0.1, alpha: 1.0)
    let graphText = UIColor.init(hex: "5D5D5D")
        //UIColor(red: 105, green255: 185, blue255: 123) //(colorLiteralRed: 0.2, green: 0.1, blue: 0.1, alpha: 1.0)
    let graphAreaTransparentGreen = UIColor(colorLiteralRed: 0.1, green: 0.6, blue: 0.1, alpha: 0.1)
    let uiGreen = UIColor(red: 105, green255: 185, blue255: 123)

    static var inst = HmGraphSetting()
}


class Shl: NSObject { // SinHooLanguage ..
    var arrLangStr = [StrLocBase]()
    var myLangIdx = -1
    var obj: StrLocBase {
        get {
            if myLangIdx == -1 { setStrings() }
            return arrLangStr[Int(langIdx)]
        }
    }

    override init() {
        super.init()
        print("\n\t\t  Shl : Singleton Creation ...  \n\n");
        setStrings()
    }

    func setCurrentLanguageWith(idx: Int32) {
        print("\n\n\n  Shl :: setStrings  langIdx : \(idx)  \n\n\n")
        langIdx = idx
        HsBleSingle.inst().langObj = obj
        //HsBleSingle.inst().shlObj = langStr
    }

    func saveCPRProtocol(idx: Int32) {
        let ix = Int(idx)
        cprProtoN.setIntValue(Int(ix))
        NSUserDefaults.standardUserDefaults().setInteger(ix, forKey: "Guideline")
    }

    func saveManikinKind(idx: Int32) {
        let ix = Int(idx)
        manikinKindN.setIntValue(Int(ix))
        NSUserDefaults.standardUserDefaults().setInteger(ix, forKey: "ManikinKind")
    }

    func saveAudioCountInterval(itv: Int32) {
        let ix = Int(itv)
        audioCountInterval.setIntValue(Int(ix))
        NSUserDefaults.standardUserDefaults().setInteger(ix, forKey: "AudioCountInterval")
    }

    func saveDateFormat(itv: Int) {
        dateFormat = itv
        HsBleSingle.inst().dateFormat = Int32(dateFormat)
        NSUserDefaults.standardUserDefaults().setInteger(itv, forKey: "DateFormat")
    }

    /// Score 옵션 저장..
    func saveScoreOptions() {
        print("\n\t\t  Shl : saveScoreOptions  ...  \n\n");
        NSUserDefaults.standardUserDefaults().setObject(scoreOption, forKey: "ScoreOption")
        NSUserDefaults.standardUserDefaults().setInteger(scoreWeight, forKey: "ScoreWeight")
    }

    func setStrings() {
        print("\n\n\n  Shl :: setStrings  langIdx : \(langIdx)  \n\n\n")
        myLangIdx = Int(langIdx)

        arrLangStr = [StrLocBase]()
        arrLangStr.append(StrLocEng0())
        arrLangStr.append(StrLocDeu2())
        arrLangStr.append(StrLocSpn4()) // 인스턴스 생성.
        arrLangStr.append(StrLocFrn3())
        arrLangStr.append(StrLocKor1())

        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad {
            let x1: CGFloat = 58, x2: CGFloat = 404, y1: CGFloat = 190, y2: CGFloat = 560
            let w: CGFloat = 307, h: CGFloat = 337
            rectArr = [ CGRectMake(x1, y1, w, h), CGRectMake(x2, y1, w, h),
                        CGRectMake(x1, y2, w, h), CGRectMake(x2, y2, w, h) ]
        }

        for obj in arrLangStr {
            obj.setValues()
        }

        let usdf = NSUserDefaults.standardUserDefaults()
        if usdf.objectForKey("Guideline") != nil { // ([dObj integerForKey:@"Guideline"]) {
            cprProtoN.setIntValue(usdf.integerForKey("Guideline"))
            print("\n Shl :: CPR Protocol : \(cprProtoN.theVal) \n")
        }
        if usdf.objectForKey("ManikinKind") != nil {
            manikinKindN.setIntValue(usdf.integerForKey("ManikinKind"))
            print("\n Shl :: Manikin Kind : \(manikinKindN.theVal) \n")
        }
        if usdf.objectForKey("AudioCountInterval") != nil {
            audioCountInterval.setIntValue(usdf.integerForKey("AudioCountInterval"))
            print("\n Shl :: Audio Count Interval : \(audioCountInterval.theVal) \n")
        }
        print("\n Shl :: HsBleSingle.inst().langObj = self.obj  할당. \n")
        HsBleSingle.inst().langObj = self.obj
        print("\n Shl :: HsBleSingle.inst().langObj \(HsBleSingle.inst().langObj)  \n")
        //HsBleSingle.inst().shlObj = self 임...  여기서 재귀...
        //print("\n Shl :: HsBleSingle.inst().langObj = self.obj  할당. 완료 \n")

        HsGlobal.delay(0.1) {
            print(" delayed 0.1 sec >>>  HsBleSingle.inst().shlObj = self  // ..  여기서 재귀  할당.   langIdx : \(langIdx)")
            HsBleSingle.inst().shlObj = self  // ..  여기서 재귀...
            HsBleSingle.inst().langObj = self.obj
            HsBleSingle.inst().cprProto = cprProtoN
            HsBleSingle.inst().manikinKind = manikinKindN
            HsBleSingle.inst().audioCountInterval = audioCountInterval

            HsBleSingle.inst().scoreOption = scoreOption
            HsBleSingle.inst().scoreWeight = Int32(scoreWeight)


        }
    }
}
