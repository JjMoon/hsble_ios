//
//  CheckButtonManager.swift
//  Test
//
//  Created by Jongwoo Moon on 2016. 2. 16..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation


class CheckButtonManager: NSObject {
    var arrBttn = [UIButton]()
    var allBttn: UIButton?, labelCheckAll: UILabel?
    let checkImg = UIImage(named: "btn_check"), checkNotImg = UIImage(named: "btn_uncheck")


    func toggleCheckAll() { // All 버튼
        if allBttn?.tag == 1 {  selectAll(false)
        } else { selectAll(true) }
    }

    func toggleCheck(btn: UIButton) { // 일반 버튼..
        if btn.tag == 1 {
            forceCheck(btn, checked: false)
        } else {
            forceCheck(btn, checked: true)
        }
        refreshCheckAllButton()
    }

    private func selectAll(all: Bool) {
        var tagVal = 0, immg = checkNotImg
        if all { tagVal = 1; immg = checkImg }
        for btn in arrBttn {
            btn.tag = tagVal
            btn.setImage(immg, forState: .Normal)
        }
        allBttn?.tag = tagVal
        allBttn?.setImage(immg, forState: .Normal)
        refreshCheckAllButton()
    }

    private func forceCheck(btn: UIButton, checked: Bool) {
        if checked {
            btn.tag = 1; btn.setImage(checkImg, forState: .Normal)
        } else {
            btn.tag = 0; btn.setImage(checkNotImg, forState: .Normal)
        }
    }

    private func refreshCheckAllButton() {
        let checkedNum = arrBttn.filter { (btn) -> Bool in return btn.tag == 1 }.count
        if checkedNum < arrBttn.count { // 선택 안된 놈이 있다.
            forceCheck(allBttn!, checked: false)
            labelCheckAll?.text = langStr.obj.all_check
        } else {
            forceCheck(allBttn!, checked: true)
            labelCheckAll?.text = langStr.obj.all_uncheck
        }
    }
}

class HtButtonManBase: NSObject {
    var arrBttn = [UIButton]()
    var curActiveBttn: UIButton?

    var imgNameSelect = "select", imgNameUnselect = "unselect"

    func unSelectAll() { // 이미지와 태그를 리셋 ..
        for btn in arrBttn {
            btn.setImage(UIImage(named: imgNameUnselect), forState: .Normal)
            btn.tag = 0
        }
        curActiveBttn = nil
    }

    // MARK:  하나만 선택할 때..
    ///  arrBttn 에 버튼을 할당하고..  setImageBase 로 이미지 이름을 세팅하고..
    ///  selectThisBttn 을 콜 하면 됨...
    func setImageBase(selectImg: String, unselect: String) {
        imgNameSelect = selectImg
        imgNameUnselect = unselect
    }

    func selectThisBttn(btn: UIButton) {
        unSelectAll()
        curActiveBttn = btn
        curActiveBttn?.tag = 1
        curActiveBttn?.setImage(UIImage(named: imgNameSelect), forState: .Normal)
    }

}