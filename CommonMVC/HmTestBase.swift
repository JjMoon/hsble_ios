//
//  HmTestBase.swift
//  HS Test
//
//  Created by Jongwoo Moon on 2016. 11. 14..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation

class HmTest: HsJsonBase { // 한 사람이 이 객체를 2개 갖는다..
    var stdntID = 0, dbID = 0, baseKey = 0, standard = 0, amIAdult = false, timeStamp = NSDate()

    var pass = false

    var dicStep1 = [String: Bool?]()
    var dicStepRest = [String: Bool?]()

    private func intToOptBool(p: Int?) -> Bool? {
        if p == nil { return nil }
        return p! == 1
    }

    func setCommonData(rcrd: FMResultSet) {
        stdntID = readIntFromDB(rcrd, name: "StudentID")
        standard = readIntFromDB(rcrd, name: "Standard")
        timeStamp = readDate(rcrd, name: "theTime")

//        if let timeobj = rcrd.dateForColumn("thetime") {
//            timeStamp = timeobj
//        } else { timeStamp = NSDate() }
        //print(" stdntID  \(stdntID)  standard  \(standard)  \(timeStamp.formatYYMMDDspaceTime) ")
    }

    func setPropStep1FromDB(rcrd: FMResultSet) {
        // Response 의식 확인, Emergency 구조 요청, Check pulse 맥박 확인
        dicStep1["의식확인"] = readBoolFromOptInt(rcrd, name: "response") // Int 에서 Bool 로
        dicStep1["구조AED"] = readBoolFromOptInt(rcrd, name: "rescueaed")
        dicStep1["안전확인"] = readBoolFromOptInt(rcrd, name: "chksafety")
        dicStep1["기도확보"] = readBoolFromOptInt(rcrd, name: "airwaycfm")
        dicStep1["호흡확인"] = readBoolFromOptInt(rcrd, name: "chkbreath")
        dicStep1["맥박확인"] = readBoolFromOptInt(rcrd, name: "chkpulse")
        standard = readIntFromDB(rcrd, name: "standard")
        pass = readBoolFrom(rcrd, name: "pass")
        print("\(#function) : dicStep1 \(dicStep1)")
    }

    func step1Color(lbl: UILabel) {
        let falseNum = step1PropArr().filter { (bl) -> Bool in
            bl == false
        }.count
        if 0 < falseNum {
            lbl.backgroundColor = UIColor.clearColor()
            lbl.textColor = colBttnRed
        } else {
            lbl.backgroundColor = UIColor.clearColor()
            lbl.textColor = colBttnGreen
        }
    }

    func step1PropArr() -> [Bool] {
        print("  step1 : \(dicStep1)")

        switch standard {
        case 0: // "의식확인" "구조AED" "호흡확인" "맥박확인"
            return [ dicStep1["의식확인"]!!, dicStep1["구조AED"]!!, dicStep1["호흡확인"]!!, dicStep1["맥박확인"]!! ]
        case 1:
            return [ dicStep1["안전확인"]!!, dicStep1["의식확인"]!!, dicStep1["기도확보"]!!, dicStep1["호흡확인"]!!, dicStep1["구조AED"]!! ]
        default:
            return [ dicStep1["안전확인"]!!, dicStep1["의식확인"]!!, dicStep1["구조AED"]!!, dicStep1["기도확보"]!!, dicStep1["호흡확인"]!! ]
        }
    }

    func getBlsStepArray() -> [String] {
        var arrStr = ["abc"]
        print("\n\n getBlsStepArray : amIAdult \(amIAdult)  standard : \(standard) ")

        // 결과지에 쓰임.
        switch standard {
        case 0:
            arrStr = [ langStr.obj.bls_adult_1_check_response, langStr.obj.bls_adult_1_check_danger, langStr.obj.bls_adult_1_check_breath, langStr.obj.bls_adult_1_check_pulse ]
        case 1: // 안전확인,
            arrStr = [ langStr.obj.bls_adult_1_check_danger, langStr.obj.bls_adult_1_check_response,
                       langStr.obj.bls_adult_1_open_airway, langStr.obj.bls_adult_1_check_breath,
                       langStr.obj.bls_adult_1_shouts_for_help ]
        default:
            arrStr = [ langStr.obj.bls_adult_1_check_danger, langStr.obj.bls_adult_1_check_response,
                       langStr.obj.bls_adult_1_shouts_for_help, langStr.obj.bls_adult_1_open_airway,
                       langStr.obj.bls_adult_1_check_breath ]
        }
        return arrStr
    }

    /// UI 에서 화면 넘어가면서 값 저장할 때.
    func setStepBaseProps(dic: [String:Int]) {
        print("\n\n")
        for obj in dic {
            dicStep1[obj.0] = intToOptBool(obj.1)
            print("Test Base Property >>  \(obj.0)  ::  \(obj.1)")
        }
        print("\n\n")
    }
    /// UI 에서 화면 넘어가면서 값 저장할 때. 영아/성인 공통 사용.
    func setStepRestProps(dic: [String:Int]) {
        print("\n\n")
        for obj in dic {
            dicStepRest[obj.0] = intToOptBool(obj.1)
            print("Test Rest Property >>  \(obj.0)  ::  \(obj.1)  adult <\(isAdult)>")
        }
        print("\n\n")
    }

    func setPropStepOwnFromDB(rcrd: FMResultSet) {
    }

    func getInt1(ky: String) -> String {
        print("dicStep1[ky]  >>  \(dicStep1[ky])")
        if let bval = dicStep1[ky] {
            if bval == nil { return "NULL" }
            if bval! == true { return "1" }
            return "0"
        }
        return "NULL"
    }

    func getInt2(ky: String) -> String {
        if dicStepRest[ky] != nil {
            if dicStepRest[ky]! == true { return "1" }
            return "0"
        }
        return "NULL"
    }

    /// Bool 을 JSON 에서 0, 1 로..
    func bool2int(bol: Bool?) -> Int {
        if bol == nil { return 0 }
        if bol! { return 1 }
        return 0
    }

    func sqlTestString(stdID: Int) -> String {
        stdntID = stdID
        // dbID = stdID
        return "INSERT INTO TEST (StudentID, response, rescueaed, chksafety, airwaycfm, chkbreath, chkpulse, standard, pass, theTime)" +
            " VALUES ( \(stdID), \(getInt1("의식확인")), \(getInt1("구조AED")), \(getInt1("안전확인")), \(getInt1("기도확보")), \(getInt1("호흡확인")), \(getInt1("맥박확인")), \(standard), \(pass.toInt()), '\(timeStamp.formatYYMMDDspaceTime)' )"
    }

    func addBaseJson() {
        let js = HsJsonBase()
        switch standard {
        case 1:
            /*  return [ dicStep1["안전확인"]!!, dicStep1["의식확인"]!!, dicStep1["기도확보"]!!, dicStep1["호흡확인"]!!, dicStep1["구조AED"]!! ]
             "testERC": {
             #      "testERC_1": 1, //안전확인                       #      "testERC_2": 1, //의식유무확인
             #      "testERC_3": 1, //기도확보                       #      "testERC_4": 1, //호흡상태확인
             #      "testERC_5": 1  //AED 요청 및 도움요청
             */
            js.addKeyValue("testERC_1", value: bool2int(dicStep1["안전확인"]!))
            js.addKeyValue("testERC_2", value: bool2int(dicStep1["의식확인"]!))
            js.addKeyValue("testERC_3", value: bool2int(dicStep1["기도확보"]!))
            js.addKeyValue("testERC_4", value: bool2int(dicStep1["호흡확인"]!))
            js.addKeyValue("testERC_5", value: bool2int(dicStep1["구조AED"]!), withFinishComma: false)
            js.makeObject()
            addKeyArray("testERC", value: js.jStr)
        case 2:
            /*  "testNZ": {  "testNZ_1": 1, //안전확인  "testNZ_2": 1, //의식유무확인
                    "testNZ_3": 1, //AED 요청 및 도움요청 "testNZ_4": 1, //호흡상태확인  "testNZ_5": 1  //기도확보    */
            js.addKeyValue("testNZ_1", value: bool2int(dicStep1["안전확인"]!))
            js.addKeyValue("testNZ_2", value: bool2int(dicStep1["의식확인"]!))
            js.addKeyValue("testNZ_3", value: bool2int(dicStep1["구조AED"]!))
            js.addKeyValue("testNZ_4", value: bool2int(dicStep1["기도확보"]!))
            js.addKeyValue("testNZ_5", value: bool2int(dicStep1["호흡확인"]!), withFinishComma: false)
            js.makeObject()
            addKeyArray("testNZ", value: js.jStr)
        default: // return [ dicStep1["의식확인"]!!, dicStep1["구조AED"]!!, dicStep1["호흡확인"]!!, dicStep1["맥박확인"]!! ]
            /* #"testAHA": {
                #      "testAHA_1": 1, //의식유무확인
                #      "testAHA_2": 1, //AED 요청 및 도움요청
                #      "testAHA_3": 1, //호흡상태 확인
                #      "testAHA_4": 1  //맥박확인
                #},  */
            js.addKeyValue("testAHA_1", value: bool2int(dicStep1["의식확인"]!))
            js.addKeyValue("testAHA_2", value: bool2int(dicStep1["구조AED"]!))
            js.addKeyValue("testAHA_3", value: bool2int(dicStep1["호흡확인"]!))
            js.addKeyValue("testAHA_4", value: bool2int(dicStep1["맥박확인"]!), withFinishComma: false)
            js.makeObject()
            addKeyArray("testAHA", value: js.jStr)
        }
    }
}


class HmTestAdlt: HmTest {

    // AED 관련
    // AED 의 전원을 켬, 패드를 올바르게 부착, 분석을 위해 환자에게서 물러남, 안전한 제세동을 위해 환자에게서 물러남, 안전하게 제세동 전달
    // 제세동 전달 후 곧바로 압박 재개

    override init() {
        super.init()
        amIAdult = true
    }
    convenience init(record: FMResultSet, adultRecord: FMResultSet) {
        self.init()
        setCommonData(record)
        setPropStep1FromDB(record)
        setPropStepOwnFromDB(adultRecord)
    }
    
    override func setPropStepOwnFromDB(rcrd: FMResultSet) {
        baseKey = Int(rcrd.intForColumn("ParentID"))

        // "전원켬" "패드부착" "분석물러남" "제세동물러남" "제세동" "압박재개"
        dicStepRest["전원켬"] = readBoolFromOptInt(rcrd, name: "aedturnon") // Int 에서 Bool 로
        dicStepRest["패드부착"] = readBoolFromOptInt(rcrd, name: "attachpad")
        dicStepRest["분석물러남"] = readBoolFromOptInt(rcrd, name: "analysis")
        dicStepRest["제세동물러남"] = readBoolFromOptInt(rcrd, name: "retreat")
        dicStepRest["제세동"] = readBoolFromOptInt(rcrd, name: "deliveraed")

        dicStepRest["압박재개"] = readBoolFromOptInt(rcrd, name: "cpragain")

         // rcrd.read dateForColumn("thetime") {
            // timeStamp = timeobj        } else { timeStamp = NSDate() }
    }

    func getAEDrslts() -> [Bool] {
        return [ dicStepRest["전원켬"]!!, dicStepRest["패드부착"]!!, dicStepRest["분석물러남"]!!, dicStepRest["제세동물러남"]!!, dicStepRest["제세동"]!! ]
    }

    func showMySelf() {
        log.logThis(" Adult :  StdnID : \(stdntID)  \(timeStamp.formatYYMMDDspaceTime)  \(dicStep1)   dbID : \(dbID)   baseKey : \(baseKey) ")
    }
    func sqlAdultString(parentID: Int) -> String {
        return "INSERT INTO TEST_ADULT (ParentID, aedturnon, attachpad, analysis, retreat, deliveraed, cpragain) " +
            "VALUES ( \(parentID), \(getInt2("전원켬")), \(getInt2("패드부착")), \(getInt2("분석물러남")), \(getInt2("제세동물러남")), \(getInt2("제세동")), \(getInt2("압박재개")) )"
    }

    func adultJson() -> String {
        jStr = "" // 초기화..
        addBaseJson()
        /* "AED": {
            "poweron_aed": 1,
            "attachpad_aed": 1,
            "analysis_aed": 1,
            "backoff_aed": 1,
            "safety_shock_aed": 1
        },
         "resume_aed_after": 1,    */

//        if 10 < opStr.getLength() { // 측정 결과가 있어야...
//            addKeyString("test4", value: " { \(test4Str) } ")
//        }

        let js = HsJsonBase()
        js.addKeyValue("poweron_aed", value: bool2int(dicStepRest["전원켬"]!))
        js.addKeyValue("attachpad_aed", value: bool2int(dicStepRest["패드부착"]!))
        js.addKeyValue("analysis_aed", value: bool2int(dicStepRest["분석물러남"]!))
        js.addKeyValue("backoff_aed", value: bool2int(dicStepRest["제세동물러남"]!))
        js.addKeyValue("safety_shock_aed", value: bool2int(dicStepRest["제세동"]!), withFinishComma: false)
        js.makeObject()
        addKeyArray("AED", value: js.jStr)

        addKeyValue("resume_aed_after", value: bool2int(dicStepRest["압박재개"]!))
        addKeyValue("pass", value: bool2int(pass), withFinishComma: false)

        return jStr
    }

    func step3Color(lbl: UILabel) {
        let falseNum = getAEDrslts().filter { (bl) -> Bool in bl == false }.count
        if 0 < falseNum {
            lbl.backgroundColor = UIColor.clearColor()
            lbl.textColor = colBttnRed
        } else {
            lbl.backgroundColor = UIColor.clearColor()
            lbl.textColor = colBttnGreen
        }
    }

    func step4Color(lbl: UILabel) {
        if dicStepRest["압박재개"]!! {
            lbl.backgroundColor = UIColor.clearColor()
            lbl.textColor = colBttnGreen
        } else {
            lbl.backgroundColor = UIColor.clearColor()
            lbl.textColor = colBttnRed
        }
    }

}


class HmTestInft: HmTest {

    // CPR check  7개의 체크 사항.
    // 1 / 2 Cycle 올바른 가습 압박, 인공 호흡    10초 미만의 압박 중단 시간.
    // 3 / 4 Cycle
    override init() {
        super.init()
        amIAdult = false
    }
    convenience init(record: FMResultSet, infantRecord: FMResultSet) {
        self.init()
        setCommonData(record)
        setPropStep1FromDB(record)
        setPropStepOwnFromDB(infantRecord)
    }

    override func setPropStepOwnFromDB(rcrd: FMResultSet) {
        // "압박1" "호흡1" "압박2" "호흡2" "중단시간" "세트3" "세트4"

        baseKey = Int(rcrd.intForColumn("ParentID"))

        dicStepRest["압박1"] = readBoolFromOptInt(rcrd, name: "cyc1comp") // Int 에서 Bool 로
        dicStepRest["호흡1"] = readBoolFromOptInt(rcrd, name: "cyc1breath")
        dicStepRest["압박2"] = readBoolFromOptInt(rcrd, name: "cyc2comp") // Int 에서 Bool 로
        dicStepRest["호흡2"] = readBoolFromOptInt(rcrd, name: "cyc2breath")
        dicStepRest["중단시간"] = readBoolFromOptInt(rcrd, name: "handsofftime")

        dicStepRest["세트3"] = readBoolFromOptInt(rcrd, name: "cycle3")
        dicStepRest["세트4"] = readBoolFromOptInt(rcrd, name: "cycle4")

        print("\(#function) : dicStepRest \(dicStepRest)")

    }

    func showMySelf() {
        log.logThis(" StdnID : \(stdntID)  \(timeStamp.formatYYMMDDspaceTime)  \(dicStep1)   dbID : \(dbID)   baseKey : \(baseKey) ")
    }
    func sqlInfantString(parentID: Int) -> String {
        baseKey = parentID
        return "INSERT INTO TEST_INFANT (ParentID, cyc1comp, cyc1breath, cyc2comp, cyc2breath, handsofftime, cycle3, cycle4) " +
        "VALUES ( \(parentID), \(getInt2("압박1")), \(getInt2("호흡1")), \(getInt2("압박2")), \(getInt2("호흡2")), \(getInt2("중단시간")), \(getInt2("세트3")), \(getInt2("세트4")) )"
    }

    func infantJson() -> String {
        jStr = "" // 초기화..
        addBaseJson()
        /* "comp_resp_data": {
            "comp_check1": 1,            "comp_check2": 1,            "comp_check3": 1,
            "resp_check1": 1,            "resp_check2": 1,            "handsoff_check": 1,
            "bvm_check": 1     },  */
        let js = HsJsonBase()
        js.addKeyValue("comp_check1", value: bool2int(dicStepRest["압박1"]!))
        js.addKeyValue("comp_check2", value: bool2int(dicStepRest["압박2"]!))
        js.addKeyValue("resp_check1", value: bool2int(dicStepRest["호흡1"]!))
        js.addKeyValue("resp_check2", value: bool2int(dicStepRest["호흡2"]!))
        js.addKeyValue("handsoff_check", value: bool2int(dicStepRest["중단시간"]!))
        js.addKeyValue("comp_check3", value: bool2int(dicStepRest["세트3"]!))
        js.addKeyValue("bvm_check", value: bool2int(dicStepRest["세트4"]!), withFinishComma: false)
        js.makeObject()
        addKeyArray("comp_resp_data", value: js.jStr)

        addKeyValue("pass", value: bool2int(pass), withFinishComma: false)
        // "testNZ" :  { "testNZ_1" : 1, "testNZ_2" : 0, "testNZ_3" : 1, "testNZ_4" : 0, "testNZ_5" : 0 } , "comp_resp_data" :  { "comp_check1" : 0, "comp_check2" : 1, "comp_check3" : 1, "resp_check1" : 0, "resp_check2" : 0, "handsoff_check" : 1, "bvm_check" : 0 } , "pass" : 0
        return jStr
    }

    func stepRestColor(lbl: UILabel) {
        let falseNum = dicStepRest.filter({ (ky, vl) -> Bool in
            vl == false
        }).count
        if 0 < falseNum {
            lbl.backgroundColor = UIColor.clearColor()
            lbl.textColor = colBttnRed
        } else {
            lbl.backgroundColor = UIColor.clearColor()
            lbl.textColor = colBttnGreen
        }
    }
}
