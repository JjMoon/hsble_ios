//
//  ReadCSText.swift
//  HsTrial
//
//  Created by Jongwoo Moon on 2016. 8. 30..
//  Copyright © 2016년 Jongwoo Moon. All rights reserved.
//

import Foundation

@objc
class ReadCommaSeperatedText: NSObject {

    static func parseFiles(fs: NSArray, dr: NSString) { // 액셀 파일 읽기.
        for fl in fs {
            //print(" 클래스 알아내기 >> getClass >>  \(object_getClass(fl))  \(fl) ")
            let filePath = String((dr as String) + "/" + String(fl) )
            print(" 파일 위치 >>  \(filePath)  ")

            let spreadsheet: BRAOfficeDocumentPackage = BRAOfficeDocumentPackage.open(filePath)
            let firstWorksheet: BRAWorksheet? = spreadsheet.workbook.worksheets[0] as? BRAWorksheet

            var lineN = 2

            if firstWorksheet != nil {
                defer {
                    // aStreamReader!.close()
                }
                var stop = false
                repeat {
                    var name = "", mail = ""
                    let nameObj = firstWorksheet!.cellForCellReference("A\(lineN)")
                    let mailObj = firstWorksheet!.cellForCellReference("B\(lineN)")
                    if nameObj != nil {
                        name = nameObj!.stringValue()
                        print("  name : \(nameObj!.stringValue())")
                    }
                    if mailObj != nil {
                        mail = mailObj!.stringValue()
                        print("  mail : \(mailObj!.stringValue())")
                    }
                    lineN += 1

                    let pureName = name.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
                    mail = mail.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())

                    print("  name without whitespace : \(pureName)   \(mail)")

                    stop = pureName == "" && mail == "" // 이메일 오류는 멈추는 조건은 아님...

                    if 0 < mail.getLength() && !mail.isValidEmail() {
                        mail = ""
                    }
                    let effective = 0 < pureName.getLength() || 0 < pureName.getLengthUTF32()
                    if !stop && effective {
                        // 이메일 형식 체크 / 빈칸.
                        print("  \t\t\t \t\t\t Add  __\(name)_   __\(mail)_    >>>>>>  ")
                        HsBleMaestr.inst.addStudent(name, em: mail, pw: "", phn: "", ag: 0)
                    }
                } while !stop
            }

            // Delete file
            do {
                try NSFileManager.defaultManager().removeItemAtPath(filePath)
            }
            catch let error as NSError {
                print("Ooops! Something went wrong: \(error)")
            }
        }

    }

    static func parseFilesTextFormat(fs: NSArray, dr: NSString) {
        print(" ReadCommaSeperatedText  files : \(fs.count) ")



        for fl in fs {
            print(" 클래스 알아내기 >> getClass >>  \(object_getClass(fl))  \(fl) ")

            let filePath = String((dr as String) + "/" + String(fl) )
            let aStreamReader = HtFileReader(path: filePath)

            if aStreamReader != nil {
                defer {
                    aStreamReader!.close()
                }
                while let line = aStreamReader!.nextLine() { // 모든 라인 파싱....



                    print("\n\n new Line Data >>  \(line)")
                }
            }

            // Delete file
            do {
                try NSFileManager.defaultManager().removeItemAtPath(filePath)
            }
            catch let error as NSError {
                print("Ooops! Something went wrong: \(error)")
            }
        }
    }
}