//
//  HtUserDef.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2015. 11. 17..
//  Copyright © 2015년 IMLab. All rights reserved.
//

import Foundation

//////////////////////////////////////////////////////////////////////     _//////////_     [ Ht User Def     << >> ]    _//////////_
// MARK: User Defaults  파일 입출력..  관련 ..  HsBleMaestr 가 갖고 있슴.

class HtUserDef : NSObject {  //
    var log = HtLog(cName: "HtUserDef")
    let uDef = NSUserDefaults.standardUserDefaults()
    
    var arrPrevDevName:[String]?
    
    override init() {
        super.init()
        log.printThisFunc("생성자", lnum: 5)
        arrPrevDevName = uDef.objectForKey("arrPrevDevName") as? [String]
        
        log.logThis(" 아래는 HsBleSingle에서 먼저 실행되어야..  arrPrevDevName   \(arrPrevDevName)  ", lnum: 1)

        initialCheck()
        
        //if uDef.objectForKey("instructorID") == nil { instructorID = 0 }
        //else { instructorID = (uDef.objectForKey("instructorID") as? UInt)! }
        log.printAlways("   Stage Limit :  \(HsBleSingle.inst().stageLimit)   lanIdx : \(langIdx)   0: en,   1: 한글 , instuctorID : \(instructorID)")
        
        if arrPrevDevName == nil {
            log.logThis("  no Prev Dev Name ...   set   _ _ _ _ ", lnum: 1)
            arrPrevDevName = ["_", "_", "_", "_"]
        } else {
            log.logThis("  arrPrevDevName  ::  \(arrPrevDevName?.count)     \(arrPrevDevName) ", lnum: 2)
        }
    }

    func initialCheck() {
        if uDef.objectForKey("StageLimit") != nil {
            HsBleSingle.inst().stageLimit = Int32(uDef.integerForKey("StageLimit"))
            log.logThis("StageLimit : 읽기 OK   \(HsBleSingle.inst().stageLimit)")
        } else {
            uDef.setInteger(Int(HsBleSingle.inst().stageLimit), forKey: "StageLimit")
            log.logThis("stageLimit : 새로 쓰기 OK   \(HsBleSingle.inst().stageLimit)")
        }
        if uDef.objectForKey("ManikinDirection") != nil {
            isManikinLeft = uDef.boolForKey("ManikinDirection")
            log.logThis("isManikinLeft : 읽기 OK   \(isManikinLeft)")
        } else {
            uDef.setBool(isManikinLeft, forKey: "ManikinDirection")
            log.logThis("ManikinDirection : 새로 쓰기 OK   \(isManikinLeft)")
        }
        if uDef.objectForKey("Language") != nil {
            langIdx = Int32(uDef.integerForKey("Language"))
            log.logThis("langIdx : 읽기 OK   \(langIdx)")
        } else {
            uDef.setInteger(Int(langIdx), forKey: "Language")
            log.logThis("langIdx : 새로 쓰기 OK   \(langIdx)  0: en,   1: 한글")
        }
        /// 점수 옵션 저장..
        if uDef.objectForKey("ScoreOption") != nil {
            if let scrArr : AnyObject? = uDef.objectForKey("ScoreOption") {
                scoreOption = scrArr! as! [Bool]
                log.logThis("ScoreOption : 읽기 OK   \(scoreOption)")
            }
        } else {
            uDef.setObject(scoreOption, forKey: "ScoreOption")
            log.logThis("ScoreOption :   \(scoreOption) ")
        }
        if uDef.objectForKey("ScoreWeight") != nil {
            scoreWeight = uDef.integerForKey("ScoreWeight")
        } else {
            uDef.setInteger(scoreWeight, forKey: "ScoreWeight")
        }
        if uDef.objectForKey("DateFormat") != nil {
            dateFormat = uDef.integerForKey("DateFormat")
            log.logThis("DateFormat : 읽기 OK   \(dateFormat)")
        } else {
            uDef.setInteger(dateFormat, forKey: "DateFormat")
            log.logThis("DateFormat : 새로 쓰기 OK   \(dateFormat) ")
        }

    }
    
    func writeInstructorID() { // 이건 디폴트로 안 써도 됨..  기본이 0 ..
        if 0 < instructorID {
            uDef.setObject(instructorID, forKey: "instructorID")
        }
    }
    
    func writePrevDevName() { uDef.setObject(arrPrevDevName, forKey: "arrPrevDevName") }
}
