//
//  HsBaseDBDel.swift
//  HS Test
//
//  Created by Jongwoo Moon on 2016. 8. 10..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation

extension HsBaseDB {


    func updateAStudent(sObj: HmStudent) -> Bool {
        log.printThisFunc("add A Student", lnum: 5)
        let curDB = FMDatabase(path: dbPath as String)
        var rVal = false
        if curDB.open() {
            let sql = "UPDATE STUDENT SET SvrID = \(sObj.svrID), SvrSaved = \(sObj.svrSaved), " +
                "Name = '\(sObj.name)', Email = '\(sObj.email)' WHERE ID = \(sObj.dbid)"
            log.logThis(sql, lnum: 1)
            let rslt = curDB.executeUpdate(sql, withArgumentsInArray: nil)
            if !rslt { log.printAlways("  Error ", lnum: 10); rVal = true } // error
            else {
                log.printAlways(" Update :: OK   \(sObj.name),  ID : \(sObj.dbid), SvrID : \(sObj.svrID) ", lnum: 5) }
        }
        curDB.close()
        return rVal
    }


    //////////////////////////////////////////////////////////////////////     _//////////_     [     ]    _//////////_   삭제
    // MARK:  database ..   Delete  >>>  삭제 ..

    func deleteFromTable(tableName: String, condStr: String) -> Bool {
        log.printThisFNC("deleteFromTable", comment: "\(tableName)  >> \(condStr)" )
        let curDB = FMDatabase(path: dbPath as String)
        var rVal = false
        if curDB.open() {
            let sql = "Delete From \(tableName) WHERE \(condStr)"
            log.logThis(sql, lnum: 1)
            let rslt = curDB.executeUpdate(sql, withArgumentsInArray: nil)
            if !rslt { log.printAlways("  Error ", lnum: 10); rVal = true } // error
            else { log.printAlways(" Delte :: OK  ", lnum: 1) }
        }
        curDB.close()
        return rVal
    }

    func deleteStudent(sObj: HmStudent) -> Bool {
        log.printThisFunc("deleteStudent A Student", lnum: 5)
        let curDB = FMDatabase(path: dbPath as String)
        var rVal = false
        if curDB.open() {
            defer {
                curDB.close()
            }
            let sql = "Delete From STUDENT Where Name is '\(sObj.name)'"
            let rslt = curDB.executeUpdate(sql, withArgumentsInArray: nil)
            if !rslt { log.printAlways("  Error ", lnum: 10); rVal = true } // error
            else { log.printAlways(" Delete :: OK   \(sObj.name) ", lnum: 5) }
        }

        deleteSetsDataOf(sObj.dbid)
        return false
    }

    func deleteSetsDataOf(studentID: Int) {
        log.printThisFunc("delete all sets of student : \(studentID)", lnum: 5)

        let curDB = FMDatabase(path: dbPath as String)
        var arrSetID = [Int]()
        if curDB.open() { // 데이터 삭제..
            let sql = "DELETE from DATA where Owner = \(studentID)"
            log.logThis(sql, lnum: 1)
            curDB.executeUpdate(sql, withArgumentsInArray: nil)
        };   curDB.close()

        if curDB.open() {  // let sql = "UPDATE STUDENT SET SvrID = \(sObj.svrID) SvrSaved = \(sObj.svrSaved) WHERE ID = \(sObj.dbid)"
            let sql = "SELECT * from UNITSET where Owner = \(studentID)"
            log.logThis(sql, lnum: 1)
            let rslt:FMResultSet? = curDB.executeQuery(sql, withArgumentsInArray:  nil)
            while rslt?.next() == true {
                arrSetID.append( Int(rslt!.stringForColumn("ID"))! )
            }
        };   curDB.close()
        // 스트로크 들을 지운다..
        for id in arrSetID {
            if curDB.open() { // let sql = "UPDATE STUDENT SET SvrID = \(sObj.svrID) SvrSaved = \(sObj.svrSaved) WHERE ID = \(sObj.dbid)"
                let sql = "DELETE from COMPSTROKE where SetID = \(id)"
                log.logThis(sql, lnum: 1)
                curDB.executeUpdate(sql, withArgumentsInArray: nil)
            }; curDB.close()
            if curDB.open() { // let sql = "UPDATE STUDENT SET SvrID = \(sObj.svrID) SvrSaved = \(sObj.svrSaved) WHERE ID = \(sObj.dbid)"
                let sql = "DELETE from  BRTHSTROKE where SetID = \(id)"
                log.logThis(sql, lnum: 1)
                curDB.executeUpdate(sql, withArgumentsInArray: nil)
            }; curDB.close()
        }
        // 세트들을 지운다..
        if curDB.open() { // let sql = "UPDATE STUDENT SET SvrID = \(sObj.svrID) SvrSaved = \(sObj.svrSaved) WHERE ID = \(sObj.dbid)"
            let sql = "DELETE from UNITSET where OWNER = \(studentID)"
            log.logThis(sql, lnum: 1)
            curDB.executeUpdate(sql, withArgumentsInArray: nil)
        }; curDB.close()

        grabAllSet()
    }
}
