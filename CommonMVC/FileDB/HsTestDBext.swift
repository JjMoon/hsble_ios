//
//  HsTestDBext.swift
//  HS Test
//
//  Created by Ryan on 2016. 12. 2..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation

extension HsMainDB {
    
    ///  기존 테이블이 있는 지 검색.
    func 이전_Test_테이블이_있는지() -> Bool {
        log.printThisFunc("versionCheckAndUpgrade_Test3", lnum: 10)
        let curDB = FMDatabase(path: dbPath as String)
        
        if curDB.open() {
            defer { curDB.close() }
            return curDB.tableExists("INFANT1")
        }
        return false // 에러 케이스
    }
    
    func 이전_DB_전송데이터_있는지(addStudent: Bool) -> Bool {
        이전_DB_읽기(addStudent)
        let arrStd = HsBleMaestr.inst.arrStudent
        for obj in arrStd {
            print("  \(obj.name) : old Data : \(obj.arrTestOld.count)  Send ? : \(전송데이터_있나(obj))")
        }
        print("\n\n\n\n\n")
        return true
    }
    
    func 이전데이터_보유_학생() -> [HmStudent] {
        return HsBleMaestr.inst.arrStudent.filter { (st) -> Bool in
            전송데이터_있나(st)
        }
    }
    
    func 이전데이터삭제() {
        let curDB = FMDatabase(path: dbPath as String)
        let tableName = [ "ADULT1", "ADULT2", "INFANT1", "INFANT2" ]
        
        if curDB.open() {
            defer { curDB.close() }
            for (_, name) in tableName.enumerate() {
                let sql = "DROP TABLE \(name)"
                let rsl = curDB.executeUpdate(sql, withArgumentsInArray:  nil)
                print(" \(#file) > \(#function)  >> \(sql)  <\(rsl)> ")
            }
        }
    }
    
    /// 학생별로 데이터 검색.
    func 전송데이터_있나(obj: HmStudent) -> Bool {
        var inf = 0, adt = 0, adtOperate = false
        for dt in obj.arrTestOld {
            if dt.amIAdult { adt += 1 }
            else { inf += 1 }
        }
        adtOperate = adt == 2 && obj.hasOperateData
        print(" inf : \(inf) , adult : \(adt) , obj.hasOperateData : \(obj.hasOperateData)")
        return inf == 2 || adtOperate
    }
    
    func 이전_DB_읽기(addStudent: Bool) {
        print(" \(#function) ")
        let curDB = FMDatabase(path: dbPath as String)
        
        let tableName = [ "ADULT1", "ADULT2", "INFANT1", "INFANT2" ]
        
        for st in HsBleMaestr.inst.arrStudent {
            st.arrTestOld.removeAll()
        }
        
        if curDB.open() {
            for (ix, name) in tableName.enumerate() {
                let sql = "SELECT * from  \(name)"
                log.logThis(sql, lnum: 1)
                let rslt:FMResultSet? = curDB.executeQuery(sql, withArgumentsInArray:  nil)
                var aObj: HmTestBase?
                while rslt?.next() == true {
                    switch ix {
                    case 0: aObj = HmTestAdult(rcrd: rslt!, is1st: true)
                    aObj!.isRes1 = true
                    case 1: aObj = HmTestAdult(rcrd: rslt!, is1st: false)
                    aObj!.isRes2 = true
                    case 2: aObj = HmTestInfant(rcrd: rslt!, is1st: true)
                    aObj?.isRes1 = true
                    default: aObj = HmTestInfant(rcrd: rslt!, is1st: false)
                    aObj!.isRes2 = true
                    }
                    
                    if addStudent { addAdultToStudent(aObj!) }
                }
            }
        }
        curDB.close()
    }
    
    /// Adult 객체를 해당 학생의 멤버에 추가함...    학생 삭제된 경우 Unknown 추가.
    private func addAdultToStudent(obj: AnyObject) {
        let tstObj = obj as! HmTestBase
        let stdnt = HsBleMaestr.inst.arrStudent.filter { (std) -> Bool in
            std.dbid == tstObj.stdntID
        }.first
        
        if stdnt == nil {
            let newStudent = HmStudent(nm: "Unknown", em: "user@unknown", pw: "", phn: "", ag: 0)
            print("  Unknown :  user@unknown    \(tstObj.stdntID)")
            newStudent.dbid = tstObj.stdntID
            newStudent.arrTestOld.append(obj as! HmTestBase)
            HsBleMaestr.inst.arrStudent.append(newStudent)
        } else {
            stdnt?.arrTestOld.append(obj as! HmTestBase)
        }
        
        print(" test Object count : \(stdnt?.arrTestOld.count)  타잎 : \(obj.dynamicType)")
    }


}
