//
//  HsTestDB.swift
//  Test
//
//  Created by Jongwoo Moon on 2016. 2. 28..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation

class HsMainDB: HsBaseDB {

    override func checkAndMakeDB() {
        super.checkAndMakeDB()
        log.clsName = "HsMainDB<Test>"
        makeTestTables()
    }


    private func makeTestTables() {

        let curDB = FMDatabase(path: dbPath as String)
        if curDB.open() {
            // 공통 디비 생성  // TEST
            var sqlState = "CREATE TABLE IF NOT EXISTS TEST (ID INTEGER PRIMARY KEY AUTOINCREMENT, StudentID Int, " +
                "response Int, rescueaed Int, chksafety Int, airwaycfm Int, chkbreath Int, chkpulse Int, " +
            "theTime text, standard Int, pass Int) "
            print(sqlState)
            if !curDB.executeStatements(sqlState) {
                HSLogError("Create Table Test")
            }
            else{
                HSLogNotice("Create Table Test")
            }

            // 영아 디비 생성  // INFANT
            sqlState = "CREATE TABLE IF NOT EXISTS TEST_INFANT (ID INTEGER PRIMARY KEY AUTOINCREMENT, ParentID Int, " +
            "cyc1comp Int, cyc1breath Int, cyc2comp Int, cyc2breath Int, handsofftime Int, cycle3 Int, cycle4 Int)"
            print(sqlState)
            if !curDB.executeStatements(sqlState) {
                HSLogError("Create Table TestInfant")
            }
            else{
                HSLogNotice("Create Table TestInfant")
            }

            // 성인 디비 생성 // ADULT
            sqlState = "CREATE TABLE IF NOT EXISTS TEST_ADULT (ID INTEGER PRIMARY KEY AUTOINCREMENT, ParentID Int, " +
            "aedturnon Int, attachpad Int, analysis Int, retreat Int, deliveraed Int, cpragain Int ) "
            print(sqlState)
            if !curDB.executeStatements(sqlState) {
                HSLogError("Create Table TestAdult")
            }
            else{
                HSLogNotice("Create Table TestAdult")
            }
            curDB.close()
            log.printThisFNC("checkAndMakeDB<Test>", comment: "  OK >> DB :: Close")
        } else { log.printAlways("  Error   ", lnum: 20) }

    }


    func checkAndMakeDBPrevxx() { /// 이전 데이터..
        super.checkAndMakeDB()
        log.clsName = "HsMainDB<Test>"
        let curDB = FMDatabase(path: dbPath as String)
        if curDB.open() {
            // 영아 디비 생성
            var sqlState = "CREATE TABLE IF NOT EXISTS INFANT1 (ID INTEGER PRIMARY KEY AUTOINCREMENT, StudentID Int, " +
                "Test1 Int, Test2 Int, Test3 Int, Test6a Int, Test6b Int, Test7a Int, Test7b Int, " +
            "CompPosi Int, CompRate Int, CompDepth Int, CompRecoil Int, Interrupt Int, Pass1 Int) "
            if !curDB.executeStatements(sqlState) {
                log.printAlways(" create table INFANT1  error  ", lnum: 20)
            }
            sqlState = "CREATE TABLE IF NOT EXISTS INFANT2 (ID INTEGER PRIMARY KEY AUTOINCREMENT, StudentID Int, " +
                "Test5 Int, Pass2 Int) "
            if !curDB.executeStatements(sqlState) {
                log.printAlways(" create table INFANT2  error  ", lnum: 20)
            }

            // 성인 디비 생성
            sqlState = "CREATE TABLE IF NOT EXISTS ADULT1 (ID INTEGER PRIMARY KEY AUTOINCREMENT, StudentID Int, " +
                "Test1 Int, Test2 Int, Test3 Int, Test6 Int, Test9rs1 Int, Test9rs2 Int, Pass1 Int) "
            if !curDB.executeStatements(sqlState) {
                log.printAlways(" create table ADULT1  error  ", lnum: 20)
            }
            sqlState = "CREATE TABLE IF NOT EXISTS ADULT2 (ID INTEGER PRIMARY KEY AUTOINCREMENT, StudentID Int, " +
                "Test5 Int, Test7 Int, Test8 Int, Test9cc1 Int, Test9cc2 Int, Pass2 Int)"
            if !curDB.executeStatements(sqlState) {
                log.printAlways(" create table ADULT2  error  ", lnum: 20)
            }
//            sqlState = "CREATE TABLE IF NOT EXISTS ADULTEST4 (ID INTEGER PRIMARY KEY AUTOINCREMENT, AdultID Int, SN Int, " +
//            "CcCnt Int, CcTime Int, CcPassCnt Int, RpCnt Int, RpTime Int, RpPassCnt Int)"
//            if !curDB.executeStatements(sqlState) {
//                log.printAlways(" create table ADULTEST4  error  ", lnum: 20)
//            }
            curDB.close()
            log.printThisFNC("checkAndMakeDB<Test>", comment: "  OK >> DB :: Close")
        } else { log.printAlways("  Error   ", lnum: 20) }
    }

    //var arrInfant = [HmTestInfant](), arrAdult = [HmTestAdult]()

    /** 테스트에 추가된 디비를 추가로 읽어 들인다..   초기에..  
     
     - Student 의
     
     */

    override func versionCheckAndUpgrade_Test1() {  // 다음번엔 메타데이터 저장할 테이블을 넣자..
        log.printThisFunc("versionCheckAndUpgrade_Test1", lnum: 10)
        let curDB = FMDatabase(path: dbPath as String)
        let tableNames = [ "INFANT1", "INFANT2", "ADULT1", "ADULT2" ]

        if curDB.open() {
            for tName in tableNames {
                let timeStamp = curDB.columnExists("thetime", inTableWithName: tName)

                if !timeStamp {
                    log.logThis(" \(tName) 테이블에 testingtime 이 없다..   업그레이드 실시", lnum: 5)
                    let sqll = "ALTER TABLE \(tName) ADD COLUMN thetime text" // DEFAULT CURRENT_TIMESTAMP NOT NULL"
                    print(sqll)
                    curDB.executeUpdate(sqll, withArgumentsInArray: nil)
                }
            }
        }
    }

    ///  chkSafety airway checkBreath 추가.
    override func versionCheckAndUpgrade_Test2() {
        log.printThisFunc("versionCheckAndUpgrade_Test2", lnum: 10)
        let curDB = FMDatabase(path: dbPath as String)
        let tableNames = [ "INFANT1", "ADULT1" ]

        if curDB.open() {
            defer { curDB.close() }

            for tName in tableNames {
                let timeStamp = curDB.columnExists("ChkSafety", inTableWithName: tName)

                if !timeStamp {
                    log.logThis(" \(tName) 테이블에 testingtime 이 없다..   업그레이드 실시", lnum: 5)
                    var sqll = "ALTER TABLE \(tName) ADD COLUMN ChkSafety Int"
                    print(sqll)
                    curDB.executeUpdate(sqll, withArgumentsInArray: nil)
                    sqll = "ALTER TABLE \(tName) ADD COLUMN Airway Int"
                    print(sqll)
                    curDB.executeUpdate(sqll, withArgumentsInArray: nil)
                    sqll = "ALTER TABLE \(tName) ADD COLUMN ChkBreath Int" // DEFAULT CURRENT_TIMESTAMP NOT NULL"
                    print(sqll)
                    curDB.executeUpdate(sqll, withArgumentsInArray: nil)
                }
            }
        }
    }

    ///  테스트 기준 추가.  Standard  0: AHA, 1: ERC, 2: ANZCOR
    override func versionCheckAndUpgrade_Test3() {
        log.printThisFunc("versionCheckAndUpgrade_Test3", lnum: 10)
        let curDB = FMDatabase(path: dbPath as String)
        let tableNames = [ "INFANT1", "INFANT2", "ADULT1", "ADULT2" ]

        if curDB.open() {
            defer { curDB.close() }
            for tName in tableNames {
                let timeStamp = curDB.columnExists("Standard", inTableWithName: tName)
                if !timeStamp {
                    log.logThis(" \(tName) 테이블에 Standard 이 없다..   업그레이드 실시", lnum: 5)
                    let sqll = "ALTER TABLE \(tName) ADD COLUMN Standard Int"
                    print(sqll)
                    curDB.executeUpdate(sqll, withArgumentsInArray: nil)
                }
            }
        }
    }


    ///  테스트 기준 추가.  Standard  0: AHA, 1: ERC, 2: ANZCOR
    override func versionCheckAndUpgrade_Test4() {
        log.printThisFunc("versionCheckAndUpgrade_Test4", lnum: 10)
        let curDB = FMDatabase(path: dbPath as String)

        if curDB.open() {
            if !curDB.tableExists("TEST") {
                log.logThis(" 테스트 테이블 추가 .   업그레이드 실시", lnum: 5)
                checkAndMakeDB()
            }
        }
    }

    override func readAppDependentData() {
        log.printThisFunc("readAppDependentData", lnum: 3)
        let curDB = FMDatabase(path: dbPath as String)

        let tableName = [ "ADULT1", "ADULT2", "INFANT1", "INFANT2" ]

        if curDB.open() {
            defer { curDB.close() }
//            for (ix, name) in tableName.enumerate() {
//                let sql = "SELECT * from  \(name)"
//                log.logThis(sql, lnum: 1)
//                let rslt:FMResultSet? = curDB.executeQuery(sql, withArgumentsInArray:  nil)
//                var aObj: HmTest?
//                while rslt?.next() == true {
//                    switch ix {
//                    case 0: aObj = HmTestAdult(rcrd: rslt!, is1st: true)
//                    aObj!.isRes1 = true
//                    case 1: aObj = HmTestAdult(rcrd: rslt!, is1st: false)
//                    aObj!.isRes2 = true
//                    case 2: aObj = HmTestInfant(rcrd: rslt!, is1st: true)
//                    aObj?.isRes1 = true
//                    default: aObj = HmTestInfant(rcrd: rslt!, is1st: false)
//                    aObj!.isRes2 = true
//                    }
//                    addToStudent(aObj!)
//                }
//            }
        }
    }

    override func reloadStudentTestInfo(){
        HSLogNotice()
        testDebugging()
        for student in HsBleMaestr.inst.arrStudent {
            student.arrTestObjDB.removeAll()

            let info = grabAdultInfo(student)
            if info != nil {
                print("  성인이었슴.")
                student.arrTestObjDB.append(info!)
            }
            let infnt = grabInfantInfo(student)
            if infnt != nil {
                student.arrTestObjDB.append(infnt!)
            }
            print("  student arrTestObjDB.count : \(student.arrTestObjDB.count)")
        }
    }

    func testDebugging() {
        let curDB = FMDatabase(path: dbPath as String)
        if curDB.open(){
            defer { curDB.close() }

            var qStr = "SELECT * from TEST"
            var q = curDB.executeQuery(qStr, withArgumentsInArray: nil)
            while q.next() {
                print("    ID : \(q.intForColumn("ID")), StudID : \(q.intForColumn("StudentID"))  standard : \(q.intForColumn("standard")), pass : \(q.intForColumn("pass")) ")
            }
            print("  ~~~~~~~~~~~~~~~~~ ~~~~~~~~~~ ~~~~~~~~~~ ~~~~~~~~~~ ~~~~~~~~~~ ")
            qStr = "SELECT * from TEST_INFANT"
            q = curDB.executeQuery(qStr, withArgumentsInArray: nil)
            while q.next() {
                print("    ID : \(q.intForColumn("ID")), ParentID : \(q.intForColumn("ParentID"))  cyc1comp : \(q.intForColumn("cyc1comp")), handsofftime : \(q.intForColumn("handsofftime")) ")
            }
            print("  ~~~~~~~~~~~~~~~~~ ~~~~~~~~~~ ~~~~~~~~~~ ~~~~~~~~~~ ~~~~~~~~~~ ")
            qStr = "SELECT * from TEST_ADULT"
            q = curDB.executeQuery(qStr, withArgumentsInArray: nil)
            while q.next() { // "aedturnon Int, attachpad Int, analysis Int, retreat Int, deliveraed Int, cpragain Int ) "
                print("    ID : \(q.intForColumn("ID")), ParentID : \(q.intForColumn("ParentID"))  aedturnon : \(q.intForColumn("aedturnon")), analysis : \(q.intForColumn("analysis")) ")
            }
        }

    }

    /** Adult 객체를 해당 학생의 멤버에 추가함... */
    private func addToStudent(obj: AnyObject) {
        HSLogNotice()
        let tstObj = obj as! HmTest
        let stdnt = HsBleMaestr.inst.arrStudent.filter { (std) -> Bool in
            std.dbid == tstObj.stdntID
        }
        stdnt.first?.arrTestObjDB.append(obj as! HmTest)
//        print(" test Object count : \(stdnt.first?.arrTestObjDB.count)  타잎 : \(obj.dynamicType)")
    }

    /// 영아 테스트 결과 저장.
    override func addInfantInfo(infnt: HmTestInft, student: HmStudent) -> Bool {
        log.printThisFNC("add An Infant _1_", comment: " student : \(student.dbid) ", lnum: 10)
        timeStamp = NSDate()
        let curDB = FMDatabase(path: dbPath as String)
        var retVal = false
        if  student.dbid < 1 { log.printAlways(" Student ID 오류. >>\(student.dbid)<< "); return true }
        if curDB.open(){
            // 학생 아이디의 TEST 를 지워야 함.  아래 InfantTestTable 있으면 지워야 함.
            let queryTestStr = "SELECT * from TEST WHERE StudentID = \(student.dbid)"
            let queryTest = curDB.executeQuery(queryTestStr, withArgumentsInArray: nil)
            while queryTest.next() {
                let curID = queryTest.intForColumn("ID")
                let queryInfantStr = "SELECT * From TEST_INFANT WHERE ParentID = \(curID)"

                let qrInfnt = curDB.executeQuery(queryInfantStr, withArgumentsInArray: nil)

                if qrInfnt.next() {  // delete
                    var sql = "DELETE from TEST_INFANT where ParentID = \(curID)"
                    curDB.executeUpdate(sql, withArgumentsInArray: nil)
                    HSLogNotice(sql)
                    sql = "DELETE from TEST where ID = \(curID)"
                    curDB.executeUpdate(sql, withArgumentsInArray: nil)
                    HSLogNotice(sql)
                }
            }

            let insertStr = infnt.sqlTestString(student.dbid)
            HSLogNotice(insertStr)
            let insertTestResult = curDB.executeUpdate(insertStr, withArgumentsInArray: nil)
            if !insertTestResult {
                HSLogError("Test Insert Failed, Abort Infant Insert", classInfo:self)
                retVal = true
            } else {
                let testID = Int(curDB.lastInsertRowId())
                let insertStr = infnt.sqlInfantString(testID)
                HSLogNotice("Test Insert Sql: " + insertStr, classInfo: self)
                
                let result = curDB.executeUpdate(insertStr, withArgumentsInArray: nil)
                if result {
                    HSLogNotice("Infant Insert Success", classInfo: self)
                } else {
                    HSLogError("Infant Insert Failed", classInfo: self)
                    retVal = true
                }
            }
        } else {
            retVal = true
            HSLogError("DB Open Failed", classInfo: self)
        }
        curDB.close()
        return retVal
    }

    /// 성인 테스트 결과 저장.
    override func addAdultInfo(adlt: HmTestAdlt, student: HmStudent) -> Bool {
        log.printThisFNC("add An Adult _1_", comment: " student : \(student.dbid) ", lnum: 10)
        timeStamp = NSDate()
        let curDB = FMDatabase(path: dbPath as String)
        var retVal = false
        if  student.dbid < 1 { log.printAlways(" Student ID 오류. >>\(student.dbid)<< "); return true }
        if curDB.open(){
            // 학생 아이디의 TEST 를 지워야 함.  아래 InfantTestTable 있으면 지워야 함.
            let queryTestStr = "SELECT * from TEST WHERE StudentID = \(student.dbid)"
            let queryTest = curDB.executeQuery(queryTestStr, withArgumentsInArray: nil)
            while queryTest.next() {
                let curID = queryTest.intForColumn("ID")
                let queryInfantStr = "SELECT * From TEST_ADULT WHERE ParentID = \(curID)"
                
                let qrInant = curDB.executeQuery(queryInfantStr, withArgumentsInArray: nil)
                
                if qrInant.next() {  // delete
                    var sql = "DELETE from TEST_ADULT where ParentID = \(curID)"
                    curDB.executeUpdate(sql, withArgumentsInArray: nil)
                    HSLogNotice(sql)
                    sql = "DELETE from TEST where ID = \(curID)"
                    curDB.executeUpdate(sql, withArgumentsInArray: nil)
                    HSLogNotice(sql)
                }
            }
            let insertStr = adlt.sqlTestString(student.dbid)
            HSLogNotice(insertStr)
            let insertTestResult = curDB.executeUpdate(insertStr, withArgumentsInArray: nil)
            if !insertTestResult {
                HSLogError("Test Insert Failed, Abort Adult Insert", classInfo:self)
            } else {
                let testID = Int(curDB.lastInsertRowId())
                let insertStr = adlt.sqlAdultString(testID)
                HSLogNotice("Test Insert Sql: " + insertStr, classInfo: self)
                
                let result = curDB.executeUpdate(insertStr, withArgumentsInArray: nil)
                if result {
                    HSLogNotice("Adult Insert Success", classInfo: self)
                } else {
                    HSLogError("Adult Insert Failed", classInfo: self)
                }
            }
        }
        curDB.close()
        return retVal
    }

    //////////////////////////////////////////////////////////////////////     _//////////_     [     ]    _//////////_   정보 읽기
    // MARK:  Grab Info
    override func grabInfantInfo(student: HmStudent) -> HmTestInft? {
        log.printThisFNC("grabInfantInfo", comment: " 학생 dbid : \(student.dbid)")
        let curDB = FMDatabase(path: dbPath as String) // 해당 세트 어레이에 추가하고 마무리..  정렬해야..
        var rObj: HmTestInft?
        if curDB.open() {
            defer { curDB.close() }
            let sql = "SELECT * from TEST WHERE StudentID = \(student.dbid)"
            HSLogNotice(sql)
            let rslt:FMResultSet? = curDB.executeQuery(sql, withArgumentsInArray:  nil)
            while rslt?.next() == true {
                let testID = rslt!.intForColumn("ID")
                let sql = "SELECT * from TEST_INFANT WHERE ParentID = \(testID)"
                let infantResult = curDB.executeQuery(sql, withArgumentsInArray: nil)
                if true == infantResult?.next() {
                    rObj = HmTestInft(record: rslt!, infantRecord: infantResult)
                    HSLogNotice("Student has Infant data")
                    break;
                }
            }
        }
        return rObj
    }

    override func grabAdultInfo(student: HmStudent) -> HmTestAdlt? {
        log.printThisFNC("grabAdultInfo", comment: " 학생 dbid : \(student.dbid)")
        let curDB = FMDatabase(path: dbPath as String) // 해당 세트 어레이에 추가하고 마무리..  정렬해야..
        var rObj: HmTestAdlt?
        if curDB.open() {
            defer { curDB.close() }
            let sql = "SELECT * from TEST WHERE StudentID = \(student.dbid)"
            HSLogNotice(sql)
            let rslt:FMResultSet? = curDB.executeQuery(sql, withArgumentsInArray:  nil)
            while rslt?.next() == true {
                let testID = rslt!.intForColumn("ID")
                let sql = "SELECT * from TEST_ADULT WHERE ParentID = \(testID)"
                let adultResult = curDB.executeQuery(sql, withArgumentsInArray: nil)
                if true == adultResult?.next() {
                    rObj = HmTestAdlt(record: rslt!, adultRecord: adultResult)
                    HSLogNotice("Student has Adult data")
                    break;
                }
            }
        } else {
            HSLogError("DB Open Error", classInfo: self)
        }
        return rObj
    }

    //////////////////////////////////////////////////////////////////////     _//////////_     [     ]    _//////////_   삭제
    // MARK:  database ..   Delete  >>>  삭제 ..
    override func deleteStudent(sObj: HmStudent) -> Bool {
        //super.deleteStudent(sObj)
        print("HsTestDB :: deleteStudent >>  ")
        let stdntID = sObj.dbid
        let rVal = super.deleteStudent(sObj)

        let curDB = FMDatabase(path: dbPath as String)
        if curDB.open() {
            var sql = "SELECT * From TEST Where StudentID = \(stdntID)"
            log.logThis(sql, lnum: 1)
            let resultSelection = curDB.executeQuery(sql, withArgumentsInArray: nil)
            while (resultSelection.next()){
                let parID = resultSelection.intForColumn("ID")


                print(" asdfasdf  >> parID : \(parID)")

                var sql = "DELETE From TEST_INFANT Where ParentID = \(parID)"
                var rslt = curDB.executeUpdate(sql, withArgumentsInArray: nil)
                sql = "DELETE From TEST_ADULT Where ParentID = \(parID)"
                rslt = curDB.executeUpdate(sql, withArgumentsInArray: nil)
            }
            sql = "DELETE From TEST Where StudentID = \(stdntID)"
            let resultDeletion = curDB.executeUpdate(sql, withArgumentsInArray: nil)
            if !resultDeletion {
                HSLogError("TestDeletionError")
                //rVal = true
            } else {
                HSLogNotice(" TEST Delete :: OK, DBID: \(sObj.dbid)")
            }
        }
        curDB.close()

        return rVal
    }
//
//    func deleteAllTestObjects(sObj: HmStudent) -> Bool {
//        log.printThisFNC("deleteTestObjects", comment: "  StudentID : \(sObj.dbid)")
//
//        print("  sObj.finishedInfant  \(sObj.finishedInfant)   sObj.finishedAdult \(sObj.finishedAdult) ")
//
//        // 끝났는지 여부에 상관없이 몽땅 지워야..
//        //if sObj.finishedInfant {
//            deleteFromTable("INFANT1", condStr: "StudentID = \(sObj.dbid)")
//            deleteFromTable("INFANT2", condStr: "StudentID = \(sObj.dbid)")
//
//        //if sObj.finishedAdult {
//            deleteFromTable("ADULT1", condStr: "StudentID = \(sObj.dbid)")
//            deleteFromTable("ADULT2", condStr: "StudentID = \(sObj.dbid)")
//        return false // OK..
//    }
}
