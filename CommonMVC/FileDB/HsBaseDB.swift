//
//  HsBaseDB.swift
//  Test
//
//  Created by Jongwoo Moon on 2016. 3. 3..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation

/**     DB Management 설명

*/

class HsBaseDB: HtObject {
    var dbPath = NSString()
    var arrSet = [HmUnitSet]()
    var arrData = [HsData]()
    var timeStamp = NSDate()

    func saveDataAfterEntireProcess(stdObj: HmStudent) {
        HSLogNotice("" , classInfo: self)
        deleteSetsDataOf(stdObj.dbid) // 기존 데이터가 있다면 삭제..
        stdObj.myData!.timeStamp = NSDate()
        addAData(stdObj.myData!, parent: stdObj)
        for st in stdObj.myData!.arrSet {
            if addASet(st, parent: stdObj) {
                HSLogError("세트 추가 에러")
            }
        }
    }

    func readStudentDataSetInfo() {
        grabAllDatum()
        grabAllSet()
    }



    //////////////////////////////////////////////////////////////////////     _//////////_     [     ]    _//////////_
    // MARK:  DB 처음 생성..
    /**
    앱 실행 초기에 디비 생성 확인

    @param 없슴.

    @return No.
    */
    func checkAndMakeDB() {
        log.printAlways("HsBaseDB :: checkAndMakeDB", lnum: 2)
        let fileMan = NSFileManager.defaultManager()
        let path = NSSearchPathForDirectoriesInDomains(.DocumentationDirectory, .UserDomainMask, true)
        let docDir = path[0]

        dbPath = docDir.stringByAppendingString("main.db")

        if !fileMan.fileExistsAtPath(dbPath as String) {
            let newDB = FMDatabase(path: dbPath as String)

            if newDB == nil {  log.printAlways("  Database Creation Failed !!!!", lnum: 20)  }

            if newDB.open() {
                log.printAlways(" 데이터베이스 파일 생성  ", lnum: 10)
                var sqlState = "CREATE TABLE IF NOT EXISTS STUDENT (ID INTEGER PRIMARY KEY AUTOINCREMENT, SvrID Int, SvrSaved Int, " +
                "Name TEXT, Password Text, Email TEXT, Phone TEXT, Age Int)"
                if !newDB.executeStatements(sqlState) {
                    log.printAlways(" create table STUDENT  error  ", lnum: 20)
                }
                sqlState = "CREATE TABLE IF NOT EXISTS DATA (ID INTEGER PRIMARY KEY AUTOINCREMENT, Owner Int)"
                if !newDB.executeStatements(sqlState) {
                    log.printAlways(" create table DATA  error  ", lnum: 20)
                }
                sqlState = "CREATE TABLE IF NOT EXISTS UNITSET (ID INTEGER PRIMARY KEY AUTOINCREMENT, Owner Int, SetNum Int, " +
                    "CcCount Int, RpCount Int, CcPassCount Int, RpPassCount Int, HandsOffTime Int, CcTime Double, RpTime Double," +
                "CcPosi1 Int, CcPosi2 Int, CcPosi3 Int, CcPosi4 Int, CcPosi5 Int, thetime Text)"
                if !newDB.executeStatements(sqlState) {
                    log.printAlways(" create table SET  error  ", lnum: 20)
                }
                sqlState = "CREATE TABLE IF NOT EXISTS COMPSTROKE (ID INTEGER PRIMARY KEY AUTOINCREMENT, SetID Int, SN Int, MaxD Int, RecoilD Int, Period Double, wPosiIdx Int)"
                if !newDB.executeStatements(sqlState) {
                    log.printAlways(" create table COMPSTROKE  error  ", lnum: 20)
                }
                sqlState = "CREATE TABLE IF NOT EXISTS BRTHSTROKE (ID INTEGER PRIMARY KEY AUTOINCREMENT, SetID Int, SN Int, Max Int, Period Double)"
                if !newDB.executeStatements(sqlState) {
                    log.printAlways(" create table BRTHSTROKE  error  ", lnum: 20)
                }

                newDB.close()
            } else { log.printAlways("  Error   ", lnum: 20) }
        } else {
            log.printAlways("  기존 DB 있슴   OK ", lnum: 10)
            versionCheckAndUpgrade_Ver2()
        }
    }

    func versionCheckAndUpgrade_Ver2() {
        log.printThisFunc("versionCheckAndUpgrade_Ver2", lnum: 33)
        let curDB = FMDatabase(path: dbPath as String)
        var wPosiExist = false
        if curDB.open() {
            wPosiExist = curDB.columnExists("wPosiIdx", inTableWithName: "COMPSTROKE")
            // .columnExists("COMPSTROKE", columnName: "wPosiIdx")
            log.logThis(" Final wPosiExist  ::  \(wPosiExist)")

            if wPosiExist == false {
                log.logUiAction(" 기존 데이터는 Ver 1.0 임..  업데이트.. ")
                let sqll = "ALTER TABLE COMPSTROKE ADD COLUMN wPosiIdx Int"
                print(sqll)
                curDB.executeUpdate(sqll, withArgumentsInArray: nil)
            }
            curDB.close()
        }
    }

    func versionCheckAndUpgrade_Ver3() {  // 다음번엔 메타데이터 저장할 테이블을 넣자..
        log.printThisFunc("versionCheckAndUpgrade_Ver3", lnum: 10)
        let curDB = FMDatabase(path: dbPath as String)

        if curDB.open() {
            let timeStamp = curDB.columnExists("thetime", inTableWithName: "UNITSET")

            if !timeStamp {
                log.logThis(" UNITSET 테이블에 testingtime 이 없다..   업그레이드 실시", lnum: 5)
                let sqll = "ALTER TABLE UNITSET ADD COLUMN thetime text" // DEFAULT CURRENT_TIMESTAMP NOT NULL"
                print(sqll)
                curDB.executeUpdate(sqll, withArgumentsInArray: nil)
            }
        }
    }

    func versionCheckAndUpgrade_Ver4() {
        log.printThisFunc("versionCheckAndUpgrade_Ver4", lnum: 10)
        let curDB = FMDatabase(path: dbPath as String)
        let tableNames = [ "DATA" ]

        if curDB.open() {
            for tName in tableNames {
                let timeStamp = curDB.columnExists("Standard", inTableWithName: tName)
                if !timeStamp {
                    log.logThis(" \(tName) 테이블에 Standard 이 없다..   업그레이드 실시", lnum: 5)
                    var sqll = "ALTER TABLE \(tName) ADD COLUMN Standard Int"
                    print(sqll)
                    curDB.executeUpdate(sqll, withArgumentsInArray: nil)
                    sqll = "ALTER TABLE \(tName) ADD COLUMN theTime text" // DEFAULT CURRENT_TIMESTAMP NOT NULL"
                    print(sqll)
                    curDB.executeUpdate(sqll, withArgumentsInArray: nil)
                }
            }
        }

    }

    func versionCheckAndUpgrade_Test1() {}  // 다음번엔 메타데이터 저장할 테이블을 넣자..

    func versionCheckAndUpgrade_Test2() {}

    func versionCheckAndUpgrade_Test3() {}

    func versionCheckAndUpgrade_Test4() {}

    func versionCheckAndUpgrade_Monitor1() {}

    func reloadStudentTestInfo() { }

    func dateTest() {
        let db = FMDatabase(path: dbPath as String)
        db.open()

        let curDate = NSDate(timeIntervalSinceNow: 0)
        print("  curDate.formatYYMMDDspaceTime   \(curDate.formatYYMMDDspaceTime) ")

        if !db.executeUpdate("create table if not exists foo (bar text, transactionTimestamp text)", withArgumentsInArray: nil) {
            print(db.lastErrorMessage())
        }
        if !db.executeUpdate("insert into foo (bar, transactionTimestamp) values ('sometest', ?)", // ? 만 어레이에 넣는다.
                             withArgumentsInArray: [ NSDate()]) {  /// 요 줄에서 Date 생성함...
            print(db.lastErrorMessage())
        }
        if let rs = db.executeQuery("select bar, transactionTimestamp from foo", withArgumentsInArray: nil) {
            while rs.next() {
                let bar       = rs.stringForColumn("bar")
                let timestamp = rs.dateForColumn("transactionTimestamp")

                if let timess = rs.dateForColumn("bar") {
                    print("  it's not nill   \(timess)  \(object_getClass(timess)) ")
                } else {
                    print("  nil ")
                }
                print("bar = \(bar); timestamp = \(timestamp) \(timestamp.formatYYMMDDspaceTime)  )  ")
                print("    \(object_getClass(timestamp))")
                print("   \(timestamp.formatFromOption(0))  \(timestamp.formatFromOption(1))    \(timestamp.formatFromOption(2)) ")
            }
            rs.close()
        } else {
            print(db.lastErrorMessage())
        }
        print("\n\n\n")
    }



    //////////////////////////////////////////////////////////////////////     _//////////_     [     ]    _//////////_   Add
    // MARK:  Write to database ..   Add ...

    func addAStudent(sObj: HmStudent) -> Bool {
        log.printThisFunc("add A Student", lnum: 5)
        let curDB = FMDatabase(path: dbPath as String)
        var rVal = false
        if curDB.open() {
            let insertSql = "INSERT INTO STUDENT (SvrID, SvrSaved, Name, Email, Password, Phone, Age) VALUES " +
            " ( \(sObj.svrID), \(sObj.svrSaved), '\(sObj.name)', '\(sObj.email)', '\(sObj.password)', '\(sObj.phone)', \(sObj.age))"
            log.logThis(insertSql, lnum: 1)
            let rslt = curDB.executeUpdate(insertSql, withArgumentsInArray: nil)
            if !rslt { log.printAlways("  Error ", lnum: 10); rVal = true } // error
            else {
                sObj.dbid = Int(curDB.lastInsertRowId())
                log.printAlways(" Inserted :: OK   \(sObj.name),  ID : \(sObj.dbid) ", lnum: 5) }
        }
        curDB.close()
        return rVal
    }


    func addAData(obj: HsData, parent: HmStudent) -> Bool {
        log.printThisFunc("add A Data : standard \(obj.standard)", lnum: 2)
        let curDB = FMDatabase(path: dbPath as String)
        var rVal = false
        if  parent.dbid < 1 { log.printAlways(" Data ID 오류. >> Student : \(parent.dbid)<<", lnum: 10); return true }
        if curDB.open() {
            let insertSql = "INSERT INTO DATA (Owner, Standard, theTime) VALUES (\(parent.dbid), \(obj.standard), '\(obj.timeStamp.formatYYMMDDspaceTime)'  )"
            log.logThis(insertSql, lnum: 1)
            let rslt = curDB.executeUpdate(insertSql, withArgumentsInArray: nil)
            if !rslt { log.printAlways("  Add a Data Error ", lnum: 10); rVal = true } // error
            else {
                obj.dbRecordID = Int(curDB.lastInsertRowId())
                log.printAlways(" Data \(obj.dbRecordID) Inserted :: OK    Student ID : \(parent.dbid) ", lnum: 1)
            }
        }
        curDB.close()
        return rVal
    }

    func addASet(aSet: HmUnitSet, parent: HmStudent) -> Bool {
        log.printThisFunc("add A Set", lnum: 2)
        let curDB = FMDatabase(path: dbPath as String)
        var rVal = false
        if  parent.dbid < 1 { log.printAlways(" Data ID 오류. >>\(parent.dbid)<<", lnum: 10); return true }
        if curDB.open() {
            let insertSql = "INSERT INTO UNITSET (Owner, SetNum, CcCount, RpCount, CcPassCount, RpPassCount, " +
                "HandsOffTime, CcTime, RpTime, CcPosi1, CcPosi2, CcPosi3, CcPosi4, CcPosi5, thetime) VALUES " +
                " (\(parent.dbid), \(aSet.setNum), \(aSet.ccCount), \(aSet.rpCount), \(aSet.ccPassCnt), " +
                "\(aSet.rpPassCnt), \(Int(aSet.holdTime)), \(aSet.ccTime), \(aSet.rpTime), \(aSet.ccPositionStr), ? )"
            log.logThis(insertSql, lnum: 1)
            let rslt = curDB.executeUpdate(insertSql, withArgumentsInArray: [ aSet.timeStamp ])

            if !rslt { log.printAlways("  Add a Set Error ", lnum: 10); rVal = true } // error
            else {
                aSet.dbRecordID = Int(curDB.lastInsertRowId())
                log.printAlways(" Set \(aSet.setNum) Inserted :: OK    Data ID : \(parent.dbid) ", lnum: 5)
            }
        }
        curDB.close()

        // comp data
        for (idx, cp) in aSet.arrComprs.enumerate() { addCompData(idx+1, comp: cp, par: aSet) }

        // breath data
        for (idx, cp) in aSet.arrBreath.enumerate() { addBrthData(idx+1, brth: cp, par: aSet) }

        return rVal
    }

    func addCompData(sn: Int, comp: HmCompStroke, par: HmUnitSet) -> Bool {
        log.printThisFunc("add A Comp Stroke", lnum: 2)
        let curDB = FMDatabase(path: dbPath as String)
        var rVal = false
        if  par.dbRecordID < 1 { log.printAlways(" Set ID 오류. >>\(par.dbRecordID)<<"); return true }
        if curDB.open() {
            let insertSql = "INSERT INTO COMPSTROKE (SetID, SN, MaxD, RecoilD, Period, wPosiIdx) VALUES (\(par.dbRecordID), \(sn), \(comp.maxDepth), \(comp.recoilDepth), \(comp.strkTime), \(comp.wPosiIdx))"
            log.logThis(insertSql, lnum: 1)
            let rslt = curDB.executeUpdate(insertSql, withArgumentsInArray: nil)
            if !rslt { log.printAlways("  Add a Comp Error ", lnum: 10); rVal = true } // error
            else { log.printAlways(" Comp at \(par.setNum) set  Inserted :: OK    MaxDepth : \(comp.maxDepth) ", lnum: 0) }
        }
        curDB.close()
        return rVal
    }

    func addBrthData(sn: Int, brth: HmBrthStroke, par: HmUnitSet) -> Bool {
        log.printThisFunc("add A Brth Stroke", lnum: 2)
        let curDB = FMDatabase(path: dbPath as String)
        var rVal = false
        if  par.dbRecordID < 1 { log.printAlways(" Set ID 오류. >>\(par.dbRecordID)<<"); return true }
        if curDB.open() {
            let insertSql = "INSERT INTO BRTHSTROKE (SetID, SN, Max, Period) VALUES (\(par.dbRecordID), \(sn), \(brth.max), \(brth.strkTime))"
            log.logThis(insertSql, lnum: 1)
            let rslt = curDB.executeUpdate(insertSql, withArgumentsInArray: nil)
            if !rslt { log.printAlways("  Add a Brth Error ", lnum: 10); rVal = true }
            else { log.printAlways(" Brth at \(par.setNum) set  Inserted :: OK    Max : \(brth.max) ", lnum: 0) }
        }
        curDB.close()
        return rVal
    }

    //////////////////////////////////////////////////////////////////////     _//////////_     [     ]    _//////////_   Set 정보 읽기
    // MARK:  Grab Set

    func grabAllDatum() { // 세트는 읽어서 멤버 변수 어레이로 갖고 있자..
        log.printThisFunc("grabAllDatum", lnum: 2)
        let curDB = FMDatabase(path: dbPath as String)
        arrData = [HsData]()

        if curDB.open() {
            let sql = "SELECT * from DATA"
            log.logThis(sql, lnum: 1)
            let rslt:FMResultSet? = curDB.executeQuery(sql, withArgumentsInArray:  nil)
            var obj: HsData?
            while rslt?.next() == true {
                obj = HsData(recd: rslt!)

                // UNITSET (ID INTEGER PRIMARY KEY AUTOINCREMENT, Owner Int, SetNum Int, CcPassCount Int, RpPassCount Int,
                // HandsOffTime Int, CcPosi1 Int, CcPosi2 Int, CcPosi3 Int, CcPosi4 Int, CcPosi5 Int)
                log.logThis("  Read Data Info ..   ID : \(obj?.dbRecordID), standard : \(obj?.standard)  Student ID : \(obj?.parentID) ", lnum: 0)
                arrData.append(obj!)
            }
        }
        curDB.close()

        //log.logThis("  rArr : \(arrData.count)")

        setDataToStudent()  // data 에 set 추가..
    }


    func grabAllSet() { // 세트는 읽어서 멤버 변수 어레이로 갖고 있자..
        log.printThisFunc("grabAllSet", lnum: 2)
        let curDB = FMDatabase(path: dbPath as String)
        arrSet = [HmUnitSet]() // 새로 생성해서 리프레시..

        if curDB.open() {
            let sql = "SELECT * from  UNITSET"
            log.logThis(sql, lnum: 1)
            let rslt:FMResultSet? = curDB.executeQuery(sql, withArgumentsInArray:  nil)
            var aSet: HmUnitSet?
            while rslt?.next() == true {
                aSet = HmUnitSet(theCode: "-")
                aSet?.setValuesFromDB(rslt!)
                // UNITSET (ID INTEGER PRIMARY KEY AUTOINCREMENT, Owner Int, SetNum Int, CcPassCount Int, RpPassCount Int,
                // HandsOffTime Int, CcPosi1 Int, CcPosi2 Int, CcPosi3 Int, CcPosi4 Int, CcPosi5 Int)
                log.logThis("  Read Set Info ..   ID : \(aSet?.dbRecordID),   SetNum : \(aSet?.setNum) ", lnum: 0)
                arrSet.append(aSet!)
            }
        }
        curDB.close()

        //log.logThis("  rArr : \(arrSet.count)")

        setSetToStudent()  // data 에 set 추가..
        sortSetsInDataObject() // setNum 에 따라 재정렬.
        grabAllStrokes()
    }

    func grabAllStrokes() {
        //  COMPSTROKE (ID INTEGER PRIMARY KEY AUTOINCREMENT, SetID Int, SN Int, MaxD Int, RecoilD Int, Period Double)"
        //  BRTHSTROKE (ID INTEGER PRIMARY KEY AUTOINCREMENT, SetID Int, SN Int, Max Int, Period Double)"
        log.printThisFunc("grabAllStrokes", lnum: 1)
        let curDB = FMDatabase(path: dbPath as String) // 해당 세트 어레이에 추가하고 마무리..  정렬해야..
        if curDB.open() {
            let sql = "SELECT * from  COMPSTROKE"
            let rslt:FMResultSet? = curDB.executeQuery(sql, withArgumentsInArray:  nil)
            var aStrk: HmCompStroke?
            while rslt?.next() == true {
                aStrk = HmCompStroke(rcrd: rslt!)
                let theSet = arrSet.filter({ (curSet) -> Bool in return curSet.dbRecordID == aStrk?.setID })
                if theSet.count == 0 { log.printAlways(" 현재 압박 스트로크 해당 세트 없슴. Error . ") }
                else {   theSet.first?.arrComprs.append(aStrk!)      }
            }
        }
        curDB.close()
        if curDB.open() {
            let sql = "SELECT * from  BRTHSTROKE"
            let rslt:FMResultSet? = curDB.executeQuery(sql, withArgumentsInArray:  nil)
            var aStrk: HmBrthStroke?
            while rslt?.next() == true {
                aStrk = HmBrthStroke(rcrd: rslt!)
                let theSet = arrSet.filter({ (curSet) -> Bool in return curSet.dbRecordID == aStrk?.setID })
                if theSet.count == 0 { log.printAlways(" 현재 >> 호흡 <<  스트로크 해당 세트 없슴. Error . ") }
                else {   theSet.first?.arrBreath.append(aStrk!)      }
            }
        }
        curDB.close()

        // 정렬
        for aSet in arrSet {
            let bArr = aSet.arrBreath.sort({ (aStrk, bStrk) -> Bool in  aStrk.sn < bStrk.sn   })
            aSet.arrBreath = bArr
            let cArr = aSet.arrComprs.sort({ (aStrk, bStrk) -> Bool in  aStrk.sn < bStrk.sn   })
            aSet.arrComprs = cArr
        }
    }

    func sortSetsInDataObject() {
        for stdnt in HsBleMaestr.inst.arrStudent {
            let newArr = stdnt.myDataFromDB?.arrSet.sort({ (before, after) -> Bool in
                before.setNum < after.setNum
            })
            stdnt.myDataFromDB?.arrSet = newArr!
        }
    }

    func setDataToStudent() {
        log.printThisFunc("setDataToStudent", lnum: 1)
        let arrStu = HsBleMaestr.inst.arrStudent
        for ast in arrData {
            let stuArr = arrStu.filter({ (stdnt) -> Bool in  return stdnt.dbid == ast.parentID } )
            if stuArr.count == 0 {
                log.logThis(" 학생을 찾을 수 없슴..  Error . ", lnum: 10)
            } else {
                log.logThis("학생에 HsData 할당  standard : \(ast.standard) to Student : \(ast.parentID) ", lnum: 1)
                stuArr.first?.myDataFromDB = ast
            }
        }
    }

    func setSetToStudent() {
        log.printThisFunc("setSetToData", lnum: 1)
        let arrStu = HsBleMaestr.inst.arrStudent
        for ast in arrSet {
            let stuArr = arrStu.filter({ (stdnt) -> Bool in  return stdnt.dbid == ast.parentID } )
            if stuArr.count == 0 { log.logThis(" 현재 세트를 갖는 DataObject 가 없슴.  Error . ", lnum: 1) }
            else {
                if stuArr.first?.myDataFromDB == nil {  log.logThis("  myDataFromDB is nil ", lnum: 2) }
                else {  stuArr.first?.myDataFromDB?.arrSet.append(ast) }
            }
        }
    }

    func grabAllStudents() -> [HmStudent]{
        log.printThisFunc("grabAllStudents", lnum: 2)
        let curDB = FMDatabase(path: dbPath as String)
        var rArr = [HmStudent]()

        if curDB.open() {
            let sql = "SELECT * from  STUDENT"
            log.logThis(sql, lnum: 5)
            let rslt:FMResultSet? = curDB.executeQuery(sql, withArgumentsInArray:  nil)
            var hmStudent: HmStudent?
            while rslt?.next() == true {
                hmStudent = HmStudent(recd: rslt!)
                rArr.append(hmStudent!)
            }
        }
        curDB.close()
        return rArr
    }

    func findAStudent(pName: String) -> HmStudent? {
        log.printThisFunc("findAStudent", lnum: 20)
        let curDB = FMDatabase(path: dbPath as String)
        var hmStudent: HmStudent?
        if curDB.open() {
            let sql = "SELECT * from  STUDENT WHERE Name = '\(pName)'"
            log.logThis(sql, lnum: 1)
            let rslt:FMResultSet? = curDB.executeQuery(sql, withArgumentsInArray:  nil)
            if rslt?.next() == true {
                hmStudent = HmStudent(nm: (rslt?.stringForColumn("name"))!, em: (rslt?.stringForColumn("email"))!, pw: (rslt?.stringForColumn("password"))!,
                    phn: (rslt?.stringForColumn("phone"))!, ag: Int((rslt?.stringForColumn("age"))!)!)
            } else {
                log.printAlways(" find :: error ", lnum: 5);
                hmStudent = nil
            }
        }
        curDB.close()
        return hmStudent
    }




    func readAppDependentData() {

    }

    func addInfantInfo(infnt: HmTestInft, student: HmStudent) -> Bool {
        return true
    }
    func addAdultInfo(adlt: HmTestAdlt, student: HmStudent) -> Bool {
        return true
    }

    func grabInfantInfo(student: HmStudent) -> HmTestInft? { return nil }
    func grabAdultInfo(student: HmStudent) -> HmTestAdlt? { return nil }
}
