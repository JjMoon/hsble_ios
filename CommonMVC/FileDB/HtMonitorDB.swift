//
//  HtMonitorDB.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2015. 11. 19..
//  Copyright © 2015년 IMLab. All rights reserved.
//

import Foundation

class HsMainDB: HsBaseDB {
 
    /**
     앱 실행 초기에 디비 생성 확인
     */

    override func checkAndMakeDB() {
        super.checkAndMakeDB()
        log.clsName = "HsMainDB"
        log.printThisFunc("  checkAndMakeDB  ", lnum: 5)
    }

    ///  테스트 기준 추가.  Standard  0: AHA, 1: ERC, 2: ANZCOR
    override func versionCheckAndUpgrade_Monitor1() {
    }
}



//  아래는 테이블 정보를 가져와서 프린트 하는 소스...  db.columnExist 로 간단히 해결될 걸 아래처럼 고생했슴...
//            let sql = "PRAGMA table_info(COMPSTROKE)"
//            log.logThis(sql, lnum: 1)
//            let rslt:FMResultSet? = curDB.executeQuery(sql, withArgumentsInArray:  nil)
//
//            while rslt?.next() == true {
//                //print("PRAGMA table_info(COMPSTROKE  >>   \(rslt!.columnNameForIndex(idx))")
//
//                print("\(rslt?.resultDictionary())")
//                print("\(rslt?.resultDictionary()["name"])   \(String((rslt!.resultDictionary()["name"])!)) ")
//
//                if String((rslt!.resultDictionary()["name"])!) == "wPosiIdx" {
//                    print("\n\n  OK  \n\n")
//                    wPosiExist = true
//                } else {
//                    print("  wPosiIdx : nil")
//                }
//
//                idx += 1
//            }

