//
//  HmStudent.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2015. 11. 18..
//  Copyright © 2015년 IMLab. All rights reserved.
//

import Foundation

//////////////////////////////////////////////////////////////////////     _//////////_     [     ]    _//////////_   Go to Main View
// MARK:  Student
/** 

 - arrTestObjDB : DB 에서 읽은 객체는 어레이로 채워 넣자. 순서 없슴.  최대 4개.

*/
class HmStudent: NSObject {
    var dbid:Int = -1, svrID: UInt = 0, svrSaved = 0,
    name = "", email = "@", password = "pwpwpw", phone = "82-10-123-4567", age = 0
    //var xxarrComp = [CompObj]()
    var arrResp = [RespObj]()
    var arrCInf = [CompInf]()
    var arrTestObjDB = [HmTest](), arrTestOld = [HmTestBase]()
    var myData: HsData? // 일단 하나만 갖도록 하자.
    var myDataFromDB: HsData? // DB 에서 읽었으나 서버로 보내지 않은 데이터.
    var bleMan: HsBleSuMan? // UI 에서 잠깐 잠깐 쓰임.
    var testObj: HmTest?

    
    //////////////////////////////////////////////////////////////////////     [   기존 테스트 객체 관련.  >> ]

    var finishedAdultOld: Bool {
        get {
            let theData = arrTestOld.filter { (t) -> Bool in
                t.amIAdult
            }.count == 2
            let setData = myDataFromDB != nil && 0 < myDataFromDB!.arrSet.count
            return theData && setData
        }
    }
    var finishedInfantOld: Bool {
        get {
            return arrTestOld.filter { (t) -> Bool in
                !t.amIAdult
                }.count == 2
        }
    }
    
    func getOldAdultFromDB() -> HmTestAdult? {
        let arrInf = arrTestOld.filter { (obj) -> Bool in
            return obj <^> HmTestAdult()
        }
        if arrInf.count == 0 { return nil }
        let rInfant = HmTestAdult()
        for obj in arrInf {
            let inf = obj as! HmTestAdult
            if 0 < inf.dbID1 { rInfant.setResc1From(inf) }
            if 0 < inf.dbID2 { rInfant.setResc2From(inf) }
        }
        //print("  isRescure1, 2 ? \(rInfant.isRes1) | \(rInfant.isRes2) ")
        return rInfant
    }
    func getOldInfantFromDB() -> HmTestInfant? {
        let arrInf = arrTestOld.filter { (obj) -> Bool in
            return obj <^> HmTestInfant()
        }
        if arrInf.count == 0 { return nil }
        let rInfant = HmTestInfant()
        for obj in arrInf {
            let inf = obj as! HmTestInfant
            if 0 < inf.dbID1 { rInfant.setResc1From(inf) }
            if 0 < inf.dbID2 { rInfant.setResc2From(inf) }
        }
        
        return rInfant
    }
    
    //////////////////////////////////////////////////////////////////////     [   기존 테스트 객체 관련.  >> ]

    // UI 상태 관련 변수..  uiSendServerTargeted = false
    var uiTargeted = false, isMain = false, isSub = false, isMainTester: Bool?, evalFinished = false  // delete
    // 실습 데이터가 DB에 있는지
    var hasOperateData: Bool { get { return myDataFromDB != nil && 0 < myDataFromDB!.arrSet.count }}

    // MARK: Database ....
    var isDataInDB: Bool {
        //if AppIdTrMoTe == 2 { return finishedAdult || finishedInfant }
        if AppIdTrMoTe == 2 {
            //print("   isSomeAdult || isSomeInfant   \(finishedAdult)  |  \(finishedInfant) ")
            return finishedAdult || finishedInfant } // 뭐 하나라도 끝냈으면 보여준다.
        if myDataFromDB == nil { return false }
        if myDataFromDB?.arrSet.count == 0 { return false }
        else {
            return true
        }
    }

    var finishedAdult:  Bool { get { return dataExistOfResc(true, adultCase: true)  }}
    var finishedInfant: Bool { get { return dataExistOfResc(true, adultCase: false) }}
    var haveGraphData:  Bool { get { return dataExistOfResc(true, adultCase: true)  }}

    var passAdult: Bool { get {
        if finishedAdult {
            let aObj = getAdultFromDB()
            return aObj!.pass // aObj!.passRes1 && aObj!.passRes2
        }
        return false
        }}

    var passInfant: Bool { get {
        if finishedInfant {
            let aObj = getInfantFromDB()
            return aObj!.pass // aObj!.passRes1 && aObj!.passRes2
        }
        return false
        }}


    override init() {
        super.init()
    }

    convenience init(nm: String, em: String, pw: String, phn: String, ag: Int) {
        self.init()
        name = nm; email = em; password = pw; phone = phn; age = ag
    }

    convenience init(recd: FMResultSet) {
        self.init()
        // STUDENT (ID, SvrID Int, SvrSaved Int, Name TEXT, Password Text, Email TEXT, Phone TEXT, Age Int)"
        dbid = Int(recd.stringForColumn("ID"))!
        //print(" HmStudent :: init(recd: FMResultSet ...   \(dbid) ")
        svrID = UInt(recd.stringForColumn("SvrID"))!
        svrSaved = Int(recd.stringForColumn("SvrSaved"))! // 1: saved, 0: not yet
        name = recd.stringForColumn("Name"); email = recd.stringForColumn("Email"); password = recd.stringForColumn("Password")
        phone = recd.stringForColumn("Phone"); age = Int(recd.stringForColumn("Age"))!
        myDataFromDB = HsData()

        let totalScore = myDataFromDB!.세트점수()
        //print ("  scoreJsonTest >>>  \(totalScore)   \(myDataFromDB!.scoreJson())")
        showMySelf()
    }

    /// DB 에 저장된 객체가 있는 지..
    func dataExistOfResc(is1st: Bool, adultCase: Bool = isAdult) -> Bool {
        //print("\n HmStudent :: dataExistOfResc   \(name)  \t dbid : \(dbid) \t  adultCase ? \(adultCase)    is1st ?  \(is1st) \t     arrTestObjDB : \(arrTestObjDB.count) ")
        var clsStr = "HmTestInft"
        if adultCase { clsStr = "HmTestAdlt" }
        let arrTst = arrTestObjDB.filter { (obj) -> Bool in
            //print("  dataExistOfResc  \(object_getClass(obj))  \(clsStr)    ")
            let hasObj =  String(object_getClass(obj)) == clsStr // filter..
            if adultCase {
                return hasObj && hasOperateData
            } else {
                return hasObj
            }
        }
        return 0 < arrTst.count
    }

    /// AHA, ERC, ANZ 리턴..
    func getStandardStr() -> String {
        let stnd = getStandardIdx()
        print(" AHA / ENC / ANZ \(#function)    \(stnd)  ")
        switch stnd {
        case 0:  return "AHA"
        case 1:  return "ERC"
        case 2:  return "ANZ"
        default: return ""
        }
    }

    /// 테스트 결과 기준 리턴.  없을 땐 -1.
    func getStandardIdx() -> Int {
        print(" AHA / ENC / ANZ \(#function)")
        //print("  \(name) :: isTestOldCase \(isTestOldCase)  count : \(arrTestOld.count) ")
        if AppIdTrMoTe == 1 {
            if isDataInDB {
                return myDataFromDB!.standard
            } else {
                if myData != nil { return myData!.standard }
                return -1
            }
        }
        if isTestOldCase && 0 < arrTestOld.count {
            //print("  isTestOldCase \(isTestOldCase)  count : \(arrTestOld.count)  standard :: >> \(arrTestOld.first!.standard)")
            return arrTestOld.first!.standard
        }
        
        if arrTestObjDB.count == 0 {
            return -1
        }
        return arrTestObjDB.last!.standard
    }

    /// 현재 기준과 일치하는가.. 혹은 처음 테스트..
    func activeForStandard() -> Bool {
        return getStandardIdx() == cprProtoN.theVal || arrTestObjDB.count == 0
    }

    /** 테스트 : DB 에서 읽은 정보 취합하여 객체 전달..  INFANT */
    func getInfantFromDB() -> HmTestInft? {
        let arrInf = arrTestObjDB.filter { (obj) -> Bool in
            return obj <^> HmTestInft()
        }
        if arrInf.count == 0 { return nil }

        for obj in arrInf {
            if let inf = obj as? HmTestInft {
                return inf
            }
            //if 0 < inf.dbID1 { rInfant.setResc1From(inf) }
            //if 0 < inf.dbID2 { rInfant.setResc2From(inf) }
        }
        return HmTestInft()
    }

    /** 테스트 : DB 에서 읽은 정보 취합하여 객체 전달..  ADULT */
    func getAdultFromDB() -> HmTestAdlt? {
        let arrInf = arrTestObjDB.filter { (obj) -> Bool in
            return obj <^> HmTestAdlt()
        }
        if arrInf.count == 0 { return nil }
        //let rInfant = HmTestAdult()
        for obj in arrInf {
            if let inf = obj as? HmTestAdlt {
                return inf
            }
            //if 0 < inf.dbID1 { rInfant.setResc1From(inf) }
            //if 0 < inf.dbID2 { rInfant.setResc2From(inf) }
        }
        //print("  isRescure1, 2 ? \(rInfant.isRes1) | \(rInfant.isRes2) ")
        return HmTestAdlt()
    }

    func getOperateTime() -> NSDate? {
        if AppIdTrMoTe == 1 {
            if !isDataInDB { return nil }
            return myDataFromDB?.timeStamp
        }
        if AppIdTrMoTe == 2 {
            if 0 < arrTestOld.count {
                let sorted = arrTestOld.sort({ (t1, t2) -> Bool in
                    t1.timeStamp.compare(t2.timeStamp) == .OrderedDescending // 거꾸로 하여 맨 앞에 것..
                })
                return sorted.first!.timeStamp
            }
            
            if arrTestObjDB.count == 0 { return nil }
            if 1 < arrTestObjDB.count {
                let sorted = arrTestObjDB.sort({ (t1, t2) -> Bool in
                    t1.timeStamp.compare(t2.timeStamp) == .OrderedDescending // 거꾸로 하여 맨 앞에 것..
                })   //{ $0.timestamp .compare($1.date) == NSComparisonResult.OrderedAscending }
                return sorted.first!.timeStamp
            } else {
                return arrTestObjDB.first!.timeStamp
            }
        }
        return nil
    }

    func getTestDateInfant(infantCase: Bool) -> NSDate? {
        //for dd in arrTestObjDB { print("  testObj date :  \(dd.timeStamp.formatFromOption(dateFormat))") }
        let objs = arrTestObjDB.filter { (tst) -> Bool in
            if infantCase { return !tst.amIAdult }
            else { return tst.amIAdult }
        }
        if objs.count == 0 { return nil }
        if 1 < objs.count {
            let sorted = objs.sort({ (t1, t2) -> Bool in
                t1.timeStamp.compare(t2.timeStamp) == .OrderedDescending // 거꾸로 하여 맨 앞에 것..
                })   //{ $0.timestamp .compare($1.date) == NSComparisonResult.OrderedAscending }
            return sorted.first!.timeStamp
        }
        return objs.first!.timeStamp
    }

    func getTestDateTotal() -> NSDate? {
        let ift = getTestDateInfant(true)
        let adt = getTestDateInfant(false)
        if ift == nil { return adt }
        if adt == nil { return ift }
        if ift!.compare(adt!) == .OrderedDescending { return ift }
        return adt
    }

    /** 테스트 : 오퍼레이션 정보  JSON  */
    func operationJsonString() -> String {
        //if myDataFromDB == nil || 0 == myDataFromDB?.arrSet.count { print("  myDataFromDB == nil   "); return "" }
        var json = HsJsonBase(), cTime = "", cCnt = "", cCorCnt = "", rTime = "", rCnt = "", rCorCnt = ""
        for obj in myDataFromDB!.arrSet {
            cTime += obj.ccTimeIntStr + ", " // 소수 1자리..
            cCnt += "\(obj.ccCount), "
            cCorCnt += "\(obj.ccPassCnt), "
            rTime += obj.rpTimeIntStr + ", "
            rCnt += "\(obj.rpCount), "
            rCorCnt += "\(obj.rpPassCnt), "
        }
        json.addKeyArray("comp_time", value: json.makeArray(cTime.removeLast(2)))
        json.addKeyArray("comp_count", value: json.makeArray(cCnt.removeLast(2)))
        json.addKeyArray("comp_correct", value: json.makeArray(cCorCnt.removeLast(2)))
        json.addKeyArray("resp_time", value: json.makeArray(rTime.removeLast(2)))
        json.addKeyArray("resp_count", value: json.makeArray(rCnt.removeLast(2)))
        json.addKeyArray("resp_correct", value: json.makeArray(rCorCnt.removeLast(2)), withFinishComma: false) // 계속 되므로. 아닌거같애..
        return json.jStr
    }

    func operationJsonStringTest() -> String {
        //if myDataFromDB == nil || 0 == myDataFromDB?.arrSet.count { print("  myDataFromDB == nil   "); return "" }
        var json = HsJsonBase(), cTime = "", cCnt = "", cCorCnt = "", rTime = "", rCnt = "", rCorCnt = ""
        for obj in myDataFromDB!.arrSet {
            cTime += obj.ccTimeIntStr + ", " // 소수 1자리..
            cCnt += "\(obj.ccCount), "
            cCorCnt += "\(obj.ccPassCnt), "
            rTime += obj.rpTimeIntStr + ", "
            rCnt += "\(obj.rpCount), "
            rCorCnt += "\(obj.rpPassCnt), "
        }
        json.addKeyArray("comptime", value: json.makeArray(cTime.removeLast(2)))
        json.addKeyArray("compcount", value: json.makeArray(cCnt.removeLast(2)))
        json.addKeyArray("compnum", value: json.makeArray(cCorCnt.removeLast(2)))
        json.addKeyArray("resptime", value: json.makeArray(rTime.removeLast(2)))
        json.addKeyArray("respcount", value: json.makeArray(rCnt.removeLast(2)))
        json.addKeyArray("respnum", value: json.makeArray(rCorCnt.removeLast(2)), withFinishComma: false)
        return json.jStr
    }

    func getDataObj() -> HsData? {
        if myData != nil { return myData }
        if myDataFromDB != nil { return myDataFromDB }
        return nil
    }

    func showMySelf() {
        print("\n \t\t\t \(barLDashBase) \(barLDashBase) \t\t HmStudent :: showMySelf >>>    dbid : \(dbid)  svrID : \(svrID)   svrSaved : \(svrSaved)  " +
            "\n\t\t    name : \(name), email : \(email),  phone : \(phone), age : \(age)   ")
        print("\t\t\t\t data|dataDB : \(myData) | \(myDataFromDB)  ")
        if myDataFromDB != nil {
            print("\t\t\t    DB에 저장된 세트 수..> \t myDataFromDB?.arrSet.count : \(myDataFromDB?.arrSet.count) ") }
        if myData != nil {
            print("\t\t\t    SetNum in DB   >>>  \t myData?.arrSet.count : \(myData?.arrSet.count) ") }
    }

    func updateDatabase() {
        if email == "user@unknown" {
            print("  이메일 user@unknown 케이스.. 스킵..")
            return
        }
        HsBleMaestr.inst.fmdbHandler.updateAStudent(self)
    }

    // MARK: Test 메인/서브 테스터 세팅 관련.
    func setMainOrSubInTester(isMain: Bool) {
        isMainTester = isMain
    }

    func releaseTester() { // 테스터 선택 아님...
        isMainTester = nil
    }

    func debugMyStateInTest() {
        print("debugMyState ::  \(name)   >> isMainTester : \(isMainTester) ")
    }
    
}
