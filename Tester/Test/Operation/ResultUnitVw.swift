//
//  ResultUnitVw.swift
//  HS Test
//
//  Created by Jongwoo Moon on 2016. 10. 5..
//  Copyright © 2016년 IMLab. All rights reserved.
//


import Foundation

/// A1,2, R1, 2 등은 VC 에서 세팅하도록 하자.. 여기 저기서 다른 값이 들어가므로..
class ResultUnitView: HtView {

    var container: HtTableView!

    @IBOutlet var arrView: [UIView]!

    var view1: ResultSingleRow!, view2: ResultSingleRow!,
    view3: ResultSingleRow!, view4: ResultInfantStep4!,
    view5: ResultSingleRow!, view6: ResultInfant2A!, view7: ResultInfant2A!


    @IBOutlet weak var labelA1: UILabel!
    @IBOutlet weak var labelA2: UILabel!
    @IBOutlet weak var labelR1: UILabel!
    @IBOutlet weak var labelR2: UILabel!

    var infant: HmTestInft?, adult: HmTestAdlt?

    func initSet(infant: HmTestInft) {
        super.initSet()
        self.infant = infant
        backgroundColor = colorGreyBright
        addSubViews()
    }

    private func addBlsSteps(tObj: HmTest) {
        print("  ResultUnitVw  ::  addBlsSteps(tObj: HmTestBase) ")
//        let (stepStrArr, valArr) = tObj.getBlsStepArray()
//
//        let bundle = NSBundle(forClass: self.dynamicType)
//        let nib = UINib(nibName: "ResultSingleRow", bundle: bundle)
//
//        for (idx, st) in stepStrArr.enumerate() {
//            let newVw = nib.instantiateWithOwner(self, options: nil).last as? ResultSingleRow
//            //newVw!.frame = CGRect(x: 0, y: 0, width: 320, height: 45)
//            newVw?.setTitleMain("\(idx + 1)", main: st)
//            newVw?.isLabelCase(false); newVw!.singleRow()
//            newVw?.setCheckImgB(valArr[idx]) // 1번 값 설정..
//            //newVw!.cornerRad(6)
//            container.data[0].arrRow.append(newVw!) // 첫번째 칸에 추가.
//        }
    }

    private func addSubViews() {
        print("  ResultUnitVw  ::  func addSubViews() ")

        let headHght: CGFloat = 80
        let tableRect = CGRect(x: 0, y: headHght, width: 668, height: 820 - headHght)
        container = HtTableView(frame: tableRect)
        container.offset = 3
        container.initSet(tableRect, scroll: true)
        container.backgroundColor = UIColor.clearColor()
        container.tableView.backgroundColor = UIColor.clearColor()
        container.data = [
            HtTableSection(title: langStr.obj.bls_infant_sheet_title_1,
                headerHeight: 40, arrRow: []),
            HtTableSection(title: langStr.obj.bls_infant_sheet_title_2,
                headerHeight: 40, arrRow: [] )
        ]
        container.tableView.reloadData()

        addBlsSteps(infant!)

        addSubview(container)
    }

    // MARK:  언어 세팅.
    override func setLanguageString() {
//        labelTitle1.text = langStr.obj.bls_infant_sheet_title_1
//        labelTitle2.text = langStr.obj.bls_infant_sheet_title_2
    }
}



