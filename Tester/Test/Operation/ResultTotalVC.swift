//
//  ResultTableVC.swift
//  Test
//
//  Created by Jongwoo Moon on 2016. 3. 10..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation

/// 학생 정보 관리 화면에서 불림.. DB 정보 뿌려줌..


class ResultTotalVC : HtViewController, ResultPrtcBls {
    var log = HtLog(cName: "ResultTotalVC")

    var container: HtTableView?
    var titleNum: Int = 0

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var btnGraph: UIButton!
    @IBOutlet weak var btnResult: UIButton!

    @IBOutlet weak var labelDate: UILabel!

    var infantVw: UIView?, adultVw: UIView?

    var stdnt = HmStudent()
    var infant: HmTestInft?, adult: HmTestAdlt?

    @IBAction func bttnActResult(sender: AnyObject) {
        closeMyself()
//        showGeneralPopup(langStr.obj.cancel, message: langStr.obj.want_back, yesStr: langStr.obj.yes, noString: langStr.obj.no, yesAction: {  self.closeMyself() })
    }

    @IBAction func bttnActGraph(sender: AnyObject) {
        if !stdnt.dataExistOfResc(true, adultCase: true) { return }

        let vc = ResultGraphVC(nibName: "ResultGraph", bundle: nil)
        vc.dbStudent = stdnt
        presentViewController(vc, animated: viewAnimate, completion: nil)
    }

    //////////////////////////////////////////////////////////////////////     [ UI   ViewDidLoad   << >> ]

    override func viewDidLoad() {
        super.viewDidLoad()
        log.printAlways("viewDidLoad", lnum: 5)

        setLanguageString()

        let tableRect = CGRect(x: 50, y: 100, width: 668, height: 840)
        container = HtTableView(frame: tableRect)
        //container!.offset = 3
        container!.initSet(tableRect, scroll: true)
        container!.backgroundColor = UIColor.clearColor()
        container!.tableView.backgroundColor = UIColor.clearColor()

        container!.data = []

        addInfantView()
        addAdultView()

        container!.tableView.reloadData()
        view.addSubview(container!)

        stdnt.myDataFromDB!.세트점수()

        // Debug_JSon Print
        HsStudentDataJobs.singltn.setJsonString(stdnt) // 디버깅 용
    }

    func addInfantView() {
        let infantExist = stdnt.finishedInfant
        if infantExist == false {
            print(" Infant 없슴...")
            return
        }
        print("addInfantResultView")        //titleNum = 1

        container!.data = [
            HtTableSection(title: langStr.obj.bls_infant_sheet_title_1,
                           headerHeight: 60, arrRow: [UIView]()),
            HtTableSection(title: "Infant CPR Stpe 1 and 2", //   langStr.obj.bls_infant_sheet_title_2,
                headerHeight: 60, arrRow: [UIView]()),
            HtTableSection(title: "Infant CPR Stpe 3 and 4", //   langStr.obj.bls_infant_sheet_title_2,
                headerHeight: 60, arrRow: [UIView]())
        ]
        titleNum = 3
        let testObj = stdnt.getInfantFromDB()
        insertBlsSteps(testObj!, idx: 0)
        insertInfantStep2(testObj!, idx: 1)
    }

    func addAdultView() {
        let adultExist = stdnt.finishedAdult
        if adultExist == false {
            print(" Adult 없슴...")
            return
        }
        container!.data.append(
            HtTableSection(title: langStr.obj.bls_adult_sheet_title_1,
                           headerHeight: 60, arrRow: [UIView]()))
        container!.data.append(
            HtTableSection(title: langStr.obj.bls_adult_sheet_title_2,
                           headerHeight: 60, arrRow: [UIView]()))
        container!.data.append(
            HtTableSection(title: langStr.obj.bls_adult_sheet_title_3,
                           headerHeight: 60, arrRow: [UIView]()))
        container!.data.append(
            HtTableSection(title: langStr.obj.bls_adult_sheet_title_4,
                           headerHeight: 60, arrRow: [UIView]()))

        let adult = stdnt.getAdultFromDB()
        insertBlsSteps(adult!, idx: titleNum)
        insertAdultConductCPR(adult!, dataObj: stdnt.myDataFromDB!, idx: titleNum + 1)
        insertAdultAED(adult!, idx: titleNum + 2)
    }

    func closeMyself() {
        dismissViewControllerAnimated(viewAnimate, completion: nil)
    }

    // MARK:  언어 세팅.
    func setLanguageString() {
        labelTitle.text = langStr.obj.all_title
        labelDate.text = stdnt.getTestDateTotal()?.formatFromOption(dateFormat)

        //btnResult.localizeSizeAlignment(1)
        btnGraph.localizeSizeAlignment(1)
        //btnResult.setTitle(langStr.obj.manage_data, forState: .Normal)
        btnGraph.setTitle(langStr.obj.graph, forState: .Normal)
    }
    
}



