//
//  ResultViewAdult.swift
//  Test
//
//  Created by Jongwoo Moon on 2016. 2. 22..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation

class ResultViewAdult: HtView {

    @IBOutlet var arrView: [UIView]!
    @IBOutlet weak var labelTitle1: UILabel!
    @IBOutlet weak var labelTitle2: UILabel!
    @IBOutlet weak var labelTitle3: UILabel!

    @IBOutlet weak var view1: ResultSingleRow!
    @IBOutlet weak var view2: ResultSingleRow!
    @IBOutlet weak var view3: ResultSingleRow!
    @IBOutlet weak var view4: ResultAdultCPR!

    @IBOutlet weak var view5: ResultSingleRow!
    @IBOutlet weak var view6: ResultSingleRow!
    @IBOutlet weak var view7: ResultSingleRow!
    @IBOutlet weak var view8: ResultSingleRow!

    @IBOutlet weak var labelA1: UILabel!
    @IBOutlet weak var labelA2: UILabel!
    @IBOutlet weak var labelR1: UILabel!
    @IBOutlet weak var labelR2: UILabel!

    @IBOutlet weak var afterShockView: ResultAdultAfterShock!

    func initSet(adlt1: HmTestAdlt, dataObj: HsData) {
        super.initSet()
        //for vw in arrView { vw.cornerRad(5) }

        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "ResultSingleRow", bundle: bundle)
        let nib4 = UINib(nibName: "ResultAdultCPR", bundle: bundle)
        let nibas = UINib(nibName: "ResultAdultAfterShock", bundle: bundle)

//        var v1 = nib.instantiateWithOwner(self, options: nil).last as? ResultSingleRow
//        v1?.labelTitle.text = "1"
//        v1?.labelMain.text = langStr.obj.bls_adult_sheet_check_1
//        v1?.setCheckImgB(adlt1.tst1) // 리스폰스...
//        v1?.isLabelCase(false); v1?.singleRow()
//        view1.addSubview(v1!)
//
//        v1 = nib.instantiateWithOwner(self, options: nil).last as? ResultSingleRow
//        v1?.labelTitle.text = "2"
//        v1?.labelMain.text = langStr.obj.bls_adult_sheet_check_2
//        v1?.setCheckImgB(adlt1.tst2) // 이머젼시
//        v1?.isLabelCase(false); v1?.singleRow()
//        view2.addSubview(v1!)
//        v1 = nib.instantiateWithOwner(self, options: nil).last as? ResultSingleRow
//        v1?.labelTitle.text = "3"
//        v1?.labelMain.text = langStr.obj.bls_adult_sheet_check_3
//        v1?.setCheckImgB(adlt1.tst3) // 체크 펄스.
//        v1?.isLabelCase(false); v1?.singleRow()
//        view3.addSubview(v1!)
//
//        let v4 = nib4.instantiateWithOwner(self, options: nil).last as? ResultAdultCPR
//        v4?.initSet(adlt1, dataObj: dataObj)  // 4번 값 설정..
//        view4.addSubview(v4!)
//
//        v1 = nib.instantiateWithOwner(self, options: nil).last as? ResultSingleRow
//        v1?.labelTitle.text = "5"
//        v1?.labelMain.text = langStr.obj.bls_adult_sheet_check_5
//        v1?.setCheckImgB(adlt1.tst5) // 5번 값 설정
//        v1?.initSet(); v1?.isLabelCase(false); v1?.singleRow()
//        view5.addSubview(v1!)
//
//        v1 = nib.instantiateWithOwner(self, options: nil).last as? ResultSingleRow
//        v1?.labelTitle.text = "6"
//        v1?.labelMain.text = langStr.obj.bls_adult_sheet_check_6
//        v1?.setCheckImgB(adlt1.tet6) // 6번 값 설정
//        v1?.initSet(); v1?.isLabelCase(false); v1?.singleRow()
//        view6.addSubview(v1!)
//
//        v1 = nib.instantiateWithOwner(self, options: nil).last as? ResultSingleRow
//        v1?.labelTitle.text = "7"
//        v1?.labelMain.text = langStr.obj.bls_adult_sheet_check_7
//        v1?.setCheckImgB(adlt1.tet7) // 7번 값 설정
//        v1?.initSet(); v1?.isLabelCase(false); v1?.singleRow()
//        view7.addSubview(v1!)
//
//        v1 = nib.instantiateWithOwner(self, options: nil).last as? ResultSingleRow
//        v1?.labelTitle.text = "8"
//        v1?.labelMain.text = langStr.obj.bls_adult_sheet_check_8
//        v1?.setCheckImgB(adlt1.tet8) // 8번 값 설정
//        v1?.initSet(); v1?.isLabelCase(false); v1?.singleRow()
//        view8.addSubview(v1!)
//
//        let asVw = nibas.instantiateWithOwner(self, options: nil).last as? ResultAdultAfterShock
//        asVw?.initSet(adlt1, resc2: adlt1) // 9번 객체 할당..
//        afterShockView.addSubview(asVw!)
    }

    override func setLanguageString() {
        labelTitle1.text = langStr.obj.bls_adult_sheet_title_1
        labelTitle2.text = langStr.obj.bls_adult_sheet_title_2
        labelTitle3.text = langStr.obj.bls_adult_sheet_title_3
    }
    
}
