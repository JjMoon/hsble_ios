//
//  SubBlsView.swift
//  Test
//
//  Created by Jongwoo Moon on 2016. 2. 16..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation

class SubAdultFinal : HtView { // 성인 최종 항목
    var checkMan = CheckButtonManager()
    var bleMan: HsBleSuMan?

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelCheckAll: UILabel!
    @IBOutlet weak var btnCheckAll: UIButton!

    @IBOutlet weak var const100or10: NSLayoutConstraint!

    @IBAction func btnActCheck(sender: UIButton) { // 3개 버튼의 액션
        checkMan.toggleCheck(sender)
    }

    @IBAction func btnActCheckAll(sender: UIButton) { // 모두 선택
        checkMan.toggleCheckAll()
    }

    @IBAction func btnActNext(sender: AnyObject) {
        let arrbtn = checkMan.arrBttn, adlt = bleMan!.testObj as! HmTestAdlt
        // "전원켬" "패드부착" "분석물러남" "제세동물러남" "제세동" "압박재개"
        adlt.setStepRestProps([ "압박재개" : arrbtn[0].tag ])
        uiCallBack()
    }

    func initSet(bleObj: HsBleSuMan) {
        super.initSet()
        bleMan = bleObj
        bleMan!.operationStop()

        checkMan.allBttn = btnCheckAll; checkMan.labelCheckAll = labelCheckAll
        checkMan.arrBttn.removeAll()

        var yPosi : CGFloat = 40
        let yDist : CGFloat = 999
        let arrLblString = [ langStr.obj.bls_adult_3_check_1 ]

        for str in arrLblString {
            let nVw = HsCheckView(posi: CGPoint(x: 15, y: yPosi), labelText: str)
            checkMan.arrBttn.append(nVw.bttn!)
            nVw.bttn!.addTarget(self, action: #selector(btnActCheck), forControlEvents: .TouchUpInside)
            addSubview(nVw)
            yPosi += yDist
        }

        cornerRad(20)
        print("  set Infant conduct CPR  ... ")
    }

    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        let bezPath = UIBezierPath()
        let yCo : CGFloat = 230
        bezPath.moveToPoint(CGPoint(x: 20, y: yCo))
        bezPath.addLineToPoint(CGPoint(x: 250, y: yCo))
        bezPath.lineWidth = 0.4
        colorBttnDarkGray.setStroke()
        bezPath.lineCapStyle = .Round
        bezPath.stroke()
    }

    // MARK:  언어 세팅.
    override func setLanguageString() {
        labelTitle.text =     langStr.obj.bls_adult_3_title
        labelCheckAll.text = langStr.obj.all_check
    }
}
