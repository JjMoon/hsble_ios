//
//  InfantResultView.swift
//  Test
//
//  Created by Jongwoo Moon on 2016. 2. 19..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation

class InfantResultView: HtView {

    @IBOutlet var arrView: [UIView]!

    @IBOutlet weak var view4: ResultWithTitleBar!
    @IBOutlet weak var view6: ResultWithTitleBar!
    @IBOutlet weak var view7: ResultWithTitleBar!

    @IBOutlet weak var labelTitle1: UILabel!
    @IBOutlet weak var labelTitle2: UILabel!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var img1chk: UIImageView!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var img2chk: UIImageView!
    @IBOutlet weak var label3: UILabel!
    @IBOutlet weak var img3chk: UIImageView!

    @IBOutlet weak var label4_0: UILabel!
    @IBOutlet weak var label4_1: UILabel!
    @IBOutlet weak var label4_2: UILabel!
    @IBOutlet weak var label4_3: UILabel!
    @IBOutlet weak var label4_4: UILabel!
    @IBOutlet weak var label4_5: UILabel!
    @IBOutlet weak var img4_1chk: UIImageView!
    @IBOutlet weak var img4_3chk: UIImageView!
    @IBOutlet weak var img4_4chk: UIImageView!
    @IBOutlet weak var img4_5chk: UIImageView!


    @IBOutlet weak var label5: UILabel!
    @IBOutlet weak var img5: UIImageView!

    @IBOutlet weak var label6_0: UILabel!
    @IBOutlet weak var label6_1: UILabel!
    @IBOutlet weak var label6_2: UILabel!
    @IBOutlet weak var img6Achk: UIImageView!
    @IBOutlet weak var img6Bchk: UIImageView!


    @IBOutlet weak var label7_0: UILabel!
    @IBOutlet weak var label7_1: UILabel!
    @IBOutlet weak var label7_2: UILabel!


    override func initSet() {
        super.initSet()
        backgroundColor = colorGreyBright
        view4.subLineNum = 5
        view4.unitY = 35 // 5 * 35 + 40 하부 공간 5 = 175 + 40 + 5 = 220
        view6.subLineNum = 2
        view7.subLineNum = 2

        for vw in arrView {
            vw.layer.cornerRadius = 5
        }
    }



    // MARK:  언어 세팅.
    override func setLanguageString() {
        labelTitle1.text = langStr.obj.bls_infant_sheet_title_1
        labelTitle2.text = langStr.obj.bls_infant_sheet_title_2

        label1.text = langStr.obj.bls_infant_sheet_check_1
        label2.text = langStr.obj.bls_infant_sheet_check_2
        label3.text = langStr.obj.bls_infant_sheet_check_3
        label4_0.text = langStr.obj.bls_infant_sheet_check_4
        label4_1.text = langStr.obj.bls_infant_sheet_check_4_1
        label4_2.text = langStr.obj.bls_infant_sheet_check_4_2
        label4_3.text = langStr.obj.bls_infant_sheet_check_4_3
        label4_4.text = langStr.obj.bls_infant_sheet_check_4_4
        label4_5.text = langStr.obj.bls_infant_sheet_check_4_5

        label5.text = langStr.obj.bls_infant_sheet_check_5
        label6_0.text = langStr.obj.bls_infant_sheet_check_6
        label6_1.text = langStr.obj.bls_infant_sheet_check_6_1
        label6_2.text = langStr.obj.bls_infant_sheet_check_6_2
        label7_0.text = langStr.obj.bls_infant_sheet_check_7
        label7_1.text = langStr.obj.bls_infant_sheet_check_7_1
        label7_2.text = langStr.obj.bls_infant_sheet_check_7_2
    }
}

class ResultWithTitleBar: UIView {
    var subLineNum = 1
    var unitY: CGFloat = 50


    override func drawRect(rect: CGRect) {
        super.drawRect(rect)

        let bezPath = UIBezierPath(), yV: CGFloat = 40, stX: CGFloat = 70,
        edX = frame.size.width - 10
        bezPath.moveToPoint   (CGPoint(x: stX, y: yV))
        bezPath.addLineToPoint(CGPoint(x: edX, y: yV))
        bezPath.lineWidth = 2
        colorBttnDarkGray.setStroke()
        bezPath.lineCapStyle = .Round
        bezPath.stroke()

        var curY = yV + unitY

        for _ in 1...subLineNum - 1 {
            let bez = UIBezierPath()
            bez.moveToPoint     (CGPoint(x: stX, y: curY))
            bez.addLineToPoint  (CGPoint(x: edX, y: curY))
            bez.lineWidth = 1
            colorBttnDarkGray.setStroke()
            bez.lineCapStyle = .Round;   bez.stroke()
            curY += unitY
        }
        layer.cornerRadius = 5
    }


}