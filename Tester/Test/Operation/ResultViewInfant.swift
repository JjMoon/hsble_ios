//
//  InfantResultView.swift
//  Test
//
//  Created by Jongwoo Moon on 2016. 2. 19..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation

/**

 더이상 안 씀..

 */


/// A1,2, R1, 2 등은 VC 에서 세팅하도록 하자.. 여기 저기서 다른 값이 들어가므로..
class ResultViewInfant: HtView {

    @IBOutlet var arrView: [UIView]!

    @IBOutlet weak var view1: ResultSingleRow!
    @IBOutlet weak var view2: ResultSingleRow!
    @IBOutlet weak var view3: ResultSingleRow!
    @IBOutlet weak var view4: ResultInfantStep4!

    @IBOutlet weak var view5: ResultSingleRow!
    @IBOutlet weak var view6: ResultInfant2A!
    @IBOutlet weak var view7: ResultInfant2A!


    @IBOutlet weak var labelTitle1: UILabel!
    @IBOutlet weak var labelTitle2: UILabel!

    @IBOutlet weak var labelA1: UILabel!
    @IBOutlet weak var labelA2: UILabel!
    @IBOutlet weak var labelR1: UILabel!
    @IBOutlet weak var labelR2: UILabel!

    func initSet(infant: HmTestInft) {
        super.initSet()
//        backgroundColor = colorGreyBright
//
//        //for vw in arrView { vw.cornerRad(5) }
//
//        let bundle = NSBundle(forClass: self.dynamicType)
//        let nib = UINib(nibName: "ResultSingleRow", bundle: bundle)
//        let nib4 = UINib(nibName: "ResultInfantStep4", bundle: bundle)
//        let nib67 = UINib(nibName: "ResultInfant2A", bundle: bundle)

//        var newView = nib.instantiateWithOwner(self, options: nil).last as? ResultSingleRow
//        newView?.setTitleMain("1", main: langStr.obj.bls_infant_sheet_check_1)
//        newView?.isLabelCase(false); newView?.singleRow()
//        newView?.setCheckImgB(infant.tst1) // 1번 값 설정..
//        view1.addSubview(newView!)
//        newView = nib.instantiateWithOwner(self, options: nil).last as? ResultSingleRow
//        newView?.setTitleMain("2", main: langStr.obj.bls_infant_sheet_check_2)
//        newView?.isLabelCase(false); newView?.singleRow()
//        newView?.setCheckImgB(infant.tst2) // 2번 값 설정..
//        view2.addSubview(newView!)
//        newView = nib.instantiateWithOwner(self, options: nil).last as? ResultSingleRow
//        newView?.setTitleMain("3", main: langStr.obj.bls_infant_sheet_check_3)
//        newView?.isLabelCase(false); newView?.singleRow()
//        newView?.setCheckImgB(infant.tst3)  // 3번 값 설정..
//        view3.addSubview(newView!)
//        newView = nib.instantiateWithOwner(self, options: nil).last as? ResultSingleRow
//        newView?.setTitleMain("5", main: langStr.obj.bls_infant_sheet_check_5)
//        newView?.isLabelCase(false); newView?.singleRow()
//        newView?.setCheckImgB(infant.tst5)  // 5번 값 설정..
//        view5.addSubview(newView!)
//        let vw4 = nib4.instantiateWithOwner(self, options: nil).last as? ResultInfantStep4
//        //vw4?.initSet(infant); view4.addSubview(vw4!) // 4번 값 설정..
//
//        // 6, 7 번 모두 1st 구조자 값 세팅.
//        var vw6 = nib67.instantiateWithOwner(self, options: nil).last as? ResultInfant2A
//        vw6?.labelTitle.text = "6"; vw6?.initSet(infant); vw6?.setCololumn6(); view6.addSubview(vw6!)
//        vw6 = nib67.instantiateWithOwner(self, options: nil).last as? ResultInfant2A
//        vw6?.labelTitle.text = "7"; vw6?.initSet(infant); vw6?.setCololumn7(); view7.addSubview(vw6!)
    }

    // MARK:  언어 세팅.
    override func setLanguageString() {
        labelTitle1.text = langStr.obj.bls_infant_sheet_title_1
        labelTitle2.text = langStr.obj.bls_infant_sheet_title_2
    }
}

class ResultWithTitleBar: UIView {
    var subLineNum = 1
    var unitY: CGFloat = 50


    override func drawRect(rect: CGRect) {
        super.drawRect(rect)

        let bezPath = UIBezierPath(), yV: CGFloat = 40, stX: CGFloat = 70,
        edX = frame.size.width - 10
        bezPath.moveToPoint   (CGPoint(x: stX, y: yV))
        bezPath.addLineToPoint(CGPoint(x: edX, y: yV))
        bezPath.lineWidth = 2
        colorBttnDarkGray.setStroke()
        bezPath.lineCapStyle = .Round
        bezPath.stroke()

        var curY = yV + unitY

        for _ in 1...subLineNum - 1 {
            let bez = UIBezierPath()
            bez.moveToPoint     (CGPoint(x: stX, y: curY))
            bez.addLineToPoint  (CGPoint(x: edX, y: curY))
            bez.lineWidth = 1
            colorBttnDarkGray.setStroke()
            bez.lineCapStyle = .Round;   bez.stroke()
            curY += unitY
        }
        //layer.cornerRadius = 5
    }


}
