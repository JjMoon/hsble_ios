//
//  SubBlsView.swift
//  Test
//
//  Created by Jongwoo Moon on 2016. 2. 16..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation

class SubBlsView : HtView {
    var log = HtLog(cName: "SubBlsView")
    var bleMan: HsBleSuMan?
    var checkMan = CheckButtonManager()

    var arrChkView = [HsCheckView]()

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var labelCheckAll: UILabel!
    @IBOutlet weak var btnCheckAll: UIButton!

    func btnActCheck(sender: UIButton) { // 3개 버튼의 액션
        checkMan.toggleCheck(sender)
    }

    @IBAction func btnActCheckAll(sender: UIButton) { // 모두 선택
        checkMan.toggleCheckAll()
    }

    @IBAction func btnActNext(sender: AnyObject) {
        // 저장하는 부분..
        let arrbtn = checkMan.arrBttn, tObj = bleMan!.testObj!

        switch cprProtoN.theVal {
        case 0: // AHA
            tObj.setStepBaseProps([ "의식확인" : arrbtn[0].tag, "구조AED" : arrbtn[1].tag,
                "호흡확인" : arrbtn[2].tag, "맥박확인" : arrbtn[3].tag ])
        case 1:
            tObj.setStepBaseProps([ "안전확인" : arrbtn[0].tag, "의식확인" : arrbtn[1].tag,
                "기도확보" : arrbtn[2].tag, "호흡확인" : arrbtn[3].tag, "구조AED" : arrbtn[4].tag ])
        default:
            tObj.setStepBaseProps([ "안전확인" : arrbtn[0].tag, "의식확인" : arrbtn[1].tag,
                "구조AED" : arrbtn[2].tag, "기도확보" : arrbtn[3].tag, "호흡확인" : arrbtn[4].tag ])
        }
        uiCallBack()
    }

    func initSet(bleObj: HsBleSuMan) {
        super.initSet()
        bleMan = bleObj
        //checkMan.arrBttn = [ btnResponse, btnEmergency, btnCheckPulse ]
        checkMan.allBttn = btnCheckAll; checkMan.labelCheckAll = labelCheckAll

        checkMan.arrBttn.removeAll()

        var yPosi : CGFloat = 40, yDist : CGFloat = 35,
        arrLblString = [ langStr.obj.response, langStr.obj.emergency,
                         langStr.obj.check_breath, langStr.obj.check_pulse ]
        switch cprProtoN.theVal {
        case 0:
            yDist = 45 // 60
        case 1:
            arrLblString = [ langStr.obj.check_danger, langStr.obj.response, langStr.obj.open_airway,
                             langStr.obj.check_breath, langStr.obj.emergency ]
        case 2:
            arrLblString = [ langStr.obj.check_danger, langStr.obj.response, langStr.obj.emergency,
                             langStr.obj.open_airway, langStr.obj.check_breath ]
        default: break
        }

        for str in arrLblString {
            // view 추가
            let nVw = HsCheckView(posi: CGPoint(x: 15, y: yPosi), labelText: str)
            checkMan.arrBttn.append(nVw.bttn!)
            nVw.bttn!.addTarget(self, action: #selector(btnActCheck), forControlEvents: .TouchUpInside)
            addSubview(nVw)
            yPosi += yDist
        }

        // 동작 진행..
        if isAdult && bleMan!.isConnectionTotallyFinished {
            bleMan?.testOperationStart()
        }
        layer.cornerRadius = 15
        print("  set check view ... ")
    }

    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        let bezPath = UIBezierPath()
        let yCo : CGFloat = 230
        bezPath.moveToPoint(CGPoint(x: 20, y: yCo))
        bezPath.addLineToPoint(CGPoint(x: 250, y: yCo))
        bezPath.lineWidth = 0.4
        colorBttnDarkGray.setStroke()
        bezPath.lineCapStyle = .Round
        bezPath.stroke()
    }

    // MARK:  언어 세팅.
    override func setLanguageString() {
        if isAdult {
            lblTitle.text = langStr.obj.bls_adult_1_title
        } else {
            lblTitle.text = langStr.obj.bls_infant_1_title
        }
//        labelResponse.text = langStr.obj.response
//        labelEmergency.text = langStr.obj.emergency
//        labelCheckPulse.text = langStr.obj.check_pulse
        labelCheckAll.text = langStr.obj.all_check
    }
}


