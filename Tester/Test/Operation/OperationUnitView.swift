//
//  OperationUnitView.swift
//  Test
//
//  Created by Jongwoo Moon on 2016. 1. 25..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation

class OperationUnitView: HtView {
    var log = HtLog(cName: "OperateUnitView")
    var uiTimer = NSTimer(), cnt = 0
    var adultObj = HmTestAdlt(), infantObj = HmTestInft(), bleMan:HsBleSuMan?
    var arrUiObj = HsUIArray()

    var curStateNum = 0
    //var speedLogic : HsSpeedLgcMon?
    var subView: HtView?, blsView: SubBlsView?, afdView: SubAfdView?, stepView: SubStepCheckView?, operView: OperatingView?

    var btnXCloseResultAction = { }
    var showDataSaveButtonView = { }
    var sensorLostCallback: (OperationUnitView, String) -> () =
        { (unitV: OperationUnitView, String) -> Void in }

    /// 화면 확대 관련..
    var enlarged = false
    var bttnClose: UIButton?, enlargeCljr: (OperationUnitView) -> () =  { (vw: OperationUnitView) -> Void in }


    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelStudent1: UILabel! // bleMan 에 할당되어 있는 학생..
    @IBOutlet weak var labelStudent2: UILabel!
    @IBOutlet weak var bttnCloseResult: UIButton!


    @IBAction func bttnMainX(sender: AnyObject) {
        btnXCloseResultAction()
    }

    //////////////////////////////////////////////////////////////////////     [ UI update   << >> ]
    func update() {
        connectionCheck()
    }

    // MARK: 기타 private
    private func connectionCheck() {
        //log.printThisFunc(" connectionCheck : \(labelTitle.text)")

        // 센서 체크
        let sensorInfo = bleMan?.stateMan.sensorStateCheck(), preKitName = bleMan!.prevKitName
        if sensorInfo != nil {
            sensorLostCallback(self, sensorInfo! + " (\(preKitName))")
            sensorLostConnectionAndRestartTheSession()
        }

        // if isTrainerConnected { labelTitle.textColor = colorBttnDarkGreen } 요건 트레이너가 연결 됐는 지 확인 할 때..
        //else { labelTitle.textColor = colorBttnDarkGray }
        labelTitle.textColor = colorTxtGreen // 일단은 항상 푸르게 푸르게... ㅋㅋ

        log.logThis("  View : \(labelTitle.text)  \(HsUtil.curStepStr())  ", lnum: 2)

        //if bleMan == nil { disableThisView(); return }
        if (bleMan?.isConnected == true) {

        } else {
            print("\n\n\n\n\n  DisConnected     \n\n\n\n\n")
            disableThisView()
        }
        log.함수차원_출력_End()
    }

    func sensorLostConnectionAndRestartTheSession() {
        bleMan?.testOperationStart()
        operView?.resetCell()
    }


    //////////////////////////////////////////////////////////////////////     [   화면 확대 관련..   << >> ]
    func addEnlargeButton() {
        bttnClose = UIButton()
        bttnClose!.frame.origin = CGPoint(x: 50, y: 50)
        bttnClose!.frame.size = frame.size.offset(-100, hei: -150)

        // bttnClose!.titleLabel?.text = "Close this.."
        bttnClose!.addTarget(self, action: #selector(self.shrinkAction), forControlEvents: .TouchUpInside)
        self.addSubview(bttnClose!)
        bringSubviewToFront(bttnClose!)
    }

    func shrinkAction(sender: UIButton!) {
        print("shrinkAction  ...  in   operation unit view ")
        enlarged = !enlarged
        bttnCloseResult.hidden = enlarged
        enlargeCljr(self)
    }

    private func addTestSubView(nV: UIView) {
        addSubview(nV)
        if (bttnClose == nil) {
            HsGlobal.delay(0.2, closure: { 
                self.closeButtonBringToFront()
            })
        }
        closeButtonBringToFront()
    }

    func closeButtonBringToFront() {
        if (bttnClose != nil) { bringSubviewToFront(bttnClose!) }
    }

    //////////////////////////////////////////////////////////////////////     [   ..   << >> ]
    func resetStateBySensorError() {
//        if stepMan != nil {
//            bleMan?.calcManager.data.reset()
//            //stepByStepSetState(prevStepState)
//        }
//        else {
//            //startEntireProcess()
//            bleMan?.operationStart()
//            //sensorErrorSkip = true
//        }
    }

    private func disableThisView() {
        self.hidden = true
        uiTimer.invalidate()
    }



    func goToNextViewCallBack() {
        //log.printThisFNC("goToNextViewCallBack", comment: "  \(curStateNum) ", lnum: 10)
        if isAdult { adultNextSubView() }
        else { infantNextSubView() }
        curStateNum += 1
    }

    func adultNextSubView() {
        print("adultNextSubView  >>  \(curStateNum) ")
        switch curStateNum {
        case 0: initUIMembers()
        case 1: operationView()
        case 2: aedSkillView() // stepCheckView()
        case 3: adultFinal()
        default:
            //labelStudent1.hideMe()            labelStudent2.hideMe()
            bttnCloseResult.hideMe()
            stepCheckView()
        }
    }

    func infantNextSubView() {
        print("infantNextSubView  >  ")
        switch curStateNum {
        case 0: initUIMembers()
        case 1: conductCprView()
        case 2: infant2Bls()
        default:
            //labelStudent1.hideMe()            labelStudent2.hideMe()
            bttnCloseResult.hideMe()
            stepCheckView()

            let ifan = bleMan!.testObj as! HmTestInft
            print("\(ifan.infantJson())")

        }
    }

    // MARK:  Infant 관련...
    func conductCprView() {
        log.printThisFNC("conductCprView", comment: "  ")

        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "SubConductCpr", bundle: bundle)
        let newView = nib.instantiateWithOwner(self, options: nil).last as? SubConductCpr
        newView?.layer.cornerRadius = 15
        newView?.frame = CGRect(x: 0, y: 50, width: 300, height: 290)
        newView?.initSet(bleMan!)
        newView?.uiCallBack = goToNextViewCallBack
        addTestSubView(newView!)
    }

    func infant2Bls() {
        log.printThisFNC("infant2Bls", comment: "  ")

        labelTitle.text = bleMan?.name

        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "SubInfant2Bls", bundle: bundle)
        let newView = nib.instantiateWithOwner(self, options: nil).last as? SubInfant2Bls
        newView?.layer.cornerRadius = 15
        newView?.frame = CGRect(x: 0, y: 50, width: 300, height: 290)
        // 높이 : 340 - 50 = 290, 폭은 동일 : 300
        newView?.initSet(bleMan!)
        newView?.uiCallBack = goToNextViewCallBack
        addTestSubView(newView!)
    }


    // MARK:  최종 판정 화면..
    func stepCheckView() {
        log.printThisFNC("stepCheckView", comment: "  ")

        showDataSaveButtonView() // 최종 화면을 부르면서 콜백..
        labelTitle.text = bleMan?.name
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "SubStepCheckView", bundle: bundle)
        stepView = nib.instantiateWithOwner(self, options: nil).last as? SubStepCheckView
        stepView?.layer.cornerRadius = 15
        stepView?.frame = CGRect(x: 0, y: 50, width: 300, height: 290)
        stepView!.arrUiObj = arrUiObj

        // 높이 : 340 - 50 = 290, 폭은 동일 : 300
        stepView?.initSet(bleMan!)
        stepView?.uiCallBack = goToNextViewCallBack

        //stepView?.labelR1.text = labelStudent1.text
        //stepView?.labelR2.text = labelStudent2.text

        
        stepView?.cornerRad(15)
        addTestSubView(stepView!)

        if isAdult { // bttnStep4 때문에 살짝 옆으로...
            //bringSubviewToFront(stepView!.bttnStep4)
            bttnClose!.frame.origin = CGPoint(x: 100, y: 120)
            bttnClose!.frame.size = CGSize(width: 200, height: 150)
            //CGRect(x: )  frame.size.offset(-100, hei: -150) 이렇게 하면 확대뷰에서는 크게 생성됨.
        }

        // 디버깅 용 외곽선으로 크기 보게 ...  >>>>>>>>>  확대 버튼 보이기 ....
        //bttnClose!.setBorder(2, rad: 1, borderCol: colorBarBgRed, bgCol: colHalfTransGray)


        bttnCloseResult.setImage(UIImage(named: "btn_detail"), forState: .Normal)

        btnXCloseResultAction = {
            print("  show result   ")
            self.bleCallBack(self.bleMan!)
            // show result ...
        }
    }

    // MARK:  성인 뷰  ....
    func aedSkillView() {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "SubAfdView", bundle: bundle)

        afdView = nib.instantiateWithOwner(self, options: nil).last as? SubAfdView
        afdView?.layer.cornerRadius = 15
        afdView?.frame = CGRect(x: 0, y: 50, width: 300, height: 290)
        // 높이 : 340 - 50 = 290, 폭은 동일 : 300
        afdView?.initSet(bleMan!)
        afdView?.uiCallBack = goToNextViewCallBack
        afdView?.cornerRad(15)
        addTestSubView(afdView!)

        log.logThis(" add afdView 완료 ")
        log.endFunctionLog()
    }

    func adultFinal() {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "SubAdultFinal", bundle: bundle)
        let fvw = nib.instantiateWithOwner(self, options: nil).last as? SubAdultFinal
        fvw?.layer.cornerRadius = 15
        fvw?.frame = CGRect(x: 0, y: 50, width: 300, height: 290)
        // 높이 : 340 - 50 = 290, 폭은 동일 : 300
        fvw?.initSet(bleMan!)
        fvw?.uiCallBack = goToNextViewCallBack
        fvw?.cornerRad(15)
        addTestSubView(fvw!)
        log.logThis(" final view 완료 ")
        log.endFunctionLog()
    }


    func operationView() {
        log.printThisFNC("operationView", comment: "  실행 뷰 차례.. ")
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "OperatingView", bundle: bundle)

        //subView = nib.instantiateWithOwner(self, options: nil).last as? OperatingView
        let opsubView = nib.instantiateWithOwner(self, options: nil).last as? OperatingView
        opsubView?.layer.cornerRadius = 15
        opsubView?.frame = CGRect(x: 0, y: 50, width: 300, height: 290)
        // 높이 : 340 - 50 = 290, 폭은 동일 : 300
        opsubView?.uiCallBack = goToNextViewCallBack
        opsubView?.bleMan = bleMan
        operView = opsubView
        opsubView?.initSet(adultObj, infant: infantObj)
        opsubView?.cornerRad(15)
        addTestSubView(opsubView!)
    }

    func initUIMembers() {
        log.printThisFNC("initUIMembers", comment: "  isAdult : \(isAdult)  연결 조건 : \(!(bleMan?.isConnectionTotallyFinished)!) ")

        if isAdult {
            if !(bleMan?.isConnectionTotallyFinished)! {
                self.hideMe(); return
            }
        } else {
            if bleMan?.curStudent == nil && bleMan?.curStdntSub == nil { // 학생 세팅이 둘다 없다.
                self.hideMe(); return
            }
        }

        // 여기서 객체 생성..
        if isAdult {
            uiTimer = NSTimer.scheduledTimerWithTimeInterval(0.1, // second
                target:self, selector: #selector(OperationUnitView.update), userInfo: nil, repeats: true)
            print(" HmTestAdult 객체 생성  ")
            bleMan?.testObj = HmTestAdlt()
            bleMan?.testOperationStart() // 여기서 오퍼레이션을 시작한다..
        } else {
            print(" HmTestInfant 객체 생성  ")
            bleMan?.testObj = HmTestInft()
        }

        labelTitle.text = bleMan?.name
        if bleMan?.curStudent == nil {  labelStudent1.text = "-"
        } else { labelStudent1.text = bleMan?.curStudent?.name        }
        if bleMan?.curStdntSub == nil { labelStudent2.text = "-"
        } else { labelStudent2.text = bleMan?.curStdntSub?.name       }

        btnXCloseResultAction = {
            self.bleMan?.reset()
            self.hideMe()
        }

        // Sub View Add ...
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "SubBlsView", bundle: bundle)
        blsView = nib.instantiateWithOwner(self, options: nil).last as? SubBlsView
        blsView?.frame = CGRect(x: 0, y: 50, width: 300, height: 290)
        blsView?.initSet(bleMan!)
        blsView?.uiCallBack = goToNextViewCallBack
        blsView?.cornerRad(15)
        addTestSubView(blsView!)

        log.logThis(" add blsView 완료 ")
        log.endFunctionLog()
    }


}
