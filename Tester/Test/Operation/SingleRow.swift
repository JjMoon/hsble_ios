//
//  ResultSingleRow.swift
//  Test
//
//  Created by Jongwoo Moon on 2016. 2. 22..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation

class SingleRow: HtView {
    @IBOutlet weak var labelMain: UILabel!
    @IBOutlet weak var imgA: UIImageView!
    @IBOutlet weak var imgB: UIImageView!
    @IBOutlet weak var labelA: UILabel!
    @IBOutlet weak var labelB: UILabel!

    override func initSet() {
        super.initSet()
        //layer.cornerRadius = 5
    }

    override func awakeFromNib() {
        //print(" SingleRow :: func awakeFromNib    cornerRadius = 5  ")
        //layer.cornerRadius = 5
    }

    func isLabelCase(isLabel: Bool) {
        if isLabel {    imgA.hideMe(); imgB.hideMe()
        } else {        labelA.hideMe(); labelB.hideMe()
        }
    }

    func singleRow() {
        imgA.hideMe(); labelA.hideMe()
    }

    func titleOnly() {
        singleRow()
        imgB.hideMe(); labelB.hideMe()
    }

    func setLabelRed() {
        labelA.textColor = colorBarBgRed
        labelB.textColor = colorBarBgRed
    }


    func setImageX() {
        //print("setImageX()    constaint >>> ")
        imgA.image = UIImage(named: "btn_mr_delete")
        imgB.image = UIImage(named: "btn_mr_delete")

        NSLayoutConstraint(item: imgA, attribute: .Height, relatedBy: .Equal,
            toItem: imgA, attribute: .Width, multiplier: 1.0, constant: 0).active = true

        imgB.frame.size.width = 30
        imgB.frame.size.height = 30

    }

    func setCheckImgA(chk: Bool) {
        if chk {
            imgA.image = UIImage(named: "img_bls_check")
        } else {
            imgA.image = UIImage(named: "img_bls_uncheck")
        }
    }
    func setCheckImgB(chk: Bool) {
        if chk {
            imgB.image = UIImage(named: "img_bls_check")
        } else {
            imgB.image = UIImage(named: "img_bls_uncheck")
        }
    }

}
