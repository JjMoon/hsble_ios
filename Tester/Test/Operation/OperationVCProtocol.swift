//
//  OperationVCProtocol.swift
//  HS Test
//
//  Created by Jongwoo Moon on 2016. 8. 9..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation



// 처음 뷰 생성시 enlargeButton 을 붙인다..  거기를 눌르면 콜백을 계속 받자..
extension OperationVC: PrtclEnlargeView {

    func enlargeView(unitVw: OperationUnitView) {
        targetView = unitVw
        tarPoint = screenCenter
        // baseOrigin 은 노터치.

        print(" screen center : \(screenCenter) ")

        var selfAsEnlargeView : PrtclEnlargeView = self
        selfAsEnlargeView.animation()
    }

    func enlargeCallback(unitVw: OperationUnitView) {
        targetView = unitVw
        tarPoint = screenCenter

        var selfAsEnlargeView : PrtclEnlargeView = self
        selfAsEnlargeView.animation()
    }
    
    
}