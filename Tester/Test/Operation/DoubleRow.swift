//
//  ResultSingleRow.swift
//  Test
//
//  Created by Jongwoo Moon on 2016. 2. 22..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation

class DoubleRow: HtView {

    @IBOutlet weak var imgChk1: UIImageView!
    @IBOutlet weak var imgChk2: UIImageView!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!

    override func initSet() {
        super.initSet()
        //layer.cornerRadius = 5
    }

    override func awakeFromNib() {
        //print(" SingleRow :: func awakeFromNib    cornerRadius = 5  ")
        //layer.cornerRadius = 5
    }
//
//    func isLabelCase(isLabel: Bool) {
//        if isLabel {    imgA.hideMe(); imgB.hideMe()
//        } else {        labelA.hideMe(); labelB.hideMe()
//        }
//    }
//
    func setProps(lbl1: String, check1: Bool, lbl2: String?, check2: Bool?) {

        label1.text = lbl1
        setCheckImg(true, chk: check1)
        if lbl2 == nil {
            hideEvery(label2, imgChk2)
            return
        }
        showEvery(label2, imgChk2)
        label2.text = lbl2!
        setCheckImg(false, chk: check2!)
    }

//
//    func titleOnly() {
//        singleRow()
//        imgB.hideMe(); labelB.hideMe()
//    }
//
//    func setLabelRed() {
//        labelA.textColor = colorBarBgRed
//        labelB.textColor = colorBarBgRed
//    }
//

    func setImageX() {
        //print("setImageX()    constaint >>> ")
        imgChk1.image = UIImage(named: "btn_mr_delete")
        imgChk2.image = UIImage(named: "btn_mr_delete")

//        NSLayoutConstraint(item: imgA, attribute: .Height, relatedBy: .Equal,
//                           toItem: imgA, attribute: .Width, multiplier: 1.0, constant: 0).active = true

//        imgB.frame.size.width = 30
//        imgB.frame.size.height = 30

    }

    func setCheckImg(isFirst: Bool, chk: Bool) {
        let imgVw = isFirst ? imgChk1 : imgChk2
        if chk {
            imgVw.image = UIImage(named: "img_bls_check")
        } else {
            imgVw.image = UIImage(named: "img_bls_uncheck")
        }
    }
}
