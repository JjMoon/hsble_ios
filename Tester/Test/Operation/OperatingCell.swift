//
//  OperatingCell.swift
//  Test
//
//  Created by Jongwoo Moon on 2016. 2. 17..
//  Copyright © 2016년 IMLab. All rights reserved.
//




import Foundation
import UIKit

class OperatingCell : UITableViewCell {

    // var uiTimer = NSTimer()

    @IBOutlet weak var lblSetNum: UILabel!
    @IBOutlet weak var lblCpCount: UILabel!
    @IBOutlet weak var lblCpTime: UILabel!
    @IBOutlet weak var lblRpCount: UILabel!
    @IBOutlet weak var lblRpTime: UILabel!

    var setNum: Int = 0
    var dataObj: HsData = HsData()
    var mySet: HmUnitSet?

    //var dObj = HsBleManager.inst().dataObj

    func initialCellSetting(setN: Int) {
        print("initialCellSetting(setN: Int) \(setN)")
        lblSetNum.text = String(setN + 1)
        //uiTimer = NSTimer.scheduledTimerWithTimeInterval(0.2, // second
          //  target:self, selector: Selector("update"), userInfo: nil, repeats: true)
    }

    func resetText() {
        lblCpCount.text = "00/00"
        lblRpCount.text = "00/00"
        lblCpTime.text = "0s"
        lblRpTime.text = "0s"
        lblCpCount.textColor = UIColor.blackColor()
        lblRpCount.textColor = UIColor.blackColor()
        lblCpTime.textColor = UIColor.blackColor()
        lblRpTime.textColor = UIColor.blackColor()
    }

    func update() {

    }

    func setItems(setNumber: Int, dObj: HsData) {
//        setNum = setNumber
//        dataObj = dObj
//        let idx = setNum - 1
//        let stgObj = dataObj.arrSet[idx]
//
//        switch langIdx {
//        case 0: // en
//            lblSetNum.text = "\(local Str("cycle")) \(setNum)"
//        default: // 1: ko
//            lblSetNum.text = "\(setNum) \(loc alStr("cycle"))"
        //}
//        lblSetNum.alignCenter()
//        lblCount.text = "\(stgObj.ccPassCnt) / \(stgObj.ccCount) "
//        if 20 < stgObj.ccPassCnt { lblCpCount.textColor = HmGraphSetting.inst.graphGreen }
//        else { lblCount.textColor = HmGraphSetting.inst.warningRed }
//
//        let pushpermin = stgObj.분당압박횟수()
//        lblTime.text = String(pushpermin)
//        if 100 <= pushpermin && pushpermin <= 120 { lblTime.textColor = HmGraphSetting.inst.graphGreen }
//        else { lblTime.textColor = HmGraphSetting.inst.warningRed }
//
//        lblRpCount.text = "\(stgObj.rpPassCnt) / 2 "
//        if 0 < stgObj.rpPassCnt { lblRpCount.textColor = HmGraphSetting.inst.graphGreen }
//        else { lblRpCount.textColor = HmGraphSetting.inst.warningRed }
//
//        lblRpTime.text = String(Int(stgObj.rpTime))
//        if stgObj.rpTime < 10 { lblRpTime.textColor = HmGraphSetting.inst.graphGreen }
//        else { lblRpTime.textColor = HmGraphSetting.inst.warningRed }

    }
}

