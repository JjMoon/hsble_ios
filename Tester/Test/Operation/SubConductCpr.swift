//
//  SubConductCpr.swift
//  Test
//
//  Created by Jongwoo Moon on 2016. 2. 18..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation

class SubConductCpr: HtView {
    var checkMan = CheckButtonManager()

    var bleMan: HsBleSuMan?

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelCheckAll: UILabel!
    @IBOutlet weak var btnCheckAll: UIButton!

    @IBAction func btnActCheck(sender: UIButton) { // 3개 버튼의 액션
        checkMan.toggleCheck(sender)
    }

    @IBAction func btnActCheckAll(sender: UIButton) { // 모두 선택
        checkMan.toggleCheckAll()
    }

    @IBAction func btnActNext(sender: AnyObject) {
        let arrbtn = checkMan.arrBttn, infant = bleMan!.testObj as! HmTestInft  // 모두 제1 구조자 역할임..

        // "압박1" "호흡1" "압박2" "호흡2" "중단시간" "세트3" "세트4"
        infant.setStepRestProps([ "압박1" : arrbtn[0].tag, "호흡1" : arrbtn[1].tag,
            "압박2" : arrbtn[2].tag, "호흡2" : arrbtn[3].tag,
            "중단시간" : arrbtn[4].tag ])

        uiCallBack()
    }

//    func initSet(bleObj: HsBleSuMan) {
//        super.initSet()
//        bleMan = bleObj
//        checkMan.arrBttn = [ btnFinger, btnDepth, btnRecoil, btnMinimize ]
//        checkMan.allBttn = btnCheckAll; checkMan.labelCheckAll = labelCheckAll
//    }


    func initSet(bleObj: HsBleSuMan) {
        super.initSet()
        bleMan = bleObj
        //checkMan.arrBttn = [ btnResponse, btnEmergency, btnCheckPulse ]
        checkMan.allBttn = btnCheckAll; checkMan.labelCheckAll = labelCheckAll

        checkMan.arrBttn.removeAll()

        var yPosi : CGFloat = 40
        let yDist : CGFloat = 35
        let arrLblString = [ langStr.obj.bls_infant_2_check_1, langStr.obj.bls_infant_2_check_2,
                         langStr.obj.bls_infant_2_check_3, langStr.obj.bls_infant_2_check_4,
                         langStr.obj.bls_infant_2_check_5 ]

        for str in arrLblString {
            // view 추가
            let nVw = HsCheckView(posi: CGPoint(x: 15, y: yPosi), labelText: str)
            checkMan.arrBttn.append(nVw.bttn!)
            nVw.bttn!.addTarget(self, action: #selector(btnActCheck), forControlEvents: .TouchUpInside)
            addSubview(nVw)
            yPosi += yDist
        }

        cornerRad(15)
        print("  set Infant conduct CPR  ... ")
    }

    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        let bezPath = UIBezierPath()
        let yCo : CGFloat = 230
        bezPath.moveToPoint(CGPoint(x: 20, y: yCo))
        bezPath.addLineToPoint(CGPoint(x: 250, y: yCo))
        bezPath.lineWidth = 0.4
        colorBttnDarkGray.setStroke()
        bezPath.lineCapStyle = .Round
        bezPath.stroke()
    }

    // MARK:  언어 세팅.
    override func setLanguageString() {
        labelTitle.text =     langStr.obj.bls_infant_2_title
        labelCheckAll.text =  langStr.obj.all_check
//        labelFinger.text =    langStr.obj.bls_infant_2_check_1
//        labelCompRate.text =  langStr.obj.bls_infant_2_check_2
//        labelCompDepth.text = langStr.obj.bls_infant_2_check_3
//        labelRecoil.text =    langStr.obj.bls_infant_2_check_4
//        labelMinimize.text =  langStr.obj.bls_infant_2_check_5
    }

}
