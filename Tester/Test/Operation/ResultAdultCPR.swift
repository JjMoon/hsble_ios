//
//  ResultAdultCPR.swift
//  Test
//
//  Created by Jongwoo Moon on 2016. 2. 22..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation


class ResultAdultCPR: HtView {
    //@IBOutlet weak var lblNumber: UILabel!

    @IBOutlet weak var label0: UILabel!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!
    @IBOutlet weak var label4: UILabel!

    @IBOutlet weak var rsltSetNum: ResultCPR!
    @IBOutlet weak var rsltTime: ResultCPR!
    @IBOutlet weak var rsltCC: ResultCPR!

    @IBOutlet weak var rsltRpTime: ResultCPR!
    @IBOutlet weak var rsltRP: ResultCPR!

    var adt1: HmTestAdlt?, dObj: HsData?

    func initSet(adult1: HmTestAdlt, dataObj: HsData) {
        super.initSet()
        adt1 = adult1; dObj = dataObj
        setTableValues()
        setValuesOfEachSet()
    }

    func setTableValues() {
        var yco: CGFloat = 0
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "ResultCPR", bundle: bundle)
        var v1 = nib.instantiateWithOwner(self, options: nil).last as? ResultCPR
        v1?.setLabelArray()
        let xCo: CGFloat = 380, wid: CGFloat = 260, yDist: CGFloat = 40
        v1?.frame = CGRect(x: xCo, y: yco, width: wid, height: 40); yco += yDist
        rsltSetNum = v1!
        addSubview(v1!)

        v1 = nib.instantiateWithOwner(self, options: nil).last as? ResultCPR
        v1?.setLabelArray();
        v1?.frame = CGRect(x: xCo, y: yco, width: wid, height: 40); yco += yDist
        rsltTime = v1!
        addSubview(v1!)

        v1 = nib.instantiateWithOwner(self, options: nil).last as? ResultCPR
        v1?.setLabelArray();
        rsltCC = v1!
        v1?.frame = CGRect(x: xCo, y: yco, width: wid, height: 40); yco += yDist
        addSubview(v1!);

        v1 = nib.instantiateWithOwner(self, options: nil).last as? ResultCPR
        v1?.setLabelArray()
        rsltRpTime = v1!
        v1?.frame = CGRect(x: xCo, y: yco, width: wid, height: 40); yco += yDist
        addSubview(v1!);

        v1 = nib.instantiateWithOwner(self, options: nil).last as? ResultCPR
        v1?.setLabelArray();
        rsltRP = v1!
        v1?.frame = CGRect(x: xCo, y: yco, width: wid, height: 40); yco += yDist
        addSubview(v1!);
    }

    func setValuesOfEachSet() {
        print("   number of ...   >>  \(rsltTime.arrLabel.count)  \(dObj!.arrSet.count)  ")
        for (idx, aSt) in dObj!.arrSet.enumerate() {

            print("   number of ...   >>   SET : \(idx + 1) ")

            rsltSetNum.arrLabel[idx].text = "\(idx + 1)"
            rsltTime.arrLabel[idx].text = aSt.ccTimeSec
            rsltTime.arrLabel[idx].textColor = colorGreenRedBy(aSt.ccTimePass)
                //setColorAccord(aSt.ccTimePass, tColor: colorBarBgGrn, fColor: colorBarBgRed)
            rsltRpTime.arrLabel[idx].text = aSt.rpTimeSec
            rsltRpTime.arrLabel[idx].textColor = colorGreenRedBy(aSt.rpTimePass)
            rsltCC.arrLabel[idx].text = "\(aSt.ccPassCnt) / \(aSt.ccCount)"
            rsltCC.arrLabel[idx].textColor = colorGreenRedBy(aSt.ccCntPass)
            rsltRP.arrLabel[idx].text = "\(aSt.rpPassCnt) / \(aSt.rpCount)"
            rsltRP.arrLabel[idx].textColor = colorGreenRedBy(aSt.rpCntPass)
        }
    }

    override func setLanguageString() {
        label0.text = langStr.obj.bls_adult_sheet_check_4
        label1.text = langStr.obj.bls_adult_sheet_check_4_1
        label2.text = langStr.obj.bls_adult_sheet_check_4_2
        label3.text = langStr.obj.bls_adult_sheet_check_4_3
        label4.text = langStr.obj.bls_adult_sheet_check_4_4
    }

    override func drawRect(rect: CGRect) {
        super.drawRect(rect)

        let subLineNum = 4
        let unitY: CGFloat = 40

        let bezPath = UIBezierPath(), yV: CGFloat = 40,
        stX: CGFloat = 30, edX: CGFloat = 640  // frame.size.width - 10
        bezPath.moveToPoint   (CGPoint(x: stX, y: yV))
        bezPath.addLineToPoint(CGPoint(x: edX, y: yV))
        bezPath.lineWidth = 2
        colorBttnDarkGray.setStroke()
        bezPath.lineCapStyle = .Round
        bezPath.stroke()

        var curY = yV + unitY
        for _ in 1...subLineNum - 1 {
            let bez = UIBezierPath()
            bez.moveToPoint     (CGPoint(x: stX, y: curY))
            bez.addLineToPoint  (CGPoint(x: edX, y: curY))
            bez.lineWidth = 1
            colorBttnDarkGray.setStroke()
            bez.lineCapStyle = .Round;   bez.stroke()
            curY += unitY
        }
        layer.cornerRadius = 5
    }
}

class ResultCPR: UIView {
    @IBOutlet weak var labelSet1: UILabel!
    @IBOutlet weak var labelSet2: UILabel!
    @IBOutlet weak var labelSet3: UILabel!
    @IBOutlet weak var labelSet4: UILabel!
    @IBOutlet weak var labelSet5: UILabel!

    var arrLabel = [UILabel]()

    func setLabelArray() {
        arrLabel = [ labelSet1, labelSet2, labelSet3, labelSet4, labelSet5 ]
    }

    func setCC() {

    }

    func setRP() {

    }

    func setCompTime() {

    }
    
    func setRpTime() {
        
    }
    
    
}
