//
//  ResultVC.swift
//  Test
//
//  Created by Jongwoo Moon on 2016. 2. 19..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation

class ResultVC: HtViewController, ResultPrtcBls {
    var log = HtLog(cName: "ResultVC")

    // ResultPrtcBls 관련
    var container: HtTableView?
    var titleNum: Int = 0

    var bleMan: HsBleSuMan?, testObj: HmTest?
    var isTesting = true // or data manage ...
    let nickList = [ "A", "B", "C", "D" ]

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var btnGraph: UIButton!
    @IBOutlet weak var btnResult: UIButton!

    ///  A B C D   버튼 관련..  테스트는 위에서 넣어주자..
    var abcdObj = [HsBleSuMan?]()

    @IBOutlet var bttnABCD: [UIButton]!
    @IBOutlet weak var bttnA: UIButton!
    @IBOutlet weak var bttnB: UIButton!
    @IBOutlet weak var bttnC: UIButton!
    @IBOutlet weak var bttnD: UIButton!

    @IBAction func btnActSelectAtoD(sender: UIButton) {
        for btn in bttnABCD {
            btn.setTitleColor(colBttnDarkGray, forState: .Normal)
            btn.setBorder(0, rad: 1, borderCol: colBttnGreen, bgCol: nil)
        }
        sender.setTitleColor(colBttnGreen, forState: .Normal)
        sender.setBorder(1, rad: 1, borderCol: colBttnGreen, bgCol: nil)

        bleMan = HsBleMaestr.inst.getBleWithNick(sender.titleLabel!.text!)
        testObj = bleMan?.testObj

        print("  switched bleMan>>>  >>   \(bleMan)   \(sender.titleLabel!.text!)")
        print("  switched bleMan>>>  >>   \(bleMan?.name)   \(bleMan?.nick) ")

        reloadView()
        //tableContent.reloadData()
    }

    private func reloadView() {
        //theView?.removeFromSuperview()
        if isAdult {
            addAdultResultView()
        } else {
            addInfantResultView()
        }
    }

    func setInitABCDBttns() {
        let btnArr = [ bttnA, bttnB, bttnC, bttnD ]
        for (ix, ble) in abcdObj.enumerate() {
            if ble == nil {
                btnArr[ix].hideMe()
            }
        }
        //print("  my Nick : \(bleMan!.nick)")
        for (ix, nk) in nickList.enumerate() {
            if nk == bleMan!.nick { // 내 버튼은 선택된 걸로 놔야지...
                btnActSelectAtoD(btnArr[ix])
            }
        }
    }

    @IBAction func btnActGraph(sender: AnyObject) {
        if !isAdult { return }

        let vc = ResultGraphVC(nibName: "ResultGraph", bundle: nil)
        //vc.dbStudent = bleMan!.curStudent
        vc.bleMan = bleMan!; vc.dbStudent = nil
        presentViewController(vc, animated: viewAnimate, completion: nil)
    }

    @IBAction func btnActResult(sender: AnyObject) {
        closeMyself()
        //showGeneralPopup(langStr.obj.cancel, message: langStr.obj.want_back, 
        //  yesStr: langStr.obj.yes, noString: langStr.obj.no, yesAction: {  self.closeMyself() })
    }

    //////////////////////////////////////////////////////////////////////     [ UI   ViewDidLoad   << >> ]
    override func viewDidLoad() {
        log.printAlways("viewDidLoad", lnum: 5)
        setLanguageString()
        //let resultV = InfantResultView()
        self.view.backgroundColor = colorGreyBright

        let tableRect = CGRect(x: (768 - 668) / 2, y: 90, width: 668, height: 850)
        container = HtTableView(frame: tableRect)
        //container!.offset = 3
        container!.initSet(tableRect, scroll: true)
        container!.backgroundColor = UIColor.clearColor()
        container!.tableView.backgroundColor = UIColor.clearColor()

        if isTesting { // 테스팅 후 바로 보여질 때
            // btnGraph.hideMe()
        } else { // DB 자료를 보여줄 때
            //btnResult.hideMe()
        }
    }

    override func viewWillAppear(animated: Bool) {

    }

    override func viewDidAppear(animated: Bool) {
        if isAdult {
            btnGraph.showMe()
            addAdultResultView()
        } else {
            btnGraph.hideMe()
            addInfantResultView()
        }

        if bleMan != nil && HsBleMaestr.inst.tempBle != nil  {
            bleMan = HsBleMaestr.inst.tempBle
            reloadView()
        }
        setInitABCDBttns()

        if isAdult {
            super.viewDidAppear(animated)  // 1.1 sec timer set..
        }
    }

    override func update() {
        let btnArr = [ bttnA, bttnB, bttnC, bttnD ]
        
        for (ix, ble) in HsBleMaestr.inst.arrBleSuMan.enumerate() {
            if !ble.isConnectionTotallyFinished {
                btnArr[ix].hideMe()
                if ble.nick == bleMan!.nick {
                    closeMyself()
                }
            }
        }

        let connNum = abcdObj.filter { (ble) -> Bool in
            return ble != nil && ble!.isConnectionTotallyFinished
        }

        if connNum.count == 0 {
            //showSimpleMessageWith(langStr.obj.connect_fail) { () -> Void in
            closeMyself()
        }
    }

    //////////////////////////////////////////////////////////////////////     [ <<  단위 레포트 추가 >> ]
    func addInfantResultView() {
        print("addInfantResultView")

        container!.data = [
            HtTableSection(title: langStr.obj.bls_infant_sheet_title_1,
                headerHeight: 60, arrRow: [UIView]()),
            HtTableSection(title: "Infant CPR Stpe 1 and 2", //   langStr.obj.bls_infant_sheet_title_2,
                headerHeight: 60, arrRow: [UIView]()),
            HtTableSection(title: "Infant CPR Stpe 3 and 4", //   langStr.obj.bls_infant_sheet_title_2,
                headerHeight: 60, arrRow: [UIView]())
        ]
        
        titleNum = 1

        insertBlsSteps(testObj!, idx: 0)
        insertInfantStep2(testObj! as! HmTestInft, idx: 1)
        container!.tableView.reloadData()
        view.addSubview(container!)
    }

    func addAdultResultView() {
        print(" addAdultResultView  ")

        container!.data = [
            HtTableSection(title: langStr.obj.bls_adult_sheet_title_1,  // BLS
                headerHeight: 60, arrRow: [UIView]()),
            HtTableSection(title: langStr.obj.bls_adult_sheet_title_2,  // 실습 화면
                headerHeight: 60, arrRow: [UIView]()),
            HtTableSection(title: langStr.obj.bls_adult_sheet_title_3, // AED
                headerHeight: 60, arrRow: [UIView]()),
            HtTableSection(title: langStr.obj.bls_adult_sheet_title_4, // 압박 재개
                headerHeight: 60, arrRow: [UIView]())
        ]

        titleNum = 1
        let adlt = testObj as! HmTestAdlt
        insertBlsSteps(testObj!, idx: 0)
        insertAdultConductCPR(adlt, dataObj: bleMan!.calcManager.data, idx: 1)
        insertAdultAED(adlt, idx: 2)
//        insertAdultAHA(adlt, idx: 1)
//        insertAdultAfterShock(adlt, resc2: adlt, idx: 2)

        container!.tableView.reloadData()
        view.addSubview(container!)
    }


    func closeMyself() {
        dismissViewControllerAnimated(viewAnimate, completion: nil)
    }

    // MARK:  언어 세팅.
    func setLanguageString() {
//        if isAdult {
//            labelTitle.text = langStr.obj.adult_title
//        } else {
//            UIView.animateWithDuration(0.1, delay: 0, usingSpringWithDamping: 10, initialSpringVelocity: 3, options: [   ],
//                animations: {
//                    [unowned self] in self.btnResult!.transform = CGAffineTransformMakeTranslation(120, 0)
//                }) { [] (finished: Bool) in }
//
//            labelTitle.text = langStr.obj.infant_title
//        }
        //btnResult.setTitle(langStr.obj.manage_data, forState: .Normal)
        //btnResult.setTitle(langStr.obj.result, forState: .Normal)
        btnGraph.setTitle(langStr.obj.graph, forState: .Normal)
    }


}
