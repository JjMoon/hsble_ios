//
//  ResultSingleRow.swift
//  Test
//
//  Created by Jongwoo Moon on 2016. 2. 22..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation

class ResultSingleRow: HtView {
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelMain: UILabel!
    @IBOutlet weak var imgA: UIImageView!
    @IBOutlet weak var imgB: UIImageView!
    @IBOutlet weak var labelA: UILabel!
    @IBOutlet weak var labelB: UILabel!

    override func initSet() {
        super.initSet()
        //layer.cornerRadius = 5
    }

    func isLabelCase(isLabel: Bool) {
        if isLabel {    imgA.hideMe(); imgB.hideMe()
        } else {        labelA.hideMe(); labelB.hideMe()
        }
    }

    func singleRow() {
        imgA.hideMe(); labelA.hideMe()
    }

    func setTitleMain(ttl: String, main: String) {
        labelTitle.text = ttl
        labelMain.text = main
    }

    func setCheckImgA(chk: Bool) {
        if chk {
            imgA.image = UIImage(named: "img_bls_check")
        } else {
            imgA.image = UIImage(named: "img_bls_uncheck")
        }
    }

    func setCheckImgB(chk: Bool) {
        if chk {
            imgB.image = UIImage(named: "img_bls_check")
        } else {
            imgB.image = UIImage(named: "img_bls_uncheck")
        }
    }



}
