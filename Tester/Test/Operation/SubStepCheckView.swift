//
//  SubBlsView.swift
//  Test
//
//  Created by Jongwoo Moon on 2016. 2. 16..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation

class SubStepCheckView : HtView {
    var checkMan = CheckButtonManager()

    var arrStepLbl = [UILabel]()
    var bleMan: HsBleSuMan?

    var arrUiObj = HsUIArray()

    var viewDiscrim: DiscriminantView?
    @IBOutlet weak var bttnViewDetail: UIButton!

    @IBOutlet weak var btnCPR: UIButton!

    @IBOutlet weak var stp1: UILabel!
    @IBOutlet weak var stp2: UILabel!
    @IBOutlet weak var stp3: UILabel!
    @IBOutlet weak var stp4: UILabel!

    @IBOutlet weak var labelNotice: UILabel!

    @IBOutlet weak var btnPass: UIButton!
    @IBOutlet weak var btnFail: UIButton!

    @IBAction func bttnActDetailClose(sender: AnyObject) {
        print(" bttnActDetailClose  Close ... >>>  ")
        viewDiscrim?.hideMe()
        sender.hideMe()
        showEvery(btnPass, btnFail)
    }

    @IBAction func btnActAdultCPR(sender: AnyObject) {
        addDiscriminantView()
    }

    @IBAction func btnActPass(sender: UIButton) {
        bttnInitSet()
        btnPass.setBorder(2, rad: 5, borderCol: colBttnGreen, bgCol: colBttnGreen)
        btnPass.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        bleMan!.curStudent?.evalFinished = true
        bleMan!.testObj?.pass = true
    }

    @IBAction func btnActFail(sender: UIButton) {
        bttnInitSet()
        btnFail.setBorder(2, rad: 5, borderCol: colBttnRed, bgCol: colBttnRed)
        btnFail.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        bleMan!.curStudent?.evalFinished = true
        bleMan!.testObj?.pass = false
    }

    private func bttnInitSet() {
        btnPass.setBorder(2, rad: 5, borderCol: colBttnGreen, bgCol: UIColor.whiteColor())
        btnFail.setBorder(2, rad: 5, borderCol: colBttnRed, bgCol: UIColor.whiteColor())
        btnPass.setTitleColor(colBttnGreen, forState: .Normal)
        btnFail.setTitleColor(colBttnRed, forState: .Normal)
    }

    func initSet(bleObj: HsBleSuMan) {
        super.initSet()
        self.cornerRad(15)
        bleMan = bleObj
        bttnInitSet()

        bleObj.testObj!.step1Color(stp1)

        if isAdult {
            setAdultProp()
            addDiscriminantView()
        } else {
            setInfantProp()
        }
    }

    func addDiscriminantView() {
        if viewDiscrim == nil {
            let bundle = NSBundle(forClass: self.dynamicType)
            let nib = UINib(nibName: "DiscriminantView", bundle: bundle)

            viewDiscrim = nib.instantiateWithOwner(self, options: nil).last as? DiscriminantView

            if UI_USER_INTERFACE_IDIOM() == .Pad {
                viewDiscrim?.frame = CGRect(x: 0, y: 0, width: 300, height: 205)
            } else {
                viewDiscrim?.frame = CGRect(x: 0, y: 0, width: 180, height: 105)
            }
            addSubview(viewDiscrim!)

            viewDiscrim!.dataObj = bleMan?.dataObj

            bttnViewDetail.setTitle("\(langStr.obj.close) >>", forState: .Normal)
            bttnViewDetail.setTitleColor(viewDiscrim?.setDiscreminant(), forState: .Normal)
        }
        hideEvery(btnPass, btnFail)
        showEvery(bttnViewDetail, viewDiscrim!)
        bringSubviewToFront(bttnViewDetail)
        bringSubviewToFront(btnCPR)
    }

    private func setInfantProp() {
        let testInfant = bleMan?.testObj as! HmTestInft
        showEvery(btnPass, btnFail)
        hideEvery(bttnViewDetail, stp3, stp4)
        btnCPR.deactivate()
        // step 2 모양 조절
        stp2.setBorder(0, rad: 4, borderCol: UIColor.whiteColor(), bgCol: UIColor.whiteColor())
        //stp2.textColor = colBttnGreen
        testInfant.stepRestColor(stp2)

        viewDiscrim?.hideMe()

        if cprProtoN.theVal != 0 { setEuNzLabels() }
    }

    private func setAdultProp() {
        let adlt = bleMan?.testObj as! HmTestAdlt
        hideEvery(btnPass, btnFail)
        showEvery(stp3, stp4)
        btnCPR.activate()

        // step 2 모양 조절
        let colStp2 = bleMan!.dataObj.totalStringColor
        stp2.setBorder(0, rad: 4, borderCol: colStp2, bgCol: colStp2)
        stp2.textColor = UIColor.whiteColor()
        // step 3, 4
        adlt.step3Color(stp3)
        adlt.step4Color(stp4)

//        bttnStep4.setTitleColor(.whiteColor(), forState: .Normal)
//        if bleMan!.dataObj.allSetPass() {
//            bttnStep4.backgroundColor = colorLblDarkGreen
//        } else {
//            bttnStep4.backgroundColor = colorBarBgRed
//        }

        //setLabelColor(stp4, theBool: bleMan!.dataObj.allSetPass())

//        setAdultLabelColor(stp1, theBool: (bleMan?.testObj?.tst1)!)
//        setAdultLabelColor(stp2, theBool: (bleMan?.testObj?.tst2)!)
//        setAdultLabelColor(stp3, theBool: (bleMan?.testObj?.tst3)!)
//        setAdultLabelColor(stp5, theBool: adlt.tst5)
//        setAdultLabelColor(stp6, theBool: adlt.tet6)
//        setAdultLabelColor(stp7, theBool: adlt.tet7)
//        setAdultLabelColor(stp8, theBool: adlt.tet8)
//        setAdultLabelColor(stp9, theBool: adlt.finalStepOf1stRescure && adlt.finalStepOf2ndRescure)

        if cprProtoN.theVal != 0 { setEuNzLabels() }
    }

    private func setEuNzLabels() {
//        let tO = bleMan!.testObj!
//        var arr = [ tO.chkSafety, tO.tst1, tO.airway, tO.chkBreath, tO.tst2 ]
//        if cprProtoN.theVal == 2 {
//            arr = [ tO.chkSafety, tO.tst1, tO.tst2, tO.airway, tO.chkBreath ]
//        }
//        bttnStep4.setTitle(langStr.obj.step + " 6", forState: .Normal)
//        for (idx, lbl) in lblStep1to5.enumerate() {
//            if isAdult {
//                setAdultLabelColor(lbl, theBool: arr[idx])
//            } else {
//                setLabelColor(lbl, theBool: arr[idx])
//            }
//        }
    }

    private func setAdultLabelColor(lbl: UILabel, theBool: Bool) {
        lbl.backgroundColor = colorTransparent
        if theBool { lbl.textColor = colorLblDarkGreen }
        else { lbl.textColor = colorBarBgRed }
    }

    private func setLabelColor(lbl: UILabel, theBool: Bool) {
        if theBool { lbl.backgroundColor = colorLblDarkGreen }
        else { lbl.backgroundColor = colorBarBgRed }
    }

    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
//
//        let bezPath = UIBezierPath(), xV = 150
//        bezPath.moveToPoint(CGPoint(x: xV, y: 140))
//        bezPath.addLineToPoint(CGPoint(x: xV, y: 270))
//        bezPath.lineWidth = 1
//        colorBttnDarkGray.setStroke()
//        bezPath.lineCapStyle = .Round
//        bezPath.stroke()
    }

    // MARK:  언어 세팅.
    override func setLanguageString() {
        btnPass.setTitle(langStr.obj.pass, forState: .Normal)
        btnFail.setTitle(langStr.obj.needs_remediation, forState: .Normal)
        labelNotice.text = langStr.obj.pass_nr_info
        if isAdult {
            stp1.text = langStr.obj.bls_adult_1_title
            stp2.text = langStr.obj.cpr
            stp3.text = langStr.obj.bls_adult_2_title
            stp4.text = langStr.obj.bls_adult_3_title
        } else { // 평가 및 가동      가슴압박 / 호흡
            stp1.text = langStr.obj.bls_infant_1_title
            stp2.text = langStr.obj.bls_infant_2_title
        }


//        for (idx, lbl) in arrStepLbl.enumerate() {
//            lbl.cornerRad(5)// layer.cornerRadius = 5            lbl.layer.masksToBounds = true
//            lbl.backgroundColor = colorLblDarkGreen
//            if cprProtoN.theVal == 0 {
//                lbl.text = langStr.obj.step + " \(idx+1)"
//            } else { // print("  \(#function)  \(#line)  \(idx)")
//                lbl.text = langStr.obj.step + " \(idx+1)"
//                if 2 < idx {
//                    lbl.text = langStr.obj.step + " \(idx+3)"
//                }
//            }
//        }
//        for (i, lbl) in lblStep1to5.enumerate() {
//            lbl.cornerRad(5)
//            lbl.backgroundColor = colorLblDarkGreen
//            lbl.text = langStr.obj.step + " \(i + 1)" // "\(lbl.tag)"
//        }
    }
}
