//
//  ResultInfantStep4.swift
//  Test
//
//  Created by Jongwoo Moon on 2016. 2. 24..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation

class ResultInfantStep4: HtView {

    @IBOutlet weak var lblNumber: UILabel!

    let unitY: CGFloat = 40

    func initSet(rescure1: HmTestInft, num: Int) {
        super.initSet()

        // 전체 높이 : 240.
        // 604 30  >>  40

        lblNumber.text = "\(num)"

        var yco: CGFloat = 4

        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "SingleRow", bundle: bundle)
        var newView = nib.instantiateWithOwner(self, options: nil).last as? SingleRow
        //newView?.labelMain.text = langStr.obj.bls_infant_sheet_check_4
        //newView?.layer.cornerRadius = 5

        newView!.frame = CGRect(x: 64, y: yco, width: 604, height: 30); yco += unitY
        newView?.titleOnly()
        addSubview(newView!)

//
//        newView = nib.instantiateWithOwner(self, options: nil).last as? SingleRow
//        newView?.labelMain.text = langStr.obj.bls_infant_sheet_check_4_1
//        newView?.setCheckImgB(rescure1.finger1)
//        newView?.isLabelCase(false); newView?.singleRow()
//        newView!.frame = CGRect(x: 64, y: yco, width: 604, height: 30); yco += unitY
//        addSubview(newView!)
//        newView = nib.instantiateWithOwner(self, options: nil).last as? SingleRow
//        newView?.labelMain.text = langStr.obj.bls_infant_sheet_check_4_2
//        newView?.labelB.text = "\(rescure1.compRateN)s"
//        //if rescure1.compRateN <= 18 {
//        newView?.labelB.textColor = colorGreenRedBy(rescure1.compRateN <= 18)
//
//        //else { newView?.labelB.textColor = colorBarBgRed }
//
//        newView?.isLabelCase(true); newView?.singleRow()
//        newView!.frame = CGRect(x: 64, y: yco, width: 604, height: 30); yco += unitY
//        addSubview(newView!)
//        newView = nib.instantiateWithOwner(self, options: nil).last as? SingleRow
//        newView?.labelMain.text = langStr.obj.bls_infant_sheet_check_4_3
//        newView?.setCheckImgB(rescure1.adqDepth3)
//        newView?.isLabelCase(false); newView?.singleRow()
//        newView!.frame = CGRect(x: 64, y: yco, width: 604, height: 30); yco += unitY
//        addSubview(newView!)
//        newView = nib.instantiateWithOwner(self, options: nil).last as? SingleRow
//        newView?.labelMain.text = langStr.obj.bls_infant_sheet_check_4_4
//        newView?.setCheckImgB(rescure1.recoil4)
//        newView?.isLabelCase(false); newView?.singleRow()
//        newView!.frame = CGRect(x: 64, y: yco, width: 604, height: 30); yco += unitY
//        addSubview(newView!)
//        newView = nib.instantiateWithOwner(self, options: nil).last as? SingleRow
//        newView?.labelMain.text = langStr.obj.bls_infant_sheet_check_4_5
//        newView?.setCheckImgB(rescure1.minSusComp5)
//        newView?.isLabelCase(false); newView?.singleRow()
//        newView!.frame = CGRect(x: 64, y: yco, width: 604, height: 30); yco += unitY
        addSubview(newView!)
    }

    override func setLanguageString() {
    }

    override func drawRect(rect: CGRect) {
        super.drawRect(rect)

        let subLineNum = 5

        let bezPath = UIBezierPath(), yV: CGFloat = 40, stX: CGFloat = 60,
        edX = frame.size.width - 10
        bezPath.moveToPoint   (CGPoint(x: stX, y: yV))
        bezPath.addLineToPoint(CGPoint(x: edX, y: yV))
        bezPath.lineWidth = 2
        colorBttnDarkGray.setStroke()
        bezPath.lineCapStyle = .Round
        bezPath.stroke()

        var curY = yV + unitY

        for _ in 1...subLineNum - 1 {
            let bez = UIBezierPath()
            bez.moveToPoint     (CGPoint(x: stX, y: curY))
            bez.addLineToPoint  (CGPoint(x: edX, y: curY))
            bez.lineWidth = 1
            colorBttnDarkGray.setStroke()
            bez.lineCapStyle = .Round;   bez.stroke()
            curY += unitY
        }
        //layer.cornerRadius = 5
    }

}
