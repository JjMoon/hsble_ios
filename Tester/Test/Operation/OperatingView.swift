//
//  OperatingView.swift
//  Test
//
//  Created by Jongwoo Moon on 2016. 2. 16..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation

class OperatingView: HtView, UITableViewDelegate, UITableViewDataSource {
    var log = HtLog(cName: "OperatingView")
    var uiTimer = NSTimer(), bleMan:HsBleSuMan?, cnt = 0
    var adultObj: HmTestAdlt?, infantObj: HmTestInft?

    var curStateNum = 0
    var arrCell = [OperatingCell]()

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelStudent1: UILabel!
    @IBOutlet weak var labelStudent2: UILabel!

    @IBOutlet weak var labelComp: UILabel!
    @IBOutlet weak var labelBreath: UILabel!
    @IBOutlet weak var labelCompCount: UILabel!
    @IBOutlet weak var labelCompTime: UILabel!
    @IBOutlet weak var labelBreathCount: UILabel!
    @IBOutlet weak var labelBreathTime: UILabel!

    @IBOutlet weak var theTable: UITableView!

    @IBOutlet weak var bttnRightUp: UIButton!

    var blsView: SubBlsView?, afdView: SubAfdView?, stepView: SubStepCheckView?

    @IBAction func btnActRestart(sender: AnyObject) {
        bleMan?.testOperationStart()
        resetCell()
    }


    func initSet(adult: HmTestAdlt, infant: HmTestInft) {
        super.initSet()
        print(" OperatingView >> \(#function)  ")
        adultObj = adult; infantObj = infant
        log.bleObj = bleMan
        uiTimer = NSTimer.scheduledTimerWithTimeInterval(0.2, // second
          target:self, selector: #selector(OperatingView.update), userInfo: nil, repeats: true)

        //bleMan?.testOperationStart()  여기 있으면 안됨...

//        bleMan?.startWholeInOneProcess() // 나레이션 부분...
//        bleMan?.bleState = .S_WHOLESTEP_PRECPR  // 기다리는 부분...
//        bleMan?.operationStart()
//        bleMan?.startCompressionProcess()
    }

    func update() {
        if bleMan?.stt == .S_WHOLESTEP_AED {
            uiCallBack()
            uiTimer.invalidate()
        }
        bleMan!.uiMessageOff()
        let calc = bleMan!.calcManager
        if calc.first_cycle_start_time_set || 1 < calc.stage { updateSetInfo() }
    }

    func resetCell() {
        for cell in arrCell {
            cell.resetText()
        }
    }

    func setCompTextAndColor(cll: OperatingCell, st: HmUnitSet) {
        cll.lblCpCount.text = st.compCountStr; cll.lblCpCount.textColor = st.colorCCCount
        cll.lblCpTime.text = st.ccTimeSec;     cll.lblCpTime.textColor = st.colorCCTime
    }

    func updateSetInfo() {
        let arrSet = bleMan?.calcManager.data.arrSet, setN = arrSet?.count, calc = bleMan!.calcManager
        //log.printThisFNC("update", comment: " arrSet : \(arrSet?.count) ea")
        if 2 <= setN {
            for ix in 0...(setN! - 2) {
                let curCell = arrCell[ix], st = bleMan!.dataObj.arrSet[ix]
                setCompTextAndColor(curCell, st: st)
                //curCell.lblCpCount.text = st.compCountStr; curCell.lblCpCount.textColor = st.colorCCCount
                //curCell.lblCpTime.text = st.ccTimeSec;     curCell.lblCpTime.textColor = st.colorCCTime
                curCell.lblRpCount.text = st.brthCountStr; curCell.lblRpCount.textColor = st.colorRPCount
                curCell.lblRpTime.text = st.rpTimeSec;     curCell.lblRpTime.textColor = st.colorRPTime
            }
        }

        let arrIdx = calc.stage - 1
        //print("\n\n crash point :: arrIdx(calc.stage - 1) = \(arrIdx), arrCell 갯수 : \(arrCell.count) ")
        let curCll = arrCell[arrIdx]
        let curSet = bleMan!.dataObj.arrSet[arrIdx]
        if !calc.first_cycle_start_time_set && 0 < arrIdx {
            arrCell[arrIdx - 1].lblRpTime.text = (bleMan?.calcManager.compHoldTimer.timeSinceIntStr)! + "s"
            return
        }

        if bleMan!.stt == .S_WHOLESTEP_COMPRESSION {
            curCll.lblCpCount.text = curSet.compCountStr
            curCll.lblCpTime.text = (bleMan?.dataObj.compTimeInTest)! + "s"
        }
        if bleMan!.stt == .S_WHOLESTEP_BREATH {
            setCompTextAndColor(curCll, st: curSet)
            curCll.lblRpCount.text = curSet.brthCountStr
            curCll.lblCpTime.text = curSet.ccTimeSec
            curCll.lblRpTime.text = (bleMan?.calcManager.compHoldTimer.timeSinceIntStr)! + "s" }
    }

    //////////////////////////////////////////////////////////////////////     _//////////_     [ UI      << >> ]    _//////////_   Table View
    // MARK: Table View
    func addTableCellNib() {
        let nib = UINib(nibName: "OperatingCell", bundle: nil)
        theTable.registerNib(nib, forCellReuseIdentifier: "operatingCell")
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        print(" numberOfRowsInSection :: \(HsBleSingle.inst().stageLimit)  \(bleMan?.dataObj.arrSet.count) ")
        return Int(HsBleSingle.inst().stageLimit)
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        print ("cellForRowAtIndexPath   \(indexPath)  \(indexPath.row) ")
        var cell:OperatingCell? = tableView.dequeueReusableCellWithIdentifier("operatingCell") as? OperatingCell
        if cell == nil {
            addTableCellNib()
            cell = tableView.dequeueReusableCellWithIdentifier("operatingCell") as? OperatingCell
        }
        cell!.initialCellSetting(indexPath.row)
        arrCell.append(cell!)
        //cell?.setItems(indexPath.row + 1, dObj: (bleMan?.dataObj)!)
        return cell!
    }



    // MARK:  언어 세팅.
    override func setLanguageString() {
    }



    
}
