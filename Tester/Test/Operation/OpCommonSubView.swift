//
//  OpSubAlerCallPuls.swift
//  Test
//
//  Created by Jongwoo Moon on 2016. 1. 25..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation

class OpCommonSubView: UIView { // 이 뷰는 일단 중지하자...  나중에 체계를 잡도록하고.. 일단은 그냥 뷰로 간다..
    var log = HtLog(cName: "OpCommonSubView")

    var arrViews = UIView()


    let vwYfr: CGFloat = 60, vwYto: CGFloat = 200  // 수직 분포 영역 정의

    let leftMargin: CGFloat = 20, checkBttnSize: CGFloat = 30, space: CGFloat = 10,
    labelSize: CGSize = CGSize(width: 120, height: 30)

    func stepFirstUISet() {
        log.printThisFNC("stepFirstUISet", comment: "")

        var aVw = self.addSubViewVertCenter(nil, frY: vwYfr, toY: vwYto, marginX: 0, nth: 0, ea: 4, margin: 0) // 컨테이너 뷰 추가
        let vwHeight = (aVw?.frame.height)!
        let buttonY = (vwHeight - checkBttnSize) / 2   //,  labelY = (vwHeight - labelSize.height) / 2
        var aCheckBttn = HtCheckBoxButton(x: leftMargin, y: buttonY, size: checkBttnSize, state: false) // 의식 확인
        var aLabel = HtUltraLightLabel(x: leftMargin + checkBttnSize + space , y: 0, wid: labelSize.width,
            hgh: vwHeight, size: 17, txt: "의식 확인") // sizeToFit 하고 위치시킴.
        aVw!.addSubview(aCheckBttn); aVw!.addSubview(aLabel)
        aVw!.backgroundColor = colorWhiteAlpha10

        aVw = self.addSubViewVertCenter(nil, frY: vwYfr, toY: vwYto, marginX: 0, nth: 1, ea: 4, margin: 0) // 컨테이너 뷰 추가
        aCheckBttn = HtCheckBoxButton(x: leftMargin, y: buttonY, size: checkBttnSize, state: false) // 의식 확인
        aLabel = HtUltraLightLabel(x: leftMargin + checkBttnSize + space , y: 0, wid: labelSize.width,
            hgh: vwHeight, size: 17, txt: "구조 요청") // sizeToFit 하고 위치시킴.
        aVw!.addSubview(aCheckBttn); aVw!.addSubview(aLabel)

        aVw = self.addSubViewVertCenter(nil, frY: vwYfr, toY: vwYto, marginX: 0, nth: 2, ea: 4, margin: 0) // 컨테이너 뷰 추가
        aCheckBttn = HtCheckBoxButton(x: leftMargin, y: buttonY, size: checkBttnSize, state: false) // 의식 확인
        aLabel = HtUltraLightLabel(x: leftMargin + checkBttnSize + space , y: 0, wid: labelSize.width,
            hgh: vwHeight, size: 17, txt: "맥박 확인") // sizeToFit 하고 위치시킴.
        aVw!.addSubview(aCheckBttn); aVw!.addSubview(aLabel)


    }

    override func drawRect(rect: CGRect) {
        log.printThisFNC("drawRect", comment: "")
        super.drawRect(rect)

        //graph.startDraw(UIGraphicsGetCurrentContext()!)

        let bezPath = UIBezierPath()
        bezPath.moveToPoint(CGPoint(x: 30, y: 220))
        bezPath.addLineToPoint(CGPoint(x: 250, y: 220))
        bezPath.lineWidth = 1
        colorBttnDarkGray.setStroke()
        bezPath.lineCapStyle = .Round
        bezPath.stroke()
    }



}