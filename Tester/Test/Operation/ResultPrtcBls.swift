//
//  ResultPrtcBls.swift
//  HS Test
//
//  Created by Jongwoo Moon on 2016. 10. 18..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation


protocol ResultPrtcBls : class {

    var titleNum: Int { get set }
    var container: HtTableView? { get set }

    func insertBlsSteps(tObj: HmTest, idx: Int)

    func insertInfantStep2(tObj: HmTestInft, idx: Int)



    //func insertInfantConductCPR(tObj: HmTest, idx: Int)
    //func insertInfant2ndRescure(tObj: HmTest, idx: Int)

    func insertAdultConductCPR(tObj: HmTestAdlt, dataObj: HsData, idx: Int)
    func insertAdultAED(tObj: HmTestAdlt, idx: Int)

    //func insertAdultAHA(tObj: HmTestAdlt, idx: Int)
    //func insertAdultEurAnz(tObj: HmTestAdult, idx: Int)
    func insertAdultAfterShock(resc1: HmTestAdlt, resc2: HmTestAdlt, idx: Int)

    func getNextIndex() -> Int
}


extension ResultPrtcBls where Self: UIViewController {

    func insertBlsSteps(tObj: HmTest, idx: Int) {
        print("\n\nResultPrtcBls ::  insertBlsSteps(tObj: HmTestBase) ")
        let stepStrArr = tObj.getBlsStepArray()
        let valArr = tObj.step1PropArr()
        insertRowAt(idx, arrBool: valArr, arrStr: stepStrArr)
        print("insertBlsSteps : idx \(idx)  \n\n")
    }

    func insertAdultAED(tObj: HmTestAdlt, idx: Int) {
        // "전원켬" "패드부착" "분석물러남" "제세동물러남" "제세동" "압박재개"
        var arrKey = [ "전원켬", "패드부착", "분석물러남", "제세동물러남", "제세동" ]
        var arrBool = arrKey.map { (ky) -> Bool in tObj.dicStepRest[ky]!! }
        let lg = langStr.obj
        var arrMsg = [ lg.bls_adult_sheet_aed1, lg.bls_adult_sheet_aed2, lg.bls_adult_sheet_aed3,
                       lg.bls_adult_sheet_aed4, lg.bls_adult_sheet_aed5 ]
        insertRowAt(idx, arrBool: arrBool, arrStr: arrMsg)

        arrKey = [ "압박재개" ]
        arrBool = arrKey.map { (ky) -> Bool in tObj.dicStepRest[ky]!! }
        arrMsg = [ lg.bls_adult_3_check_1 ]
        insertRowAt(idx + 1, arrBool: arrBool, arrStr: arrMsg)

        print("성인 AED  : idx \(idx)")
    }


    func insertInfantStep2(tObj: HmTestInft, idx: Int) {
        // "압박1" "호흡1" "압박2" "호흡2" "중단시간" "세트3" "세트4"
        var arrKey = [ "압박1", "호흡1", "압박2", "호흡2", "중단시간" ]
        var arrBool = arrKey.map { (ky) -> Bool in tObj.dicStepRest[ky]!! }
        let lg = langStr.obj
        var arrMsg = [ lg.bls_infant_2_check_1, lg.bls_infant_2_check_2, lg.bls_infant_2_check_3,
                       lg.bls_infant_2_check_4, lg.bls_infant_2_check_5 ]
        insertRowAt(idx, arrBool: arrBool, arrStr: arrMsg)

        arrKey = [ "세트3", "세트4" ]
        arrBool = arrKey.map { (ky) -> Bool in tObj.dicStepRest[ky]!! }
        arrMsg = [ lg.bls_infant_3_check_1, lg.bls_infant_3_check_2 ]
        insertRowAt(idx + 1, arrBool: arrBool, arrStr: arrMsg)
    }

    /// adult Conduct CPR  압박/호흡 실습 결과 뷰..
    func insertAdultConductCPR(tObj: HmTestAdlt, dataObj: HsData, idx: Int) {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib4 = UINib(nibName: "AdultCPRVw", bundle: bundle)
        let v4 = nib4.instantiateWithOwner(self, options: nil).last as? ResultAdultCPR
        v4?.initSet(tObj, dataObj: dataObj)  // 4번 값 설정..

        //v4!.lblNumber.text = "\(titleNum)"
        //titleNum += 1

        //v4!.cornerRad(6)
        container!.data[idx].arrRow.append(v4!)

        print("성인 conduct Cpr : idx \(idx)")
    }


    private func insertRowAt(secNum: Int, arrBool: [Bool], arrStr: [String]) {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "DoubleRow", bundle: bundle)

        var b1 = false, lbl1 = "", vw: DoubleRow?
        for (idx, st) in arrStr.enumerate() {
            if (idx % 2) == 0 {
                let newVw = nib.instantiateWithOwner(self, options: nil).last as? DoubleRow
                newVw!.frame = CGRect(x: 0, y: 0, width: 668, height: 60)

                b1 = arrBool[idx]; lbl1 = st
                newVw!.setProps(lbl1, check1: b1, lbl2: nil, check2: nil)
                vw = newVw!
                container!.data[secNum].arrRow.append(newVw!) // 첫번째 칸에 추가.
            } else {
                vw!.setProps(lbl1, check1: b1, lbl2: st, check2: arrBool[idx])
            }
        }
    }
    


//    func insertAdultAHA(tObj: HmTestAdlt, idx: Int) {
//        let bundle = NSBundle(forClass: self.dynamicType)
//        let nib = UINib(nibName: "ResultSingleRow", bundle: bundle)
//        var v1 = nib.instantiateWithOwner(self, options: nil).last as? ResultSingleRow
//        v1?.labelTitle.text = "\(titleNum)"  // "\(getNextIndex())"
//        titleNum += 1
//
//        v1?.labelMain.text = langStr.obj.bls_adult_sheet_check_6
//        //v1?.setCheckImgB(tObj.tet6) // 6번 값 설정
//        v1?.initSet(); v1?.isLabelCase(false); v1?.singleRow(); //v1?.cornerRad(6)
//        container!.data[idx].arrRow.append(v1!)
//
//        v1 = nib.instantiateWithOwner(self, options: nil).last as? ResultSingleRow
//        v1?.labelTitle.text = "\(titleNum)"  // "\(getNextIndex())"
//        titleNum += 1
//
//        v1?.labelMain.text = langStr.obj.bls_adult_sheet_check_7
//        //v1?.setCheckImgB(tObj.tet7) // 6번 값 설정
//        v1?.initSet(); v1?.isLabelCase(false); v1?.singleRow(); //v1?.cornerRad(6)
//        container!.data[idx].arrRow.append(v1!)
//
//        v1 = nib.instantiateWithOwner(self, options: nil).last as? ResultSingleRow
//        v1?.labelTitle.text = "\(titleNum)"  // "\(getNextIndex())"
//        titleNum += 1
//
//        v1?.labelMain.text = langStr.obj.bls_adult_sheet_check_8
//        //v1?.setCheckImgB(tObj.tet8)
//        v1?.initSet(); v1?.isLabelCase(false); v1?.singleRow(); //v1?.cornerRad(6)
//        container!.data[idx].arrRow.append(v1!)
//
//        print("성인 insertAdultAHA : idx \(idx)")
//    }


    func insertAdultAfterShock(resc1: HmTestAdlt, resc2: HmTestAdlt, idx: Int) {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nibas = UINib(nibName: "ResultAdultAfterShock", bundle: bundle)
        let asVw = nibas.instantiateWithOwner(self, options: nil).last as? ResultAdultAfterShock
        //asVw?.initSet(resc1, resc2: resc2) // 9번 객체 할당..

        asVw?.lblNumber.text = "\(titleNum)"
        titleNum += 1

        //asVw!.cornerRad(6)
        container!.data[idx].arrRow.append(asVw!)
    }

    /// 현재까지 몇개의 뷰가 추가되었나..
    func getNextIndex() -> Int {
        return container!.data.map { (dt) -> Int in
            dt.arrRow.count
            }.reduce(0) { (a, b) -> Int in
                a + b
            } + 1
    }

}
