//
//  ResultInfantStep4.swift
//  Test
//
//  Created by Jongwoo Moon on 2016. 2. 24..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation

class ResultInfant2A: HtView {

    var rescure: HmTestInft?

    var arrRow = [SingleRow]()

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var row0: SingleRow!
    @IBOutlet weak var row1: SingleRow!
    @IBOutlet weak var row2: SingleRow!

    let unitY: CGFloat = 30

    func initSet(rescure2: HmTestInft) { // 높이 .. 30, 40, 40 ..
        super.initSet()

        var yco: CGFloat = 0

        rescure = rescure2
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "SingleRow", bundle: bundle)

        var newView = nib.instantiateWithOwner(self, options: nil).last as? SingleRow
        newView?.isLabelCase(true)  //   1   2   로 세팅...
        newView!.frame = CGRect(x: 64, y: yco, width: 604, height: 30); yco += unitY
        addSubview(newView!); arrRow.append(newView!)
        newView = nib.instantiateWithOwner(self, options: nil).last as? SingleRow
        newView!.frame = CGRect(x: 64, y: yco, width: 604, height: 40); yco += 40
        addSubview(newView!); arrRow.append(newView!)
        newView = nib.instantiateWithOwner(self, options: nil).last as? SingleRow
        newView?.frame.size.height = 40
        newView!.frame = CGRect(x: 64, y: yco, width: 604, height: 40); yco += 40
        addSubview(newView!); arrRow.append(newView!)
    }
//
//    func setCololumn6() {
//        arrRow[0].labelMain.text = langStr.obj.bls_infant_sheet_check_6
//        arrRow[1].labelMain.text = langStr.obj.bls_infant_sheet_check_6_1
//        arrRow[2].labelMain.text = langStr.obj.bls_infant_sheet_check_6_2
//
//        arrRow[1].isLabelCase(false);    arrRow[2].isLabelCase(false)
//
//        arrRow[1].setImageX()
//        arrRow[2].setCheckImgA(rescure!.tst6a)
//        arrRow[2].setCheckImgB(rescure!.tst6b)
//    }
//
//    func setCololumn7() {
//        arrRow[0].labelMain.text = langStr.obj.bls_infant_sheet_check_7
//        arrRow[1].labelMain.text = langStr.obj.bls_infant_sheet_check_7_1
//        arrRow[2].labelMain.text = langStr.obj.bls_infant_sheet_check_7_2
//        arrRow[1].isLabelCase(true);    arrRow[2].isLabelCase(false)
//        arrRow[1].labelA.text = "\(rescure!.tst7a)s"
//        if rescure?.tst7a <= 9 { arrRow[1].labelA.textColor = colorBarBgGrn }
//        else { arrRow[1].labelA.textColor = colorBarBgRed }
//        arrRow[1].labelB.text = "\(rescure!.tst7b)s"
//        if rescure?.tst7b <= 9 { arrRow[1].labelB.textColor = colorBarBgGrn }
//        else { arrRow[1].labelB.textColor = colorBarBgRed }
//        arrRow[2].setImageX()
//    }

    override func setLanguageString() {

    }


    override func drawRect(rect: CGRect) {
        super.drawRect(rect)

        let subLineNum = 2
        let unitY: CGFloat = 40 // 반복 셀 높이.
        let yV: CGFloat = 30  // 첫 셀 높이
        let bezPath = UIBezierPath(), stX: CGFloat = 60,
        edX = frame.size.width - 10
        bezPath.moveToPoint   (CGPoint(x: stX, y: yV))
        bezPath.addLineToPoint(CGPoint(x: edX, y: yV))
        bezPath.lineWidth = 2
        colorBttnDarkGray.setStroke()
        bezPath.lineCapStyle = .Round
        bezPath.stroke()

        var curY = yV + unitY

        for _ in 1...subLineNum - 1 {
            let bez = UIBezierPath()
            bez.moveToPoint     (CGPoint(x: stX, y: curY))
            bez.addLineToPoint  (CGPoint(x: edX, y: curY))
            bez.lineWidth = 1
            colorBttnDarkGray.setStroke()
            bez.lineCapStyle = .Round;   bez.stroke()
            curY += unitY
        }
        //layer.cornerRadius = 5
    }

}
