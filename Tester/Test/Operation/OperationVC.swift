//
//  OperationVC.swift
//  Test
//
//  Created by Jongwoo Moon on 2016. 1. 25..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation
import AVFoundation



/**  객체 할당 방법


 BleSuMan = 한 화면..  
 >> 한 화면에 2명의 학생..
 
 #;> Model 객체는 testObj1, 2 로 bleSuMan 이 갖고 있는다.
    이 객체는 OperationUnitView 로 들어갈 때 생성됨.

 
 테스트 끝날 때 까지 bleMan 에 TestObj 를 할당해서 쓰고..  학생도 따로 갖고 있고? ..
 
 DB에 그냥 저장.. 나중에 학생 정보에 가서는 DB 를 다시 긁어야지..

 한 학생은 :: 영아|성인 X 1 | 2 ..  이렇게 4가지 역할에 대한 정보가 있어야 함..
 

 화면 조절 :
 영아 : bleMan?.curStudent, curStdntSub == nil 조건. ble를 reset 하면 이놈들도 nil 세팅.


 HmTestAdult
 
 >> 오퍼레이션 끝난 후에는    ResultVC 를 로딩하고.
    학생 관리 화면에서는     ResultTotalVC 를 로딩 함..


**/

class OperationVC : HtViewController, UIAlertViewDelegate {
    var log = HtLog(cName: "OperationVC")

    var arrUiObj = HsUIArray()
    var arrView = [OperationUnitView]()

    /// PrtcEnlargeView 관련..
    var targetView: UIView?
    var tarPoint: CGPoint?
    var baseOrigin: CGPoint? = nil
    var scal : CGFloat  = 2.0
    var enlargeClsr: ((Void) -> Void)?
    var bgView: UIView?
    var bttnClose: UIButton?
    var vwResultTitle: ResultTitleView!

    var finishedViewNum = 0

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var bttnGoToHome: UIButton!
    @IBOutlet weak var bttnQuit: UIButton!

    @IBOutlet weak var vwLeftUp: OperationUnitView!
    @IBOutlet weak var vwRigtUp: OperationUnitView!
    @IBOutlet weak var vwLeftLo: OperationUnitView!
    @IBOutlet weak var vwRigtLo: OperationUnitView!
    @IBOutlet weak var imgStepIcons: UIImageView!
    @IBOutlet weak var imgPfSelect: UIImageView!
    @IBOutlet weak var bttnSelectPF: UIButton!

    var openSendDataViewCljr: () -> () = { }

    //////////////////////////////////////////////////////////////////////     [ UI      << >> ]
    // MARK:  메인으로 돌아가기.
    @IBAction func bttnActGoHome(sender: AnyObject) {
        goHomeAlert()
    }

    @IBAction func btnActQuit(sender: AnyObject) {
        if isAdult { disConnectAlert() }
        else { gotoMainAlert() }
    }

    @IBAction func btnActSelectPFConfirm(sender: UIButton) {
        sender.hideMe()
        imgPfSelect.hideMe()
    }

    //////////////////////////////////////////////////////////////////////     [ UI   ViewDidLoad   << >> ]
    override func viewDidAppear(animated: Bool) { // 타이머 세팅..
        super.viewDidAppear(animated)

//        switch cprProtoN.theVal { // 이걸 원상 복구 하는 건 Select View 에서..
//        case 0:
//            CC_STRONG_POINT = 101
//        default:
//            CC_STRONG_POINT = Int32(CC_STRONG_POINT_DEFAULT)
//        }

        if 0 < arrView.count { return }

        add4SubViews()
        addResultTitleView()
    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)

    }

    override func update() {
        checkFinishedState()
        if !isAdult { return }

        if HsBleMaestr.inst.connectionNumber() == 0 { // 앞 화면으로 돌아가기.
            uiTimer.invalidate()

            self.navigationController?.popViewControllerAnimated(viewAnimate)

            /*
            let altVw = UIAlertView(title: langStr.obj.at_least_1_connect, message: nil, delegate: self,
                cancelButtonTitle: nil)
            altVw.show()
            HsGlobal.delay(3.0, closure: { () -> () in
                altVw.dismissWithClickedButtonIndex(0, animated: viewAnimate)
                self.navigationController?.popViewControllerAnimated(viewAnimate)
            })  */
        } else {

            // checkFinishedState()
        }
    }

    func gotoMainAlert() {
        showGeneralPopup(nil, message: langStr.obj.want_main, yesStr: langStr.obj.yes, noString: langStr.obj.no,
            yesAction: { self.navigationController?.popToRootViewControllerAnimated(viewAnimate) })
    }

    func goHomeAlert() {
        let altVw = UIAlertView(title: langStr.obj.to_main, message: langStr.obj.want_main,
            delegate: self, cancelButtonTitle: langStr.obj.yes, otherButtonTitles: langStr.obj.no)
        altVw.tag = 33
        altVw.show()
    }

    func disConnectAlert() {
        showAlertPopupOfBackToMainInMonitor() {
            HsBleMaestr.inst.disconnectAllConnections()
        }
    }

    // MARK:  Alert View Delegate ...
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if buttonIndex != 0 { return }
        switch alertView.tag {
        case 33:
            log.logUiAction("sendCommand(.S_READY)", lnum: 5, printOn: true)
            HsBleMaestr.inst.setParsingState(.StandBy)
            HsBleMaestr.inst.sendCommand(.S_READY)
            self.navigationController?.popToViewController((self.navigationController?.viewControllers[1])!, animated: viewAnimate)
        default:
            print("ERROR in Alert View Delegate ....  tag : 1, 10, 20 ")
        }
    }

    // MARK:  데이터 저장 팝업 띄우기
    func addResultTitleView() { // 뷰 로딩하고 숨겨 놓는다.
        //let frm = vwResultTitle.frame
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "ResultTitleView", bundle: bundle)
        vwResultTitle = nib.instantiateWithOwner(self, options: nil).last as! ResultTitleView
        vwResultTitle.frame = CGRect(x: 0, y: 100, width: 768, height: 80)
        vwResultTitle.backgroundColor = HmGraphSetting.inst.viewBgGray
        view.addSubview(vwResultTitle)
        vwResultTitle.openSendDataViewCljr = { () -> () in  self.openSendDataView() }
        vwResultTitle.initSet()
        vwResultTitle.hideMe()
    }

    func openSendDataView() {
        let vc = DataSavePopoverVC(nibName: "DataSavePopover", bundle: nil)
        vc.arrUiObj = arrUiObj

        vc.modalPresentationStyle = .FormSheet // .Popover
        presentViewController(vc, animated: true, completion: nil)
        vc.popoverPresentationController?.sourceView = view
        vc.popoverPresentationController?.sourceRect = view.frame
        vc.setLanguageString()
    }

    func checkFinishedState() {  //  160801 checkFinishedState
        //var activeVwNum = 0
//        if isAdult { activeVwNum = HsBleMaestr.inst.connectionNumber() } // 현재 상태를 ble객체에 담는 게 어떨지..
//        else { activeVwNum = HsBleMaestr.inst.numOfTestInfantActiveView }

        // 보이는 뷰로 판단하자..
        let activeVwNum = arrView.filter({ (vw) -> Bool in
            !vw.hidden
        }).count

        // 접속 수 만큼 받으면 뷰 띄우기..

        if activeVwNum == 0 { return }

        //print("  연결 수 : adult  :  \(isAdult)    viewNum  :  \(activeVwNum)    \(finishedViewNum)")

        if finishedViewNum == activeVwNum {
            vwResultTitle.showMe()
            vwResultTitle.setNeedsLayout()

            imgStepIcons.hideMe()

            for vw in arrView {
                vw.bttnCloseResult.showMe()
            }

            switch langIdx {
            case 0:
                imgPfSelect.image = UIImage(named: "pf_select_en.png")
            case 1:
                imgPfSelect.image = UIImage(named: "pf_select_de.png")
            case 2:
                imgPfSelect.image = UIImage(named: "pf_select_es.png")
            case 3:
                imgPfSelect.image = UIImage(named: "pf_select_fr.png")
            default:
                imgPfSelect.image = UIImage(named: "pf_select_kr.png")
            }

            //imgPfSelect.showMe()
            //bttnSelectPF.showMe()
            //view.bringSubviewToFront(imgPfSelect)
            //view.bringSubviewToFront(bttnSelectPF)

            finishedViewNum = 0 //  160801  다시 안 뜨게..
        }
    }

    //  160801 checkFinishedState
    func receivedDataSaveCallBackFromUnitView() {
        finishedViewNum += 1
    }

    func showResultView(bleObj: HsBleSuMan) {  // 결과 뷰 보여주기....
        let vc = ResultVC()
        vc.bleMan = bleObj; vc.isTesting = true
        vc.testObj = bleObj.testObj
        let nickList = [ "A", "B", "C", "D" ]
        vc.abcdObj.removeAll()

        if isAdult {
            for nk in nickList { // 연결된 놈들 어레이에 세팅..  nil 도 있슴.
                vc.abcdObj.append(HsBleMaestr.inst.getConnectedBLEwithhNick(nk))
            }
        } else {
            vc.abcdObj = HsBleMaestr.inst.getInfantViewArray()
        }

        HsBleMaestr.inst.tempBle = nil
        presentViewController(vc, animated: true, completion: nil)
    }

    override func viewDidLoad() {
        view.backgroundColor = HmGraphSetting.inst.viewBgGray

        //add4SubViews()
        setLanguageString()
        //addResultTitleView()

        for ble in HsBleMaestr.inst.arrBleSuMan {
            print("  arrUiObj.addUIElementWith  Student : \(ble.curStudent?.name) , \(ble.curStdntSub?.name) ")
            arrUiObj.addUIElementWith(ble.curStudent)
            arrUiObj.addUIElementWith(ble.curStdntSub)
        }
        arrUiObj.printAll()
    }

    private func add4SubViews() {
        //let temp = [ vwLeftUp, vwRigtUp, vwLeftLo, vwRigtLo ]
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "OperationUnitView", bundle: bundle)

        //for (idx, var vw) in temp.enumerate() {
        for (idx, vwR) in rectArr.enumerate() {
            //let frm = vw
            let vw = nib.instantiateWithOwner(self, options: nil).last as! OperationUnitView
            vw.frame = vwR
            vw.bleMan = HsBleMaestr.inst.arrBleSuMan[idx]
            vw.bleCallBack = showResultView
            vw.sensorLostCallback = hardwareCallback
            vw.goToNextViewCallBack()
            vw.layer.cornerRadius = 15
            vw.showDataSaveButtonView = receivedDataSaveCallBackFromUnitView

            if let testObj = vw.bleMan!.testObj { // 현재 기준은 testObj 에 할당. 이건 각 뷰당 하나임.
                testObj.standard = cprProtoN.theVal
            }

            vw.enlargeCljr = enlargeCallback
            vw.addEnlargeButton()

            arrView.append(vw)
            self.view.addSubview(vw)
        }
    }

    func hardwareCallback(vw: OperationUnitView, mssg: String) {
        log.printThisFNC("hardwareCallback", comment: mssg)
        showSimpleMessageWith(mssg) { () -> Void in
            vw.resetStateBySensorError()
        }
    }

    // MARK:  언어 세팅.
    func setLanguageString() {
        if isAdult {
            labelTitle.text = langStr.obj.adult_title
        } else {
            labelTitle.text = langStr.obj.infant_title
        }
        bttnQuit.setTitle(langStr.obj.quitArrow, forState: .Normal) //
        bttnSelectPF.setTitle(langStr.obj.confirm, forState: .Normal)
    }
}




