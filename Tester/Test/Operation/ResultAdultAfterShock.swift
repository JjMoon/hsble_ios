//
//  ResultAdultAfterShock.swift
//  Test
//
//  Created by Jongwoo Moon on 2016. 2. 23..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation

class ResultAdultAfterShock: HtView {

    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!

    @IBOutlet weak var imgA1: UIImageView!
    @IBOutlet weak var imgA2: UIImageView!
    @IBOutlet weak var imgB1: UIImageView!
    @IBOutlet weak var imgB2: UIImageView!

    @IBOutlet weak var lblNumber: UILabel!


    func initSet(resc1: HmTestAdlt, resc2: HmTestAdlt) {
        super.initSet()
//        label1.text = langStr.obj.bls_adult_sheet_check_9
//        label2.text = langStr.obj.bls_adult_sheet_9_cc
//        label3.text = langStr.obj.bls_adult_sheet_9_rp

//        setCheckImgOf(imgA1, accordingTo: resc2.afterComp9a)
//        setCheckImgOf(imgA2, accordingTo: resc2.afterComp9b)
//        setCheckImgOf(imgB1, accordingTo: resc1.respWithBag1)
//        setCheckImgOf(imgB2, accordingTo: resc1.respWithBag2)
    }

    override func drawRect(rect: CGRect) {
        super.drawRect(rect)

        let subLineNum = 2
        let unitY: CGFloat = 30
        let bezPath = UIBezierPath(), yV: CGFloat = 35, stX: CGFloat = 60,
        edX = frame.size.width - 10
        bezPath.moveToPoint   (CGPoint(x: stX, y: yV))
        bezPath.addLineToPoint(CGPoint(x: edX, y: yV))
        bezPath.lineWidth = 2
        colorBttnDarkGray.setStroke()
        bezPath.lineCapStyle = .Round
        bezPath.stroke()

        var curY = yV + unitY

        for _ in 1...subLineNum - 1 {
            let bez = UIBezierPath()
            bez.moveToPoint     (CGPoint(x: stX, y: curY))
            bez.addLineToPoint  (CGPoint(x: edX, y: curY))
            bez.lineWidth = 1
            colorBttnDarkGray.setStroke()
            bez.lineCapStyle = .Round;   bez.stroke()
            curY += unitY
        }
        //layer.cornerRadius = 5
    }


    override func setLanguageString() {
//        label1.text = langStr.obj.bls_adult_sheet_check_9
//        label2.text = langStr.obj.bls_adult_sheet_9_cc
//        label3.text = langStr.obj.bls_adult_sheet_9_rp
    }
}
