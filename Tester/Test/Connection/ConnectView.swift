//
//  Operation.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2015. 11. 10..
//  Copyright © 2015년 IMLab. All rights reserved.
//

import Foundation
import UIKit

class ConnectView : UIView, SearchDeviceListViewDelegate {
    var log = HtLog(cName: "ConnectView")
    var isCalibration = false

    var myBleSuMan:HsBleSuMan?
    var deviceListView = SearchDeviceListView()
    var popup = KLCPopup()
    var theTimer:NSTimer?
    var disconnectCallBack: (HsBleSuMan) -> Void = { (bleObj) in  }
    
    @IBOutlet weak var labelTitleAtoD: UILabel!
    @IBOutlet weak var labelStateNoti: UILabel!
    @IBOutlet weak var labelConnecting: UILabel!
    
    @IBOutlet weak var bttnOffX: UIButton!
    @IBOutlet weak var bttnConnPrevKit: UIButton!
    @IBOutlet weak var bttnChangeKit: UIButton!
    @IBOutlet weak var bttnChangeKitLetter: UIButton!
    @IBOutlet weak var bttnStartCalibration: HtBoxButton!
    
    @IBOutlet weak var labelKitNum: UILabel!
    
    @IBOutlet weak var imgVwLoading: UIImageView!

    @IBOutlet weak var labelReady: UILabel!

    var openCalibrationVC: (HsBleManager) -> () = { bleman in  }
    var connectionSuccess: (String)->() = { (kitId) -> () in  }

    @IBAction func bttnActOff(sender: AnyObject) {
        disconnectCallBack(myBleSuMan)  // myBleSuMan?.reset()
    }
    
    @IBAction func bttnConnPrevKit(sender: UIButton) {
        log.logUiAction("  bttnConnPrevKit ::  \(myBleSuMan?.name)", lnum: 10)
        
        if labelKitNum.text?.lengthOfBytesUsingEncoding(NSASCIIStringEncoding) < 2 {
            changeKitAction()
            return
        }
        
        myBleSuMan!.setConnectView(self)
        myBleSuMan?.removePeripherals()
        setLoadingView()
        theTimer?.invalidate()
        theTimer = nil

        myBleSuMan?.connectPreviousDevice(myBleSuMan!.prevKitFullName, withFailBlock: {
            // Alert View ...  대신 상태 변경.
            self.hideAllUIElements()
            //self.labelKitNum.showMe()
            self.labelStateNoti.text = self.localStr("disconnected")

            //self.labelStateNoti.text = self.localStr("select_prevdev_failure")
            //self.labelStateNoti.textColor = UIColor.redColor()

            HsGlobal.delay(2.0, closure: {
                self.onlineActions()
                self.myBleSuMan!.stopScan()
            })
        })
    }
    
    @IBAction func bttnActChangeKit(sender: AnyObject) {
        log.logUiAction("bttnActChangeKit")
        changeKitAction()
    }

    @IBAction func bttnActStartCali(sender: AnyObject) {
        log.logUiAction("bttnActStartCali")
        openCalibrationVC(myBleSuMan!)
    }
    
    override func awakeFromNib() {
        log.printThisFunc("awakeFromNib", lnum: 3)
        
        myBleSuMan?.exceptionProc = {(com : Int32, val: Int32) -> Void in
            print("  myBleSuMan.exceptionProc  ::  com : \(com)  value : \(val)")
        }
    }

    
    func changeKitAction() {
        log.printThisFunc("changeKitAction", lnum: 1)
        myBleSuMan!.setConnectView(self)
        myBleSuMan?.removePeripherals()
        setLoadingView()
        theTimer?.invalidate()
        theTimer = nil
        myBleSuMan?.connect(nil, andPopupBlock: {
            self.selectDeviceList()
        })
    }
    
    func initializeView() {
        log.printThisFunc("initializeView", lnum: 1)
        onlineActions()

        //if labelKitNum.text?.lengthOfBytesUsingEncoding(NSASCIIStringEncoding) < 2 {
        if (myBleSuMan?.isTherePrevKit)! == false {
            bttnChangeKit.hideMe(); bttnChangeKitLetter.hideMe()
        }
    }
    
    func setLoadingView() {
        log.printThisFunc("setLoadingView", lnum: 1)
        hideAllUIElements()
        imgVwLoading.showMe()
        labelConnecting.showMe()
        imgVwLoading.rotate360Degrees(1.5, repeatCnt: Float.infinity, completionDelegate: nil)
    }

    func hideAllUIElements() {
        let hid = true
        log.printThisFunc("hideUI(hid: \(hid))", lnum: 1)
        bttnChangeKit.hidden = hid
        bttnChangeKitLetter.hidden = hid
        bttnConnPrevKit.hidden = hid
        labelReady.hidden = hid
        labelConnecting.hidden = hid
        imgVwLoading.hidden = hid
        labelKitNum.hidden = hid
    }
    
    func onlineActions() {
        log.printThisFunc("onlineActions", lnum: 1)

        bttnConnPrevKit.hidden = (myBleSuMan?.isConnectionTotallyFinished)!
        bttnChangeKit.hidden = (myBleSuMan?.isConnectionTotallyFinished)!
        labelKitNum.hidden = (myBleSuMan?.isConnectionTotallyFinished)!
        bttnChangeKitLetter.hidden = (myBleSuMan?.isConnectionTotallyFinished)!
        imgVwLoading.hidden = (myBleSuMan?.isConnectionTotallyFinished)!
        labelReady.hidden = !(myBleSuMan?.isConnectionTotallyFinished)!
        
        if ((myBleSuMan?.isConnectionTotallyFinished) == true) { // isConnectionTotallyFinished
            log.printAlways("myBleSuMan.isConnected", lnum: 5)
            labelStateNoti.text = localStr("connected")
            labelStateNoti.textColor = HmGraphSetting.inst.graphGreen
            bttnOffX.hidden = false
        } else {
            theTimer?.invalidate()
            theTimer = nil
            log.printAlways("myBleSuMan.isConnected  Not  ", lnum: 5)
            myBleSuMan?.reset()
            labelStateNoti.text = localStr("disconnected")
            labelStateNoti.textColor = UIColor.blackColor()
            bttnOffX.hidden = true
        }
        if labelKitNum.text?.lengthOfBytesUsingEncoding(NSASCIIStringEncoding) < 2 {
            bttnChangeKit.hideMe(); bttnChangeKitLetter.hideMe()
        }
        imgVwLoading.hidden = true
    }
    
    func enableButtons() {
        log.printThisFunc("enableButtons", lnum: 1)
        hideAllUIElements()
        bttnChangeKit.showMe(); bttnChangeKitLetter.showMe()
        bttnConnPrevKit.showMe()
        //labelStateNoti.text = localStr("disconnected")
        labelStateNoti.textColor = UIColor.blackColor()
        //if labelKitNum.text?.lengthOfBytesUsingEncoding(NSASCIIStringEncoding) < 2 {
        if (myBleSuMan?.isTherePrevKit)! == false {
            bttnChangeKit.hideMe(); bttnChangeKitLetter.hideMe()
        }
    }
    
    func connectionFinishedClojure() {
        log.printThisFunc("connectionFinishedClojure ==> process     monitor send  .READY", lnum: 5)
        //self.substringFromIndex(self.startIndex.advancedBy(index))
        labelReady.text = myBleSuMan?.kitName.subStringFrom(4)
        labelReady.showMe()
        labelConnecting.hideMe()
        HsBleMaestr.inst.deviceConnected((myBleSuMan)!)
        myBleSuMan?.connectedActions()

        connectionSuccess( (myBleSuMan?.prevKitName)! )
        onlineActions()
        if isCalibration {
            labelReady.hideMe()
            bttnStartCalibration.showMe()
            bttnStartCalibration.setTitle(localStr("calibration_start"), forState: .Normal)
        } else {
            bttnStartCalibration.hideMe()
        }

        theTimer = NSTimer.scheduledTimerWithTimeInterval(0.5, // second
            target:self, selector: Selector("uiUpdateAction"),
            userInfo: nil, repeats: true)
    }

    func uiUpdateAction() {
        //log.printThisFunc("Update Action", lnum: 1)
        if ((myBleSuMan?.isConnected) == false) {
            onlineActions()
        }
    }

    func connectDeviceResult(peripheral: HSPeripheral!, hsService service: HSBaseService!) {
        popup.dismiss(true)
    }
    
    func selectDeviceList() {
        let contentVw = UIView(frame: CGRect(x: 50, y: 50, width: 400, height: 250)) //UIView* contentView = [[UIView alloc] init]; contentView = [[UIView alloc]initWithFrame:CGRectMake(50.f, 50.f, 400.f, 250.f)];
        
        deviceListView = SearchDeviceListView(frame: CGRect(x: 0, y: 0, width: contentVw.frame.size.width, height: contentVw.frame.size.height))
        deviceListView.setBlockAndBleMan(myBleSuMan)
        deviceListView.backgroundColor = UIColor.yellowColor()
        deviceListView.lstVwDelegate = self // // 여기서 딜리깃을 세팅해 줌..  뷰 끼리 주고받는 거니..  그냥 놔둔다.
        myBleSuMan?.tempViewObj = deviceListView
        
        contentVw.addSubview(deviceListView)
        
        let layout = KLCPopupLayout(horizontal: KLCPopupHorizontalLayout.Center, vertical: KLCPopupVerticalLayout.Center)
        popup = KLCPopup(contentView: contentVw, showType: KLCPopupShowType.FadeIn, dismissType: KLCPopupDismissType.FadeOut, maskType: KLCPopupMaskType.Dimmed, dismissOnBackgroundTouch: true, dismissOnContentTouch: false)
        popup.showWithLayout(layout)  //[popup showWithLayout:layout];
        
        print(" popup.showWithLayout ")
        
        popup.didFinishDismissingCompletion = {
            if self.myBleSuMan?.connState == ConnectionStarted {
                self.setLoadingView()
            } else {
                self.log.printAlways(" popup.didFinishDismissingCompletion : State : \(self.myBleSuMan?.connState)", lnum: 5)
                if self.myBleSuMan?.connState == Initial { self.enableButtons() }
            }
        }
    }

    // MARK:  언어 세팅.
    func setLanguageString() {
        labelConnecting.hideMe()
        labelConnecting.text = localStr("connecting_kit")
        labelStateNoti.text = localStr("disconnected")
        bttnChangeKitLetter.setTitle(localStr("change_kit"), forState: .Normal)
        bttnChangeKitLetter.titleLabel?.numberOfLines = 0
        bttnChangeKitLetter.titleLabel?.alignCenter() // textAlignment = .Center
    }
}