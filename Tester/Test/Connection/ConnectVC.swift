//
//  ConnectVC.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2015. 11. 17..
//  Copyright © 2015년 IMLab. All rights reserved.
//

import Foundation



class ConnectVC : UIViewController, UIAlertViewDelegate {
    var log = HtLog(cName: "ConnectVC")
    var isCalibration = false
    var connectionSuccessCallback: (String)->() = { (kitId) -> () in }
    var arrView = [ConnectView]()
    var arrAddedView = [ConnectView]()
    var uiTimer = NSTimer()
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var viewLeftUp: ConnectView!
    @IBOutlet weak var viewRightUp: ConnectView!
    @IBOutlet weak var viewLeftLow: ConnectView!
    @IBOutlet weak var viewRightLow: ConnectView!
    @IBOutlet weak var bttn4debug: UIButton!
    @IBOutlet weak var bttnQuit: UIButton!
    
    @IBOutlet weak var bttnAssignStudent: UIButton!

    @IBAction func bttnTestRun(sender: AnyObject) {
        log.logUiAction()
    }
    
    
    @IBAction func bttnActAssignStudent(sender: AnyObject) {
        log.logUiAction("bttnActAssignStudent")


        let vc =  AssignStudents(nibName: "AssignStudents", bundle: nil)
        self.navigationController?.pushViewController(vc, animated:viewAnimate)


        return



        if HsBleMaestr.inst.connectionNumber() == 0 {
            let altVw = UIAlertView(title: langStr.obj.error, message: langStr.obj.at_least_1_connect, delegate: nil, cancelButtonTitle: langStr.obj.confirm)
            altVw.show()
        } else {
            let vc =  AssignStudents(nibName: "AssignStudents", bundle: nil)
            self.navigationController?.pushViewController(vc, animated:viewAnimate)
        }
    }
    
    //////////////////////////////////////////////////////////////////////     _//////////_     [     ]    _//////////_   Go to Main View
    // MARK:  메인으로 돌아가기.
    @IBAction func bttnActGoHome(sender: AnyObject) {
        showAlertctrlRestart()
    }

    @IBAction func bttnActHelp(sender: AnyObject) {
        openHeartisenseInfoWeb(1)// Monitor
    }

    @IBAction func bttnActQuit(sender: AnyObject) {
        showAlertPopupOfBackToMainInMonitor() {
            HsBleMaestr.inst.disconnectAllConnections()
        }
    }

    override func viewDidLoad() {
        if isDebugMode { bttn4debug.showMe() } else { bttn4debug.hideMe() }
        log.logUiAction("viewDidLoad", lnum: 10, printOn: true)

        uiTimer = NSTimer.scheduledTimerWithTimeInterval(0.5, // second
            target:self, selector: Selector("update"), userInfo: nil, repeats: true)

        arrView = [ viewLeftUp, viewRightUp, viewLeftLow, viewRightLow ]
        arrAddedView = [ConnectView]()
        connectionSuccessCallback = { (kitId) -> () in
            self.showConnectionSuccessMessage(kitId)
            self.refreshPrevKitName()
        }

        let title = ["A", "B", "C", "D"]
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "ConnectUnitView", bundle: bundle)

        for (idx, var vw) in arrView.enumerate() {
            log.logThis("Set Connect Unit Vie  \(idx)    name :: >>  \(HsBleMaestr.inst.arrBleSuMan[idx].name)  ", lnum: 1)

            let vwFrame = vw.frame
            vw = nib.instantiateWithOwner(self, options: nil).last as! ConnectView
            vw.frame = vwFrame
            vw.myBleSuMan = HsBleMaestr.inst.arrBleSuMan[idx]

            vw.disconnectCallBack = { (bleMan) in
                self.showGeneralPopup(langStr.obj.disconnect, message: langStr.obj.want_disconnect, yesStr: langStr.obj.yes, noString: langStr.obj.no, yesAction: { () -> Void in
                    bleMan.reset()
                })
            }

            HsGlobal.delay(0.1, closure: {
                vw.myBleSuMan!.setConnectView(vw)
                vw.labelTitleAtoD.text = title[idx]
                vw.initializeView()
            })
            // 기존 키트 번호 세팅.
            if (vw.myBleSuMan?.isTherePrevKit)! == true {
                vw.labelKitNum.text = vw.myBleSuMan!.prevKitName
            } else {
                vw.labelKitNum.text = langStr.obj.register_kit
            }

            vw.connectionSuccess = connectionSuccessCallback
            self.view.addSubview(vw)
            vw.setLanguageString()
            vw.isCalibration = isCalibration
            vw.openCalibrationVC = { (bleman: HsBleManager) -> () in
                let vc = CalibrationVC(nibName: "CalibrationVC", bundle: nil)
                vc.setBleObject(bleman)
                self.presentViewController(vc, animated:viewAnimate, completion: nil)
            }
        }

    }

    override func viewDidAppear(animated: Bool) {
           }

    func refreshPrevKitName() {
        for vw in arrAddedView {
            // 기존 키트 번호 세팅.
            if (vw.myBleSuMan?.isTherePrevKit)! == true {
                vw.labelKitNum.text = vw.myBleSuMan!.prevKitName
            } else {
                vw.labelKitNum.text = langStr.obj.register_kit
            }
        }
    }

    override func viewWillAppear(animated: Bool) {
        setLanguageString()
        calibration관련세팅()
    }
    
    override func viewWillDisappear(animated: Bool) {
        uiTimer.invalidate()
        for vw in arrView {
            vw.theTimer?.invalidate()
        }
    }

    func update() {
        //bttnAssignStudent.enabled = 0 < HsBleMaestr.inst.connectionNumber()
        if 0 < HsBleMaestr.inst.connectionNumber() {
            bttnAssignStudent.backgroundColor = colorBttnActiveBlue
        } else {
            bttnAssignStudent.backgroundColor = colorBttnDarkGray
        }
    }

    func calibration관련세팅() {
        if isCalibration {
            bttnAssignStudent.hideMe()
        }
    }

    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if buttonIndex == 0 {
            self.navigationController?.popViewControllerAnimated(viewAnimate)
        }
    }

    
    // MARK:  언어 세팅.
    func setLanguageString() {
        view.backgroundColor = HmGraphSetting.inst.viewBgBrightGray
        if isCalibration {
            labelTitle.text = localStr("calibration")
        } else {
            if isAdult {
                labelTitle.text = langStr.obj.adult_title
            } else {
                labelTitle.text = langStr.obj.infant_title
            }
        }
        bttnAssignStudent.setTitle(langStr.obj.assign_student, forState: .Normal)      //localStr("assign_student_conn"), forState: .Normal)
        bttnQuit.setTitle(langStr.obj.quitArrow, forState: .Normal) //localStr("bttn_quit_leftarrow"), forState: .Normal)
    }

}


