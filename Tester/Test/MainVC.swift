//
//  MainVC.swift
//  Test
//
//  Created by Jongwoo Moon on 2016. 1. 19..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation



class MainVC : UIViewController, UIActionSheetDelegate {
    var log = HtLog(cName: "MainVC")

    @IBOutlet weak var labelVersion: UILabel!
    @IBOutlet weak var bttnInfo: UIButton!

    @IBOutlet weak var bttnAdult: UIButton!
    @IBOutlet weak var bttnInfant: UIButton!
    @IBOutlet weak var bttnDataManage: UIButton!

    @IBAction func bttnActInfo(sender: AnyObject) {
        openHeartisenseInfoWeb(1)// Monitor
    }

    @IBAction func bttnDebug(sender: AnyObject) {
//        let viewController:UIViewController = UIStoryboard(name: "testOftest", bundle: nil).instantiateViewControllerWithIdentifier("TestViewMain") as UIViewController
//        self.presentViewController(viewController, animated:viewAnimate, completion: nil)


        bttnDebug.cornerRad(5)
    }


    @IBAction func bttnActSetting(sender: AnyObject) {
        let actSheet = UIActionSheet(title: nil, delegate: self, cancelButtonTitle: nil,
            destructiveButtonTitle: nil,
            otherButtonTitles:  langStr.obj.option_preference, langStr.obj.option_calibration, langStr.obj.bls_setting)
        actSheet.showInView(self.view)
    }

    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int) {
        log.printThisFunc("actionSheet_delegate ::   buttonIdx : \(buttonIndex)")
        if buttonIndex == 0 {
            let vc = InitialSettingVC(nibName: "InitialSettingVCPort", bundle: nil) //("InitialSettingVC")
            dispatch_async(dispatch_get_main_queue(), {
                self.presentViewController(vc, animated:viewAnimate, completion: nil)
            })
        }
        if buttonIndex == 1 {  // 캘리브레이션 세팅.
            let vc = ConnectVC()
            vc.isCalibration = true
            HsBleMaestr.inst.setConnectionMode(true)
            dispatch_async(dispatch_get_main_queue(), {
                self.navigationController?.pushViewController(vc, animated:viewAnimate)
            })  
        }
        if buttonIndex == 2 {
            let vc = ScoreOptionVC()
            dispatch_async(dispatch_get_main_queue(), {
                self.navigationController?.pushViewController(vc, animated: viewAnimate)
            })
        }
    }

    @IBAction func bttnActAdult(sender: AnyObject) {
        isAdult = true        //buttonColorChange()
        startEducation()
    }

    @IBAction func bttnActInfant(sender: AnyObject) {
        isAdult = false
        startEducation()
    }

    func startEducation() {
        log.printThisFNC("bttnActStartEducation", comment: " ")
        let vc = ConnectVC(nibName: "ConnectVC", bundle: nil)
        vc.isCalibration = false
        HsBleMaestr.inst.setConnectionMode(false)
        self.navigationController?.pushViewController(vc, animated:viewAnimate)
    }

    @IBAction func bttnActManageStudent(sender: AnyObject) {
        log.printThisFNC("bttnActManageStudent", comment: " ")
        let vc = ManageStudentOver(nibName: "ManageStudentVC", bundle: nil)
        self.navigationController?.pushViewController(vc, animated:viewAnimate)
    }

    override func viewWillAppear(animated: Bool) {
        setLanguageString()
    }

    override func viewDidDisappear(animated: Bool) {
        for b in HsBleMaestr.inst.arrBleSuMan {
            b.closePort()
        }
    }

    @IBOutlet weak var bttnDebug: UIButton!
    override func viewDidLoad() {
        AppIdTrMoTe = 2 // 테스트로 세팅.. 이게 싱글톤들 부르기 전에 있어야..
        // App Dependent 세팅...
        log.printAlways("\n\n\n   Main VC   viewDidLoad    AppIdTrMoTe = \(AppIdTrMoTe)  TimeStamp : \(NSDate(timeIntervalSinceNow: 0).formatYYMMDDspaceTime) \(HsBleMaestr.inst.arrStudent.count)  \n\n\n")
        //print(" Student count ;;   \(HsBleMaestr.inst.arrStudent.count)")

        if isDebugMode { bttnDebug.hidden = false }
        setLanguageString()
        labelVersion.text = testVersion
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)

        HsGlobal.delay(0.5) { 
            // self.testProcess()
        }
        //print("  proto N \(cprProtoN.theVal)")
    }

    // MARK:  언어 세팅.
    func setLanguageString() {
        bttnAdult.setTitle(langStr.obj.adult, forState: .Normal)
        bttnInfant.setTitle(langStr.obj.infant, forState: .Normal)

        bttnDataManage.setTitle(langStr.obj.manage_data, forState: UIControlState.Normal)
    }
    


}
