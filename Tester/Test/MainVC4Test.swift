//
//  MainVC4Test.swift
//  HS Test
//
//  Created by Jongwoo Moon on 2016. 11. 16..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation


extension MainVC {

    func testProcess() {

        let sigg = HsBleMaestr.inst
        print(" \(#function)   \(HsBleMaestr.inst.arrBleSuMan.count) \(HsBleMaestr.inst.arrStudent.count) ")

        sigg.readDB()

        print(" \(#function)   \(HsBleMaestr.inst.arrBleSuMan.count) \(HsBleMaestr.inst.arrStudent.count) ")

        let inf = HmTestInft()
        inf.stdntID = 1

        inf.dicStep1["안전확인"] = true

        let bleMan = HsBleMaestr.inst.arrBleSuMan.first!

        bleMan.curStudent = HsBleMaestr.inst.arrStudent.first!
        bleMan.testObj = inf

        bleMan.saveInfantRescure(true)
        bleMan.saveAdultRescure(true)

        inf.stdntID = 2
        bleMan.saveInfantRescure(true)
        bleMan.saveInfantRescure(true)
        
        bleMan.saveAdultRescure(true)
        bleMan.saveAdultRescure(true)


        inf.stdntID = 2
        bleMan.saveInfantRescure(true)
        bleMan.saveAdultRescure(true)
        bleMan.saveInfantRescure(true)
        bleMan.saveAdultRescure(true)



    }

    func test2() {
        let dObj = HsData()

        var aSet = HmUnitSet()
        aSet.ccPassCnt = 23;         aSet.ccCount = 28
        aSet.rpPassCnt = 1;         aSet.rpCount = 2
        aSet.ccTime = 15;            aSet.holdTime = 6
        dObj.arrSet.append(aSet)
        aSet = HmUnitSet()
        aSet.ccPassCnt = 25;        aSet.ccCount = 26
        aSet.rpPassCnt = 2;         aSet.rpCount = 2
        aSet.ccTime = 14;           aSet.holdTime = 6
        dObj.arrSet.append(aSet)

        let discrem = dObj.discreminant()
        print("\n\n\n  \t\t\t res ult : \(discrem) \n\n\n")
    }

    func test1() {
        let dObj = HsData()

        var aSet = HmUnitSet()
        aSet.ccPassCnt = 23;         aSet.ccCount = 28
        aSet.rpPassCnt = 1;         aSet.rpCount = 2
        aSet.ccTime = 13;            aSet.holdTime = 12
        dObj.arrSet.append(aSet)
        aSet = HmUnitSet()
        aSet.ccPassCnt = 20;        aSet.ccCount = 25
        aSet.rpPassCnt = 2;         aSet.rpCount = 2
        aSet.ccTime = 20;           aSet.holdTime = 15
        dObj.arrSet.append(aSet)

        let discrem = dObj.discreminant()
        print("\n\n\n  \t\t\t res ult : \(discrem) \n\n\n")
    }
}
