//
//  AppDelegate.h
//  Test
//
//  Created by Jongwoo Moon on 2016. 1. 19..
//  Copyright © 2016년 IMLab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

