//
//  ManageStudentCell.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2016. 1. 4..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation



class ManageStudentCell : UITableViewCell, UIAlertViewDelegate {
    var log = HtLog(cName: "ManageStudentCell")
    var curStudent:HmStudent?

    var selectedCallBack: (HmStudent, Bool)->() = {obj, isOn in  }
    var deleteTargetedStudent: ()->() = { }
    var editTargetedStudent: ()->() = { }
    var showGraph: (HmStudent)->() = {hmst in  }

    @IBOutlet weak var imgCheck: UIImageView!

    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelEmail: UILabel!
    @IBOutlet weak var bttnEdit: UIButton!
    @IBOutlet weak var bttnDelete: UIButton!
    @IBOutlet weak var bttnGraph: UIButton!

    @IBAction func bttnActGraph(sender: AnyObject) {
        showGraph(curStudent!)
    }

    @IBAction func bttnActEdit(sender: AnyObject) {
        log.logUiAction("  셀 수정  ")
        editTargetedStudent()
    }

    @IBAction func bttnActDelete(sender: AnyObject) {
        log.logUiAction("  셀 Delete  ")
        let altVw = UIAlertView(title: localStr("delete"), message: localStr("want_delete"), delegate: self,
            cancelButtonTitle: localStr("yes"), otherButtonTitles: localStr("no"))
        altVw.show()

    }
    override func awakeFromNib() {

    }


    override func layoutSubviews() {

    }

    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {

        log.printAlways(" AlertViewDelegate ::   index : \(buttonIndex) ")

        if buttonIndex == 0 { // delete  :  yes
            curStudent!.uiTargeted = true
            deleteTargetedStudent()
        }
    }


    func setSelection(isYes: Bool) {
        if isYes {
            imgCheck.image = UIImage(named: "btn_check")
        } else {
            imgCheck.image = UIImage(named: "btn_check_not")
        }
    }
    //
    //    @IBAction func switchValueChanged(sender: UISwitch) {
    //        print("  Touched ::  value is \(sender.on)")
    //
    //        selectedCallBack(curStudent!, swtActive.on)
    //
    //    }

    func setStudent(stu: HmStudent) {
        curStudent = stu
        labelName.text = curStudent?.name
        labelEmail.text = curStudent?.email
        bttnGraph.show_다음이_참이면(curStudent?.isDataInDB)
    }

    func baseSet() {
        imgCheck.image = UIImage(named: "btn_check_not")
        labelName.text = "John Doe"
        labelEmail.text = "dkdkdk@asdj.net"
    }
    
    
}