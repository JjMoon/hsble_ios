//
//  AssignStudntCell.swift
//  Test
//
//  Created by Jongwoo Moon on 2016. 1. 21..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation

class AssignStudntCellTest : UITableViewCell {
    var log = HtLog(cName: "AssignStudentCell")
    var curStudent: HmStudent?
    var curTestObj: HmTest?
    var isMain: Bool {
        set { curStudent!.isMain = newValue }
        get { return curStudent!.isMain }
    }
    var isSub: Bool {
        set { curStudent!.isSub = newValue }
        get { return curStudent!.isSub }
    }
    var isCellSelected: Bool {
        get {
            return isMain || isSub
        }
    }

    var bleName = "", mainSub = ""  // bleman 은 뷰에서 지정해 줘야 함.

    var eventCallBack: (HmStudent, AssignStudntCellTest)->() = { studnt, mySelf in }
    var isThereRoomCallBack: (Bool)->(Bool) = { (isMain) in return true }

    //@IBOutlet weak var swtActive: UISwitch!

    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var bttnR1: UIButton!
    @IBOutlet weak var bttnR2: UIButton!

    @IBOutlet weak var labelR1: UILabel!
    @IBOutlet weak var labelR2: UILabel!

    @IBOutlet weak var labelResult: UILabel!

    override func awakeFromNib() {

    }

    //////////////////////////////////////////////////////////////////////     _//////////_     [     ]    _//////////_   Bttn Touch Actions
    // MARK:  버튼 터치 이벤트 처리
    @IBAction func bttnActR1() {
        log.logUiAction("  Main 버튼 ....  isMain : \(isMain)")
        isMain = !isMain
        if isMain && isThereRoomCallBack(true) == false { return }
        if isMain { isSub = false }
        if isMain {  curStudent?.setMainOrSubInTester(isMain) } // 메인 으로 설정.
        else { curStudent?.releaseTester() } // nil 로 설정..
        refresh()
    }

    @IBAction func bttnActR2() {
        log.logUiAction("  Sub 버튼 ....  isSub : \(isSub)")
        isSub = !isSub
        if isSub && isThereRoomCallBack(false) == false { return }
        if isSub { isMain = false }
        if isSub {  curStudent?.setMainOrSubInTester(false) } // sub 로 설정..
        else { curStudent?.releaseTester() } // nil 로 설정..
        refresh()
    }

    func delselectStudent(stdnt: HmStudent?) {
        if stdnt == nil { return }
        if stdnt == curStudent! {
            isMain = false
            isSub = false
            refresh()
        }
    }

    func cancelAllSelection() { // 메인뷰에서 콜..
        isMain = false
        isSub = false
        curStudent?.bleMan = nil // 선택 해제해줘야 헷갈리지 않지..
        refresh()
    }

    func setResult() {
        if isMain {
            mainSub = "1"
            curStudent?.isMainTester = true
        }
        if isSub {
            mainSub = "2"
            curStudent?.isMainTester = false
        }
        if isCellSelected {
            if curStudent?.bleMan != nil {
                bleName = (curStudent?.bleMan?.name)!
            }
        } else {
            bleName = ""; mainSub = ""
            curStudent?.isMainTester = nil
        }
        labelResult.text = bleName + "-" + mainSub
    }

    func setStudent(stu: HmStudent) {
        curStudent = stu
        labelName.text = curStudent?.name
        backgroundColor = HmGraphSetting.inst.viewBgBrightGray

        reset()
        setCheckImage()
        setResult()

        //print("\n  name : \(curStudent?.name)" + "    setStudent : \(curStudent?.arrTestObjDB)   ")

        if isAdult {
            curTestObj = curStudent!.getAdultFromDB()
        } else {
            curTestObj = curStudent!.getInfantFromDB()
        }
        setPassFail()
        checkProtocol()
    }

    func checkProtocol() {
        if curStudent!.activeForStandard() { return }
        // 현재 기준이 아닐 때 Disable...
        reset()
        bttnR1.hideMe(); bttnR2.hideMe()
        labelResult.text = curStudent!.getStandardStr()
        let _ = [ labelResult, labelName ].map { (lbl) in
            lbl.textColor = colBttnGray // UIColor.lightGrayColor()
        }
    }

    func setPassFail() {
        if curTestObj == nil {
            return
        }
//        if curTestObj!.isRes1 {
//            print("curTestObj!.isRes1")
//            if curTestObj!.passRes1 {
//                labelR1.text = "P"
//                bttnR1.hideMe()
//            } else {
//                labelR1.text = "F"
//            }
//            labelR1.textColor = colorGreenRedBy(curTestObj!.passRes1)
//        }
//        if curTestObj!.isRes2 {
//            print("curTestObj!.isRes2")
//            if curTestObj!.passRes2 {
//                labelR2.text = "P"
//                bttnR2.hideMe()
//            } else {
//                labelR2.text = "F"
//            }
//            labelR2.textColor = colorGreenRedBy(curTestObj!.passRes2)
//        }
    }

    private func reset() {
        labelR1.text = "-"
        labelR1.textColor = UIColor.blackColor()
        bttnR1.showMe()
        labelR2.text = "-"
        labelR2.textColor = UIColor.blackColor()
        bttnR2.showMe()
    }

    private func refresh() {
        log.printThisFNC("", comment: " 이미지 바꾸고.. 결과 반영하고 콜백 .... ")
        reset()
        setCheckImage()
        setResult()
        eventCallBack(curStudent!, self)
        setPassFail()
        checkProtocol()
    }

    private func setCheckImage() {
        if isMain {
            bttnR1.setImage(UIImage(named: "btn_check"), forState: .Normal)
        } else {
            bttnR1.setImage(UIImage(named: "btn_uncheck"), forState: .Normal)
        }
        if isSub {
            bttnR2.setImage(UIImage(named: "btn_check"), forState: .Normal)
        } else {
            bttnR2.setImage(UIImage(named: "btn_uncheck"), forState: .Normal)
        }
    }


}
