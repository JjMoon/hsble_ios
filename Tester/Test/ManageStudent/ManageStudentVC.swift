//
//  ManageStudentVC.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2015. 11. 18..
//  Copyright © 2015년 IMLab. All rights reserved.
//

import Foundation
import UIKit
import PSAlertView

class ManageStudentVC : UIViewController, UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate {
    var log = HtLog(cName: "ManageStudentVC")
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelAddStudent: UILabel!
    @IBOutlet weak var bttnSend2Server: UIButton!
    @IBOutlet weak var bttnDelete: UIButton!
    
    // 선택해서 기록 전송.
    // 아이디가 없는 학생
    // 측정 안 한 학생 > 오른쪽 그래프 보는 버튼이 없슴.
    // 측정 하고 안 보낸 학생
    // 서버에 보낸 학생 > 안 보임.
    
    @IBOutlet weak var tableStudent: UITableView!
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    
    @IBAction func bttnGoHomeTouched(sender: UIButton) {
        sender.setImage(UIImage(named: "btn_bottom_home_press"), forState: UIControlState.Normal)
    }
    
    @IBAction func bttnGoHome(sender: UIButton) {
        sender.setImage(UIImage(named: "btn_bottom_home_unpress"), forState: UIControlState.Normal)
    }
    
    @IBAction func bttnSend2Server(sender: AnyObject) {
        log.logUiAction("   서버에 업로드  ", lnum: 10)
        if 0 < instructorID {
            log.logThis("  Has ..  Instructor ID : \(instructorID) ")
            let vc = SendDataVC(nibName: "SendData", bundle: nil)
            presentViewController(vc, animated: true, completion: nil)
        } else {
            let vc = LoginVC(nibName: "Login", bundle: nil)
            vc.modalPresentationStyle = .FormSheet // .Popover
            presentViewController(vc, animated: true, completion: nil)
            vc.popoverPresentationController?.sourceView = view
            vc.popoverPresentationController?.sourceRect = view.frame
            vc.setLanguageString()
        }
    }
    
    @IBAction func bttnAddStudent(sender: AnyObject) {
        log.logUiAction("   학생   추가  이름의 길이 : \(txtName.text?.getLength())", lnum: 5)
        if txtName.text?.getLengthUTF32() == 0 { return }
        
        // email validation check
        if HsBleMaestr.inst.addStudent(txtName.text!, em: txtEmail.text!, pw: "pwpw", phn: "000-000", ag: 11) {
            log.printAlways(" Add Student ... OK  \(HsBleMaestr.inst.arrStudent.count) ", lnum: 5)
            tableStudent.reloadData()
        } else {
            HtSimpleUI.ToastWithTitle("Woops !!", msg: "Existing Name  !! ", cancelTtl: nil)
            log.printAlways(" Add Student  fail ", lnum: 5)
        }
        view.endEditing(true)
        txtName.text = ""; txtEmail.text = ""
    }
    
    @IBAction func bttnActDelete(sender: UIButton) {
        deleteTargetedStudents()
    }

    //////////////////////////////////////////////////////////////////////     _//////////_     [     ]    _//////////_   Go to Main View
    // MARK:  view will ...  did load ....
    override func viewDidLoad() {
        let nib = UINib(nibName: "ManageStudentCell", bundle: nil)
        tableStudent.registerNib(nib, forCellReuseIdentifier: "manageStudentCell")
        HsBleMaestr.inst.readDB()
        setLanguageString()
    }

    override func viewWillAppear(animated: Bool) {
        log.printThisFNC("viewWillAppear", comment: "studentsWithData = nil   리셋 ..  ")
        studentsWithData = nil
        tableStudent.reloadData()
    }
    
    func editStudentPopup() {
        let vc = StudentInfo(nibName: "StudentInfo", bundle: nil)
        vc.modalPresentationStyle = .FormSheet // .Popover
        presentViewController(vc, animated: true, completion: nil)
        vc.popoverPresentationController?.sourceView = view
        vc.popoverPresentationController?.sourceRect = view.frame
    }

    func deleteTargetedStudents() {
        HsBleMaestr.inst.deleteStudents()
        tableStudent.reloadData()
    }

    func showGraph(sObj: HmStudent) {
        let vc = ResultGraphVC(nibName: "ResultGraph", bundle: nil)
        vc.dbStudent = sObj
        presentViewController(vc, animated: true, completion: nil)
    }
    
    //////////////////////////////////////////////////////////////////////     _//////////_     [     ]    _//////////_   Go to Main View
    // MARK:  메인으로 돌아가기.
    @IBAction func bttnGo2Home(sender: AnyObject) {
        let altVw = UIAlertView(title: localStr("to_main"), message: localStr("want_main"), delegate: self,
            cancelButtonTitle: localStr("yes"), otherButtonTitles: localStr("no"))
        altVw.show()
    }

    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if buttonIndex == 0 {
            self.navigationController?.popViewControllerAnimated(true)
        }
    }

    func goHome() {
        self.navigationController?.popToViewController((self.navigationController?.viewControllers[1])!, animated: true)
    }

    var studentsWithData: [HmStudent]? // 이거 반복할 필요 없다.
    func getStudentsWithData() -> [HmStudent] {
        if studentsWithData == nil {
            studentsWithData = HsBleMaestr.inst.arrStudent.filter({ (std) -> Bool in
                return std.isDataInDB
            })
        }
        return studentsWithData!
    }

    // MARK:  table view delegate, data ..
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return HsBleMaestr.inst.arrStudent.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        //log.printAlways("  Assign Student : \(indexPath.row) ", lnum: 5)
        let cell:ManageStudentCell = tableView.dequeueReusableCellWithIdentifier("manageStudentCell", forIndexPath:indexPath) as! ManageStudentCell
        let student = HsBleMaestr.inst.arrStudent[indexPath.row]
        cell.setStudent(student)
        
        cell.deleteTargetedStudent = { self.deleteTargetedStudents() }
        cell.editTargetedStudent = { self.editStudentPopup() }
        cell.showGraph = { student in self.showGraph(student) }
        cell.bringSubviewToFront(cell.bttnGraph)
        cell.bringSubviewToFront(cell.bttnEdit)
        cell.bringSubviewToFront(cell.bttnDelete)
        
        return cell
    }
    
    @IBAction func editBttnAction(sender: UIButton) {
        log.logUiAction(" cell button ")
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let curCell = tableStudent.cellForRowAtIndexPath(indexPath) as! ManageStudentCell
        curCell.setSelection(true)
        //curCell.swtActive.setOn(true, animated: true)
        curCell.curStudent!.uiTargeted = true
        
        bttnDelete.hidden = false
    }
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        let curCell = tableStudent.cellForRowAtIndexPath(indexPath) as! ManageStudentCell
        curCell.setSelection(false)
        
        curCell.curStudent!.uiTargeted = false
        
        if HsBleMaestr.inst.numOfUiTargetedStudents() == 0 {
            bttnDelete.hidden = true
        }
    }


    // MARK:  언어 세팅.
    func setLanguageString() {
        labelTitle.text = localStr("adminstudent_title")
        labelAddStudent.text = localStr("adminstudent_addstudent")
        bttnSend2Server.setTitle(localStr("adminstudent_senddata"), forState: UIControlState.Normal)
        bttnDelete.setTitle(localStr("delete"), forState: UIControlState.Normal)
    }



    
}