//
//  AssignStudents.swift
//  Test
//
//  Created by Jongwoo Moon on 2016. 1. 21..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation
import UIKit

/**
 arrMain, arrSub : 4개의 HmStudent 로 UI에서 보여지는 학생 저장.
 할당이 끝나면 bleSuMane 의 curStudent, curStudentSub 에 Student 객체 할당.
 학생은 자기가 할당된 bleman 도 갖고 있슴.
**/

class AssignStudentTestVC : HtViewController, UIAlertViewDelegate, SearchViewPrtcl {
    var log = HtLog(cName: "AssignStudentView")

    /// SearchViewPrtcl
    var searchTxtField: UITextField?
    var searchMode = false
    var searchConstraintWidth: NSLayoutConstraint?
    var searchedList = [HmStudent]()

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var tableStudent: UITableView!
    @IBOutlet weak var labelStudentInfo: UILabel!
    @IBOutlet weak var labelSelectR1: UILabel!
    @IBOutlet weak var labelSelectR2: UILabel!
    @IBOutlet weak var labelAssignResult: UILabel!

    @IBOutlet weak var labelA: UILabel!
    @IBOutlet weak var labelB: UILabel!
    @IBOutlet weak var labelC: UILabel!
    @IBOutlet weak var labelD: UILabel!

    @IBOutlet weak var labelAName: UILabel!
    @IBOutlet weak var labelBName: UILabel!
    @IBOutlet weak var labelCName: UILabel!
    @IBOutlet weak var labelDName: UILabel!

    @IBOutlet weak var labelA2Name: UILabel!
    @IBOutlet weak var labelB2Name: UILabel!
    @IBOutlet weak var labelC2Name: UILabel!
    @IBOutlet weak var labelD2Name: UILabel!

    @IBOutlet weak var bttnFinish: UIButton!
    @IBOutlet weak var bttnCancel: UIButton!

    /// Sorting...
    var sortByDate = true, sortAscend = false
    @IBOutlet weak var bttnSortDate: SortBttn!
    @IBOutlet weak var bttnSortName: SortBttn!

    @IBAction func sortTouched(sender: SortBttn) {
        sender.sortTouched()
        sortByDate = sender.accessibilityIdentifier! == "date"
        sortAscend = sender.isAscend
        sortAction()
    }

    func sortFromSearchProtocol(union: [HmStudent]) {
        searchedList = sortStudentArrayBy(sortByDate, ascn: sortAscend, union: studentList)
    }

    func sortAction() {
        HsBleMaestr.inst.arrStudent =
            sortStudentArrayBy(sortByDate, ascn: sortAscend, union: HsBleMaestr.inst.arrStudent)
        sortFromSearchProtocol(studentList)
        reloadTableView()
    }

    var arrMain: [HmStudent?] = [nil, nil, nil, nil], arrSub: [HmStudent?] = [nil, nil, nil, nil] // 항상 4개..
    var curCell: AssignStudntCellTest?, arrCell = [AssignStudntCellTest]()
    var arrBleOnLine: [HsBleSuMan?] = [nil, nil, nil, nil]

    var studentList: [HmStudent] {
        get {
            if searchMode { return searchedList
            } else {        return HsBleMaestr.inst.arrStudent }
        }
    }

    @IBAction func bttnFinished(sender: AnyObject) {
        HsBleMaestr.inst.showStudentsTest()

        if testInfant {
            let validNum = arrMain.filter({ (sObj) -> Bool in return sObj != nil }).count +
            arrSub.filter({ (sObj) -> Bool in return sObj != nil }).count
            if 0 < validNum {
                goToOperationVC()
            }
            else {
                let altVw = UIAlertView(title: langStr.obj.at_least_1_connect, message: nil, delegate: self,
                    cancelButtonTitle: nil)
                altVw.show()
                HsGlobal.delay(3.0, closure: { () -> () in
                    altVw.dismissWithClickedButtonIndex(0, animated: true)
                })
            }
            return
        }

        if HsBleMaestr.inst.connectionNumber() == 0 { //        if HsBleMaestr.inst.connectionNumber() != 0 {
            let altVw = UIAlertView(title: langStr.obj.at_least_1_connect, message: nil, delegate: self,
                cancelButtonTitle: nil)
            altVw.show()
            HsGlobal.delay(3.0, closure: { () -> () in
                altVw.dismissWithClickedButtonIndex(0, animated: true)
            })
        } else {
            goToOperationVC()
        }
    }

    func goToOperationVC() {
        showGeneralPopup(nil, message: langStr.obj.want_assign_completed, yesStr: langStr.obj.yes, noString: langStr.obj.no, yesAction:
            { () -> Void in
                for (idx, ble) in HsBleMaestr.inst.arrBleSuMan.enumerate() {
                    ble.curStudent = self.arrMain[idx]
                    ble.curStudent?.myData = ble.dataObj // HSData. Adult case.. ?
                    ble.curStdntSub = self.arrSub[idx]
                    print(" AssignStudent >>  student : \(ble.curStudent?.name)  sub student : \(ble.curStdntSub?.name)  ")
                }
                let vc =  OperationVC(nibName: "OperationVC", bundle: nil)
                self.navigationController?.pushViewController(vc, animated: viewAnimate)
        })
    }

    @IBAction func bttnCancel(sender: AnyObject) {
        showGeneralPopup(nil, message: langStr.obj.want_back, yesStr: langStr.obj.yes, noString: langStr.obj.no, yesAction:
            { () -> Void in
                self.navigationController?.popToViewController((self.navigationController?.viewControllers[1])!, animated: viewAnimate)
        })
    }

    //////////////////////////////////////////////////////////////////////     [   검색 관련    << >> ]

    @IBOutlet weak var constSearchWidth: NSLayoutConstraint!
    @IBOutlet weak var bttnSearch: UIButton!
    @IBOutlet weak var txtSearch: UITextField!

    @IBAction func bttnActSearch(sender: UIButton) { // 탐색 기능 토글..
        searchMode = !searchMode
        searchUIAnimation()
        if searchMode {
            txtSearch.becomeFirstResponder()
        } else { self.view.endEditing(true) }
        refreshSearchTextAction({ (std) -> Bool in
            return std.name.containsString(self.txtSearch.text!) || std.email.containsString(self.txtSearch.text!)
            }, totalList: HsBleMaestr.inst.arrStudent)
    }

    @IBAction func searchTextChangedAction(sender: UITextField) {
        print("searchTextChangedAction  length : \(sender.text!.getLength())   \(sender.text!.getLengthUTF32())")

        refreshSearchTextAction({ (std) -> Bool in
            return std.name.containsString(self.txtSearch.text!) || std.email.containsString(self.txtSearch.text!)
            }, totalList: HsBleMaestr.inst.arrStudent)
    }


    //////////////////////////////////////////////////////////////////////     [   viewDidLoad    << >> ]
    override func viewDidLoad() {
        print("  Assign Student View    view Did Load")
        HsBleMaestr.inst.readDB()

        let nib = UINib(nibName: "AssignStudntCell", bundle: nil)
        tableStudent.registerNib(nib, forCellReuseIdentifier: "assignStudentCell")
        labelTitle.text = "Assign Students ... "

        view.backgroundColor = HmGraphSetting.inst.viewBgBrightGray
        tableStudent.backgroundColor = HmGraphSetting.inst.viewBgBrightGray

        let arrUiEle: [[UILabel]]  = [ [labelA, labelAName, labelA2Name], [labelB, labelBName, labelB2Name ],
            [labelC, labelCName, labelC2Name ], [labelD, labelDName, labelD2Name ] ]
        
        for (idx, obj) in HsBleMaestr.inst.arrBleSuMan.enumerate() {
            if obj.isConnectionTotallyFinished || testInfant {
                arrBleOnLine[idx] = obj
            } else {
                for uiObj in arrUiEle[idx] {
                    uiObj.hideMe()
                }
            }
        }

        labelAName.text = "..";        labelBName.text = ".."
        labelCName.text = "..";        labelDName.text = ".."
        labelA2Name.text = "..";       labelB2Name.text = ".."
        labelC2Name.text = "..";       labelD2Name.text = ".."

        tableStudent.allowsSelection = false
        tableStudent.allowsMultipleSelection = true

        /// Search Protocol
        searchTxtField = txtSearch
        searchConstraintWidth = constSearchWidth
    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        HsBleMaestr.inst.reloadStudentData = { print(" \(#file)  \(#function)  cljr ") }
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

        txtSearch.hideMe()
        setLanguageString()
        searchUIAnimation()
        HsBleMaestr.inst.reloadStudentData = self.reloadTableView

        /// sorting initial action
        bttnSortDate.initDateNameState(bttnSortName)
        sortAction()
    }

    override func viewDidAppear(animated: Bool) {
        timerInterval = 0.8 // second
        super.viewDidAppear(animated) // 타이머 세팅.
    }

    override func update() {
        super.update()

        if !isAdult { return }

        if HsBleMaestr.inst.connectionNumber() == 0 { // 앞 화면으로 돌아가기.
            uiTimer.invalidate() // 이게 없으면 여러번 뜬다..
            let altVw = UIAlertView(title: langStr.obj.at_least_1_connect, message: nil, delegate: self,
                cancelButtonTitle: nil)
            altVw.show()
            HsGlobal.delay(3.0, closure: { () -> () in
                altVw.dismissWithClickedButtonIndex(0, animated: viewAnimate)
                self.navigationController?.popViewControllerAnimated(viewAnimate)
            })
            return
        }
        checkBleConnections()
    }

    func deselectStudentInCell(stdnt: HmStudent?) {
        if stdnt == nil { return }
        for cll in arrCell {
            cll.delselectStudent(stdnt)
        }
    }

    private func checkBleConnections() {
        let arrABCD = [ labelA, labelB, labelC, labelD ]

        for idx in 0...3 {
            let bObj = HsBleMaestr.inst.arrBleSuMan[idx]
            let labelShow = !arrABCD[idx].hidden
            //let stdnt = arrSelectedStudents[idx]

            if bObj.isConnectionTotallyFinished == false &&
                (labelShow || arrMain[idx] != nil || arrSub[idx] != nil)  { // 끊겼는데 할당되어 있을 때..
                    log.printAlways("   끊겼는데 할당되어 있을 때..  ")
                    deselectStudentInCell(arrMain[idx])
                    deselectStudentInCell(arrSub[idx])
                    removeTheStudent(arrMain[idx])
                    removeTheStudent(arrSub[idx])
                    arrBleOnLine[idx] = nil
                    arrMain[idx] = nil
                    arrSub[idx] = nil
                    kitNameLabelShowHide()
            }
        }
    }
    
    private func kitNameLabelShowHide() {
        let arrMan = HsBleMaestr.inst.arrBleSuMan
        if !arrMan[0].isConnected { labelA.hidden = true; labelAName.hidden = true; labelA2Name.hideMe() }
        if !arrMan[1].isConnected { labelB.hidden = true; labelBName.hidden = true; labelB2Name.hideMe() }
        if !arrMan[2].isConnected { labelC.hidden = true; labelCName.hidden = true; labelC2Name.hideMe() }
        if !arrMan[3].isConnected { labelD.hidden = true; labelDName.hidden = true; labelD2Name.hideMe() }
    }

    // cell 에서 불리는 콜백..
    func isThereRoomFor(isMain: Bool) -> Bool {
        var curArray = [HmStudent?]()
        if isMain { curArray = arrMain }
        else { curArray = arrSub }

        let onlineNum = arrBleOnLine.filter { (ble) -> Bool in return ble != nil }.count
        let assignedNum = curArray.filter { (studnt) -> Bool in return studnt != nil }.count

        //print (" return     ble 연결 수 : \(onlineNum)   >  할당 학생수 : \(assignedNum)   ???  ")

        return onlineNum > assignedNum
    }

    /// cell 의 콜백함수.
    func refreshTable(clickedStudent: HmStudent, pCell: AssignStudntCellTest) { // cell 의 refresh 에서 불려짐.
        log.printThisFNC("refreshTable", comment: " refresh Table  \(clickedStudent.name)    isMain? : \(clickedStudent.isMainTester)  ")
        curCell = pCell
        removeTheStudent(clickedStudent)
        if clickedStudent.isMainTester == nil { // 선택에서 완전 해제..
            // 어레이에서 제거 : 항상 가능
            removeTheStudent(clickedStudent)
        } else if clickedStudent.isMainTester == true {
            // 메인 리스트에 추가 :
            if let bleRtn = setStudentInABCD(clickedStudent, theArray: &arrMain) {
                bleRtn.curStudent = clickedStudent
            } else { pCell.cancelAllSelection() } // 못 넣었을 때 }
        } else {
            // 서브 리스트에 추가
            if let bleRtn = setStudentInABCD(clickedStudent, theArray: &arrSub) {
                bleRtn.curStdntSub = clickedStudent
            } else { pCell.cancelAllSelection() }
        }
        setNames()
    }

    func removeTheStudent(theStudent: HmStudent?) {
        if theStudent == nil { return }
        removeStudentInArray(theStudent, theArray: &arrMain)
        removeStudentInArray(theStudent, theArray: &arrSub)
    }

    func removeStudentInArray(student: HmStudent?, inout theArray: [HmStudent?]) {
        for (idx, obj) in theArray.enumerate() {
            //if obj != nil && obj?.dbid == student?.dbid && obj?.name == student?.name {
            if obj != nil && obj?.dbid == student?.dbid && obj! == student! { // 이름으로 비교 못함..
                print(" remove object : \(obj!.name)  dbid : \(obj!.dbid)")
                theArray[idx] = nil
            }
        }
    }

    /// 빈 곳에 학생을 채워 넣기...
    func setStudentInABCD(student: HmStudent?, inout theArray: [HmStudent?]) -> HsBleSuMan? {
        log.printThisFNC("setStudentInABCD", comment: " 인수 array.count : \(theArray.count)")
        let assignedStudentNum = theArray.filter { (sobj) -> Bool in return sobj != nil }.count
        let online = arrBleOnLine.filter { (ble) -> Bool in return ble != nil }
        log.logThis("  studentNum : \(online.count) <= \(assignedStudentNum)  ")

        if online.count <= assignedStudentNum { return nil } // 못 넣는다. 캔슬 해야..

        for (idx, obj) in theArray.enumerate() {
            if obj == nil && arrBleOnLine[idx] != nil { // 첫번째 nil 을 찾으면.
                theArray[idx] = student
                student?.bleMan = HsBleMaestr.inst.arrBleSuMan[idx]
                curCell?.setResult()

                print ("  할당 >>  student : \(student!.name) ")
                return (student?.bleMan)!
            }
        }
        return nil
    }

    func setNames() {
        log.printThisFNC("setNames", comment: " ~~~~~~~~~~~~~~~~~  ~~~~~~~~~~~~~~  ~~~~~~~~~~~~~~ ")
        let arrMainL = [ labelAName, labelBName, labelCName, labelDName ]
        let arrSubL = [ labelA2Name, labelB2Name, labelC2Name, labelD2Name ]

        for (idx, sobj) in arrMain.enumerate() {
            if sobj == nil {
                arrMainL[idx].text = ".."
            } else {
                arrMainL[idx].text = sobj!.name
            }
        }
        for (idx, sobj) in arrSub.enumerate() {
            if sobj == nil {
                arrSubL[idx].text = ".."
            } else {
                arrSubL[idx].text = sobj!.name
            }
        }
    }

    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if buttonIndex == 0 {
            //self.navigationController?.popViewControllerAnimated(true)
        }
    }
    /// Xib 에서 multi cell selection 을 선택 안해서 고생.. ㅠㅠㅠ
    func reloadTableView() {
        if UI_USER_INTERFACE_IDIOM() == .Phone {
            //tableStudent.rowHeight = 30;
        }
        bttnSearch.setImageAccordingTo(searchMode, trueImg: "search_close.png", falseImg: "search.png")
        tableStudent.reloadData()
    }

    // MARK:  언어 세팅.
    func setLanguageString() {
        //view.backgroundColor = HmGraphSetting.inst.viewBgBrightGray
        labelTitle.text = langStr.obj.assign_student
        labelStudentInfo.text = langStr.obj.student_name
        labelSelectR1.text = langStr.obj.rescuer + " 1"  // "R1 " + langStr.obj.select
        labelSelectR2.text = langStr.obj.rescuer + " 2"  // "R2 " + langStr.obj.select
        labelAssignResult.text = langStr.obj.assign_result

        bttnFinish.setTitle(langStr.obj.start_test, forState: .Normal)
        bttnCancel.setTitle(langStr.obj.cancel, forState: .Normal)
    }
}

extension AssignStudentTestVC : UITableViewDataSource, UITableViewDelegate {

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("tableView: UITableView, numberOfRowsInSection : \(studentList.count)")
        // #warning Incomplete implementation, return the number of rows

        return studentList.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        print(" AssignStudentView ::  Assign Student : \(indexPath.row) ")
        let cell:AssignStudntCellTest = tableView.dequeueReusableCellWithIdentifier("assignStudentCell", forIndexPath:indexPath) as! AssignStudntCellTest

        let student = studentList[indexPath.row]

        cell.setStudent(student)
        cell.bringSubviewToFront(cell.bttnR1)
        cell.bringSubviewToFront(cell.bttnR2)

        cell.eventCallBack = refreshTable
        cell.isThereRoomCallBack = isThereRoomFor
        //cell.labelResult.text = "-"

        let tapped:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "TappedOnSwitch:")
        tapped.numberOfTapsRequired = 1
        //cell.swtActive.addGestureRecognizer(tapped)

        arrCell.append(cell)

        return cell
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        log.printAlways("   did Select at \(indexPath.row) ")
        let curCell = tableStudent.cellForRowAtIndexPath(indexPath) as! AssignStudntCellTest
        curCell.setSelected(false, animated: false)
    }

    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        print(" de Selected :: at \(indexPath.row)")
    }

}


