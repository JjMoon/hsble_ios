//
//  AppDelegate.m
//  Test
//
//  Created by Jongwoo Moon on 2016. 1. 19..
//  Copyright © 2016년 IMLab. All rights reserved.
//

#import "AppDelegate.h"
#import <AVFoundation/AVFoundation.h>
#import "Common-Swift.h"


@interface AppDelegate ()

@end

@implementation AppDelegate

AVAudioPlayer *audioPlayer;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {


    NSLog(@"  AppDelegate >>   shl >>> ");

    HsBleSingle *singltn = HsBleSingle.inst;
    NSLog(@"  AppDelegate >>   shl >>> HsBleSingle ");
    //[singltn setLanguageString];
    NSLog(@"  AppDelegate >>   shl >>> setLanguage ");


    UIColor *temp = HmGraphSetting.inst.graphRed;
    NSLog(@"  AppDelegate >>   shl >>> GraphSetting.. ");

    NSString *path = [NSString stringWithFormat:@"%@/AudioFiles/logo.mp3", [[NSBundle mainBundle] resourcePath]];

    NSURL *soundUrl = [NSURL fileURLWithPath:path];


    audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];

    NSLog(@"Sound>>> %@ %@ %@", path, soundUrl, audioPlayer);
    NSLog(@"Sound>>> %@", audioPlayer);

    [audioPlayer play];

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.

    NSLog(@"  applicationWillEnterForeground  ");
    [HsBleMaestr.inst readDB];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    NSFileManager *filemgr = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSLog(@"documentsDirectory  : %@", documentsDirectory);

    NSString* inboxPath = [documentsDirectory stringByAppendingPathComponent:@"Inbox"];
    NSLog(@"inboxPath  : %@", inboxPath);

    NSArray *dirFiles = [filemgr contentsOfDirectoryAtPath:inboxPath error:nil];

    NSLog(@"  Call   >>>   [ReadCommaSeperatedText parseFiles:dirFiles dr:inboxPath]; ");
    [ReadCommaSeperatedText parseFiles:dirFiles dr:inboxPath];

    return true;
}


@end
