//
//  ViewController.m
//  heartisense
//
//  Created by jeongyuchan on 2015. 4. 20..
//  Copyright (c) 2015년 gyuchan. All rights reserved.
//

#import "Common-Swift.h" // $(SWIFT_MODULE_NAME)-Swift.h
#import "ViewController.h"
#import "HSDataStaticValues.h"
#import "NSObject+Util.h"
#import "HsBleManager.h"
#import "HsBleSingle.h"

#import "StepByStepTrialViewController.h"
#import "SearchDeviceListView.h"
#import "HSEnumSet.h"
#import "HSDataCalculator.h"
#import "DPLocalizationManager.h"
#import "KLCPopup.h"
#import "PSPDFAlertView.h"
#import "MBProgressHUD.h"

#import "InitialSettingVCTrn.h"

@interface ViewController () <IGLDropDownMenuDelegate> {
    HtLog *log;
    MBProgressHUD *HUD;
    SearchDeviceListView *deviceListView;
    KLCPopup* popup;
    
    NSTimer* uiTimer;
    MuState *curStepWatch;
    NSArray *arrSensorCmdMsgKey; // = @[@"", @""];    //NSArray *nameList = @[@"HS_01"];
    HsBleManager* bleMan;
    BOOL isTherePreviousDevice;
    StrLocBase *lObj;
    //TrainerSignalView *testSignal;
}

@property (strong, nonatomic) NSMutableArray *bleList;
@property (strong, nonatomic) HSPeripheral *hsPeripheral;
@property (strong, nonatomic) HSBaseService *hsBaseService;

@end

//////////////////////////////////////////////////////////////////////     _//////////_     [ ViewController ]    _//////////_
@implementation ViewController
//////////////////////////////////////////////////////////////////////////////////////////
- (void)viewDidLoad {
    [super viewDidLoad];
    [self logCallerMethodwith:@"" newLine:5];

    _labelVersion.text = @"V 1.10.04"; // 버전 표시

    UIColor *temp = HmGraphSetting.inst.graphRed; // 이거 나중에 없애자..
    //[HsBleSingle.inst setLanguageString];
    lObj = HsBleSingle.inst.langObj;
    NSLog(@"\n\n\n  Shl :: ViewController ...  viewDidLoad >>>  langIDx : %d  %@ \n\n\n", langIdx, lObj.yes);

    NSLog(@" screenSize >> %f %f", self.view.frame.size.width, self.view.frame.size.height);



    bleMan = HsBleManager.inst;
    log = [[HtLog alloc] initWithCName:@" ViewController 메인 뷰 " active: true];
    __weak typeof(self) this = self;

    //블루투스 리스트 팝업 뷰 정의
    _bleList = [bleMan arrBle];  // MOOON : MVC new manager
    
    bleMan.goToSelectionModeView = ^(void) {

        this.imgVwLoading.hidden = false; // [HUD hide:NO] ;

        SelectModeVC *dvc = [[SelectModeVC alloc] initWithNibName:@"SelectModeVC" bundle:nil];
        [this.navigationController pushViewController:dvc animated:viewAnimate];
    };
    
    // 센서 확인 실패 오류 메시지.
    arrSensorCmdMsgKey = @[@"idx0", @"battery", @"sensor_piezo", @"sensor_accel", @"sensor_atm", @"ble_num1", @"ble_num2"];

    bleMan.productTypeErrorEvent = ^ {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                       message:HsBleSingle.inst.langObj.productTypeErrorExit
                                                                preferredStyle:UIAlertControllerStyleAlert];
        [this presentViewController:alert animated:viewAnimate completion:nil];
        [HsGlobal delay:4.0 closure:^{
            exit(0);
        }];
    };

    bleMan.exceptionProc = ^(int command, int value) {
        [this logCallerMethodwith:[NSString stringWithFormat:@" UIAlertController exceptionProc ^ >>>   Error >>  com : %d, value : %d", command, value]
                          newLine:20];
        NSString *strMsg, *sensor;
        if (command == 1 && value == -1) strMsg = @"Low Battery";  //strMsg = @"battery_under_25";
        sensor = arrSensorCmdMsgKey[command];
        
        if (value == 1) strMsg = lObj.connect_fail;          //strMsg = @"connect_fail";
        if (value == 2) strMsg = lObj.sensor_error_detected; //strMsg = @"sensor_error_detected";

        UIAlertController *alert = [UIAlertController alertControllerWithTitle:HsBleSingle.inst.langObj.error message:strMsg
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        if (HsBleManager.inst.isConnectionTotallyFinished) return;  // 연결이 되었다면..  띄우지 않고
        
        [this presentViewController:alert animated:viewAnimate completion:nil];
        
        [HsGlobal delay:2.0 closure:^{
            [alert dismissViewControllerAnimated:viewAnimate completion:nil];
        }];
    };
    
    NSArray *dataArray = @[@{@"image":@"sun.png",@"title":@"English"},
                           @{@"image":@"clouds.png",@"title":@"한국어"}];
    NSMutableArray *dropdownItems = [[NSMutableArray alloc] init];
    for (int i = 0; i < dataArray.count; i++) {
        NSDictionary *dict = dataArray[i];
        
        IGLDropDownItem *item = [[IGLDropDownItem alloc] init];
        [item setIconImage:[UIImage imageNamed:dict[@"image"]]];
        [item setText:dict[@"title"]];
        [dropdownItems addObject:item];
    }

    // 기기 선택 ..  Button Setting
    _curBttn = nil;
    [self changeButtonSetInit:true orFinalError:false];
    // 기존 기기에 연결... 버튼 세팅..
    [self searchButtonSetInit:true orFinalError:false];
}

- (void)viewDidAppear:(BOOL)animated{
    [self logCallerMethodwith:@"  view Did Appear " newLine:5];
    [self buttonEnable:true];
}

- (void)viewWillAppear:(BOOL)animated{
    [self logCallerMethodwith:@"  HsBleManager.inst reset  " newLine:20];

    lObj = HsBleSingle.inst.langObj;

    isTherePreviousDevice = [[NSUserDefaults standardUserDefaults] stringForKey:@"PreviousDeviceNum"] != nil;
    _imgVwLoading.hidden = true;
    [_imgVwLoading rotate360Degrees:1.5 repeatCnt:999999999 completionDelegate:nil];
    [bleMan reset];

    // 기기 선택 ..  Button Setting
    _curBttn = nil;
    [self changeButtonSetInit:true orFinalError:false];
    // 기존 기기에 연결... 버튼 세팅..
    [self searchButtonSetInit:true orFinalError:false];

    uiTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self
                                             selector:@selector(uiUpdateAction) userInfo:nil repeats:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self logCallerMethodwith:@"  [uiTimer invalidate];  " newLine:10];
    [uiTimer invalidate];
    uiTimer = nil;
}


//////////////////////////////////////////////////////////////////////     _//////////_     [ UI || UX         << >> ]    _//////////_
#pragma mark -  기기 변경 버튼    이벤트....
- (IBAction)bttnActChangeKit:(UIButton *)sender { // 기기 변경 버튼    이벤트....
    [self logCallerMethodwith:@" \t\t\t << SearchFlow01 >>" newLine:10];
    _curBttn = sender;
    [self selectDevice];
}

- (void)selectDevice { // 키트 선택 창 오픈 ...
    [self connectButtonTouchedCommonAction];

    [bleMan connect:^(void){  //      BleManager 의 함수 콜..
        [self setUISearchButtons];
    }
      andPopupBlock:^{  // block
          [self showDeviceSelectPopupView];
      }
     ];  // MOOON : MVC new manager
    [self setUISearchButtons];
}


//////////////////////////////////////////////////////////////////////     _//////////_     [ Search Button Touched .. ]    _//////////_
#pragma mark -  기존에 연결된 키트에 재접속.
- (void)bttnReconnectPreviousDevice:(id)sender  // it's aka IBAction  기존에 연결된 키트에 재접속.
{
    [self logCallerMethodwith:@"searchButtonTouched  \t\t\t << SearchFlow01 >>" newLine:3];
    _curBttn = (UIButton*)sender;
    if (!isTherePreviousDevice) { // 기존 기기가 없으면 이 버튼이 change 팝업을 띄워야 ...
        [self selectDevice];
        return;
    }

    [self connectButtonTouchedCommonAction];
    [self progressAnimation];
    
    [bleMan connectPreviousDevice:[[NSUserDefaults standardUserDefaults] stringForKey:@"PreviousDeviceNum"] withFailBlock:  //  BleManager 의 함수 콜..  아래가 failureBlock()
     ^{  // 앞에서 연결됐는지 이미 확인한다..
         NSLog(@"\n\n\n   ViewController ::  Fail Block  of  connectPreviousDevice   \n\n\n");
         _imgVwLoading.hidden = true; //[HUD hide:true];
         
         UIAlertView *alertVw; // 실패 팝업....
         alertVw = [[UIAlertView alloc] initWithTitle:lObj.fail
                                              message:lObj.search_fail //   [self local Str:@"kit_search_fail"]
                                             delegate:self
                                    cancelButtonTitle:lObj.confirm //[self local Str:@"confirm"]
                                    otherButtonTitles:nil];
         [alertVw show];
         [bleMan stopScan]; // 151125 이거 괜찮나?
     }];
}

- (void)connectButtonTouchedCommonAction {  //  접속 할 때 공통 부분 기능...
    [self logCallerMethodwith:@"connectButtonTouchedCommonAction  << 접속 할 때 공통 부분 기능 >>" newLine:3];

    __weak typeof(self) weakSelf = self;
    bleMan.timerErrorBlock = ^(void) { // 접속 오류 팝업...
        NSLog(@"\n\n\n  bleMan.timerErrorBlock 실행.....  \n\n\n");
        _curBttn = nil;
        [weakSelf searchButtonSetInit:false orFinalError:true]; // 에러 경우..
        [weakSelf changeButtonSetInit:false orFinalError:true];
    };
    [bleMan stopScan];
    [bleMan operationStop];
    [self buttonEnable:false];
}


- (void)uiUpdateAction {
    //[curStepWatch compareThis_Once:(int)bleMan.bleState willPrint:true]; // 비교 한번...

    if (bleMan.connState == Initial && _bttnInfo.hidden == true) { // 꺼져 있다...
        [_bttnSetting showMe]; [_bttnInfo showMe];
    }
    if (bleMan.connState != Initial && _bttnInfo.hidden == false) { // 꺼져 있다...
        [_bttnSetting hideMe]; [_bttnInfo hideMe];
    }
    
    if (_curBttn) { [_curBttn setTitle:[bleMan bleButtonText] forState:UIControlStateNormal];  }
    
    if (!isBypassMode)    return;  // 바이패스가 아니면 여기서 나가..
}


- (void)checkPreferenceValuesFromUserDefaults {
    NSUserDefaults *dObj = [NSUserDefaults standardUserDefaults];
    isManikinLeft = [dObj boolForKey:@"ManikinDirection"];
    if ([dObj integerForKey:@"StageLimit"])
        HsBleSingle.inst.stageLimit = [dObj integerForKey:@"StageLimit"];
    if ([dObj integerForKey:@"Language"])
        langIdx = [dObj integerForKey:@"Language"];
}

//////////////////////////////////////////////////////////////////////     _//////////_     [ UI || UX         << >> ]    _//////////_
#pragma mark - 이전 기기 재접속 버튼 모양 세팅
- (void)searchButtonSetInit:(BOOL)isInit orFinalError:(BOOL)finalError {
    [self buttonEnable:false];
    [bleMan reset];

    if (isInit) {
        NSString* prevDeviceNum = [[NSUserDefaults standardUserDefaults] stringForKey:@"PreviousDeviceNum"];

        //NSLog(@"\n\n\n  Shl :: Regist Kit :: %@ \n\n", lObj.register_kit);

        if (prevDeviceNum == nil) {
            [_searchButton setTitle:lObj.register_kit forState:UIControlStateNormal];
        } else {
            [_searchButton setTitle:lObj.connect_kit forState:UIControlStateNormal];
        }
        //[self logCallerMethodwith:[NSString stringWithFormat:@" 기존 기기에 연결 버튼 세팅 >> PreviousDeviceNum ::  %@ ", prevDeviceNum] newLine:5];
        [_searchButton addTarget:self action:@selector(bttnReconnectPreviousDevice:) forControlEvents:UIControlEventTouchUpInside];
        [_searchButton.titleLabel setNumberOfLines:2];
        [_searchButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
        [_searchButton.layer setCornerRadius:5.f];
    }
    
    if (finalError) {
        _imgVwLoading.hidden = true; //[HUD hide:true];
        [bleMan stopScan];
        
        UIAlertView *alertView;
        alertView = [[UIAlertView alloc] initWithTitle:lObj.connect_fail
                                               message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:nil];
        [alertView show];
        [bleMan stopScan]; // 151125 이거 괜찮나?
        
        [HsGlobal delay:2.0 closure:^{ // 2초 후에 뷰 닫기.
            NSLog(@"   [HsGlobal delay:2.0 closure:^{ // 2초 후에 뷰 닫기.   ");
            [self buttonEnable:true];
            [alertView dismissWithClickedButtonIndex:0 animated:YES];
            //[bleMan stopScan];
        }];
    }
}
//  이건 키트 변경 버튼 모양...
- (void)changeButtonSetInit:(BOOL)init orFinalError:(BOOL)finalError {
    if (init || finalError) {
        [_bttnChangeKit setTitle:lObj.change_kit_1 forState:UIControlStateNormal];
    }
}

- (void)setPrevKitID {
    if (isTherePreviousDevice) {
        NSString *prevName = [[NSUserDefaults standardUserDefaults] stringForKey:@"PreviousDeviceNum"];
        _labelKitID.text = [NSString stringWithFormat:@"%@ : %@",
                            lObj.kit_id, [prevName substringFromIndex:4] ];
    } else {
        _labelKitID.text = @" ";
    }
}


- (void)buttonEnable:(BOOL)able {
    NSLog(@" bttnEnable >>>  able : %d  %d ", able, bleMan.connState);
    [self setPrevKitID];

    if (isTherePreviousDevice) {
        [_searchButton setTitle:lObj.connect_kit forState:UIControlStateNormal];
    } else {
        [_searchButton setTitle:lObj.register_kit forState:UIControlStateNormal];
    }

    if (able) {
        //[_bttnInfo showMe]; [_bttnSetting showMe];
        [_searchButton setBackgroundColor:[UIColor colorWithRed:231/255.f green:64/255.f blue:56/255.f alpha:1.f]];
    } else {
        // 색깔을 disable 처럼 보이게...
        [_searchButton setBackgroundColor:[UIColor colorWithRed:0.7f green:0.7f blue:0.7f alpha:0.8f]];
    }
    _searchButton.enabled = able;
    _bttnChangeKit.enabled = able;
    _bttnChangKitArrow.hidden = _bttnChangeKit.hidden = !isTherePreviousDevice; // 기존 기기가 없으면 .. 이놈을 가리고 저놈이 대신한다.
}

- (void)setUISearchButtons { // 해당 버튼의 텍스트를 ... searching ..  찾는 중...  으로 표시..
    NSString* bttnText = [bleMan bleButtonText];
    //[self logCallerMethodwith:[NSString stringWithFormat:@"Change Button Text with : %@ ", bttnText] newLine:1];
    [_curBttn setTitle:bttnText forState:UIControlStateNormal];
}

//////////////////////////////////////////////////////////////////////       [ Search Button -->  Popup View ]
- (void)showDeviceSelectPopupView
{
    [self logCallerMethodwith:@"" newLine:5];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    // Show in popup
    UIView* contentView = [[UIView alloc] init];
    contentView = [[UIView alloc]initWithFrame:CGRectMake(50.f, 50.f, 400.f, 250.f)];
    deviceListView = [[SearchDeviceListView alloc]initWithFrame:CGRectMake(0.f, 0.f, contentView.frame.size.width, contentView.frame.size.height)];
    
    [deviceListView setBlockAndBleMan:bleMan]; // 모니터 땜시..  이거 해줘야 함..
    
    [deviceListView setBackgroundColor:[UIColor whiteColor]];
    [deviceListView setLstVwDelegate:self];  // 여기서 딜리깃을 세팅해 줌..  뷰 끼리 주고받는 거니..  그냥 놔둔다.
    [bleMan setTempViewObj:deviceListView];  // 여기서 매니저의 뷰를 세팅해 줌.
    [contentView addSubview:deviceListView];
    [deviceListView setLogHeader:@"___   \t SearchDevice ListView  \t:: \t>>   "];
    KLCPopupLayout layout = KLCPopupLayoutMake(KLCPopupHorizontalLayoutCenter, KLCPopupVerticalLayoutCenter);
    popup = [KLCPopup popupWithContentView:contentView
                                  showType:KLCPopupShowTypeFadeIn
                               dismissType:KLCPopupDismissTypeFadeOut
                                  maskType:KLCPopupMaskTypeDimmed
                  dismissOnBackgroundTouch:YES
                     dismissOnContentTouch:NO];
    [popup showWithLayout:layout];
    //__weak ViewController *this = self;
    popup.didFinishDismissingCompletion = ^(void) { // 팝업이 없어진 후의 액션..
        
        if (bleMan.connState != ConnectionStarted) {
            [self searchButtonSetInit:true orFinalError:false];
            _curBttn = nil;
        }
        
        [_bttnChangeKit setTitle:lObj.change_kit_1 forState:UIControlStateNormal];

        NSLog(@" popup.didFinishDismissingCompletion = ^(void) { // 팝업이 없어진 후의 액션.. ");
        [self buttonEnable: bleMan.connState == Initial ];  // true ..
        
        [self logCallerMethodwith:@" ViewController : showPopupView  Completion Block ...  " newLine:5];
        //   요건 팝업창에서 부르는 거니까.. 그냥 무시하자... [this setUISearchButtons];
    };
}

//////////////////////////////////////////////////////////////////////     _//////////_     [ Device Selection Button Touched .. ]    _//////////_
//////////////////////////////////////////////////////////////////////       [ Delegate  methods ]
#pragma mark - 기기가 선택되면 팝업 닫고 후속 작업 시작
- (void)connectDeviceResult:(HSPeripheral*)peripheral hsService:(HSBaseService*)service {  // 기기 선택 창 닫는 부분...  이거 나중에 딴데로 옮길 것...
    _hsPeripheral = peripheral;    _hsBaseService = service;
    [self logCallerMethodwith:@"<<  SelectDeviceFlow08  >> ~from~ SearchDeviceListView => Delegate ~~" newLine:10];

    // 뺑뺑이 돌리고 device 체크로 넘어간다.
    [self progressAnimation];
    [popup dismiss:YES];
}

-(void) progressAnimation {
    _imgVwLoading.hidden = false; // [HUD show:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - IGLDropDownMenuDelegate

- (void)dropDownMenu:(IGLDropDownMenu *)dropDownMenu selectedItemAtIndex:(NSInteger)index
{
    //IGLDropDownItem *item = dropDownMenu.dropDownItems[index];
    //    /*       * index 0 : english     * index 1 : korean     */
    
    langIdx = index;
    [self setUISearchButtons];
}

//////////////////////////////////////////////////////////////////////////////////////////

-(void) updateDeviceListView:(HSCentralManager*)centralManager
{
    [deviceListView setDeviceList:centralManager.ymsPeripherals];
    [deviceListView reloadListData];
}

//////////////////////////////////////////////////////////////////////     _//////////_     [ Info,   Setting Buttons . ]    _//////////_
#pragma mark -  세팅 ...   인포   등 기타 버튼..

- (IBAction)bttnActionSetting:(UIButton *)sender { // 세팅 버튼...

    UIActionSheet *setAct = [[UIActionSheet alloc]
                             initWithTitle:nil delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil
                             otherButtonTitles:lObj.option_preference, lObj.option_calibration, nil];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        setAct = [[UIActionSheet alloc]
                  initWithTitle:lObj.select_menu delegate:self cancelButtonTitle:lObj.cancel destructiveButtonTitle:nil
                  otherButtonTitles:lObj.option_preference, lObj.option_calibration, nil];
    }

    [setAct showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    //NSLog(@" actionSheet ::  at %d", buttonIndex);
    
    if (buttonIndex == 0) {
        InitialSettingVCTrn* vc = [[InitialSettingVCTrn alloc] initWithNibName:@"InitialSettingVC" bundle:nil];
        
        NSLog(@"  make v c");
        dispatch_async(dispatch_get_main_queue(), ^ {
            NSLog(@"  async  make v c");
            //[self.navigationController pushViewController:vc animated:viewAnimate];
            [self presentViewController:vc animated:viewAnimate completion:nil];
        });
    }
    if (buttonIndex == 1) {
        if (!bleMan.isConnected) {
            UIAlertView *alertView;
            alertView = [[UIAlertView alloc]
                         initWithTitle:lObj.need_connect_kit message:nil delegate:self
                         cancelButtonTitle:lObj.confirm otherButtonTitles:nil];
            [alertView show];
        } else {
            SensivitySettingVC* senseVC = [[SensivitySettingVC alloc] init];
            dispatch_async(dispatch_get_main_queue(), ^ {
                [self.navigationController pushViewController:senseVC animated:viewAnimate];
                //[self presentViewController:senseVC animated:viewAnimate completion:nil];
            });
        }
    }
}

- (IBAction)bttnActionInfomation:(UIButton *)sender {  //
    [self openHeartisenseInfoWeb:0]; // Trainer

//[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://heartisense.com/eng/heartisense/overview.php"]];
}

- (void)someVoidVoidTest
{
    NSLog(@"\n\n   -ViewController :: (void)someVoidVoidTest    \n\n\n");
}

@end


//3456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
//////////////////////////////////////////////////////////////////////       [ CLASSname ]


//로딩 HUD를 띄운다.  Pairing 메시지와 돌아가는 애니메이션.
//    HUD = [[MBProgressHUD alloc] initWithView:self.view];
//    [self.view addSubview:HUD];
//    HUD.delegate = self;
//    HUD.labelText = [self local Str:@"pairing"];
//    [HUD setDimBackground:YES];

