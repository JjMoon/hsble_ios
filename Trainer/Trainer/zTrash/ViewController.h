//
//  ViewController.h
//  heartisense
//
//  Created by jeongyuchan on 2015. 4. 20..
//  Copyright (c) 2015년 gyuchan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>

#import "HSCentralManager.h"
#import "IGLDropDownMenu.h"

#import "SearchDeviceListView.h"
#import "MBProgressHUD.h"


/**
 디바이스 검색 결과를 선택했을 때 액션을 받기 위한 SearchDeviceListViewDelegate Protocol 추가
 Progress HUD 로딩을 보여주기 위한 MBProgressHUDDelegate Protocol 추가
 */
@interface ViewController : UIViewController <UIActionSheetDelegate, SearchDeviceListViewDelegate, MBProgressHUDDelegate>
//<CBPeripheralDelegate,

{

}



- (void)buttonEnable:(BOOL)able;
- (void)changeButtonSetInit:(BOOL)init orFinalError:(BOOL)finalError;


- (IBAction)bttnActChangeKit:(UIButton *)sender;
@property (weak, nonatomic) UIButton *curBttn;
@property (weak, nonatomic) IBOutlet UIButton *bttnInfo;
@property (weak, nonatomic) IBOutlet UIButton *bttnSetting;

@property (weak, nonatomic) IBOutlet UIImageView *imgVwLoading;

//-(void) goToSelectionModeView;

-(void)updateDeviceListView:(HSCentralManager*)centralManager;

@property (weak, nonatomic) IBOutlet UIButton *searchButton;

@property (weak, nonatomic) IBOutlet UIButton *bttnChangeKit;
@property (weak, nonatomic) IBOutlet UIButton *bttnChangKitArrow;

@property (weak, nonatomic) IBOutlet UILabel *labelVersion;

@property (weak, nonatomic) IBOutlet UILabel *labelKitID;

@end

