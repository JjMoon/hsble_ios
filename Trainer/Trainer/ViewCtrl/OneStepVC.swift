//
//  OneStepVC.swift
//  heartisense
//
//  Created by Jongwoo Moon on 2015. 9. 17..
//  Copyright (c) 2015년 gyuchan. All rights reserved.
//

import UIKit
import AVFoundation


//////////////////////////////////////////////////////////////////////     _//////////_     [ One Step Process View Controller ]    _//////////_
class OneStepVC: UIViewController {
    @IBOutlet weak var imgVwBG: UIImageView!
    
    
    
    //@IBOutlet weak var progressVBar: M13ProgressViewBar!
    @IBOutlet weak var progressVBar: M13ProgressViewBar!

    var hsProg = HsProgress()
    var audioPlayer = AVAudioPlayer()

    var audioURL = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("school_kr", ofType: "mp3")!)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        do {
            try audioPlayer = AVAudioPlayer(contentsOfURL: audioURL)
        } catch {
            print("NO AUDIO PLAYER")
        }        
        
        audioPlayer.play()

        imgVwBG.image = UIImage(named: "bg_officee_scenario.jpg")
        HsGlobal.delay(3.0, closure: {
            self.imgVwBG.image = UIImage(named: "bg_officee_scenario_2.jpg")
            self.delayClsrChangeImage2()
        })
    
        //Setting ProgressView
        hsProg.countProgressView = progressVBar
        hsProg.initAction()
        
        

//        [_countProgressView setProgressDirection:M13ProgressViewBarProgressDirectionLeftToRight];
//        [_countProgressView setPercentagePosition:M13ProgressViewBarPercentagePositionTop];
//        [_countProgressView setPrimaryColor:[UIColor whiteColor]];
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //////////////////////////////////////////////////////////////////////     _//////////_     [ Delayed Closure ]    _//////////_
    // MARK:  ////////////////////////////   Delayed Closure ...  ..
    func delayClsrChangeImage2() {
        HsGlobal.delay(5.0, closure: {
            self.imgVwBG.image = UIImage(named: "bg_officee_scenario_3.jpg")
            self.someNext()
        })
    }

    func someNext() {
        
    }
    
    //////////////////////////////////////////////////////////////////////     _//////////_     [ Process ]    _//////////_
    // MARK:  ////////////////////////////   Delayed Closure ...  ..

    

}
