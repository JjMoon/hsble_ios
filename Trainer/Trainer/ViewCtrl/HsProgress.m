//
//  HsProgress.m
//  heartisense
//
//  Created by Jongwoo Moon on 2015. 9. 18..
//  Copyright © 2015년 gyuchan. All rights reserved.
//

#import <Foundation/Foundation.h>


#import "HsProgress.h"

//////////////////////////////////////////////////////////////////////     _//////////_     [ StepByStepTrialViewController ]    _//////////_
@implementation HsProgress
//////////////////////////////////////////////////////////////////////////////////////////

@synthesize countProgressView, progressView;

- (id)init {
    self = [super init];
    return self;
}

- (void)initAction {
    [countProgressView setProgressDirection:M13ProgressViewBarProgressDirectionLeftToRight];
    [countProgressView setPercentagePosition:M13ProgressViewBarPercentagePositionTop];
    [countProgressView setPrimaryColor:[UIColor whiteColor]];
    
    [countProgressView setProgress:11/30.f animated:YES];

}





@end