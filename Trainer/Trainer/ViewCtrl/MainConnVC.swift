//
//  MainConnVC.swift
//  Trainer
//
//  Created by Jongwoo Moon on 2016. 10. 24..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation
import Darwin

class MainConnVC: UIViewController, UIActionSheetDelegate, BleSearchPopupPrtc {
    var log = HtLog(cName: "MainConnVC")

    /// BleSearchPopupPrtc 관련 변수..
    var searchView: BleSearchVw?, conPrevKitObj = BleConnectPreviousKit(), basicProcssObj = HsInitialConnectSession()
    var popUpVw: KLCPopup?
    var disMissed: ((Void) -> Void)?, prevDeviceName: String?

    @IBOutlet weak var lblKitNum: UILabel!

    @IBOutlet weak var lblVersion: UILabel!

    @IBOutlet weak var imgLoadingSign: UIImageView!

    @IBOutlet weak var bttnMainConn: UIButton!
    @IBOutlet weak var bttnChangeKit: UIButton!
    @IBOutlet weak var bttnChangKitArrow: UIButton!


    //////////////////////////////////////////////////////////////////////     [ <<  Connection UI  >> ]
    var mainBttnCljr = { }
    var connTimer: NSTimer?

    @IBAction func actionConnMain(sender: AnyObject) {
        log.printAlways("actionConnMain", lnum: 5)
        mainBttnCljr()
    }
    @IBAction func actionChangeKit(sender: AnyObject) { actChangeKit() }
    @IBAction func actionChangeKitArrow(sender: AnyObject) { actChangeKit() }
    func actChangeKit() {
        selectKit()
    }

    @IBAction func actionSetting(sender: AnyObject) {
        let setAct = UIActionSheet(title: nil, delegate: self, cancelButtonTitle: nil, destructiveButtonTitle: nil,
                                   otherButtonTitles: langStr.obj.option_preference, langStr.obj.calibration)
        setAct.showInView(self.view)
    }

    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int) {
        switch buttonIndex {
        case 0:
            let setVC = InitialSettingVCTrn(nibName: "InitialSettingVC", bundle: nil)
            dispatch_async(dispatch_get_main_queue(), {
                self.presentViewController(setVC, animated: viewAnimate, completion: nil)
            })
        case 1:
            if trnBleCtrl.isPortClosed {
                let alertView = UIAlertView(title: langStr.obj.need_connect_kit, message: nil, delegate: self,
                                            cancelButtonTitle: langStr.obj.confirm)
                alertView.show()
            } else {
                let vc = SensivitySettingVC(nibName: "SensivitySettingVC", bundle: nil)
                dispatch_async(dispatch_get_main_queue(), {
                    self.presentViewController(vc, animated: viewAnimate, completion: nil)
                })
            }
        default:
            break
        }
    }

    @IBAction func actionInfo(sender: AnyObject) {
        openHeartisenseInfoWeb(0) //  self openHeartisenseInfoWeb:0]; // Trainer
    }

    //////////////////////////////////////////////////////////////////////     [   View    << >> ]
    override func viewDidLoad() {
        print("\n\n MainConnVC : viewDidLoad \n\n")

    }

    override func viewWillAppear(animated: Bool) {
        trnBleCtrl.initJob()
        trnBleCtrl.setStateOfVC = setState

        basicProcssObj.baseSetting(trnBleCtrl)

        basicProcssObj.productTypeErrorEvent = {  // 키트 productType 에러 시 팝업 후 꺼짐..
            self.showSimpleMessageWith(langStr.obj.productTypeErrorExit, yesCallBack: { () in
                exit(0)
            })
        }

        // 접속 후 액션..
        disMissed = afterConnectionProcess // 키트 변경 연결
        // conPrevKitObj.finishConnection = afterConnectionProcess // 이전 키트 연결.

        lblVersion.text = trainerVersion // 버전 표시

        // 로딩 사인  회전 애니메이션 및 초기화.
        imgLoadingSign.rotate360Degrees(repeatCnt: 99999999)
        imgLoadingSign.hideMe()

        // [bleMan connectPreviousDevice:[[NSUserDefaults standardUserDefaults] stringForKey:@"PreviousDeviceNum"] withFailBlock:  //

        prevDeviceName = NSUserDefaults.standardUserDefaults().stringForKey("PreviousDeviceNum")
        
        setInitialUiComponents()
    }

    override func viewDidAppear(animated: Bool) {

    }

    //////////////////////////////////////////////////////////////////////     [   << Connection >> ]

    func afterConnectionProcess() {
        self.imgLoadingSign.showMe()
        self.setState()

        // 임시로 써보자..
        if let prt = trnBleCtrl.thePort {
            NSUserDefaults.standardUserDefaults().setObject(prt.name, forKey: "PreviousDeviceNum")
        }

    }

    func conPrevKit() {
        log.printAlways("conPrevKit()   \(trnBleCtrl.conState) ", lnum: 10)
        buttonActivate(false)
        trnBleCtrl.conState = .None
        conPrevKitObj.connectWith(trnBleCtrl, kitName: prevDeviceName!)
    }

    func selectKit() {
        buttonActivate(false)
        showBleListPopup(trnBleCtrl)
    }

    private func setInitialUiComponents() {
        if prevDeviceName == nil {
            lblKitNum.text = ""
            bttnMainConn.setTitle(langStr.obj.register_kit, forState: .Normal)
            bttnChangeKit.hideMe(); bttnChangKitArrow.hideMe()
            mainBttnCljr = selectKit
        } else {
            lblKitNum.text = langStr.obj.kit_id + " : " + prevDeviceName!.subStringFrom(4)
            bttnMainConn.setTitle(langStr.obj.connect_kit, forState: .Normal)
            mainBttnCljr = conPrevKit
        }

        bttnMainConn.cornerRad(5)
        bttnMainConn.backgroundColor = colBttnRed
    }

    func buttonActivate(act: Bool) {
        if act {
            imgLoadingSign.hideMe()
            bttnMainConn.backgroundColor = colBttnRed
            bttnMainConn.activate(); bttnChangeKit.activate(); bttnChangKitArrow.activate()
        } else {
            imgLoadingSign.showMe()
            bttnMainConn.backgroundColor = colBttnBrightGray
            bttnMainConn.deactivate(); bttnChangeKit.deactivate(); bttnChangKitArrow.deactivate()
        }
    }

    func setState() {
        switch trnBleCtrl.conState {
        case .None:
            print("  case .None:  ")
            bttnMainConn.backgroundColor = colBttnRed
            buttonActivate(true)
        case .OpenStart:
            bttnMainConn.backgroundColor = colBttnBrightGray
        case .Connected:
            //bttnMainConn.backgroundColor = UIColor.brownColor()
            trnBleCtrl.futureConState = .BasicJobStarted
            basicProcssObj.startProcess() //  여기서 후속작업을 시작 시킨다...
            buttonActivate(false)
        case .ConnectFail:
            conPrevKitObj.cancelAction()
            basicProcssObj.cancelAction()
            trnBleCtrl.closePort() // 여기서 상태 변경..
        case .BasicJobStarted, .Cali: break

        case .Rest:            goToNextPage()
        case .Error: // 다시 활성화...
            if let msg = basicProcssObj.errorMsg {
                showSimpleMessageWith(msg, yesCallBack: { () in
                    trnBleCtrl.conState = .None
                })
            }
            basicProcssObj.cancelAction()
            buttonActivate(true)
        default:
            print("\(#file) :: \(#function) >>>   default : \(trnBleCtrl.conState)")
            //bttnMainConn.backgroundColor = UIColor.cyanColor()
        }
    }

    func goToNextPage() {
        log.printAlways("goto Select VC")
        trnBleCtrl.setStateOfVC = nil
        trnBleCtrl.dataReceived = nil

        basicProcssObj.timerAction()
        basicProcssObj.timerAction()

        if let prt = trnBleCtrl.thePort {
            NSUserDefaults.standardUserDefaults().setObject(prt.name, forKey: "PreviousDeviceNum")
        }
        let dvc = SelectModeVC(nibName: "SelectModeVC", bundle: nil)
        navigationController?.pushViewController(dvc, animated: viewAnimate)
    }
}



