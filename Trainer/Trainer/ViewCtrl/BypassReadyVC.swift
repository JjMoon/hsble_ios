//
//  BypassReadyVC.swift
//  heartisense
//
//  Created by Jongwoo Moon on 2015. 10. 15..
//  Copyright © 2015년 gyuchan. All rights reserved.
//

import Foundation
import UIKit
import PSAlertView

class BypassReadyVC: UIViewController {
    var log = HtLog(cName: "BypassReady VC")
    var uiTimer = NSTimer()
    var curStepWatch = MuState()
    var progressTimer = NSTimer()
    var ccLimitPointTemp:Float = 0, rpLimitPointTemp:Float = 0
    var stateWatch = MuState()
    
    var bypass: HsBypassManager?
    
    var dObj = trnBleCtrl.dataObj
    var currentStep : CurStep { get { return trnBleCtrl.stt }}
    
    // 20 - 1620,  50 - 1050
    // 나올 때 cc end point 에 저장.
    
    @IBOutlet weak var labelReady: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        log.logUiAction("viewDidLoad", lnum: 10)
        labelReady.text = langStr.obj.wait_instructor
        
        ccLimitPointTemp = Float(dObj.ccEndPoint - dObj.ccStaPoint)
        rpLimitPointTemp = Float(dObj.rpEndPoint - dObj.rpStaPoint)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        log.logUiAction("view Will Appear", lnum: 10)
        bypass = trnBleCtrl.bypass
        uiTimer = NSTimer.scheduledTimerWithTimeInterval(0.5, // second
            target:self, selector: #selector(BypassReadyVC.uiUpdateAction),
            userInfo: nil, repeats: true)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        uiTimer.invalidate()
    }
    
    func openOperationView() {
        let vc = OperationViewCtrl(nibName: "StepByStepTrialViewController", bundle: nil)

        vc.setProps(trnBleCtrl)

        navigationController?.pushViewController(vc, animated: viewAnimate)
    }
    
    var alert = PSPDFAlertView(title: nil, message: "")
    
    func checkBypassEnd() -> Bool {
        if currentStep == .S_QUIT || !isBypassMode { // 바이패스가 아니면 원래 Select 화면으로 감..
            log.printAlways("  currentStep ::  \(HsUtil.curStepStr())", lnum: 10)
            이뷰에서나가기()
            return true
        }
        return false
    }

    private func 이뷰에서나가기() {
        let vcs = navigationController?.viewControllers
        let vcNum = vcs?.count  //log.logThis("  vcNum : \(vcNum)", lnum: 2)
        navigationController?.popToViewController((vcs?[vcNum! - 2])!, animated: viewAnimate)
    }

    func uiUpdateAction() {
        if checkBypassEnd() == true { return }
        
        stateWatch.compareThis_Once(currentStep, willPrint: true)
        log.logThis("  App Info ??   \(bypass?.isMonitor.intVal)   isBypass : \(isBypassMode)   \(HsUtil.curStepStr())  ", lnum: 0)
        if bypass?.quitApplication() == true {
            //log.printThisFunc("  bypass?.quitApplication() == true  ", lnum: 5)
            var messge = ""
            if bypass?.isMonitor.intVal == 2 { messge = langStr.obj.bls_using }     // Test
            else {  messge = langStr.obj.calibration_using }                        // Calibration
            
            print("  \n\n\n\n\n  여기서 이유를 말하고 앱 종료.. \(messge) \n\n\n\n\n ")
            uiTimer.invalidate()
            alert = PSPDFAlertView(title: nil, message: messge)
            alert.alertViewStyle = UIAlertViewStyle.Default
            alert.show()
            
            HsGlobal.delay(3.0, closure: { () -> () in
                exit(0)
            })
        }
        
        
        if currentStep != .S_READY { openOperationView() }
    }
}
