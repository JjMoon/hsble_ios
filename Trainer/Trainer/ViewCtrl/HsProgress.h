//
//  HsProgress.h
//  heartisense
//
//  Created by Jongwoo Moon on 2015. 9. 18..
//  Copyright © 2015년 gyuchan. All rights reserved.
//

#ifndef HsProgress_h
#define HsProgress_h


#endif /* HsProgress_h */


#import "M13ProgressViewSegmentedBar.h"
#import "M13ProgressViewBar.h"


@interface HsProgress : NSObject

{
    __weak M13ProgressViewSegmentedBar *progressView;
    __weak M13ProgressViewBar *countProgressView;
}


- (void)initAction;


@property (weak, nonatomic) M13ProgressViewSegmentedBar *progressView;
@property (weak, nonatomic) M13ProgressViewBar *countProgressView;


@end

