//
//  HsGlobal.swift
//  heartisense
//
//  Created by Jongwoo Moon on 2015. 9. 16..
//  Copyright (c) 2015년 gyuchan. All rights reserved.
//

import Foundation
import UIKit

class HsGlobal : NSObject {
    // 압박  // 65 | 78 2016.5.23
    let foreGrndColors : [UIColor] = ([UIColor] (count: 35, repeatedValue: colorBarYelow)) +
        ([UIColor] (count: 12, repeatedValue: colorBarGreen)) + ([UIColor] (count: 13, repeatedValue: colorBarRedfg))
    let backGrndColors : [UIColor] = ([UIColor] (count: 35, repeatedValue: colorBarBgYlw)) +
        ([UIColor] (count: 12, repeatedValue: colorBarBgGrn)) + ([UIColor] (count: 13, repeatedValue: colorBarBgRed))
    // 호흡
    let rpForeGrndColors : [UIColor] =  ([UIColor] (count: 21, repeatedValue: colorBarYelow))
        + ([UIColor] (count: 21, repeatedValue: colorBarGreen)) + ([UIColor] (count: 18, repeatedValue: colorBarRedfg))
    let rpBackGrndColors : [UIColor] = ([UIColor] (count: 21, repeatedValue: colorBarBgYlw))
        + ([UIColor] (count: 21, repeatedValue: colorBarBgGrn)) + ([UIColor] (count: 18, repeatedValue: colorBarBgRed))
    // 카운트  30개로
    let countBG : [UIColor] = ([UIColor] (count: 120, repeatedValue: UIColor(red: 0.1, green: 0.1, blue: 0.1, alpha: 0.1)))
    let countFG : [UIColor] = ([UIColor] (count: 120, repeatedValue: UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1)))
    
    static func delay(delay:Double, closure:()->()) {  // 딜레이 함수..
        dispatch_after(
            dispatch_time(
                DISPATCH_TIME_NOW,
                Int64(delay * Double(NSEC_PER_SEC))
            ),
            dispatch_get_main_queue(), closure)
    }
}



class MuState : NSObject {
    var sName = "MuState"
    var isChanged = false
    var prevStep:CurStep = .S_INIT0
    
    var initJob:(Void)->Void = { }

    
    override init() {
        super.init()
    }

    convenience init(name:String) {
        self.init()
        self.sName = name
    }
    
    func setInitValueTo(newState:Bool) {
        //print ("  new State :  \(newState)")
        isChanged = newState
    }
    
    func compareThis_Once(curState:CurStep, willPrint:Bool) -> Bool {
        isChanged = prevStep != curState
        //isChanged = HsUtil.curStepStr(curState) != HsUtil.curStepStr(prevStep) // 스트링으로 비교..
        if isChanged && willPrint { print ("\n\n  State of < \(sName) >  is Changed  from \(HsUtil.curStepStr(prevStep)) \(HsUtil.curStepStr(curState) )  !!!  \n\n") }
        prevStep = curState
        return isChanged
    }
    
//    var initJob:(Void)->Void = { }
//    var compareState:(Void) -> Bool = { return false }
    
}