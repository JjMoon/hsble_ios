//
//  NSObject+Util.h
//  ObjectPainter
//
//  Created by Jongwoo Moon on 12. 1. 2..
//  Copyright (c) 2012년 jongwoomooon@gmail.com All rights reserved.
//
//  NSObject Category
//
//  mit : integer       mfo : float         mbl : bool      mgo : geometry
//  arr : array         dic : dictionary    
//  mdd : distance      man : ANima         mla : Layer     
//  mht : hansoo term   mgn : generator
//  AN : Anima          HT : HT Object      GN : Generator  



#import <Foundation/Foundation.h>


@interface NSObject (Util)



#pragma mark - Log Functions

// bit Calculation Utilities

- (UInt16)parseTwoBytes:(unsigned char*)data at:(int)idx;
- (UInt16)parseTwoBytes:(unsigned char)val1 and:(unsigned char)val2;
- (UInt16)parseOneByte:(unsigned char*)data at:(int)idx;
- (UInt16)getIntShift8:(unsigned char*)data at:(int)idx;
- (BOOL)isValueNotBiggerThan0x00:(Byte)dir shift:(int16_t)sht;

// Utilities.
-(NSString*)getSpace:(int)pSpaceNum; // 사이띄기 스트링 리턴..
-(void)specialMark:(NSString*)pComment ; //

- (NSString*)localStrXXX:(NSString*)key;
- (NSString*)formatDouble2f:(double)value;

// NSLog, String related Functions.
-(NSString*)getStringAt:(int)pIndex divider:(NSString*)pDivider inString:(NSString*)pStr;
// "12#34#56"  0:12, 1:34 returns.

-(void)logSpecialComment:(NSString*)pComment withSpace:(int)pSpaceNum;
-(void)logComment:(NSString*)pComment withSpace:(int)pSpaceNum;
-(void)logComment:(NSString*)pComment withInteger:(int)pInt of:(NSString*)pIntCmt 
        withSpace:(int)pSpaceNum;
-(void)logComment:(NSString*)pComment withFloat:(float)pVal of:(NSString*)pValCmt
        withSpace:(int)pSpaceNum;

-(void) logNewLine:(int)num;
-(void) logMarkWith:(NSString*)message;
-(void) logCallerMethod;
-(void) logCallerMethodwith:(NSString*)msg newLine:(int)num;

-(void)logMethodMark:(NSString*)pClassMethod andComment:(NSString*)pComment 
             isStart:(BOOL)pStart; // 함수 앞 뒤에서 마킹..
// 2015 // -(void)log2points:(CGPoint)pPt1 nPoint2:(CGPoint)pPt2 andComment:(NSString*)pComment ;

//-(void)logRetainCount:(NSString*)pComment withSpace:(int)pSpaceNum ;


// Random Utilities
-(int)randomIntFrom:(int)pMinValue To:(int)pMaxValue;
-(float)getAround:(float)pFloat anySign:(BOOL)pSign;
-(float)getAround:(float)pFloat inPrecision:(NSRange)pPrecision;
-(float)getAroundOf:(float)pValue withPercent:(int)pPercent;

/* // 2015 // -(UIColor*)randomNewColor;
-(UIColor*)getDarkColorUnderIn255:(int)pValue;
-(UIColor*)getBrightColorOverIn255:(int)pValue;
// 2015 //  */



@end
