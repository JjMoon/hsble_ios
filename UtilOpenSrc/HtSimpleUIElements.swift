//
//  HtSimpleUIElements.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2015. 11. 19..
//  Copyright © 2015년 IMLab. All rights reserved.
//

import Foundation


class HtSimpleUI: NSObject {
    
    
    static func ToastWithTitle(ttl: String?, msg: String?, cancelTtl: String?) {
        let alertVw = UIAlertView(title: ttl, message: msg, delegate: nil, cancelButtonTitle: cancelTtl)
        alertVw.show()
        HsGlobal.delay(3.0, closure: {
            alertVw.dismissWithClickedButtonIndex(0, animated: true)
        })
    }

    
}