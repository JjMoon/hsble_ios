//
//  HsUtil.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2016. 1. 14..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation



class HsUtil : NSObject {
    //       InitCalibration00, CalibrationView01, AllInOne10 = 10, StepByStep20 = 20, StandBy
    static func parseStr(bleObj: HsBleCtrl) -> String {
//        switch bleObj.parsingState {
//        case InitCalibration00 : return "InitCalibration00"
//        case CalibrationView01:  return "CalibrationView01"
//        case AllInOne10:         return "AllInOne10"
//        case StepByStep20:       return "StepByStep20"
//        case StandBy:            return "StandBy"
//        default:        return "parsingState Error"
//        }
        return ""
    }

    static func curStepLocalized(pStep: CurStep = globalState) -> String {
        //print(" HsUtil >>   \(pStep)  ")
        var curStep = pStep
        if AppIdTrMoTe == 0 { curStep = trnBleCtrl.stt }

        switch curStep {
        case .S_INIT0:                  return "S_INIT0"
        case .S_STEPBYSTEP_SAFETY:      return langStr.obj.check_danger_info_title
        case .S_STEPBYSTEP_AIRWAY:      return langStr.obj.open_airway_info_title
        case .S_STEPBYSTEP_CHECK_BRTH:  return langStr.obj.check_breath
        case .S_STEPBYSTEP_RESPONSE:    return langStr.obj.response
        case .S_STEPBYSTEP_EMERGENCY:   return langStr.obj.emergency
        case .S_STEPBYSTEP_CHECKPULSE:  return langStr.obj.check_pulse //("pulse_check")
        case .S_STEPBYSTEP_COMPRESSION, .S_WHOLESTEP_COMPRESSION:
            return langStr.obj.compression
        case .S_STEPBYSTEP_MULTI:       return langStr.obj.response
        case .S_STEPBYSTEP_BREATH, .S_WHOLESTEP_BREATH:
            return langStr.obj.breath
        case .S_STEPBYSTEP_AED, .S_WHOLESTEP_AED:
            return langStr.obj.aed
        case .S_WHOLESTEP_DESCRIPTION:  return "WHOLESTEP_DESCRIPTION"

        case .S_WHOLESTEP_RESULT:       return langStr.obj.result
        case .S_WHOLESTEP_PRECPR:       return langStr.obj.preCpr // local Str("precpr")

        case .S_READY:                  return langStr.obj.ready
        case .S_QUIT:                   return "S_QUIT"
        case .CAL_LOAD:                 return "CAL_LOAD"
        case .CAL_NEW_CC:               return "CAL_NEW_CC"
        case .CAL_NEW_RP:               return "CAL_NEW_RP"
        case .S_CALIBRATION:            return "S_CALIBRATION"
        case .S_COMPLETE:               return "S_COMPLETE"
        case .BypassStepComp:           return "BypassStepComp"
        case .BypassStepBrth:           return "BypassStepBrth"
        case .WholeStepStart:           return "WholeStepRestart"
        }
    }

/*  
     S_INIT0, S_STEPBYSTEP_RESPONSE = 3, S_STEPBYSTEP_EMERGENCY, S_STEPBYSTEP_CHECKPULSE,

     S_STEPBYSTEP_SAFETY, S_STEPBYSTEP_AIRWAY, S_STEPBYSTEP_CHECK_BRTH,

     S_STEPBYSTEP_COMPRESSION = 10, S_STEPBYSTEP_BREATH, S_STEPBYSTEP_AED, 
     
     BypassStepComp, BypassStepBrth,

     S_WHOLESTEP_DESCRIPTION = 15, S_WHOLESTEP_PRECPR, // 이때 one sensor data parsing..
     WholeStepStart,
     S_WHOLESTEP_COMPRESSION = 20, S_WHOLESTEP_BREATH, S_WHOLESTEP_AED, S_WHOLESTEP_RESULT,
     S_READY = 30, S_QUIT, S_STEPBYSTEP_MULTI,
     S_CALIBRATION, S_COMPLETE, CAL_NEW_CC, CAL_NEW_RP, CAL_LOAD  };

     */

    static func curStepStr(pStep: CurStep = globalState) -> String {
        let curStep = pStep
        //if AppIdTrMoTe == 0 { curStep = HsBleManager.inst().bleState }
        switch curStep {
        case .S_INIT0:                  return "S_INIT0"
        case .S_STEPBYSTEP_SAFETY:      return "S_STEPBYSTEP_SAFETY"
        case .S_STEPBYSTEP_AIRWAY:      return "S_STEPBYSTEP_AIRWAY"
        case .S_STEPBYSTEP_CHECK_BRTH:  return "S_STEPBYSTEP_CHECK_BRTH"
        case .S_STEPBYSTEP_RESPONSE:    return "S_STEPBYSTEP_RESPONSE"
        case .S_STEPBYSTEP_EMERGENCY:   return "S_STEPBYSTEP_EMERGENCY"
        case .S_STEPBYSTEP_CHECKPULSE:  return "S_STEPBYSTEP_CHECKPULSE"
        case .S_STEPBYSTEP_COMPRESSION: return "S_STEPBYSTEP_COMPRESSION"
        case .S_STEPBYSTEP_MULTI:       return "S_STEPBYSTEP_MULTI"
        case .S_STEPBYSTEP_BREATH:      return "S_STEPBYSTEP_BREATH"
        case .S_STEPBYSTEP_AED:         return "S_STEPBYSTEP_AED"
        case .S_WHOLESTEP_DESCRIPTION:  return "S_WHOLESTEP_DESCRIPTION"
        case .S_WHOLESTEP_COMPRESSION:  return "S_WHOLESTEP_COMPRESSION"
        case .S_WHOLESTEP_BREATH:       return "S_WHOLESTEP_BREATH"
        case .S_WHOLESTEP_AED:          return "S_WHOLESTEP_AED"
        case .S_WHOLESTEP_RESULT:       return "S_WHOLESTEP_RESULT"
        case .S_WHOLESTEP_PRECPR:       return "S_WHOLESTEP_PRECPR"

        case .S_READY:                  return "S_READY"
        case .S_QUIT:                   return "S_QUIT"
        case .CAL_LOAD:                 return "CAL_LOAD"
        case .CAL_NEW_CC:               return "CAL_NEW_CC"
        case .CAL_NEW_RP:               return "CAL_NEW_RP"
        case .S_CALIBRATION:            return "S_CALIBRATION"
        case .S_COMPLETE:               return "S_COMPLETE"
        case .BypassStepComp:           return "BypassStepComp"
        case .BypassStepBrth:           return "BypassStepBrth"
        case .WholeStepStart:           return "WholeStepRestart"
        }
    }

    static func prinCurStep(pStr:String) {
        print(" currentStep is ::   " + HsUtil.curStepStr() + "  _____  " + pStr)
    }

    static func isComp(stateObj: CurStep) -> Bool {
        switch stateObj {
        case .S_STEPBYSTEP_COMPRESSION, .S_WHOLESTEP_COMPRESSION:
            return true
        default:
            return false
        }
    }

    static func isBreath(stateObj: CurStep) -> Bool {
        switch stateObj {
        case .S_STEPBYSTEP_BREATH, .S_WHOLESTEP_BREATH:
            return true
        default:
            return false
        }
    }
}
