//
//  YgJsonBase.swift
//  YgManager
//
//  Created by Jongwoo Moon on 2015. 11. 14..
//  Copyright © 2015년 Jongwoo Moon. All rights reserved.
//

import Foundation

class LearnNSURLSession: NSObject, NSURLSessionDelegate, NSURLSessionTaskDelegate {
    typealias CallbackBlock = (result: String, error: String?) -> ()
    var callback: CallbackBlock = {
        (resultString, error) -> Void in
        if error == nil {
            print(resultString)
        } else {
            print(error)
        }
    }

    func httpGet(request: NSMutableURLRequest!, callback: (String, String?) -> Void) {
            let configuration =
            NSURLSessionConfiguration.defaultSessionConfiguration()
            let session = NSURLSession(configuration: configuration,
                delegate: self,
                delegateQueue:NSOperationQueue.mainQueue())
            let task = session.dataTaskWithRequest(request) {
                (data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
                if error != nil {
                    callback("", error?.localizedDescription)
                } else {
                    let result = NSString(data: data!, encoding:
                        NSASCIIStringEncoding)!
                    callback(result as String, nil)
                }
            }
            task.resume()
    }
    //optional func URLSession(session: NSURLSession, didReceiveChallenge challenge: NSURLAuthenticationChallenge,
    //func URLSession(session: NSURLSession, didReceiveChallenge challenge: NSURLAuthenticationChallenge,
      //  completionHandler: (NSURLSessionAuthChallengeDisposition, NSURLCredential!) -> Void) {
    func URLSession(session: NSURLSession, task: NSURLSessionTask,
        didReceiveChallenge challenge: NSURLAuthenticationChallenge,
        completionHandler: (NSURLSessionAuthChallengeDisposition, NSURLCredential?) -> Void) {
            completionHandler(
                NSURLSessionAuthChallengeDisposition.UseCredential,
                NSURLCredential(forTrust: challenge.protectionSpace.serverTrust!))
    }

    /* func URLSession(session: NSURLSession,  task: NSURLSessionTask,
        willPerformHTTPRedirection response: NSHTTPURLResponse,
        newRequest request: NSURLRequest,
        completionHandler: (NSURLRequest!) -> Void) {
            var newRequest : NSURLRequest? = request
            println(newRequest?.description);
            completionHandler(newRequest)
    }  */
}



class HsJsonBase : HtObject {
    var logB = HtLog(cName: "HsJsonBase")

    var serviceCode = "INIT"
    var version: Int = 1 // 패킷별 버전
    let qm = "\""
    var successJob:(()->())?, failJob:(()->())?
    var urlSession = LearnNSURLSession()
    
    var jStr = "", jResp:NSURLResponse?, jData:NSData?
    var respStr = ""
    
    // response 관련 공통 값들.
    var result = -1
    
    override init() {
        super.init()
    }
    
    convenience init(theCode: String) {
        self.init()
        serviceCode = theCode
        makeHeader(theCode)
    }

    // Convert from NSData to json object
    func nsdataToJSON(data: NSData) -> AnyObject? {
        do {
            return try NSJSONSerialization.JSONObjectWithData(data, options: .MutableContainers)
        } catch let myJSONError {
            print(myJSONError)
        }
        return nil
    }

    // Convert from JSON to nsdata
    func jsonToNSData(json: AnyObject) -> NSData? {
        do {
            return try NSJSONSerialization.dataWithJSONObject(json, options: NSJSONWritingOptions.PrettyPrinted)
        } catch let myJSONError {
            print(myJSONError)
        }
        return nil;
    }
    
    func makeHeader(theCode: String) {
        jStr = ""
        serviceCode = theCode
        addKeyValue("serviceCode", value: serviceCode, withFinishComma: true)
        addKeyValue("version", value: version, withFinishComma: true)
    }
    
    func finishEncoding() -> String {
        jStr = "{  " + jStr + "  }"
        return jStr
    }
    
    func makeArray(content: String) -> String {
        return " [ \(content) ] "
    }

    func makeCaretItem(content: String) -> String {
        return " { \(content) } "
    }

    func makeObject() {
        jStr = makeCaretItem(jStr)
    }
    
    func quoat(strObj: String) -> String {
        return qm + strObj + qm
    }
    
    func keyValueString(key: String, valStr: String) -> String {
        return quoat(key) + " : " + valStr
    }

    func addKeyString(key: String, value: String, withFinishComma: Bool = true) {
        jStr += keyValueString(key, valStr: value)
        if withFinishComma { jStr += ", " }
    }

    func addKeyValue(key: String, value: AnyObject, withFinishComma: Bool = true) {
        switch value {
        case is Int, is Int32, is Int64, is Int8, is UInt, is UInt16, is UInt64, is Double, is Float:
                    jStr += keyValueString(key, valStr: ("\(value)"))
        default:
                    jStr += keyValueString(key, valStr: quoat("\(value)"))
        }
        if withFinishComma { jStr += ", " }
    }

    func addKeyArray(key: String, value: AnyObject, withFinishComma: Bool = true) {
        jStr += keyValueString(key, valStr: ("\(value)"))
        if withFinishComma { jStr += ", " }
    }

    func requestSecure(addr: String, finishClj: (Void) -> Void) {
        logB.printThisFunc("*****     request secure  <  \(wasURL)\(addr) >  *****")
        let url = NSURL(string: wasURL + addr)
        let reqst = NSMutableURLRequest(URL: url!)
        reqst.HTTPMethod = "POST"
        reqst.HTTPBody = jStr.dataUsingEncoding(NSUTF8StringEncoding)

        print("\n\n\n  send Request >>> JSON >>>    \(jStr)  \n\n\n ")

        urlSession.httpGet(reqst) { (str, addStr) -> Void in
            print(" requestSecure  Https :: responsed ")
            print("\n requestSecure  String :: str >>> \(str) <<<")
            print("\n requestSecure  addStr  \(addStr) ")

            //self.jData = self.jsonToNSData(str)
            self.jData = str.dataUsingEncoding(NSUTF8StringEncoding)

            print("\n jData : \(self.jData)")

            finishClj()
        }
    }
    
//    func xxrequest(addr: String, finishClj: (Void) -> Void) {
//        logB.printThisFunc("*****     request   <  \(wasURL)   \(addr) >  *****")
//        let url = NSURL(string: wasURL + addr)
//        let reqst = NSMutableURLRequest(URL: url!)
//        reqst.HTTPMethod = "POST"
//        reqst.HTTPBody = jStr.dataUsingEncoding(NSUTF8StringEncoding)
//        
//        print("\n\n\n  send Request >>> JSON >>>    \(jStr)  \n\n\n ")
//        
//        let task = NSURLSession.sharedSession().dataTaskWithRequest(reqst) {
//            data, respse, errr in
//            print ("  response :  \(respse)")
//            print ("  data :  \(data)")
//            self.jResp = respse
//            self.jData = data
//            if errr != nil { // error case
//                print("\n\n\n\n\n  YgJsonBase :: request  >>>    http error   \(errr?.description)  !!!!!  >>>>>   >>>>>   >>>>>   >>>>>   >>>>>   >>>>>   >>>>>   >>>>>   >>>>>    ERROR   \n\n\n")
//                return
//            } else { finishClj() }
//        }
//        task.resume()
//    }
    
    
}
