// 
// Copyright 2013-2014 Yummy Melon Software LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
//  Author: Charles Y. Choi <charles.choi@yummymelon.com>
//

#import "YMSCBCharacteristic.h"
#import "NSMutableArray+fifoQueue.h"
#import "YMSCBPeripheral.h"
#import "YMSCBDescriptor.h"

#import "NSObject+util.h"

//////////////////////////////////////////////////////////////////////       [ YMSCBCharacteristic ]

@implementation YMSCBCharacteristic


- (instancetype)initWithName:(NSString *)oName parent:(YMSCBPeripheral *)pObj uuid:(CBUUID *)oUUID offset:(int)addrOffset {

    self = [super init];
    
    if (self) {
        _name = oName;
        _parent = pObj;
        _uuid = oUUID;
        _offset = [NSNumber numberWithInt:addrOffset];
        _writeCallbacks = [NSMutableArray new];
        _readCallbacks = [NSMutableArray new];
    }
    
    return self;
}


- (void)setNotifyValue:(BOOL)notifyValue withBlock:(void (^)(NSError *))notifyStateCallback {
    
    [self logCallerMethodwith:@" @50 Crash !!! " newLine:2];
    
    if (notifyStateCallback) {
        self.notificationStateCallback = [notifyStateCallback copy];
    }
    [self.parent.cbPeripheral setNotifyValue:notifyValue forCharacteristic:self.cbCharacteristic];
}

- (void)executeNotificationStateCallback:(NSError *)error {

    [self logCallerMethodwith:@" @60 " newLine:2];

    YMSCBWriteCallbackBlockType callback = self.notificationStateCallback;
    
    if (self.notificationStateCallback) {
        if (callback) {
            callback(error);
        } else {
            NSAssert(NO, @"ERROR: notificationStateCallback is nil; please check for multi-threaded access of executeNotificationStateCallback");
        }
        self.notificationStateCallback = nil;
    }
}

//3456789012345678901234567890123456789012345678901234567890123456789012345678901234567890

//////////////////////////////////////////////////////////////////////       [ writeValue ]
#pragma mark - writeValue.

- (void)writeValue:(NSData *)data withBlock:(void (^)(NSError *))writeCallback {
    
    //NSLog(@"WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW  YMSCBCharacteristic :: writeValue ");
    //NSLog(@"___   YMSCB Characteristic ::  >>    writeValue  %@   ", data);
    
    
    if (writeCallback) {
        [self.writeCallbacks push:[writeCallback copy]];
        [self.parent.cbPeripheral writeValue:data
                           forCharacteristic:self.cbCharacteristic
                                        type:CBCharacteristicWriteWithResponse];
    } else {
        if (self.parent == nil)  NSLog(@"\n\n\n\n\n self.parent is nil");
        
        if (self.parent.cbPeripheral == nil)  NSLog(@"\n\n\n\n\n self.parent.cbPeri...  is nil");
        
        [self.parent.cbPeripheral writeValue:data
                           forCharacteristic:self.cbCharacteristic
                                        type:CBCharacteristicWriteWithoutResponse];
    }
}

- (void)writeByte:(int8_t)val withBlock:(void (^)(NSError *))writeCallback {
    
    NSLog(@"___   YMSCB Characteristic ::  >>    executeReadCallback    vdvsw ");
    
    NSData *data = [NSData dataWithBytes:&val length:1];
    [self writeValue:data withBlock:writeCallback];
}


- (void)readValueWithBlock:(void (^)(NSData *, NSError *))readCallback {
    
    NSLog(@"\n\n\n\n\n ___   YMSCB Characteristic ::  >>    readValueWithBlock    ");
    
    
    [self.readCallbacks push:[readCallback copy]];
    [self.parent.cbPeripheral readValueForCharacteristic:self.cbCharacteristic];
}


- (void)executeReadCallback:(NSData *)data error:(NSError *)error {
    
    
    NSLog(@"___   YMSCB Characteristic ::  >>    executeReadCallback    ");
    
    
    YMSCBReadCallbackBlockType readCB = [self.readCallbacks pop];
    readCB(data, error);
}

- (void)executeWriteCallback:(NSError *)error {
    
    NSLog(@"___   YMSCB Characteristic ::  >>    executeReadCallback    333");
    
    YMSCBWriteCallbackBlockType writeCB = [self.writeCallbacks pop];
    writeCB(error);
}

- (void)discoverDescriptorsWithBlock:(void (^)(NSArray *, NSError *))callback {
    
    NSLog(@"___   YMSCB Characteristic ::  >>    executeReadCallback    2432 이거 안들어 옴 .. ");
    
    if (self.cbCharacteristic) {
        self.discoverDescriptorsCallback = callback;
    
        [self.parent.cbPeripheral discoverDescriptorsForCharacteristic:self.cbCharacteristic];
    } else {
        NSLog(@"WARNING: Attempt to discover descriptors with null cbCharacteristic: '%@' for %@", self.name, self.uuid);
    }
}


- (void)handleDiscoveredDescriptorsResponse:(NSArray *)ydescriptors withError:(NSError *)error {
    YMSCBDiscoverDescriptorsCallbackBlockType callback = [self.discoverDescriptorsCallback copy];

    
    NSLog(@"___   YMSCB Characteristic ::  >>    executeReadCallback   23fc ");
    
    if (callback) {
        callback(ydescriptors, error);
        self.discoverDescriptorsCallback = nil;
    } else {
        NSAssert(NO, @"ERROR: discoverDescriptorsCallback is nil; please check for multi-threaded access of handleDiscoveredDescriptorsResponse");
    }
}

- (void)syncDescriptors:(NSArray *)foundDescriptors {
    
    NSLog(@"___   YMSCB Characteristic ::  >>    executeReadCallback    sdff ");
    
    
    NSMutableArray *tempList = [[NSMutableArray alloc] initWithCapacity:[foundDescriptors count]];
    
    for (CBDescriptor *cbDescriptor in foundDescriptors) {
        YMSCBDescriptor *yd = [YMSCBDescriptor new];
        yd.cbDescriptor = cbDescriptor;
        yd.parent = self.parent;
        [tempList addObject:yd];
    }
    
    self.descriptors = tempList;
}

@end

//3456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
//////////////////////////////////////////////////////////////////////       [ CLASSname ]
