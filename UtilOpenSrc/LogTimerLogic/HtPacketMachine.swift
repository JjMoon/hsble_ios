//
//  HtPacketMachine.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2015. 12. 9..
//  Copyright © 2015년 IMLab. All rights reserved.
//

import Foundation

/**
 
 패킷 어레이를 갖고 작업.  트레이너가 없으면 리트라이를 계속하는데..  이를 제지해야 함.


**/

class HtPacketMachine : NSObject {
    var log = HtLog(cName: "HtPacketMachine")
    var arrPckt = [HtBaseJob]()
    var timer = NSTimer()

    var additionalUpdate: (Void -> Void)?

    override init() {
        super.init()
    }
    
    convenience init(interval: Double) {
        self.init()
        timer = NSTimer.scheduledTimerWithTimeInterval(interval, target: self,
                                                       selector: #selector(HtPacketMachine.timerAction),
                                                       userInfo: nil, repeats: true)
    }

    func invalidate() {
        timer.invalidate()
    }

    /// 작업 추가..  이름이 같은 건 제외하자...
    func addBaseJob(aJob: HtBaseJob) {
//        let duplicate = arrPckt.filter { (jb) -> Bool in
//            jb.jobName == aJob.jobName
//        }
//        if 0 < duplicate.count {
//            log.printAlways("  동일 작업 리스트에 올리지 않음...  ", lnum: 2)
//            return
//        }
        arrPckt.append(aJob)
    }

    func addUnitJobWith(entry: Void -> Void, finish: Void -> Bool, title: String) {
        let uJob = UnitJob(name: title)
        uJob.initJob = entry
        uJob.didFinished = finish
        arrPckt.append(uJob)
    }

    func ackReceivedAndRemoveJob() {
        if 0 < arrPckt.count {
            log.printAlways("arrPckt.removeFirst()")
            arrPckt.removeFirst()
        }
        if 0 < arrPckt.count { // 20161025 추가..  즉시 다음 패킷 실행..
            let curJob = arrPckt[0]
            curJob.initJob();
            curJob.retryCount = 1;
        }
    }

    func checkFinishAndStartNext() {
        if (arrPckt.count == 0) { return }
        let curJob = arrPckt[0]
        if curJob.didFinished() {
            ackReceivedAndRemoveJob()
        }
    }

    func timerAction() {

        additionalUpdate?()

        if (arrPckt.count == 0) { return }
        let curJob = arrPckt[0]

        // for debugging print("\n\n\n\n   timerAction")
        //for jb in arrPckt { print("  HtPacketMachine : arrPckt name >>>  \(jb.jobName)  ") }

        if (curJob.retryCount == 0) {
            log.logThis("   First try  :::  initJob of  \(curJob.jobName)    남은 작업 수 : \(arrPckt.count)  ", lnum: 1)
            curJob.initJob();
            curJob.retryCount = 1;
            return;
        } else {
            if (curJob.didFinished()) { // OK..  done..
                //curJob = nil;
                print("arrPckt   removefirst   \(curJob.jobName)    남은 작업 수 : \(arrPckt.count)  ")
                arrPckt.removeFirst()
                return;
            }
        }
        
        if (curJob.retryLimit <= curJob.retryCount) {
            //log.logUiAction("  retryLimit <= curJob.retryCount    \(curJob.jobName)   실패 .... ", lnum:10)
            arrPckt.removeFirst()
        } else {
            log.logThis(" initJob  of   \(curJob.jobName)   retryNum : \(curJob.retryCount)   남은 작업 수 : \(arrPckt.count)", lnum: 1)
            curJob.initJob();
            curJob.retryCount += 1
        }
        log.함수차원_출력_End()
    }
}
