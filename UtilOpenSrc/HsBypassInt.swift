//
//  HsBypassInt.swift
//  HS Monitor
//
//  Created by Jongwoo Moon on 2016. 4. 15..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation


class HsBypassInt: NSObject {
    private var theValue = 0
    private var bypassValue = 0 // 모니터에서만 쓰임.

    override init() {
        super.init()
    }

    convenience init(iniVal: Int) {
        self.init()
        theValue = iniVal
    }

    var theVal : Int {
        get {
            if AppIdTrMoTe == 0 { // Trainer
                if isBypassMode {
                    return bypassValue
                }
                return theValue
            }
            return theValue
        }
    }

    func parseValueInTrainerBypassMode(pVal: Int) {
        print("parseValueInTrainerBypassMode >>  \(pVal)")
        bypassValue = pVal
    }

    func setIntValue(pVal: Int) {
        theValue = pVal
    }
}