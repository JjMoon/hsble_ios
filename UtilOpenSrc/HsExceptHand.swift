//
//  HsExceptHand.swift
//  heartisense
//
//  Created by Jongwoo Moon on 2015. 9. 14..
//  Copyright (c) 2015년 gyuchan. All rights reserved.
//

import Foundation

import UIKit

//struct UnitJob {    var name = "name" }


@objc
class HsExceptHand : NSObject
{
    var finshed: Bool
    //var alrtAction: UIAlertAction
    
    lazy var curViewCtrl = UIViewController()
    

    override init () {
        //super.init()        print ("HsExceptHand :: init ... ")
        finshed = false
        super.init()
    }
    
    deinit {
        print ("HsExceptHand :: deinit ... ")
    }

    
    
    func testFromSwift(parInt: Int, parStr: String) -> Int {
        var int1: Int = 33;
        
        print("  값이 변하는가 \(int1)")
        var _ : Int = oneIntName( &int1, intTwo : 55)
        
        print("  값이 변하는가 \(int1)")
        
        return parInt
    }
    
    
    
    // 여러개의 파라메터
    func manyParam(manyStr: String...) {
        for str in manyStr {
            print( str );
        }
    }
    
    
    // 함수형 언어 ..
    // 이런것도 되네 ...
    var someFunc = manyParam
    // 함수를 받아서... 함수를 리턴...
    func functionParameter(void함수: (), float함수: (Float)->Float ) -> ()->Float {
        
        return {  return 2.3 }
        
    }
    
    
    
    
    func defltParam(aStr: String = "Default", aInt: Int = 33) -> String {
        return " \(aStr) is aStr and integer is \(aInt)"
    }
    
    func oneIntName (inout intOne:Int, intTwo: Int) -> Int { // inout 는 값을 변화시킴..  ref 처럼..
        intOne += 1
        return intOne + intTwo
    }

    func twoIntsOneInt (intOne:Int, intTwo: Int) -> Int {
        
        
        return intOne + intTwo
    }
    
    func popupWarning (vc : UIViewController ) {
        
        print ("HsExceptHand :: testAction () { ... ")
        
        curViewCtrl = vc
        
        let actionSheetController: UIAlertController = UIAlertController(title: "Alert", message: "Swiftly Now! Choose an option!", preferredStyle: .Alert)
        
        //Create and add the Cancel action
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .Cancel) { action -> Void in
            //Do some stuff
        }
        actionSheetController.addAction(cancelAction)
        //Create and an option action
        let nextAction: UIAlertAction = UIAlertAction(title: "Next", style: .Default) { action -> Void in
            //Do some other stuff
        }
        actionSheetController.addAction(nextAction)
        //Add a text field
        actionSheetController.addTextFieldWithConfigurationHandler { textField -> Void in
            //TextField configuration
            textField.textColor = UIColor.blueColor()
        }
        
        //Present the AlertController
        curViewCtrl.presentViewController(actionSheetController, animated: viewAnimate, completion: nil)
        
        
        
//        var aPopUpView = UIAlertController()
//        aPopUpView.title = "title"
//        aPopUpView.message = "something msg"
//        aPopUpView.addAction(alrtAction) // addBufttonWithTitle("first Bttn")
        //aPopUpView.addButtonWithTitle("secon Bttn")
        
        //aPopUpView.show .show()
        
        
        
    }
}