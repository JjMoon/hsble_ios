//
//  HsCalcWrap
//  BleMaestro
//
//  Created by Jongwoo Moon on 2016. 6. 20..
//  Copyright © 2016년 Jongwoo Moon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HsCalcWrap.h"

// import CPP files
#import "HsCalculation.h"
#import "HsPacket.h"

@interface HsCalcWrap ()
{
    HsCalculation calcObj; // C++ 클래스는 스위프트에 노출 불가..

}
@end

@implementation HsCalcWrap

- (void)someTest {
    calcObj.someFunction();



}


-(void)parsePacket:(NSData *)pData {
    // unsigned char  에서 바꿨는데..  ???
    unsigned char val[pData.length];
    [pData getBytes:&val length:pData.length];
    calcObj.addPacket(val);

    _compVal = calcObj.curSet()->curPack()->mDepth;
    _brthVal = calcObj.curSet()->curPack()->mAmount;

    //compVal = calcObj.curSet()->addNewStroke()
}


-(void)showResult {
    calcObj.showResult();
}

//
//-(void)parsePacket:(NSData *)pData withCallback:(void (^)(void))callBack {
//
////    NSData *data = characteristic.value;
//    //unsigned char val[pData.length];
//    char val[pData.length];
//    [pData getBytes:&val length:pData.length];
//
//    //HsPacket cppObj = HsPacket(val);
//
////    cppObj.voidCallBack = ^{
////        NSLog(@"  call back");
////    };
//
////    cppObj.voidCallBack = callBack;
////
////    cppObj.proceedCallBack();
//
//
//
//
//
//}

@end
