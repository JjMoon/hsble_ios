//
//  AppDelegate.h
//  BleMaestro
//
//  Created by Jongwoo Moon on 2016. 4. 12..
//  Copyright © 2016년 Jongwoo Moon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

