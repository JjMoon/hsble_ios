//
//  PeripheralVC.swift
//  BleMaestro
//
//  Created by Jongwoo Moon on 2016. 4. 21..
//  Copyright © 2016년 Jongwoo Moon. All rights reserved.
//

import Foundation
import UIKit
import CoreBluetooth

class PeripheralVC: UIViewController {
    var uiTimer = NSTimer(), cnt = 0
    let timerInterval = 0.1

    var calcWrap = HsCalcWrap()
    var aStreamReader : HtFileReader?

    @IBOutlet weak var progComp: UIProgressView!
    @IBOutlet weak var progBrth: UIProgressView!
    @IBOutlet weak var labelCnt: UILabel!

    var bleMaestro = HtBleManager()

    @IBAction func bttnActBroadcasting(sender: AnyObject) {


    }

    @IBAction func bttnActCppTest(sender: AnyObject) {
        cnt = 0
        fileOpen()
        uiTimer = NSTimer.scheduledTimerWithTimeInterval(timerInterval, // second
            target:self, selector: #selector(PeripheralVC.update),
            userInfo: nil, repeats: true)
    }

    func update() {
        readSingleLine()
    }

    func readSingleLine() {
        cnt += 1

        if let line = aStreamReader!.nextLine() {
            let theData = line.dataFromHexadecimalString()

            print("\n\n new Line Data >>  \(theData)")

            calcWrap.parsePacket(theData)

            progComp.setProgress( Float(calcWrap.compVal) / 100, animated: false)
            progBrth.setProgress( Float(calcWrap.brthVal) / 100, animated: false)
            labelCnt.text = "Count : \(cnt)"
        } else {
            print("  종료  !!!  ")
            aStreamReader?.close()
            uiTimer.invalidate()
            calcWrap.showResult()
        }
    }



    func fileOpen() {
        let path = NSBundle.mainBundle().resourcePath! + "/TestSet01.log"

        aStreamReader = HtFileReader(path: path)

        readSingleLine()

//        
//
//        if aStreamReader != nil {
//            defer {
//                aStreamReader.close()
//            }
//
//            readSingleLine()

//            while let line = aStreamReader.nextLine() {
//
//                let theData = line.dataFromHexadecimalString()
//
//                print("\n" + line)
//                //print(" DATA :: >>  \(theData)   \(theData?.length)")
//
//
//                calcWrap.parsePacket(theData)
//
//                // [data getBytes:&val length:data.length];
//
//                // var buf : UnsafeMutablePointer<UInt8> = nil
//                // theData!.getBytes(&buf, length: theData!.length)
//
////                let packWrap = HsCalcWrap()
////                packWrap.parsePacket(theData!, withCallback: {
////                    print("  swift from callcabasdf asdfklj")
////                })
//
//
//
//
//            }        }

    }
}