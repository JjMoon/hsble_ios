//
//  HsCalcCapsule.h
//  BleMaestro
//
//  Created by Jongwoo Moon on 2016. 6. 20..
//  Copyright © 2016년 Jongwoo Moon. All rights reserved.
//

#ifndef HsCalcWrap_h
#define HsCalcWrap_h

#import <Foundation/Foundation.h>



@interface HsCalcWrap : NSObject
{


}



-(void)someTest;

-(void)showResult;
-(void)parsePacket:(NSData *)pData;


@property (nonatomic, readonly) int compVal, brthVal;

//-(void)parsePacket:(NSData *)pData withCallback:(void (^)(void))callBack;



@end




#endif /* HsCalcWrap_h */
