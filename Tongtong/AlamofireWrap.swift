//
//  AlamofireWrap.swift
//  Tongtong
//
//  Created by Jongwoo Moon on 2016. 9. 7..
//  Copyright © 2016년 IMLabInc. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift


public class AlamofireWrap : NSObject {

    func aTest() {

        Alamofire.request(.GET, "https://httpbin.org/get", parameters: ["foo": "bar"])
            .responseJSON { response in
                print(response.request)  // original URL request
                print(response.response) // URL response
                print(response.data)     // server data
                print(response.result)   // result of response serialization

                if let JSON = response.result.value {
                    print("\n\n\n")
                    print("JSON: \(JSON)")

                }
        }
    }



}