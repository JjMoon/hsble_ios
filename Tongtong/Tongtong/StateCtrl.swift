//
//  StateCtrl.swift
//  Tongtong
//
//  Created by Jongwoo Moon on 2016. 9. 27..
//  Copyright © 2016년 IMLabInc. All rights reserved.
//

import Foundation


enum BleState {
    case Init; case Scan; case Conn; case Operate
}

enum TrainState {
    case Step; case Entire; case Calibration
}

enum ViewState {
    case Init; case Setting
}

private let _singletoneObj = Stt()
class Stt: NSObject {
    /// singletone obj : Stt.singl
    class var singl: Stt { return _singletoneObj }

    var view = ViewState.Init
    var ble = BleState.Init
    var train = TrainState.Step
}

