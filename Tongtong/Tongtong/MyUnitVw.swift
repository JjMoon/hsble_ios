//
//  MyUnitVw.swift
//  Tongtong
//
//  Created by Jongwoo Moon on 2016. 9. 20..
//  Copyright © 2016년 IMLabInc. All rights reserved.
//

import Foundation
import UIKit

class MyUnitView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        didLoad()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        didLoad()
    }

    convenience init() {
        self.init(frame: CGRectZero)
    }

    func didLoad() {
        //Place your initialization code here

        //I actually create & place constraints in here, instead of in
        //updateConstraints
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        //Custom manually positioning layout goes here (auto-layout pass has already run first pass)
    }

    override func updateConstraints() {
        super.updateConstraints()

        //Disable this if you are adding constraints manually
        //or you're going to have a 'bad time'
        //self.translatesAutoresizingMaskIntoConstraints = false
        
        //Add custom constraint code here
    }
}
