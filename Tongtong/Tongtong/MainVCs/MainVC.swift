//
//  MainVC.swift
//  Tongtong
//
//  Created by Jongwoo Moon on 2016. 9. 8..
//  Copyright © 2016년 IMLabInc. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import RxSwift
import RxCocoa
import SwiftyJSON


class MainVCtrl: UIViewController {
    var mainVM: MainTongVM!

    var toggleAction = { }


    let viewModel = ViewModel()
    let disposeBag = DisposeBag()



    @IBOutlet weak var nameTextField: UITextField!

    @IBOutlet weak var secondTextField: UITextField!



    @IBOutlet weak var degreesLabel: UILabel!
    @IBOutlet weak var cityNameLabel: UILabel!
    
    @IBOutlet weak var asdfasdf: UIButton!
    @IBAction func asdsdfsfsf(sender: AnyObject) {

        print("asdfasfda ")

        let sb = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let vc = sb.instantiateViewControllerWithIdentifier("another") as! AnotherVC
        //vc?.navigationBackTitle = "프로필"
        navigationController?.pushViewController(vc, animated: true)

    }

    // Main 에서 컨테이너로 가는 신호.
    //var toggleOptionVC : (Bool) -> () = { (bl: Bool) -> () in    }

    @IBAction func bttnActOption(sender: UIButton) {
        print("\n\n MainVCtrl ::   @IBAction func bttnActionToggle(sender: UIButton) ")
        //toggleAction()

        let sb = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let vc = sb.instantiateViewControllerWithIdentifier("another") as! AnotherVC
        //vc?.navigationBackTitle = "프로필"
        navigationController?.pushViewController(vc, animated: true)

    }

    override func viewDidLoad() {
        print("\n\n MainVCtrl :: vidwDidLoad")

        /// 첫번째 입력 스트링... 바인딩
//        nameTextField.rx_text.subscribeNext { (txt) in
//            print ("  txt \(txt)")
//            self.degreesLabel.text = txt
//        }.addDisposableTo(disposeBag)

        nameTextField.rx_text.subscribeNext { text in
            self.viewModel.searchText.onNext(text)
            }
            .addDisposableTo(disposeBag)
        secondTextField.rx_text.subscribeNext { text in
            self.viewModel.confrmText.onNext(text)
            }
            .addDisposableTo(disposeBag)

        viewModel.resultTxt.bindTo(degreesLabel.rx_text)
        .addDisposableTo(disposeBag)

        viewModel.callBack2VC = {
            self.degreesLabel.text = "Good Password"
        }

        viewModel.callBackFail2VC = {

            self.degreesLabel.text = "Bad Password"
        }

        /// 두번째 입력 스트링... 바인딩
//        secondTextField.rx_text.subscribeNext { (txt2) in
//            print("  txt2 : \(txt2)")
//            self.cityNameLabel.text = txt2
//        }.addDisposableTo(disposeBag)


        //Binding the UI
        viewModel.cityName.bindTo(cityNameLabel.rx_text)
            .addDisposableTo(disposeBag)

        viewModel.degrees.bindTo(degreesLabel.rx_text)
            .addDisposableTo(disposeBag)

    }


    override func viewDidAppear(animated: Bool) {


    }

    func constaintDebug() {

        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "MyUnitVw", bundle: bundle)

        let newView = nib.instantiateWithOwner(self, options: nil).last as? MyUnitView!
        //newView!.frame = CGRect(x: 50, y: 200, width: 300, height: 300)
        newView!.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(newView!)

        let cons = newView!.getConstSize(300, height: 200)
        newView!.addConstraints(cons)


        let con2 = newView!.getConstCenterX(cityNameLabel, delta: 0)
        view.addConstraint(con2)
        let cony = newView!.getConstCenterY(cityNameLabel, delta: 0)
        view.addConstraint(cony)



        
        newView!.setNeedsLayout()
    }


    func bindOriginal() {
        //Binding the UI
        viewModel.cityName.bindTo(cityNameLabel.rx_text)
            .addDisposableTo(disposeBag)

        viewModel.degrees.bindTo(degreesLabel.rx_text)
            .addDisposableTo(disposeBag)

        nameTextField.rx_text.subscribeNext { text in
            self.viewModel.searchText.onNext(text)
            }
            .addDisposableTo(disposeBag)
    }
}




class Weather {
    var name:String?
    var degrees:Double?

    init(json: AnyObject) {
        let data = JSON(json)
        self.name = data["name"].stringValue
        self.degrees = data["main"]["temp"].doubleValue
    }
}


class ViewModel: NSObject {

    private struct Constants {
        static let URLPrefix = "http://api.openweathermap.org/data/2.5/weather?q="
        static let URLSuffix = "ff73891794c4e985fcc0bec6b9946716"
    }

    let disposeBag = DisposeBag()

    var searchText = BehaviorSubject<String?>(value: "")
    var confrmText = BehaviorSubject<String?>(value: "")

    var cityName = BehaviorSubject<String>(value: "")
    var degrees = BehaviorSubject<String>(value: "")

    var callBack2VC = { }, callBackFail2VC = { }

    var resultTxt = BehaviorSubject<String>(value: "")

    override init() {
        super.init()

        searchText.subscribeNext { (txt) in
            print(" ViewModel : init length \(txt?.getLength()) ")
        }

        _ = Observable.combineLatest(searchText, confrmText) {
            //"\($0) \($1)"
            self.compare($0!, st2: $1!)
            }
            .subscribe {
                if $0.element! {
                    print("  combineLatest :: \($0)   true .. ")

                    self.resultTxt.onNext("  OK")

                    //self.callBack2VC()
                } else {
                    //self.callBackFail2VC()
                    self.resultTxt.onNext("  NG")
                }
                print("\n")
        }
    }

    private func compare(st: String, st2: String) -> Bool {
        return st == st2 && 4 < st.getLength()
    }




//
//    func compareStrings() -> Bool {
//        do {
//            //try print(" compareStrings()  \(searchText.value())    \(confrmText.value())  ")
//
//
//            var t1 = 0, t2 = 0
//
//            try {
//                t1 = searchText.value()!.getLength()
//                t2 = confrmText.value()!.getLength()
//
//
//
//            }
//            return t1 == t2
//
//        } catch  {
//            print("   catched ...  throw 1 ")
//            return false
//        }
//
//    }


    var weather: Weather? {
        didSet {
            print("  didset ")
            if let name = weather?.name {
                dispatch_async(dispatch_get_main_queue()) {
                    self.cityName.onNext(name)
                }
            }
            if let temp = weather?.degrees {
                dispatch_async(dispatch_get_main_queue()) {
                    self.degrees.onNext("\(temp)°F")
                }
            }
        }
    }
}
