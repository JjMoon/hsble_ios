//
//  ContainerVC.swift
//  Tongtong
//
//  Created by Jongwoo Moon on 2016. 9. 8..
//  Copyright © 2016년 IMLabInc. All rights reserved.
//

import Foundation
import UIKit


struct MainVCStateStrc {

    var viewState: MainViewState = .MainView
    mutating func setNewState(newStt: MainViewState) -> MainViewState {
        self.viewState = newStt
        return self.viewState
    }
}
//
//extension MainVCStateStrc {
//    mutating func setNewState(newStt: MainViewState) {
//        self.viewState = newStt
//    }
//}

class ContainerVC: UIViewController {

    var state = MainVCStateStrc()

    var mainNavCtrl: UINavigationController!
    var mainVC: MainVCtrl!
    var optionTVC: OptionsTVC!


    var mainVMdl: MainTongVM!

    /// UI, Gesture etc ...
    var touchBeginPoint: CGPoint?
    let centerPanelExpandedOffset: CGFloat = 150 // 안 밀리는 폭..

    override func viewDidLoad() {
        super.viewDidLoad()

        print("  ContainerVC : viewDidLoad    ")

        /// mainVC 생성.
        mainVC = UIStoryboard.getMainVC()
        mainVC.toggleAction = optionToggleHandle
        optionTVC = nil

        print("  mainVC : \(mainVC)")


        /// Navigation Controller
        mainNavCtrl = UINavigationController(rootViewController: mainVC)
        view.addSubview(mainNavCtrl.view)
        addChildViewController(mainNavCtrl)
        mainNavCtrl.didMoveToParentViewController(self)
        mainVMdl = MainTongVM(cont: self)
    }

    //////////////////////////////////////////////////////////////////////     [   UI 처리..     << >> ]
    func optionToggleHandle() {
        mainVMdl.optionToggleHandle()

    }
//
//    func mainViewTouched() {
//        switch state.viewState {
//        case .MainView:
//            return
//        case .OptionShown:
//            toggleOptionView(false)
//        }
//    }

}

private extension UIStoryboard {
    class func mainStoryboard() -> UIStoryboard { return UIStoryboard(name: "Main", bundle: NSBundle.mainBundle()) }

    class func getMainVC() -> MainVCtrl? {
        return mainStoryboard().instantiateViewControllerWithIdentifier("mainVC") as? MainVCtrl
    }

    class func getOptionTVC() -> OptionsTVC? {
        return mainStoryboard().instantiateViewControllerWithIdentifier("optionsTVC") as? OptionsTVC
    }

//    class func rightViewController() -> RightSlideVC? {
//        return mainStoryboard().instantiateViewControllerWithIdentifier("RightSlideVC") as? RightSlideVC
//    }
}