//
//  MainTongVM.swift
//  Tongtong
//
//  Created by Jongwoo Moon on 2016. 9. 9..
//  Copyright © 2016년 IMLabInc. All rights reserved.
//

import Foundation
import UIKit

enum MainViewState {
    case MainView
    case OptionShown

}

// 뷰의 열고 닫음. 애니메이션 담당.
class MainTongVM : NSObject {

    var container: UIViewController!
    var mainNavCtrl: UINavigationController!
    var mainVC: MainVCtrl!
    var optionTVC: OptionsTVC!

    var state = MainVCStateStrc()


    let centerPanelExpandedOffset: CGFloat = 150 // 안 밀리는 폭..


    // 내가 바꾸고
    var stateChangeHandler: ((MainViewState) -> Void)?


    override init() {
        super.init()
    }

    convenience init(cont: ContainerVC) {
        self.init()
        container = cont

        mainNavCtrl = cont.mainNavCtrl
        mainVC = cont.mainVC
        state = cont.state
        optionTVC = nil

        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(MainTongVM.handlePanGesture(_:)))
        mainNavCtrl.view.addGestureRecognizer(panGestureRecognizer)
        let tapGestrRecgnzr = UITapGestureRecognizer(target: self, action:
            #selector(MainTongVM.handleTap(_:)))
        mainNavCtrl.view.addGestureRecognizer(tapGestrRecgnzr)
        mainNavCtrl.navigationBar.setHeightWith(1)
    }

    func optionToggleHandle() {
        switch state.viewState {
        case .MainView:
            toggleOptionView(true)
        default:
            toggleOptionView(false)
        }
    }
    func toggleOptionView(open: Bool) {
        print("MainTongVM :: func toggleOptionView() ")

        if open {
            addRightPanelViewController()
            animateRightPanel(true)
        } else {
            animateRightPanel(false)
        }
    }

    func mainOpensOption() {

    }
}


extension MainTongVM: UIGestureRecognizerDelegate {

    func addRightPanelViewController() {
        print("MainTongVM :: func addRightPanelViewController() \(optionTVC)")
        if (optionTVC == nil) {
            optionTVC = UIStoryboard.getOptionTVC()
            //rightViewController!.animals = Animal.allDogs()
            container.view.insertSubview(optionTVC.view, atIndex: 0)

            container.addChildViewController(optionTVC)
            optionTVC.didMoveToParentViewController(container)


        }
    }


    func handleTap(sender: UITapGestureRecognizer) {
        if sender.state == .Ended {
            animateRightPanel(false)
        }
    }

    func handlePanGesture(recognizer: UIPanGestureRecognizer) {
        let gestureIsDraggingFromLeftToRight = (recognizer.velocityInView(container.view).x > 0)

        switch(recognizer.state) {
        case .Began:


            if state.viewState == .MainView {
                // 드래그 해서 뷰를 열 때..
                if (gestureIsDraggingFromLeftToRight) {

                } else {
                    addRightPanelViewController()
                }
                showShadowForCenterViewController(true)
            }
        case .Changed:
            recognizer.view!.center.x = recognizer.view!.center.x + recognizer.translationInView(container.view).x
            recognizer.setTranslation(CGPointZero, inView: container.view)
        case .Ended:
            print("ContainerVC ::   .Ended ... \(optionTVC)   \(recognizer.view!.center.x)")

            if (optionTVC != nil) {
                let hasMovedGreaterThanHalfway = recognizer.view!.center.x < 0
                animateRightPanel(hasMovedGreaterThanHalfway)
            } else {
                animateRightPanel(false)
            }
        default:
            break
        }
    }

    func showShadowForCenterViewController(shouldShowShadow: Bool) {
        if (shouldShowShadow) {
            mainNavCtrl.view.layer.shadowOpacity = 0.8
        } else {
            mainNavCtrl.view.layer.shadowOpacity = 0.0
        }
    }


    func animateRightPanel(shouldExpand: Bool) {
        print("ContainerVC :: func animateRightPanel(shouldExpand: \(shouldExpand))   \(CGRectGetWidth(mainNavCtrl.view.frame))")

        switch state.viewState {
        case .MainView:
            if !shouldExpand { return }
        case .OptionShown:
            if shouldExpand { return }
        }

        if shouldExpand {
            animateCenterPanelXPosition(-CGRectGetWidth(mainNavCtrl.view.frame) + centerPanelExpandedOffset)
            state.viewState = .OptionShown
        } else {
            animateCenterPanelXPosition(0) { _ in
                if self.optionTVC != nil {
                    self.optionTVC!.view.removeFromSuperview()
                    self.optionTVC = nil;
                }
                self.state.viewState = .MainView
            }
        }
    }

    func animateCenterPanelXPosition(targetPosition: CGFloat, completion: ((Bool) -> Void)! = nil) {
        UIView.animateWithDuration(0.5, delay: 0, usingSpringWithDamping: 0.8,
                                   initialSpringVelocity: 0, options: .CurveEaseInOut,
                                   animations: {
                                    self.mainNavCtrl.view.frame.origin.x = targetPosition
            }, completion: completion)
    }

}


private extension UIStoryboard {
    class func mainStoryboard() -> UIStoryboard { return UIStoryboard(name: "Main", bundle: NSBundle.mainBundle()) }

    class func getMainVC() -> MainVCtrl? {
        return mainStoryboard().instantiateViewControllerWithIdentifier("mainVC") as? MainVCtrl
    }

    class func getOptionTVC() -> OptionsTVC? {
        return mainStoryboard().instantiateViewControllerWithIdentifier("optionsTVC") as? OptionsTVC
    }

    //    class func rightViewController() -> RightSlideVC? {
    //        return mainStoryboard().instantiateViewControllerWithIdentifier("RightSlideVC") as? RightSlideVC
    //    }
}