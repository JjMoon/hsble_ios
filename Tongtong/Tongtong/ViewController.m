//
//  ViewController.m
//  Tongtong
//
//  Created by Jongwoo Moon on 2016. 9. 7..
//  Copyright © 2016년 IMLabInc. All rights reserved.
//

#import "ViewController.h"

#import <Tongtong-Swift.h>


@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.


    AlamofireWrap *wrap = [[AlamofireWrap alloc] init];

    [wrap aTest];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
