//
//  TempGlobal.swift
//  Tongtong
//
//  Created by Jongwoo Moon on 2016. 9. 9..
//  Copyright © 2016년 IMLabInc. All rights reserved.
//

import Foundation
import UIKit


let colorGreyBright = UIColor(red: 220, green255: 220, blue255: 220)
let colorTransparent = UIColor(red: 1, green: 0.96, blue: 0.32, alpha: 0)
let colorWhiteAlpha10 = UIColor(red255: 255, green255: 255, blue255: 255, alpha: 0.1)
let colorWhiteAlpha20 = UIColor(red255: 255, green255: 255, blue255: 255, alpha: 0.2)
let colorWhiteAlpha80 = UIColor(red255: 255, green255: 255, blue255: 255, alpha: 0.8)

let colorBarYelow = UIColor(red: 1, green: 0.96, blue: 0.32, alpha: 1)
let colorBarGreen = UIColor(red: 0.12, green: 0.98, blue: 0.33, alpha: 1)
let colorBarRedfg = UIColor(red: 1, green: 0.12, blue: 0.12, alpha: 1)
let colorBarBgYlw = UIColor(red: 0.82, green: 0.79, blue: 0.25, alpha: 0.5)
let colorBarBgGrn = UIColor(red: 0.07, green: 0.44, blue: 0.14, alpha: 0.8)
let colorBarBgRed = UIColor(red: 0.69, green: 0.07, blue: 0.1, alpha: 0.8)
let colorTxtGreen = UIColor(red255: 20, green255: 142, blue255: 56, alpha: 1)

let colBttnYellow = UIColor.init(hex: "F8B000")
let colBttnGreen = UIColor.init(hex: "69B97B")
let colBttnRed = UIColor.init(hex: "E54411")
let colBttnDarkGray = UIColor.init(hex: "5D5D5D")
let colBttnGray = UIColor.init(hex: "8E8E8E")
let colBttnBrightGray = UIColor.init(hex: "D6D6D6")
let colGraphGreenArea = UIColor.init(hex: "EFFFFA")
let colHalfTransGray = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 0.7)


/// OK Green   //  69 b9 7b
let colorOKGreen = UIColor(red: 80, green255: 200, blue255: 80)
let colorLabelGreen = UIColor(red: 105, green255: 185, blue255: 123)
let colorLblDarkGreen = UIColor(red: 80, green255: 200, blue255: 80)
/// Not Good  !!! Red
let colorNtGdRed = UIColor(red: 1, green: 0.12, blue: 0.12, alpha: 1)

let colorBttnActiveBlue = UIColor(red: 105, green255: 185, blue255: 123)
let colorBttnDarkGray = UIColor(red: 142, green255: 142, blue255: 142)
let colorBttnDarkGreen = UIColor(red: 86, green255: 216, blue255: 180)
