//
//  AppDelegate.h
//  Tongtong
//
//  Created by Jongwoo Moon on 2016. 9. 7..
//  Copyright © 2016년 IMLabInc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

{
    UIWindow *window;
}


@property (strong, nonatomic) UIWindow *window;


@end

