//
//  HSDataCalculator.h
//  heartisense
//
//  Created by jeongyuchan on 2015. 4. 26..
//  Copyright (c) 2015년 gyuchan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HSEnumSet.h"

@class HSStepDataCalculator, HsData;


typedef void(^CalibrationBlock)(HSCommendList sensor, HSCaliResultType result, UInt16 resultData);



@interface HSDataCalculator : NSObject
{


}

@property (nonatomic, weak) HsData *dataObj;
@property (nonatomic) UInt16 cc_cali_value, rp_cali_value;

+(void)responseCalculator:(NSData*)data
             successBlock:(void (^)(HSSensorStatus result, HSCommendList cmd))successBlock;

//
+ (int)halfOfAbsSum:(int)first sub1:(int)subA sub2:(int)subB;
+ (int)calculateForce:(UInt16)x;
+ (double)vectorSum:(double)a second:(double)b  thrid:(double)c;

- (void)setCaliValue;

-(void)sensorCalibrationData:(NSData*)data
                successBlock:(CalibrationBlock)successBlock;

-(void)resetCounter;

-(void)sensorDataParsing:(NSData*)data
            successBlock:(void (^)())successBlock;
@end
