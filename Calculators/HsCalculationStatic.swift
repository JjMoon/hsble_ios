//
//  HsCalculationStatic.swift
//  HS Monitor
//
//  Created by Jongwoo Moon on 2016. 8. 3..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation


class HsCalculationStatic {
    static func parseDepthInCM(depthVal: Double) -> Double {
        var result = 0.0
        let point6 = Double(Comp6cmPoint), point5 = Double(Comp5cmPoint)
        if (depthVal < point5) {
            result = depthVal * 5.0 / point5
        } else if (depthVal <= point6) {
            result = 5.0 + ((depthVal - point5) * 1.0 / (point6 - point5))
        } else {
            result = depthVal * 6.0 / point6
        }
        return result * 10
    }

    static func parseAmountInML(amount: Double) -> Double {
        let ml700 = Double(Brth700mlPoint), ml400 = Double(Brth400mlPoint)
        switch amount {
        case 0...ml400:
            let denomi = amount / ml400
            return denomi * 400
        case ml400...ml700:
            let denomi = (amount - ml400) / (ml700 - ml400)
            return 400 + denomi * 300
        case ml700...100.0:
            let denomi = (amount - ml700) / (100 - ml700)
            return 700 + denomi * 300
        default:
            return 0
        }
    }
}

func calculateForce(val: Int) -> Int {
    let a = 3.58322799547958, b = 0.00153868372576938, c = 0.000000113167937766884;
    let v2 = val * val, dv2 = Double(v2)
    let y = exp(a + b * Double(val) + c * dv2)
    return Int(y)
}

/*
+ (int)calculateForce:(UInt16)x {
    //float a = 2.62239001285986, b = 0.00398209350025777, c = -0.000000810748753229992;
    float a = 3.58322799547958, b = 0.00153868372576938, c = 0.000000113167937766884;
    UInt64 xxx = x * x;
    double x2 = (double)xxx;
    double y = exp(a + b * (double)(x) + c * x2);

    return (int)y;
}
*/
