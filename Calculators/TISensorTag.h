// 
// Copyright 2013-2014 Yummy Melon Software LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
//  Author: Charles Y. Choi <charles.choi@yummymelon.com>
//

#include "YMS128.h"

/**
 Data pulled from the following sources:
 
 * http://processors.wiki.ti.com/index.php/SensorTag_User_Guide
 * http://processors.wiki.ti.com/index.php/File:BLE_SensorTag_GATT_Server.pdf
 
 */

#ifndef Deanna_TISensorTag_h
#define Deanna_TISensorTag_h

// Is this unique?
// #define DATALINE_UUID @"0000ffb2-0000-1000-8000-00805f9b34fb"
// #define SERVICE_UUID @"0000ffb0-0000-1000-8000-00805f9b34fb"

#define SERVICE_UUID_STRING @"0xFFB0"
#define SERVICE_UUID 0xFFB0
#define DATALINE_UUID 0xFFB2

#define kSensorTag_BASE_ADDRESS_HI 0xF000000004514000
#define kSensorTag_BASE_ADDRESS_LO 0xB000000000000000

#define HSSensorTag_BATTERY_INFO 0x01
#define HSSensorTag_PIEZO_INFO 0x02
#define HSSensorTag_ACCELERATION_INFO 0x03
#define HSSensorTag_ATM_INFO 0x04
#define HSSensorTag_BLUETOOTH1_INFO 0x05
#define HSSensorTag_BLUETOOTH2_INFO 0x06



yms_u128_t yms_u128_genSensorTagOffset(int value);
yms_u128_t yms_u128_genSensorTagAddressWithInt(yms_u128_t *base, int value);


#endif
