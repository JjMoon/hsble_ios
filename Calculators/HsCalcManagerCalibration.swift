//
//  HsCalcManagerCalibration.swift
//  Trainer
//
//  Created by Jongwoo Moon on 2015. 12. 26..
//  Copyright © 2015년 IMLab. All rights reserved.
//

import Foundation



extension HsCalcManager {

    func calibrationViewDataParsing(pData:[UInt8]) {
        //log.printThisFunc("calibrationViewDataParsing", lnum: 0)
        curDatas = pData
        
        if !commonDataParsing() {
            return
        }

        log.함수차원_출력_End()
    }

    
    func caliSubSetDepthAmount() {
        //log.printThisFunc("caliSubSetDepthAmount( )", lnum: 0)
        //var debug1:Double = 0, debug2:Double = 0
        for i in 0...2 { // x, y, z
            depth_min_cv[i] = Double (HCStatic.uint8TosignedByte(curDatas[8 + (i * 2)])) / 10.0
            depth_max_cv[i] = Double (HCStatic.uint8TosignedByte(curDatas[9 + (i * 2)])) / 10.0
        }
        switch (stepMoni) {
        case .CAL_NEW_CC:
            var temp_d = 100 * (force_value - data.ccStaPoint) / (limit_p) // data.ccStaPoint
            if (temp_d < 5) { temp_d = 0 } else if (temp_d > 100) {temp_d = 100 }
            depth = temp_d
            //print(" caliSubSetDepthAmount    temp_d \(temp_d),  depth  \(depth) ")
            parseCalibrationCompData()
        case .CAL_NEW_RP:
            var temp_d = 100 * (breath_value - init_rp) / (limit_rp)
            if (temp_d < 5) { temp_d = 0 } else if (temp_d > 100) {temp_d = 100 }
            amount = Int(temp_d)
            //print(" caliSubSetDepthAmount    temp_d \(temp_d),  depth  \(depth) ")
            parseCalibrationBreathData()
        case .CAL_LOAD:
            var temp_d = Int(Float(force_value - (data.ccStaPoint)) / Float((data.ccLimitPointTemp)) * 100)  // 그래프 값...
            if (temp_d < 5) { temp_d = 0 } else if (temp_d > 100) {temp_d = 100 }
            depth = temp_d
            temp_d = Int(Float(breath_value - (data.rpStaPoint)) / Float((data.rpLimitPointTemp)) * 100)
            if (temp_d < 5) { temp_d = 0 } else if (temp_d > 100) {temp_d = 100 }
            amount = Int(temp_d)
        default:
            break;
        }
    }
    
    func parseCalibrationCompData() {
        log.logThis("CalibrationCC", lnum: 0)
        if (!is_turning_d) {
            if (prev_depth_max_cv <= Double(depth_min_cv[2])) {
                // Prev_max <= min <= max
                for i in 0...2 { // (int i = 0; i < 3; i++) {
                    depth_order[0][i] = Double(depth_min_cv[i])
                    depth_order[1][i] = Double(depth_max_cv[i])
                }
            } else {
                for i in 0...2 { //				for (int i = 0; i < 3; i++) {
                    depth_order[0][i] = Double(depth_max_cv[i])
                    depth_order[1][i] = Double(depth_min_cv[i])
                }
            }
            prev_depth_min_cv = Double(depth_max_cv[2])
            prev_depth_max_cv = prev_depth_min_cv
            
            //log.logThis(" Not turning prev_depth_max_cv  \(prev_depth_max_cv)   ", lnum: 0)
        } else {
            if (prev_depth_min_cv >= Double(depth_max_cv[2])) {  // Prev_min >= max >= min
                for i in 0...2 {
                    depth_order[0][i] = Double(depth_max_cv[i])
                    depth_order[1][i] = Double(depth_min_cv[i])
                }
            } else {
                for i in 0...2 {
                    depth_order[0][i] = Double(depth_min_cv[i])
                    depth_order[1][i] = Double(depth_max_cv[i])
                }
            }
            
            prev_depth_min_cv = Double(depth_min_cv[2])
            prev_depth_max_cv = prev_depth_min_cv
            //log.logThis(" Yes turning prev_depth_max_cv  \(prev_depth_max_cv)   ", lnum: 0)
        }
        processAccel()
        processLoadCell()
    }


    
    func parseCalibrationBreathData () {
        //log.printThisFunc("CalibrationRP", lnum: 5)
        //HsUtil.prinCurStep(" caliSubSetDepthAmount   ")
        let prevMaxValue = (prevMaxRp - data.rpStaPoint) / 2 + data.rpStaPoint
        if (!is_turning_rp) {
            let rpLimitValue = 10 + data.rpStaPoint
            //if ( (countRP == 0 && max_rp <= rpLimitValue && breath_value <= rpLimitValue) ||
            if ( (countRP == 0 && breath_value <= rpLimitValue) ||
                (0 < countRP && breath_value <= prevMaxValue) ||
                max_rp <= Int(breath_value + RP_FILTER) ) {
                if (breath_value >= max_rp) {  max_rp = breath_value }
            } else {
                is_turning_rp = true;
                sum_rp += max_rp
                prevMaxRp = max_rp // 20160118
                max_rp = (data.rpStaPoint) - 100 //RP_START_POINT - 100;
                countRP += 1
                print("\n\n countRP ++    \(countRP)  \n\n ")
            }
        } else {
            //if (breath_value <= Int(RP_FILTER) + (data.rpStaPoint)) {
            if breath_value <= prevMaxValue {
                is_turning_rp = false;
                min_rp = (data.rpStaPoint) + limit_p + 100;
                if (countRP == 5) {
                    print(" sum_rp : \(sum_rp)  (data.rpStaPoint) : \((data.rpStaPoint))")
                    
                    data.rpLimitPointNew = (sum_rp / 5 - (data.rpStaPoint)) * 100 / 52 //  RP_LIMIT_POINT_NEW
                    log.logThis("   Calibration  RP_LIMIT_POINT_NEW : \((data.rpLimitPointNew)) ", lnum: 3)
                    
                    //if (data.rpLimitPointNew < 50) { data.rpLimitPointNew = 50 }                    if (RP_LIMIT_POINT_NEW > 1050) { RP_LIMIT_POINT_NEW = 1050 }
                    data.rpLimitPointNew = data.rpLimitPointNew.setBetweenFrom(50, maxVal: 1050)
                        //setBetweenFrom(50, maxVal: 1050, theVal: (data.rpLimitPointNew))
                    data.ccLimitPointTemp = (data.ccLimitPointNew)
                    data.rpLimitPointTemp = (data.rpLimitPointNew)
                    
                    print("\n\n  \t\t  CalibrationRP ::  RP_LIMIT_POINT_NEW    RP :  \((data.rpLimitPointNew))")
                    activateLoad()
                }
            }
        }
    }


    func setPeakDFromDepthOrder (ii:Int, jj:Int, doidx:Int) {
        for k in 0...2 {
            peak_d[ii][jj][k] = Double(depth_order[doidx][k])  // peak_d[0][count_d][0] = depth_order[i][0];
        }
    }

    func processAccel() {
        for i in 0...1 {   // (int i = 0; i < 2; i++) {
            if (!is_turning_d) {
                if depth_order[i][2] + data.distanceFilter >= Double(max_d) {
                    if depth_order[i][2] > Double(max_d) {
                        max_d = depth_order[i][2]
                        //print("  set max_d @ \(max_d)")
                        setPeakDFromDepthOrder(0, jj: countD, doidx: i)
                    }
                } else {
                    is_turning_d = true;
                }
            } else {
                if depth_order[i][2] - data.distanceFilter <= Double(min_d) {
                    if depth_order[i][2] < Double(min_d) {
                        min_d = depth_order[i][2]
                        //print("  set min_d @ \(min_d)")
                        setPeakDFromDepthOrder(1, jj: countD, doidx: i)
                    }
                } else {
                    is_turning_d = false;
                    min_d = 100
                    max_d = -100
                    countD += 1
                    //print(" \n\n   countD ++   \(countD)  &  초기화 ...  \n\n\n")
                }
            }
        }
    }
    
    func processLoadCell() {
        let startLimitPo = data.ccStaPoint + limit_p / 20
        if (!is_turning_p) {
            //if (force_value <= init_p + ccFilter || force_value >= max_p) {
            if (max_p <= startLimitPo && force_value <= startLimitPo) || max_p <= force_value {
                if (force_value > max_p) {
                    max_p = force_value;

                    if (countP < 15) {
                        peak_p[0][countP] = force_value
                    }
                }
            } else {
                is_turning_p = true;
                max_p = data.ccStaPoint // init_p;
            }
        } else {
            //if (force_value - ccFilter <= min_p && force_value > init_p + ccFilter) {
            if force_value - ccFilter <= min_p && force_value > startLimitPo {
                if (force_value < min_p) {
                    min_p = force_value;
                    peak_p[1][countP] = force_value;
                }
            } else {
                is_turning_p = false;
                if (force_value < min_p) {
                    min_p = force_value;
                    peak_p[1][countP] = force_value;
                }
                min_p = data.ccStaPoint + limit_p;
                countP += 1
                
                if (countP == 15) { //  5) { // 15) { // 30번 완료 후..
                    findGradientInCalibration()
                    activateBreathCalibration()
                }
            }
        }
    }
    
    func activateLoad() {
        stepMoni = .CAL_LOAD
    }
    
    func activateBreathCalibration() {
        stepMoni = .CAL_NEW_RP
        countRP = 0
        //limit_rp = 1050
        is_turning_rp = false;
        init_rp = (data.rpStaPoint)
        min_rp = (data.rpStaPoint) + limit_p + 100;
        max_rp = (data.rpStaPoint) - 100;
        sum_rp = 0;
        
        print("\n\n\n\n\n\n\n")
        print("     activateBreathCalibration      data.rpStartPoint \(data.rpStaPoint),   RP_LIMIT_POINT_NEW \(data.rpLimitPointNew),   RP_LIMIT_POINT \(data.rpLimitPoint),   min_rp \(min_rp), max_rp \(max_rp)")
        print("\n\n\n\n\n\n\n")
    }

    func findGradientInCalibration() {
        var dx:Double = 0, dy:Double = 0, dz:Double = 0, xx:Double = 0, xy:Double = 0
        for idx in 1...14 {  //29 { // (int idx = 1; idx < 30; idx++) {  30번 평균..
            dx = (fabs(peak_d[0][idx][0] - peak_d[1][idx][0]) + fabs(peak_d[0][idx][0] - peak_d[1][idx - 1][0])) / 2.0;
            dy = (fabs(peak_d[0][idx][1] - peak_d[1][idx][1]) + fabs(peak_d[0][idx][1] - peak_d[1][idx - 1][1])) / 2.0;
            dz = (fabs(peak_d[0][idx][2] - peak_d[1][idx][2]) + fabs(peak_d[0][idx][2] - peak_d[1][idx - 1][2])) / 2.0;
            
            //final_d[idx - 1] = 1.2 * HSDataCalculator.vectorSum(dx, second: dy, thrid: dz)
            final_d[idx - 1] = 10.0 / 9.0 * HSDataCalculator.vectorSum(dx, second: dy, thrid: dz)
            //  표 값으로 가져온다...
            final_p[idx - 1] = Int(HSDataCalculator.halfOfAbsSum(Int32(peak_p[0][idx]), sub1: Int32(peak_p[1][idx]), sub2 : Int32(peak_p[1][idx-1])))
            
            xx += pow(final_d[idx - 1], 2) // final_d[idx - 1] * final_d[idx - 1];
            xy += final_d[idx - 1] * Double(final_p[idx - 1])  //pow(Double(final_p[idx - 1]), 2) // * final_p[idx - 1];
            
            print("  findGradient :: xyz : \(dx)  \t \(dy)  \t \(dz)  \t     xx : \(xx)   xy : " + String(format: "%.2f", xy))
        }
        data.ccLimitPointNew = Int(100 * xy / (xx * 13))
        
        print ("  \n\n  (data.ccLimitPointNew)!  = \((data.ccLimitPointNew)) ")
        
        //if (CC_LIMIT_POINT_NEW < 20) { CC_LIMIT_POINT_NEW = 20;}        if (CC_LIMIT_POINT_NEW > 1620) {    CC_LIMIT_POINT_NEW = 1620; }
        //data.ccLimitPointNew = setBetweenFrom(20, maxVal: 1620, theVal: (data.ccLimitPointNew)!)
        data.ccLimitPointNew = data.ccLimitPointNew.setBetweenFrom(20, maxVal: 3220)
            //setBetweenFrom(20, maxVal: 3220, theVal: (data.ccLimitPointNew))
        
        data.ccLimitPointTemp = data.ccLimitPointNew // CC_LIMIT_POINT_TEMP
        
        print("\n\n \t\t\t  result of find Gradient >>>  CC_LIMIT_POINT_NEW and ...TEMP  \((data.ccLimitPointNew)) \n\n")
    }
    


}
