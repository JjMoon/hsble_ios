//
//  HSAttributedString.h
//  heartisense
//
//  Created by jeongyuchan on 2015. 4. 24..
//  Copyright (c) 2015년 gyuchan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


#define COLOR_HS_BLACK [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:1]
#define COLOR_HS_GRAY [UIColor colorWithRed:30/255.0 green:30/255.0 blue:30/255.0 alpha:1]
#define COLOR_HS_WHITE [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1]
#define COLOR_HS_BLUE [UIColor colorWithRed:220/255.0 green:210/255.0 blue:20/255.0 alpha:1]

@interface HSAttributedString : NSObject

+ (NSMutableAttributedString*)defaultStyleAttributedString:(NSString*)Title
                                                      font:(UIFont*)font
                                                 fontColor:(id)fontColor;

@end
