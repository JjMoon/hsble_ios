//
//  HCalcBrth.swift
//  heartisense
//
//  Created by Jongwoo Moon on 2015. 10. 12..
//  Copyright © 2015년 gyuchan. All rights reserved.
//

import Foundation


class HCalcObjt : NSObject {
    
    var objType = "CalcObj"
    
    var weakUIcljr:(Void)->Void = {}
    
    var theArray = ValueArray()
    
    override init() {
        theArray.weakUIcljr = weakUIcljr
        super.init()
    }
    
    func addNewValue(newValue:Int32) {
        
        theArray.addNewValue(newValue)
    }
    
}

class xxHCalcComp : HCalcObjt {
    // 70 <
    override init() {
        super.init()
        objType = "Comp"
    }
    
    //var count:Int
    
}


class xxHCalcBrth : HCalcObjt {
    // 35 < < 70
    override init() {
        super.init()
        objType = "Brth"
    }
}


class ValueArray : NSObject {
    var values = [Int]() // 최저에서 최고까지 갔다 온 값.
    
    var weakUIcljr:(Void)->Void = {}
    
    var minEff = 35, maxEff = 70
    
    let meanNum = 3
    
    var cnt = 0, corCnt = 0
    
    var correctMin = 30, correctMax = 70, countMin = 10
    var isIncresing = false, isCounted = false, correctValueSet = false
    
    var correctIndex = 0 // -1 weak, 0, normal, 1 strong
    
    var oldstMean = 0, lastMean = 0, curMean = 0, serial = 0, maxVal = 0, serialDecreasingNum = 0
    
    
    var weakNum = 0, strongNum = 0
    
    func directionCheck() {
        
        print(" \n directionCheck >>  values >>>  \(oldstMean) \(lastMean)  \(curMean) ")
        
        if oldstMean < lastMean && lastMean < curMean {
            isIncresing = true
            serialDecreasingNum = 0
            print ("\n\n\n set isIncresing = true")
        }
        
        if oldstMean > lastMean && lastMean > curMean {
            isIncresing = false
            serialDecreasingNum += 1
            print ("\n\n\n set isIncresing = false")
        }
    }
    
    func countCheck() {
        if isCounted {
            
            print ("countCheck :: curMean : \(curMean)  increasing? \(isIncresing) serialDecreasingNum : \(serialDecreasingNum) ")
            
            if curMean <= 10 && !isIncresing { // 카운트 재시작.  내려갈 때. 10 이하..
                isCounted = false
                maxVal = curMean
                serialDecreasingNum = 0 // 초기화
                correctIndex = 0 // 초기화
                print("  \n\n\n Counted::Initialize::  \(cnt) corCnt : \(corCnt) \n\n\n")
            }

            // max 값 산출을 위해..  내려가는 횟수 기록.
            if !isIncresing && 0 <= serialDecreasingNum && !correctValueSet {
                serialDecreasingNum += 1
                if 3 < serialDecreasingNum { // 연속되는 내려가기 5번..  판단..
                    print("  \n\n\n  Value Checked  Max : \(maxVal)   \n\n\n")
                    correctValueSet = true 
                    if minEff < maxVal && maxVal < maxEff {
                        correctIndex = 0
                        weakUIcljr()
                        corCnt += 1
                    }
                    
                    
//                    switch maxVal {
//                    case 0..<30:
//                        print("  \n\n\n  Weak Value Checked \n\n\n")
//                        weakUIcljr()
//                        weakNum++
//                        correctIndex = -1
//                    case 30..<80:
//                        correctIndex = 0
//                        weakUIcljr()
//                        corCnt++
//                    case 80..<1001:
//                        print("  \n\n\n  Strong Value Checked \n\n\n")
//                        weakUIcljr()
//                        strongNum++
//                        correctIndex = 1
//                    default:
//                        correctIndex = 2 // error
//                    }
                }
            }
            return
        }
        if isIncresing && maxVal > 10 {
            cnt += 1
            if 30 <= maxVal && maxVal <= 70 {
                corCnt += 1
            }
            isCounted = true
            correctValueSet = false
            print("  \n\n\n Counted::OK::  \(cnt) corCnt : \(corCnt) \n\n\n")
        }
    }
    
    func addNewValue(newValue:Int32) {

        print ( "\n\n\n addNewValue : \(newValue) is added ...  ")
        serial += 1
        values.append(Int(newValue))
        let newVal = Int(newValue)
        if values.count > 20 {
            values.removeFirst()
        }
        
        if serial % 2 == 1 {
            oldstMean = lastMean
            lastMean = curMean
        }
        
        curMean = meanValueOfNumber(meanNum)
        
        // maxvalue setting..
        if (maxVal < Int(newVal)) {
            maxVal = Int(newVal)
        }
        
        directionCheck()
        
        countCheck()
        
        print ( "\n\n\n addNewValue : Last >>> mean value  \(meanValueOfNumber(3)) counted ? \(isCounted) count : \(cnt)  incresing? : \(isIncresing)    serialDecreasingNum : \(serialDecreasingNum)" )
        print ( "   Count :   \(cnt) Effective :  \(corCnt) weak : \(weakNum)  strong : \(strongNum)  \n\n" )
    }
    
    func meanValueOfNumber(num:Int) -> Int {
        
        var rVal = 0, cnt = num
        let startIdx = (values.count - num)
        if startIdx < 0 {
            return (0)
        }
        //print("  startIdx : \(startIdx)  count : \(values.count) ")
        for idx in startIdx...(values.count - 1) {
            //print ( " idx : \(idx)")
            rVal += values[idx]
        }
        
        return Int(rVal / cnt)
    }
    

    
}