//
//  HsProcessManager.h
//  heartisense
//
//  Created by Jongwoo Moon on 2015. 9. 11..
//  Copyright (c) 2015년 gyuchan. All rights reserved.
//

#ifndef heartisense_HsProcessManager_h
#define heartisense_HsProcessManager_h


#endif




@interface HsProcessManager : NSObject

{
    //BOOL isScanning;
    //NSMutableArray *arrUnitJob, *arrBle;
    
    //NSTimer* mtmTimer;
}



//- (void) requestCompCalibrSetDataPeri:(HSPeripheral*)peri withService:(HSBaseService*)service;


// UI | UX



-(void)startCompressProcess;
//-(void)startBreathProcess;
-(void)stopCompressProcess;
-(void)stopBreathProcess;



// Block Define
@property (nonatomic, strong) void (^goToSelectionModeView)(void);

//
@property (strong) NSTimer* mtmTimer;

//@property (nonatomic, strong) NSMutableArray *arrUnitJob;
//@property (nonatomic, strong) NSMutableArray *arrBle;
//
//
//@property (nonatomic, weak) HSPeripheral* cellPeripheral;
//@property (nonatomic, weak) HSBaseService* cellService;
//
//@property (nonatomic, weak) SearchDeviceListViewCell* curCell;
//@property (nonatomic, weak) UIView* tempViewObj;


@end
