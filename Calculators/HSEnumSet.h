//
//  HSEnumSet.h
//  heartisense
//
//  Created by jeongyuchan on 2015. 4. 26..
//  Copyright (c) 2015년 gyuchan. All rights reserved.
//

#import <UIKit/UIKit.h>

//typedef NS_ENUM(NSInteger, HSUserMode) {
//    HSlayRescuerMode= 0,
//    HShealthcareProviderMode,
//    HsBypassMode
//};

typedef NS_ENUM(NSInteger, HSSelectMode) {
    HSStepByStepMode= 0,
    HSWholeStepMode,
};


typedef NS_ENUM(NSInteger, HSSensorStatus) {
    HSSensorConnected= 0,
    HSSensorNotConnected,
    HSSensorError,
    HSSensorNotDefineError,
};

typedef NS_ENUM(NSInteger, HSCommendList) {
    HSBattry= 0,
    HSPiezo,
    HSAcceleration,
    HSATMCheck,
    HSBluetooth1,
    HSBluetooth2,
    HSPressureModule,
    HSATMModule,
};

typedef NS_ENUM(NSInteger, HSCaliResultType){
    HSCaliSuccessType = 0,
    HSCaliEditType,
};


typedef NS_ENUM(NSInteger, HSCompressionMessageType) {
    HSCompressionGoodType= 0,
    HSCompressionWrongPositionType,
    HSCompressionTooWeakType,
    HSCompressionTooStrongType,
    HSCompressionNotRecoil,
    HSCompressionAllClearType,
};

typedef NS_ENUM(NSInteger, HSBreathMessageType) {
    HSBreathGoodType = 0,
    HSBreathTooWeakType,
    HSBreathTooStrongType,
    HSBreathAllClearType,
};


typedef NS_ENUM(NSInteger, HSCalculatorType){
    HSCompressionCalculatorType = 0,
    HSBreathCalculatorType,
};


typedef NS_ENUM(NSInteger, HSStepByStepSelectedType){
    ResponseType = 0,
    EmergencyType,
    CheckPulseType,
    CompressionType,
    BreathType,
    AEDType,
};


