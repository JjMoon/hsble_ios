//
//  HSStepDataCalculator.h
//  heartisense
//
//  Created by jeongyuchan on 2015. 5. 2..
//  Copyright (c) 2015년 gyuchan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HSDataCalculator.h"

typedef void(^PositionUpdateBlock)(BOOL *position);
typedef void(^DepthUpdateBlock)(int depth);
typedef void(^CountUpdateBlock)(int count);
typedef void(^MessageUpdateBlock)(HSCompressionMessageType messageType);

typedef void(^amountUpdateBlock)(int amount);
typedef void(^BreathMessageUpdateBlock)(HSBreathMessageType messageType);




@interface HSStepDataCalculator : NSObject
{
    //HsData *dataObj;
    
}

@property (nonatomic, weak) HsData *dataObj;

- (void)startCompression;
- (void)setStart:(BOOL)start;



/**
 1. 페이지가 살아있을 때 false
 2. 페이지를 나갔을 때 true
 */
- (void)setQuit:(BOOL)quit;

- (void)stepCompressionOperation:(NSData*)data
             positionUpdateBlock:(PositionUpdateBlock)positionUpdateBlock
                depthUpdateBlock:(DepthUpdateBlock)depthUpdateBlock
                countUpdateBlock:(CountUpdateBlock)countUpdateBlock
              messageUpdateBlock:(MessageUpdateBlock)messageUpdateBlock;


- (void)startBreath;

- (void)stepBreathOperation:(NSData*)data
          amountUpdateBlock:(amountUpdateBlock)amountUpdateBlock
           countUpdateBlock:(CountUpdateBlock)countUpdateBlock
         messageUpdateBlock:(BreathMessageUpdateBlock)messageUpdateBlock;

@end
