//
//  HSAttributedString.m
//  heartisense
//
//  Created by jeongyuchan on 2015. 4. 24..
//  Copyright (c) 2015년 gyuchan. All rights reserved.
//

#import "HSAttributedString.h"

@implementation HSAttributedString

+ (NSMutableAttributedString*)defaultStyleAttributedString:(NSString*)Title
                                                      font:(UIFont*)font
                                                 fontColor:(id)fontColor{
    NSMutableAttributedString *attributedTitleString = [[NSMutableAttributedString alloc]init];
    attributedTitleString = [[NSMutableAttributedString alloc] initWithString:Title];
    [attributedTitleString addAttribute:NSLigatureAttributeName value:@(0.5) range:NSMakeRange(0, [attributedTitleString length])];
    [attributedTitleString addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, [attributedTitleString length])];
    [attributedTitleString addAttribute:NSForegroundColorAttributeName value:fontColor range:NSMakeRange(0, [attributedTitleString length])];
    return attributedTitleString;
}

@end
