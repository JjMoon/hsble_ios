//
//  HsCalcManagerBrth.swift
//  Trainer
//
//  Created by Jongwoo Moon on 2015. 12. 26..
//  Copyright © 2015년 IMLab. All rights reserved.
//

//  호흡, 최종 프로세스 등 ...



import Foundation


extension HsCalcManager {

    // MARK: - 모니터 관련 함수
    func prepareEntireProcess() {
        entireStepFinished = false
        log.printThisFunc("prepareEntireProcess : ", lnum: 1)
        data.reset()
        data.arrSet = [HmUnitSet]()
        stage = 0
        increaseSet()
    }


    // MARK: - 세트 증가.
    func increaseSet() {
        stage += 1


        brthTimer.Reset()

        arrRevision.removeAll()
        isRevisionFinished = false

        minForce = 99999999; maxForce = 0 // realcali revision 관련

        log.printThisFNC("increaseSet", comment: "  stage : \(stage)  ccTime : \(curStageObj.ccTime)   holdTime : \(compHoldTimer.GetSecond(0))")

        if curStageObj.ccTime < 0.0001 { // 압박에서 압박으로 넘어온 경우.
            curStageObj.ccTime = data.operationTimer.timeSinceStart - compHoldTimer.GetSecond(0)
            //print(" erasethissss  cc time = \(curStageObj.ccTime)")
        }

        if data.arrSet.count > 0 {
            curStageObj.rpTime = data.operationTimer.timeSinceStart
            curStageObj.holdTime = compHoldTimer.GetSecond(0)

            print("타임>> rptime세팅 .. 칼크에서  \(curStageObj.rpTime)  얘는 무시됨...")
        }

        log.logThis("  count : \(curStageObj.ccCount) | \(curStageObj.rpCount)   ccTime : \(curStageObj.ccTime) | \(curStageObj.rpTime) H \(curStageObj.holdTime)")

        curStageObj = data.increaseSet(stage) //HmUnitSet()
        log.logThis("  세트 추가 후 ::  Stage :: \(stage)  arrSet count \(data.arrSet.count)")
        log.logThis("  count : \(curStageObj.ccCount) | \(curStageObj.rpCount)   ccTime : \(curStageObj.ccTime) | \(curStageObj.rpTime) H \(curStageObj.holdTime)   timer :  \(data.operationTimer.timeSinceStart)")
        log.함수차원_출력_End()
    }
    
    func increaseBrthCount() {
        log.printThisFunc("호흡카운트 *증가", lnum: 5)
        data.count += 1  //  호흡 카운트 증가..
        curStageObj.rpCount = data.count // 테스터에서 필요..


        log.logThis("     rpCount : \(data.count)     maxAmount : \(maxAmount)  ", lnum: 1)



        let newAmount = HmBrthStroke()
        newAmount.max = maxAmount
        curStageObj.arrBreath.append(newAmount)  // 최대값 저장.

        curStageObj.holdTime = compHoldTimer.GetSecond(0) // V.1.08.160718 매 카운트할때마다 타임을 찍는다.



//
//        print(" strokeTimeOfBreath : \(curStageObj.arrBreath.last?.strkTime)")
//        print(" strokeTimeOfBreath : \(brthTimer.dueTime)")

        //speedLogic?.ifSpeedOKofBreath(brthTimer.dueTime)

        //brthTimer.Reset()


        log.함수차원_출력_End()
    }
    
    
    //////////////////////////////////////////////////////////////////////     _//////////_     [ Breath      << >> ]    _//////////_   호 흡
    // MARK: DataParsing :  호흡 관련 breath
    func breathDataParsing() {   // [UInt8]) {
        let timeAfterGameStart = data.operationTimer.timeSinceStart //NSDate().timeIntervalSince1970 - data.gameStartTime


        print("      breathDataParsing>>>  maxAmount :: \(maxAmount)  \(RP_FILTER)   ")

        // V.1.08.160718  아래는 호흡을 한번도 안 할 때는 불리지 않는다..
        //if (stage == stageLimit && timeAfterGameStart > 15 && isAllInOnePrcss) { // 모든 스테이지 종료 위치..
        if (stage == stageLimit && timeAfterGameStart > 9 && isAllInOnePrcss) { // 모든 스테이지 종료 위치.. 9초로 변경..
            entireStepFinalProcess()
        } else if amount < 10  && (depth > 20 || dir < 240) && stage < stageLimit && timeAfterGameStart >= 2 && isAllInOnePrcss {
            //print ("  rpCorrectCount  ::  \(data.rpCorCount) \(data.rp_pass_count.count)  stage : \(stage) ")
            curStageObj.rpPassCnt = data.rpCorCount // data.rp_pass_count[stage - 1] = data.rpCorCount
            curStageObj.rpCount = data.count // data.rp_count[stage - 1] = data.count
            if (data.ccCorCount >= Int(CC_PASS) && data.rpCorCount >= Int(RP_PASS)) {
                increaseStateOfPatient()
            }
            
            data.ccCorCount = 0; data.rpCorCount = 0
            
            increaseSet()

            bleMan?.stt = .S_WHOLESTEP_COMPRESSION
            stepMoni = (bleMan?.stt)!

            log.printAlways(" breathDataParsing :   ... stage++  :  \(stage)    다음 스테이지로...  압박으로 세팅..   initializeCompression() ", lnum: 5)
            initializeCompressionGoToNextSet()
        } else {
            if !isTurning {
                checkAmount() // 증가..
            } else {
                recoilAmount() // 감소..
            }
            //isPrint += 1
        }

        //setCompBrthTestTime()
        log.함수차원_출력_End()
    }

    private func sendBrthStrengthSignalToSpeedLogic() {
        if maxAmount < Int(RP_WEAK_POINT) {
            speedLogic?.tooStrongWeakAct(true)
        } else if Int(RP_STRONG_POINT) < maxAmount {
            speedLogic?.tooStrongWeakAct(false)
        } else {
            speedLogic?.amountVolumeGood()
        }
    }

    
    // MARK: 호흡..   check Amount   전환점 세팅. 최대값 세팅.
    func checkAmount() {
        //log.printThisFunc("check Amount", lnum: 1)
        //log.logThis("  amount \(amount)  RP_WEAK_POINT : \(RP_WEAK_POINT)   RP_STRONG_POINT : \(RP_STRONG_POINT)     \(data.rpCorCount) / \(data.count) ", lnum: 0)
        //  0. 48. 80. 0 >>  짧게 큰 힘이 들어가는 경우는 카운트 안함.. 실제로는 서서히 감소할 것임.

        if (amount <= Int(RP_FILTER)) || amount + Int(RP_FILTER) >= maxAmount {
            //log.logThis(" 계속 증가하는 상태..  카운트 안됨..    \(amount) <= \(RP_FILTER) || \(amount) + \(RP_FILTER) >= \(maxAmount)  ", lnum: 0)
            if amount >= maxAmount {
                maxAmount = amount
                maxValueForSL = maxAmount
            }
        } else {
            isTurning = true
            log.logThis("   isTurning = true   maxAmount : \(maxAmount)   \(RP_WEAK_POINT) \(RP_STRONG_POINT) ", lnum: 0)
            if maxAmount < Int(RP_WEAK_POINT) {
                bleMan?.messageName = "message_weak"
                data.isCorrectAmount = 1;
                data.isMessageDone = true

                //// speedLogic?.tooStrongWeakAct(true)

            } else if Int(RP_STRONG_POINT) < maxAmount {
                bleMan?.messageName = "message_strong"
                data.isCorrectAmount = 2;
                data.isMessageDone = true
                //// speedLogic?.tooStrongWeakAct(false)
            } else {
                curStageObj.rpPassCnt += 1
                data.rpCorCount += 1 // rpCorrectCount++
                //log.printAlways("   here is GOOD  ...   약하지도 않고..  강하지도 않은...    rpCorrectCount++  >>  \(data.rpCorCount)  ", lnum: 1)
                // bleMan?.messageName = "message_good"
                data.isCorrectAmount = 0;  data.isMessageDone = true
                //// speedLogic?.amountVolumeGood()
            }
            increaseBrthCount()
            //print("    \(curStageObj.arrBreath.count) ")
            log.logThis("       ... count++ :  \(data.count) count++   maxAmount : \(maxAmount)  rpCorrectCount++  >>  \(data.rpCorCount)", lnum: 5)

        }
        log.함수차원_출력_End()
    }
    
    func initializeEntireBreath() { // 전과정에서만.
        log.printThisFunc("initialize Breath 이제부터 호흡", lnum: 5)

        curStageObj.ccTime = data.operationTimer.timeSinceStart - compHoldTimer.GetSecond(0)
            //operationNetTime //data.operationTimer.timeSinceStart - compHoldTimer.GetSecond(0) // 테스트에서 증가하다가 줄어드는 과정  필요..

        log.logThis("   curStageObj.ccTime   >>>   \(curStageObj.ccTime) ", lnum: 3)

        data.operationTimer.resetTime()

        stepMoni = .S_WHOLESTEP_BREATH
        data.isCorrectAmount = 0
        maxAmount = 0

        data.operationTimer.theTime = curStageObj.lastCompObj.strokeTimer.endTime
        //data.gameStartTime = curStageObj.lastCompObj.strokeTimer.endTime  //data.cycleEndTime // NSDate().timeIntervalSince1970
        data.count = 0; data.rpCorCount = 0; isTurning = false;
        log.함수차원_출력_End()
    }

    
    func clearRpCycle() {
        //log.printThisFunc("<<<<<     clear RP Cycle  호흡     >>>>>", lnum: 5)
        maxAmount = 0
        data.isCorrectAmount = 0
        log.함수차원_출력_End()
    }
    
    func startAED() {
        print("\n\n\n  startAED  \n\n\n")
        // 호흡이 시작됐슴.
        // 애니메이션, 상태에 따라 해당 이미지 출력.
        isPause = false;
        // 여기서  cpr5_timer 를 시작하는데... 무슨 의미인가?
    }
    
    func startPreCpr() {
        print("\n\n\n  startPreCpr    ::       startPreCpr      stage = 1   \n\n\n");
        print("  func startPreCpr() {  ")
        health = 0;
        stage = 1;
    }

    /// 호흡이 완료되어 리코일까지 되면 스트로크 시간 세팅 및 최종 종료 확인..
    func recoilAmount() {
        if amount <= Int(RP_FILTER) {
            isTurning = false
            sendBrthStrengthSignalToSpeedLogic()

            clearRpCycle()

            if isAllInOnePrcss {
                curStageObj.markBreathTime()
                speedLogic?.ifSpeedOKofBreath(curStageObj.arrBreath.last!.strkTime)
            } else {
                brthTimer.setDueTime()
                //print("  호흡 시간 >>  speedLogic?.ifSpeedOKofBreath(brthTimer.dueTime)   \(brthTimer.dueTime)")
                speedLogic?.ifSpeedOKofBreath(brthTimer.dueTime)
            }

            brthTimer.Reset()

            // 전과정 제한 : 마지막 세트에는 2회 호흡이면 종료
            if isAllInOnePrcss && stage == stageLimit && data.count == 2 {
                entireStepFinalProcess()
            }
        }
    }


    //////////////////////////////////////////////////////////////////////     [ UI   ViewDidLoad   << >> ]

    func entireStepFinalProcess() {
        log.printThisFunc("entireStep FinalProcess", lnum: 5)

        if curStageObj.rpCount == 0 { // V.1.08.160718 매 카운트마다 기록하므로
            curStageObj.holdTime = compHoldTimer.GetSecond(0)
        }

        data.finalProcess()
        data.showResultCounts(" data.finalprocess ")

        log.logThis("  data. count , rpCorCount \(data.count),  \(data.rpCorCount) ", lnum: 1)
        curStageObj.rpTime = compHoldTimer.timeSinceStart

        if curStageObj.rpCount == 0 {
            curStageObj.rpTime = 9.0
        } else {
            curStageObj.rpTime = curStageObj.holdTime
        }        //print("타임>> rptime세팅 .. 칼크에서 Final  \(curStageObj.rpTime)")

        data.showResultCounts(" increaseState?? ")

        if data.ccCorCount >= Int(CC_PASS) && data.rpCorCount >= Int(RP_PASS) {
            increaseStateOfPatient()
        }

        data.ccCorCount = 0; data.rpCorCount = 0
        entireStepFinished = true
        log.logUiAction ("     checkAmount ::     stage : \(stage)  arrSet : \(data.arrSet.count)   과정 종료  ", lnum: 20)
        stepMoni = .S_WHOLESTEP_AED

        data.showResultCounts(" final ")
        data.세트점수()
        startAED()
    }


    func parseDepthForObjC() -> Double {
        return HsCalculationStatic.parseDepthInCM(Double(maxValueForSL))
    }

    func parseAmountForObjC() -> Double {
        return HsCalculationStatic.parseAmountInML(Double(maxValueForSL))
    }

}
