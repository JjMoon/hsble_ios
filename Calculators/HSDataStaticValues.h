//
//  HSDataStaticValues.h
//  heartisense
//
//  Created by jeongyuchan on 2015. 5. 5..
//  Copyright (c) 2015년 gyuchan. All rights reserved.
//

#import <Foundation/Foundation.h>

#define GVALUE 9.80665   // 중력 가속도 G
#define BUFFER_SIZE 10 // filtering을 위한 버퍼의 크기

extern UInt16 AppIdTrMoTe;

extern int stagexLimit, langIdx, productType; // 0 : Eng, 1 : Kor
// var ble1connected = false, isTrainerConnected = false // 모니터|테스터, 2 : 트레이너
extern bool isManikinLeft, isRescureMode, isCalibrationFinished, viewAnimate, isGlobalMuteOn;

// Current Step ..
typedef NS_ENUM(char, CurStep) {
    S_INIT0, S_STEPBYSTEP_RESPONSE = 3, S_STEPBYSTEP_EMERGENCY, S_STEPBYSTEP_CHECKPULSE,

    S_STEPBYSTEP_SAFETY, S_STEPBYSTEP_AIRWAY, S_STEPBYSTEP_CHECK_BRTH,

    S_STEPBYSTEP_COMPRESSION = 10, S_STEPBYSTEP_BREATH, S_STEPBYSTEP_AED, BypassStepComp, BypassStepBrth,
    S_WHOLESTEP_DESCRIPTION = 15, S_WHOLESTEP_PRECPR, // 이때 one sensor data parsing..
    WholeStepStart,
    S_WHOLESTEP_COMPRESSION = 20, S_WHOLESTEP_BREATH, S_WHOLESTEP_AED, S_WHOLESTEP_RESULT,
    S_READY = 30, S_QUIT, S_STEPBYSTEP_MULTI,
    S_CALIBRATION, S_COMPLETE, CAL_NEW_CC, CAL_NEW_RP, CAL_LOAD  };
//extern CurStep globalState;

extern int CC_FILTER, CC_FILTER_CALI, RP_FILTER; //, force_value, breath_value;
//extern BOOL isTurning, isPause, isCorrect Position, isMessageDone, isCorrectRecoil;


// Bypass related
extern bool  isBypassMode;
//extern enum HrsnsApps { SINGLEMODE, MONITOR10 = 10, TESTER20 = 20 };
//extern enum HrsnsApps BypassAppID;


//extern int cc_count[5]; // = [Int32] (count: 5, repeatedValue: 0),


//extern int CC_START_POINT, CC_END_POINT;		// 피에조센서 Baseline  피에조센서 Endline
//extern int RP_START_POINT, RP_END_POINT;		// 기압센서 Baseline  기압센서 Endline

extern int MESSAGE_NO, MESSAGE_WP, MESSAGE_TW, MESSAGE_TS, MESSAGE_NR, MESSAGE_GD;
// 새로운 상수 : MINIMUM_POINT
extern int MINIMUM_POINT, CC_PRESSURE_MINIMUM_POINT, CC_PIEZO_MINIMUM_POINT, RP_WEAK_POINT, RP_STRONG_POINT;

extern double CC_CM;
extern int CC_WEAK_POINT;		// Weak-Good 경계 이 값 미만은 Weak
extern int CC_STRONG_POINT_DEFAULT;
extern int CC_STRONG_POINT;	// Good-Strong 경계 이 값 초과는 Strong, AHA(미국)일 때 100이고 KACPR(한국) 일 때 91

extern int CC_PRESSURE_POINT;	// 중앙 인식을 위한 최소한의 값

//extern int RP_WEAK_POINT;		// Weak-Good 경계 이 값 미만은 Weak
//extern int RP_STRONG_POINT;	// Good-Strong 경계 이 값 초과는 Strong,

extern double CC_FAST_POINT;	// Fast-Good 경계 이 값 미만은 Fast
extern double CC_LATE_POINT;	// Good-Late 경계 이 값 초과는 Late

extern double CC_CYCLE_START_POINT;	// 주기 Baseline (0초)
extern double CC_CYCLE_END_POINT;	// 주기 Endline (1초)

extern int CC_PASS;		// 가슴압박 성공기준 30회중 23회 이상
extern int RP_PASS;		// 인공호흡 성공기준 2회중 1회이상

extern short RP_PRESSURE_MINIMUM_POINT;

extern int xxisPrint; //, //left_depth, right_depth; // ccCorrectCount, rpCorrectCount

//extern double gameStartTime, cycleStartTime, cycleEndTime;


@interface HSDataStaticValues : NSObject


@end
