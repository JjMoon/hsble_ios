//
//  HsCalcMain.swift
//  Trainer
//
//  Created by Jongwoo Moon on 2016. 5. 31..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation


class HsLogicBase: NSObject {
    var arrPack = [HsPack](), count = 0, corCount = 0
    var nextLogic: HsLogicBase?
    var cur: HsPack?, strk: HmUnitStoke?

    // Logic related
    var isTurning = false, mx = MinMax()


    func processParse(packet: HsPack) -> HsLogicBase {
        arrPack.append(packet)
        cur = packet



        // set min, max



        return self
    }

    //
    func increaseCount() {

    }
}

class HsCompLogic: HsLogicBase {
    override func processParse(packet: HsPack) -> HsLogicBase {
        super.processParse(packet)

        if isTurning {

        } else {

        }

        return self
    }

}


class HsBrthLogic: HsLogicBase {
    override func processParse(packet: HsPack) -> HsLogicBase {
        super.processParse(packet)

        return self
    }
}


class HsASet: NSObject {
    var comp: HsCompLogic?, brth: HsBrthLogic?

    convenience init(compress: HsCompLogic, breath: HsBrthLogic) {
        self.init()
        comp = compress; brth = breath
    }

    override init() {
        super.init()
    }

}


class HsCalcMain: NSObject {
    var arrSet = [HsASet]()
    var compObj = HsCompLogic(), brthObj = HsBrthLogic(), curLogic: HsLogicBase // 처음 객체는 버림..
    var myLimitPo = 0, myStaPo = 0, myEndPo = 0, rpStaPo = 0, rpEndPo = 0

    override init() {
        curLogic = compObj
        super.init()
        increaseSet()
    }

    func parsePacket(packet: UnsafeMutablePointer<UInt8>) {
        let aPack = HsPack(packet: packet)

        aPack.lComp = Int(Float(aPack.pzLeft - (myStaPo)) / Float((myEndPo) - (myStaPo)) * 100)
        aPack.rComp = Int(Float(aPack.pzRigt - (myStaPo)) / Float((myEndPo) - (myStaPo)) * 100)
        aPack.comp = Int(Float(aPack.pzForc - myStaPo) / Float(myEndPo - myStaPo) * 100)  // 그래프 값...
        aPack.brth = Int(Float(aPack.airPress - (rpStaPo)) / Float((rpEndPo) - (rpStaPo)) * 100)


        curLogic = curLogic.processParse(aPack) // 여기서 스위치가 일어남..

    }


    func increaseSet() {
        compObj = HsCompLogic(); brthObj = HsBrthLogic() // 새로 생성해서..
        compObj.nextLogic = brthObj; brthObj.nextLogic = compObj // 이렇게 서로를 다음 실행할 것으로 지정함.
        arrSet.append(HsASet(compress: compObj, breath: brthObj))
    }



}


