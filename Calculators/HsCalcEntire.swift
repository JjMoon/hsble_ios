//
//  HsCalcEntire.swift
//  Trainer
//
//  Created by Jongwoo Moon on 2015. 12. 22..
//  Copyright © 2015년 IMLab. All rights reserved.
//

import Foundation

class HsCalcEntire : NSObject {
    var log = HtLog(cName: "전과정 obj")
    
    static var singltn = HsCalcEntire()
    var calcObj : HsCalcManager?
    var bleMan: HsBleManager! { get { return calcObj!.bleMan } }
    var data: HsData! { get { return calcObj!.data } }
    
    var sttMch = HtGeneralStateManager()
    var compMch = HtGeneralStateManager(), brthMch = HtGeneralStateManager()
    
    
    // calc var
    var arrWrongPs = [Bool](count: 4, repeatedValue: true)
    var max_depth = 0, min_depth = 100, isCorrectRecoil = true, wrongPositionNum = -1,
    position = [Bool] (count: 4, repeatedValue: false),
    positionResult = [Bool] (count: 4, repeatedValue: false)
    
    //////////////////////////////////////////////////////////////////////     _//////////_     [ Compression      << >> ]    _//////////_   압 박
    // MARK: 파싱 불리는 곳.
    func parseStartingHere() {
        sttMch.update()
    }

    // MARK: 상태 머신 세팅
    func setVariables(calc: HsCalcManager) { // prepare 에서 불림.
        calcObj = calc
        sttMch = HtGeneralStateManager()
        var aState = HtUnitState(duTime: 12, myName: "Description")
        aState.entryAction = { ()->() in
            
        }
        sttMch.addAState(aState)

        aState = HtUnitState(duTime: 12, myName: "Entire_Compression")
        aState.entryAction = { ()->() in
            // 초기값 세팅
            self.compressionStateStartActions()
        }
        aState.durinAction = {
            self.compMch.update()
        }
        sttMch.addAState(aState)
        
        aState = HtUnitState(duTime: 12, myName: "Entire_Breath")
        aState.entryAction = { ()->() in
            
        }
        aState.durinAction = {
            self.brthMch.update()
        }
        sttMch.addAState(aState)
    }
    
    // MARK: Compression related ...
    private func compressionStateStartActions() {
        log.printThisFunc("compressionStateStartActions", lnum: 10)
        data.isCorrectPosition = true; data.isCorrectDepth = 0
        isCorrectRecoil = true; wrongPositionNum = -1
        max_depth = 0; min_depth = 100
        position = [Bool] (count: 4, repeatedValue: false)
        positionResult = [Bool] (count: 4, repeatedValue: false)
        data.operationTimer.resetTime()
        data.count = 0; data.ccCorCount = 0
        log.함수차원_출력_End()
    }
    
    private func setCompressionStateMachine() { // 압박 상태머신
        compMch = HtGeneralStateManager()
        var aState = HtUnitState(duTime: 12, myName: "Pushing")
        aState.entryAction = { ()->() in
            
        }
        aState.durinAction = {
            self.setWrongPosition()
            self.checkPosition()
            self.checkDepth()
        }
        compMch.addAState(aState)

        aState = HtUnitState(duTime: 12, myName: "Recoil")
        aState.entryAction = { ()->() in
            
        }
        aState.durinAction = {
            self.setWrongPosition()
            self.checkRecoil()
        }
        compMch.addAState(aState)
        
        compMch.prepareActions()
    }
    
    private func setBreathStateMachine() { // 호흡 상태머신
        compMch = HtGeneralStateManager()
        var aState = HtUnitState(duTime: 12, myName: "Pushing")
        aState.entryAction = { ()->() in
            
        }
        aState.durinAction = {
            self.setWrongPosition()
            self.checkPosition()
            self.checkDepth()
        }
        compMch.addAState(aState)
        
        aState = HtUnitState(duTime: 12, myName: "Recoil")
        aState.entryAction = { ()->() in
            
        }
        aState.durinAction = {
            self.setWrongPosition()
            self.checkRecoil()
        }
        compMch.addAState(aState)
        
        compMch.prepareActions()
    }

    
    func increaseCompCount() {
        log.printThisFunc("  압박 카운트 증가 () ")
//        curStageObj.lastCompObj.period = curStageObj.lastCompObj.strokeTimer.timeSinceStart
//        log.logThis("  curStageObj.lastCompObj.period : \(curStageObj.lastCompObj.period)   gameStartTime : \(data.operationTimer.timeSinceStart)    \(curStageObj.arrComprs.count) ")
//        data.count++
//        let newStroke = HmCompStroke()
//        newStroke.maxDepth = max_depth
//        curStageObj.arrComprs.append(newStroke)  // 최대값 저장.
//        log.logThis("     압박 카운트 증가  ... count++ :  \(data.count) count++   mad depth : \(max_depth)  cc cor count", lnum: 1)
        log.함수차원_출력_End()
    }

    
    func compressionDataParsing() {
        
    }
    
    func breathDataParsing() {
        
    }
    
    func setWrongPosition() {
        arrWrongPs[0] = !calcObj!.isValueNotBiggerThan0x00(UInt8(calcObj!.dir), shift: 4)
        arrWrongPs[3] = !calcObj!.isValueNotBiggerThan0x00(UInt8(calcObj!.dir), shift: 5)
        arrWrongPs[2] = !calcObj!.isValueNotBiggerThan0x00(UInt8(calcObj!.dir), shift: 6)
        arrWrongPs[1] = !calcObj!.isValueNotBiggerThan0x00(UInt8(calcObj!.dir), shift: 7)
    }
    
    func checkRecoil() {
        
    }
    
    func checkPosition() {
        
    }
    
    func checkDepth() {
        
    }
    
}