//
//  HCStatic.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2015. 10. 6..
//  Copyright © 2015년 gyuchan. All rights reserved.
//

import Foundation

class HCStatic : NSObject {
    
    // Data 파일의 Static
    //static ACSUtility HeartiSense_Bluetooth;
    //static ACSUtility HeartiSense_Bluetooth_search;
    
    
    //static var isPause = false
    //static var messageType = 0

    
    //static var is_send_start = true, is_send_info = true
    
    
    // 5개 짜리 어레이..
    
//     
//    static func calculateForce(x: UInt16) -> Int32 {
//        print("  x : \(x) ")
//        let a = 2.62239001285986
//        let b = 0.00398209350025777
//        let c = -0.000000810748753229992
//        let xxx:UInt64 = x * x
//        let x2 = Double(xxx)
//        let y = exp(a + b * Double(x) + c * x2)
//        
//        print (" y : \(y)")
    //
    //        return Int32(y);
    //    }
    
    static func valueBetween(pVal: Int, pMin: Int, pMax: Int) -> Int {
        var rVal = pVal
        if pVal < pMin {
            rVal = pMin
        }
        if pMax < rVal {
            return pMax
        }
        return rVal //        var rVal = (pVal < pMin)? pMin: pVal
    }
    
    static func uint8TosignedByte(pInt8: UInt8) -> Int {
        if pInt8 > 127 {
            return (Int(pInt8) - 256)
        }
        return Int(pInt8)
    }
    
    
    static func httpTest() {
        
        let url = NSURL(string: "http://192.168.0.23/httpTest/index.php?q=IMLAB")
        let request = NSURLRequest(URL: url!)
        
        print("url   \(url)")
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()) {
            (response, data, error) in
            
            print("  data  \(data)  response  \(response)  error \(error)")
            //print(NSString(data: data!, encoding: NSUTF8StringEncoding))
        }
        
    }
    
}

