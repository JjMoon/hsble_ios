//
//  HtScore.swift
//  HS Monitor
//
//  Created by Jongwoo Moon on 2016. 7. 19..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation
import Swift

/**
    점수 계산 로직 :: 9. 21.
 */

class CPR_Scoring_return {
    var comp_depth_score = 0.0, comp_recoil_score = 0.0, comp_rate_score = 0.0, comp_pos_score = 0.0,
     comp_num_score = 0.0, resp_amount_score = 0.0, resp_rate_score = 0.0, resp_num_score = 0.0

    var comp_score = 0.0, resp_score = 0.0, fraction_score = 0.0, total_score = 0.0

    init() {    }

    convenience init(_ pcomp_depth_score: Double, _ pcomp_recoil_score: Double, _ pcomp_rate_score: Double,
                       _ pcomp_pos_score: Double, _ pcomp_num_score: Double, _ presp_amount_score: Double,
                         _ presp_rate_score: Double, _ presp_num_score: Double, _ pcomp_score: Double,
                           _ presp_score: Double, _ pfraction_score: Double, _ ptotal_score: Double) {
        self.init()
        comp_depth_score = pcomp_depth_score
        comp_recoil_score = pcomp_recoil_score
        comp_rate_score = pcomp_rate_score
        comp_pos_score = pcomp_pos_score
        comp_num_score = pcomp_num_score
        resp_amount_score = presp_amount_score
        resp_rate_score = presp_rate_score
        resp_num_score = presp_num_score
        comp_score = pcomp_score
        resp_score = presp_score
        fraction_score = pfraction_score
        total_score = ptotal_score
    }

    convenience init(_ pcomp_score: Double, _ presp_score: Double, _ pfraction_score: Double, _ ptotal_score: Double) {
        self.init()
        comp_score = pcomp_score;
        resp_score = presp_score;
        fraction_score = pfraction_score;
        total_score = ptotal_score;
    }
}


class CPR_Scoring_scurve {
    var F_low = 0.0
    var F_high = 0.0
    var Alpha = 0.0
    var T0 = 0.0

    var X1 = 0.0, X2 = 0.0, Y1 = 0.0, Y2 = 0.0

    init() {

    }

    convenience init(xa1 : Double, xa2 : Double, ya1 : Double, ya2: Double, time1: Double,  time2: Double,  pvalue1: Double,  pvalue2: Double) {
        self.init()
        X1 = xa1;        X2 = xa2;
        Y1 = ya1 / 100;        Y2 = ya2 / 100;
        let value1 = pvalue1 / 100;        let value2 = pvalue2 / 100;

        Alpha = (log(1 / value1 - 1) - log(1 / value2 - 1)) / (time2 - time1);
        T0 = log(1 / value1 - 1) / Alpha + time1
        let k1 = 1 + exp( -Alpha * (X1 - T0))
        let k2 = 1 + exp( -Alpha * (X2 - T0))
        F_low = (k2 * Y2 - k1 * Y1) / (k2 - k1);
        F_high = k1 * Y1 - (k1 - 1) * F_low;
    }

    convenience init(_ xa1 : Double, _ xa2 : Double, _ ya1 : Double, _ ya2: Double, _ time1: Double, _ time2: Double, _ pvalue1: Double, _ pvalue2: Double) {
        self.init(xa1: xa1, xa2 : xa2, ya1 : ya1, ya2: ya2, time1: time1,  time2: time2,  pvalue1: pvalue1,  pvalue2: pvalue2)
    }

    func calculate(x : Double) -> Double {
        var result = 0.0
        //print("  result : \(result)   \(x)  \(X1) \(X2)  \(Y1) \(Y2)")

        if (x <= X1) {
            result = Y1;
        } else if (x >= X2) {
            result = Y2;
        } else {
            result = F_low + (F_high - F_low) / (1 + exp(-Alpha * (x - T0)));
            if (result > max(Y1, Y2)) { result = max(Y1, Y2)  }
            if (result < min(Y1, Y2)) { result = min(Y1, Y2)  }
        }
        return result
    }
}

//////////////////////////////////////////////////////////////////////     [ << CPR_Scoring >> ]
class CPR_Scoring {
    //var CC _WEAK_POINT = 58, CC_ STRONG_POINT = 78, RP _WEAK_POINT = 35, RP _STRONG_POINT = 70
    var CM_5_POINT = 50, CM_6_POINT = 20, ML_400_POINT = 11, ML_700_POINT = 11
    var CPR_GUIDELINE = cprProtoN.theVal + 1 // 일단 글로번로 초기 세팅.
    let AHA_KACPR = 1, ERC = 2, ANZCOR = 3

    var use_comp_count = true, use_comp_pos = true, use_comp_rate = true, use_comp_depth = true,
    use_comp_recoil = true

    var use_resp_count = true, use_resp_amount = true, use_resp_rate = true

    var comp_count_curve_inc = CPR_Scoring_scurve(15, 30, 0, 100, 20, 25, 25, 75)
    var comp_count_curve_dec = CPR_Scoring_scurve(30, 45, 100, 0, 35, 40, 75, 25)
    var comp_pos_curve = CPR_Scoring_scurve(0, 100, 0, 100, 9.64, 90.36, 7.27, 92.73)
    var comp_rate_curve_inc = CPR_Scoring_scurve(54.9, 100, 20, 100, 62.2, 92.7, 13.2, 86.8)
    var comp_rate_curve_dec = CPR_Scoring_scurve(120, 165.1, 100, 20, 127.3, 157.8, 86.8, 13.2)
    var comp_depth_curve_inc = CPR_Scoring_scurve(32, 50, 0, 100, 38, 44,	39.4, 60.6)
    var comp_depth_curve_dec = CPR_Scoring_scurve(60, 78, 100, 0, 66, 72, 60.6, 39.4)
    var comp_recoil_curve = CPR_Scoring_scurve(5, 18.1, 100, 0, 8.45, 14.65, 3.65, 96.35)

    var resp_amount_curve_inc = CPR_Scoring_scurve(0, 35, 35, 100, 13.3, 21.7, 35.9, 64.1)
    var resp_amount_curve_dec = CPR_Scoring_scurve(70, 105, 100, 35, 83.3, 91.7, 64.1, 35.9)

    var fraction_curve = CPR_Scoring_scurve(40, 70, 0, 100, 46.88, 63.12, 8.68, 91.32)

    var comp_resp_ratio = 0.75

    init() { }

    convenience init(c_count: Bool, c_pos: Bool, c_rate: Bool, c_depth: Bool, c_recoil: Bool, r_count: Bool,
                     r_amount: Bool, r_rate: Bool, ratio: Double,
                     cm_5: Int, cm_6: Int, ml_400: Int, ml_700: Int, guideline: Int) {
                     //cc_weak: Int, cc_strong: Int, rp_weak: Int, rp_strong: Int) {
        self.init()
        use_comp_count = c_count;
        use_comp_pos = c_pos;  use_comp_rate = c_rate;  use_comp_depth = c_depth;  use_comp_recoil = c_recoil;

        use_resp_count = r_count;        use_resp_amount = r_amount;        use_resp_rate = r_rate;
        comp_resp_ratio = ratio;
        CM_5_POINT = cm_5; CM_6_POINT = cm_6; ML_400_POINT = ml_400; ML_700_POINT = ml_700;
        CPR_GUIDELINE = guideline
    }

    func set_CPR_Scoring_factor(c_count: Bool, c_pos: Bool,
                                c_rate: Bool,  c_depth: Bool,  c_recoil: Bool,  r_count: Bool,
                                r_amount: Bool,  r_rate: Bool, ratio: Double) {
        use_comp_count = c_count;
        use_comp_pos = c_pos;        use_comp_rate = c_rate;
        use_comp_depth = c_depth;        use_comp_recoil = c_recoil;
        use_resp_count = r_count;        use_resp_amount = r_amount;
        use_resp_rate = r_rate;        comp_resp_ratio = ratio;
    }

    func cal_resp_count(x: Int) -> Double {
        var result = 0.0
        let abs_x = abs(2 - x)

        switch (abs_x) {
        case 0:
            result = 1;
            break;
        case 1:
            result = 0.6;
            break;
        default:
            result = 0;
            break;
        }

        return result;
    }

    func cal_resp_rate(x: Int)-> Double {
        var result = 0.0
        let abs_x = abs(8 - x)

        switch (abs_x) {
        case 0...4:
            result = 1;
            break;
        case 5:
            result = 0.75;
            break;
        case 6:
            result = 0.5;
            break;
        case 7:
            result = 0.25;
            break;
        default:
            result = 0;
            break;
        }

        return result;
    }

    func parse_rate(rate: Double) -> Double {
        return 60 / rate
    }

    func parse_depthXX(depth: Double) -> Double {
        // HsCalculationStatic.parseDepthInCM
        var result = 0.0
        //let dWeakPoint = Double(CC_WEAK_POINT), dStrgPoint = Double(CC_STRONG_POINT)
        let point6 = Double(CM_6_POINT), point5 = Double(CM_5_POINT)
        if (depth < point5) {
            result = depth * 5.0 / point5
        } else if (depth <= point6) {
            result = 5.0 + ((depth - point5) * 1.0 / (point6 - point5))
        } else {
            result = depth * 6.0 / point6
        }
        return result * 10
    }

    private func applyScoreOption() {
        use_comp_depth = scoreOption[0]
        use_comp_recoil = scoreOption[1]
        use_comp_rate = scoreOption[2]
        use_comp_pos = scoreOption[3]
        use_comp_count = scoreOption[4]
        use_resp_amount = scoreOption[5]
        use_resp_rate = scoreOption[6]
        use_resp_count = scoreOption[7]

        /// 세팅값 적용..
        comp_resp_ratio = Double(scoreWeight)
        //print(" *&*& comp_resp_ratio :: \(comp_resp_ratio)  ")
    }

    func calculate_score(comp_count: [Int], comp_pos: [Int], comp_rate: [[Double]],  comp_depth: [[Int]],
                         comp_recoil: [[Int]], resp_count: [Int], resp_amount: [[Int]], resp_rate: [[Double]],
                         fraction: Double,  set_num: Int) -> CPR_Scoring_return {
        var total_score = 1.0
        var comp_score = 0.0
        var resp_score = 0.0
        var fraction_score = 1.0
        var set_comp_score = [Double](count: set_num, repeatedValue: 0.0)
        var set_resp_score = [Double](count: set_num, repeatedValue: 0.0)
        var comp_count_score = 0.0, comp_pos_score = 0.0, comp_rate_score = 0.0, comp_depth_score = 0.0,
         comp_recoil_score = 0.0, resp_count_score = 0.0, resp_amount_score = 0.0, resp_rate_score = 0.0

        applyScoreOption()

        for i in 0..<set_num { // print("\n\n\n calculate_score :: >>   세트 :  \(i)  ")

            set_comp_score[i] = 1;
            set_resp_score[i] = 1;

            if (use_comp_count) {
                var count_point = 1.0;
                if (comp_count[i] < 30) { // 160726 set_comp_score[i] *= comp_count_curve_inc.calculate(Double(comp_count[i]))
                    count_point = comp_count_curve_inc.calculate(Double(comp_count[i]))
                } else if (comp_count[i] > 30) { // 160726 set_comp_score[i] *= comp_count_curve_dec.calculate(Double(comp_count[i]))
                    count_point = comp_count_curve_dec.calculate(Double(comp_count[i]))
                }
                set_comp_score[i] *= count_point
                comp_count_score += count_point / Double(set_num);
                print("  comp_count_score  ::  \(comp_count_score) 160726")
            }
            //print(" 압박 스코어 : \(set_comp_score[i])   count : \(comp_count[i])  use_comp_count ")

            if (use_comp_pos) {
                let pos_point: Double = comp_pos_curve.calculate(Double(comp_pos[i]))
                set_comp_score[i] *= pos_point
                comp_pos_score += pos_point / Double(set_num) //  print("  comp_pos_score  ::  \(comp_pos_score) 160726")
            } //print(" 압박 스코어 : \(set_comp_score[i])   use_comp_pos ")
            if (use_comp_rate) {
                var rate_sum = 0.0, rate_value = 0.0, rate_point = 0.0
                var is_first = true
                for k in comp_rate[i] {
                    if (is_first) {
                        rate_sum += 1;
                        is_first = false;
                    } else {
                        rate_value = parse_rate(k);
                        if (rate_value < 100) {
                            rate_sum += comp_rate_curve_inc.calculate(rate_value);
                        } else if (rate_value <= 120) {
                            rate_sum += 1;
                        } else {
                            rate_sum += comp_rate_curve_dec.calculate(rate_value);
                        }
                    }
                }
                // 160726 set_comp_score[i] *= rate_sum / Double(comp_rate[i].count)
                rate_point = rate_sum / Double(comp_rate[i].count)
                set_comp_score[i] *= rate_point;
                comp_rate_score += rate_point / Double(set_num) //print("  comp_rate_score  ::  \(comp_rate_score)  160726")
            }
            //print(" 압박 스코어 : \(set_comp_score[i])   use_comp_rate ")

            if (use_comp_depth) {
                var depth_sum = 0.0, depth_value = 0.0, depth_point = 0.0
                for k in comp_depth[i] {
                    depth_value = HsCalculationStatic.parseDepthInCM(Double(k))
                    if depth_value < 50 {
                        depth_sum += comp_depth_curve_inc.calculate(depth_value)
                    } else if CPR_GUIDELINE == AHA_KACPR || depth_value <= 60 {
                        depth_sum += 1;
                    } else {
                        depth_sum += comp_depth_curve_dec.calculate(depth_value)
                    }
                    // print("calculate_score :: >>   comp_depth \(k)   \(depth_sum)   \(depth_sum / Double(comp_depth[i].count))")
                }
                // 160726  set_comp_score[i] *= depth_sum / Double(comp_depth[i].count)
                depth_point = depth_sum / Double(comp_depth[i].count)
                set_comp_score[i] *= depth_point;
                comp_depth_score += depth_point / Double(set_num)
                print("  comp_depth_score :: \(comp_depth_score) 160726")
            }

            if (use_comp_recoil) {
                var recoil_sum = 0.0, recoil_value = 0.0, recoil_point = 0.0
                for k in comp_recoil[i] {
                    //recoil_value = parse_depth(Double(k))
                    recoil_value = HsCalculationStatic.parseDepthInCM(Double(k))
                    recoil_sum += comp_recoil_curve.calculate(recoil_value);
                }
                // 160726 set_comp_score[i] *= recoil_sum / Double(comp_recoil[i].count)
                recoil_point = recoil_sum / Double(comp_recoil[i].count)
                set_comp_score[i] *= recoil_point;
                comp_recoil_score += recoil_point / Double(set_num)
                print("  comp_recoil_score  ::  \(comp_recoil_score)  160726")
            }

            //////////////////////////////////////////////////////////////////////     [   호  흡    << >> ]

            if (use_resp_count) {
                // 160726 set_resp_score[i] *= cal_resp_count(resp_count[i]);
                var count_point = 0.0
                count_point = cal_resp_count(resp_count[i]);
                set_resp_score[i] *= count_point;
                resp_count_score += count_point / Double(set_num)
                //print("  resp_count_score  ::  \(resp_count_score)   \(count_point)  \(set_num) 160726")
            }

            if use_resp_amount && resp_count[i] != 0 {
                var amount_sum = 0.0, amount_point = 0.0
                for k in resp_amount[i] {
                    if (k < ML_400_POINT) { //if (k < RP_WEAK_POINT) {
                        amount_sum += resp_amount_curve_inc.calculate(Double(k))
                    } else if (k <= ML_700_POINT) { //} else if (k <= RP_STRONG_POINT) {
                        amount_sum += 1;
                    } else {
                        amount_sum += resp_amount_curve_dec.calculate(Double(k))
                    } //print("calculate_score :: >>   amount_sum \(k)   \(amount_sum) ")
                }
                // 160726  set_resp_score[i] *= amount_sum / Double(resp_amount[i].count)
                amount_point = amount_sum / Double(resp_amount[i].count)
                set_resp_score[i] *= amount_point;
                resp_amount_score += amount_point / Double(set_num)
                print("  resp_amount_score  ::  \(resp_amount_score)  \(set_resp_score[i])  160726")
            }

            if use_resp_rate && resp_count[i] != 0 {
                var rate_sum = 0.0, rate_value = 0.0, rate_point = 0.0
                var is_first = true;
                for k in resp_rate[i] {
                    rate_value = parse_rate(k);
                    if (is_first) {
                        rate_sum += 1;
                        is_first = false;
                    } else {
                        print("\t\t\t  rate Value :: \(rate_value)   round : \(round(rate_value))")
                        rate_sum += cal_resp_rate(Int(round(rate_value)))
                    }
                }
                // 160726  set_resp_score[i] *= rate_sum / Double(resp_rate[i].count)
                //print("    set_resp_score[i] >> \(i) :: \(set_resp_score[i])  ")
                rate_point = rate_sum / Double(resp_rate[i].count)
                set_resp_score[i] *= rate_point
                resp_rate_score += (rate_point / Double(set_num))
                //print("  resp_rate_score  ::  \(resp_rate_score)   rateSum : \(rate_sum) / \(resp_rate[i].count)  160726")
                //print("    set_resp_score[i] >> \(i) :: \(set_resp_score[i])  ")
            }
            comp_score += (set_comp_score[i] / Double(set_num))
            resp_score += (set_resp_score[i] / Double(set_num))
            //print(" 이번 세트까지 comp   \(comp_score)       resp   \(resp_score) ")
        }
        //print("\n\n   comp_score : \(comp_score)     resp_score : \(resp_score)    fraction_score  >>  \(fraction_score)")
        //print("   fraction : \(fraction)  으로부터..")

        fraction_score = fraction_curve.calculate(fraction)        //print("   fraction_score : \(fraction_score)  이 나오고.. ..")
        total_score = (comp_score * comp_resp_ratio * 0.01 + resp_score * (1 - comp_resp_ratio * 0.01)) * fraction_score;
        //print("\n\n  세팅 값 .. 압박..  : \(comp_resp_ratio)  \n\n")

        if total_score < 0 { total_score = 0 }
        print(" total score  \(total_score) ")

        //return CPR_Scoring_return(comp_score, resp_score, fraction_score, total_score);

        return CPR_Scoring_return(comp_depth_score, comp_recoil_score, comp_rate_score, comp_pos_score, comp_count_score,
            resp_amount_score, resp_rate_score, resp_count_score, comp_score, resp_score,
            fraction_score, total_score)
    }
}
