//
//  HsPack.swift
//  Trainer
//
//  Created by Jongwoo Moon on 2016. 5. 31..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation


class MinMax: NSObject{
    var min: Double = 0, max: Double = 0

    func setVals(mn: Double, mx: Double) {
        min = mn; max = mx
    }

    func copyFrom(tar: MinMax) {
        min = tar.min; max = tar.max
    }

    func copyFlipFrom(tar: MinMax) {
        min = tar.max; max = tar.min
    }

    func setAllWith(newVal: Double) {
        min = newVal; max = newVal


    }

    func valStr() -> String {
        return " X: \(max), N: \(min)"
    }
}

/// 패킷 하나에 해당하는 데이터 단위.
class HsPack : NSObject {

    //////////////////////////////////////////////////////////////////////     _//////////_     [ Compression      << >> ]
    var rawData: UnsafeMutablePointer<UInt8> = nil

    var xV = MinMax(), yV = MinMax(), zV = MinMax(), flipped = false
    var pzLeft = 0, pzRigt = 0, pzForc = 0
    var airPress = 0

    var comp = 0, brth = 0, lComp = 0, rComp = 0

    var arrWP = [Bool](count: 4, repeatedValue: true) // wrong position
    

    // Util
    var isWrPos: Bool { get { return !(arrWP[0] && arrWP[1] && arrWP[2] && arrWP[3]) }}

    // 4 debugging.
    var forceStr: String { get { return "Force : <L\(pzLeft) : R\(pzRigt)> \(pzForc) " }}
    var brethStr: String { get { return "airPress : \(airPress) " }}

    var distaStr: String { get { return "Dist : <X: \(xV.min) \(xV.max)> \t<Y: \(yV.min) \(yV.max)> \t<Z: \(zV.min) \(zV.max)>" }}
    var wrPosStr: String { get { return "Wrong : <H:\(arrWP[0]) L:\(arrWP[3]) R:\(arrWP[2]) B:\(arrWP[1]) > " }}


    convenience init(packet: UnsafeMutablePointer<UInt8>) {
        self.init()
        rawData = packet
        parse()
    }

    override init() {
        super.init()
    }

    //////////////////////////////////////////////////////////////////////     _//////////_     [ private      << >> ]
    private func byteToDouble(idx: Int) -> Double {
        return 0





        //return Double(HCStatic.uint8TosignedByte(rawData[idx])) / 10
    }

    private func readDistInMM(ix: Int) -> Double {
        return byteToDouble(ix) / 10
    }

    private func parsetDistance() {
        xV.setVals(readDistInMM(8), mx: readDistInMM(9))
        yV.setVals(readDistInMM(10), mx: readDistInMM(11))
        zV.setVals(readDistInMM(12), mx: readDistInMM(13))
    }




    /// //  return (dir & (0x01 << sht)) > 0x00;
    func isOneAtBit(shift: Int, target: Int) -> Bool {
        return (target & (0x01 << shift)) > 0x00
    }


    ///   - (UInt16)parseTwoBytes:(unsigned char*)data at:(int)idx {
    func parseTwoBytes(idx: Int) -> Int {
        //print("  \(rawData[idx])   \(rawData[idx + 1])")
        return (Int(rawData[idx]) << 8) + Int(rawData[idx+1])
    }


    private func parseWrongPosition() {
        let dir = 0
        //arrWP[0] = !isValueNotBiggerThan0x00(UInt8(dir), shift: 4) // arrP 0, 3, 2, 1 이었슴..
        arrWP[0] = !isOneAtBit(4, target: dir)


//        arrWP[3] = !isValueNotBiggerThan0x00(UInt8(dir), shift: 5) //
//        arrWP[2] = !isValueNotBiggerThan0x00(UInt8(dir), shift: 6) // Leg
//        arrWP[1] = !isValueNotBiggerThan0x00(UInt8(dir), shift: 7) // arrP 순서 : 머리, 왼, 다리, 오른.
    }

    private func showMyself() {
        print("\t \(forceStr) /t \(brethStr) /t \(distaStr) ")
        if isWrPos { print("\t\t \(wrPosStr)") }
    }

    /// 일단 들어오면 모두 파싱.
    func parse() {

        // 압박값 파싱.
        let piezo1_value = parseTwoBytes(3)
        let piezo2_value = parseTwoBytes(5)
        //pzLeft = Int(HSDataCalculator.calculateForce(piezo1_value))
//        pzRigt = Int(HSDataCalculator.calculateForce(piezo2_value))
        //pzForc = pzLeft + pzRigt

        // 호흡값 파싱.
        // airPress = Int(parseTwoBytes(rawData, at: 17)) // 기압..  이 값이 0 이면 연결 유실.


        print(" piezo 1 : \(piezo1_value)  2 : \(piezo2_value)")

        // 가속도 값.
        parsetDistance()

        //showMyself()



    }


}