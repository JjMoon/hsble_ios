//
//  HSDataCalculator.m
//  heartisense
//
//  Created by jeongyuchan on 2015. 4. 26..
//  Copyright (c) 2015년 gyuchan. All rights reserved.
//

#import <math.h>
#import "HSDataCalculator.h"
#import "HSStepDataCalculator.h"
#import "HSDataStaticValues.h"
#import "Common-Swift.h"
#import "NSObject+util.h"


@implementation HSDataCalculator{
    
    BOOL set_init_value;
    int piezo_value;
    int count_atm;
    int standard_atm;
    int breath_value;
    int sum_atm;
    
    int count_pressure;
    int standard_pressure;
    int sum_pressure;
}

@synthesize dataObj, cc_cali_value, rp_cali_value;

+ (int)halfOfAbsSum:(int)first sub1:(int)subA sub2:(int)subB {
    return (int)((abs(first - subA) + abs(first - subB)) / 2);
}

+ (int)calculateForce:(UInt16)x {
    //float a = 2.62239001285986, b = 0.00398209350025777, c = -0.000000810748753229992;
    float a = 3.58322799547958, b = 0.00153868372576938, c = 0.000000113167937766884;
    UInt64 xxx = x * x;
    double x2 = (double)xxx;
    double y = exp(a + b * (double)(x) + c * x2);
    
    return (int)y;
}


+ (double)vectorSum:(double)a second:(double)b  thrid:(double)c {
    return sqrt(pow(a, 2) + pow(b, 2) + pow(c, 2));
}

+ (void)responseCalculator:(NSData*)data successBlock:(void (^)(HSSensorStatus result, HSCommendList cmd))successBlock {

    [self logCallerMethodwith:@" << +++ >> " newLine:1];
    
    unsigned char val[data.length];
    [data getBytes:&val length:data.length];
    
    UInt16 value = val[2] & 0xFF;
    
    // [self logCallerMethodwith:[NSString stringWithFormat:@"<< cmd : %d, value : %d >>", cmd, value] newLine:3];
    // 010 2831 4277
    
    NSLog(@" HSDataCalculator :: responseCalculator     >>   value : %d", value);
    
    switch (value) {
        case 1:
            [self sensorInfomationResult:val sensor:HSBattry resultBlock:successBlock];
            break;
        case 2:
            [self sensorInfomationResult:val sensor:HSPiezo resultBlock:successBlock];
            break;
        case 3:
            [self sensorInfomationResult:val sensor:HSAcceleration resultBlock:successBlock];
            break;
        case 4:
            [self sensorInfomationResult:val sensor:HSATMCheck resultBlock:successBlock];
            break;
        case 5:
            [self sensorInfomationResult:val sensor:HSBluetooth1 resultBlock:successBlock];
            break;
        case 6:
            [self sensorInfomationResult:val sensor:HSBluetooth2 resultBlock:successBlock];
            break;
        default:
            break;
    }
}

+ (void)sensorInfomationResult:(unsigned char*)data sensor:(HSCommendList)cmd resultBlock:(void (^)(HSSensorStatus result, HSCommendList cmd))resultBlock {
    [self logCallerMethodwith:@" << +++ >> " newLine:1];
    
    int code = data[3] & 0xFF;
    switch (code) {
        case 0:
            resultBlock(HSSensorConnected,cmd);
            break;
        case 1:
            resultBlock(HSSensorNotConnected,cmd);
            break;
        case 2:
            resultBlock(HSSensorError,cmd);
            break;
        default:
            resultBlock(HSSensorNotDefineError,cmd);
            break;
    }
}


- (id)init {
    self = [super init];
    cc_cali_value = 30; rp_cali_value = 70;
    return self;
}


- (void)setCaliValue {
    
    [self logCallerMethodwith:[NSString stringWithFormat:@"  BleManiger 에서 직접 불림.  ....  기존 cc, rp Cali 값.. %d  %d  ", cc_cali_value, rp_cali_value ] newLine:20];

    if (cc_cali_value < 0 || 100 < cc_cali_value) cc_cali_value = 30;
    if (rp_cali_value < 0 || 100 < rp_cali_value) rp_cali_value = 70;
    
    //dataObj.ccLimitPoint = (cc_cali_value) * 16 + 20; //  CC_LIMIT_POINT = (cc_cali_value) * 16 + 20;
    dataObj.ccLimitPoint = (cc_cali_value) * 32 + 20; //  CC_LIMIT_POINT = (cc_cali_value) * 16 + 20;
    dataObj.rpLimitPoint = (rp_cali_value) * 10 + 50; //  RP_LIMIT_POINT = (rp_cali_value) * 10 + 50;
    
    dataObj.ccEndPoint = dataObj.ccStaPoint + dataObj.ccLimitPoint; //    CC_END_POINT = CC_START_POINT + CC_LIMIT_POINT;
    dataObj.rpEndPoint = dataObj.rpStaPoint + dataObj.rpLimitPoint; //  RP_LIMIT_POINT;

    NSLog(@" dataObj.ccLimitPoint : %ld  cc Sta : End :    %ld : %ld ", dataObj.ccLimitPoint, dataObj.ccStaPoint, dataObj.ccEndPoint);
}

-(void)resetCounter {
    set_init_value = false;
    count_atm = count_pressure = sum_atm = sum_pressure = standard_atm = standard_pressure = 0;
}

//////////////////////////////////////////////////////////////////////       [ sensorDataParsing ]
#pragma mark - 센서 데이터 수신.
- (void)sensorDataParsing:(NSData*)data successBlock:(void (^)())successBlock {
    // called from BleManager  ==> [cal sensorDataParsing:data successBlock:^{
    
    unsigned char datas[data.length];
    [data getBytes:&datas length:data.length];
    
    // calculate force ...  식 적용...   piezo_value = ([self parseTwoBytes:datas at:3] + [self parseTwoBytes:datas at:5])/ 2;
    piezo_value = [HSDataCalculator  calculateForce:[self parseTwoBytes:datas at:3]] +
    [HSDataCalculator  calculateForce:[self parseTwoBytes:datas at:5]];
    
    breath_value = [self parseTwoBytes:datas at:17];
    //piezo_value = (((datas[3] << 8) & 0xFF00) + (datas[4] & 0xFF) + ((datas[5] << 8) & 0xFF00) + (datas[6] & 0xFF)) / 2;    // 피에조 센서 데이터 수신
    //breath_value = ((datas[14] << 8) & 0xFF00) + (datas[15] & 0xFF);    // 기압 센서 데이터 수신 ..  수정 전... 14, 15

    [self logCallerMethodwith:@"   40으로 변경한 함수   <<  CalibrationFlow 11  >> " newLine:1];
    
    //NSLog(@"\n\nWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW  HSDataCalculator :: sensorDataParsing ");
    NSLog(@" data : %@ ,  count  > atm : %d, pressure : %d , setInitVal : %d ", data,
          count_atm, count_pressure, set_init_value);
    NSLog(@" piezo : %d , breath : %d, sum atm : %d,  sum press : %d ", piezo_value, breath_value, sum_atm, sum_pressure);
    
    //if(successBlock == nil)  [self logCallerMethodwith:@" successBlock == nil  " newLine:2];
    
    if (!set_init_value) {
        if (count_atm < 10) {            // 기압센서의 10개의 데이터를 모을 때가지 진행
            if (abs(standard_atm - breath_value) < 100) {    // 새로운 값이 기존 값과 10 미만의 차이를 보인다면
                sum_atm += breath_value;          // 기존 값에 누적하여 데이터를 저장함
                count_atm++;
            } else {              // 새로운 값이 기존 값과 10 이상의 차이를 보인다면
                sum_atm = breath_value;            // 새 값을 기준으로 다시 10개의 데이터를 모음
                count_atm = 1;
                standard_atm = breath_value;
            }
        }
        
        if (count_pressure < 10) {          // 피에조센서의 10개의 데이터를 모을 때가지 진행
            // MOOON :: 150831  10은 너무 작아 40으로 늘림.
            if (abs(standard_pressure - piezo_value) < 100) {  // 10    // 새로운 값이 기존 값과 10 미만의 차이를 보인다면
                sum_pressure += piezo_value;          // 기존 값에 누적하여 데이터를 저장함
                count_pressure++;
            } else {              // 새로운 값이 기존 값과 40 이상의 차이를 보인다면
                NSLog(@"\n\n  RESET count_pressure = 1;   >>>   standard_pressure - piezo_value  ===>>> %d - %d = %d  > 40  ",
                      standard_pressure, piezo_value, abs(standard_pressure - piezo_value));
                
                sum_pressure = piezo_value;          // 새 값을 기준으로 다시 10개의 데이터를 모음
                //count_pressure = 1;  이건 스킵하자..
                standard_pressure = piezo_value;
            }
        }
        
        [self logCallerMethodwith:[NSString stringWithFormat:@" set_init_value : %d, count : %d, %d ",
                                   set_init_value, count_atm, count_pressure] newLine:0];
        
        if (count_atm == 10 && count_pressure == 10) {      // 기압/피에조 센서의 데이터가 모두 10개 모였다면
            if (dataObj == nil)  NSLog(@"\n\n\n\n\n   HsDataCalculator.m    dataObj == nil  \n\n\n\n\n");

            dataObj.rpStaPoint = sum_atm / 10;          // 기압의 Baseline을 세팅
            dataObj.rpEndPoint = dataObj.rpStaPoint + dataObj.rpLimitPoint;      // 기압의 Endline을 세팅
            //RP_END_POINT = RP_START_POINT + RP_LIMIT_POINT;      // 기압의 Endline을 세팅
            dataObj.ccStaPoint = sum_pressure / 10;        // 피에조의 Baseline을 세팅
            dataObj.ccEndPoint = dataObj.ccStaPoint + dataObj.ccLimitPoint;   //CC_END_POINT = CC_START_POINT + CC_LIMIT_POINT;      // 피에조의 Endline을 세팅
            set_init_value = true;            // Baseline 세팅이 완료 됨을 알림
            

            NSLog(@"\n\n\n\n\n");
            NSLog(@"================================================================================================================");
            NSLog(@"  \t\t   초기 캘리브레이션 값.. ::   CC_START_POINT : %ld, CC_END_POINT : %ld, RP_START_POINT : %ld, RP_END_POINT : %ld    ", (long)dataObj.ccStaPoint, (long)dataObj.ccEndPoint, (long)dataObj.rpStaPoint, (long)dataObj.rpEndPoint);
            NSLog(@"================================================================================================================");
            NSLog(@"\n\n\n\n\n");

            //operation 중단
            //다음 페이지로 이동
            [self logCallerMethodwith:@"   다음 페이지로 이동   <<  CalibrationFlow 21  >>  " newLine:5];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self logCallerMethodwith:@" HSDataCalculator :: sensorDataParsing  count == 10 and willCall > successBlock ()   <<  CalibrationFlow 22  >>  " newLine:10];
                successBlock();
            });
        }
    }
    
    [self logCallerMethodwith:@"   40으로 변경한 함수   <<  EO Method  >> " newLine:1];
}

@end

//3456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
//////////////////////////////////////////////////////////////////////       [ CLASSname ]


//
//
////////////////////////////////////////////////////////////////////////       [ sensorCalibrationData ]
//#pragma mark - 추후 캘리브레이션 알고리즘 개발 예정
//
//- (void)xxxxxsensorCalibrationData:(NSData*)data successBlock:(CalibrationBlock)successBlock {  // 요건 안 불리는 것 같음..
//    [self logCallerMethodwith:@"<<  CalibrationFlow 10  >> " newLine:1];
//    NSLog(@"___ \t\t\t data :  \t %@    추후 캘리브레이션 알고리즘 개발 예정 Flow04 ", data);
//
//    //추후 캘리브레이션 알고리즘 개발 예정
//    unsigned char datas[data.length];
//    [data getBytes:&datas length:data.length];
//
//    UInt16 code = (datas[3] & 0xFF);
//    UInt16 select = (datas[4] & 0xFF);
//    UInt16 value = (datas[5] & 0xFF);
//
//    NSLog(@"  code : %d, select : %d, value : %d", code, select, value);
//
//
//    if (code == 2) {                        // 요청했던 캘리브레이션 데이터 수신
//        if (select == 1) {                        // 가슴압박 캘리브레이션 값
//            if (value >= 0 && value <= 100) {          // 0~100 사이 값일경우 해당 데이터 저장 후 호흡 캘리브레이션값 호출
//                cc_cali_value = value;
//
//                NSLog(@"   압력 모듈 >>  cc_cali_value =  %d ", cc_cali_value);
//
//                successBlock(HSPressureModule, HSCaliSuccessType, 0);  // MOOON ::
//            } else {                                   // 0~10 범위 밖 값인경우(데이터가 없는경우) 현재 캘리브레이션
//
//                cc_cali_value = (dataObj.ccLimitPoint - 20) / 16;   // 캘리브레이션 값을 0~100 스케일로 조정
//
//                NSLog(@"   압력 모듈 << 비정상 값  조정 후 >>  cc_cali_value =  %d ", cc_cali_value);
//
//                successBlock(HSPressureModule,HSCaliEditType,cc_cali_value);               // 키트에 캘리브레이션 값 저장
//            }
//        } else if (select == 2) {                // 인공호흡 캘리브레이션 값
//            if (value >= 0 && value <= 100) {          // 0~100 사이 값일경우 해당 데이터 저장 후 BT1 상태 정보 요청
//                rp_cali_value = value;
//                [self setCaliValue];
//                successBlock(HSATMModule,HSCaliSuccessType,0);               // 키트에 캘리브레이션 값 저장
//            } else {                                   // 0~10 범위 밖 값인경우(데이터가 없는경우) 현재 캘리브레이션
//                rp_cali_value = (dataObj.rpLimitPoint - 50) / 10;   // 캘리브레이션 값을 0~100 스케일로 조정
//                successBlock(HSATMModule,HSCaliEditType,rp_cali_value);                // 키트에 캘리브레이션 값 저장
//            }
//        }
//    } else if (code == 3) {                  // 키트에 캘리브레이션 값 저장 완료
//        if (select == 1) {                        // 가슴압박 캘리브레이션 완료. 호흡 캘리브레이션 값 호출
//            successBlock(HSPressureModule,HSCaliSuccessType,0);
//        } else if (select == 2) {                 // 인공호흡 캘리브레이션 완료. BT1 상태 정보 요청
//            [self setCaliValue];
//            successBlock(HSATMModule,HSCaliSuccessType,0);
//        }
//    }
//}
//
//
