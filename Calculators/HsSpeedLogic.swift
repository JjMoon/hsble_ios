//
//  HsSpeedLogic.swift
//  Trainer
//
//  Created by Jongwoo Moon on 2016. 3. 28..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation

/**
 
 StepByStepVC 에서 생성하여 멤버로 갖고 있으며 CalcManager 에 assign 하여 공유한다.
 
 우선 순위 
 > Wrong Position
 > Strong | Weak
 > Recoil
 > Fast | Slow
 > Good

 
 */

/////  호흡 ...  Good  Too fast 가 뜬다.

class HsSpeedLogic: HtDelayTimer {
    var log = HtLog(cName: "HsSpeedLogic")

    var resetTimer: NSTimer? // 1초 안에 아무일 없으면 모두 초기화하는 타이머..
    // DelayTimer :: theTimer

    var idxOdr = 10
    var arrAll = [UIView?]() // 모든 뷰..  라벨, 이미지 토탈..
    var arrLabel = [UILabel](), arrSignImg = [UIImageView]()
    var lblTitleSpeed : UILabel?, lblSpeedValue: UILabel? // 타이틀.  100 / min 속도 표시..
    var lblSlow : UILabel?, lblGood : UILabel?, lblFast : UILabel?
    var imgSlow : UIImageView?, imgGood : UIImageView?, imgFast : UIImageView?
    var imgUpperSignBoard : UIImageView? // 상부 메시지 이미지.
    var imgRecoil: UIImageView? // 하부 메시지 .. 리코일만 쓰임..

    var refreshDepthVolumn = false

    // 표현 관련 색, 상태 등등
    var activeLabelColor = UIColor.whiteColor(), inactiveLabelColor = UIColor.grayColor()

    // 색깔별 점.. 빨강, 녹색, 노랑.
    var imgFastName = "cpr_rate_fast", imgGoodName = "cpr_rate_good", imgSlowName = "cpr_rate_slow",
    imgInactive = "cpr_rate_inactivated"

    // 큰 그림 메시지들...
    var slowSign = UIImage(named: "message_slow"), fastSign = UIImage(named: "message_fast"),
    tooWeakSign = UIImage(named: "message_weak"), tooStrongSign = UIImage(named: "message_strong"),
    wrongPosiSign = UIImage(named: "message_wrongposition"),
    recoilSign = UIImage(named: "message_notrelease"),
    goodSign = UIImage(named: "message_good")

    override func setDelay(delay: Double, closure: () -> ()) { // 이게 0.1초 딜레이 됨...
        super.setDelay(delay, closure: closure)
        defaultDelayTime = 0.01 // sec
    }

    //////////////////////////////////////////////////////////////////////       [ UI Sub  ]
    
    override func cancel() {
        super.cancel()
    }

    /// 배경 같은 뷰를 여기서 추가..
    func addTotalViews(anyView: UIView?...) {
        arrAll += anyView
    }

    func addSingleView(aVw: UIView) {
        arrAll.append(aVw)
    }

    func setLabels(title: UILabel?, speedVal: UILabel?, slow: UILabel?, good: UILabel?, fast: UILabel?) {
        lblSpeedValue = speedVal // 신설 됨..
        lblTitleSpeed = title; lblSlow = slow; lblGood = good; lblFast = fast
        arrLabel = [lblSlow!, lblGood!, lblFast!]
        addTotalViews(title, speedVal, slow, good, fast)
    }

    func setImages(slw: UIImageView?, god: UIImageView?, fst: UIImageView?, sign: UIImageView?, sign2: UIImageView?) {
        imgSlow = slw; imgGood = god; imgFast = fst; imgUpperSignBoard = sign; imgRecoil = sign2
        arrSignImg = [ slw!, god!, fst! ] // , sign!, sign2! ]
        addTotalViews(slw, god, fst, sign, sign2)
        signboard.targetImgVw = imgUpperSignBoard
    }

    func hideOrShowAll(show: Bool) {
        for vw in arrAll { vw?.show_다음이_참이면(show) }
    }

    /// 글자 회색으로 만들고, 신호등은 모두 회색으로....
    func deactivateAll() {
        imgUpperSignBoard?.hideMe(); imgRecoil?.hideMe()
        lblTitleSpeed?.showMe(); lblSpeedValue?.showMe()
        deactivateSpeedSign()
    }

    /// 신호등 색깔 / 라벨 초기화...
    private func deactivateSpeedSign() {
        for vw in arrLabel { vw.textColor = inactiveLabelColor }
        for vw in arrSignImg { vw.image = UIImage(named: imgInactive) }
    }

    /** 우선 순위
     > Wrong Position   0 순위
     > Strong | Weak    1
     > Recoil           2
     > Fast | Slow      3
     > Good               이것도 0 순위로 해야...
     
     호흡  은 살짝 다르게..  세기가 good 이면 3으로 했다가 속도까지 좋으면 최종 good 으로 판정..
     > 세기  weak | good | strong
     > 속도  slow | ---- | fast
     */

    /// 최 우선 사항이므로 무조건 보여짐.
    func wrongPositionAct() { // 0 순위
        //log.printAlways("wrongPositionAct", lnum: 5)
        idxOdr = 0
        setMessageAndShow(wrongPosiSign!)
    }

    func tooStrongWeakAct(isWeak: Bool) { // 1 순위
        if idxOdr < 1 { return }
        //log.printAlways("tooStrongWeakAct : weak? \(isWeak)", lnum: 5)
        if isWeak { setMessageAndShow(tooWeakSign!) }
        else { setMessageAndShow(tooStrongSign!) }
        idxOdr = 1
    }

    func recoilAct() { // 2 순위
        if idxOdr < 2 { return }
        //log.printAlways("recoilAct", lnum: 5)
        setMessageAndShow(recoilSign!, isUp: false)
        idxOdr = 2
    }

    func finallyGoodAct() {
        idxOdr = 0
        setMessageAndShow(goodSign!)
    }

    /// 모니터에서 override ..   트레이너에서도 필요..   호흡만 ...
    func amountVolumeGood() {
        //print("    amountVolumeGood()
        idxOdr = 3  // 세기니까..  good = 3..  세기 에러는 1 ..
        setMessageAndShow(goodSign!)
    }

    /// 트레이너  상/하  사인보드 ...    메시지 보였다가 끄는 부분..
    private func setMessageAndShow(img: UIImage, isUp: Bool = true) {
        //print("  HsSpeedLogic ::  setMessageAndShow")
        theTimer.invalidate()
        deactivateAll()
        if isUp {
            imgUpperSignBoard?.image = img
            imgUpperSignBoard?.showMe()
        } else {
            imgRecoil?.image = img //여긴 항상 리코일임..
            imgRecoil?.showMe()
        }
        setDelay(0.7) {
            self.deactivateAll();  self.idxOdr = 10
            //print("  closure   idxOdr  \(self.idxOdr)")
        }
    }

    /// 신호등 켜기 전 리셋하는 부분..
    private func speedPanelUpdate(sTime: Double) {
        deactivateSpeedSign()
        setResetTimer()

        /// 0.4초 안에 초기화하지 않으므로 ....  초기화 없다.
        if sTime < 0.5 { // Fast
            lblFast?.textColor = activeLabelColor // 글자 색..
            imgFast?.image = UIImage(named: imgFastName) // 신호등 이미지.
        } else if 0.5 <= sTime && sTime < 0.6 {           // 정상이므로 true 리턴..
            lblGood?.textColor = activeLabelColor
            imgGood?.image = UIImage(named: imgGoodName)
            return
        } else { // Slow
            lblSlow?.textColor = activeLabelColor
            imgSlow?.image = UIImage(named: imgSlowName)
        }
    }

    ///  압박 : 이 함수를 불러 실제 액션 실행...  여기서 UI 를 처리하고, Calc 객체에 정상인지 여부를 알려준다.
    func ifSpeedOKandProceedUIactions(strokeTime: Double) -> Bool {
        //print("  ifSpeedOKandProceedUIactions   idxOdr  \(idxOdr)")
        speedPanelUpdate(strokeTime)

        refreshDepthVolumn = true

        // 여기서 105 / min 이거 세팅...
        if 0.05 < strokeTime {
            let eaPerMin = 60 / strokeTime
            lblSpeedValue?.text = " \(Int(eaPerMin))  / \(langStr.obj.minute)"
        } else {
            lblSpeedValue?.text = " 0 / \(langStr.obj.minute)"
        }

        if idxOdr < 3 { return 0.5 <= strokeTime && strokeTime < 0.6 }
        //log.printThisFNC("ifSpeedOKandProceedUIactions", comment: "  time :  0.5 < ? \(strokeTime)  < ? 0.6 ")

        deactivateAll();        theTimer.invalidate()

        /// 0.4초 안에 초기화하지 않으므로 ....
        if strokeTime < 0.5 { // Fast
            imgUpperSignBoard?.image = fastSign // 메시지 이미지.
            lblFast?.textColor = activeLabelColor // 글자 색..
            imgFast?.image = UIImage(named: imgFastName) // 신호등 이미지.
        } else if 0.5 <= strokeTime && strokeTime < 0.6 {           // 정상이므로 true 리턴..
            lblGood?.textColor = activeLabelColor
            imgGood?.image = UIImage(named: imgGoodName)
            return true
        } else { // Slow
            imgUpperSignBoard?.image = slowSign
            lblSlow?.textColor = activeLabelColor
            imgSlow?.image = UIImage(named: imgSlowName)
        }
        setDelay(0.4) {
            self.imgUpperSignBoard?.hideMe()
        }
        imgUpperSignBoard?.showMe() // 너무 빠르거나 느릴 때 여기까지 온다.
        return false
    }


    var signboard = HtSpeedLogicOrder()

    /// 호흡 :
    func ifSpeedOKofBreath(strokeTime: Double) {

        refreshDepthVolumn = true
        var eaPerMin = 0.1
        // 여기서  분당 호흡 횟수 :: 105 / min 이거 세팅...
        if 0.1 < strokeTime {
            eaPerMin = 60 / strokeTime
            lblSpeedValue?.text = " \(Int(eaPerMin))  / \(langStr.obj.minute)"
        } else {
            lblSpeedValue?.text = " 0 / \(langStr.obj.minute)"
        }        //print("  ifSpeedOKofBreath(strokeTime    idxOdr  \(idxOdr)   ea / Min : \(eaPerMin) ")
        // speedPanelUpdate(strokeTime)
        //deactivateAll();        theTimer.invalidate()
        deactivateSpeedSign()

        switch eaPerMin {
        case 0...4:
            //imgSlowFastSign?.image = slowSign
            lblSlow?.textColor = activeLabelColor
            imgSlow?.image = UIImage(named: imgSlowName)
//            if signboard.setMessageAndShow(slowSign!, odr: 5) {
//                theTimer.invalidate()
//            }
        case 4...12:
            lblGood?.textColor = activeLabelColor
            imgGood?.image = UIImage(named: imgGoodName)
            if idxOdr == 3 {
                print("if idxOdr == 3 {  goodSign ")//setMessageAndShow(goodSign!)
                theTimer.invalidate()
                signboard.setMessageAndShow(goodSign!, odr: 0)
            }
            setResetTimer()
        default:
            //imgSlowFastSign?.image = fastSign // 메시지 이미지.
            lblFast?.textColor = activeLabelColor // 글자 색..
            imgFast?.image = UIImage(named: imgFastName) // 신호등 이미지.
//            if signboard.setMessageAndShow(fastSign!, odr: 5) {
//                theTimer.invalidate()
//            }
        }

        //setDelay(0.4) {  self.imgSlowFastSign?.hideMe()    }
        //imgSlowFastSign?.showMe() // 너무 빠르거나 느릴 때 여기까지 온다.
    }

    /// 트레이너 호흡 시 초기화 안되는 현상에 대한 부가적 조치.
    private func setResetTimer() {
        if resetTimer != nil {
            resetTimer?.invalidate()
        }
        resetTimer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: #selector(self.deactivateAll),
                                                            userInfo: nil, repeats: false)
    }

    // MARK:  언어 세팅.
    func setLanguageString() {
        lblSlow?.text = langStr.obj.slow
        lblGood?.text = langStr.obj.good_rate
        lblFast?.text = langStr.obj.fast
    }

}



class HtSpeedLogicOrder: HtDelayTimer {
    var targetImgVw: UIImageView?
    var prevOdr = 10

    func setMessageAndShow(img: UIImage, odr: Int) -> Bool {
        print("\t\t\t\t\t\t    set message")
        if prevOdr < odr { return true }  // 우선순위가 밀리면 True 리턴..
        targetImgVw!.showMe()
        targetImgVw!.image = img
        theTimer.invalidate()
        theTimer = NSTimer.scheduledTimerWithTimeInterval(0.7, target: self, selector: #selector(self.deactivateAll),
                                                            userInfo: nil, repeats: false)

        return false // 세팅 되었슴.
    }

    func deactivateAll() {
        print("\t\t\t\t\t\t    deactivate  ....   target image hide ... ")
        targetImgVw?.hideMe()
        prevOdr = 10
    }

}



