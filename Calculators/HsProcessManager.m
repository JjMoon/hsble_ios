//
//  HsProcessManager.m
//  heartisense
//
//  Created by Jongwoo Moon on 2015. 9. 11..
//  Copyright (c) 2015년 gyuchan. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "HsProcessManager.h"
#import "NSObject+Util.h"
//#import "HsBleManager.h"

//#import "HSEnumSet.h"

#import "HSStepDataCalculator.h"




//////////////////////////////////////////////////////////////////////     [ HsProcessManager   private ]
@interface HsProcessManager()
//////////////////////////////////////////////////////////////////////////////////////////
{
//    BOOL checkRespirationCaliSetData;
//    BOOL checkBluetooth1Info;
    
    bool isOperating;
    
    int voidBlockRepeat;
    
    HSStepDataCalculator *stepCal;
    HSCalculatorType calType;
    HSStepByStepSelectedType pageType;
    
//    void (^blockUpdateDevListView) (HSCentralManager*);
//    void (^blockVoid)(void);
    
    
}


//@property (nonatomic, weak) HSPeripheral *hsPeripheral;
//@property (nonatomic, weak) HSBaseService *hsBaseService;

@end

//////////////////////////////////////////////////////////////////////     _//////////_     [ HsBleManager ]    _//////////_
@implementation HsProcessManager
//////////////////////////////////////////////////////////////////////////////////////////

@synthesize mtmTimer;
//@synthesize arrUnitJob, arrBle;


-(void)startCompressProcess
{
    
    pageType = CompressionType;
    
    
    [HsBleManager.inst operationStart];
    
    
    //ProgressSetting
    //  [self showProgressView:YES];
    
    /*
     * button 활성화 셋팅  UI ....
     *
    if(_userMode == HSlayRescuerMode){
        [self setButtonView:2];
    }
    if(_userMode == HShealthcareProviderMode){
        [self setButtonView:3];
    } */
    
    
    /*
     * 기존 재생되던 소리를 멈춘다.
     */
    
    
    /*
     * 이미지를 변경한다.
     */
       /*
     * 재생되던 소리가 완료되면 자동으로 CPR이 시작된다.
     */
    
    
    
    //timer 작동
//    [_timerLabel setHidden:NO];
//    [_timerLabel reset];
//    [_timerLabel setTimerType:MZTimerLabelTypeTimer];
//    [_timerLabel setCountDownTime:17];
//    [_timerLabel startWithEndingBlock:^(NSTimeInterval countTime) {
//        //timer 끝
//        
//        // MOOON
//        [stepCal setStart:NO];
//        [HsBleManager.inst operationStop];
//    }];
    
    
    
    //Calculator setting
    calType = HSCompressionCalculatorType;
    [stepCal startCompression];
    [stepCal setStart:YES];

    
}

-(void)stopCompressProcess
{
    
}

-(void)startBreathProcess
{
    
}

@end
