//
//  HSDataStaticValues.m
//  heartisense
//
//  Created by jeongyuchan on 2015. 5. 6..
//  Copyright (c) 2015년 gyuchan. All rights reserved.
//

#import "HSDataStaticValues.h"

///  0 : Trainer, 1 : Monitor, 2 : Test
UInt16 AppIdTrMoTe = 0; // 0 : Trainer, 1 : Monitor, 2 : Test 트레이너는 그냥 세팅 안해도 됨..

int stageLimit = 3, langIdx = 0, guideLine = 0, productType = -1;
bool isManikinLeft = true, isRescureMode, isCalibrationFinished = false, viewAnimate = false,
isGlobalMuteOn = false;  // isTrainerConnected = false


// current Step
CurStep currentStep;

int CC_FILTER = 10, CC_FILTER_CALI = 50, RP_FILTER = 10; //, force_value, breath_value;

//BOOL isTurning, isPause, isCorrect Position, isMessageDone, isCorrectRecoil;


// Bypass related
bool isBypassMode;
//enum HrsnsApps BypassAppID = SINGLEMODE;

//int cc_count[5]; //


//int CC_START_POINT, CC_END_POINT;
//int RP_START_POINT, RP_END_POINT;		// 기압센서 Baseline  기압센서 Endline

int MESSAGE_NO = 0, MESSAGE_WP = 1, MESSAGE_TW = 2, MESSAGE_TS = 3, MESSAGE_NR = 4, MESSAGE_GD = 5;

int MINIMUM_POINT = 55, CC_PRESSURE_MINIMUM_POINT = 20, CC_PIEZO_MINIMUM_POINT = 0,
RP_WEAK_POINT = 35, RP_STRONG_POINT = 70;

//#warning 나중에 초기값을 셋팅값에 따라 가져오도록 한다.
//int CC_LIMIT_POINT, CC_LIMIT_POINT_NEW, CC_LIMIT_POINT_TEMP;
//int RP_LIMIT_POINT, RP_LIMIT_POINT_NEW, RP_LIMIT_POINT_TEMP;


double CC_CM = 13;
int CC_WEAK_POINT = 58; //65;		// Weak-Good 경계 이 값 미만은 Weak
int CC_STRONG_POINT_DEFAULT = 84;
int CC_STRONG_POINT = 84; // 160730  78; // CC_CM * 6 도 마찬가지.  100   // Good-Strong 경계 이 값 초과는 Strong, AHA(미국)일 때 100이고 KACPR(한국) 일 때 91

int CC_PRESSURE_POINT = 15;	// 중앙 인식을 위한 최소한의 값

int RP_START_POINT;		// 기압센서 Baseline
int RP_END_POINT;		// 기압센서 Endline


double CC_FAST_POINT = 0.5;	// Fast-Good 경계 이 값 미만은 Fast
double CC_LATE_POINT = 0.65;	// Good-Late 경계 이 값 초과는 Late

double CC_CYCLE_START_POINT = 0;	// 주기 Baseline (0초)
double CC_CYCLE_END_POINT = 1;	// 주기 Endline (1초)
//
int CC_PASS = 23;		// 가슴압박 성공기준 30회중 23회 이상
int RP_PASS = 1;		// 인공호흡 성공기준 2회중 1회이상
//

short RP_PRESSURE_MINIMUM_POINT = 15;

int isPrint;
//left_depth = 0, right_depth = 0;

//double gameStartTime, cycleStartTime, cycleEndTime;

@implementation HSDataStaticValues

@end
