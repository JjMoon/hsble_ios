//
//  HsSpeedLgcMon.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2016. 3. 29..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation


class HsSpeedLgcMon: HsSpeedLogic {
    var mLblDepth: UILabel?, mLblSpeed: UILabel?, mLblWrong: UILabel?, mLblRecoil: UILabel?
    var ble: HsBleCtrl?

    //let colWeak = UIColor(hex: "f8b000"), colGood = UIColor(hex: "69b97b"), colStrong = UIColor(hex: "e54411"),
    let colWeak = colBttnYellow, colGood = colBttnGreen, colStrong = colBttnRed,
    colFast = colBttnRed, colSlow = colBttnYellow,
    colFail = colBttnRed, colWrong = colBttnRed

    var depthTxt = "", speedTxt = "", recoilTxt = ""
    var depthCol = colBttnDarkGray, speedCol = colBttnDarkGray, recoilCol = colBttnDarkGray

    override init() {
        super.init()
        log.clsName = "HsSpeedLgc Mon"
    }

    internal func setMonitorLabels(depth: UILabel, spd: UILabel, wrong: UILabel, recoil: UILabel) {
        mLblDepth = depth; mLblSpeed = spd; mLblWrong = wrong; mLblRecoil = recoil
        deactivateAll()
        arrLabel = [ depth, spd, recoil ] // wrong 이 왜 빠졌을까?  따로 논다..
        for vw in arrLabel + [ wrong ] { vw.cornerRad(2) }
    }

    /// 깊이 : Weak, Good, Strong    /// 위치 : Wrong    /// 속도 : Slow, Good, Fast    /// 이완 : Good, Fail

    /** 우선 순위
     > Wrong Position   0 순위
     > Strong | Weak    1
     > Recoil           2
     > Fast | Slow      3
     > Good               이것도 0 순위로 해야...
     */

    //////////////////////////////////////////////////////////////////////     [ UI 관련 override << >> ]

    override func tooStrongWeakAct(isWeak: Bool) {
        log.printThisFunc("tooStrongWeakAct : weak? \(isWeak)", lnum: 2)
        if isWeak {
            log.logThis("depthTxt = 'WEAK'; depthCol = colWeak", lnum: 1)
            depthTxt = ble!.progressValueWithUnit() //"WEAK"; 
            depthCol = colWeak
        } else {
            log.logThis("depthTxt = 'STRONG'; depthCol = colStrong", lnum: 1)
            depthTxt = ble!.progressValueWithUnit() // "STRONG";
            depthCol = colStrong
        }
        log.함수차원_출력_End()
    }

    override func amountVolumeGood() {
        depthTxt = ble!.progressValueWithUnit() // "GOOD";
        depthCol = colGood
    }

    /// Recoil 불량 ..
    override func recoilAct() {
        //log.printAlways("recoilAct", lnum: 5)
        recoilTxt = "FAIL"; recoilCol = colFail
    }

    override func finallyGoodAct() {
        depthTxt = ble!.progressValueWithUnit() // "GOOD";
        depthCol = colGood
        speedTxt = "\(eaPerMin.format(".0"))" // "GOOD";
        speedCol = colGood
        recoilTxt = "GOOD"; recoilCol = colGood
    }


    ///  압박 : 이 함수를 불러 실제 액션 실행...  여기서 UI 를 처리하고, Calc 객체에 정상인지 여부를 알려준다.
    override func ifSpeedOKandProceedUIactions(strokeTime: Double) -> Bool {
        var rVal = false

        if 0.1 < strokeTime {
            eaPerMin = 60 / strokeTime
        }

        /// 0.4초 안에 초기화하지 않으므로 ....
        if strokeTime < 0.5 { // Fast
            tooSlowFastAct(false)
        } else if 0.5 <= strokeTime && strokeTime < 0.6 {           // 정상이므로 true 리턴..
            speedTxt = "\(eaPerMin.format(".0"))" // "GOOD";
            speedCol = colGood
            rVal = true
        } else { // Slow
            tooSlowFastAct(true)
        }

        for vw in arrLabel + [ mLblRecoil! ] { vw.showMe() } // 호흡에서 끄고.. 안 켤 경우..

        showAfterStroke()
        return rVal
    }

    var eaPerMin = 0.01
    /// 호흡 :
    override func ifSpeedOKofBreath(strokeTime: Double) {
        eaPerMin = 0.1
        if 0.1 < strokeTime {
            eaPerMin = 60 / strokeTime
        }
        print(" Breath >>>dkddk    \(strokeTime)  \(eaPerMin)")

        switch eaPerMin {
        case 0...4:
            tooSlowFastAct(true)
        case 4...12:
            speedTxt = "\(eaPerMin.format(".0"))" // "GOOD";
            speedCol = colGood
        default:
            tooSlowFastAct(false)
        }

        showAfterStroke()
    }

    /// 글자 회색으로 만들고, 신호등은 모두 회색으로....
    override func deactivateAll() {
        for lbl in arrLabel + [ mLblWrong! ]{
            setMessageAndShow(lbl, txt: "", col: colBttnDarkGray)
        }
        // 초기화..  리코일은  showAfterStroke 에서 다시 처리.
        recoilTxt = ""
        if ble != nil {
            depthTxt = ble!.progressValueWithUnit() // "GOOD";
        }
        depthCol = colGood
    }

    //////////////////////////////////////////////////////////////////////     [  public << >> ]

    /// 최 우선 사항이므로 무조건 보여짐.
    func wrongPositionAct(isOn: Bool) { // 독립적으로 온 / 오프..  화살표도 같이..
        //log.printAlways("wrongPositionAct", lnum: 5)
        if isOn {
            setMessageAndShow(mLblWrong!, txt: "WRONG", col: colWrong)
        } else {
            setMessageAndShow(mLblWrong!, txt: "", col: colBttnDarkGray)
        }
    }

    //////////////////////////////////////////////////////////////////////     [  private << >> ]
    private func tooSlowFastAct(isSlow: Bool) {
        //log.printAlways("tooSlowFastAct : slow? \(isSlow)", lnum: 5)
        if isSlow {
            speedTxt = "\(eaPerMin.format(".0"))" // "SLOW";
            speedCol = colSlow
        } else {
            speedTxt = "\(eaPerMin.format(".0"))" // "FAST";
            speedCol = colFast
        }
    }

    private func showAfterStroke() {
        theTimer.invalidate()
        setMessageAndShow(mLblDepth!, txt: depthTxt, col: depthCol)
        setMessageAndShow(mLblSpeed!, txt: speedTxt, col: speedCol)
        if recoilTxt != "FAIL" {
            recoilTxt = "GOOD"; recoilCol = colGood
        }
        setMessageAndShow(mLblRecoil!, txt: recoilTxt, col: recoilCol)
        setDelay(0.4) { 
            self.deactivateAll()
        }
    }

    private func setMessageAndShow(lbl: UILabel, txt: String, col: UIColor) {
        lbl.backgroundColor = col
        lbl.text = txt
        lbl.textColor = UIColor.whiteColor()
    }

}
