//
//  HSStepDataCalculator.m
//  heartisense
//
//  Created by jeongyuchan on 2015. 5. 2..
//  Copyright (c) 2015년 gyuchan. All rights reserved.
//

#import "HSStepDataCalculator.h"
#import "HSDataStaticValues.h"
#import "Common-Swift.h"
#import "NSObject+util.h"


@implementation HSStepDataCalculator
{
    BOOL is_quit; //페이지에서 나갔는지 체크
    
    BOOL is_start;    // 가슴압박/인공호흡 시작 종료 플래그
    BOOL is_timer_on;  // 타이머 동작 플래그(주어진 시간에 30번을 다한 경우 is_start가 off 되지만 타이머는 보여줘야 함 )
    
    long game_start_time;  // is_start가 on 되는 순간 시간 값을 저장(ms)
    
    int count;        // 가슴압박/인공호흡 횟수
    
    BOOL is_turning;    // 가슴압박/인공호흡에서 최저점->최고점 일때 fasle, 최고점->최저짐일 때 true
    
    int is_correct_depth, is_correct_amount;      // depth 판단(0:good 1:weak 2:strong), amount 판단(0:good 1:weak 2:strong)
    BOOL is_correct_position, is_correct_recoil;    // position 판단, recoil 판단
    
    int is_print;      // 2프레임당 1번씩 UI 출력하기 위해 사용.
    
    BOOL is_message_done;    // is_message_done이 true이면, 현재 사이클에서 메세지를 이미 출력했음을 의미(Wrong position, too weak, too strong, not fully recoil, good)
    int message_type;        // 출력할 message 타입
    long message_start_time;    // message 출력 시작 시간(이시간부터 0.4초간 출력)
    
    // ==============================================================================
    
    // ==============================================================================
    int depth, max_depth, min_depth;  // depth : 현재 깊이. max_depth : 최대 깊이(압력 최대점). min_depth : 최소 깊이(압력 최소점)
    double acc_value, max_acc_depth, min_acc_depth;  // acc_value : 현재 가속도값, max_acc_depth : 최대 깊이(가속도 최대점), min_acc_depth(가속도 최소점)
    int amount, max_amount, min_amount;  // amount : 현재 기압값. max_amount : 최대 기압. min_amount : 최소 기압값

    BOOL position[4]; // 현재 눌리고 있는 방향 스위치
    BOOL position_result[4]; // 한 사이클 동안 눌린 방향 스위치
    
    double acc_init_value;// 가속도 센서에 기본적으로 들어오는 중력 가속도 G를 뺴기 위한 값,
    // 센서가 움직이지 않을 때 읽은 값을 G라고 인식, 이후 들어오는 데이터와 기존 acc_init_value를 합쳐서 새로운 acc_init_value 만듬
    int acc_value_count; // 읽어들인 acc_value 갯수(filtering에 쓸 buffer용)
    double acc_value_buffer[BUFFER_SIZE];
    double acc_value_sum; // acc_value_buffer에 저장된 acc값들의 합
}

@synthesize dataObj;

-(id) init
{
    self = [super init];
//    if (self){
//        [self startCompression];
//    }
    return self;
}

- (void)startCompression {
    [self logCallerMethodwith:@" <<  초기에 한번 생성 ??  >> " newLine:5];
    /**
     Start Compression Init Data
     */
    
    is_start = false;						// 가슴 압박 중단 상태
    is_timer_on = false;					// 타이머 중단 상태
    
    max_depth = 0;
    min_depth = 100;
    
    max_acc_depth = -100;
    min_acc_depth = 100;
    
    for (int i = 0; i < 4; i++) {
        position[i] = false;
        position_result[i] = false;
    }
    count = 0;
    
    is_turning = false;
    
    is_correct_recoil = true;
    is_correct_position = true;
    is_correct_depth = 0;
    
    is_message_done = false;
    
    acc_value_count = 0;
    acc_value_sum = 0;
    
    is_print = 0;

    ////////////////////////////////////////////////
}

- (void)startBreath {
    [self logCallerMethodwith:@" <<    >> " newLine:5];
    
    /**
     Start Breath Init Data
     */
    
    is_start = false;						// 가슴 압박 중단 상태
    is_timer_on = false;					// 타이머 중단 상태
    
    max_amount = 0;
    
    count = 0;
    
    is_turning = false;
    
    is_correct_amount = 0;
    
    is_message_done = false;
    
    is_print = 0;
}

- (void)setStart:(BOOL)start{
    is_start = start;
}

- (void)setQuit:(BOOL)quit{
    is_quit = quit;
}

/*     가슴압박 알고리즘     */
- (void)stepCompressionOperation:(NSData*)data
             positionUpdateBlock:(PositionUpdateBlock)positionUpdateBlock
                depthUpdateBlock:(DepthUpdateBlock)depthUpdateBlock
                countUpdateBlock:(CountUpdateBlock)countUpdateBlock
              messageUpdateBlock:(MessageUpdateBlock)messageUpdateBlock {
    
    //Step 1. data process
    unsigned char datas[data.length];
    [data getBytes:&datas length:data.length];
    
    int piezo_value = (((datas[3] << 8) & 0xFF00) + (datas[4] & 0xFF) +
                       ((datas[5] << 8) & 0xFF00) + (datas[6] & 0xFF)) / 2;  // 2개의 피에조 센서의 평균값 산출
    
    [self logCallerMethodwith:[NSString stringWithFormat:@" <<  가슴압박 알고리즘  piezo : %d >> ", piezo_value ]newLine:0];
    
    short accel_raw_short;
    double accel_raw_double;
    double accel_raw_pure;
    
    accel_raw_short = (short) (((datas[12] << 8) & 0xFF00) + (datas[13] & 0xFF));  // 가속도 센서의 z 축 raw 데이터 파싱
    accel_raw_double = (accel_raw_short * 0.00390625 / 2 + 0.03577027) * GVALUE - 2;  // 가속도 센서 raw 데이터를 m/s 스케일로 변환

    //---------------------------------------------------------------
    // 가속도 센서 중력가속도 g를 제거하기 위한 baseline 설정
    //---------------------------------------------------------------
    if (acc_value_count != 0) {  // 첫 acc값이 아니라면 기존 값과 연산하여 새로운 acc_init_value를 만듬
        acc_init_value = 0.8 * acc_init_value + 0.2 * accel_raw_double;
    }
    else {            // 첫 acc 값이라면 acc_init_value에 저장
        acc_init_value = accel_raw_double;
    }
    //---------------------------------------------------------------

    
    accel_raw_pure = accel_raw_double - acc_init_value; //가속도 센서 - 중력가속도로 순수한 가속도값 구함
    
    //---------------------------------------------------------------
    //튀는 가속도 값들을 부드럽게 해주기 위한 파트(필터링)
    //---------------------------------------------------------------
    if (acc_value_count < BUFFER_SIZE) {
        acc_value_buffer[acc_value_count] = accel_raw_pure;
        acc_value_sum += accel_raw_pure;
        acc_value = acc_value_sum / (acc_value_count);
    }
    else {
        acc_value_sum += (accel_raw_pure - acc_value_buffer[acc_value_count
                                                            % BUFFER_SIZE]);
        acc_value = acc_value_sum / BUFFER_SIZE;
        acc_value_buffer[acc_value_count % BUFFER_SIZE] = accel_raw_pure;
    }
    acc_value_count++;
    
    //---------------------------------------------------------------
    // 방향 스위치 데이터 파싱
    // p[0] : 머리(9시), p[1] : 위(12시), p[1] : 배(3시),  p[1] : 아래(6시)
    //---------------------------------------------------------------
    Byte direction = (Byte) (datas[7] & 0xFF);
    BOOL p[4];
    
    if ((direction & (0x01 << 4)) > 0x00)
        p[0] = false;
    else
        p[0] = true;
    
    if ((direction & (0x01 << 5)) > 0x00)
        p[3] = false;
    else
        p[3] = true;
    
    if ((direction & (0x01 << 6)) > 0x00)
        p[2] = false;
    else
        p[2] = true;
    
    if ((direction & (0x01 << 7)) > 0x00)
        p[1] = false;
    else
        p[1] = true;
    //---------------------------------------------------------------
    // 깊이 데이터를 100 스케일로 변환

    
    //depth = (int) (((piezo_value - CC_START_POINT) / ((float) (CC_END_POINT - CC_START_POINT))) * 100);
    depth = (int) (((piezo_value - dataObj.ccStaPoint) / ((float) (dataObj.ccEndPoint - dataObj.ccStaPoint))) * 100);
    
    //깊이가 10이하라면 0, 100 이상이라면 100으로 변환
    if (depth < 10)
        depth = 0;
    
    if (depth > 100)
        depth = 100;
    
    if(is_start){// 가슴압박이 진행중일 때, 피에조 및 가속도 센서로 가슴압박 분석
        if (!is_turning) { // 가슴을 누르는 중일 때
            [self checkPosition:messageUpdateBlock positon:p];//위치 검사
            [self checkDepth:messageUpdateBlock];// 깊이 검사
        }
        else { // 가슴에서 손을 뗄 때
            [self checkRecoil:messageUpdateBlock countUpdateBlock:countUpdateBlock];// 끝까지 손을 떼었는지 검사 및 1사이클 초기화
        }
        
        if (is_print % 2 == 0) { // 2프레임에 1번씩 UI 변환, iOS에서 무리가 없다면 매 프레임마다 보여줘도 됨
            // 화면 피드백 UI 업데이트
            positionUpdateBlock(position);
            depthUpdateBlock(depth);
            countUpdateBlock(count);
        }
        is_print++;
    }else{// 가슴압박 설명 사운드 재생 및 가슴압박 시간이 종료되었을 때
        messageUpdateBlock(HSCompressionAllClearType);
        
//        if (is_print % 2 == 0 && !is_quit) { // 2프레임에 1번씩 UI 변환, iOS에서 무리가 없다면 매 프레임마다 보여줘도 됨
            // 화면 피드백 UI 업데이트
//            positionUpdateBlock(position);
//            depthUpdateBlock(depth);
//            countUpdateBlock(count);
//        }
        is_print++;
    }

}

//위치를 검사한다.
- (void)checkPosition:(MessageUpdateBlock)messageUpdateBlock positon:(BOOL *)p {

    [self logCallerMethodwith:[NSString stringWithFormat:@" <<  위치 검사  >> position : %s ", p] newLine:0];

    BOOL is_center = false;
    if (depth > CC_PRESSURE_POINT) { // 피에조값이 일정 이상 눌렸다면 가운데에 눌렸다고 판정, CC_PRESSURE_POINT = 30;
        is_center = true;
    }
    
    for (int i = 0; i < 4; i++) {
        
        if (p[i] && !is_center) {  // 가운데에 일정 이상의 압력이 전달되지 않으면서 위치 스위치가 눌렸다면 Wrong Position!
            position[i] = true;
            position_result[i] = true;
            is_correct_position = false;
            if (!is_message_done) {  // 0.4초간 Wrong Position 메세지 출력
                messageUpdateBlock(HSCompressionWrongPositionType);
                is_message_done = true;                // 현재 사이클에서 메세지 출력이 완료됨(다음 사이클 까지 메세지 출력 X)
                NSLog(@"Wrong Position");
            }
        } 
        else {
            position[i] = false;
        }
    }
}

- (void)checkDepth:(MessageUpdateBlock)messageUpdateBlock {
    [self logCallerMethodwith:@" << 중앙을 눌렀을 경우 피에조 센서로 체크  >> " newLine:0];
    
    if (is_correct_position) {  // 중앙을 눌렀을 경우 피에조 센서로 체크
        if (depth <= 10 || depth >= max_depth) {  // 피에조 값이 증가하는 추세라면(depth가 max_depth보다 크다면) 계속 해서 max_depth를 갱신
            if (depth >= max_depth)
                max_depth = depth;
        }
        else {  // depth가 max_depth보다 작다면 손을 떼는 중이고, max_depth로 압박 깊이 판단
            is_turning = true;
            if (max_depth < CC_WEAK_POINT) {  // CC_WEAK_POINT보다 낮다면 Too Weak 메세지 출력, CC_WEAK_POINT = 65
                if (!is_message_done) {      // 현재 사이클에서 메세지를 아직 출력하지 않았다면 Too weak 메세지 출력
                    messageUpdateBlock(HSCompressionTooWeakType);
                    is_message_done = true;
                }
                is_correct_depth = 1;
            }
            else if (max_depth > CC_STRONG_POINT) {  // CC_STRONG_POINT보다 크다면면 Too STRONG 메세지 출력, AHA 기준(미국)일 떄 CC_STRONG_POINT = 100, KACPR 기준(한국)일 때 CC_STRONG_POINT = 91
                if (!is_message_done) {      // 현재 사이클에서 메세지를 아직 출력하지 않았다면 Too Strong 메세지 출력
                    messageUpdateBlock(HSCompressionTooStrongType);
                    is_message_done = true;
                }
                is_correct_depth = 2;
            }
            else {  // 두 기준 사이라면 GOOD 범위
                is_correct_depth = 0;
            }
        }
    }
    
    if (acc_value + 0.75 >= max_acc_depth) {  // 가속도 센서로 체크, 가속도 센서는 변화가 심해서 0.75 이상의 감소가 있어야 판단
        if (acc_value > max_acc_depth) {
            max_acc_depth = acc_value;
        }
    } 
    else {
        if (!is_correct_position) {  // 잘못된 위치에서는 압력값이 제대로 측정되지 않으므로 가속도 센서로 횟수 판단
            is_turning = true;
        }
    }
}

- (void)checkRecoil:(MessageUpdateBlock)messageUpdateBlock countUpdateBlock:(CountUpdateBlock)countUpdateBlock {
    [self logCallerMethodwith:@" <<  중앙을 눌렀을 경우 피에조 센서로 체크  >> " newLine:0];
    
    if (is_correct_position) {  // 중앙을 눌렀을 경우 피에조 센서로 체크
        if (depth > 10 && depth - 10 <= min_depth) { // 압력값이 10 이상이고 피에조 값이 감소 한다면 손을 떼는 중
            if (depth <= min_depth)
                min_depth = depth;
        }
        else {  // 압력값이 10 이하거나, 피에조값이 증가한다면 다음 압박 진행중으로 판단
            is_turning = false;
            
            if (depth > 10 && min_depth > 10) {  // 손을 완전히 떼지 않고 다음 압박을 진행하면 Not Recoil 메시지 출력
                messageUpdateBlock(HSCompressionNotRecoil);
                is_correct_recoil = false;
            }
            
            if (is_correct_position && is_correct_depth == 0
                && is_correct_recoil) {  // 깊이, 위치, 이완 모두 성공했다면 Good 메세지 출력
                messageUpdateBlock(HSCompressionGoodType);
            }
            
            [self clear_cc_cycle]; // 변수 초기화
            count++;  // 카운트 증가
            countUpdateBlock(count);// 카운트 UI 변경
            
            if (count == 30) {  // 30번 모두 했다면 가슴 압박 측정 종료
                is_start = false;
            }
        }
    }
    if (acc_value - 0.75 <= min_acc_depth) { // 가속도 센서로 체크, 가속도 센서는 변화가 심해서 0.75 이상의 증가가 있어야 판단
        if (acc_value < min_acc_depth) {
            min_acc_depth = acc_value;
        }
    }
    else {
        if (!is_correct_position) { // 잘못된 위치에서는 압력값이 제대로 측정되지 않으므로 가속도 센서로 횟수 판단
            is_turning = false;
            
            [self clear_cc_cycle]; // 변수 초기화
            count++;  // 카운트 증가

            countUpdateBlock(count);// 카운트 UI 변경

            if (count == 30) {  // 30번 모두 했다면 가슴 압박 측정 종료
                is_start = false;
            }
        }
    }
}

- (void)clear_cc_cycle{
    max_depth = 0;
    min_depth = 100;
    
    max_acc_depth = -100;
    min_acc_depth = 100;
    
    for (int i = 0; i < 4; i++) {
        position[i] = false;
        position_result[i] = false;
    }
    
    is_correct_recoil = true;
    is_correct_position = true;
    is_correct_depth = 0;
    
    is_message_done = false;
}



/*
 * 인공호흡 알고리즘
 */
- (void)stepBreathOperation:(NSData *)data
          amountUpdateBlock:(amountUpdateBlock)amountUpdateBlock
           countUpdateBlock:(CountUpdateBlock)countUpdateBlock
         messageUpdateBlock:(BreathMessageUpdateBlock)messageUpdateBlock {
    [self logCallerMethodwith:@" <<  인공호흡  >> " newLine:5];
    
    unsigned char datas[data.length];
    [data getBytes:&datas length:data.length];
    
    //int breath_value = (((datas[14] << 8) & 0xFF00) + (datas[15] & 0xFF)); // 기압값 파싱 단위: 파스칼
    
    //amount = (int) ((breath_value - RP_START_POINT) / ((double) (RP_END_POINT - RP_START_POINT)) * 100); // 100 스케일로 기압값 변환
    dataObj.amount = (int) ((dataObj.brethVal - dataObj.rpStaPoint) / ((double) (dataObj.rpEndPoint - dataObj.rpStaPoint)) * 100); // 100 스케일로 기압값 변환
    
    if (amount < 10) // 10 이하일 때 0, 100이상일 때 100으로 변환
        amount = 0;
    if (amount > 100)
        amount = 100;
    
    if (is_start) { // 인공호흡 진행중일 때, 기압센서로 인공호흡 분석
        if (!is_turning) {  // 호흡을 불어넣고 있을 때
            
            [self check_amount:amountUpdateBlock countUpdateBlock:countUpdateBlock messageUpdateBlock:messageUpdateBlock];  // 호흡량 검사
        }
        else {  // 1회 호흡을 끝냈을 때
            [self recoil_amount]; // 1사이클 초기화
        }
        
//        if (is_print % 2 == 0) { // 2프레임에 1번씩 UI 변환, iOS에서 무리가 없다면 매 프레임마다 보여줘도 됨
//            amountUpdateBlock(amount);
//            countUpdateBlock(count);
//        }
        amountUpdateBlock(amount);
        countUpdateBlock(count);
        is_print++;
    }
    else { // 가슴압박 설명 사운드 재생 및 가슴압박 시간이 종료되었을 때
        messageUpdateBlock(HSBreathAllClearType);
//        if (is_print % 2 == 0 && !is_quit) { // 2프레임에 1번씩 UI 변환, iOS에서 무리가 없다면 매 프레임마다 보여줘도 됨
//                 compression_print_function(); // 화면 피드백 UI 업데이트, 쉴 때에는 압력 게이지바만 변환
//        }
        is_print++;
    }
    
}



- (void)check_amount:(amountUpdateBlock)amountUpdateBlock
    countUpdateBlock:(CountUpdateBlock)countUpdateBlock
  messageUpdateBlock:(BreathMessageUpdateBlock)messageUpdateBlock {
    [self logCallerMethodwith:@" <<    >> " newLine:5];
    if (amount <= 10 || amount + 10 >= max_amount) {  // 호흡 진행중이라면 계속해서 max_amount 갱신
        if (amount >= max_amount)
            max_amount = amount;
    }
    else {  // 호흡이 중단되었다면 호흡량 측정
        is_turning = true;
        
        if (max_amount < RP_WEAK_POINT) {  // RP_WEAK_POINT = 35
            messageUpdateBlock(HSBreathTooWeakType);
            is_correct_amount = 1;
        }
        else if (max_amount > RP_STRONG_POINT) { // RP_STRONG_POINT = 70
            messageUpdateBlock(HSBreathTooStrongType);
            is_correct_amount = 2;
        }
        else {    // 35 <= max_amount <= 70
            messageUpdateBlock(HSBreathGoodType);
            is_correct_amount = 0;
        }
        count++;
        countUpdateBlock(count);
    }
}
- (void)recoil_amount{
    if (amount <= 10) {
        is_turning = false;
        
        [self clear_rp_cycle];
        
        if (count == 2) {
            is_start = false;
        }
    }
}
- (void)clear_rp_cycle{
    max_amount = 0;
    
    is_correct_amount = 0;
}

@end
