//
//  HsCalcRealCalibration.swift
//  Trainer
//
//  Created by Jongwoo Moon on 2016. 5. 10..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation



class DistSet: NSObject {
    var xV = MinMax(), yV = MinMax(), zV = MinMax(), flipped = false
    var arrVal = [MinMax]()

    func byteToDouble(data: [UInt8], idx: Int) -> Double {
        return Double(HCStatic.uint8TosignedByte(data[idx])) / 10
    }

    func setArrVal() {
        if arrVal.count == 0 {
            arrVal = [ xV, yV, zV ]
        }
    }

    func setXYZ(data: [UInt8]) {
        xV.setVals(byteToDouble(data, idx: 8), mx: byteToDouble(data, idx: 9))
        yV.setVals(byteToDouble(data, idx: 10), mx: byteToDouble(data, idx: 11))
        zV.setVals(byteToDouble(data, idx: 12), mx: byteToDouble(data, idx: 13))
        setArrVal()

        //print(" \t\t\t\t\t  Dist Value :: \t X> \(xV.valStr())  Y> \(yV.valStr())   \t\t  Z> \(zV.valStr()) ")
    }

    func copyFrom(tar: DistSet, flipMinMax: Bool) {
        setArrVal();  tar.setArrVal()
        flipped = flipMinMax
        for (idx, obj) in arrVal.enumerate() {
            if flipMinMax {
                obj.copyFlipFrom(tar.arrVal[idx])
            } else {
                obj.copyFrom(tar.arrVal[idx])
            }
        }
    }

    func getValOf(xyz: Int, seq: Int) -> Double {
        let obj = arrVal[xyz]
        //if flipped {
            if seq == 0 { return obj.max }
            else { return obj.min }
//        } else {
//            if seq == 0 { return obj.min }
//            else { return obj.max }
//        }
    }

}

class Buff: NSObject {
    var force: Int = 0, forceMax: Int = 0, rate: Int = 0, prevCali: Int = 0,
    depth = [Double] (count:3, repeatedValue: 0), depthMax = [Double] (count:3, repeatedValue: 0)

    var depthVector: Double { get { return depth[0].vectSum(depth[1], z: depth[2]) }}

    func copyDepthFrom(buf: Buff) {
        for i in 0...2 {  depth[i] = buf.depth[i]  }
    }
    func copyDepthMaxFrom(buf: Buff) {
        for i in 0...2 {  depthMax[i] = buf.depthMax[i]  }
    }

    func showMe() {
        print("Buffer >>   force : \(force)   forceMax \(forceMax)    prevCali \(prevCali)   rate \(rate) ")
        print("       depth    : \(depth[0])   \(depth[1])   \(depth[2]) ")
        print("       depthMax : \(depthMax[0])   \(depthMax[1])   \(depthMax[2]) ")
    }

}


class DataRevision: NSObject {
    var max_force = 0, min_force = 0, is_correct_depth = false, is_correct_recoil = false,
    set = 0, pos = 0

    override init() {
        super.init()
    }

    convenience init(maxforce: Int, minforce: Int, iscordepth: Bool, iscorrecoil: Bool, pset: Int, ppos: Int) {
        self.init()
        max_force = maxforce; min_force = minforce
        is_correct_depth = iscordepth; is_correct_recoil = iscorrecoil
        set = pset; pos = ppos
    }
}


//////////////////////////////////////////////////////////////////////     _//////////_     [   실시간 캘리브레이션    ]
class HsCalcRealCalibration : NSObject {
    var log = HtLog(cName: "HsCalcRealCalibration")
    var depth = 0.0, rawCali = 0
    var revisionTable = [[Double]]()
    var dataObj = HsData()

    // depth 와 force 의 현재 위치의 버퍼를 준다.   버퍼 관련 변수들...
    var buffSize = 5, buffPosi = 0, arrBuff = [Buff]()
    var curBuff: Buff { get { return arrBuff[ buffPosi % buffSize ] } }
    var prevBuff: Buff { get { return arrBuff[ (buffPosi - 1) % buffSize ] } }
    // 하나로 통일 var curFbpBuff: Buff { get { return arrBuff[ forceBuffPosi % buffSize ] } }

    var isTurnD = false, isTurnP = false,
    curDistSet = DistSet(), prevDepthMM = MinMax(),
    dist = MinMax(), // min_d|max_d
    pres = MinMax(),
    depthOrder = DistSet()

    var pressureIdleCnt = 0, isPresCycleStart = false, isPresCycleEnd = false, isDepthCycleEnd = false, isWrongPosi = false
    var prevDepthMin = [Double] (count:3, repeatedValue: 0), increasingsDepth = [Double] (count:3, repeatedValue: 0)
    var myStaPoint = 0, ccFilterRatio = 0.0, ccPresThrshldRatio = 0.0, ccFilter = 0, ccPresThrshld = 0
    var referenceCali = 0

    override init() {
        super.init()
        for _ in 1...buffSize {
            arrBuff.append(Buff())
        }
        revisionTable = [ [ 1.254, 1.297, 1.293, 1.274, 1.235, 1.238 ], [ 1.254, 1.297, 1.293, 1.274, 1.235, 1.238 ],
                          [ 1.254, 1.297, 1.293, 1.274, 1.235, 1.238 ], [ 1.241, 1.248, 1.191, 1.206, 1.210, 1.238 ],
                          [ 1.218, 1.212, 1.208, 1.206, 1.201, 1.191 ], [ 1.201, 1.154, 1.141, 1.150, 1.124, 1.131 ],
                          [ 1.201, 1.071, 1.071, 1.062, 1.057, 1.059 ] ]

        pres.setVals(100000, mx: -100000)  // min_p = 100000;  max_p = -100000;
        dist.setVals(100, mx: -100)  // min_d = 100;  max_d = -100;
        isTurnD = false; isTurnP = false
        pressureIdleCnt = 0; isPresCycleStart = false; isPresCycleEnd = false; isDepthCycleEnd = false;
        //this.DISTANCE_FILTER = distance_filter; this.CC_FILTER_RATIO = cc_filter_ratio;  한번만 쓰이므로 변수로 따로 지정하지 말자.
        //this.CC_PRESSURE_THRESHOLD_RATIO = cc_pressure_threshold_ratio
    }

    func initialProcess(dObj: HsData, ccFilter: Int, presThrhRto: Double) {
        dataObj = dObj
        ccFilterRatio = Double(ccFilter) / 100
        self.ccFilter = ccFilter
        ccPresThrshldRatio = presThrhRto
        // myStaPoint = ccStaPo  이거는 따로 세팅함..

        print("\n  HsCalcRealCalibration :: initialProcess  >>  \(ccFilter) \(ccFilterRatio)   \(ccPresThrshldRatio)")
    }

    //////////////////////////////////////////////////////////////////////     _//////////_     [ Compression      << >> ]  Calibration
    //////////////////////////////////////////////////////////////////////     _//////////_     [ Compression      << >> ]  Calibration
    //////////////////////////////////////////////////////////////////////     _//////////_     [ Compression      << >> ]  Calibration
    //////////////////////////////////////////////////////////////////////     _//////////_     [ Compression      << >> ]  Calibration
    func processCalibration(pCcLimit: Int, forceValue: Int, rate: Int,  data: [UInt8]) ->
        (ccLimit: Int, depth: Double, force: Int, caliVal: Int, ccStaPo: Int, distSet: DistSet)
    {
        ccFilter = Int(Double(pCcLimit) * ccFilterRatio)
        ccPresThrshld = Int(Double(pCcLimit) * ccPresThrshldRatio)

        curDistSet.setXYZ(data)

        var depth_return = 0.0, force_return = 0, calibration_return = (pCcLimit - 20) / 32;
        var newCcLimit = pCcLimit

        //print("\n  HsCalcRealCalibration :: processCalibration ::  \t\t  limit : \(pCcLimit)    rate : \(rate)  \t force > \(forceValue)")
        //print("  HsCalcRealCalibration :: ccFilter \(ccFilter)  ccPresTrhd : \(ccPresThrshld)  ")

        processAccel()
        processPressure(forceValue, compRate: rate)  // 요기까지...


        //print("  l::154 \((isPresCycleStart && isPresCycleEnd)) ||  \((!isPresCycleStart && isWrongPosi))  && depthEnd : \(isDepthCycleEnd) ")

        if ((isPresCycleStart && isPresCycleEnd) || (!isPresCycleStart && isWrongPosi)) && isDepthCycleEnd {

            //print("  isWrongPosi : \(isWrongPosi) count : \(dataObj.count) ")

            depth_return = depthRevision(curBuff.depth[2], r: curBuff.rate)
            force_return = curBuff.force

            //print("  depth/force >>>  \(depth_return)   \(force_return) ")

            if !dataFilter(depth_return, count: dataObj.count) && !isWrongPosi {
                buffPosi += 1
                let rVal = findGradientRealCali(pCcLimit) // depth , force 리턴.
                newCcLimit = rVal.rCcLimit
                calibration_return = (pCcLimit - 20) / 32
            }
            //isNewCycle = true
            isPresCycleStart = false
            isPresCycleEnd = false
            isWrongPosi = false
            isDepthCycleEnd = false
        }
        return (newCcLimit, depth_return, force_return, calibration_return, myStaPoint, curDistSet)
        // (ccLimit: Int, depth: Double, caliVal: Int, rawCaliVal: Int, ccStaPo: Int)
    }


    //////////////////////////////////////////////////////////////////////     _//////////_     [ Compression      << >> ]  압박값 처리.
    func processPressure(forceValue: Int, compRate: Int) {
        curBuff.rate = compRate  // set_rate_buffer(compression_rate);

        if isTurnP { pressureDecreasing(forceValue) }
        else { pressureIncreasing(forceValue) }

        /* private void process_pressure(int force_value, int compression_rate) {
         set_rate_buffer(compression_rate);
         if (!is_turning_p) {
         pressure_increasing(force_value);
         } else {
         pressure_decreasing(force_value);
         }
         }  */

    }

    func pressureIncreasing(forceValue: Int) {
        //print(" pressureIncreasing : f: \(forceValue) ccFilter \(ccFilter) isPresCycleStart/end  \(isPresCycleStart)  \(isPresCycleEnd) ")

        if (ccFilter <= forceValue && !isPresCycleStart) {
            isPresCycleStart = true
        }

        if pres.max <= Double(forceValue + ccFilter) { // >= max_p) {
            if pres.max <= Double(forceValue) {
                pres.max = Double(forceValue)
            }
        } else {
            isTurnP = true
            setForceMax(Int(pres.max))
            //print("\n\n    Turning Point        isTurnP \(isTurnP)   최대값을 찍었슴.  \(pres.max)   \n\n")
        }
    }

    func pressureDecreasing(forceValue: Int) {
        //print(" pressureDecreasing : \(forceValue)  ccFilter : \(ccFilter)  min : \(pres.min)  idleCnt : \(pressureIdleCnt) ")

        if forceValue - ccFilter <= Int(pres.min) && pressureIdleCnt < 10 {
            if forceValue < ccFilter {
                pressureIdleCnt += 1
            }
            if Double(forceValue) <= pres.min {
                pres.min = Double(forceValue)
            }
        } else {
            isTurnP = false
            isPresCycleEnd = true
            setForce(Int(pres.min))
            //print("\n\n\n    Turning Point    isTurnP \(isTurnP)  \n\n\n")
            //print("xxx 210  \(pres.min) mysta ..  \(myStaPoint)     \(ccPresThrshld)  ")
            if pres.min < Double(myStaPoint + ccPresThrshld) {
                //print("  set mystapoint >>  \(pres.min)")
                myStaPoint = Int(pres.min)
            }
            pres.setVals(100000, mx: -100000)
            pressureIdleCnt = 0
        }
    }

    func checkOutlier() {
        let d = depthRevision(prevBuff.depth[2], r: prevBuff.rate)
        let p = prevBuff.force

        //print(" referenceCali   \(d) \(referenceCali) ")

        if d * Double(referenceCali) < 0.0001 { return }
        let a = Int(100 * Double(p) / (d * 13)), refa = Double(referenceCali) * 0.8, refb = Double(referenceCali) * 1.2
        //print("  consider outlier")

        if Double(a) < refa || refb < Double(a) {
            //print("  OK  check outlier  ")

            arrBuff[0].showMe()
            prevBuff.showMe()
            //print("  After ....")

            arrBuff[0].force = prevBuff.force
            arrBuff[0].forceMax = prevBuff.forceMax

            arrBuff[0].copyDepthFrom(prevBuff)  // for 문을 함수로 대체..
            arrBuff[0].copyDepthMaxFrom(prevBuff)

            arrBuff[0].rate = prevBuff.rate


            arrBuff[0].showMe()

            buffPosi = 1
        }

        /*
        double d = depth_revision(depth_buffer[n][2], rate_buffer[n]);
        double p = force_buffer[n];
        int a = (int) (100 * p / d / 13);
        if (reference_cali * 0.8 > a || a > reference_cali * 1.2) {
            force_buffer[0] = force_buffer[n];
            force_max_buffer[0] = force_max_buffer[n];

            for (int i = 0; i < 3; i++) {
                depth_buffer[0][i] = depth_buffer[n][i];
                depth_max_buffer[0][i] = depth_max_buffer[n][i];
            }
            
            rate_buffer[0] = rate_buffer[n];
            
            buffer_pos = 1;
        }
         */


    }

    func findGradientRealCali(pCcLimit: Int) -> (rCcLimit: Int, rDepth: Double, rForce: Int) {
        log.printThisFNC("findGradientRealCali", comment: " ccLimit: \(pCcLimit)  ")
        var finalD = 0.0, finalP = 0, xx = 0.0, xy = 0.0

        checkOutlier()

        //print("  loopNum >>  \(buffPosi)  \(buffSize)  ")
        let loopNum = min(buffPosi, buffSize) // int loop_num = Math.min(depth_buffer_pos, RCAL_BUFFER_SIZE);
        //print("  loopNum >>  \(buffPosi)  \(buffSize)  min is >>>   \(loopNum)")

        for i in 0...loopNum - 1 { //print("  in Loop >>>   \(i)")
            finalD = depthRevision(arrBuff[i].depth[2], r: arrBuff[i].rate) // 기존 depthRevision 는 vectSum 임..
            // depth_revision(vector_sum(depth_buffer[i][0], depth_buffer[i][1], depth_buffer[i][2]), rate_buffer[i]);
            finalP = arrBuff[i].force // final_p = force_buffer[i];
            //print ("\t < \(i) >  REALTIME!!!!   D   \(finalD)     P  \(finalP)")
            xy += Double(finalP)
            xx += finalD
        }
        //print(" xy : \(xy), xx : \(xx), loopNum : \(loopNum), buffSize : \(buffSize) ")

        var newCcLimit = Int(100 * xy / xx / 13)
        rawCali = newCcLimit

        //print(" newCCLimit: \(newCcLimit)   ")

        //dataObj.ccLimitPoint = (buffSize - loopNum) * dataObj.ccLimitPoint / buffSize + loopNum * Int(100 * xy / xx / 13) / buffSize

        var caliSum = 0, caliMean = 0
        if buffSize < buffPosi {
            for i in 0...buffSize - 1 { // (int i = 0; i < RCAL_BUFFER_SIZE; i++) {
                caliSum +=  arrBuff[i].prevCali // prev_cali_buffer[i];
            }
            caliMean = caliSum / buffSize

            // print("   referenceCali \(referenceCali)  calimean : \(caliMean) ")

            let refa = Double(referenceCali) * 0.95, refb = Double(referenceCali) * 1.05

            if (Double(caliMean) < refa) || (refb < Double(caliMean)) {
                referenceCali = caliMean
            }
        }

        arrBuff[(buffPosi - 1) % buffSize].prevCali = newCcLimit // CC_LIMIT_POINT;

        //print("  논리식 \(Double(referenceCali) * 0.9 <= Double(newCcLimit))  \(Double(newCcLimit) <= Double(referenceCali) * 1.1)      \(referenceCali)     \(newCcLimit) ")

        if Double(referenceCali) * 0.9 <= Double(newCcLimit) &&
            Double(newCcLimit) <= Double(referenceCali) * 1.1 {
            //print(" referenceCali  367 >>  \(referenceCali) ")
            newCcLimit = referenceCali
        } else {
            //print(" referenceCali  370 >>  \(referenceCali) ")
            referenceCali = newCcLimit
        }
        //print(" ____ ccLimitPoint : \(dataObj.ccLimitPoint)  referenceCali : \(referenceCali) ")
        newCcLimit = newCcLimit.betweenValueOf(340, high: 3220)
        //print(" before return >>  ccLimitPoint : \(dataObj.ccLimitPoint)  referenceCali : \(referenceCali) ")
        return (newCcLimit, xx / Double(loopNum), Int(xy / Double(loopNum)))
    }
    



    private func getDepth() -> Double {
        // return depth_revision(depth_buffer[get_bp()][2], rate_buffer[get_bp()]);  >>>>   이거 dbp 인지 fbp 인지 확인해야 함...  ㅌㅌ  ㅊ.ㅊ
        return depthRevision(curBuff.depth[2], r: curBuff.rate)
    }

    private func dataFilter(depth: Double, count: Int) -> Bool {
        //print(" curBuff.rate>>  \(curBuff.rate)    \(depth)   \(count) ")
        return (curBuff.rate < 80 || curBuff.rate > 133 || depth < 3 || depth > 7 || count == 1)
    }

    func setRate(period: Double) { // 0.0975 뭐 이런식..  이걸 105 이런 rate 로 바꿔야 함.
        curBuff.rate = Int( 60.0 / period )
        // print("setRate ::  \(period)    rate : \(curFbpBuff.rate)")
    }

    func setForce(force: Int) {
        curBuff.force = curBuff.forceMax - force
        //print("  setForce ::    forceMax : \(curBuff.forceMax)    force : \(curBuff.force)  buffPosi : \(buffPosi)   >>>>>>>>>>>>  ")
    }

    // func getForce() -> Int {        return curBuff.force    }

    func setForceMax(force: Int) {
        curBuff.forceMax = force
    }

    //func increaseForceBufferPos() {
        //print("  increaseForceBufferPos  :  \(forceBuffPosi)  ")
        /*if forceBuffPosi - depthBuffPosi < 1 { // (force_buffer_pos - depth_buffer_pos < 1)
            forceBuffPosi += 1 // force_buffer_pos++;
        } */
    //}

    func showForces() {
        for obj in arrBuff {
            print(" force : \(obj.force) ")
        }
    }

    func setForceWrongPosition() {
        isWrongPosi = true
    }


    //func processAccel(data: UnsafeMutablePointer<UInt8>, ccEndStaPoint: Int) ->
      //  (ccLimit: Int, depth: Double, caliVal: Int, rawCaliVal: Int) {
    func processAccel() {
        //var calibrationValue = (ccEndStaPoint - 20) / 32
        //let distanceFilter = 0.5 //  이거 나중에 제대로 할 것.
        //cc_limit = ccEndStaPoint

        //print("\n processAccel ::  ccEndStaPoint >> \(ccEndStaPoint) ")

        //curDistSet.setXYZ(data)
        if isTurnD { // 원래는 ! 인데 여기서만 바꿨슴.
            // 다음 조건이면 최대 / 최소 값을 바꿔서 대입할 것.  뒤집힌 것은 변수로 저장됨.
            depthOrder.copyFrom(curDistSet, flipMinMax: prevDepthMM.min < curDistSet.zV.max)
            prevDepthMM.setAllWith(curDistSet.zV.min)
            //print(" isTurnD : true  ")

        } else {
            depthOrder.copyFrom(curDistSet, flipMinMax: prevDepthMM.max <= curDistSet.zV.min)
            prevDepthMM.setAllWith(curDistSet.zV.max)
            //print(" isTurnD : false  ")
        }

        for i in 0...1 {
            let curDepthVal = depthOrder.getValOf(2, seq: i)
            if (!isTurnD) {
                //print(" @ 172  isTurnD : \(isTurnD)  curDepthVal : \(curDepthVal)  dist.max : \(dist.max) ")

                if curDepthVal + dataObj.distanceFilter >= dist.max {
                    if curDepthVal > dist.max {
                        dist.max = curDepthVal
                        for j in 0...2 { // set_depth_max_buffer(depth_order[i]);
                            curBuff.depthMax[j] = depthOrder.getValOf(j, seq: i)
                        }
                    }
                } else {
                    isTurnD = true
                    //print(" !!!   \t\t\t changed isTurnD : \(isTurnD) ")
                    for j in 0...2 { // set_increasing_depth()
                        increasingsDepth[j] = Double.abs( curBuff.depthMax[j] - prevDepthMin[j] )
                    }
                }
            } else {
                if curDepthVal - dataObj.distanceFilter <= dist.min  {
                    if curDepthVal < dist.min { // (depth_order[i][2] - DISTANCE_FILTER <= min_d) {
                        dist.min = curDepthVal // min_d = depth_order[i][2];
                        for j in 0...2 {  // set_depth_buffer(depth_order[i]);
                            curBuff.depth[j] = (increasingsDepth[j] + Double.abs(curBuff.depthMax[j] - depthOrder.getValOf(j, seq: i))) / 2
                            //print("  curBuff.depth[j]  ::  \(curBuff.depth[j])")
                            prevDepthMin[j] = depthOrder.getValOf(j, seq: i) // set_prev_depth_min(depth_order[i]); prev_depth_min[i] = depth[i];
                        }
                    }
                } else {
                    isTurnD = false
                    isDepthCycleEnd = true
                    //print(" !!!   \t\t\t changed isTurnD : \(isTurnD) ")
                    dist.setVals(100, mx: -100) // min_d = 100; max_d = -100;
                }
            }
        }
        //return (cc_limit, depth, calibrationValue, rawCali)
    }

    func depthRevision (d: Double, r: Int) -> Double {
        var re_d = Int(d)
        var re_r = ((r - 1) / 10) - 8
        if (re_d > 6) { re_d = 6 }
        if (re_r < 0) { re_r = 0 }
        if (re_r > 5) { re_r = 5 }
        //print(" depthRevision  d: \(d)  r: \(r)    re_d : \(re_d),  re_r : \(re_r)  ")
        return d * revisionTable[re_d][re_r]
    }

    func setIncreasingDepth() {
        for i in 0...2 {
            increasingsDepth[i] = Double.abs(curBuff.depthMax[i] - prevDepthMin[i] )
        }
        // for (int i = 0; i < 3; i++) increasing_depth[i] = Math.abs(depth_max_buffer[get_dbp()][i] - prev_depth_min[i]);
    }

}
