//
//  HsCalcManager.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2015. 10. 7..
//  Copyright © 2015년 gyuchan. All rights reserved.
//

import Foundation

@objc
class HsCalcManager: NSObject {
    var log = HtLog(cName: "HsCalcManager")
    var compHoldTimer = HtTimer()
    var bypass: HsBypassManager?
    var bleMan: HsBleCtrl? //var bleMan: HsBleManager?
    var speedLogic: HsSpeedLogic?

    var maxValueForSL = 0

    var realCali = HsCalcRealCalibration(), myLimitPo = 0, myStaPo = 0, myEndPo = 0 // data 에서 카피하고 로컬로만 쓴다. real cali 관련

    // 전역 변수 ..  캐스팅 관련..
    var countD:Int { get { return Int ((data.countD)) } set {  data.countD = Int(newValue) } }
    var countP:Int { get { return Int ((data.countP)) } set {  data.countP = Int(newValue) } }
    var countRP:Int { get { return Int((data.countRP)) } set { data.countRP = Int(newValue) } }
    var depth:Int { get { return (data.depth) } set { data.depth = newValue } }
    var amount:Int { get { return (data.amount) } set { data.amount = newValue } }
    var force_value:Int { get { return (data.forceVal) } set { data.forceVal = newValue } }
    var breath_value:Int { get { return (data.brethVal) } set { data.brethVal = newValue } }
    var min_depth:Int { get { return (data.minDepth) } set { data.minDepth = newValue } }
    var max_depth:Int { get { return (data.maxDepth) } set { data.maxDepth = newValue } }
    var maxAmount:Int { get { return (data.maxAmount) } set { data.maxAmount = newValue } }
    var stepMoni: CurStep { get { return (bleMan?.stt)! } set { bleMan?.stt = newValue } }
    var isWrongPosi: Bool { get { return bleMan?.messageName != nil && bleMan!.messageName == "message_wrongposition" }}

    var data: HsData { get { return (bleMan?.dataObj)! } }  // 요거 대신 새로운 객체로 쓰자..  낭중에..

    var min_d:Double = 100, max_d:Double = -100, health = 0,
    init_rp:Int = 0, min_rp:Int = 0,  max_rp:Int = 0,  sum_rp:Int = 0,
    min_p:Int = 100, max_p:Int = -100, limit_rp:Int = 500  // 1050
    var is_init_a_value = false, is_init_p_value = false, is_pressure_start = false,
    is_turning_p = false, is_turning_d = false, is_turning_rp = false, is_quit = false

    var ccFilter:Int {
        get {
            switch stepMoni {
            case .CAL_NEW_CC:   return 50
            //case .S_STEPBYSTEP_COMPRESSION, .S_WHOLESTEP_COMPRESSION, .S_WHOLESTEP_PRECPR:                return 10
            default:            return 10 // calibration..
            }
        }
    }

    // Calibration Setting
    var peak_p = Array(count: 2, repeatedValue: [Int](count: 100, repeatedValue: 0))
    var final_p = [Int] (count: 100, repeatedValue: 0)
    var peak_d = Array(count: 2, repeatedValue: Array(count: 100, repeatedValue: [Double](count:3, repeatedValue: 0)))
    var final_d = [Double] (count: 100, repeatedValue: 0)
    var depth_order = Array(count:2, repeatedValue: [Double](count: 3, repeatedValue: 0))
    var prev_depth_min_cv:Double = 0, prev_depth_max_cv:Double = 0
    var depth_min_cv  = [Double](count: 3, repeatedValue: 0) // cm
    var depth_max_cv  = [Double](count: 3, repeatedValue: 0)
    var limit_p:Int = 2000, dir = 0 // init_p:Int = 0,
    //var DISTANCE_FILTER: Double = 0.5 // 캘리브레이션 때 사용.

    var weakUIcljr:(Void)->Void = {}
    var  isTurning = false, isPause = false, isCorrectRecoil = false;
    var wrongPositionNum = -1, isCycleStart = false
    var isAllInOnePrcss:Bool { get {
        return (bleMan?.isAllInOnePrcss)! }} // stt == AllInOne10 } }

        //return bleMan?.parsingState == AllInOne10 } }

    var stage: Int {
        get { return Int((bleMan?.stage)!) }
        set { bleMan?.stage = newValue }
    }

    var stageLimit:Int {
        get { return Int(HsBleSingle.inst().stageLimit) }
        set { HsBleSingle.inst().stageLimit = Int32(newValue) }
    }

    var first_cycle_start_time_set:Bool = false, is_comp_stop_timer = false
    var curDatas = [UInt8]() //  UnsafeMutablePointer<UInt8> = nil //UnsafeMutablePointer()
    var curStageObj = HmUnitSet()
    var curStroke : HmCompStroke { get { return curStageObj.lastCompObj }  }

    var entireStepFinished = false
    var prevMaxRp: Int = 0
    var updateTestTimer: Bool { get { return first_cycle_start_time_set || 1 < stage }}

    var arrRevision = [DataRevision]() // 실시간 캘리 관련.
    var isCorrectDepth = false
    var switchThresholds = [Int]() // = [ 40, 20, 55, 20 ] // 머리, 오른쪽, 배, 왼쪽
    var brthTimer = HtTimer()

    //////////////////////////////////////////////////////////////////////     _//////////_     [ init () ]
    override init() {
        super.init()
    }

    //convenience init(bleObj: HsBleManager, bypassObj: HsBypassManager) {
    convenience init(bleObj: HsBleCtrl, bypassObj: HsBypassManager) {
        self.init()
        bypass = bypassObj
        bleMan = bleObj
        //log.bleObj = bleMan
        realCali.initialProcess(data, ccFilter: ccFilter, presThrhRto: 0.05)
        switchThresholds = [ 40, 20, 55, 20 ] // 머리, 오른쪽, 배, 왼쪽
    }

    //func isEntireProcess() -> Bool {        return bleMan?.parsingState == AllInOne10    }


    //////////////////////////////////////////////////////////////////////     _//////////_     [ Compression      << >> ]    _//////////_   전과정 || 단계별 파싱.
    // MARK: - 파싱 메인.  올인원, 단계별, 바이패스
    func allInOneDataParsing(pData: [UInt8]) { // pData:UnsafeMutablePointer<UInt8>) {
        //HsCalcEntire.singltn.parseStartingHere()
        curDatas = pData
        //log.printThisFNC("allInOneDataParsing", comment: " currentStep : \(currentStep) | bleMan?.stepEntire : \(bleMan?.stepEntire)>>  S_WHOLESTEP_COMPRESSION = 11  S_WHOLESTEP_BREATH = 12   ")
        if !commonDataParsing() { return }

        switch stepMoni {
        //case .S_WHOLESTEP_PRECPR: // 10         //precaustionDataParsing()
        case .S_WHOLESTEP_COMPRESSION, .S_WHOLESTEP_PRECPR: // 9
            compressionDataParsing()
        case .S_WHOLESTEP_BREATH: // 8
            breathDataParsing() // breathDataParsing(pData)
        default:
            break
        }
    }

    func stepByStepParsing(pData: [UInt8]) { // UnsafeMutablePointer<UInt8>) {
        curDatas = pData
        if !commonDataParsing() {
            return
        }

        log.logThis(" stepByStepParsing  ", lnum: 0)

        switch stepMoni {
            //case .S_WHOLESTEP_PRECPR: // 10
        //  precaustionDataParsing()
        case .S_STEPBYSTEP_COMPRESSION: // 9
            compressionDataParsing()
        case .S_STEPBYSTEP_BREATH: // 8
            breathDataParsing() // breathDataParsing(pData)
        default:
            break
        }
    }


    //////////////////////////////////////////////////////////////////////     [ 압박.      << >> ]
    // MARK: DataParsing :  compression & breath
    func compressionDataParsing() {
        //log.printThisFNC("compressionDataParsing", comment: "   depth : \(depth) ,   amount : \(amount) ")

        //  V.1.08.160718   data.operationTimer.timeSinceStart 이거 쓰면 이전 압박 시간으로 계산해서 더 짧아 진다...
        let finalProcess = stage == stageLimit && data.count > 0 && compHoldTimer.GetSecond() > 9  // V.1.08.160718
        let goToBreath = !isCycleStart &&  curStageObj.lastCompObj.strokeTimer.timeSinceEnd >= 2 &&
            amount > depth && data.count > 0   // 호흡 값이 클 때 호흡으로 전환..   전과정 only

        if isAllInOnePrcss && (finalProcess || goToBreath) {
            log.printAlways("   호흡 값이 클 때 호흡으로 전환..  스테이지 그대로..  stage : \(stage) / \(stageLimit)  timeSinceStart \(data.operationTimer.timeSinceStart)>>>   initializeEntireBreath() ", lnum: 10)
            curStageObj.ccPassCnt = data.ccCorCount
            curStageObj.ccCount = data.count
            curStageObj.lastCompObj.strkTime = data.operationTimer.timeSinceStart
            curStageObj.lastCompObj.setPeriodByPacket()
            initializeEntireBreath()


            if finalProcess { entireStepFinalProcess() } //  V.1.08.160718
            

        } else if !isCycleStart &&  curStageObj.lastCompObj.strokeTimer.timeSinceEnd >= 2
            && (depth > amount || dir < 240)
            && data.count > 0 // 이게 없으면 계속 올라갈테니..
            && stage < stageLimit && isAllInOnePrcss { // 호흡을 안해서 다음 스테이지로  전과정 only
            log.logThis(" 호흡을 안해서 다음 스테이지로 :  stage : \(stage), stageLimit : \(stageLimit)  count : \(data.count) ")

            curStageObj.ccPassCnt = data.ccCorCount // data.rp_pass_count[stage - 1] = data.rpCorCount
            curStageObj.ccCount = data.count // data.rp_count[stage - 1] = data.count
            curStageObj.lastCompObj.strkTime = data.operationTimer.timeSinceStart
            curStageObj.lastCompObj.setPeriodByPacket()

            curStageObj.rpPassCnt = 0
            curStageObj.rpCount = 0 //  data.rp_pass_count[stage - 1] = 0    data.rp_count[stage - 1] = 0

            data.ccCorCount = 0; data.rpCorCount = 0            //stage++;  // 호흡 스킵하고 스테이지 증가.
            increaseSet()

            log.printAlways("  @263   2초 공백...   다시 호흡 ..    stage++   \(stage )   stageLimit : \(stageLimit) ", lnum: 10)

            initializeCompressionGoToNextSet() // 초기화.
        } else {
            log.logThis("compressionDataParsing : isTurning \(isTurning) ? checkRecoil : check position and depth ..  \t... \t... \t... \t...   <<<   depth : \(depth) amount : \(amount)   >>>", lnum: 0)

            var arrP = [Bool](count: 4, repeatedValue: true)
            arrP[0] = !isValueNotBiggerThan0x00(UInt8(dir), shift: 4) // arrP 0, 3, 2, 1 이었슴..
            arrP[3] = !isValueNotBiggerThan0x00(UInt8(dir), shift: 5) //
            arrP[2] = !isValueNotBiggerThan0x00(UInt8(dir), shift: 6) // Leg
            arrP[1] = !isValueNotBiggerThan0x00(UInt8(dir), shift: 7) // arrP 순서 : 머리, 왼, 다리, 오른.

            switch2entireCompFromPreCPR()

            if (isTurning) {
                checkRecoil()
            } else { //  최고점을 지나면 isTurning 이 true..  최고점 두번째에 checkRecoil 로 들어감.
                checkPosition(arrP) // 압력 시 위치 확인.
                checkDepth()
            }

            ///// 리턴 튜플 : (ccLimit: Int, depth: Double, caliVal: Int, rawCaliVal: Int)
            //print("   시간비교 ::   //  \(curStageObj.arrComprs.count)   \(curStageObj.prevCompRate)")
            //print("  data >>>   \(data.count)")
            let rcReturn = realCali.processCalibration(
                myEndPo - myStaPo, forceValue: force_value,
                rate: curStageObj.prevCompRate, data: curDatas)

            myStaPo = rcReturn.ccStaPo
            myLimitPo = rcReturn.ccLimit
            myEndPo = myStaPo + myLimitPo
            curAccelxyz = rcReturn.distSet

            if rcReturn.depth != 0 {
                //print("  실시간 캘리 ..결과값 rcReturn.depth  / force   \(rcReturn.depth)\t\(rcReturn.force)\t\(realCali.rawCali)\t\(realCali.buffPosi)")
                caliTempDepth = rcReturn.depth
                caliTempForce = rcReturn.force
            }

            if realCali.buffPosi == 1 && !isRevisionFinished {
                print("  데이터 리비젼 ...  count :  \(data.count)   arr size : \(arrRevision.count) ")
                startDataRevision()
            }
        }

        log.endFunctionLog()
    }


    var caliTempDepth = 0.0, caliTempForce = 0

    var curAccelxyz = DistSet()
    var isRevisionFinished = false
    var isRevisionNeed: Bool { get { return isAllInOnePrcss && !isRevisionFinished }}

    func addRevisionData() {
        if !isRevisionNeed { return }
        let aData = DataRevision(maxforce: maxForce, minforce: minForce, iscordepth: isCorrectDepth,
                                 iscorrecoil: isCorrectRecoil, pset: stage, ppos: data.count)
        arrRevision.append(aData)
        /*
         view_group[n].dr.add(new Data_Revision(
         view_group[n]., view_group[n].min_force_value, view_group[n].is_correct_depth == 0,
         view_group[n].is_correct_recoil == 0, view_group[n].stage - 1, view_group[n].count));
         */
    }

    func startDataRevision() {
        log.printAlways("startDataRevision :: 전과정 실시간 캘리 2 스트로크 후 리비젼.", lnum: 3)

        for (idx, dt) in arrRevision.enumerate() {
            var d = Int(  Double(dt.max_force - myStaPo) / Double(myEndPo - myStaPo) * 100)
            //print("   max/min : \(dt.max_force)  \(dt.min_force)  \(d) ")

            if (d < 10) { d = 0 }
            if (100 < d) { d = 100 }
            var r = Int(Double(dt.min_force - myStaPo) / Double(myEndPo - myStaPo) * 100)
            if r < data.pressureThreshold { r = 0 }
            if (100 < r) { r = 100 }

            let curStrk = curStageObj.arrComprs[idx]

            //print(" 기존 : \(curStrk.maxDepth)  \(curStrk.recoilDepth)     새로운 맥스 / 리코일 값 :  \(d) \t \(r)")

            curStrk.maxDepth = d
            curStrk.recoilDepth = r

            let isCorDepth = (Int(CC_WEAK_POINT) <= d && d <= Int(CC_STRONG_POINT)) // 새로 계산한 값이 정상.
            let isCorDepthBefore = dt.is_correct_depth
            let isCorRecoilBefore = dt.is_correct_recoil

            if isCorRecoilBefore { r = 0 }

            if !isCorDepthBefore && isCorDepth && isCorRecoilBefore {
                data.ccCorCount += 1
            } else if isCorDepthBefore && !isCorDepth && isCorRecoilBefore {
                data.ccCorCount -= 1
            }
        }
        arrRevision.removeAll()
        isRevisionFinished = true
    }

    func switch2entireCompFromPreCPR() {
        //log.printThisFNC("switch2entireCompFromPreCPR", comment: "   \(HsUtil.curStepStr((bleMan?.stepEntire)!)) ")
        if !bleMan!.isAllInOnePrcss { return }
        //if stepMoni == .S_WHOLESTEP_COMPRESSION && currentStep == .S_WHOLESTEP_COMPRESSION { return }
        if stepMoni == .S_WHOLESTEP_COMPRESSION { return }
        if depth > 10 || (stepMoni == .S_WHOLESTEP_PRECPR && isWrongPosi) {
            stepMoni = .S_WHOLESTEP_COMPRESSION
            //currentStep = .S_WHOLESTEP_COMPRESSION
        }
    }

    func sensorConnectionCheck(accl1 : Int16, accl2 : Int16, accl3 : Int16, breathVal : Int) -> Bool { // Z(Gravity), X, Y
        let b1 = accl1 == 0, b2 = (accl2 == -1 || accl2 == 0 || accl2 == 255), b3 = (accl3 == -1 || accl3 == 0 || accl3 == 255 || accl3 == 3)
        if (b1 && b2 && b3) {
            print("   \(accl1)   \(accl2)   \(accl3)")
            log.printAlways(" 패드 연결 유실 ", lnum: 10)
            bleMan!.stateMan.sensorConnectionLost(false)  // 패드 연결이 끊겼다.
            return true
        }
        if (breathVal == 0) {
            log.printAlways(" 호흡 센서 연결 유실 ", lnum: 10)
            bleMan!.stateMan.sensorConnectionLost(true)  // 호흡 연결이 끊겼다.
            return true
        }
        bleMan?.stateMan.sensorConnectionsAreAllGood()
        log.함수차원_출력_End()
        return false
    }

    var leftDepth = 0, rigtDepth = 0, minForce = 999999999, maxForce = 0

    // MARK: - 공통 데이터 파싱.  센서 값들 계산.
    /// 공통 데이터 파싱.  센서 값들 계산.
    func commonDataParsing() -> Bool {
        //log.printThisFunc(" 공통 데이터 파싱 ", lnum: 1)
        log.함수차원_출력_End()
        //var accel_raw_short:Int16, accel_raw_short2:Int16, accel_raw_short3:Int16
        let piezo1_value:UInt16 = parseTwoBytes(curDatas[3], and: curDatas[4])
        let piezo2_value:UInt16 = parseTwoBytes(curDatas[5], and: curDatas[6])
        let left_force_value = HSDataCalculator.calculateForce(piezo1_value)
        let right_force_value = HSDataCalculator.calculateForce(piezo2_value)

        // 둘 중의 맥스값 사용.
        force_value = max(Int(left_force_value), Int(right_force_value))  // Int(left_force_value + right_force_value)

        breath_value = Int(parseTwoBytes(curDatas[17], and: curDatas[18])) // 기압..  이 값이 0 이면 연결 유실.
        log.logThis(" force : \(force_value),  breath : \(breath_value) ")

        curStroke.newPacketReceived()
        if isRevisionNeed {
            if (force_value < minForce) { minForce = force_value }
            if (maxForce < force_value) { maxForce = force_value }
        }
        let accel1 = Int16(curDatas[16]) // Z Int16(curDatas[16])  // 단절 시 -1  -1  0
        let accel2 = Int16(curDatas[14]) // X
        let accel3 = Int16(curDatas[15]) // Y
        //log.logThis("   accel  ::   \(accel1)   \(accel2)   \(accel3)  ")
        if sensorConnectionCheck(accel1, accl2: accel2, accl3: accel3, breathVal: (data.brethVal)) {
            log.logThis("  sensorConnectionCheck  >>>>>  /t/t return false", lnum: 2)
            return false
        }
        //print("     myStaPo  \(myStaPo)  myEndPo   \(myEndPo)   myLimitPo   \(myLimitPo)")
        if (myEndPo == 0) {
            myStaPo = data.ccStaPoint; myEndPo = data.ccEndPoint; myLimitPo = data.ccLimitPoint;
            realCali.myStaPoint = myStaPo
            //print("  Initial Set  :::  \(myStaPo)  \(myEndPo)  \(myLimitPo) ")
        }

        if bleMan!.isCalibration {   //case CalibrationView01:
            caliSubSetDepthAmount()
        } else {  //default:
            //print(" asf  cali  nonono   \( Float ((data.ccEndPoint) - (data.ccStaPoint)) )  ")
            //log.logThis("  value ::  \( Float ((data.ccEndPoint) - (data.ccStaPoint)) )" )
            //            log.logThis("  value ::  ccEnd : \((data.ccEndPoint) ) ccSta  \((data.ccStaPoint) ) " );
            leftDepth = Int(Float(left_force_value - (myStaPo)) / Float((myEndPo) - (myStaPo)) * 100)
            rigtDepth = Int(Float(right_force_value - (myStaPo)) / Float((myEndPo) - (myStaPo)) * 100)
            if (leftDepth < 10)  { leftDepth = 0 }; if (leftDepth > 100) { leftDepth = 100}
            if (rigtDepth < 10)  { rigtDepth = 0 }; if (rigtDepth > 100) { rigtDepth = 100}
            depth = Int(Float(force_value - myStaPo) / Float(myEndPo - myStaPo) * 100)  // 그래프 값...
            amount = Int(Float(breath_value - (data.rpStaPoint)) / Float((data.rpEndPoint) - (data.rpStaPoint)) * 100)
        }

        if HsBleSingle.inst().manikinKind.theVal == 1 {
            // print("  프레스탄 보정.  prestan manikin  \(depth)")
            depth = Int(Double(depth) * 13.0 / 12.0)    //print("  prestan manikin  after  \(depth)")
        }
        if (depth < 10)  { depth = 0 }; if (depth > 100) { depth = 100}
        if (amount < 10) { amount = 0 }; if (amount > 100) { amount = 100 }
        dir = Int(curDatas[7] & 0xFF)

        print( " commonDataParsing  ::    <c:11 b:12>      __________  depth < \(depth)>    amount < \(amount)> __________" +
            "     count < \(data.count)  correctCount < \(data.ccCorCount) | \(data.rpCorCount) >      dir : \(dir)  \n")
        return true
    }
    
    //////////////////////////////////////////////////////////////////////       [ UI Sub  ]
    
    /*
     UI View 에서 다음과 같이 처리..
     RC_Return rc_return = RC.process_accel(depth_min_cv, depth_max_cv, CC_END_POINT - CC_START_POINT);
     CC_END_POINT = CC_START_POINT + rc_return.getCc_limit();
     */

    // MARK: - 실시간 캘리브레이션 ...


    // MARK: - 체크 리코일
    func checkRecoil() { // 리코일 시 불리는 함수..  min_depth 를 계속 내려 줌..  처음엔 100..
        if (!data.isCorrectPosition) { return }
        //log.printThisFunc("checkRecoil", lnum: 1)
        log.logThis("  내려오고 있는 중 ......      depth : \( depth ) >>>>>>>>>>>>  ✸ ✸ ❀ ❀ ◎ ◉ ◎  min_depth : \(min_depth) ✸ ✸ ❀ ❀ ◎ ◉ ◎   <<<<<<<<<<<<<   ", lnum: 1)

        //if (depth > ccFilter && (depth - ccFilter) <= min_depth) {   //  적십자 수정 전.
        if (depth > 20 && (depth - ccFilter) <= min_depth) { // 아직 리코일 판정할 때가 아님.. 최소값 갱신만 확인..
            if (depth <= min_depth) {
                min_depth = depth; // 최소값 설정.
            }
        } else { // 맨 아래까지 내려오면 불리는 함수.
            //log.logThis("   check Recoil :: @777   isTurning = false;  최저점 도달..   사이클 시간 모두 리셋... ..   \( depth )   \(min_depth)  ", lnum: 2)
            isTurning = false;

            //if (depth > 10 && min_depth > 10) {  // 적십자 적용 전
            if (depth > 20 && min_depth > 20) {
                curStageObj.lastCompObj.recoilDepth = min(depth, min_depth, max_depth) // 리코일 미완성 시 최소값 저장.
                curStageObj.lastCompObj.maxDepth = max(depth, min_depth, max_depth)

                speedLogic?.recoilAct() //bleMan?.messageName = "message_notrelease"  //data.isMessageDone = true
                isCorrectRecoil = false;
            }

            if (data.isCorrectPosition && data.isCorrectDepth == 0 && isCorrectRecoil) {
                data.ccCorCount += 1
                curStageObj.ccPassCnt += 1

                let strokeCycle = curStageObj.lastCompObj.strokeTimer.timeSinceStart  // 첫 사이클 예외..

                if speedLogic != nil && speedLogic!.ifSpeedOKandProceedUIactions(strokeCycle) {
                    speedLogic!.finallyGoodAct() //bleMan?.messageName = "message_good";  data.isMessageDone = true
                }
            }

            addRevisionData()

            increaseCompCount()

            isCycleStart = false
            clearCcCycle()
            log.logThis(" checkRecoil :   ... count++  :  \(data.count)   ", lnum: 3)
        }
        log.함수차원_출력_End()
    }

    /// 압박 시작을 판단. check Position|Depth
    func startCompState() {

        if (first_cycle_start_time_set) { return }
        if stage != 1 && isAllInOnePrcss {  // 160203 크래쉬 되서 동영상 찍다가 추가.
            //data.arrSet[stage - 2].rpTime += curStageObj.lastCompObj.strokeTimer.timeSinceEnd // 여기서 크래쉬 됐슴.  이전의 rptime..
            data.arrSet[stage - 2].rpTime = compHoldTimer.timeSinceStart // 이게 최종적인 것...

            print("타임>> rptime세팅 ..   \(data.arrSet[stage - 2].rpTime) 이게 최종 맞음...")
        }
        curStageObj.lastCompObj.strokeTimer.markStart() // data.cycleStartTime = NSDate().timeIntervalSince1970
        data.operationTimer.resetTime()
        first_cycle_start_time_set = true;
    }

    // MARK: - check Position :  화살표 표시..
    func checkPosition(arrIn:[Bool]) {
        //log.printThisFunc("checkPosition", lnum: 1)
        let is_center = depth > Int(CC_PRESSURE_MINIMUM_POINT) // 20보다 커야 센터임...
        log.logThis("  is_center : \(is_center)  isCorrectPosition : \(data.isCorrectPosition)   arrIn \(arrIn[0]) | \(arrIn[1]) | \(arrIn[2]) | \(arrIn[3]) ", lnum: 0)

        var lr: Double = 5, rl: Double = 5
        if 0 < rigtDepth { lr = Double(leftDepth) / Double(rigtDepth) }
        if 0 < leftDepth { rl = Double(rigtDepth) / Double(leftDepth) }

        // let isCenterNew = Int(MINIMUM_POINT) < depth && rl <= 4 && lr <= 4  // MINIMUM_POINT 는 55임..

        for i in 0...3 {
            var isCenterNew = false
            if i % 2 == 1 { isCenterNew = switchThresholds[i] < depth }
            else { isCenterNew = switchThresholds[i] < depth && rl <= 4 && lr <= 4 }

            if (arrIn[i] && !isCenterNew && 1 < curAccelxyz.zV.max) { // || (i % 2 == 1 && arrIn[i] && !isCenterNew) {
                //if (i % 2 == 0 && arrIn[i] && !isCenterNew) || (i % 2 == 1 && arrIn[i] && !is_center) {
                // Wrong Position 케이스.  스위치가 눌리고, 센터가 아닐때..
                log.logThis(" for [\(i)] 문...    Wrong Position 케이스.  position, positionResult ", lnum: 0)
                //data.position[i] = true;
                //data.positionResult[i] = true // 멤버 변수..
                data.isCorrectPosition = false
                startCompState()
                cycleStarted()

                curStroke.wPosiIdx = i + 1 // 여기서 한번 테스트 해보자..

                if (!data.isMessageDone) {
                    log.printAlways(" !data.isMessageDone 케이스.   wrong position 세팅.  wrongPositionNum = \(i)", lnum: 2)
                    if AppIdTrMoTe == 0 { // 떨림 현상.
                        speedLogic?.wrongPositionAct()
                    }

                    maxValueForSL = 0
                    data.isMessageDone = true
                    wrongPositionNum = i;
                }
            } else {  // print(" checkPosition :   position OK case ...   wrongPositionNum : \(wrongPositionNum)      depth  \(depth) ")
                if (wrongPositionNum == i && depth <= 20) {  // 약하게 눌렸을 때 카운트.
                    wrongPositionNum = -1;
                    for j in 0...3 {
                        if arrIn[j] {
                            wrongPositionNum = j
                            break;
                        }
                    }
                    if wrongPositionNum == -1 {

                        realCali.setForceWrongPosition()

                        increaseCompCount()
                        isCycleStart = false
                        log.logThis("   wrong position >>>     ... count++  :  \(data.count)     \(wrongPositionNum)  \(depth)  ", lnum: 3)

                        clearCcCycle()
                    }
                }
            }
        }
        log.함수차원_출력_End()
    }


    func cycleStarted() {
        //log.logUiAction("  Cycle Start >>>>>>>>    strokeTimer : \(curStageObj.lastCompObj.strokeTimer.timeSinceEnd) sec  ")
        isCycleStart = true
        isCorrectDepth = false

        compHoldTimer.Reset()

    }

    func checkDepth() {
        //log.printThisFunc("checkDepth", lnum: 1)
        if !data.isCorrectPosition { // 올바르면 리턴 안 함...   아래 소스 실행..
            return
        } //print (" check Depth >>  depth \(depth)   max_depth \(max_depth)  ")

        if (depth > 20) { startCompState() }
        if !isCycleStart && 20 < depth { cycleStarted()  }

        if (max_depth <= 20 && depth <= 20) || max_depth <= depth {
            if (max_depth <= depth) { max_depth = depth }
        } else {
            if CC_WEAK_POINT <= Int32(max_depth) && Int32(max_depth) < CC_STRONG_POINT {
                isCorrectDepth = true
            }
            turningHappened()
        }
        log.함수차원_출력_End()
    }

    func turningHappened() {
        isTurning = true;
        curStageObj.arrPositionCnt[4] += 1

        maxValueForSL = max_depth

        //log.printAlways("  checkDepth ?? weakCheck   max_depth : \(max_depth)  < CC_WEAK Point  \(CC_WEAK_POINT)   Done ? \(data.isMessageDone)  \t\t\t 방향 전환 점..    ~~~~~~~~~~~~~~~~~~~~~~~~~", lnum: 5)
        if (max_depth < Int(CC_WEAK_POINT)) {
            speedLogic?.tooStrongWeakAct(true)
            data.isCorrectDepth = 1;
        } else if (max_depth > Int(CC_STRONG_POINT)) {
            speedLogic?.tooStrongWeakAct(false)
            data.isCorrectDepth = 2;
        } else {
            data.isCorrectDepth = 0;
        }
        // realCali.setForceMax(force_value)  이것두 없어짐..
    }

    /// 환자 상태 증가    state + +
    func increaseStateOfPatient() {
        log.printThisFNC("increaseStateOfPatient", comment: " 현재 상태 : \(health)")
        if health == 5 { return }
        health += 1
        log.logThis("    상태 증가 : \(health)  <<  health++ >> ")
    }

    // MARK: - 압박 카운트 증가 시 작업
    /// 압박 카운트. 스트로크 타이머 세팅. ccTime 세팅..
    func increaseCompCount() {
        log.printThisFunc("  압박 카운트 *증가 () ")
        let curCompStroke = curStageObj.lastCompObj
        var strokeCycle = curStageObj.lastCompObj.strokeTimer.timeSinceStart  // 첫 사이클 예외..
        if data.count == 0 { strokeCycle = 0.55 } // sec

        log.logThis("   datacount >>  : \(data.count) + 1  strokeCycle : \(strokeCycle)  maxDepth \(max_depth) ", lnum: 0)

        speedLogic?.ifSpeedOKandProceedUIactions(strokeCycle)
        //log.logThis("  curStageObj.lastCompObj.period : \(curStageObj.lastCompObj.period)   gameStartTime : \(data.operationTimer.timeSinceStart)    \(curStageObj.arrComprs.count) ", lnum: 0)
        data.count += 1
        curStageObj.ccCount = data.count // 테스트에서 필요..
        curCompStroke.maxDepth = max_depth

        curCompStroke.setPeriodByPacket()

        let newStroke = data.increaseCompCount(curCompStroke) // HmCompStroke() // New comp ... 요건 데이터 클래스로 이동...


        /*  real Cali.setRate(newStroke.period) // 실시간 캘리
         let rate = Int(60.0 / newStroke.period)

         print(" \t\t 압박 카운트 증가 :   rate : \(rate)  force : \(max_depth) ")

         if (rate < 80 || 130 < rate ) {  // 80 130
         real Cali.setForceMax(0); real Cali.setForce(0)
         } else {
         real Cali.setForce(max_depth)
         real Cali.increaseForceBufferPos()
         }  // 실시간 캘리   */

        curStageObj.arrComprs.append(newStroke)  // 최대값 저장.  마지막 하나는 카운트 외의 것임..

        log.logThis("     압박 카운트 증가  ... count++ :  \(data.count) count++   mad depth : \(max_depth)  cc cor count", lnum: 1)
        log.함수차원_출력_End()
    }

    func hideArrow(idx: Int) -> Bool {     // idx : img[idx] 를 켤 것인가?
        // idx : 1 > Left Arrow Image, 2 > Up, 3 > Right, 4 > Down..
        // // arrP 순서 : 머리, 왼, 다리, 오른.
        // Manikin Left ..   1 :
        var idx0 = idx - 1
        if !isManikinLeft && AppIdTrMoTe == 0 {
            if idx0 < 2 { idx0 += 2 }
            else        { idx0 -= 2 }
        }        //print(" idx : \(idx)  idx0 : \(idx0)  position[idx0] \(position[idx0])  ")
        //curStroke.wPosiIdx = i + 1 // 여기서 한번 테스트 해보자..
        if idx0 + 1 == curStroke.wPosiIdx { return false } // 원래 인덱스는 0 1 2 3 이므로 + 1을 하여 비교하면 됨..  이건 트레이너에서 보여짐.. 방향..
        //if data.position[idx0] == true { return false } 옛날 버전..
        return true
    }

    func activateNewCC() { // BleManager 에서 불림..  캘리브레이션 압박 30회 할 때..0
        stepMoni = .CAL_NEW_CC
        countP = 0
        is_init_a_value = false; is_init_p_value = false;
        is_pressure_start = false;
        is_turning_p = false; is_turning_d = false;

        //init_p = (data.ccStaPoint) //CC_START_POINT;

        min_d = 100;        max_d = -100;        limit_p = 2000;
        min_p = data.ccStaPoint + limit_p// init_p + limit_p;
        max_p = data.ccStaPoint //init_p;
    }

    /**
     전과정 세트 넘어갈 때 초기화
     */
    func initializeCompressionGoToNextSet() {
        log.printAlways("initialize Compression    Now < \(NSDate().timeIntervalSince1970)>  stage : \(stage)  stageLimit : \(stageLimit) ", lnum: 10)
        clearCcCycle(true)

        isCycleStart = false
        if stepMoni == .S_STEPBYSTEP_COMPRESSION { data.reset() }

        data.isCorrectPosition = true; data.isCorrectDepth = 0
        isCorrectRecoil = true
        wrongPositionNum = -1

        //data.isMessageDone = false // 처음 시작...  1.05.01  이거 살리면 카운트가 안 됨.
        
        max_depth = 0; min_depth = 100
        //data.position = [Bool] (count: 4, repeatedValue: false)
        //data.positionResult = [Bool] (count: 4, repeatedValue: false)
        data.operationTimer.resetTime()
        first_cycle_start_time_set = false
        data.count = 0; data.ccCorCount = 0; isTurning = false;
        is_comp_stop_timer = false
    }
    
    func clearCcCycle(dodgeHoldingTimeReset: Bool = false) {
        //log.printThisFunc("<<<<<     clear CC Cycle      holdTimer : \(compHoldTimer.GetSecond())    >>>>>", lnum: 5)
        max_depth = 0;  min_depth = 100;
        isCorrectRecoil = true;
        data.isCorrectPosition = true;
        data.isCorrectDepth = 0
        if !dodgeHoldingTimeReset { compHoldTimer.Reset() }
        log.함수차원_출력_End()
    }
}


