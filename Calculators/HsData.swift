//
//  HtData.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2015. 11. 20..
//  Copyright © 2015년 IMLab. All rights reserved.
//

import Foundation



struct CompObj {
    var set_num:Int = 0, try_num:Int = 0, depth:Int = 0, recoil:Int = 0, cycle:Double = 0, record_id:Int = 0
}
struct RespObj {
    var set_num:Int = 0, try_num:Int = 0, amount:Int = 0, record_id:Int = 0
}
struct CompInf {
    var set_num:Int = 0, position_count1:Int = 0, position_count2:Int = 0, position_count3:Int = 0, position_count4:Int = 0, position_count5:Int = 0,
    center_percent:Int = 0, record_id:Int = 0
}

class HsData: HtObject {  // BleSuMan 이 하나씩 갖음.
    //var log = HtLog(cName: "HtData")
    var arrSet = [HmUnitSet]()
    var scoreObj = CPR_Scoring_return()

    var isCorrectPosition = true, isMessageDone = false, isCorrectDepth = 0, isCorrectAmount = 0
    var dbRecordID = -1, parentID = -1
    var timeStamp = NSDate(), standard = 0
    
    //////////////////////////////////////////////////////////////////////     Timer 들..
    //var xgameStartTime: Double = 0, xcycleStartTime: Double = 0, xcycleEndTime: Double = 0
    var operationTimer = HtStopWatch() // gameStartTime
    
    var count: Int = 0, ccCorCount: Int = 0, rpCorCount: Int = 0

    var totalOpTime: Double { // 호흡 시간 / 전체 시간 in %
        log.printThisFunc("totalOpTime  Set >> ", lnum: 2)
        if arrSet.count == 0 { return 0 }

        var compT: Double = 0, totT: Double = 0
        arrSet.map { (set) -> () in
            compT += set.ccTime
            totT += (set.ccTime + set.rpTime)

            log.logThis("   Comp / Brth  \(set.ccTime)  /  \(set.rpTime) ", lnum: 1)
        }
        return 100 * compT / totT
    }

    var countD: Int = 0, countP: Int = 0, countRP: Int = 0
    var maxDepth: Int = 0, minDepth: Int = 0, maxAmount: Int = 0
    var depth: Int = 0, amount: Int = 0
    var forceVal: Int = 0, brethVal: Int = 0
    var rpStaPoint: Int = 0, rpEndPoint: Int = 0, ccStaPoint: Int = 0, ccEndPoint: Int = 0, discreminantPass = false

    var ccLimitPoint: Int = 0, ccLimitPointNew: Int = 0, ccLimitPointTemp: Int = 0
    var rpLimitPoint: Int = 0, rpLimitPointNew: Int = 0, rpLimitPointTemp: Int = 0
    var distanceFilter = 0.5, pressureThreshold = 10

    var compTimeInTest: String { get { return operationTimer.timeSinceIntStr } }

    /// 호흡 점수 정수로.
    var scoreCompInt : Int { get { return Int(scoreObj.comp_score * 100) }}
    var scoreCompColor: UIColor {
        get { if scoreCompInt < scorePassIndex { return colBttnRed }
              else { return colBttnGreen }}}
    var scoreCompImgname: String {
        get { if scoreCompInt < scorePassIndex { return "icon_comp_fail" }
        else { return "icon_comp_pass" }}}

    var scoreBrthInt : Int { get { return Int(scoreObj.resp_score * 100) }}
    var scoreBrthColor: UIColor {
        get { if scoreBrthInt < scorePassIndex { return colBttnRed }
        else { return colBttnGreen }}}
    var scoreBrthImgname: String {
        get { if scoreBrthInt < scorePassIndex { return "icon_resp_fail" }
        else { return "icon_resp_pass" }}}

    var scoreTotalInt : Int { get { return Int(scoreObj.total_score * 100) }}
    var scoreFraction : Int { get { return Int(scoreObj.fraction_score * 100) }} // fraction 은 totalOpTime 임..
    var scoreFractionColor: UIColor {
        get { if scoreFraction < scorePassIndex { return colBttnRed }
        else { return colBttnGreen }}}

    var totalStringColor: UIColor {
        get {
            //if scoreTotalInt total_score < Double(scorePassIndex) { return colBttnRed }
            if scoreTotalInt <= scorePassIndex { return colBttnRed }
            else { return colBttnGreen }
        }
    }

    override init() {
        super.init()
        log = HtLog(cName: "HsData")
    }
    
    convenience init(recordID: String, parID: String) {
        self.init()
        dbRecordID = Int(recordID)!; parentID = Int(parID)!
        log.printAlways("New Data from DB :: \(dbRecordID),  StudentID : \(parentID)")
    }

    convenience init (recd: FMResultSet) {
        self.init()
        self.dbRecordID = Int(recd.stringForColumn("ID"))!
        self.parentID = Int(recd.stringForColumn("Owner"))!
        self.standard = Int(recd.stringForColumn("Standard"))!
        timeStamp = readDate(recd, name: "theTime")
    }

    func setRpStaPo(val: Int) { rpStaPoint = val; print ("  rpStaPoint : \(rpStaPoint)  from \(val)") }

    func reset() {
        log.printThisFunc("reset : cc EndPoint : \(ccEndPoint)  cc StaPoint : \(ccStaPoint) ", lnum: 5)
        arrSet.removeAll() // 20160421 :: 모니터에서 여러번 반복할 때 카운트가 누적되는 문제..
        operationTimer.resetTime()
        count = 0; ccCorCount = 0; rpCorCount = 0
        countD = 0; countP = 0; countRP = 0
    }

    func increaseSet(stageNum: Int) -> HmUnitSet {
        log.printAlways("increaseSet(\(stageNum))")
        let newSet = HmUnitSet()
        newSet.setNum = stageNum
        newSet.arrComprs.append(HmCompStroke())
        newSet.arrBreath.append(HmBrthStroke())
        arrSet.append(newSet)
        return newSet
    }

    func increaseCompCount(curStrk: HmCompStroke) -> HmCompStroke {
        // wrong position 처리
        //print(" curStrk : arrset \(arrSet.count)")
        //let curStrk = arrSet.last!.arrComprs.last
        //curStrk.setPosiInfoWith(true) // position)
        curStrk.strkTime = curStrk.strokeTimer.timeSinceStart
        curStrk.setPeriodByPacket()

        let newStroke = HmCompStroke() // New comp ...
        newStroke.strokeTimer.markStart()
        newStroke.strokeTimer.markEnd()
        newStroke.strkTime = curStrk.strkTime // 실시간 캘리에서 rate 구할 때 사용

        return newStroke
    }
    
    func discreminant() -> Int {
        let total = 세트점수()
        discreminantPass = 75 <= total
        return Int(total)
    }

    func allSetPass() -> Bool {
        for aSet in arrSet {
            if aSet.allPass == false { return false }
        }
        return true
    }
    
    func finalProcess() {
        log.printThisFNC("finalProcess()", comment: " arrSet : \(arrSet.count)")
        for setObj in arrSet {
            log.logThis(" set passCount : \(setObj.ccPassCnt), \(setObj.rpPassCnt) ", lnum: 1)
            setObj.finalProcess()
        }
    }

    func ccCountJson() -> String {
        if arrSet.count == 0 { return "" }; var rStr = ""
        for st in arrSet { rStr += ", \(st.ccCount)"  }
        return "[ \(rStr.subStringFrom(2)) ]"
    }
    func rpCountJson() -> String {
        if arrSet.count == 0 { return "" }; var rStr = ""
        for st in arrSet { rStr += ", \(st.rpCount)"  }
        return "[ \(rStr.subStringFrom(2)) ]"
    }
    
    func compDepthJson() -> String {
        if arrSet.count == 0 { return "" }
        var rStr = ""
        for st in arrSet { rStr += ", \(st.compDepthJson())"  }
        return "[ \(rStr.subStringFrom(2)) ]"
    }
    
    func compRecoilJson() -> String {
        if arrSet.count == 0 { return "" }
        var rStr = ""
        for st in arrSet { rStr += ", \(st.compRecoilJson())"  }
        return "[ \(rStr.subStringFrom(2)) ]"
    }
    func compStrokeJson() -> String {
        if arrSet.count == 0 { return "" }
        var rStr = ""
        for st in arrSet { rStr += ", \(st.compStrokeJson())"  }
        return "[ \(rStr.subStringFrom(2)) ]"
    }
    func breathVolJson() -> String {
        if arrSet.count == 0 { return "" };  var rStr = ""
        for st in arrSet { rStr += ", \(st.breathVolJson())"  }
        return "[ \(rStr.subStringFrom(2)) ]"
    }

    func breathCycleJson() -> String {
        if arrSet.count == 0 { return "" };  var rStr = ""
        for st in arrSet { rStr += ", \(st.breathCycleJson())"  }
        return "[ \(rStr.subStringFrom(2)) ]"
    }


    func handsOffTime() -> String {
        if arrSet.count == 0 { return "" }; var rStr = ""
        for st in arrSet { rStr += ", \(Int(st.holdTime))"  }
        return "[ \(rStr.subStringFrom(2)) ]"
    }
    func rpCorrectJson() -> String {
        if arrSet.count == 0 { return "" }; var rStr = ""
        for st in arrSet { rStr += ", \(st.rpPassCnt)"  }
        return "[ \(rStr.subStringFrom(2)) ]"
    }
    func ccCorrectJson() -> String {
        if arrSet.count == 0 { return "" }; var rStr = ""
        for st in arrSet { rStr += ", \(st.ccPassCnt)"  }
        return "[ \(rStr.subStringFrom(2)) ]"
    }


    /// Test Json
    func showResultCounts(mark: String) {
        log.printThisFNC("showResultCounts", comment: " >\(mark)< set num : \(arrSet.count)")
        for st in arrSet {
            log.logThis(" Comp : \(st.ccPassCnt) / \(st.ccCount)  |||  Brth : \(st.rpPassCnt) / \(st.rpCount)", lnum: 1)
            st.showAllBreathTime()
        }
    }

    func rightPositionPercent() -> String {
        if arrSet.count == 0 { return "" }
        var rStr = ""
        for st in arrSet { rStr += ", \(st.ccCorrectPositionPercent)"  }
        return "[ \(rStr.subStringFrom(2)) ]"
    }
    
    func positionJson() -> String { // "pos1" : [ 0, 0, 0 ], "pos2" : [ 0, 0,   이 스트링 만들기
        var arrStr = ["", "", "", ""], norStr = ""
        for st in arrSet {
            var pCnt = [0, 0, 0, 0], cenCnt = 0
            for ob in st.arrComprs {
                if ob.wPosiIdx == 0 { cenCnt += 1 }
                else {
                    pCnt[ob.wPosiIdx - 1] += 1
                }
            }

            for idx in 0...3 {
                arrStr[idx] += "\(pCnt[idx]), "  // 3, 4, 5, 2 이렇게 추가..
            }
            norStr += "\(cenCnt), " // 중앙부 추가.
        }
        var rStr = ""
        for ix in 0...3 {
            rStr += "\"pos\(ix+1)\" : [ \(arrStr[ix].removeLast(2)) ], "
        }  //  "pos1" : [ 3, 4, 3, 4 ], "pos4" : [ 3, 3, 3, 3 ] 이렇게 생성..

        return rStr + "\"pos5\" : [ \(norStr.removeLast(2)) ]"
    }



    func 세트점수() -> Int {
        let scoring = CPR_Scoring(c_count: true, c_pos: true, c_rate: true, c_depth: true, c_recoil: true, r_count: true, r_amount: true, r_rate: true, ratio: 0.75, cm_5: 40, cm_6: 50, ml_400: 50, ml_700: 60,
                                  guideline: cprProtoN.theVal + 1)

        let compTime = arrSet.map({ (set) -> [Double] in // 각 세트의 매 압박 시간.
            set.arrComprs.map({ (comp) -> Double in
                comp.strkTime
            }) })

        let compDep = arrSet.map({ (set) -> [Int] in // 각 세트의 매 압박 시간.
            set.arrComprs.map({ (comp) -> Int in
                comp.maxDepth
            }) })
        let compRcl = arrSet.map({ (set) -> [Int] in // 각 세트의
            set.arrComprs.map({ (comp) -> Int in
                comp.recoilDepth
            }) })
        let amountt = arrSet.map({ (set) -> [Int] in // 각 세트의
            set.arrBreath.map({ (brth) -> Int in
                brth.max
            }) })
        let brTime = arrSet.map({ (set) -> [Double] in // 각 세트의 매 압박 시간.
            set.arrBreath.map({ (comp) -> Double in
                comp.strkTime
            }) })

        scoreObj = scoring.calculate_score(arrSet.map({ (set) -> Int in set.arrComprs.count }),
                                comp_pos: arrSet.map({ (set) -> Int in set.ccCorrectPositionPercent }),
                                comp_rate: compTime, comp_depth: compDep, comp_recoil: compRcl,
                                resp_count: arrSet.map({ (set) -> Int in set.arrBreath.count }),
                                resp_amount: amountt, resp_rate: brTime, fraction: totalOpTime, set_num: arrSet.count)

        //print("\n\n HsData : 토탈점수 : \(scoreObj.total_score)   \()")
        return Int(round(scoreObj.total_score * 100))
    }

    func scoreJson() -> String {       //  형식 ==>>  "resp_score": 90.7,
        let jObj = HsJsonBase()
        세트점수()

        jObj.addKeyString("overall_score", value: scoreObj.total_score.makePercentStr(1))
        jObj.addKeyString("comp_score", value: scoreObj.comp_score.makePercentStr(1))
        jObj.addKeyString("depth_score", value: scoreObj.comp_depth_score.makePercentStr(1))
        jObj.addKeyString("rate_score", value: scoreObj.comp_rate_score.makePercentStr(1))
        jObj.addKeyString("recoil_score", value: scoreObj.comp_recoil_score.makePercentStr(1))
        jObj.addKeyString("num_score", value: scoreObj.comp_num_score.makePercentStr(1))
        jObj.addKeyString("position_score", value: scoreObj.comp_pos_score.makePercentStr(1))
        jObj.addKeyString("resp_score", value: scoreObj.resp_score.makePercentStr(1))
        jObj.addKeyString("resp_vol_score", value: scoreObj.resp_amount_score.makePercentStr(1))
        jObj.addKeyString("resp_rate_score", value: scoreObj.resp_rate_score.makePercentStr(1))
        jObj.addKeyString("resp_num_score", value: scoreObj.resp_num_score.makePercentStr(1))
        jObj.addKeyString("comp_ratio_score", value: scoreObj.fraction_score.makePercentStr(1), withFinishComma: false)
        jObj.finishEncoding()
        return jObj.jStr
    }

}
