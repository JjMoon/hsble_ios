//
//  HSLog.swift
//  HS Test
//
//  Created by imlab on 2016. 11. 16..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation

public func HSLogNotice(message: String = "", classInfo: AnyObject?=nil, function: String = #function, line: Int = #line) {
    print("✅[Notice] 🆑\(classInfo) : 🎯\(function) : \(line) - 👉\(message)")
}
public func HSLogWarning(message: String = "", classInfo: AnyObject?=nil, function: String = #function, line: Int = #line) {
    print("⚠️[Warning] 🆑\(classInfo) : 🎯\(function) : \(line) - 👉\(message)")
}
public func HSLogError(message: String = "", classInfo: AnyObject?=nil, function: String = #function, line: Int = #line) {
    print("🚨[Error] 🆑\(classInfo) : 🎯\(function) : \(line) - 👉\(message)")
}
