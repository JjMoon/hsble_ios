//
//  MonitorIntro.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2015. 12. 24..
//  Copyright © 2015년 IMLab. All rights reserved.
//

import Foundation

class MonitorIntro: UIViewController {
    var log = HtLog(cName: "MonitorIntro")
    
    var stdnt = HmStudent(nm: "DebugName", em: "debug@test.com", pw: "password", phn: "000-111-222", ag: 23)
    var dObj = HsData()
    
    let dbHandle = HsBleMaestr.inst.fmdbHandler


    override func viewDidAppear(animated: Bool) {

        let flo = 123.456789012345
        print(" MonitorIntro ::     \t\t\t    format 예제 >>   \(flo.format(".3"))   \(flo.format(".5")) ")

    }

    @IBAction func bttnActVCTest(sender: AnyObject) {

        let vc = SendDataVC(nibName: "SendData", bundle: nil)
        presentViewController(vc, animated: viewAnimate, completion: nil)


    }


    @IBAction func bttnActResultDB(sender: AnyObject) {
        log.logUiAction("__  bttnActResultDB  __")
        
        dbHandle.addAStudent(stdnt)
        HsBleMaestr.inst.arrStudent = dbHandle.grabAllStudents()
        
        log.logUiAction("  Add Student ::  \(HsBleMaestr.inst.arrStudent.count)")
        
        var aSet = HmUnitSet(theCode: "Set1")  // New Set
        aSet.setNum = 1
        aSet.ccPassCnt = 1; aSet.rpPassCnt = 2
        aSet.arrPositionCnt = [0, 0, 1, 0, 0]
        var comp = HmCompStroke();  comp.setVars(100, recoilD: 0, prd: 0.09);   aSet.arrComprs.append(comp)
        comp = HmCompStroke();      comp.setVars(58, recoilD: 15, prd: 0.11);   aSet.arrComprs.append(comp)
        comp = HmCompStroke();      comp.setVars(85, recoilD: 5, prd: 0.11);   aSet.arrComprs.append(comp)
        comp = HmCompStroke();      comp.setVars(50, recoilD: 5, prd: 0.11);   aSet.arrComprs.append(comp)
        comp = HmCompStroke();      comp.setVars(50, recoilD: 5, prd: 0.11);   aSet.arrComprs.append(comp)
        comp = HmCompStroke();      comp.setVars(63, recoilD: 15, prd: 0.11);   aSet.arrComprs.append(comp)
        comp = HmCompStroke();      comp.setVars(64, recoilD: 19, prd: 0.11);   aSet.arrComprs.append(comp)
        comp = HmCompStroke();      comp.setVars(65, recoilD: 20, prd: 0.11);   aSet.arrComprs.append(comp)
        comp = HmCompStroke();      comp.setVars(66, recoilD: 21, prd: 0.11);   aSet.arrComprs.append(comp)
        comp = HmCompStroke();      comp.setVars(70, recoilD: 5, prd: 0.11);   aSet.arrComprs.append(comp)
        comp = HmCompStroke();      comp.setVars(78, recoilD: 22, prd: 0.11);   aSet.arrComprs.append(comp)
        comp = HmCompStroke();      comp.setVars(79, recoilD: 0, prd: 0.11);   aSet.arrComprs.append(comp)
        comp = HmCompStroke();      comp.setVars(70, recoilD: 5, prd: 0.11);   aSet.arrComprs.append(comp)
        comp = HmCompStroke();      comp.setVars(81, recoilD: 10, prd: 0.11);   aSet.arrComprs.append(comp)
        comp = HmCompStroke();      comp.setVars(82, recoilD: 0, prd: 0.11);   aSet.arrComprs.append(comp)
        comp = HmCompStroke();      comp.setVars(77, recoilD: 5, prd: 0.11);   aSet.arrComprs.append(comp)
        comp = HmCompStroke();      comp.setVars(83, recoilD: 0, prd: 0.11);   aSet.arrComprs.append(comp)
        comp = HmCompStroke();      comp.setVars(84, recoilD: 0, prd: 0.11);   aSet.arrComprs.append(comp)
        comp = HmCompStroke();      comp.setVars(85, recoilD: 0, prd: 0.11);   aSet.arrComprs.append(comp)
        comp = HmCompStroke();      comp.setVars(86, recoilD: 0, prd: 0.11);   aSet.arrComprs.append(comp)
        comp = HmCompStroke();      comp.setVars(87, recoilD: 0, prd: 0.11);   aSet.arrComprs.append(comp)
        comp = HmCompStroke();      comp.setVars(88, recoilD: 0, prd: 0.11);   aSet.arrComprs.append(comp)
        comp = HmCompStroke();      comp.setVars(89, recoilD: 0, prd: 0.11);   aSet.arrComprs.append(comp)
        comp = HmCompStroke();      comp.setVars(90, recoilD: 0, prd: 0.11);   aSet.arrComprs.append(comp)
        comp = HmCompStroke();      comp.setVars(91, recoilD: 0, prd: 0.11);   aSet.arrComprs.append(comp)

        var brth = HmBrthStroke(mx: 60, prd: 2.5);  aSet.arrBreath.append(brth)
        brth = HmBrthStroke(mx: 66, prd: 2.5);      aSet.arrBreath.append(brth)
        aSet.holdTime = 4
        aSet.ccCount = aSet.arrComprs.count;  aSet.rpCount = aSet.arrBreath.count
        dObj.arrSet.append(aSet)
        aSet = HmUnitSet(theCode: "Set2")  // New Set
        aSet.setNum = 2
        aSet.ccPassCnt = 1; aSet.rpPassCnt = 2
        aSet.arrPositionCnt = [0, 1, 0, 0, 1]
        comp = HmCompStroke();      comp.setVars(20, recoilD: 33, prd: 0.10);   aSet.arrComprs.append(comp)
        comp = HmCompStroke();      comp.setVars(19, recoilD: 37, prd: 0.11);   aSet.arrComprs.append(comp)
        brth = HmBrthStroke(mx: 45, prd: 1.4);      aSet.arrBreath.append(brth)
        brth = HmBrthStroke(mx: 46, prd: 1.5);      aSet.arrBreath.append(brth)
        aSet.holdTime = 7
        aSet.ccCount = aSet.arrComprs.count;  aSet.rpCount = aSet.arrBreath.count
        dObj.arrSet.append(aSet)
        stdnt.myData = dObj

        dbHandle.saveDataAfterEntireProcess(stdnt)
        //dbHandle.addAData(dObj, parent: HsBleMaestr.inst.arrStudent.last!)
        // dbHandle.grabAllDatum()
        
        //HsStudentDataJobs.singltn.sendDataRequest(stdnt, successBlck: { }, failureBlck: { } )
        
        dbHandle.readStudentDataSetInfo() // 여기서 다시 읽고..
        //HsStudentDataJobs.singltn.sendDataRequest(stdnt, successBlck: { }, failureBlck: { } ) // json 출력
    }
    
    func showPopoverView() {
        let vc = DataSavePopoverVC(nibName: "DataSavePopover", bundle: nil)
        vc.modalPresentationStyle = .Popover // .Popover
        presentViewController(vc, animated: viewAnimate, completion: nil)
        vc.popoverPresentationController?.sourceView = view
        vc.popoverPresentationController?.sourceRect = view.frame
        vc.setLanguageString()

    }
    
}


class MyTestView: UIView {
    var graph = GraphSubStroke()

    override func drawRect(rect: CGRect)  {
        graph.startDraw(UIGraphicsGetCurrentContext()!)

        graph.drawPartMoon(90, cenY: 50, rad: 40, h: 20, col: UIColor.blueColor())
        graph.drawPartMoon(90, cenY: 120, rad: 50, h: 5, col: UIColor.redColor())
    }

    func drawHalfMoonYellow() {

        let ctx = UIGraphicsGetCurrentContext()!

        CGContextBeginPath(ctx);
        //CGContextMoveToPoint(ctx, 10, 10);

        //let theRect = CGRect(x: Int(corX - dia * 0.5), y: Int(corY - dia * 0.5), width: Int(dia), height: Int(dia))
        CGContextAddArc(ctx, 80, 50, 50, 3.14/4, 3.14/4 * 3, 0)

        CGContextClosePath(ctx)

        CGContextSetFillColorWithColor(ctx, UIColor.yellowColor().CGColor)
        //CGContextFillEllipseInRect(ctx, theRect)

        // draw the path
        CGContextDrawPath(ctx, CGPathDrawingMode.Fill);

    }

}




