//
//  OperateCompSign.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2016. 3. 29..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation

class OperateCompSign : HtView {

    var spdLgc: HsSpeedLgcMon?

    @IBOutlet weak var labelWeakStrong: UILabel!
    @IBOutlet weak var labelWrong: UILabel!
    @IBOutlet weak var labelSpeed: UILabel!
    @IBOutlet weak var labelRecoil: UILabel!

    @IBOutlet weak var labelCompression: UILabel!
    @IBOutlet weak var labelSpeedText: UILabel!
    @IBOutlet weak var labelRecoilText: UILabel!
    @IBOutlet weak var labelPosition: UILabel!


    var isCompCase = true

    func setUiObjectsToSpeedLogic(sLgc: HsSpeedLgcMon) {
        spdLgc = sLgc

        spdLgc!.setMonitorLabels(labelWeakStrong, spd: labelSpeed, wrong: labelWrong, recoil: labelRecoil)
    }

    func prepareCompression(isComp: Bool) {
        isCompCase = isComp
        setLanguageString()
    }

    override func setLanguageString() {
        if isCompCase {
            labelCompression.text = langStr.obj.depth
        } else {
            labelCompression.text = langStr.obj.breath_volume
        }
        labelSpeedText.text = langStr.obj.rate
        labelRecoilText.text = langStr.obj.recoil
        labelPosition.text = langStr.obj.position

        // position hide.
        labelWrong.show_다음이_참이면(isCompCase)
        labelPosition.show_다음이_참이면(isCompCase)
        labelRecoil.show_다음이_참이면(isCompCase)
        labelRecoilText.show_다음이_참이면(isCompCase)
    }

}
