//
//  MainVC.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2015. 11. 10..
//  Copyright © 2015년 IMLab. All rights reserved.
//

import Foundation
import UIKit

class MainVC : UIViewController {
    var log = HtLog(cName: "MainVC")

    @IBOutlet weak var labelVersion: UILabel!
    @IBOutlet weak var bttnInfo: UIButton!
    @IBOutlet weak var bttnRescure: UIButton!
    @IBOutlet weak var bttnDoctor: UIButton!
    
    @IBOutlet weak var bttnStartCourse: UIButton!
    @IBOutlet weak var bttnDataManagement: UIButton!
    
    @IBOutlet weak var consBttnDistToBottom: NSLayoutConstraint!
    @IBOutlet weak var bttnDebug: UIButton!  //var vwCompPlmn = PlusMinusView()

    @IBAction func bttnActInfo(sender: AnyObject) {
        openHeartisenseInfoWeb(1)// Monitor
    }

    @IBAction func bttnActRescure(sender: AnyObject) {
        isRescureMode = true
        buttonColorChange()
    }

    @IBAction func bttnActDoctor(sender: AnyObject) {
        isRescureMode = false
        buttonColorChange()
    }

    @IBAction func bttnActStartEducation(sender: AnyObject) {
        let vc = ConnectVC()
        vc.isCalibration = false
        HsBleMaestr.inst.setConnectionMode(false)
        self.navigationController?.pushViewController(vc, animated: viewAnimate)
    }

    @IBAction func bttnActManageStudent(sender: AnyObject) {
        //let vc =  AssignStudentView(nibName: "AssignStudentsView", bundle: nil)
        let vc = ManageStudentVC()
        self.navigationController?.pushViewController(vc, animated: viewAnimate)
    }

    @IBAction func bttnDebug(sender: AnyObject) {
        let vc = MonitorIntro(nibName: "MonitorIntro", bundle: nil)
        presentViewController(vc, animated: viewAnimate, completion: nil)
    }

    @IBAction func bttnActSetting(sender: AnyObject) {
        let actSheet = UIActionSheet(title: nil, delegate: self, cancelButtonTitle: nil,
                                     destructiveButtonTitle: nil, otherButtonTitles:
            langStr.obj.option_preference, langStr.obj.option_calibration, langStr.obj.bls_setting)
            //local Str("setting_init"), loalStr("setting_sensitivity"))
        actSheet.showInView(self.view)
    }

    //////////////////////////////////////////////////////////////////////     [ UI   ViewDidLoad   << >> ]

    override func viewDidLoad() {
        AppIdTrMoTe = 1 // 모니터 세팅..
        if isDebugMode { bttnDebug.hidden = false }
        labelVersion.text = monitorVersion
        buttonColorChange()

        print("\n\n\n   Main VC   viewDidLoad    AppIdTrMoTe = 1   TimeStamp : \(NSDate(timeIntervalSinceNow: 0).formatYYMMDDspaceTime)  \n\n\n")
        print(" Student count ;;   \(HsBleMaestr.inst.arrStudent.count)")

        setLanguageString()
    }

    override func viewWillAppear(animated: Bool) {
        setLanguageString()
        setUIaccordingToProtocol()
    }

    override func viewDidAppear(animated: Bool) {
//        switch cprProtoN.theVal { // 이걸 원상 복구 하는 건 Select View 에서..
//        case 0: CC _STRONG_POINT = 101
//        default: CC _STRONG_POINT = Int32(CC_STRONG_POINT_DEFAULT)        }
        for b in HsBleMaestr.inst.arrBleSuMan {
            b.closePort()
        }
    }

    func buttonColorChange() {
        if isRescureMode {
            bttnRescure.backgroundColor = UIColor.redColor()
            bttnDoctor.backgroundColor = UIColor.grayColor()
        } else {
            bttnRescure.backgroundColor = UIColor.grayColor()
            bttnDoctor.backgroundColor = UIColor.redColor()
        }
    }

    func setUIaccordingToProtocol() {
        if consBttnDistToBottom != nil {
            if cprProtoN.theVal == 0 {
                consBttnDistToBottom.constant = 200
            } else { consBttnDistToBottom.constant = 350 }
        }
        bttnRescure.show_다음이_참이면(cprProtoN.theVal == 0)
        bttnDoctor.show_다음이_참이면(cprProtoN.theVal == 0)
    }

    // MARK:  언어 세팅.
    func setLanguageString() {
        bttnRescure.localizeSizeAlignment(1)
        bttnDoctor.localizeSizeAlignment(1)
        bttnStartCourse.localizeSizeAlignment(1)
        bttnDataManagement.localizeSizeAlignment(1)
        bttnRescure.setTitle(langStr.obj.lay_rescuer, forState: UIControlState.Normal)
        bttnDoctor.setTitle(langStr.obj.bls_rescuer, forState: UIControlState.Normal)
        bttnStartCourse.setTitle(langStr.obj.start_course, forState: UIControlState.Normal)
        bttnDataManagement.setTitle(langStr.obj.manage_data, forState: UIControlState.Normal)
    }
}

extension MainVC : UIActionSheetDelegate {

    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int) {
        log.printThisFunc("actionSheet_delegate")

        switch buttonIndex {
        case 0:
            let vc = InitialSettingVC(nibName: "InitialSettingVCPort", bundle: nil) //("InitialSettingVC")
            print("self.presentViewController  InitialSettingVCPort \(vc)")

            print("self.presentViewController  InitialSettingVCPort 22")
            dispatch_async(dispatch_get_main_queue(), {
                print("self.presentViewController  InitialSettingVCPort ")
                self.presentViewController(vc, animated: viewAnimate, completion: nil)
            })
        case 1:
            let vc = ConnectVC()
            vc.isCalibration = true
            HsBleMaestr.inst.setConnectionMode(true)
            dispatch_async(dispatch_get_main_queue(), {
                self.navigationController?.pushViewController(vc, animated: viewAnimate)
            })
        case 2:
            let vc = ScoreOptionVC()
            dispatch_async(dispatch_get_main_queue(), {
                self.navigationController?.pushViewController(vc, animated: viewAnimate)
            })
        default:
            log.printAlways("  Closeing ..")
        }
    }

    func drawBoxes() {

        let testBox = HtBars(CGRect(x: 50, y: 50, width: 200, height: 50), [ 1, 2, 3], [ colBttnYellow, colBttnGreen, colBttnRed ])
        view.addSubview(testBox)


        let consBox = HtBars(frame: CGRect(x: 50, y: 150, width: 300, height: 30))


        view.addSubview(consBox)

        consBox.setWidths([3, 3, 3],  [ colBttnYellow, colBttnGreen, colBttnRed ])
    }

    func test2() {
        let dObj = HsData()

        var aSet = HmUnitSet()
        aSet.ccPassCnt = 23;         aSet.ccCount = 28
        aSet.rpPassCnt = 1;         aSet.rpCount = 2
        aSet.ccTime = 15;            aSet.holdTime = 6
        dObj.arrSet.append(aSet)
        aSet = HmUnitSet()
        aSet.ccPassCnt = 25;        aSet.ccCount = 26
        aSet.rpPassCnt = 2;         aSet.rpCount = 2
        aSet.ccTime = 14;           aSet.holdTime = 6
        dObj.arrSet.append(aSet)

        let discrem = dObj.discreminant()
        print("\n\n\n  \t\t\t res ult : \(discrem) \n\n\n")
    }

    func test1() {
        let dObj = HsData()

        var aSet = HmUnitSet()
        aSet.ccPassCnt = 23;         aSet.ccCount = 28
        aSet.rpPassCnt = 1;         aSet.rpCount = 2
        aSet.ccTime = 13;            aSet.holdTime = 12
        dObj.arrSet.append(aSet)
        aSet = HmUnitSet()
        aSet.ccPassCnt = 20;        aSet.ccCount = 25
        aSet.rpPassCnt = 2;         aSet.rpCount = 2
        aSet.ccTime = 20;           aSet.holdTime = 15
        dObj.arrSet.append(aSet)

        let discrem = dObj.discreminant()
        print("\n\n\n  \t\t\t res ult : \(discrem) \n\n\n")
    }

}

