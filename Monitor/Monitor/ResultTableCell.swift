//
//  ResultUnitView.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2015. 11. 26..
//  Copyright © 2015년 IMLab. All rights reserved.
//

import Foundation
import UIKit


class ResultTableCell : UITableViewCell {
    @IBOutlet weak var lblSetNum: UILabel!
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblRpCount: UILabel!
    @IBOutlet weak var lblRpTime: UILabel!

    var setNum: Int = 0
    var dataObj: HsData = HsData()
    //var dObj = HsBleManager.inst().dataObj
    
    func setItems(setNumber: Int, dObj: HsData) {
        setNum = setNumber
        dataObj = dObj
        let idx = setNum - 1
        let stgObj = dataObj.arrSet[idx]

        lblSetNum.text = "\(setNum)"
        
//        switch langIdx {
//        case 0: // en
//            lblSetNum.text = "\(langStr.obj.cycle) \(setNum)"
//        default: // 1: ko
//            lblSetNum.text = "\(setNum) \(langStr.obj.cycle)"
//        }
        lblSetNum.alignCenter()
        lblCount.text = "\(stgObj.ccPassCnt) / \(stgObj.ccCount) "
        if 23 <= stgObj.ccPassCnt && 27 <= stgObj.ccCount && stgObj.ccCount <= 33 {
            lblCount.textColor = colBttnGreen
        }
        else { lblCount.textColor = HmGraphSetting.inst.warningRed }
        
        let pushpermin = stgObj.분당압박횟수()
        lblTime.text = String(pushpermin)
        if 100 <= pushpermin && pushpermin <= 120 { lblTime.textColor = colBttnGreen }
        else { lblTime.textColor = HmGraphSetting.inst.warningRed }

        lblRpCount.text = "\(stgObj.rpPassCnt) / \(stgObj.rpCount) "
        if 1 <= stgObj.rpPassCnt && 1 <= stgObj.rpCount && stgObj.rpCount <= 3 {
            lblRpCount.textColor = colBttnGreen
        } else { lblRpCount.textColor = HmGraphSetting.inst.warningRed }
        
        lblRpTime.text = String(Int(stgObj.rpTime))
        if stgObj.rpTime < 10 { lblRpTime.textColor = colBttnGreen }
        else { lblRpTime.textColor = HmGraphSetting.inst.warningRed }

    }
}
