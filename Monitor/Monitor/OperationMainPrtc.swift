//
//  OperationMainPrtc.swift
//  HS Monitor
//
//  Created by Jongwoo Moon on 2016. 8. 8..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation


// 처음 뷰 생성시 enlargeButton 을 붙인다..  거기를 눌르면 콜백을 계속 받자..
extension OperationMainVC: PrtclEnlargeView {

    func enlargeView(unitVw: OperateUnitView) {
        targetView = unitVw
        tarPoint = screenCenter
        // baseOrigin 은 노터치.

        print(" screen center : \(screenCenter) ")

        var selfAsEnlargeView : PrtclEnlargeView = self
        selfAsEnlargeView.animation()
    }

    func enlargeCallback(unitVw: OperateUnitView) {
        targetView = unitVw
        tarPoint = screenCenter

        var selfAsEnlargeView : PrtclEnlargeView = self
        selfAsEnlargeView.animation()
    }


}