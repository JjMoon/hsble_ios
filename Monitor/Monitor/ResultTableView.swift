//
//  ResultTableView.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2015. 11. 30..
//  Copyright © 2015년 IMLab. All rights reserved.
//

import Foundation



class ResultTableView : UIView, UITableViewDelegate, UITableViewDataSource {
    var log = HtLog(cName: "ResultTableView")
    var bleMan: HsBleSuMan?
    var openResultGraphCljr: (HsBleSuMan) -> () = { (bleObj: HsBleSuMan) -> Void in }

    //@IBOutlet weak var labelName: UILabel!
    
    @IBOutlet weak var tableResult: UITableView!
    //@IBOutlet weak var labelTitle: UILabel!
    //  30  80  60   80   60  => 50 75 55  "  "
    @IBOutlet weak var labelCompressionTitle: UILabel!
    @IBOutlet weak var labelBreathTitle: UILabel!
    @IBOutlet weak var labelCycle: UILabel!
    @IBOutlet weak var labelCompCount: UILabel!
    @IBOutlet weak var labelCompTime: UILabel!
    @IBOutlet weak var labelBrthCount: UILabel!
    @IBOutlet weak var labelBrthTime: UILabel!
    
    @IBAction func bttnHidden(sender: AnyObject) {
        hideMe()
    }
    
    override func awakeFromNib() {
        //log.printThisFunc(" awakeFromNib ")

        //labelTitle.textColor = colorTxtGreen // 일단은 항상 푸르게 푸르게... ㅋㅋ
        // compression_simple" = "압박";        "respiration_simple" = "호흡";
        //setLanguageString()
        log.함수차원_출력_End()
    }
    
    func reloadTableView() {
        log.printThisFunc("reloadTableView")
        setLanguageString()
        tableResult.reloadData()
    }
    
    //////////////////////////////////////////////////////////////////////     _//////////_     [ UI      << >> ]    _//////////_   Table View
    // MARK: Table View
    func addTableCellNib() {
        let nib = UINib(nibName: "ResultTableCell", bundle: nil)
        tableResult.registerNib(nib, forCellReuseIdentifier: "rsltCell")
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return Int(HsBleSingle.inst().stageLimit)
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:ResultTableCell? = tableView.dequeueReusableCellWithIdentifier("rsltCell") as? ResultTableCell
        if cell == nil {
            addTableCellNib()
            cell = tableView.dequeueReusableCellWithIdentifier("rsltCell") as? ResultTableCell
        }
        cell?.setItems(indexPath.row + 1, dObj: (bleMan?.dataObj)!)
        return cell!
    }

    // MARK:  언어 세팅.
    func setLanguageString() {
        labelCycle.text = langStr.obj.cycle
        labelCompressionTitle.text = langStr.obj.compression
        labelBreathTitle.text = langStr.obj.breath // calStr("respiration_simple")
        labelCompCount.text = langStr.obj.count
        labelCompTime.text = langStr.obj.rate
        labelBrthCount.text = langStr.obj.count
        labelBrthTime.text = langStr.obj.time
    }

}
