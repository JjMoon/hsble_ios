//
//  HoldingTimeVw.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2015. 11. 25..
//  Copyright © 2015년 IMLab. All rights reserved.
//

import Foundation

// Heartisense Operation View
class HoldingTimeVw : UIView {
    var log = HtLog(cName: "HovHoldingTimeVw")
    
    var bleMan: HsBleSuMan?
    var parentView: OperateUnitView?
    
    @IBOutlet weak var labelSecond: UILabel!
    @IBOutlet weak var labelHoldTimeMsg: UILabel!
    
    @IBAction func bttnActClose(sender: UIButton) {
        parentView!.disconnectCallBack(bleMan!)
    }

    func update() {
        // log.printThisFunc()
        
        if bleMan == nil || bleMan?.isConnected == false { return }
        
        if bleMan!.isPausing {
            self.showMe()
            labelHoldTimeMsg.hideMe()
            labelSecond.text = langStr.obj.pause
            labelSecond.textColor = colBttnDarkGray
            return
        } else {
            labelHoldTimeMsg.showMe()
        }
        
        //let stage = bleMan?.stage
        //log.printThisFunc("update  \t ________________________________________________________ D/A : \(bleMan?.dataObj.depth) / \(bleMan?.dataObj.amount) stage: \(bleMan?.stage) count : \(bleMan?.dataObj.count)   currentStep : \(HsUtil.curStepStr())  ________________________ ", lnum: 0)
        // 압박 중단 시간 표시...
        let theTimer = bleMan?.calcManager.compHoldTimer
        let holdTime:Double = theTimer!.GetSecond((bleMan?.dataObj.count)!)

        labelSecond.text = "\(Int(holdTime))" //[NSString stringWithFormat: @"%d", (int)holdTime];
        if 10 < holdTime {
            labelHoldTimeMsg.textColor = colBttnRed
            labelSecond.textColor = colBttnRed
        } else {
            labelHoldTimeMsg.textColor = colBttnDarkGray
            labelSecond.textColor = colBttnDarkGray
        }
        
        switch (bleMan?.stt)! {
        case .S_WHOLESTEP_AED, .S_WHOLESTEP_DESCRIPTION, .S_WHOLESTEP_BREATH: // print("self.hidden = true;  \(#function)")
            self.hidden = true;
            return
        default:
            break
        }
        
        if (self.hidden) { // 안보이면..
            if ((bleMan?.stt)! == .S_WHOLESTEP_COMPRESSION) {
                if (bleMan?.dataObj.count == 0 && bleMan?.stage > 1) { // 스테이지 바뀌고 나서 켜는 경우..
                    self.hidden = false
                    setLanguageString()
                    log.logThis("  count \(bleMan?.dataObj.count) == 0 && stage \(bleMan?.stage) > 1  ", lnum: 5)
                    return
                } // 2스테이지 이상 처음 공백..
                if (holdTime > 2) {
                    if bleMan?.stage == 1 && bleMan?.dataObj.count == 0 { return }   // 처음의 예외..
                    log.logThis("  holdTime :: \(holdTime) > 2  && stage \(bleMan?.stage) > 1  ", lnum: 5)
                    self.hidden = false
                    setLanguageString()
                    return } // 켜고.
            }
        } else { // 보이는 상태에선 끄는 조건.
            if ((bleMan?.stt)! == .S_WHOLESTEP_COMPRESSION && bleMan?.stage == 1 && bleMan?.dataObj.count == 0) {
                self.hidden = true;  return
            }
            
            if ((bleMan?.stt)! != .S_WHOLESTEP_COMPRESSION || (  bleMan?.dataObj.count > 0 && holdTime < 0 )) { // 호흡에서 바뀐 경우는 무조건 켜주기.. 압박이 들어오는 경우.
                log.logThis("  @116    호흡에서 바뀐 경우는 무조건 켜주기.. 압박이 들어오는 경우.  ", lnum: 5)
                self.hidden = true;
            } // else ; // 그냥 놔두고..
        }
        log.함수차원_출력_End()
    }
    
    func checkResponseEmergency() { // A .. x  only..
        
    }
    
    // MARK:  언어 세팅.
    func setLanguageString() {
        labelHoldTimeMsg.text = langStr.obj.hands_off_time + " ("  + langStr.obj.second + ")"
    }

}

