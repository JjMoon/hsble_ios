//
//  OperationMainVC.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2015. 11. 16..
//  Copyright © 2015년 IMLab. All rights reserved.
//

import Foundation
import AVFoundation


class OperationMainVC : HtViewController, UIAlertViewDelegate {
    var log = HtLog(cName: "OperationMainVC")

    /// PrtcEnlargeView 관련..
    var targetView: UIView?
    var tarPoint: CGPoint?
    var baseOrigin: CGPoint? = nil
    var scal : CGFloat  = 2.2
    var enlargeClsr: ((Void) -> Void)?
    var bgView: UIView?
    var bttnClose: UIButton?

    var vwResultTitle: ResultTitleView!
    var arrAddedView = [OperateUnitView]()
    var wholeStepMan: HtGeneralStateManager?
    var audioPlayer = AVAudioPlayer(), audioCount: HtAudioPlayer? // compression_guide.mp3
    var compState: HtUnitState?, brthState: HtUnitState?

    var  openResultViewGraphCljr: (HsBleSuMan) -> () = { (bleObj: HsBleSuMan) -> Void in }
    var  openSendDataViewCljr: () -> () = { }

    @IBOutlet var allStepBtns: [UIButton]!

    @IBOutlet weak var bttn4Debug: UIButton!
    @IBOutlet weak var containerHealthCareProvider: UIView!

    @IBOutlet weak var bttnInfo: UIButton!
    // Lay Rescure
//    @IBOutlet weak var vwLeftUp: OperateUnitView!
//    @IBOutlet weak var vwRigtUp: OperateUnitView!
//    @IBOutlet weak var vwLeftLo: OperateUnitView!
//    @IBOutlet weak var vwRigtLo: OperateUnitView!

    @IBOutlet weak var bttnStepbystep: UIButton!
    @IBOutlet weak var bttnWholeprocess: UIButton!

    @IBOutlet weak var bttnHcResponse: UIButton!
    @IBOutlet weak var bttnHcEmergency: UIButton!
    @IBOutlet weak var bttnHcCheckPulse: UIButton!
    @IBOutlet weak var bttnHcCompression: UIButton!
    @IBOutlet weak var bttnHcBreath: UIButton!
    @IBOutlet weak var bttnHcAED: UIButton!

    @IBOutlet weak var bttnChkDanger: UIButton!
    @IBOutlet weak var bttnAirway: UIButton!
    @IBOutlet weak var bttnChkBreath: UIButton!

    @IBOutlet weak var viewEntireMsg: EntireStepMessage!
    @IBOutlet weak var viewSelectStepEntireMode: SelectEntireStep!
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var btnQuit: UIButton!


    //////////////////////////////////////////////////////////////////////       [ UI Sub  ]
    // MARK:  과정 변환.
    @IBAction func btnStepByStep(sender: UIButton) { // 단계별 학습.
        log.logUiAction("  Step by Step ")
        audioPlayer.stop()
        startStepMode()
    }

    func startStepMode() {
        let altVw = UIAlertView(title: langStr.obj.step_by_step_mode_short,
                                message: langStr.obj.want_start_stepbystep_mode, delegate: self,
                                cancelButtonTitle: langStr.obj.yes, otherButtonTitles: langStr.obj.no)
        altVw.tag = 10
        altVw.show()
    }
    
    @IBAction func btnWholeInOne(sender: UIButton) { // 전과정 학습.
        log.logUiAction("  Whole In One Process ")
        audioPlayer.stop()

        audioCount?.pauseTimer()

        startEntireMode()
    }

    func startEntireMode() {
        let altVw = UIAlertView(title: langStr.obj.whole_step_mode_short,
                                message: langStr.obj.want_start_wholestep_mode, delegate: self,
                                cancelButtonTitle: langStr.obj.yes, otherButtonTitles: langStr.obj.no)
        altVw.tag = 20
        altVw.show()
    }

    //////////////////////////////////////////////////////////////////////       [ Go to Main  ]
    // MARK:  메인으로 돌아가기.
    @IBAction func bttnActGoHome(sender: AnyObject) {
        audioPlayer.stop()
        let altVw = UIAlertView(title: langStr.obj.to_main, message: langStr.obj.want_main, delegate: self,
                                cancelButtonTitle: langStr.obj.yes, otherButtonTitles: langStr.obj.no)
        altVw.tag = 1
        altVw.show()
    }

    @IBAction func btnActQuit(sender: AnyObject) {
        audioPlayer.stop()
        showAlertPopupOfBackToMainInMonitor() {
            HsBleMaestr.inst.disconnectAllConnections()
        }
        //HsBleMaestr.inst.sendCommand(.S_QUIT)
    }

    //////////////////////////////////////////////////////////////////////       [ 단계별 버튼 세팅 ]
    var arrStepBttns = [UIButton]()

    func setStepButtonConstraints() {
        containerHealthCareProvider.showMe()
        setArrays()

        for btn in allStepBtns { // 버튼 정렬 및 초기화. 배경 이미지 흐리게.
            btn.hideMe()
            btn.setBackgroundImage(nil, forState: .Normal)
        } // 모든 버튼 끄고..
        for btn in arrStepBttns {
            btn.translatesAutoresizingMaskIntoConstraints = false
            btn.showMe()
        }

        let viewsDictionary = [ "resp": bttnHcResponse, "emer": bttnHcEmergency, "comp": bttnHcCompression,
                                "safe": bttnChkDanger, "airw": bttnAirway, "chkb": bttnChkBreath,
                                "brth": bttnHcBreath, "aedp": bttnHcAED, "puls": bttnHcCheckPulse ]

        var horConstraintString = ""
        var metricsDictionary = [ "spcH": 20 ] //  20   전체 폭은 130 * 7 >> 910..

        switch cprProtoN.theVal {
        case 0:
            if isRescureMode { // 버튼 크기 : 60
                metricsDictionary = [ "spcH": 40 ] // 70 ] //  (768 - 40 - 300) / 4 = 428 / 4 = 107
                horConstraintString = "H:|-20-[resp]-spcH-[emer]-spcH-[chkb]-spcH-[comp]-spcH-[brth]-spcH-[aedp]"
            } else {
                metricsDictionary = [ "spcH": 20 ] // 40 ] //  (768 - 40 - 360) / 5 = 368 / 5 = 74
                //horConstraintString = "H:|-20-[resp]-spcH-[emer]-spcH-[puls]-spcH-[chkb]-spcH-[comp]-spcH-[brth]-spcH-[aedp]"
                horConstraintString = "H:|-20-[resp]-spcH-[emer]-spcH-[chkb]-spcH-[puls]-spcH-[comp]-spcH-[brth]-spcH-[aedp]"
            }
        case 1:
            horConstraintString = "H:|-10-[safe]-spcH-[resp]-spcH-[airw]-spcH-[chkb]-spcH-[emer]-spcH-[comp]-spcH-[brth]-spcH-[aedp]"
        default:
            horConstraintString = "H:|-10-[safe]-spcH-[resp]-spcH-[emer]-spcH-[airw]-spcH-[chkb]-spcH-[comp]-spcH-[brth]-spcH-[aedp]"
        }

        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat(horConstraintString,
            options: NSLayoutFormatOptions.AlignAllCenterY,
            metrics: metricsDictionary, views: viewsDictionary))
    }

    func setArrays() {
        if cprProtoN.theVal == 0 {
            if isRescureMode {
                arrStepBttns = [ bttnHcResponse, bttnHcEmergency, bttnChkBreath,
                                 bttnHcCompression, bttnHcBreath , bttnHcAED ] // checkpulse 가 가운데..
            } else {
                arrStepBttns = [ bttnHcResponse, bttnHcEmergency, bttnChkBreath, bttnHcCheckPulse,
                                 bttnHcCompression, bttnHcBreath , bttnHcAED ]
            }
        }
        if 0 < cprProtoN.theVal {
            arrStepBttns = []
            for btn in allStepBtns { arrStepBttns.append(btn) }
            arrStepBttns.removeAtIndex(arrStepBttns.indexOf(bttnHcCheckPulse)!)
        }
    }

    //////////////////////////////////////////////////////////////////////       [  Entire Process  ]
    // MARK:  전과정 관련 함수들...
    @IBAction func btnActSkip(sender: AnyObject) {
        log.logUiAction("  전과정에서    SKIP   ")
        audioPlayer.stop()

        print("  스킵 버튼 >>>   \(wholeStepMan?.curState?.name)")

        if wholeStepMan?.curState?.name == "EntireOperations" {
            print("  스킵 버튼 >>>  리턴 \(wholeStepMan?.curState?.name)")
            return
        }

        print("  스킵 버튼 >>>  다음으로..   자식뷰들한테도 다음으로..  \(wholeStepMan?.curState?.name)")
        wholeStepMan?.goToNextStateWithSkipAction(true)

        for vw in self.getLiveKits() {
            vw.wholeStepMan?.goToNextStateWithSkipAction(true)
        }
    }

    @IBAction func bttnActHelp(sender: AnyObject) { // ? 버튼.
        openHeartisenseInfoWeb(1)// Monitor
    }

    @IBAction func bttnDebug(sender: AnyObject) {
        // HsBleMaestr.inst.sendCommand(.S_QUIT)
        enlargeView(arrAddedView.first!)
    }

    @IBAction func btnResponse(sender: AnyObject) {  //  Response 는 함수 분리..
        log.logUiAction("S_STEPBYSTEP_RESPONSE")
        setBackGroundGreen(sender)
        stepResponse()
    }

    func stepResponse() { // 화면 들어올 때 실행시키는 용도.
        stepByStepSetUiState("OtherState")
        if HsBleMaestr.inst.isAllInOne { return }
        else { HsBleMaestr.inst.sendCommand(.S_STEPBYSTEP_RESPONSE) }
        setUnitViewTotalSign() // 이거 한번 더 불러줌..  메시지 변경...
        playAudio("response")
    }

    @IBAction func btnActSafety(sender: AnyObject) {  //  Response 는 함수 분리..
        log.logUiAction("S_STEPBYSTEP_SAFETY")
        setBackGroundGreen(sender)
        stepSafety()
    }

    func stepSafety() {
        stepByStepSetUiState("OtherState")
        if HsBleMaestr.inst.isAllInOne { return }
        else { HsBleMaestr.inst.sendCommand(.S_STEPBYSTEP_SAFETY) }
        setUnitViewTotalSign() // 이거 한번 더 불러줌..  메시지 변경...
        playAudio("checkdanger")
    }
    
    @IBAction func btnEmergency(sender: AnyObject) {
        log.logUiAction("S_STEPBYSTEP_EMERGENCY")
        stepByStepSetUiState("OtherState")
        if HsBleMaestr.inst.isAllInOne { return }
        else { HsBleMaestr.inst.sendCommand(.S_STEPBYSTEP_EMERGENCY) }
        setBackGroundGreen(sender)
        playAudio("emergency")
    }

    @IBAction func btnAirway(sender: AnyObject) {
        log.logUiAction("AIRWAY")
        stepByStepSetUiState("OtherState")
        if HsBleMaestr.inst.isAllInOne { return }
        else { HsBleMaestr.inst.sendCommand(.S_STEPBYSTEP_AIRWAY ) }
        setBackGroundGreen(sender)
        playAudio("airway")
    }

    @IBAction func btnChkBreath(sender: AnyObject) {
        log.logUiAction("CheckBreath")
        stepByStepSetUiState("OtherState")
        if HsBleMaestr.inst.isAllInOne { return }
        else { HsBleMaestr.inst.sendCommand(.S_STEPBYSTEP_CHECK_BRTH ) }
        setBackGroundGreen(sender)
        playAudio("checkbreath")
    }

    @IBAction func btnCheckPulse(sender: AnyObject) {
        log.logUiAction("CHECKPULSE")
        stepByStepSetUiState("OtherState")
        if HsBleMaestr.inst.isAllInOne { return }
        else { HsBleMaestr.inst.sendCommand(.S_STEPBYSTEP_CHECKPULSE) }
        setBackGroundGreen(sender)
        playAudio("checkpulse")
    }
    
    @IBAction func btnCompression(sender: AnyObject) {
        log.logUiAction("COMPRESSION")
        stepByStepSetUiState("StepCompressionReady")
        if HsBleMaestr.inst.isAllInOne { return }
        else { HsBleMaestr.inst.sendCommand(.S_STEPBYSTEP_COMPRESSION) }
        setBackGroundGreen(sender)
        playAudio("compression")
        playAudioCount()
    }
    
    @IBAction func btnBreath(sender: AnyObject) {
        log.logUiAction("Breath")
        stepByStepSetUiState("StepBreathReady")
        if HsBleMaestr.inst.isAllInOne { return }
        else { HsBleMaestr.inst.sendCommand(.S_STEPBYSTEP_BREATH) }
        setBackGroundGreen(sender)
        playAudio("breath")
    }
    
    @IBAction func btnAED(sender: AnyObject) {
        stepByStepSetUiState("OtherState")
        log.logUiAction("Breath")
        if HsBleMaestr.inst.isAllInOne { return }
        else { HsBleMaestr.inst.sendCommand(.S_STEPBYSTEP_AED) }
        setBackGroundGreen(sender)
        playAudio("aed")
    }

    func addSubViews() throws {
        guard arrAddedView.count == 0 else {
            printException("OperationMainVC >> HsExptViewCtrl.SubviewNumber")
            throw HsExptViewCtrl.SubviewNumber
        }

        view.backgroundColor = HmGraphSetting.inst.viewBgBrightGray // viewBgGray
        openResultViewGraphCljr = { (bleObj: HsBleSuMan) -> Void in self.openResultViewGraph(bleObj) }

        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "OperateUnitView", bundle: bundle)

        let aToB = ["A", "B", "C", "D"]

        for (idx, rect) in rectArr.enumerate() {

            let vw = nib.instantiateWithOwner(self, options: nil).last as! OperateUnitView
            vw.frame = rect
            vw.bleMan = HsBleMaestr.inst.arrBleSuMan[idx]
            vw.bleMan?.nick = aToB[idx]

            self.view.addSubview(vw)
            arrAddedView.append(vw)
            vw.openResultGraphCljr = openResultViewGraphCljr
            vw.setInitialState()
            vw.sensorLostCallback = hardwareCallback
            vw.enlargeCljr = enlargeCallback

            vw.addEnlargeButton()

            /// X 버튼 누르면 연결 끊기.
            vw.disconnectCallBack = { (bleMan) in
                self.showGeneralPopup(langStr.obj.disconnect, message: langStr.obj.want_disconnect, yesStr: langStr.obj.yes, noString: langStr.obj.no, yesAction: { () -> Void in
                    bleMan.reset()
                })
            }
        }
        
        let anib = UINib(nibName: "SelectEntireStep", bundle: bundle)
        viewSelectStepEntireMode = anib.instantiateWithOwner(self, options: nil).last as! SelectEntireStep
        viewSelectStepEntireMode.frame = self.view.frame
        viewSelectStepEntireMode.motherView = self
        self.view.addSubview(viewSelectStepEntireMode)
        viewSelectStepEntireMode.setLanguageString()

        addResultTitleView()

    }

    override func viewDidLoad() {
        log.printThisFunc("viewDidLoad")

        if isDebugMode { bttn4Debug.showMe() } else { bttn4Debug.hideMe() }

        containerHealthCareProvider.hideMe()

        log.logThis(" >>>> view loading Finished ...  ")
    }

    override func viewWillAppear(animated: Bool) {

        log.logThis("  view timer activated ")

        setLanguageString()



        log.printThisFNC("viewWillAppear", comment: "  ")
    }

    override func viewDidAppear(animated: Bool) {
        if arrAddedView.count == 0 {
            try! addSubViews()
        }
        for vw in arrAddedView {
            vw.myviewWillAppear(viewSelectStepEntireMode.hidden)
        }
        timerInterval = 1.0 / 65.4
        super.viewDidAppear(animated)
        log.printThisFNC("viewDidAppear", comment: "  ")

        setStepButtonConstraints()

        //setBackGroundGreen(bttnHcResponse)
    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated) //  uiTimer.invalidate()

        log.printThisFNC("viewWillDisappear", comment: "  ")
        stopAudioCount()
        let liveKits = getLiveKits()
        for vw in liveKits {
            vw.myviewWillDisAppear()
        }
    }

    func hardwareCallback(vw: OperateUnitView, mssg: String) {
        log.printThisFNC("hardwareCallback", comment: mssg)
        showSimpleMessageWith(mssg) { () -> Void in
            vw.resetStateBySensorError()
        }
    }

    // MARK:  Alert View Delegate ...
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if buttonIndex != 0 {
            audioCount?.unpuaseTimer()
            return
        }
        switch alertView.tag {
        case 1:
            log.logUiAction("sendCommand(.S_READY)", lnum: 5, printOn: true)
            HsBleMaestr.inst.setParsingState(.StandBy)
            HsBleMaestr.inst.sendCommand(.S_READY)
            self.navigationController?.popToViewController((self.navigationController?.viewControllers[1])!, animated: viewAnimate)
        case 10:
            log.logUiAction("  alertView delegate 단계별 시작.")
            stepByStepStart()
        case 20:
            log.logUiAction("  alertView delegate 전과정 시작.")
            entireProcessStart()
        default:
            print("ERROR in Alert View Delegate ....  tag : 1, 10, 20 ")
        }
    }

    func stopAudioCount() {
        if audioCount != nil {
            audioCount?.stop()
            audioCount = nil;
        }
    }

    func playAudioCount() {
        let soundUrl = NSURL(fileURLWithPath: NSString(format: "%@/AudioFiles/compression_once.mp3", NSBundle.mainBundle().resourcePath!) as String)
        if audioCount == nil {
            audioCount = HtAudioPlayer(repeatLimit: -1, url: soundUrl, initMuteOn: false)
        }
        audioCount!.initTimer(Double(audioCountInterval.theVal))
        audioCount!.play()
    }

    func playAudio(fileHeader: String) {
        var lanStr = "_en.mp3"
        switch (langIdx) {
        case 0: lanStr = "_en.mp3"
            if cprProtoN.theVal == 2 { lanStr = "_anz.mp3" }
        case 1: lanStr = "_de.mp3"
        case 2: lanStr = "_es.mp3"
        case 3: lanStr = "_fr.mp3"
        case 4: lanStr = "_kr.mp3"
        default:
            lanStr = "_en.mp3"
        }
        //print(fileHeader + lanStr)
        let soundUrl = NSURL(fileURLWithPath: NSString(format: "%@/AudioFiles/" + fileHeader + lanStr,
            NSBundle.mainBundle().resourcePath!) as String)
        do {
            audioPlayer = try AVAudioPlayer(contentsOfURL: soundUrl)
            audioPlayer.prepareToPlay()
            audioPlayer.play()
        } catch {  print("\n\n\n\n\n Catched >> Something bad happened. Try catching specific errors to narrow things down  \n\n\n\n\n") }
    }
    
    func setBackGroundGreen(sender: AnyObject?) {
        for btn in allStepBtns {
            //btn.setBackgroundImage(UIImage(named: "btn_sbs_circle_gray"), forState: UIControlState.Normal)
            btn.setBackgroundImage(nil, forState: .Normal)
        }
        if sender == nil { return }
        let bttn = sender as! UIButton
        bttn.setBackgroundImage(UIImage(named: "btn_sbs_circle_green"), forState: UIControlState.Normal)
        setUnitViewTotalSign()
    }

    func setUnitViewTotalSign() {
        let curStr = HsUtil.curStepLocalized()
        for vw in arrAddedView {
            vw.labelTotalSign.text = curStr

        }
    }
    
    func getLiveKits() -> [OperateUnitView] { // 현결된 키트 뷰 리턴..
        var rArr = [OperateUnitView]()
        for vw in arrAddedView {
            if vw.bleMan?.isConnected == true {
                rArr.append(vw)
            } else {
                vw.hideMe()
            }
        }
        return rArr
    }

    func stepByStepSetUiState(sName: String) {
        for vw in getLiveKits() {
            print("  단계별 각 뷰 시작..." + sName)
            vw.stepByStepSetState(sName)
        }
        if sName == "OtherState" || sName == "StepBreathReady" { stopAudioCount() }
    }

    // MARK:  전체 뷰 띄우는 콜백.
    func openResultViewGraph(bleObj: HsBleSuMan) { // 0 ~ 4
        let vc = ResultGraphVC(nibName: "ResultGraph", bundle: nil)
        vc.bleMan = bleObj; vc.dbStudent = nil
        presentViewController(vc, animated: viewAnimate, completion: nil)
        //navigationController?.pushViewController(vc, animated: viewAnimate)
    }

    // MARK:  데이터 저장 팝업 띄우기
    func addResultTitleView() {
        //let frm = vwResultTitle.frame
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "ResultTitleView", bundle: bundle)
        vwResultTitle = nib.instantiateWithOwner(self, options: nil).last as! ResultTitleView
        vwResultTitle.frame = CGRect(x: 0, y: 100, width: 768, height: 80) // frm
        vwResultTitle.backgroundColor = HmGraphSetting.inst.viewBgGray
        view.addSubview(vwResultTitle)
        vwResultTitle.initSet()
        vwResultTitle.openSendDataViewCljr = { () -> () in  self.openSendDataView() }
        vwResultTitle.hidden = true
    }

    func openSendDataView() {
        let vc = DataSavePopoverVC(nibName: "DataSavePopover", bundle: nil)
        vc.modalPresentationStyle = .FormSheet // .Popover
        presentViewController(vc, animated: viewAnimate, completion: nil)
        vc.popoverPresentationController?.sourceView = view
        vc.popoverPresentationController?.sourceRect = view.frame
        vc.setLanguageString()
    }

    /// 버튼 activate 시키고 안 시키고 ...
    private func setButtonActivation() {
        let arrBttn = [ bttnHcResponse,  bttnHcEmergency,  bttnHcCompression, bttnChkDanger, bttnAirway,
                        bttnChkBreath, bttnHcBreath, bttnHcAED, bttnHcCheckPulse ]
        for btn in arrBttn {
            btn.enabled = !HsBleMaestr.inst.isAllInOne
        }
    }
    
    func setClassButtonBackground() {
        setButtonActivation()
        if HsBleMaestr.inst.isAllInOne {

            print("   stepbystep ... .>>>>   NONONO ")


            setBackGroundGreen(nil)
            // containerHealthCareProvider.hideMe()  보여주는 것으로 수정함.. 160525
            bttnWholeprocess.backgroundColor = colBttnGreen
            bttnStepbystep.backgroundColor = colBttnGray
        } else {

            print("   stepbystep ... .>>>>  ")


            containerHealthCareProvider.showMe()
            bttnWholeprocess.backgroundColor = colBttnGray
            bttnStepbystep.backgroundColor = colBttnGreen
        }
        btnSkip.hidden = (HsBleMaestr.inst.parseState == .Step)
    }

    override func update() {
        wholeStepMan?.update()

        if HsBleMaestr.inst.connectionNumber() == 0 { // 앞 화면으로 돌아가기.
            uiTimer.invalidate()
            let altVw = UIAlertView(title: langStr.obj.disconnected, message: nil, delegate: self,
                cancelButtonTitle: nil)
            altVw.show()
            HsGlobal.delay(2.0, closure: { () -> () in
                altVw.dismissWithClickedButtonIndex(0, animated: viewAnimate)
                self.navigationController?.popViewControllerAnimated(viewAnimate)
            })
        }
    }

    // MARK:  언어 세팅.
    func setLanguageString() {
        view.backgroundColor = HmGraphSetting.inst.viewBgBrightGray
        bttnStepbystep.localizeSizeAlignment(2, topSpace: 5, btmSpace: 3)
        bttnWholeprocess.localizeSizeAlignment(2, topSpace: 5, btmSpace: 3)
        bttnStepbystep.cornerRad(5); bttnWholeprocess.cornerRad(5)

        bttnStepbystep.setTitle(langStr.obj.stepbystep2line, forState: UIControlState.Normal)
        bttnWholeprocess.setTitle(langStr.obj.wholestep_2line, forState: UIControlState.Normal)
        btnSkip.setTitle(langStr.obj.skipArrow, forState: UIControlState.Normal)
        btnQuit.setTitle(langStr.obj.quitArrow, forState: UIControlState.Normal) //  quitArrow  prev_gt
    }
}
