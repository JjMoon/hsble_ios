//
//  AppDelegate.h
//  Monitor
//
//  Created by Jongwoo Moon on 2015. 11. 10..
//  Copyright © 2015년 IMLab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

