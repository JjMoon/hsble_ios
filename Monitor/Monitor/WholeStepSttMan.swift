//
//  WholeStepSttMan.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2015. 11. 26..
//  Copyright © 2015년 IMLab. All rights reserved.
//

import Foundation
import AVFoundation

class WholeStepSttMan : HtGeneralStateManager {
    
    var compState: HtUnitState?, brthState: HtUnitState?
    
    var audioPlayer = AVAudioPlayer()
    
    func setxState() {

        var aState = HtUnitState(duTime: 12, myName: "Description")
        aState.entryAction = { ()->() in
            HsBleMaestr.inst.sendCommand(.S_WHOLESTEP_PRECPR)
            // 오디오 틀고..
        }
        aState.durinAction = {
        };          addAState(aState) // Add new State
        
        compState = HtUnitState(duTime: -1, myName: "Compression")
        compState?.entryAction = {
            // 화면 전환
            HsBleMaestr.inst.sendCommand(.S_WHOLESTEP_PRECPR)
        };        addAState(compState!)
        brthState = HtUnitState(duTime: -1, myName: "Breath")
        brthState?.entryAction = {
            
        };          addAState(brthState!)
        
        aState = HtUnitState(duTime: 10, myName: "Result")
        aState.entryAction = {
            
        };          addAState(aState)
    }
    
}