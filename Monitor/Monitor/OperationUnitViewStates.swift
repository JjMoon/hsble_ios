//
//  OperationUnitViewStates.swift
//  HS Monitor
//
//  Created by Jongwoo Moon on 2016. 7. 29..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation

extension OperateUnitView {

    func addEnlargeButton() {
        bttnClose = UIButton()
        //bttnClose!.frame.origin = labelTotalSign.frame.origin
        bttnClose!.frame = CGRect(x: 0, y: (340 - 250) / 2, width: 300, height: 230)
            //CGSize(width: labelTotalSign.frame.width, height: labelTotalSign.frame.height - 30)

        print("  확대 뷰 버튼 : \(bttnClose?.frame)")

        // bttnClose!.titleLabel?.text = "Close this.."
        bttnClose!.addTarget(self, action: #selector(self.shrinkAction), forControlEvents: .TouchUpInside)
        self.addSubview(bttnClose!)
        bringSubviewToFront(bttnClose!)
    }

    func shrinkAction(sender: UIButton!) {
        print("shrinkAction  ...  in   operation unit view ")
        enlarged = !enlarged
        //btnOffX.hidden = enlarged
        enlargeCljr(self)
    }

    private func entireCompBreathChange() {
        if bleMan?.stt != prevState {
            if self.bleMan?.stt == .S_WHOLESTEP_COMPRESSION {
                setCompBreath(false)
            } else {
                setCompBreath(true)
            }
            prevState = bleMan!.stt
        }
    }

    //////////////////////////////////////////////////////////////////////     [ 전과정  상태 머신   << >> ]
    func startEntireProcess() { // OperationMainVC 에서 불림.
        bleMan?.startWholeInOneProcess()
        labelTotalSign.showMe()
        imgHuman.hidden = true; vwCompArrows.hidden = true // 사람, 화살표
        labelCycle!.hidden = true
        wholeStepMan = HtGeneralStateManager(myName: "UnitVw[전과정 머신]")
        stepMan = nil
        bleMan!.calcManager.speedLogic = bleMan!.spdLgc

        setCompBreath(false)

        var aState = HtUnitState(duTime: 22, myName: "Description")
        aState.entryAction = { ()->() in
            self.resetPause()
            hideEvery(self.bttnNext, self.viewCompSign, self.viewResultTable, self.labelTime)
            self.bleMan?.dataSaved = false
            self.btnOffX.setImage(UIImage(named: "btn_off"), forState: UIControlState.Normal)
            self.otherStateViewSet()
            self.labelTotalSign.text = langStr.obj.scenario_description
        }
        aState.durinAction = {
            if self.sensorErrorSkip { // 센서가 빠져서 오면 여기서 스킵한다...
                self.wholeStepMan?.goToNextStateWithSkipAction(true)
                self.bleMan?.stt = .S_WHOLESTEP_PRECPR
                self.sensorErrorSkip = false
            }
        }
        wholeStepMan?.addAState(aState)

        aState = HtUnitState(duTime: -1, myName: "ResponseEmergency") // precpr...
        aState.entryAction = {
            //precpr_aha_bls = "Response\nEmergency\nCheck Pulse"; precpr_aha_lay = "Response\nEmergency";
            switch cprProtoN.theVal {
            case 1:
                self.labelTotalSign.text = langStr.obj.precpr_erc
            case 2:
                self.labelTotalSign.text = langStr.obj.precpr_anzcor
            default:
                if isRescureMode { self.labelTotalSign.text = langStr.obj.precpr_aha_lay }
                else { self.labelTotalSign.text = langStr.obj.precpr_aha_bls }
            }
        }
        aState.exitCondition = { ()->(Bool) in
            //print("  압박으로 넘어가는 조건...  dataObj.depth >  \(self.bleMan?.dataObj.depth)   count : \(self.bleMan?.dataObj.count)    ")
            //print("  \(HsUtil.curStepStr((self.bleMan?.bleState)!) )   \(HsUtil.curStepStr(globalState))   ")
            return self.bleMan?.dataObj.depth > 20 || self.bleMan?.dataObj.count > 0 ||
                (self.bleMan!.messageName != nil && self.bleMan!.messageName == "message_wrongposition")
        }
        wholeStepMan?.addAState(aState)

        aState = HtUnitState(duTime: -1, myName: "EntireOperations")
        aState.entryAction = {
            print("OperateUnitView :: EntireOperations entryAction 시작 [ \(self.labelTitle.text) ]")
            showEvery(self.bttnPause, self.viewCompSign, self.progressBar, self.viewCompSign, self.labelCycle)
            self.labelTotalSign.hideMe()
            self.bleMan?.startCompressionProcess() // 압박 시작. self.log.logUiAction(" EntireOperations ");
            self.bleMan?.logCurrentState()
            self.labelCycle.text = "\((self.bleMan?.stage)!) / \(HsBleSingle.inst().stageLimit)" // 2 / 5 스테이지 표시.
            
            if self.bleMan?.stt == .S_WHOLESTEP_COMPRESSION {
                self.viewCompSign.prepareCompression(true)
            } else {
                self.viewCompSign.prepareCompression(false)
            }
        }
        aState.durinAction = {
            self.entireCompBreathChange()
            if self.bleMan?.stt == .S_WHOLESTEP_COMPRESSION {
                self.viewCompSign.prepareCompression(true)
                self.compression()
            }
            if self.bleMan?.stt == .S_WHOLESTEP_BREATH {
                self.viewCompSign.prepareCompression(false)
                self.breath()
            }
        }
        aState.exitCondition = { ()->(Bool) in return self.bleMan?.stt == .S_WHOLESTEP_AED }
        aState.exittAction = { void -> () in
            self.breath()
            self.viewCompSign.hideMe()
        }
        wholeStepMan?.addAState(aState)

        aState = HtUnitState(duTime: 10, myName: "AED") // 10초 후에 전환..
        aState.entryAction = {
            self.log.logUiAction(" A E D "); self.bleMan?.logCurrentState()

            //  self.enableGraphViewButton() // 160801

            //self.otherStatesDuringAction()
            self.otherStateViewSet()    //self.labelCompResp.hideMe(); self.labelCycle.hideMe()
            self.labelTotalSign.text = langStr.obj.aed
            hideEvery(self.bttnPause)
            showEvery(self.bttnNext)
        }
        wholeStepMan?.addAState(aState)

        aState = HtUnitState(duTime: -1, myName: "Result")
        aState.entryAction = {
            self.log.logUiAction(" Result "); self.bleMan?.logCurrentState()
            self.bleMan?.stt = .S_WHOLESTEP_RESULT // 할당 함...
            showEvery(self.viewDiscrim!, self.bttnViewDetail)
            hideEvery(self.labelTotalSign, self.bttnNext)

            if self.bleMan?.curStudent != nil { self.bleMan!.curStudent!.evalFinished = true }

            /// 점수 뷰 ...
            self.bttnViewDetail.titleLabel?.textColor = self.viewDiscrim!.setDiscreminant()

            print("  self.viewResultTable.hidden = false  결과테이블 보이기 ")
            
            self.bleMan?.operationStop()

        }
        wholeStepMan?.addAState(aState)
        wholeStepMan?.prepareActions()
        wholeStepMan?.update()
    }

    //////////////////////////////////////////////////////////////////////     [ UI Sub Method   << >> ]

    func otherStateViewSet() {
        addDiscriminantView()
        labelTotalSign.showMe()
        imgHuman.hideMe()
        vwCompArrows.hideMe()
        viewDiscrim!.hideMe() // labelDiscriminant.hideMe();
        labelCycle.hideMe(); bttnViewDetail.hideMe(); labelCount.hideMe();



        viewResp.hideMe(); viewComp.hideMe(); progressBar.hideMe()
        strkVw.hideMe()


        operationViewShow(false)
        //for lb in labelMessages { lb.hideMe() }
    }


    func stepByStepCommonAction() {
        labelCompResp.hideMe(); labelTime.hideMe() // 보통은 가린다.

        viewDiscrim!.hideMe() //  labelDiscriminant.hidden = true;
        bttnViewDetail.hidden = true // 판별식, 디테일 버튼.
        viewResultTable.hidden = true // 결과 테이블.
        self.bttnNext.hidden = true // 전과정 AED 의 -> 버튼
        imgHuman.hidden = true; vwCompArrows.hidden = true  // 사람, 화살표 등 압박, 호흡 관련....
        viewHoldingTime.hideMe() // 압박 지연 시간
        labelCount.hidden = true
        //viewComp!.hidden = true; viewResp!.hidden = true

        strkVw.hideMe()
    }

    func operationViewShow(show: Bool) {
        labelCompResp.show_다음이_참이면(show)
        labelTime.show_다음이_참이면(show)
    }

    func compression() { // 단계별, 전과정 공통..
        labelCompResp!.text = HsUtil.curStepLocalized((bleMan?.stt)!)
        operationViewShow(true) // labelCompResp.showMe(); labelTime.showMe()
        viewHoldingTime.update()
        setArrowImages()

        labelCount.showMe()
        imgHuman.showMe(); vwCompArrows.showMe() // 사람, 화살표
        labelCycle.text = "\((bleMan?.stage)!) / \(HsBleSingle.inst().stageLimit)" // 2 / 5 스테이지 표시.
        //viewComp!.hidden = false; viewResp!.hidden = true  // 스트로크 뷰 관련 삭제

        // progressBar!.setProgress(Float((bleMan?.progressValue4Display())!), animated: false)

        viewComp.hideMe(); viewResp.hideMe(); progressBar.hideMe()
        strkVw.showMe()
        strkVw.setBarD(bleMan!.progressValue4Display())



        labelCount.text = bleMan?.uiCountInfo()
    }

    func breath() { // 단계별, 전과정 공통..
        labelCompResp!.text = HsUtil.curStepLocalized((bleMan?.stt)!)
        operationViewShow(true) // labelCompResp.showMe(); labelTime.showMe()
        //viewHoldingTime.hidden = true // 압박 지연 시간
        imgHuman.hideMe(); vwCompArrows.hideMe() // 사람, 화살표

        labelCount.showMe()

        //viewComp!.hidden = true; viewResp!.hidden = false // 스트로크 뷰 관련 삭제
        viewComp.hideMe(); viewResp.hideMe(); progressBar.hideMe()
        strkVw.showMe()

        strkVw.setBarD(bleMan!.progressValue4Display())

        //progressBar!.setProgress(Float((bleMan?.progressValue4Display())!), animated: false)
        labelCount.text = bleMan?.uiCountInfo()
    }


    //////////////////////////////////////////////////////////////////////     [ 단계별  상태 머신   << >> ]

    func makeStepByStepStateMachine() {
        stepMan = HtGeneralStateManager(myName: "단계별:\((bleMan?.name)!)")
        wholeStepMan = nil
        btnOffX.setImage(UIImage(named: "btn_off"), forState: UIControlState.Normal)
        
        var aState = HtUnitState(duTime: -1, myName: "StepCompressionReady") // 압박
        aState.entryAction = { ()->() in
            print("  단계별 : StepCompressionReady  ")
            self.resetPause()
            self.viewCompSign.showMe()
            self.viewCompSign.prepareCompression(true)
            self.labelTotalSign.hideMe();
            self.progressBar.showMe()

            self.speedLogic!.deactivateAll() // 상자 초기화

            self.labelCompResp.showMe(); self.labelTime.hideMe()
            self.imgHuman.showMe(); self.vwCompArrows.showMe() // 사람, 화살표
            self.viewHoldingTime.hideMe();
            self.labelCycle.hideMe()
            self.stepWatch.resetTime(); self.stepWatch.forcePause() // 이 상태만 이 타이머 사용.
            self.bleMan?.dataObj.operationTimer.resetTime()
            self.bleMan?.dataObj.operationTimer.forcePause()
            self.setCompBreath(false) // 스트로크 뷰
            self.compression()
        }
        aState.durinAction = {
            //print("StepCompressionReady  >> aState.durinAction  value   \((self.bleMan?.progressValue4Display())!) ")
            if 0.1 < (self.bleMan?.progressValue4Display())! ||
                (self.bleMan!.messageName != nil && self.bleMan!.messageName == "message_wrongposition") {
                print("    if 0.1 < (self.bleMan   압박으로 간다.  ")
                self.stepMan!.setState("StepCompression")
            }
        }
        stepMan!.addAState(aState)

        aState = HtUnitState(duTime: -1, myName: "StepCompression")
        aState.entryAction = { ()->() in
            print("  단계별 : StepCompression  \(self.bleMan?.dataObj.operationTimer)")
            showEvery(self.bttnPause, self.labelTime, self.labelCount)
            self.pausing = false

            self.stepWatch.togglePause()
            (self.bleMan?.dataObj.operationTimer)!.togglePause()
            //self.labelCount.text = self.bleMan?.uiCountInfo()
        }
        aState.durinAction = {
            //print("  단계별 : StepCompression  during action  \(self.bleMan?.dataObj.operationTimer)")
            self.compression()
            //self.stepCompWatchState()            self.stepOtherStateWatch()
        }
        stepMan!.addAState(aState)

        aState = HtUnitState(duTime: -1, myName: "StepBreathReady") // 호흡
        aState.entryAction = { ()->() in
            self.resetPause()
            showEvery(self.viewCompSign, self.progressBar)
            
            self.viewCompSign.prepareCompression(false)
            self.labelTotalSign.hideMe();
            self.speedLogic!.deactivateAll() // 상자 초기화

            self.labelCompResp.showMe(); self.labelTime.hideMe()
            self.labelCycle.hideMe()
            self.imgHuman.hideMe(); self.vwCompArrows.hideMe() // 사람, 화살표
            self.bleMan?.dataObj.operationTimer.resetTime()
            self.bleMan?.dataObj.operationTimer.forcePause()
            self.labelCount.hidden = false
            self.labelCount.text = self.bleMan?.uiCountInfo()

            self.setCompBreath(true) // 스트로크 뷰
            self.breath()
        }
        aState.durinAction = {
            //self.stepBreathWatchState()            self.stepOtherStateWatch()
            if 0.1 < (self.bleMan?.progressValue4Display())! {
                self.stepMan!.goToNextStateWithSkipAction(false)
            }
        }
        stepMan!.addAState(aState)

        aState = HtUnitState(duTime: -1, myName: "StepBreath")
        aState.entryAction = { ()->() in
            self.resetPause()
            showEvery(self.bttnPause, self.labelTime)
            self.bleMan?.dataObj.operationTimer.togglePause()
        }
        aState.durinAction = {
            self.breath()
            //self.stepBreathWatchState()            self.stepOtherStateWatch()
        }
        stepMan!.addAState(aState)

        aState = HtUnitState(duTime: -1, myName: "OtherState")
        aState.entryAction = { ()->() in
            self.resetPause()
            self.viewCompSign.hideMe()
            self.otherStateViewSet()
        }
        aState.durinAction = {
            self.otherStatesDuringAction()
            //self.stepCompWatchState()         self.stepBreathWatchState()
        }
        stepMan!.addAState(aState)
        
        stepMan!.setState("OtherState")
        stepMan!.update()
    }

}
