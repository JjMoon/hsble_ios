//
//  OperateUnitView.swift
//  Monitor
//
//  Created by Jongwoo Moon on 2015. 11. 16..
//  Copyright © 2015년 IMLab. All rights reserved.
//

import Foundation


class OperateUnitView : HtView {
    var log = HtLog(cName: "OperateUnitView")
    var uiTimer = NSTimer(), bleMan:HsBleSuMan?, cnt = 0
    var wholeStepMan: HtGeneralStateManager?
    var stepMan: HtGeneralStateManager?
    var openResultGraphCljr: (HsBleSuMan) -> () = { (bleObj: HsBleSuMan) -> Void in }
    var sensorLostCallback: (OperateUnitView, String) -> () =
    { (unitV: OperateUnitView, String) -> Void in }
    var disconnectCallBack: (HsBleSuMan) -> Void = { (bleObj) in  }
    var prevStepState = "OtherState", prevState: CurStep = .S_WHOLESTEP_COMPRESSION
    var strkVw: HsStrokeVw!

    var stepWatch = HtStopWatch()
    var arrArrow: [UIImageView]?

    /// 화면 확대 관련..
    var enlarged = false
    var bttnClose: UIButton?, enlargeCljr: (OperateUnitView) -> () =  { (vw: OperateUnitView) -> Void in }

    var speedLogic : HsSpeedLgcMon?
    var viewDiscrim: DiscriminantView?
    var viewCompSign: OperateCompSign!

    @IBOutlet weak var viewResultTable: ResultTableView!
    @IBOutlet weak var viewHoldingTime: HoldingTimeVw!
    @IBOutlet weak var vwCompArrows: UIView!
    @IBOutlet weak var labelTime: UILabel!
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelState: UILabel!  // @IBOutlet weak var labelName: UILabel!
    
    @IBOutlet weak var btnOffX: UIButton!
    @IBOutlet weak var labelCompResp: UILabel!
    @IBOutlet weak var labelCycle: UILabel!
    @IBOutlet weak var labelCount: UILabel!
    @IBOutlet weak var viewComp: UIView!  // 압박, 호흡 그래프.
    @IBOutlet weak var viewResp: UIView!
    @IBOutlet weak var progressBar: UIProgressView! // 그래프.

    @IBOutlet weak var imgHuman: UIImageView!

    @IBOutlet weak var bttnViewDetail: UIButton!
    @IBOutlet weak var bttnNext: UIButton!
    
    @IBOutlet weak var imgArrowUp: UIImageView!
    @IBOutlet weak var imgArrowDn: UIImageView!
    @IBOutlet weak var imgArrowMid: UIImageView!
    @IBOutlet weak var imgArrowOutRight: UIImageView!

    @IBOutlet weak var labelTotalSign: UILabel!
    @IBOutlet weak var compBarGreen: UILabel!
    @IBOutlet weak var compBarRed: UILabel!

    let arrMessageName = [ "message_good", "message_weak", "message_strong", "message_wrongposition", "message_notrelease" ]

    @IBAction func bttnActViewDetail(sender: AnyObject) {
        addResultTableView()
        self.viewResultTable.hidden = false
    }
    
    @IBAction func bttnActNext(sender: AnyObject) {
        wholeStepMan?.goToNextStateWithSkipAction(true)
    }
    
    @IBAction func bttnActOffOrDetail(sender: UIButton) { // connect of graph..
        if sender.currentImage == UIImage(named: "btn_detail") {
        //if HsBleMaestr.inst.isAllInOne && bleMan?.stepEntire == .S_WHOLESTEP_RESULT {
            log.logUiAction("btnActShowResultGraph(sender: AnyObject) ")
            openResultGraphCljr(bleMan!)
        } else {
            disconnectCallBack(bleMan!) // bleMan?.reset()
        }
    }
    
    //////////////////////////////////////////////////////////////////////     [ UI  일시 중지 >> ]
    @IBOutlet weak var bttnPause: UIButton!
    
    var pausing = false
    
    @IBAction func bttnActPause(sender: UIButton) {
        print(" pause xcxc...")
        if pausing {
            bleMan?.sendPauseCommand(false) // unPause
            bleMan?.dataObj.operationTimer.startOrReleaseToggle()
            stepWatch.startOrReleaseToggle()
            bleMan?.calcManager.compHoldTimer.startOrReleaseToggle()
            bttnPause.setImage(UIImage(named: "btn_pause"), forState: .Normal)
            bleMan?.operationStart()
        } else {
            //viewHoldingTime.showMe()
            bleMan?.sendPauseCommand(true) // Pause
            bleMan?.dataObj.operationTimer.forcePause()
            stepWatch.forcePause()
            bleMan?.calcManager.compHoldTimer.forcePause()
            bttnPause.setImage(UIImage(named: "btn_unpause"), forState: .Normal)
            bleMan?.operationStop()
        }
        pausing = !pausing
        bleMan?.isPausing = pausing
    }
    
    func resetPause() {
        pausing = false
        bleMan?.isPausing = false
        bttnPause.hideMe()
        // bleMan?.sendPauseCommand(false) // unPause
        bttnPause.setImage(UIImage(named: "btn_pause"), forState: .Normal)
    }

    //////////////////////////////////////////////////////////////////////     [ UI   ViewDidLoad   << >> ]
    func myviewWillAppear(viewSelectHidden: Bool) {
        log.printAlways("  UnitView myviewWillAppear  timer 세팅. >>>>  \(bleMan?.name)  ")

        arrArrow = [ imgArrowUp, imgArrowMid, imgArrowDn, imgArrowOutRight ]

        labelState.text = bleMan!.curStudent?.name

        // set standard.
        print("   set standard.  unitView >>  \(cprProtoN.theVal)")
        bleMan!.calcManager.data.standard = cprProtoN.theVal

        uiTimer = NSTimer.scheduledTimerWithTimeInterval(0.03, // second
            target:self, selector: #selector(OperateUnitView.update), userInfo: nil, repeats: true)
        labelCycle.alignCenter()
        stepMan = nil; wholeStepMan = nil
        if viewSelectHidden { labelTotalSign.text = langStr.obj.response }
        else { labelTotalSign.text = bleMan?.prevKitName }

        bttnViewDetail.setTitle("\(langStr.obj.view_detail) >>", forState: .Normal)

        var greenWid = CC_STRONG_POINT - CC_WEAK_POINT, redWid = 100 - CC_STRONG_POINT
        if 100 < CC_STRONG_POINT { greenWid -= 1; redWid = 0 }
        let green = CGFloat(greenWid) / 100, red = CGFloat(redWid) / 100

        let grnConst = NSLayoutConstraint(item: compBarGreen, attribute: .Width, relatedBy: .Equal, toItem: viewComp,
                                      attribute: .Width, multiplier: green, constant: 0)
        viewComp.addConstraint(grnConst)
        let redConst = NSLayoutConstraint(item: compBarRed, attribute: .Width, relatedBy: .Equal, toItem: viewComp,
                                          attribute: .Width, multiplier: red, constant: 0)
        viewComp.addConstraint(redConst)
        viewComp.setNeedsLayout()

        log.함수차원_출력_End()
    }

    func setInitStrokeView() {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "HsStrokeVw", bundle: bundle)
        strkVw = nib.instantiateWithOwner(self, options: nil).last as! HsStrokeVw
        strkVw.frame = CGRect(x: 15, y: 100, width: 270, height: 20)
        strkVw.setConstraint(0.2, gRto: 0.8)
        self.addSubview(strkVw)
        strkVw.setBarD(0)
    }

    func setCompBreath(isBreath: Bool) {
        if isBreath {
            strkVw.setConstraint(0.35, gRto: 0.35)
            return
        }
        var green = CGFloat(CC_STRONG_POINT - CC_WEAK_POINT) / 100
        if 100 < CC_STRONG_POINT { green = CGFloat(100 - CC_WEAK_POINT) / 100 }
        strkVw.setConstraint(CGFloat(CC_WEAK_POINT) / 100, gRto: green)
    }

    func addDiscriminantView() {
        if viewDiscrim == nil {
            let bundle = NSBundle(forClass: self.dynamicType)
            let nib = UINib(nibName: "DiscriminantView", bundle: bundle)

            viewDiscrim = nib.instantiateWithOwner(self, options: nil).last as? DiscriminantView

            if UI_USER_INTERFACE_IDIOM() == .Pad {
                viewDiscrim?.frame = CGRect(x: 0, y: 50, width: 300, height: 205)
            } else {
                viewDiscrim?.frame = CGRect(x: 0, y: 25, width: 180, height: 105)
            }
            addSubview(viewDiscrim!)

            viewDiscrim!.dataObj = bleMan?.dataObj
        }
    }

    func myviewWillDisAppear() {
        log.printAlways("  UnitView myviewWill DisAppear  Invalidate >>>>  \(bleMan?.name)  ")
        uiTimer.invalidate()
        stepMan = HtGeneralStateManager()
        wholeStepMan = nil
    }

    func setInitialState() { // 선택 페이지에서 불림..
        labelTitle.text = bleMan?.nick
        labelTotalSign.text = bleMan?.prevKitName

        if speedLogic == nil { // 한번만 로딩해야지...
            setInitStrokeView()
            otherStateViewSet()

            speedLogic = bleMan!.calcManager.speedLogic as? HsSpeedLgcMon
            let bundle = NSBundle(forClass: self.dynamicType)
            let nib = UINib(nibName: "OperateCompSign", bundle: bundle)
            //let frm = viewCompSign.frame
            viewCompSign = nib.instantiateWithOwner(self, options: nil).last as! OperateCompSign
            viewCompSign.frame = CGRect(x: 5, y: 340 - 215, width: 280, height: 215) // frm
            addSubview(viewCompSign)
            viewCompSign.setUiObjectsToSpeedLogic(speedLogic!)
            viewCompSign.spdLgc!.ble = bleMan!
            viewCompSign.setLanguageString()
            viewCompSign.hideMe()

            // depthCol = colorBtnBgGray, speedCol = colorBtnBgGray, recoilCol = colorBtnBgGray
            speedLogic?.depthCol = UIColor.whiteColor()
            speedLogic?.speedCol = UIColor.whiteColor()
            speedLogic?.recoilCol = UIColor.whiteColor()
        }
        
        bringSubviewToFront(bttnPause)
        hideEvery(bttnPause)
    }

    func stepByStepSetState(sName: String) { // OperationMainVC 에서 부르는 함수.....
        log.logUiAction(" UnitView : Set State :: \(sName)")
        makeStepByStepStateMachine()
        prevStepState = sName
        stepMan?.setState(sName)
    }

    func enableGraphViewButton() { // 모든 학생이 다 마치면...
        btnOffX.setImage(UIImage(named: "btn_detail"), forState: .Normal)
    }

    func resetStateBySensorError() {
        if stepMan != nil {
            bleMan?.calcManager.data.reset()
            stepByStepSetState(prevStepState)
        }
        else {
            startEntireProcess()
            bleMan?.operationStart()
            sensorErrorSkip = true
        }
    }

    //////////////////////////////////////////////////////////////////////       [ UI Sub  ]

    func update() {
        cnt += 1;  // if cnt < 300 { return }
        if cnt % 30 == 5 { connectionCheck() }

        switch bleMan!.stt {
        case .S_STEPBYSTEP_BREATH, .S_WHOLESTEP_BREATH: cnt += 1
        default: bleMan!.calcManager.data.isMessageDone = false // 압박에서 메시지 처리를 다르게 했으므로 이걸 계속 보내야 함.
        }

        labelTime.text = (bleMan?.dataObj.operationTimer)!.timeInSec()
        if globalState == .S_STEPBYSTEP_COMPRESSION {
            labelTime.text = stepWatch.timeInSec()
        }

        viewHoldingTime.update()
        
        if HsBleMaestr.inst.isAllInOne {
            wholeStepMan?.update()  //print("wholeStepMan?.update()")
        } else {
            stepMan?.update() //print("stepMan?.update()")
        }
    }

    var sensorErrorSkip = false

    //////////////////////////////////////////////////////////////////////       [ UI Sub  ]
    // MARK: DataParsing :  View On Off Control
    func otherStatesDuringAction() {
        stepByStepCommonAction()
        labelCompResp!.text = HsUtil.curStepLocalized((bleMan?.stt)!)
    }
    
    func setArrowImages() {
        for imgs in arrArrow! { imgs.hidden = true }

        //speedLogic?.wrongPositionAct(!bleMan!.dataObj.isCorrectPosition) 떨림 현상

        if (bleMan?.dataObj.isCorrectPosition) == true {
            return
        } // 정상이면 스킵.

        speedLogic?.wrongPositionAct(!bleMan!.dataObj.isCorrectPosition) // 떨림 현상

        //NSLog(@"  Index : %d, %d, %d, %d", imgIdx1, imgIdx2, imgIdx3, imgIdx4); // 머리, 왼, 다리, 오른.
        imgArrowUp.hidden = (bleMan?.calcManager.hideArrow(1))!
        imgArrowOutRight.hidden = (bleMan?.calcManager.hideArrow(2))!
        imgArrowDn.hidden = (bleMan?.calcManager.hideArrow(3))!
        imgArrowMid.hidden = (bleMan?.calcManager.hideArrow(4))!
    }

    //////////////////////////////////////////////////////////////////////     _//////////_     [ UI      << >> ]    _//////////_   Table View
    // MARK: Table View
    private func addResultTableView() {
        log.printAlways("  결과 테이블 뷰 생성.  ", lnum: 5)
        if viewResultTable.tag == 1 {
            viewResultTable.reloadTableView()
            print(" 결과 테이블 이미 생성........ ")
            return
        }
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "ResultTableView", bundle: bundle)
        viewResultTable = nib.instantiateWithOwner(self, options: nil).last as! ResultTableView
        //viewResultTable.frame = CGRect(origin: CGPoint(x: 0, y: 0), size: self.frame.size)
        viewResultTable.frame = CGRect(origin: CGPoint(x: 0, y: 50), size: CGSize(width: 300, height: 340 - 15 - 50))
        viewResultTable.bleMan = bleMan!
        viewResultTable.layer.cornerRadius = 15
        viewResultTable.openResultGraphCljr = self.openResultGraphCljr
        viewResultTable.tag = 1
        
        viewResultTable.reloadTableView()
        
        self.addSubview(viewResultTable)
    }
    
    func addTableCellNib() {
        let nib = UINib(nibName: "ResultTableCell", bundle: nil)
        viewResultTable.tableResult.registerNib(nib, forCellReuseIdentifier: "rsltCell")
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(" numberOfRowsInSection")//print(" numberOfRowsInSection :: \(HsBleSingle.inst().stageLimit) ")
        if ((bleMan?.calcManager.entireStepFinished) == true) { return Int(HsBleSingle.inst().stageLimit) }
        else { return 0 }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        print ("cellForRowAtIndexPath   \(indexPath)")
        var cell:ResultTableCell? = tableView.dequeueReusableCellWithIdentifier("rsltCell") as? ResultTableCell
        if cell == nil {
            addTableCellNib()
            cell = tableView.dequeueReusableCellWithIdentifier("rsltCell") as? ResultTableCell
        }
        cell?.setItems(indexPath.row + 1, dObj: (bleMan?.dataObj)!)
        return cell!
    }
    
    //////////////////////////////////////////////////////////////////////     _//////////_     [ UI      << >> ]    _//////////_   etc
    // MARK: 기타 private
    private func connectionCheck() {
        //log.printThisFunc(" connectionCheck : \(labelTitle.text)")
        // 대기 시간 뷰 할당.
        if cnt < 10 {  // 한번만 실행..
            addHoldingTimeView()   //setStateMachines()
        }
        // 센서 체크
        let sensorInfo = bleMan?.stateMan.sensorStateCheck(), preKitName = " (\(bleMan!.prevKitName))"

        if sensorInfo != nil { //"sensor_piezo" {
            sensorLostCallback(self, sensorInfo! + "  (\(preKitName))")
            resetStateBySensorError()
        }

        // if isTrainerConnected { labelTitle.textColor = colorBttnDarkGreen } 요건 트레이너가 연결 됐는 지 확인 할 때..
        //else { labelTitle.textColor = colorBttnDarkGray }
        labelTitle.textColor = colBttnGreen // colorTxtGreen // 일단은 항상 푸르게 푸르게... ㅋㅋ
        log.logThis("  View : \(labelTitle.text)  \(HsUtil.curStepStr())  ", lnum: 2)
        
        if bleMan == nil { disableThisView(); return }
        if (bleMan?.isConnected == true) {
        } else {
            //print("\n\n\n\n\n  DisConnected     \n\n\n\n\n")
            labelState.text = "It's Not Connected !!!"
            disableThisView()
        }
        
        log.함수차원_출력_End()
    }

    private func addHoldingTimeView() {
        //log.printAlways("  대기 시간 뷰 할당.  ", lnum: 10)
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "HoldingTimeVw", bundle: bundle)
        viewHoldingTime = nib.instantiateWithOwner(self, options: nil).last as! HoldingTimeVw
        viewHoldingTime.frame = CGRect(origin: CGPoint(x: 0, y: 0), size: self.frame.size)
        viewHoldingTime.bleMan = bleMan!
        viewHoldingTime.parentView = self
        viewHoldingTime.layer.cornerRadius = 15
        self.addSubview(viewHoldingTime)
        
        bringSubviewToFront(bttnPause)
        log.함수차원_출력_End()
    }
    
    private func disableThisView() {
        self.hidden = true
        uiTimer.invalidate()
    }
}
