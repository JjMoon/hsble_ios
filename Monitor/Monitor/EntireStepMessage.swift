//
//  EntireStepMessage.swift
//  HS Monitor
//
//  Created by Jongwoo Moon on 2016. 5. 25..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation



import Foundation

/// 모니터 / 테스트 공통..
class EntireStepMessage : UIView {
    var motherView: OperationMainVC?

    @IBOutlet weak var labelPressSensor: UILabel!
    @IBOutlet weak var bttnConfirm: UIButton!

    @IBAction func bttnActConfirm(sender: AnyObject) {
//        if motherView!.wholeStepMan?.curState?.name == "EntireOperations" {
//            //print("  스킵 버튼 >>>  리턴 \(wholeStepMan?.curState?.name)")
//            return
//        }
//
//        motherView!.wholeStepMan?.goToNextStateWithSkipAction(true)
        self.hidden = true
    }

    // MARK:  언어 세팅.
    func setLanguageString() {
        bttnConfirm.setTitle(langStr.obj.confirm, forState: .Normal)
        labelPressSensor.text = langStr.obj.press_sensor_pad
    }


}