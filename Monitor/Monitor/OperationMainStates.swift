//
//  OperationMainStates.swift
//  HS Monitor
//
//  Created by Jongwoo Moon on 2016. 7. 29..
//  Copyright © 2016년 IMLab. All rights reserved.
//

import Foundation


extension OperationMainVC {

    /// 어느 한 뷰라도 압박을 시작했는가?   160730
    func didOperationStart() -> Bool {
        for vw in arrAddedView {
            if !vw.hidden && vw.wholeStepMan?.curState!.name == "EntireOperations" {
                return true
            }
        }
        return false
    }

    func entireProcessStart() {
        log.logUiAction(" MainVC 전과정 시작.  상태머신 시작. ", lnum: 5, printOn: false)
        wholeStepMan = HtGeneralStateManager(myName: "메인 전과정 머신")
        bttnInfo.hidden = false
        vwResultTitle.hidden = true
        viewEntireMsg.hideMe()

        stopAudioCount()

        var aState = HtUnitState(duTime: 22, myName: "Description")
        aState.entryAction = { ()->() in
            HsBleMaestr.inst.setParsingState(.StandBy)
            HsBleMaestr.inst.sendCommand(.S_WHOLESTEP_DESCRIPTION)
            // 오디오 틀고..
            self.playAudio("school")
            for vw in self.getLiveKits() {
                vw.startEntireProcess()
            }
        }
        aState.skipAction = {
            //HsBleMaestr.inst.send
        }
        wholeStepMan?.addAState(aState)

        compState = HtUnitState(duTime: -1, myName: "EntireMessageView")
        compState?.entryAction = {
            self.showEntireStepMessage()
            self.btnSkip.hideMe();
            self.bttnInfo.hideMe()

            HsBleMaestr.inst.sendCommand(.S_WHOLESTEP_PRECPR)  // 160730



        }
        compState?.durinAction = {  // 뷰 중에 하나라도 오퍼레이션이면 뷰를 끄고 넘어가야 함..  160730
            if self.didOperationStart() {
                self.wholeStepMan?.goToNextStateWithSkipAction(true)
            }
        }

        wholeStepMan?.addAState(compState!)

        // 압박 들어오기 전 대기화면은 유닛 뷰에서 처리.
        compState = HtUnitState(duTime: -1, myName: "EntireOperations")
        compState?.entryAction = {
            self.btnSkip.hideMe();
            self.bttnInfo.hideMe()


            // 화면 전환
            HsBleMaestr.inst.setParsingState(.Entire)



        }
        compState?.durinAction = {
            // Maestro 에서 전체 작업이 모두 끝나면 결과 화면으로 넘어가자.
            if HsBleMaestr.inst.didAllConnectedEntireProcessFinished() {
                print("   전체 화면 상태 : TotalResult ")
                self.wholeStepMan?.setState("TotalResult")
            }
        }
        wholeStepMan?.addAState(compState!)

        aState = HtUnitState(duTime: -1, myName: "TotalResult")
        aState.entryAction = {
            print(" Now Result ...   ")
            self.bttnInfo.hidden = false
            // self.vwResultTitle.showMe()  확대뷰에서 보이지 않게..
            self.vwResultTitle.backgroundColor = HmGraphSetting.inst.viewBgBrightGray
            for vw in self.getLiveKits() { vw.enableGraphViewButton() }
            self.view.bringSubviewToFront(self.vwResultTitle)
            HsBleMaestr.inst.setParsingState(.StandBy)

        }

        aState.durinAction = {
            // 확대 뷰가 없을 때 데이터 저장 뷰를 보이게 하자...
            let enlargedVwNum = self.arrAddedView.filter({ (vw) -> Bool in vw.enlarged }).count
            if enlargedVwNum == 0 && self.vwResultTitle.hidden == true {
                self.vwResultTitle.showMe()
            }
        }

        wholeStepMan?.addAState(aState)

        log.printAlways("wholeStepMan?.arrState.count ::  \(wholeStepMan?.arrState.count)")

        wholeStepMan?.prepareActions()
        wholeStepMan?.update()
        setClassButtonBackground()
    }

    /// 압박을 하면 전과정 시작된다는 메시지..   버튼을 누르면 gotoNextState 로 넘김...
    private func showEntireStepMessage() {
        let bundle = NSBundle(forClass: self.dynamicType)
        let anib = UINib(nibName: "EntireStepMessage", bundle: bundle)
        viewEntireMsg = anib.instantiateWithOwner(self, options: nil).last as! EntireStepMessage
        viewEntireMsg.frame = self.view.frame
        viewEntireMsg.motherView = self
        self.view.addSubview(viewEntireMsg)
        viewEntireMsg.setLanguageString()
        viewEntireMsg.showMe()
    }


    // MARK:   단계별 UI 선택
    func stepByStepStart() {
        log.logUiAction(" MainVC 단계별 시작 ", lnum: 5, printOn: false)
        vwResultTitle.hidden = true
        HsBleMaestr.inst.setParsingState(.Step)
        viewEntireMsg.hideMe()
        wholeStepMan = nil

        // setClassButtonBackground() // 단계별 버튼 색 조절..
        setStepButtonConstraints()

        if cprProtoN.theVal == 0 {
            HsBleMaestr.inst.sendCommand(.S_STEPBYSTEP_RESPONSE)
            setClassButtonBackground() // 단계별 버튼 색 조절..
            setBackGroundGreen(bttnHcResponse)
            stepResponse()
        } else {
            HsBleMaestr.inst.sendCommand(.S_STEPBYSTEP_SAFETY)
            setClassButtonBackground() // 단계별 버튼 색 조절..
            setBackGroundGreen(bttnChkDanger)
            stepSafety()
        }


    }

}
